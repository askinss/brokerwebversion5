/**
 * ObjectInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ug.co.waridtel;

public class ObjectInfo  implements java.io.Serializable {
    private int retn;

    private java.lang.String msisdn;

    private java.lang.String IMSI;

    private long currentCredit;

    private int isprecharged;

    public ObjectInfo() {
    }

    public ObjectInfo(
           int retn,
           java.lang.String msisdn,
           java.lang.String IMSI,
           long currentCredit,
           int isprecharged) {
           this.retn = retn;
           this.msisdn = msisdn;
           this.IMSI = IMSI;
           this.currentCredit = currentCredit;
           this.isprecharged = isprecharged;
    }


    /**
     * Gets the retn value for this ObjectInfo.
     * 
     * @return retn
     */
    public int getRetn() {
        return retn;
    }


    /**
     * Sets the retn value for this ObjectInfo.
     * 
     * @param retn
     */
    public void setRetn(int retn) {
        this.retn = retn;
    }


    /**
     * Gets the msisdn value for this ObjectInfo.
     * 
     * @return msisdn
     */
    public java.lang.String getMsisdn() {
        return msisdn;
    }


    /**
     * Sets the msisdn value for this ObjectInfo.
     * 
     * @param msisdn
     */
    public void setMsisdn(java.lang.String msisdn) {
        this.msisdn = msisdn;
    }


    /**
     * Gets the IMSI value for this ObjectInfo.
     * 
     * @return IMSI
     */
    public java.lang.String getIMSI() {
        return IMSI;
    }


    /**
     * Sets the IMSI value for this ObjectInfo.
     * 
     * @param IMSI
     */
    public void setIMSI(java.lang.String IMSI) {
        this.IMSI = IMSI;
    }


    /**
     * Gets the currentCredit value for this ObjectInfo.
     * 
     * @return currentCredit
     */
    public long getCurrentCredit() {
        return currentCredit;
    }


    /**
     * Sets the currentCredit value for this ObjectInfo.
     * 
     * @param currentCredit
     */
    public void setCurrentCredit(long currentCredit) {
        this.currentCredit = currentCredit;
    }


    /**
     * Gets the isprecharged value for this ObjectInfo.
     * 
     * @return isprecharged
     */
    public int getIsprecharged() {
        return isprecharged;
    }


    /**
     * Sets the isprecharged value for this ObjectInfo.
     * 
     * @param isprecharged
     */
    public void setIsprecharged(int isprecharged) {
        this.isprecharged = isprecharged;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ObjectInfo)) return false;
        ObjectInfo other = (ObjectInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.retn == other.getRetn() &&
            ((this.msisdn==null && other.getMsisdn()==null) || 
             (this.msisdn!=null &&
              this.msisdn.equals(other.getMsisdn()))) &&
            ((this.IMSI==null && other.getIMSI()==null) || 
             (this.IMSI!=null &&
              this.IMSI.equals(other.getIMSI()))) &&
            this.currentCredit == other.getCurrentCredit() &&
            this.isprecharged == other.getIsprecharged();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getRetn();
        if (getMsisdn() != null) {
            _hashCode += getMsisdn().hashCode();
        }
        if (getIMSI() != null) {
            _hashCode += getIMSI().hashCode();
        }
        _hashCode += new Long(getCurrentCredit()).hashCode();
        _hashCode += getIsprecharged();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ObjectInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://waridtel.co.ug", "ObjectInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://waridtel.co.ug", "Retn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msisdn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://waridtel.co.ug", "Msisdn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IMSI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://waridtel.co.ug", "IMSI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currentCredit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://waridtel.co.ug", "CurrentCredit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isprecharged");
        elemField.setXmlName(new javax.xml.namespace.QName("http://waridtel.co.ug", "Isprecharged"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
