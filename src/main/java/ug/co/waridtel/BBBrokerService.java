/**
 * BBBrokerService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ug.co.waridtel;

public interface BBBrokerService extends javax.xml.rpc.Service {
    public java.lang.String getBBBrokerServiceSoapAddress();

    public ug.co.waridtel.BBBrokerServiceSoap getBBBrokerServiceSoap() throws javax.xml.rpc.ServiceException;

    public ug.co.waridtel.BBBrokerServiceSoap getBBBrokerServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
