/**
 * BBBrokerServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ug.co.waridtel;

public class BBBrokerServiceLocator extends org.apache.axis.client.Service implements ug.co.waridtel.BBBrokerService {

    public BBBrokerServiceLocator() {
    }


    public BBBrokerServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public BBBrokerServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BBBrokerServiceSoap
    private java.lang.String BBBrokerServiceSoap_address = "http://172.27.110.50/BBBrokerService/BBBrokerService.asmx";

    public java.lang.String getBBBrokerServiceSoapAddress() {
        return BBBrokerServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String BBBrokerServiceSoapWSDDServiceName = "BBBrokerServiceSoap";

    public java.lang.String getBBBrokerServiceSoapWSDDServiceName() {
        return BBBrokerServiceSoapWSDDServiceName;
    }

    public void setBBBrokerServiceSoapWSDDServiceName(java.lang.String name) {
        BBBrokerServiceSoapWSDDServiceName = name;
    }

    public ug.co.waridtel.BBBrokerServiceSoap getBBBrokerServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BBBrokerServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBBBrokerServiceSoap(endpoint);
    }

    public ug.co.waridtel.BBBrokerServiceSoap getBBBrokerServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ug.co.waridtel.BBBrokerServiceSoapStub _stub = new ug.co.waridtel.BBBrokerServiceSoapStub(portAddress, this);
            _stub.setPortName(getBBBrokerServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBBBrokerServiceSoapEndpointAddress(java.lang.String address) {
        BBBrokerServiceSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ug.co.waridtel.BBBrokerServiceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                ug.co.waridtel.BBBrokerServiceSoapStub _stub = new ug.co.waridtel.BBBrokerServiceSoapStub(new java.net.URL(BBBrokerServiceSoap_address), this);
                _stub.setPortName(getBBBrokerServiceSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("BBBrokerServiceSoap".equals(inputPortName)) {
            return getBBBrokerServiceSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://waridtel.co.ug", "BBBrokerService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://waridtel.co.ug", "BBBrokerServiceSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("BBBrokerServiceSoap".equals(portName)) {
            setBBBrokerServiceSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
