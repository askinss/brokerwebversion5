/**
 * BBBrokerServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ug.co.waridtel;

public interface BBBrokerServiceSoap extends java.rmi.Remote {
    public ug.co.waridtel.ObjectInfo getSubscriberProfile(java.lang.String msisdn) throws java.rmi.RemoteException;
}
