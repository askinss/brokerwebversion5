package ug.co.waridtel;

public class BBBrokerServiceSoapProxy implements ug.co.waridtel.BBBrokerServiceSoap {
  private String _endpoint = null;
  private ug.co.waridtel.BBBrokerServiceSoap bBBrokerServiceSoap = null;
  
  public BBBrokerServiceSoapProxy() {
    _initBBBrokerServiceSoapProxy();
  }
  
  public BBBrokerServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initBBBrokerServiceSoapProxy();
  }
  
  private void _initBBBrokerServiceSoapProxy() {
    try {
      bBBrokerServiceSoap = (new ug.co.waridtel.BBBrokerServiceLocator()).getBBBrokerServiceSoap();
      if (bBBrokerServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bBBrokerServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bBBrokerServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bBBrokerServiceSoap != null)
      ((javax.xml.rpc.Stub)bBBrokerServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public ug.co.waridtel.BBBrokerServiceSoap getBBBrokerServiceSoap() {
    if (bBBrokerServiceSoap == null)
      _initBBBrokerServiceSoapProxy();
    return bBBrokerServiceSoap;
  }
  
  public ug.co.waridtel.ObjectInfo getSubscriberProfile(java.lang.String msisdn) throws java.rmi.RemoteException{
    if (bBBrokerServiceSoap == null)
      _initBBBrokerServiceSoapProxy();
    return bBBrokerServiceSoap.getSubscriberProfile(msisdn);
  }
  
  
}