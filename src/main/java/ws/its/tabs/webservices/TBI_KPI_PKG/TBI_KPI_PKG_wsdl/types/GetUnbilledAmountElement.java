/**
 * GetUnbilledAmountElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types;

public class GetUnbilledAmountElement  implements java.io.Serializable {
    private java.lang.String pChannel;

    private java.lang.String pSubno;

    private java.lang.String pArea;

    private java.lang.String pSubscrType;

    private java.lang.String pCallTime;

    public GetUnbilledAmountElement() {
    }

    public GetUnbilledAmountElement(
           java.lang.String pChannel,
           java.lang.String pSubno,
           java.lang.String pArea,
           java.lang.String pSubscrType,
           java.lang.String pCallTime) {
           this.pChannel = pChannel;
           this.pSubno = pSubno;
           this.pArea = pArea;
           this.pSubscrType = pSubscrType;
           this.pCallTime = pCallTime;
    }


    /**
     * Gets the pChannel value for this GetUnbilledAmountElement.
     * 
     * @return pChannel
     */
    public java.lang.String getPChannel() {
        return pChannel;
    }


    /**
     * Sets the pChannel value for this GetUnbilledAmountElement.
     * 
     * @param pChannel
     */
    public void setPChannel(java.lang.String pChannel) {
        this.pChannel = pChannel;
    }


    /**
     * Gets the pSubno value for this GetUnbilledAmountElement.
     * 
     * @return pSubno
     */
    public java.lang.String getPSubno() {
        return pSubno;
    }


    /**
     * Sets the pSubno value for this GetUnbilledAmountElement.
     * 
     * @param pSubno
     */
    public void setPSubno(java.lang.String pSubno) {
        this.pSubno = pSubno;
    }


    /**
     * Gets the pArea value for this GetUnbilledAmountElement.
     * 
     * @return pArea
     */
    public java.lang.String getPArea() {
        return pArea;
    }


    /**
     * Sets the pArea value for this GetUnbilledAmountElement.
     * 
     * @param pArea
     */
    public void setPArea(java.lang.String pArea) {
        this.pArea = pArea;
    }


    /**
     * Gets the pSubscrType value for this GetUnbilledAmountElement.
     * 
     * @return pSubscrType
     */
    public java.lang.String getPSubscrType() {
        return pSubscrType;
    }


    /**
     * Sets the pSubscrType value for this GetUnbilledAmountElement.
     * 
     * @param pSubscrType
     */
    public void setPSubscrType(java.lang.String pSubscrType) {
        this.pSubscrType = pSubscrType;
    }


    /**
     * Gets the pCallTime value for this GetUnbilledAmountElement.
     * 
     * @return pCallTime
     */
    public java.lang.String getPCallTime() {
        return pCallTime;
    }


    /**
     * Sets the pCallTime value for this GetUnbilledAmountElement.
     * 
     * @param pCallTime
     */
    public void setPCallTime(java.lang.String pCallTime) {
        this.pCallTime = pCallTime;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetUnbilledAmountElement)) return false;
        GetUnbilledAmountElement other = (GetUnbilledAmountElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.pChannel==null && other.getPChannel()==null) || 
             (this.pChannel!=null &&
              this.pChannel.equals(other.getPChannel()))) &&
            ((this.pSubno==null && other.getPSubno()==null) || 
             (this.pSubno!=null &&
              this.pSubno.equals(other.getPSubno()))) &&
            ((this.pArea==null && other.getPArea()==null) || 
             (this.pArea!=null &&
              this.pArea.equals(other.getPArea()))) &&
            ((this.pSubscrType==null && other.getPSubscrType()==null) || 
             (this.pSubscrType!=null &&
              this.pSubscrType.equals(other.getPSubscrType()))) &&
            ((this.pCallTime==null && other.getPCallTime()==null) || 
             (this.pCallTime!=null &&
              this.pCallTime.equals(other.getPCallTime())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPChannel() != null) {
            _hashCode += getPChannel().hashCode();
        }
        if (getPSubno() != null) {
            _hashCode += getPSubno().hashCode();
        }
        if (getPArea() != null) {
            _hashCode += getPArea().hashCode();
        }
        if (getPSubscrType() != null) {
            _hashCode += getPSubscrType().hashCode();
        }
        if (getPCallTime() != null) {
            _hashCode += getPCallTime().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetUnbilledAmountElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", ">getUnbilledAmountElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PChannel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "pChannel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PSubno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "pSubno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PArea");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "pArea"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PSubscrType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "pSubscrType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PCallTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "pCallTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
