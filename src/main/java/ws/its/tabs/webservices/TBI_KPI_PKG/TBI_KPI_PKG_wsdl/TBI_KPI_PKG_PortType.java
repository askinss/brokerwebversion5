/**
 * TBI_KPI_PKG_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl;

public interface TBI_KPI_PKG_PortType extends java.rmi.Remote {
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.CheckPinResponseElement checkPin(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.CheckPinElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.CheckserviceforsubscriberResponseElement checkserviceforsubscriber(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.CheckserviceforsubscriberElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetContrTariffProfileResponseElement getContrTariffProfile(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetContrTariffProfileElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetContractInformationResponseElement getContractInformation(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetContractInformationElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetContrnoBillResponseElement getContrnoBill(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetContrnoBillElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetContrnoBlanaceResponseElement getContrnoBlanace(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetContrnoBlanaceElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetCreditCardResponseElement getCreditCard(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetCreditCardElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetCreditLimitResponseElement getCreditLimit(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetCreditLimitElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetCustomerProfileResponseElement getCustomerProfile(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetCustomerProfileElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetEmailResponseElement getEmail(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetEmailElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetHistoryOfPinResponseElement getHistoryOfPin(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetHistoryOfPinElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetInternetResponseElement getInternet(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetInternetElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetOutstandingBalanceResponseElement getOutstandingBalance(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetOutstandingBalanceElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPackageDetailsResponseElement getPackageDetails(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPackageDetailsElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPaymentHistoryResponseElement getPaymentHistory(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPaymentHistoryElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPreOrPostPaidResponseElement getPreOrPostPaid(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPreOrPostPaidElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPukResponseElement getPuk(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPukElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetServiceDetailsResponseElement getServiceDetails(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetServiceDetailsElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubnoBillResponseElement getSubnoBill(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubnoBillElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubnoBlanaceResponseElement getSubnoBlanace(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubnoBlanaceElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubnoTariffProfileResponseElement getSubnoTariffProfile(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubnoTariffProfileElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscriberInformationResponseElement getSubscriberInformation(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscriberInformationElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscriberPinResponseElement getSubscriberPin(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscriberPinElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscribersServicesResponseElement getSubscribersServices(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscribersServicesElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscribersTasksResponseElement getSubscribersTasks(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscribersTasksElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetUnbilledAmountResponseElement getUnbilledAmount(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetUnbilledAmountElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetfriendandfamilymembersResponseElement getfriendandfamilymembers(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetfriendandfamilymembersElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.LogWsTransactionResponseElement logWsTransaction(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.LogWsTransactionElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.SetCreditCardResponseElement setCreditCard(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.SetCreditCardElement parameters) throws java.rmi.RemoteException;
}
