/**
 * TBI_KPI_PKG_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl;

public interface TBI_KPI_PKG_Service extends javax.xml.rpc.Service {
    public java.lang.String getTBI_KPI_PKGSoapHttpPortAddress();

    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKG_PortType getTBI_KPI_PKGSoapHttpPort() throws javax.xml.rpc.ServiceException;

    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKG_PortType getTBI_KPI_PKGSoapHttpPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
