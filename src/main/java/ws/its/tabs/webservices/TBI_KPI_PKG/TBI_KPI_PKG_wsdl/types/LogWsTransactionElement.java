/**
 * LogWsTransactionElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types;

public class LogWsTransactionElement  implements java.io.Serializable {
    private java.lang.String pChannel;

    private java.lang.String pWsName;

    private java.lang.String pInParam;

    private java.lang.String pOutParam;

    private java.lang.String pExcptionParam;

    private java.lang.String pCallTime;

    public LogWsTransactionElement() {
    }

    public LogWsTransactionElement(
           java.lang.String pChannel,
           java.lang.String pWsName,
           java.lang.String pInParam,
           java.lang.String pOutParam,
           java.lang.String pExcptionParam,
           java.lang.String pCallTime) {
           this.pChannel = pChannel;
           this.pWsName = pWsName;
           this.pInParam = pInParam;
           this.pOutParam = pOutParam;
           this.pExcptionParam = pExcptionParam;
           this.pCallTime = pCallTime;
    }


    /**
     * Gets the pChannel value for this LogWsTransactionElement.
     * 
     * @return pChannel
     */
    public java.lang.String getPChannel() {
        return pChannel;
    }


    /**
     * Sets the pChannel value for this LogWsTransactionElement.
     * 
     * @param pChannel
     */
    public void setPChannel(java.lang.String pChannel) {
        this.pChannel = pChannel;
    }


    /**
     * Gets the pWsName value for this LogWsTransactionElement.
     * 
     * @return pWsName
     */
    public java.lang.String getPWsName() {
        return pWsName;
    }


    /**
     * Sets the pWsName value for this LogWsTransactionElement.
     * 
     * @param pWsName
     */
    public void setPWsName(java.lang.String pWsName) {
        this.pWsName = pWsName;
    }


    /**
     * Gets the pInParam value for this LogWsTransactionElement.
     * 
     * @return pInParam
     */
    public java.lang.String getPInParam() {
        return pInParam;
    }


    /**
     * Sets the pInParam value for this LogWsTransactionElement.
     * 
     * @param pInParam
     */
    public void setPInParam(java.lang.String pInParam) {
        this.pInParam = pInParam;
    }


    /**
     * Gets the pOutParam value for this LogWsTransactionElement.
     * 
     * @return pOutParam
     */
    public java.lang.String getPOutParam() {
        return pOutParam;
    }


    /**
     * Sets the pOutParam value for this LogWsTransactionElement.
     * 
     * @param pOutParam
     */
    public void setPOutParam(java.lang.String pOutParam) {
        this.pOutParam = pOutParam;
    }


    /**
     * Gets the pExcptionParam value for this LogWsTransactionElement.
     * 
     * @return pExcptionParam
     */
    public java.lang.String getPExcptionParam() {
        return pExcptionParam;
    }


    /**
     * Sets the pExcptionParam value for this LogWsTransactionElement.
     * 
     * @param pExcptionParam
     */
    public void setPExcptionParam(java.lang.String pExcptionParam) {
        this.pExcptionParam = pExcptionParam;
    }


    /**
     * Gets the pCallTime value for this LogWsTransactionElement.
     * 
     * @return pCallTime
     */
    public java.lang.String getPCallTime() {
        return pCallTime;
    }


    /**
     * Sets the pCallTime value for this LogWsTransactionElement.
     * 
     * @param pCallTime
     */
    public void setPCallTime(java.lang.String pCallTime) {
        this.pCallTime = pCallTime;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LogWsTransactionElement)) return false;
        LogWsTransactionElement other = (LogWsTransactionElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.pChannel==null && other.getPChannel()==null) || 
             (this.pChannel!=null &&
              this.pChannel.equals(other.getPChannel()))) &&
            ((this.pWsName==null && other.getPWsName()==null) || 
             (this.pWsName!=null &&
              this.pWsName.equals(other.getPWsName()))) &&
            ((this.pInParam==null && other.getPInParam()==null) || 
             (this.pInParam!=null &&
              this.pInParam.equals(other.getPInParam()))) &&
            ((this.pOutParam==null && other.getPOutParam()==null) || 
             (this.pOutParam!=null &&
              this.pOutParam.equals(other.getPOutParam()))) &&
            ((this.pExcptionParam==null && other.getPExcptionParam()==null) || 
             (this.pExcptionParam!=null &&
              this.pExcptionParam.equals(other.getPExcptionParam()))) &&
            ((this.pCallTime==null && other.getPCallTime()==null) || 
             (this.pCallTime!=null &&
              this.pCallTime.equals(other.getPCallTime())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPChannel() != null) {
            _hashCode += getPChannel().hashCode();
        }
        if (getPWsName() != null) {
            _hashCode += getPWsName().hashCode();
        }
        if (getPInParam() != null) {
            _hashCode += getPInParam().hashCode();
        }
        if (getPOutParam() != null) {
            _hashCode += getPOutParam().hashCode();
        }
        if (getPExcptionParam() != null) {
            _hashCode += getPExcptionParam().hashCode();
        }
        if (getPCallTime() != null) {
            _hashCode += getPCallTime().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LogWsTransactionElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", ">logWsTransactionElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PChannel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "pChannel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PWsName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "pWsName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PInParam");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "pInParam"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POutParam");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "pOutParam"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PExcptionParam");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "pExcptionParam"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PCallTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "pCallTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
