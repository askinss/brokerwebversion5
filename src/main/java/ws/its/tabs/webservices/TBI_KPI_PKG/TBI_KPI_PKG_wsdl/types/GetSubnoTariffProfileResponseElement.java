/**
 * GetSubnoTariffProfileResponseElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types;

public class GetSubnoTariffProfileResponseElement  implements java.io.Serializable {
    private ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.TBI_KPI_PKGUser_getSubnoTariffProfile_Out result;

    public GetSubnoTariffProfileResponseElement() {
    }

    public GetSubnoTariffProfileResponseElement(
           ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.TBI_KPI_PKGUser_getSubnoTariffProfile_Out result) {
           this.result = result;
    }


    /**
     * Gets the result value for this GetSubnoTariffProfileResponseElement.
     * 
     * @return result
     */
    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.TBI_KPI_PKGUser_getSubnoTariffProfile_Out getResult() {
        return result;
    }


    /**
     * Sets the result value for this GetSubnoTariffProfileResponseElement.
     * 
     * @param result
     */
    public void setResult(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.TBI_KPI_PKGUser_getSubnoTariffProfile_Out result) {
        this.result = result;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetSubnoTariffProfileResponseElement)) return false;
        GetSubnoTariffProfileResponseElement other = (GetSubnoTariffProfileResponseElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.result==null && other.getResult()==null) || 
             (this.result!=null &&
              this.result.equals(other.getResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResult() != null) {
            _hashCode += getResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetSubnoTariffProfileResponseElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", ">getSubnoTariffProfileResponseElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("result");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "TBI_KPI_PKGUser_getSubnoTariffProfile_Out"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
