/**
 * GetSubscribersTasksElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types;

public class GetSubscribersTasksElement  implements java.io.Serializable {
    private java.math.BigDecimal actionStatus;

    private java.lang.String subno;

    private java.lang.String area;

    private java.lang.String subscrType;

    private java.lang.String maxrowcount;

    public GetSubscribersTasksElement() {
    }

    public GetSubscribersTasksElement(
           java.math.BigDecimal actionStatus,
           java.lang.String subno,
           java.lang.String area,
           java.lang.String subscrType,
           java.lang.String maxrowcount) {
           this.actionStatus = actionStatus;
           this.subno = subno;
           this.area = area;
           this.subscrType = subscrType;
           this.maxrowcount = maxrowcount;
    }


    /**
     * Gets the actionStatus value for this GetSubscribersTasksElement.
     * 
     * @return actionStatus
     */
    public java.math.BigDecimal getActionStatus() {
        return actionStatus;
    }


    /**
     * Sets the actionStatus value for this GetSubscribersTasksElement.
     * 
     * @param actionStatus
     */
    public void setActionStatus(java.math.BigDecimal actionStatus) {
        this.actionStatus = actionStatus;
    }


    /**
     * Gets the subno value for this GetSubscribersTasksElement.
     * 
     * @return subno
     */
    public java.lang.String getSubno() {
        return subno;
    }


    /**
     * Sets the subno value for this GetSubscribersTasksElement.
     * 
     * @param subno
     */
    public void setSubno(java.lang.String subno) {
        this.subno = subno;
    }


    /**
     * Gets the area value for this GetSubscribersTasksElement.
     * 
     * @return area
     */
    public java.lang.String getArea() {
        return area;
    }


    /**
     * Sets the area value for this GetSubscribersTasksElement.
     * 
     * @param area
     */
    public void setArea(java.lang.String area) {
        this.area = area;
    }


    /**
     * Gets the subscrType value for this GetSubscribersTasksElement.
     * 
     * @return subscrType
     */
    public java.lang.String getSubscrType() {
        return subscrType;
    }


    /**
     * Sets the subscrType value for this GetSubscribersTasksElement.
     * 
     * @param subscrType
     */
    public void setSubscrType(java.lang.String subscrType) {
        this.subscrType = subscrType;
    }


    /**
     * Gets the maxrowcount value for this GetSubscribersTasksElement.
     * 
     * @return maxrowcount
     */
    public java.lang.String getMaxrowcount() {
        return maxrowcount;
    }


    /**
     * Sets the maxrowcount value for this GetSubscribersTasksElement.
     * 
     * @param maxrowcount
     */
    public void setMaxrowcount(java.lang.String maxrowcount) {
        this.maxrowcount = maxrowcount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetSubscribersTasksElement)) return false;
        GetSubscribersTasksElement other = (GetSubscribersTasksElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.actionStatus==null && other.getActionStatus()==null) || 
             (this.actionStatus!=null &&
              this.actionStatus.equals(other.getActionStatus()))) &&
            ((this.subno==null && other.getSubno()==null) || 
             (this.subno!=null &&
              this.subno.equals(other.getSubno()))) &&
            ((this.area==null && other.getArea()==null) || 
             (this.area!=null &&
              this.area.equals(other.getArea()))) &&
            ((this.subscrType==null && other.getSubscrType()==null) || 
             (this.subscrType!=null &&
              this.subscrType.equals(other.getSubscrType()))) &&
            ((this.maxrowcount==null && other.getMaxrowcount()==null) || 
             (this.maxrowcount!=null &&
              this.maxrowcount.equals(other.getMaxrowcount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getActionStatus() != null) {
            _hashCode += getActionStatus().hashCode();
        }
        if (getSubno() != null) {
            _hashCode += getSubno().hashCode();
        }
        if (getArea() != null) {
            _hashCode += getArea().hashCode();
        }
        if (getSubscrType() != null) {
            _hashCode += getSubscrType().hashCode();
        }
        if (getMaxrowcount() != null) {
            _hashCode += getMaxrowcount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetSubscribersTasksElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", ">getSubscribersTasksElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actionStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "actionStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "subno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("area");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "area"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscrType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "subscrType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxrowcount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "maxrowcount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
