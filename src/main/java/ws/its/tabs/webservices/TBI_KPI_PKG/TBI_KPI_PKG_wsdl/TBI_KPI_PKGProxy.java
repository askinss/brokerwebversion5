package ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl;

public class TBI_KPI_PKGProxy implements ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKG_PortType {
  private String _endpoint = null;
  private ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKG_PortType tBI_KPI_PKG_PortType = null;
  
  public TBI_KPI_PKGProxy() {
    _initTBI_KPI_PKGProxy();
  }
  
  public TBI_KPI_PKGProxy(String endpoint) {
    _endpoint = endpoint;
    _initTBI_KPI_PKGProxy();
  }
  
  private void _initTBI_KPI_PKGProxy() {
    try {
      tBI_KPI_PKG_PortType = (new ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKG_ServiceLocator()).getTBI_KPI_PKGSoapHttpPort();
      if (tBI_KPI_PKG_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)tBI_KPI_PKG_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)tBI_KPI_PKG_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (tBI_KPI_PKG_PortType != null)
      ((javax.xml.rpc.Stub)tBI_KPI_PKG_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKG_PortType getTBI_KPI_PKG_PortType() {
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType;
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.CheckPinResponseElement checkPin(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.CheckPinElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.checkPin(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.CheckserviceforsubscriberResponseElement checkserviceforsubscriber(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.CheckserviceforsubscriberElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.checkserviceforsubscriber(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetContrTariffProfileResponseElement getContrTariffProfile(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetContrTariffProfileElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getContrTariffProfile(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetContractInformationResponseElement getContractInformation(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetContractInformationElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getContractInformation(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetContrnoBillResponseElement getContrnoBill(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetContrnoBillElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getContrnoBill(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetContrnoBlanaceResponseElement getContrnoBlanace(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetContrnoBlanaceElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getContrnoBlanace(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetCreditCardResponseElement getCreditCard(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetCreditCardElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getCreditCard(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetCreditLimitResponseElement getCreditLimit(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetCreditLimitElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getCreditLimit(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetCustomerProfileResponseElement getCustomerProfile(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetCustomerProfileElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getCustomerProfile(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetEmailResponseElement getEmail(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetEmailElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getEmail(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetHistoryOfPinResponseElement getHistoryOfPin(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetHistoryOfPinElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getHistoryOfPin(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetInternetResponseElement getInternet(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetInternetElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getInternet(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetOutstandingBalanceResponseElement getOutstandingBalance(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetOutstandingBalanceElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getOutstandingBalance(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPackageDetailsResponseElement getPackageDetails(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPackageDetailsElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getPackageDetails(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPaymentHistoryResponseElement getPaymentHistory(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPaymentHistoryElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getPaymentHistory(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPreOrPostPaidResponseElement getPreOrPostPaid(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPreOrPostPaidElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getPreOrPostPaid(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPukResponseElement getPuk(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPukElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getPuk(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetServiceDetailsResponseElement getServiceDetails(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetServiceDetailsElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getServiceDetails(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubnoBillResponseElement getSubnoBill(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubnoBillElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getSubnoBill(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubnoBlanaceResponseElement getSubnoBlanace(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubnoBlanaceElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getSubnoBlanace(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubnoTariffProfileResponseElement getSubnoTariffProfile(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubnoTariffProfileElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getSubnoTariffProfile(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscriberInformationResponseElement getSubscriberInformation(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscriberInformationElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getSubscriberInformation(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscriberPinResponseElement getSubscriberPin(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscriberPinElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getSubscriberPin(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscribersServicesResponseElement getSubscribersServices(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscribersServicesElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getSubscribersServices(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscribersTasksResponseElement getSubscribersTasks(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscribersTasksElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getSubscribersTasks(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetUnbilledAmountResponseElement getUnbilledAmount(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetUnbilledAmountElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getUnbilledAmount(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetfriendandfamilymembersResponseElement getfriendandfamilymembers(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetfriendandfamilymembersElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.getfriendandfamilymembers(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.LogWsTransactionResponseElement logWsTransaction(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.LogWsTransactionElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.logWsTransaction(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.SetCreditCardResponseElement setCreditCard(ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.SetCreditCardElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_PortType == null)
      _initTBI_KPI_PKGProxy();
    return tBI_KPI_PKG_PortType.setCreditCard(parameters);
  }
  
  
}