/**
 * GetCreditLimitElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types;

public class GetCreditLimitElement  implements java.io.Serializable {
    private java.lang.String userCode;

    private java.lang.String userType;

    private java.lang.String maxrowcount;

    public GetCreditLimitElement() {
    }

    public GetCreditLimitElement(
           java.lang.String userCode,
           java.lang.String userType,
           java.lang.String maxrowcount) {
           this.userCode = userCode;
           this.userType = userType;
           this.maxrowcount = maxrowcount;
    }


    /**
     * Gets the userCode value for this GetCreditLimitElement.
     * 
     * @return userCode
     */
    public java.lang.String getUserCode() {
        return userCode;
    }


    /**
     * Sets the userCode value for this GetCreditLimitElement.
     * 
     * @param userCode
     */
    public void setUserCode(java.lang.String userCode) {
        this.userCode = userCode;
    }


    /**
     * Gets the userType value for this GetCreditLimitElement.
     * 
     * @return userType
     */
    public java.lang.String getUserType() {
        return userType;
    }


    /**
     * Sets the userType value for this GetCreditLimitElement.
     * 
     * @param userType
     */
    public void setUserType(java.lang.String userType) {
        this.userType = userType;
    }


    /**
     * Gets the maxrowcount value for this GetCreditLimitElement.
     * 
     * @return maxrowcount
     */
    public java.lang.String getMaxrowcount() {
        return maxrowcount;
    }


    /**
     * Sets the maxrowcount value for this GetCreditLimitElement.
     * 
     * @param maxrowcount
     */
    public void setMaxrowcount(java.lang.String maxrowcount) {
        this.maxrowcount = maxrowcount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetCreditLimitElement)) return false;
        GetCreditLimitElement other = (GetCreditLimitElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.userCode==null && other.getUserCode()==null) || 
             (this.userCode!=null &&
              this.userCode.equals(other.getUserCode()))) &&
            ((this.userType==null && other.getUserType()==null) || 
             (this.userType!=null &&
              this.userType.equals(other.getUserType()))) &&
            ((this.maxrowcount==null && other.getMaxrowcount()==null) || 
             (this.maxrowcount!=null &&
              this.maxrowcount.equals(other.getMaxrowcount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUserCode() != null) {
            _hashCode += getUserCode().hashCode();
        }
        if (getUserType() != null) {
            _hashCode += getUserType().hashCode();
        }
        if (getMaxrowcount() != null) {
            _hashCode += getMaxrowcount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetCreditLimitElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", ">getCreditLimitElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "userCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "userType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxrowcount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "maxrowcount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
