/**
 * TBI_KPI_PKGUser_getSubnoTariffProfile_Out.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types;

public class TBI_KPI_PKGUser_getSubnoTariffProfile_Out  implements java.io.Serializable {
    private java.lang.String _return;

    private java.lang.String pcontrnoOut;

    public TBI_KPI_PKGUser_getSubnoTariffProfile_Out() {
    }

    public TBI_KPI_PKGUser_getSubnoTariffProfile_Out(
           java.lang.String _return,
           java.lang.String pcontrnoOut) {
           this._return = _return;
           this.pcontrnoOut = pcontrnoOut;
    }


    /**
     * Gets the _return value for this TBI_KPI_PKGUser_getSubnoTariffProfile_Out.
     * 
     * @return _return
     */
    public java.lang.String get_return() {
        return _return;
    }


    /**
     * Sets the _return value for this TBI_KPI_PKGUser_getSubnoTariffProfile_Out.
     * 
     * @param _return
     */
    public void set_return(java.lang.String _return) {
        this._return = _return;
    }


    /**
     * Gets the pcontrnoOut value for this TBI_KPI_PKGUser_getSubnoTariffProfile_Out.
     * 
     * @return pcontrnoOut
     */
    public java.lang.String getPcontrnoOut() {
        return pcontrnoOut;
    }


    /**
     * Sets the pcontrnoOut value for this TBI_KPI_PKGUser_getSubnoTariffProfile_Out.
     * 
     * @param pcontrnoOut
     */
    public void setPcontrnoOut(java.lang.String pcontrnoOut) {
        this.pcontrnoOut = pcontrnoOut;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TBI_KPI_PKGUser_getSubnoTariffProfile_Out)) return false;
        TBI_KPI_PKGUser_getSubnoTariffProfile_Out other = (TBI_KPI_PKGUser_getSubnoTariffProfile_Out) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this._return==null && other.get_return()==null) || 
             (this._return!=null &&
              this._return.equals(other.get_return()))) &&
            ((this.pcontrnoOut==null && other.getPcontrnoOut()==null) || 
             (this.pcontrnoOut!=null &&
              this.pcontrnoOut.equals(other.getPcontrnoOut())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (get_return() != null) {
            _hashCode += get_return().hashCode();
        }
        if (getPcontrnoOut() != null) {
            _hashCode += getPcontrnoOut().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TBI_KPI_PKGUser_getSubnoTariffProfile_Out.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "TBI_KPI_PKGUser_getSubnoTariffProfile_Out"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("_return");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "return"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pcontrnoOut");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl/types/", "pcontrnoOut"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
