/**
 * TBI_KPI_PKG_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl;

import com.vasconsulting.www.utility.LoadAllProperties;

public class TBI_KPI_PKG_ServiceLocator extends org.apache.axis.client.Service implements ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKG_Service {
	private LoadAllProperties properties = new LoadAllProperties();
    public TBI_KPI_PKG_ServiceLocator() {
    }


    public TBI_KPI_PKG_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public TBI_KPI_PKG_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for TBI_KPI_PKGSoapHttpPort
    private java.lang.String TBI_KPI_PKGSoapHttpPort_address = properties.getProperty("getTBI_KPI_PKGSoapHttpPortAddress");
    public java.lang.String getTBI_KPI_PKGSoapHttpPortAddress() {
        return TBI_KPI_PKGSoapHttpPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String TBI_KPI_PKGSoapHttpPortWSDDServiceName = "TBI_KPI_PKGSoapHttpPort";

    public java.lang.String getTBI_KPI_PKGSoapHttpPortWSDDServiceName() {
        return TBI_KPI_PKGSoapHttpPortWSDDServiceName;
    }

    public void setTBI_KPI_PKGSoapHttpPortWSDDServiceName(java.lang.String name) {
        TBI_KPI_PKGSoapHttpPortWSDDServiceName = name;
    }

    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKG_PortType getTBI_KPI_PKGSoapHttpPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
       System.out.println("Sending TABS request to "+TBI_KPI_PKGSoapHttpPort_address);
        try {
            endpoint = new java.net.URL(TBI_KPI_PKGSoapHttpPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getTBI_KPI_PKGSoapHttpPort(endpoint);
    }

    public ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKG_PortType getTBI_KPI_PKGSoapHttpPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKGSoapHttpStub _stub = new ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKGSoapHttpStub(portAddress, this);
            _stub.setPortName(getTBI_KPI_PKGSoapHttpPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setTBI_KPI_PKGSoapHttpPortEndpointAddress(java.lang.String address) {
        TBI_KPI_PKGSoapHttpPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKG_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKGSoapHttpStub _stub = new ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKGSoapHttpStub(new java.net.URL(TBI_KPI_PKGSoapHttpPort_address), this);
                _stub.setPortName(getTBI_KPI_PKGSoapHttpPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("TBI_KPI_PKGSoapHttpPort".equals(inputPortName)) {
            return getTBI_KPI_PKGSoapHttpPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl", "TBI_KPI_PKG");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG/TBI_KPI_PKG.wsdl", "TBI_KPI_PKGSoapHttpPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("TBI_KPI_PKGSoapHttpPort".equals(portName)) {
            setTBI_KPI_PKGSoapHttpPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
