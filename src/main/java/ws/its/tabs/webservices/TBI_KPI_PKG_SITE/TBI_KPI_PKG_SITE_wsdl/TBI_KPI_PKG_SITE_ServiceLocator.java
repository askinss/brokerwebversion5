/**
 * TBI_KPI_PKG_SITE_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl;

import com.vasconsulting.www.utility.LoadAllProperties;

public class TBI_KPI_PKG_SITE_ServiceLocator extends org.apache.axis.client.Service implements ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.TBI_KPI_PKG_SITE_Service {
	private LoadAllProperties properties = new LoadAllProperties();

    public TBI_KPI_PKG_SITE_ServiceLocator() {
    }


    public TBI_KPI_PKG_SITE_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public TBI_KPI_PKG_SITE_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for TBI_KPI_PKG_SITESoapHttpPort
    private java.lang.String TBI_KPI_PKG_SITESoapHttpPort_address = properties.getProperty("getTBI_KPI_PKGSoapHttpPortAddress");

    public java.lang.String getTBI_KPI_PKG_SITESoapHttpPortAddress() {
        return TBI_KPI_PKG_SITESoapHttpPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String TBI_KPI_PKG_SITESoapHttpPortWSDDServiceName = "TBI_KPI_PKG_SITESoapHttpPort";

    public java.lang.String getTBI_KPI_PKG_SITESoapHttpPortWSDDServiceName() {
        return TBI_KPI_PKG_SITESoapHttpPortWSDDServiceName;
    }

    public void setTBI_KPI_PKG_SITESoapHttpPortWSDDServiceName(java.lang.String name) {
        TBI_KPI_PKG_SITESoapHttpPortWSDDServiceName = name;
    }

    public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.TBI_KPI_PKG_SITE_PortType getTBI_KPI_PKG_SITESoapHttpPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(TBI_KPI_PKG_SITESoapHttpPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getTBI_KPI_PKG_SITESoapHttpPort(endpoint);
    }

    public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.TBI_KPI_PKG_SITE_PortType getTBI_KPI_PKG_SITESoapHttpPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.TBI_KPI_PKG_SITESoapHttpStub _stub = new ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.TBI_KPI_PKG_SITESoapHttpStub(portAddress, this);
            _stub.setPortName(getTBI_KPI_PKG_SITESoapHttpPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setTBI_KPI_PKG_SITESoapHttpPortEndpointAddress(java.lang.String address) {
        TBI_KPI_PKG_SITESoapHttpPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.TBI_KPI_PKG_SITE_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.TBI_KPI_PKG_SITESoapHttpStub _stub = new ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.TBI_KPI_PKG_SITESoapHttpStub(new java.net.URL(TBI_KPI_PKG_SITESoapHttpPort_address), this);
                _stub.setPortName(getTBI_KPI_PKG_SITESoapHttpPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("TBI_KPI_PKG_SITESoapHttpPort".equals(inputPortName)) {
            return getTBI_KPI_PKG_SITESoapHttpPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl", "TBI_KPI_PKG_SITE");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl", "TBI_KPI_PKG_SITESoapHttpPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("TBI_KPI_PKG_SITESoapHttpPort".equals(portName)) {
            setTBI_KPI_PKG_SITESoapHttpPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
