/**
 * GetSignElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types;

public class GetSignElement  implements java.io.Serializable {
    private java.lang.String pChannel;

    private java.math.BigDecimal pFrom;

    private java.math.BigDecimal pTo;

    public GetSignElement() {
    }

    public GetSignElement(
           java.lang.String pChannel,
           java.math.BigDecimal pFrom,
           java.math.BigDecimal pTo) {
           this.pChannel = pChannel;
           this.pFrom = pFrom;
           this.pTo = pTo;
    }


    /**
     * Gets the pChannel value for this GetSignElement.
     * 
     * @return pChannel
     */
    public java.lang.String getPChannel() {
        return pChannel;
    }


    /**
     * Sets the pChannel value for this GetSignElement.
     * 
     * @param pChannel
     */
    public void setPChannel(java.lang.String pChannel) {
        this.pChannel = pChannel;
    }


    /**
     * Gets the pFrom value for this GetSignElement.
     * 
     * @return pFrom
     */
    public java.math.BigDecimal getPFrom() {
        return pFrom;
    }


    /**
     * Sets the pFrom value for this GetSignElement.
     * 
     * @param pFrom
     */
    public void setPFrom(java.math.BigDecimal pFrom) {
        this.pFrom = pFrom;
    }


    /**
     * Gets the pTo value for this GetSignElement.
     * 
     * @return pTo
     */
    public java.math.BigDecimal getPTo() {
        return pTo;
    }


    /**
     * Sets the pTo value for this GetSignElement.
     * 
     * @param pTo
     */
    public void setPTo(java.math.BigDecimal pTo) {
        this.pTo = pTo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetSignElement)) return false;
        GetSignElement other = (GetSignElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.pChannel==null && other.getPChannel()==null) || 
             (this.pChannel!=null &&
              this.pChannel.equals(other.getPChannel()))) &&
            ((this.pFrom==null && other.getPFrom()==null) || 
             (this.pFrom!=null &&
              this.pFrom.equals(other.getPFrom()))) &&
            ((this.pTo==null && other.getPTo()==null) || 
             (this.pTo!=null &&
              this.pTo.equals(other.getPTo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPChannel() != null) {
            _hashCode += getPChannel().hashCode();
        }
        if (getPFrom() != null) {
            _hashCode += getPFrom().hashCode();
        }
        if (getPTo() != null) {
            _hashCode += getPTo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetSignElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl/types/", ">getSignElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PChannel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl/types/", "pChannel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PFrom");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl/types/", "pFrom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PTo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl/types/", "pTo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
