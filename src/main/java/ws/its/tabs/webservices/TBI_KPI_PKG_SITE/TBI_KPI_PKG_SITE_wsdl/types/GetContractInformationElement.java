/**
 * GetContractInformationElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types;

public class GetContractInformationElement  implements java.io.Serializable {
    private java.lang.String contrno;

    private java.lang.String maxrowcount;

    public GetContractInformationElement() {
    }

    public GetContractInformationElement(
           java.lang.String contrno,
           java.lang.String maxrowcount) {
           this.contrno = contrno;
           this.maxrowcount = maxrowcount;
    }


    /**
     * Gets the contrno value for this GetContractInformationElement.
     * 
     * @return contrno
     */
    public java.lang.String getContrno() {
        return contrno;
    }


    /**
     * Sets the contrno value for this GetContractInformationElement.
     * 
     * @param contrno
     */
    public void setContrno(java.lang.String contrno) {
        this.contrno = contrno;
    }


    /**
     * Gets the maxrowcount value for this GetContractInformationElement.
     * 
     * @return maxrowcount
     */
    public java.lang.String getMaxrowcount() {
        return maxrowcount;
    }


    /**
     * Sets the maxrowcount value for this GetContractInformationElement.
     * 
     * @param maxrowcount
     */
    public void setMaxrowcount(java.lang.String maxrowcount) {
        this.maxrowcount = maxrowcount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetContractInformationElement)) return false;
        GetContractInformationElement other = (GetContractInformationElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.contrno==null && other.getContrno()==null) || 
             (this.contrno!=null &&
              this.contrno.equals(other.getContrno()))) &&
            ((this.maxrowcount==null && other.getMaxrowcount()==null) || 
             (this.maxrowcount!=null &&
              this.maxrowcount.equals(other.getMaxrowcount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getContrno() != null) {
            _hashCode += getContrno().hashCode();
        }
        if (getMaxrowcount() != null) {
            _hashCode += getMaxrowcount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetContractInformationElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl/types/", ">getContractInformationElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contrno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl/types/", "contrno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxrowcount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl/types/", "maxrowcount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
