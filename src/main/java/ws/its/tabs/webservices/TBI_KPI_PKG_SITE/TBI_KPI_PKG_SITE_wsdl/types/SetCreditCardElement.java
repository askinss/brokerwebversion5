/**
 * SetCreditCardElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types;

public class SetCreditCardElement  implements java.io.Serializable {
    private java.lang.String pChannel;

    private java.lang.String pSubno;

    private java.lang.String pArea;

    private java.lang.String pSubscrType;

    private java.lang.String pCardIssuer;

    private java.lang.String pCreditCard;

    private java.lang.String pExpiryDate;

    private java.lang.String pCallTime;

    private java.lang.String pOrderBy;

    public SetCreditCardElement() {
    }

    public SetCreditCardElement(
           java.lang.String pChannel,
           java.lang.String pSubno,
           java.lang.String pArea,
           java.lang.String pSubscrType,
           java.lang.String pCardIssuer,
           java.lang.String pCreditCard,
           java.lang.String pExpiryDate,
           java.lang.String pCallTime,
           java.lang.String pOrderBy) {
           this.pChannel = pChannel;
           this.pSubno = pSubno;
           this.pArea = pArea;
           this.pSubscrType = pSubscrType;
           this.pCardIssuer = pCardIssuer;
           this.pCreditCard = pCreditCard;
           this.pExpiryDate = pExpiryDate;
           this.pCallTime = pCallTime;
           this.pOrderBy = pOrderBy;
    }


    /**
     * Gets the pChannel value for this SetCreditCardElement.
     * 
     * @return pChannel
     */
    public java.lang.String getPChannel() {
        return pChannel;
    }


    /**
     * Sets the pChannel value for this SetCreditCardElement.
     * 
     * @param pChannel
     */
    public void setPChannel(java.lang.String pChannel) {
        this.pChannel = pChannel;
    }


    /**
     * Gets the pSubno value for this SetCreditCardElement.
     * 
     * @return pSubno
     */
    public java.lang.String getPSubno() {
        return pSubno;
    }


    /**
     * Sets the pSubno value for this SetCreditCardElement.
     * 
     * @param pSubno
     */
    public void setPSubno(java.lang.String pSubno) {
        this.pSubno = pSubno;
    }


    /**
     * Gets the pArea value for this SetCreditCardElement.
     * 
     * @return pArea
     */
    public java.lang.String getPArea() {
        return pArea;
    }


    /**
     * Sets the pArea value for this SetCreditCardElement.
     * 
     * @param pArea
     */
    public void setPArea(java.lang.String pArea) {
        this.pArea = pArea;
    }


    /**
     * Gets the pSubscrType value for this SetCreditCardElement.
     * 
     * @return pSubscrType
     */
    public java.lang.String getPSubscrType() {
        return pSubscrType;
    }


    /**
     * Sets the pSubscrType value for this SetCreditCardElement.
     * 
     * @param pSubscrType
     */
    public void setPSubscrType(java.lang.String pSubscrType) {
        this.pSubscrType = pSubscrType;
    }


    /**
     * Gets the pCardIssuer value for this SetCreditCardElement.
     * 
     * @return pCardIssuer
     */
    public java.lang.String getPCardIssuer() {
        return pCardIssuer;
    }


    /**
     * Sets the pCardIssuer value for this SetCreditCardElement.
     * 
     * @param pCardIssuer
     */
    public void setPCardIssuer(java.lang.String pCardIssuer) {
        this.pCardIssuer = pCardIssuer;
    }


    /**
     * Gets the pCreditCard value for this SetCreditCardElement.
     * 
     * @return pCreditCard
     */
    public java.lang.String getPCreditCard() {
        return pCreditCard;
    }


    /**
     * Sets the pCreditCard value for this SetCreditCardElement.
     * 
     * @param pCreditCard
     */
    public void setPCreditCard(java.lang.String pCreditCard) {
        this.pCreditCard = pCreditCard;
    }


    /**
     * Gets the pExpiryDate value for this SetCreditCardElement.
     * 
     * @return pExpiryDate
     */
    public java.lang.String getPExpiryDate() {
        return pExpiryDate;
    }


    /**
     * Sets the pExpiryDate value for this SetCreditCardElement.
     * 
     * @param pExpiryDate
     */
    public void setPExpiryDate(java.lang.String pExpiryDate) {
        this.pExpiryDate = pExpiryDate;
    }


    /**
     * Gets the pCallTime value for this SetCreditCardElement.
     * 
     * @return pCallTime
     */
    public java.lang.String getPCallTime() {
        return pCallTime;
    }


    /**
     * Sets the pCallTime value for this SetCreditCardElement.
     * 
     * @param pCallTime
     */
    public void setPCallTime(java.lang.String pCallTime) {
        this.pCallTime = pCallTime;
    }


    /**
     * Gets the pOrderBy value for this SetCreditCardElement.
     * 
     * @return pOrderBy
     */
    public java.lang.String getPOrderBy() {
        return pOrderBy;
    }


    /**
     * Sets the pOrderBy value for this SetCreditCardElement.
     * 
     * @param pOrderBy
     */
    public void setPOrderBy(java.lang.String pOrderBy) {
        this.pOrderBy = pOrderBy;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SetCreditCardElement)) return false;
        SetCreditCardElement other = (SetCreditCardElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.pChannel==null && other.getPChannel()==null) || 
             (this.pChannel!=null &&
              this.pChannel.equals(other.getPChannel()))) &&
            ((this.pSubno==null && other.getPSubno()==null) || 
             (this.pSubno!=null &&
              this.pSubno.equals(other.getPSubno()))) &&
            ((this.pArea==null && other.getPArea()==null) || 
             (this.pArea!=null &&
              this.pArea.equals(other.getPArea()))) &&
            ((this.pSubscrType==null && other.getPSubscrType()==null) || 
             (this.pSubscrType!=null &&
              this.pSubscrType.equals(other.getPSubscrType()))) &&
            ((this.pCardIssuer==null && other.getPCardIssuer()==null) || 
             (this.pCardIssuer!=null &&
              this.pCardIssuer.equals(other.getPCardIssuer()))) &&
            ((this.pCreditCard==null && other.getPCreditCard()==null) || 
             (this.pCreditCard!=null &&
              this.pCreditCard.equals(other.getPCreditCard()))) &&
            ((this.pExpiryDate==null && other.getPExpiryDate()==null) || 
             (this.pExpiryDate!=null &&
              this.pExpiryDate.equals(other.getPExpiryDate()))) &&
            ((this.pCallTime==null && other.getPCallTime()==null) || 
             (this.pCallTime!=null &&
              this.pCallTime.equals(other.getPCallTime()))) &&
            ((this.pOrderBy==null && other.getPOrderBy()==null) || 
             (this.pOrderBy!=null &&
              this.pOrderBy.equals(other.getPOrderBy())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPChannel() != null) {
            _hashCode += getPChannel().hashCode();
        }
        if (getPSubno() != null) {
            _hashCode += getPSubno().hashCode();
        }
        if (getPArea() != null) {
            _hashCode += getPArea().hashCode();
        }
        if (getPSubscrType() != null) {
            _hashCode += getPSubscrType().hashCode();
        }
        if (getPCardIssuer() != null) {
            _hashCode += getPCardIssuer().hashCode();
        }
        if (getPCreditCard() != null) {
            _hashCode += getPCreditCard().hashCode();
        }
        if (getPExpiryDate() != null) {
            _hashCode += getPExpiryDate().hashCode();
        }
        if (getPCallTime() != null) {
            _hashCode += getPCallTime().hashCode();
        }
        if (getPOrderBy() != null) {
            _hashCode += getPOrderBy().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SetCreditCardElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl/types/", ">setCreditCardElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PChannel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl/types/", "pChannel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PSubno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl/types/", "pSubno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PArea");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl/types/", "pArea"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PSubscrType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl/types/", "pSubscrType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PCardIssuer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl/types/", "pCardIssuer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PCreditCard");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl/types/", "pCreditCard"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PExpiryDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl/types/", "pExpiryDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PCallTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl/types/", "pCallTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POrderBy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl/types/", "pOrderBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
