/**
 * GetAgentStatusElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types;

public class GetAgentStatusElement  implements java.io.Serializable {
    private java.lang.String pChannel;

    private java.lang.String pUsername;

    public GetAgentStatusElement() {
    }

    public GetAgentStatusElement(
           java.lang.String pChannel,
           java.lang.String pUsername) {
           this.pChannel = pChannel;
           this.pUsername = pUsername;
    }


    /**
     * Gets the pChannel value for this GetAgentStatusElement.
     * 
     * @return pChannel
     */
    public java.lang.String getPChannel() {
        return pChannel;
    }


    /**
     * Sets the pChannel value for this GetAgentStatusElement.
     * 
     * @param pChannel
     */
    public void setPChannel(java.lang.String pChannel) {
        this.pChannel = pChannel;
    }


    /**
     * Gets the pUsername value for this GetAgentStatusElement.
     * 
     * @return pUsername
     */
    public java.lang.String getPUsername() {
        return pUsername;
    }


    /**
     * Sets the pUsername value for this GetAgentStatusElement.
     * 
     * @param pUsername
     */
    public void setPUsername(java.lang.String pUsername) {
        this.pUsername = pUsername;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetAgentStatusElement)) return false;
        GetAgentStatusElement other = (GetAgentStatusElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.pChannel==null && other.getPChannel()==null) || 
             (this.pChannel!=null &&
              this.pChannel.equals(other.getPChannel()))) &&
            ((this.pUsername==null && other.getPUsername()==null) || 
             (this.pUsername!=null &&
              this.pUsername.equals(other.getPUsername())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPChannel() != null) {
            _hashCode += getPChannel().hashCode();
        }
        if (getPUsername() != null) {
            _hashCode += getPUsername().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetAgentStatusElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl/types/", ">getAgentStatusElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PChannel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl/types/", "pChannel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PUsername");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/TBI_KPI_PKG_SITE/TBI_KPI_PKG_SITE.wsdl/types/", "pUsername"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
