package ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl;

public class TBI_KPI_PKG_SITEProxy implements ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.TBI_KPI_PKG_SITE_PortType {
  private String _endpoint = null;
  private ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.TBI_KPI_PKG_SITE_PortType tBI_KPI_PKG_SITE_PortType = null;
  
  public TBI_KPI_PKG_SITEProxy() {
    _initTBI_KPI_PKG_SITEProxy();
  }
  
  public TBI_KPI_PKG_SITEProxy(String endpoint) {
    _endpoint = endpoint;
    _initTBI_KPI_PKG_SITEProxy();
  }
  
  private void _initTBI_KPI_PKG_SITEProxy() {
    try {
      tBI_KPI_PKG_SITE_PortType = (new ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.TBI_KPI_PKG_SITE_ServiceLocator()).getTBI_KPI_PKG_SITESoapHttpPort();
      if (tBI_KPI_PKG_SITE_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)tBI_KPI_PKG_SITE_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)tBI_KPI_PKG_SITE_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (tBI_KPI_PKG_SITE_PortType != null)
      ((javax.xml.rpc.Stub)tBI_KPI_PKG_SITE_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.TBI_KPI_PKG_SITE_PortType getTBI_KPI_PKG_SITE_PortType() {
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType;
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.CheckPinResponseElement checkPin(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.CheckPinElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.checkPin(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.CheckserviceforsubscriberResponseElement checkserviceforsubscriber(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.CheckserviceforsubscriberElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.checkserviceforsubscriber(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetAgentStatusResponseElement getAgentStatus(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetAgentStatusElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getAgentStatus(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetComplexLevelResponseElement getComplexLevel(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetComplexLevelElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getComplexLevel(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetContrTariffProfileResponseElement getContrTariffProfile(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetContrTariffProfileElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getContrTariffProfile(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetContractInformationResponseElement getContractInformation(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetContractInformationElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getContractInformation(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetContrnoBillResponseElement getContrnoBill(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetContrnoBillElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getContrnoBill(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetContrnoBlanaceResponseElement getContrnoBlanace(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetContrnoBlanaceElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getContrnoBlanace(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetCreditCardResponseElement getCreditCard(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetCreditCardElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getCreditCard(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetCreditLimitResponseElement getCreditLimit(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetCreditLimitElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getCreditLimit(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetCustomerProfileResponseElement getCustomerProfile(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetCustomerProfileElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getCustomerProfile(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetDefaultBackupUserResponseElement getDefaultBackupUser(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetDefaultBackupUserElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getDefaultBackupUser(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetDefaultRoleResponseElement getDefaultRole(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetDefaultRoleElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getDefaultRole(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetDepartementResponseElement getDepartement(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetDepartementElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getDepartement(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetDivisionResponseElement getDivision(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetDivisionElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getDivision(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetEmailResponseElement getEmail(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetEmailElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getEmail(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetGroupResponseElement getGroup(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetGroupElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getGroup(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetHistoryOfPinResponseElement getHistoryOfPin(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetHistoryOfPinElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getHistoryOfPin(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetInternetResponseElement getInternet(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetInternetElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getInternet(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetLanguageResponseElement getLanguage(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetLanguageElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getLanguage(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetOutstandingBalanceResponseElement getOutstandingBalance(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetOutstandingBalanceElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getOutstandingBalance(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetPackageDetailsResponseElement getPackageDetails(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetPackageDetailsElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getPackageDetails(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetPaymentHistoryResponseElement getPaymentHistory(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetPaymentHistoryElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getPaymentHistory(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetPreOrPostPaidResponseElement getPreOrPostPaid(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetPreOrPostPaidElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getPreOrPostPaid(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetPrinterGroupResponseElement getPrinterGroup(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetPrinterGroupElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getPrinterGroup(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetProfileResponseElement getProfile(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetProfileElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getProfile(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetPukResponseElement getPuk(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetPukElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getPuk(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetRegionResponseElement getRegion(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetRegionElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getRegion(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetServiceDetailsResponseElement getServiceDetails(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetServiceDetailsElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getServiceDetails(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetSignResponseElement getSign(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetSignElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getSign(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetSubnoBillResponseElement getSubnoBill(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetSubnoBillElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getSubnoBill(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetSubnoBlanaceResponseElement getSubnoBlanace(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetSubnoBlanaceElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getSubnoBlanace(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetSubnoTariffProfileResponseElement getSubnoTariffProfile(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetSubnoTariffProfileElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getSubnoTariffProfile(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetSubscriberInformationResponseElement getSubscriberInformation(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetSubscriberInformationElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getSubscriberInformation(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetSubscriberPinResponseElement getSubscriberPin(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetSubscriberPinElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getSubscriberPin(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetSubscribersServicesResponseElement getSubscribersServices(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetSubscribersServicesElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getSubscribersServices(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetSubscribersTasksResponseElement getSubscribersTasks(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetSubscribersTasksElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getSubscribersTasks(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetTablespaceResponseElement getTablespace(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetTablespaceElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getTablespace(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetTemporaryTablespaceResponseElement getTemporaryTablespace(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetTemporaryTablespaceElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getTemporaryTablespace(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetUnbilledAmountResponseElement getUnbilledAmount(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetUnbilledAmountElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getUnbilledAmount(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetUserInformationResponseElement getUserInformation(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetUserInformationElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getUserInformation(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetUsersIdResponseElement getUsersId(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetUsersIdElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getUsersId(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetfriendandfamilymembersResponseElement getfriendandfamilymembers(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.GetfriendandfamilymembersElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.getfriendandfamilymembers(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.LogWsTransactionResponseElement logWsTransaction(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.LogWsTransactionElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.logWsTransaction(parameters);
  }
  
  public ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.SetCreditCardResponseElement setCreditCard(ws.its.tabs.webservices.TBI_KPI_PKG_SITE.TBI_KPI_PKG_SITE_wsdl.types.SetCreditCardElement parameters) throws java.rmi.RemoteException{
    if (tBI_KPI_PKG_SITE_PortType == null)
      _initTBI_KPI_PKG_SITEProxy();
    return tBI_KPI_PKG_SITE_PortType.setCreditCard(parameters);
  }
  
  
}