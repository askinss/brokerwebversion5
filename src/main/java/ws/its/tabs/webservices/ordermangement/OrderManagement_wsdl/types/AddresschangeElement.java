/**
 * AddresschangeElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types;

public class AddresschangeElement  implements java.io.Serializable {
    private java.lang.String pArea;

    private java.lang.String pSubscrtype;

    private java.lang.String pSubno;

    private java.lang.String pUsername;

    private java.lang.String pServices;

    private java.lang.String pPoBox;

    private java.lang.String pPostArea;

    private java.lang.String pZipCode;

    private java.lang.String pAddrsArea;

    private java.lang.String pAddrsBlock;

    private java.lang.String pStreet;

    private java.lang.String pBuilding;

    private java.lang.String pApartment;

    private java.lang.String pHlrProfile;

    private java.lang.String pSndcmd;

    private java.lang.String pAdditonalparams;

    public AddresschangeElement() {
    }

    public AddresschangeElement(
           java.lang.String pArea,
           java.lang.String pSubscrtype,
           java.lang.String pSubno,
           java.lang.String pUsername,
           java.lang.String pServices,
           java.lang.String pPoBox,
           java.lang.String pPostArea,
           java.lang.String pZipCode,
           java.lang.String pAddrsArea,
           java.lang.String pAddrsBlock,
           java.lang.String pStreet,
           java.lang.String pBuilding,
           java.lang.String pApartment,
           java.lang.String pHlrProfile,
           java.lang.String pSndcmd,
           java.lang.String pAdditonalparams) {
           this.pArea = pArea;
           this.pSubscrtype = pSubscrtype;
           this.pSubno = pSubno;
           this.pUsername = pUsername;
           this.pServices = pServices;
           this.pPoBox = pPoBox;
           this.pPostArea = pPostArea;
           this.pZipCode = pZipCode;
           this.pAddrsArea = pAddrsArea;
           this.pAddrsBlock = pAddrsBlock;
           this.pStreet = pStreet;
           this.pBuilding = pBuilding;
           this.pApartment = pApartment;
           this.pHlrProfile = pHlrProfile;
           this.pSndcmd = pSndcmd;
           this.pAdditonalparams = pAdditonalparams;
    }


    /**
     * Gets the pArea value for this AddresschangeElement.
     * 
     * @return pArea
     */
    public java.lang.String getPArea() {
        return pArea;
    }


    /**
     * Sets the pArea value for this AddresschangeElement.
     * 
     * @param pArea
     */
    public void setPArea(java.lang.String pArea) {
        this.pArea = pArea;
    }


    /**
     * Gets the pSubscrtype value for this AddresschangeElement.
     * 
     * @return pSubscrtype
     */
    public java.lang.String getPSubscrtype() {
        return pSubscrtype;
    }


    /**
     * Sets the pSubscrtype value for this AddresschangeElement.
     * 
     * @param pSubscrtype
     */
    public void setPSubscrtype(java.lang.String pSubscrtype) {
        this.pSubscrtype = pSubscrtype;
    }


    /**
     * Gets the pSubno value for this AddresschangeElement.
     * 
     * @return pSubno
     */
    public java.lang.String getPSubno() {
        return pSubno;
    }


    /**
     * Sets the pSubno value for this AddresschangeElement.
     * 
     * @param pSubno
     */
    public void setPSubno(java.lang.String pSubno) {
        this.pSubno = pSubno;
    }


    /**
     * Gets the pUsername value for this AddresschangeElement.
     * 
     * @return pUsername
     */
    public java.lang.String getPUsername() {
        return pUsername;
    }


    /**
     * Sets the pUsername value for this AddresschangeElement.
     * 
     * @param pUsername
     */
    public void setPUsername(java.lang.String pUsername) {
        this.pUsername = pUsername;
    }


    /**
     * Gets the pServices value for this AddresschangeElement.
     * 
     * @return pServices
     */
    public java.lang.String getPServices() {
        return pServices;
    }


    /**
     * Sets the pServices value for this AddresschangeElement.
     * 
     * @param pServices
     */
    public void setPServices(java.lang.String pServices) {
        this.pServices = pServices;
    }


    /**
     * Gets the pPoBox value for this AddresschangeElement.
     * 
     * @return pPoBox
     */
    public java.lang.String getPPoBox() {
        return pPoBox;
    }


    /**
     * Sets the pPoBox value for this AddresschangeElement.
     * 
     * @param pPoBox
     */
    public void setPPoBox(java.lang.String pPoBox) {
        this.pPoBox = pPoBox;
    }


    /**
     * Gets the pPostArea value for this AddresschangeElement.
     * 
     * @return pPostArea
     */
    public java.lang.String getPPostArea() {
        return pPostArea;
    }


    /**
     * Sets the pPostArea value for this AddresschangeElement.
     * 
     * @param pPostArea
     */
    public void setPPostArea(java.lang.String pPostArea) {
        this.pPostArea = pPostArea;
    }


    /**
     * Gets the pZipCode value for this AddresschangeElement.
     * 
     * @return pZipCode
     */
    public java.lang.String getPZipCode() {
        return pZipCode;
    }


    /**
     * Sets the pZipCode value for this AddresschangeElement.
     * 
     * @param pZipCode
     */
    public void setPZipCode(java.lang.String pZipCode) {
        this.pZipCode = pZipCode;
    }


    /**
     * Gets the pAddrsArea value for this AddresschangeElement.
     * 
     * @return pAddrsArea
     */
    public java.lang.String getPAddrsArea() {
        return pAddrsArea;
    }


    /**
     * Sets the pAddrsArea value for this AddresschangeElement.
     * 
     * @param pAddrsArea
     */
    public void setPAddrsArea(java.lang.String pAddrsArea) {
        this.pAddrsArea = pAddrsArea;
    }


    /**
     * Gets the pAddrsBlock value for this AddresschangeElement.
     * 
     * @return pAddrsBlock
     */
    public java.lang.String getPAddrsBlock() {
        return pAddrsBlock;
    }


    /**
     * Sets the pAddrsBlock value for this AddresschangeElement.
     * 
     * @param pAddrsBlock
     */
    public void setPAddrsBlock(java.lang.String pAddrsBlock) {
        this.pAddrsBlock = pAddrsBlock;
    }


    /**
     * Gets the pStreet value for this AddresschangeElement.
     * 
     * @return pStreet
     */
    public java.lang.String getPStreet() {
        return pStreet;
    }


    /**
     * Sets the pStreet value for this AddresschangeElement.
     * 
     * @param pStreet
     */
    public void setPStreet(java.lang.String pStreet) {
        this.pStreet = pStreet;
    }


    /**
     * Gets the pBuilding value for this AddresschangeElement.
     * 
     * @return pBuilding
     */
    public java.lang.String getPBuilding() {
        return pBuilding;
    }


    /**
     * Sets the pBuilding value for this AddresschangeElement.
     * 
     * @param pBuilding
     */
    public void setPBuilding(java.lang.String pBuilding) {
        this.pBuilding = pBuilding;
    }


    /**
     * Gets the pApartment value for this AddresschangeElement.
     * 
     * @return pApartment
     */
    public java.lang.String getPApartment() {
        return pApartment;
    }


    /**
     * Sets the pApartment value for this AddresschangeElement.
     * 
     * @param pApartment
     */
    public void setPApartment(java.lang.String pApartment) {
        this.pApartment = pApartment;
    }


    /**
     * Gets the pHlrProfile value for this AddresschangeElement.
     * 
     * @return pHlrProfile
     */
    public java.lang.String getPHlrProfile() {
        return pHlrProfile;
    }


    /**
     * Sets the pHlrProfile value for this AddresschangeElement.
     * 
     * @param pHlrProfile
     */
    public void setPHlrProfile(java.lang.String pHlrProfile) {
        this.pHlrProfile = pHlrProfile;
    }


    /**
     * Gets the pSndcmd value for this AddresschangeElement.
     * 
     * @return pSndcmd
     */
    public java.lang.String getPSndcmd() {
        return pSndcmd;
    }


    /**
     * Sets the pSndcmd value for this AddresschangeElement.
     * 
     * @param pSndcmd
     */
    public void setPSndcmd(java.lang.String pSndcmd) {
        this.pSndcmd = pSndcmd;
    }


    /**
     * Gets the pAdditonalparams value for this AddresschangeElement.
     * 
     * @return pAdditonalparams
     */
    public java.lang.String getPAdditonalparams() {
        return pAdditonalparams;
    }


    /**
     * Sets the pAdditonalparams value for this AddresschangeElement.
     * 
     * @param pAdditonalparams
     */
    public void setPAdditonalparams(java.lang.String pAdditonalparams) {
        this.pAdditonalparams = pAdditonalparams;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AddresschangeElement)) return false;
        AddresschangeElement other = (AddresschangeElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.pArea==null && other.getPArea()==null) || 
             (this.pArea!=null &&
              this.pArea.equals(other.getPArea()))) &&
            ((this.pSubscrtype==null && other.getPSubscrtype()==null) || 
             (this.pSubscrtype!=null &&
              this.pSubscrtype.equals(other.getPSubscrtype()))) &&
            ((this.pSubno==null && other.getPSubno()==null) || 
             (this.pSubno!=null &&
              this.pSubno.equals(other.getPSubno()))) &&
            ((this.pUsername==null && other.getPUsername()==null) || 
             (this.pUsername!=null &&
              this.pUsername.equals(other.getPUsername()))) &&
            ((this.pServices==null && other.getPServices()==null) || 
             (this.pServices!=null &&
              this.pServices.equals(other.getPServices()))) &&
            ((this.pPoBox==null && other.getPPoBox()==null) || 
             (this.pPoBox!=null &&
              this.pPoBox.equals(other.getPPoBox()))) &&
            ((this.pPostArea==null && other.getPPostArea()==null) || 
             (this.pPostArea!=null &&
              this.pPostArea.equals(other.getPPostArea()))) &&
            ((this.pZipCode==null && other.getPZipCode()==null) || 
             (this.pZipCode!=null &&
              this.pZipCode.equals(other.getPZipCode()))) &&
            ((this.pAddrsArea==null && other.getPAddrsArea()==null) || 
             (this.pAddrsArea!=null &&
              this.pAddrsArea.equals(other.getPAddrsArea()))) &&
            ((this.pAddrsBlock==null && other.getPAddrsBlock()==null) || 
             (this.pAddrsBlock!=null &&
              this.pAddrsBlock.equals(other.getPAddrsBlock()))) &&
            ((this.pStreet==null && other.getPStreet()==null) || 
             (this.pStreet!=null &&
              this.pStreet.equals(other.getPStreet()))) &&
            ((this.pBuilding==null && other.getPBuilding()==null) || 
             (this.pBuilding!=null &&
              this.pBuilding.equals(other.getPBuilding()))) &&
            ((this.pApartment==null && other.getPApartment()==null) || 
             (this.pApartment!=null &&
              this.pApartment.equals(other.getPApartment()))) &&
            ((this.pHlrProfile==null && other.getPHlrProfile()==null) || 
             (this.pHlrProfile!=null &&
              this.pHlrProfile.equals(other.getPHlrProfile()))) &&
            ((this.pSndcmd==null && other.getPSndcmd()==null) || 
             (this.pSndcmd!=null &&
              this.pSndcmd.equals(other.getPSndcmd()))) &&
            ((this.pAdditonalparams==null && other.getPAdditonalparams()==null) || 
             (this.pAdditonalparams!=null &&
              this.pAdditonalparams.equals(other.getPAdditonalparams())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPArea() != null) {
            _hashCode += getPArea().hashCode();
        }
        if (getPSubscrtype() != null) {
            _hashCode += getPSubscrtype().hashCode();
        }
        if (getPSubno() != null) {
            _hashCode += getPSubno().hashCode();
        }
        if (getPUsername() != null) {
            _hashCode += getPUsername().hashCode();
        }
        if (getPServices() != null) {
            _hashCode += getPServices().hashCode();
        }
        if (getPPoBox() != null) {
            _hashCode += getPPoBox().hashCode();
        }
        if (getPPostArea() != null) {
            _hashCode += getPPostArea().hashCode();
        }
        if (getPZipCode() != null) {
            _hashCode += getPZipCode().hashCode();
        }
        if (getPAddrsArea() != null) {
            _hashCode += getPAddrsArea().hashCode();
        }
        if (getPAddrsBlock() != null) {
            _hashCode += getPAddrsBlock().hashCode();
        }
        if (getPStreet() != null) {
            _hashCode += getPStreet().hashCode();
        }
        if (getPBuilding() != null) {
            _hashCode += getPBuilding().hashCode();
        }
        if (getPApartment() != null) {
            _hashCode += getPApartment().hashCode();
        }
        if (getPHlrProfile() != null) {
            _hashCode += getPHlrProfile().hashCode();
        }
        if (getPSndcmd() != null) {
            _hashCode += getPSndcmd().hashCode();
        }
        if (getPAdditonalparams() != null) {
            _hashCode += getPAdditonalparams().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddresschangeElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">addresschangeElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PArea");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pArea"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PSubscrtype");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pSubscrtype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PSubno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pSubno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PUsername");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pUsername"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PServices");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pServices"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PPoBox");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pPoBox"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PPostArea");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pPostArea"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PZipCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pZipCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PAddrsArea");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pAddrsArea"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PAddrsBlock");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pAddrsBlock"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PStreet");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pStreet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PBuilding");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pBuilding"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PApartment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pApartment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PHlrProfile");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pHlrProfile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PSndcmd");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pSndcmd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PAdditonalparams");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pAdditonalparams"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
