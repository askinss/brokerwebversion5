/**
 * OrderManagement_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl;

public interface OrderManagement_PortType extends java.rmi.Remote {
    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.AddresschangeResponseElement addresschange(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.AddresschangeElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronadminrequestResponseElement baronadminrequest(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronadminrequestElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronsubscriberrequestResponseElement baronsubscriberrequest(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronsubscriberrequestElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangemsisdnResponseElement changemsisdn(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangemsisdnElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangepaymentresposibilityResponseElement changepaymentresposibility(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangepaymentresposibilityElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeprepostpaidResponseElement changeprepostpaid(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeprepostpaidElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeserviceResponseElement changeservice(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeserviceElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangesimcardResponseElement changesimcard(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangesimcardElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.CreatenewsubscriberResponseElement createnewsubscriber(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.CreatenewsubscriberElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonadminrequestResponseElement disconnectonadminrequest(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonadminrequestElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonsubscriberrequestResponseElement disconnectonsubscriberrequest(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonsubscriberrequestElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.HandlemultiorderResponseElement handlemultiorder(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.HandlemultiorderElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.NamechangeResponseElement namechange(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.NamechangeElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.Portpager2GsmResponseElement portpager2Gsm(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.Portpager2GsmElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectbarResponseElement reconnectbar(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectbarElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonadminrequestResponseElement reconnectonadminrequest(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonadminrequestElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonsubscriberrequestResponseElement reconnectonsubscriberrequest(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonsubscriberrequestElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReinstallsubscriberResponseElement reinstallsubscriber(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReinstallsubscriberElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TerminateonsubscriberrequestResponseElement terminateonsubscriberrequest(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TerminateonsubscriberrequestElement parameters) throws java.rmi.RemoteException;
    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TransferownershipResponseElement transferownership(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TransferownershipElement parameters) throws java.rmi.RemoteException;
}
