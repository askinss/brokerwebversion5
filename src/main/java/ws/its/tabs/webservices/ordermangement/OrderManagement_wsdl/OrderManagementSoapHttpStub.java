/**
 * OrderManagementSoapHttpStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl;

public class OrderManagementSoapHttpStub extends org.apache.axis.client.Stub implements ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.OrderManagement_PortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[20];
        _initOperationDesc1();
        _initOperationDesc2();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("addresschange");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "addresschangeElement"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">addresschangeElement"), ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.AddresschangeElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">addresschangeResponseElement"));
        oper.setReturnClass(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.AddresschangeResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "addresschangeResponseElement"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("baronadminrequest");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "baronadminrequestElement"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">baronadminrequestElement"), ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronadminrequestElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">baronadminrequestResponseElement"));
        oper.setReturnClass(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronadminrequestResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "baronadminrequestResponseElement"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("baronsubscriberrequest");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "baronsubscriberrequestElement"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">baronsubscriberrequestElement"), ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronsubscriberrequestElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">baronsubscriberrequestResponseElement"));
        oper.setReturnClass(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronsubscriberrequestResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "baronsubscriberrequestResponseElement"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("changemsisdn");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "changemsisdnElement"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">changemsisdnElement"), ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangemsisdnElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">changemsisdnResponseElement"));
        oper.setReturnClass(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangemsisdnResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "changemsisdnResponseElement"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("changepaymentresposibility");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "changepaymentresposibilityElement"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">changepaymentresposibilityElement"), ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangepaymentresposibilityElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">changepaymentresposibilityResponseElement"));
        oper.setReturnClass(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangepaymentresposibilityResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "changepaymentresposibilityResponseElement"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("changeprepostpaid");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "changeprepostpaidElement"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">changeprepostpaidElement"), ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeprepostpaidElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">changeprepostpaidResponseElement"));
        oper.setReturnClass(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeprepostpaidResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "changeprepostpaidResponseElement"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("changeservice");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "changeserviceElement"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">changeserviceElement"), ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeserviceElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">changeserviceResponseElement"));
        oper.setReturnClass(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeserviceResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "changeserviceResponseElement"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("changesimcard");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "changesimcardElement"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">changesimcardElement"), ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangesimcardElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">changesimcardResponseElement"));
        oper.setReturnClass(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangesimcardResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "changesimcardResponseElement"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("createnewsubscriber");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "createnewsubscriberElement"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">createnewsubscriberElement"), ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.CreatenewsubscriberElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">createnewsubscriberResponseElement"));
        oper.setReturnClass(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.CreatenewsubscriberResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "createnewsubscriberResponseElement"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("disconnectonadminrequest");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "disconnectonadminrequestElement"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">disconnectonadminrequestElement"), ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonadminrequestElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">disconnectonadminrequestResponseElement"));
        oper.setReturnClass(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonadminrequestResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "disconnectonadminrequestResponseElement"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("disconnectonsubscriberrequest");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "disconnectonsubscriberrequestElement"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">disconnectonsubscriberrequestElement"), ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonsubscriberrequestElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">disconnectonsubscriberrequestResponseElement"));
        oper.setReturnClass(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonsubscriberrequestResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "disconnectonsubscriberrequestResponseElement"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("handlemultiorder");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "handlemultiorderElement"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">handlemultiorderElement"), ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.HandlemultiorderElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">handlemultiorderResponseElement"));
        oper.setReturnClass(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.HandlemultiorderResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "handlemultiorderResponseElement"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("namechange");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "namechangeElement"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">namechangeElement"), ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.NamechangeElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">namechangeResponseElement"));
        oper.setReturnClass(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.NamechangeResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "namechangeResponseElement"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("portpager2gsm");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "portpager2gsmElement"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">portpager2gsmElement"), ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.Portpager2GsmElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">portpager2gsmResponseElement"));
        oper.setReturnClass(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.Portpager2GsmResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "portpager2gsmResponseElement"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("reconnectbar");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "reconnectbarElement"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">reconnectbarElement"), ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectbarElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">reconnectbarResponseElement"));
        oper.setReturnClass(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectbarResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "reconnectbarResponseElement"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("reconnectonadminrequest");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "reconnectonadminrequestElement"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">reconnectonadminrequestElement"), ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonadminrequestElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">reconnectonadminrequestResponseElement"));
        oper.setReturnClass(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonadminrequestResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "reconnectonadminrequestResponseElement"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("reconnectonsubscriberrequest");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "reconnectonsubscriberrequestElement"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">reconnectonsubscriberrequestElement"), ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonsubscriberrequestElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">reconnectonsubscriberrequestResponseElement"));
        oper.setReturnClass(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonsubscriberrequestResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "reconnectonsubscriberrequestResponseElement"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("reinstallsubscriber");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "reinstallsubscriberElement"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">reinstallsubscriberElement"), ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReinstallsubscriberElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">reinstallsubscriberResponseElement"));
        oper.setReturnClass(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReinstallsubscriberResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "reinstallsubscriberResponseElement"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("terminateonsubscriberrequest");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "terminateonsubscriberrequestElement"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">terminateonsubscriberrequestElement"), ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TerminateonsubscriberrequestElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">terminateonsubscriberrequestResponseElement"));
        oper.setReturnClass(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TerminateonsubscriberrequestResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "terminateonsubscriberrequestResponseElement"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("transferownership");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "transferownershipElement"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">transferownershipElement"), ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TransferownershipElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">transferownershipResponseElement"));
        oper.setReturnClass(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TransferownershipResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "transferownershipResponseElement"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[19] = oper;

    }

    public OrderManagementSoapHttpStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public OrderManagementSoapHttpStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public OrderManagementSoapHttpStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">addresschangeElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.AddresschangeElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">addresschangeResponseElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.AddresschangeResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">baronadminrequestElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronadminrequestElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">baronadminrequestResponseElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronadminrequestResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">baronsubscriberrequestElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronsubscriberrequestElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">baronsubscriberrequestResponseElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronsubscriberrequestResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">changemsisdnElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangemsisdnElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">changemsisdnResponseElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangemsisdnResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">changepaymentresposibilityElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangepaymentresposibilityElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">changepaymentresposibilityResponseElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangepaymentresposibilityResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">changeprepostpaidElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeprepostpaidElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">changeprepostpaidResponseElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeprepostpaidResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">changeserviceElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeserviceElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">changeserviceResponseElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeserviceResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">changesimcardElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangesimcardElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">changesimcardResponseElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangesimcardResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">createnewsubscriberElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.CreatenewsubscriberElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">createnewsubscriberResponseElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.CreatenewsubscriberResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">disconnectonadminrequestElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonadminrequestElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">disconnectonadminrequestResponseElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonadminrequestResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">disconnectonsubscriberrequestElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonsubscriberrequestElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">disconnectonsubscriberrequestResponseElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonsubscriberrequestResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">handlemultiorderElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.HandlemultiorderElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">handlemultiorderResponseElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.HandlemultiorderResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">namechangeElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.NamechangeElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">namechangeResponseElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.NamechangeResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">portpager2gsmElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.Portpager2GsmElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">portpager2gsmResponseElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.Portpager2GsmResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">reconnectbarElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectbarElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">reconnectbarResponseElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectbarResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">reconnectonadminrequestElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonadminrequestElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">reconnectonadminrequestResponseElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonadminrequestResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">reconnectonsubscriberrequestElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonsubscriberrequestElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">reconnectonsubscriberrequestResponseElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonsubscriberrequestResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">reinstallsubscriberElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReinstallsubscriberElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">reinstallsubscriberResponseElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReinstallsubscriberResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">terminateonsubscriberrequestElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TerminateonsubscriberrequestElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">terminateonsubscriberrequestResponseElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TerminateonsubscriberrequestResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">transferownershipElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TransferownershipElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">transferownershipResponseElement");
            cachedSerQNames.add(qName);
            cls = ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TransferownershipResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.AddresschangeResponseElement addresschange(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.AddresschangeElement parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/addresschange");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "addresschange"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.AddresschangeResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.AddresschangeResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.AddresschangeResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronadminrequestResponseElement baronadminrequest(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronadminrequestElement parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/baronadminrequest");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "baronadminrequest"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronadminrequestResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronadminrequestResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronadminrequestResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronsubscriberrequestResponseElement baronsubscriberrequest(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronsubscriberrequestElement parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/baronsubscriberrequest");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "baronsubscriberrequest"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronsubscriberrequestResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronsubscriberrequestResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronsubscriberrequestResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangemsisdnResponseElement changemsisdn(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangemsisdnElement parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/changemsisdn");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "changemsisdn"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangemsisdnResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangemsisdnResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangemsisdnResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangepaymentresposibilityResponseElement changepaymentresposibility(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangepaymentresposibilityElement parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/changepaymentresposibility");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "changepaymentresposibility"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangepaymentresposibilityResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangepaymentresposibilityResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangepaymentresposibilityResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeprepostpaidResponseElement changeprepostpaid(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeprepostpaidElement parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/changeprepostpaid");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "changeprepostpaid"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeprepostpaidResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeprepostpaidResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeprepostpaidResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeserviceResponseElement changeservice(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeserviceElement parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/changeservice");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "changeservice"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeserviceResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeserviceResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeserviceResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangesimcardResponseElement changesimcard(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangesimcardElement parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/changesimcard");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "changesimcard"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangesimcardResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangesimcardResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangesimcardResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.CreatenewsubscriberResponseElement createnewsubscriber(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.CreatenewsubscriberElement parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/createnewsubscriber");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "createnewsubscriber"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.CreatenewsubscriberResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.CreatenewsubscriberResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.CreatenewsubscriberResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonadminrequestResponseElement disconnectonadminrequest(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonadminrequestElement parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/disconnectonadminrequest");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "disconnectonadminrequest"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonadminrequestResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonadminrequestResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonadminrequestResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonsubscriberrequestResponseElement disconnectonsubscriberrequest(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonsubscriberrequestElement parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/disconnectonsubscriberrequest");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "disconnectonsubscriberrequest"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonsubscriberrequestResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonsubscriberrequestResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonsubscriberrequestResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.HandlemultiorderResponseElement handlemultiorder(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.HandlemultiorderElement parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/handlemultiorder");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "handlemultiorder"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.HandlemultiorderResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.HandlemultiorderResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.HandlemultiorderResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.NamechangeResponseElement namechange(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.NamechangeElement parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/namechange");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "namechange"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.NamechangeResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.NamechangeResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.NamechangeResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.Portpager2GsmResponseElement portpager2Gsm(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.Portpager2GsmElement parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/portpager2gsm");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "portpager2gsm"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.Portpager2GsmResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.Portpager2GsmResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.Portpager2GsmResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectbarResponseElement reconnectbar(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectbarElement parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/reconnectbar");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "reconnectbar"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectbarResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectbarResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectbarResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonadminrequestResponseElement reconnectonadminrequest(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonadminrequestElement parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/reconnectonadminrequest");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "reconnectonadminrequest"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonadminrequestResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonadminrequestResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonadminrequestResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonsubscriberrequestResponseElement reconnectonsubscriberrequest(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonsubscriberrequestElement parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/reconnectonsubscriberrequest");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "reconnectonsubscriberrequest"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonsubscriberrequestResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonsubscriberrequestResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonsubscriberrequestResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReinstallsubscriberResponseElement reinstallsubscriber(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReinstallsubscriberElement parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/reinstallsubscriber");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "reinstallsubscriber"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReinstallsubscriberResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReinstallsubscriberResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReinstallsubscriberResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TerminateonsubscriberrequestResponseElement terminateonsubscriberrequest(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TerminateonsubscriberrequestElement parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/terminateonsubscriberrequest");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "terminateonsubscriberrequest"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TerminateonsubscriberrequestResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TerminateonsubscriberrequestResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TerminateonsubscriberrequestResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TransferownershipResponseElement transferownership(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TransferownershipElement parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/transferownership");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "transferownership"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TransferownershipResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TransferownershipResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TransferownershipResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
