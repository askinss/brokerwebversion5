/**
 * OrderManagement_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl;

public interface OrderManagement_Service extends javax.xml.rpc.Service {
    public java.lang.String getOrderManagementSoapHttpPortAddress();

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.OrderManagement_PortType getOrderManagementSoapHttpPort() throws javax.xml.rpc.ServiceException;

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.OrderManagement_PortType getOrderManagementSoapHttpPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
