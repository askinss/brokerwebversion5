/**
 * NamechangeElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types;

public class NamechangeElement  implements java.io.Serializable {
    private java.lang.String pArea;

    private java.lang.String pSubscrtype;

    private java.lang.String pSubno;

    private java.lang.String pUsername;

    private java.lang.String pServices;

    private java.lang.String pTitle;

    private java.lang.String pFirstname;

    private java.lang.String pBin;

    private java.lang.String pBinbint;

    private java.lang.String pSecondname;

    private java.lang.String pThirdname;

    private java.lang.String pCname;

    private java.lang.String pLastname;

    private java.lang.String pHlrProfile;

    private java.lang.String pSndcmd;

    private java.lang.String pAdditonalparams;

    public NamechangeElement() {
    }

    public NamechangeElement(
           java.lang.String pArea,
           java.lang.String pSubscrtype,
           java.lang.String pSubno,
           java.lang.String pUsername,
           java.lang.String pServices,
           java.lang.String pTitle,
           java.lang.String pFirstname,
           java.lang.String pBin,
           java.lang.String pBinbint,
           java.lang.String pSecondname,
           java.lang.String pThirdname,
           java.lang.String pCname,
           java.lang.String pLastname,
           java.lang.String pHlrProfile,
           java.lang.String pSndcmd,
           java.lang.String pAdditonalparams) {
           this.pArea = pArea;
           this.pSubscrtype = pSubscrtype;
           this.pSubno = pSubno;
           this.pUsername = pUsername;
           this.pServices = pServices;
           this.pTitle = pTitle;
           this.pFirstname = pFirstname;
           this.pBin = pBin;
           this.pBinbint = pBinbint;
           this.pSecondname = pSecondname;
           this.pThirdname = pThirdname;
           this.pCname = pCname;
           this.pLastname = pLastname;
           this.pHlrProfile = pHlrProfile;
           this.pSndcmd = pSndcmd;
           this.pAdditonalparams = pAdditonalparams;
    }


    /**
     * Gets the pArea value for this NamechangeElement.
     * 
     * @return pArea
     */
    public java.lang.String getPArea() {
        return pArea;
    }


    /**
     * Sets the pArea value for this NamechangeElement.
     * 
     * @param pArea
     */
    public void setPArea(java.lang.String pArea) {
        this.pArea = pArea;
    }


    /**
     * Gets the pSubscrtype value for this NamechangeElement.
     * 
     * @return pSubscrtype
     */
    public java.lang.String getPSubscrtype() {
        return pSubscrtype;
    }


    /**
     * Sets the pSubscrtype value for this NamechangeElement.
     * 
     * @param pSubscrtype
     */
    public void setPSubscrtype(java.lang.String pSubscrtype) {
        this.pSubscrtype = pSubscrtype;
    }


    /**
     * Gets the pSubno value for this NamechangeElement.
     * 
     * @return pSubno
     */
    public java.lang.String getPSubno() {
        return pSubno;
    }


    /**
     * Sets the pSubno value for this NamechangeElement.
     * 
     * @param pSubno
     */
    public void setPSubno(java.lang.String pSubno) {
        this.pSubno = pSubno;
    }


    /**
     * Gets the pUsername value for this NamechangeElement.
     * 
     * @return pUsername
     */
    public java.lang.String getPUsername() {
        return pUsername;
    }


    /**
     * Sets the pUsername value for this NamechangeElement.
     * 
     * @param pUsername
     */
    public void setPUsername(java.lang.String pUsername) {
        this.pUsername = pUsername;
    }


    /**
     * Gets the pServices value for this NamechangeElement.
     * 
     * @return pServices
     */
    public java.lang.String getPServices() {
        return pServices;
    }


    /**
     * Sets the pServices value for this NamechangeElement.
     * 
     * @param pServices
     */
    public void setPServices(java.lang.String pServices) {
        this.pServices = pServices;
    }


    /**
     * Gets the pTitle value for this NamechangeElement.
     * 
     * @return pTitle
     */
    public java.lang.String getPTitle() {
        return pTitle;
    }


    /**
     * Sets the pTitle value for this NamechangeElement.
     * 
     * @param pTitle
     */
    public void setPTitle(java.lang.String pTitle) {
        this.pTitle = pTitle;
    }


    /**
     * Gets the pFirstname value for this NamechangeElement.
     * 
     * @return pFirstname
     */
    public java.lang.String getPFirstname() {
        return pFirstname;
    }


    /**
     * Sets the pFirstname value for this NamechangeElement.
     * 
     * @param pFirstname
     */
    public void setPFirstname(java.lang.String pFirstname) {
        this.pFirstname = pFirstname;
    }


    /**
     * Gets the pBin value for this NamechangeElement.
     * 
     * @return pBin
     */
    public java.lang.String getPBin() {
        return pBin;
    }


    /**
     * Sets the pBin value for this NamechangeElement.
     * 
     * @param pBin
     */
    public void setPBin(java.lang.String pBin) {
        this.pBin = pBin;
    }


    /**
     * Gets the pBinbint value for this NamechangeElement.
     * 
     * @return pBinbint
     */
    public java.lang.String getPBinbint() {
        return pBinbint;
    }


    /**
     * Sets the pBinbint value for this NamechangeElement.
     * 
     * @param pBinbint
     */
    public void setPBinbint(java.lang.String pBinbint) {
        this.pBinbint = pBinbint;
    }


    /**
     * Gets the pSecondname value for this NamechangeElement.
     * 
     * @return pSecondname
     */
    public java.lang.String getPSecondname() {
        return pSecondname;
    }


    /**
     * Sets the pSecondname value for this NamechangeElement.
     * 
     * @param pSecondname
     */
    public void setPSecondname(java.lang.String pSecondname) {
        this.pSecondname = pSecondname;
    }


    /**
     * Gets the pThirdname value for this NamechangeElement.
     * 
     * @return pThirdname
     */
    public java.lang.String getPThirdname() {
        return pThirdname;
    }


    /**
     * Sets the pThirdname value for this NamechangeElement.
     * 
     * @param pThirdname
     */
    public void setPThirdname(java.lang.String pThirdname) {
        this.pThirdname = pThirdname;
    }


    /**
     * Gets the pCname value for this NamechangeElement.
     * 
     * @return pCname
     */
    public java.lang.String getPCname() {
        return pCname;
    }


    /**
     * Sets the pCname value for this NamechangeElement.
     * 
     * @param pCname
     */
    public void setPCname(java.lang.String pCname) {
        this.pCname = pCname;
    }


    /**
     * Gets the pLastname value for this NamechangeElement.
     * 
     * @return pLastname
     */
    public java.lang.String getPLastname() {
        return pLastname;
    }


    /**
     * Sets the pLastname value for this NamechangeElement.
     * 
     * @param pLastname
     */
    public void setPLastname(java.lang.String pLastname) {
        this.pLastname = pLastname;
    }


    /**
     * Gets the pHlrProfile value for this NamechangeElement.
     * 
     * @return pHlrProfile
     */
    public java.lang.String getPHlrProfile() {
        return pHlrProfile;
    }


    /**
     * Sets the pHlrProfile value for this NamechangeElement.
     * 
     * @param pHlrProfile
     */
    public void setPHlrProfile(java.lang.String pHlrProfile) {
        this.pHlrProfile = pHlrProfile;
    }


    /**
     * Gets the pSndcmd value for this NamechangeElement.
     * 
     * @return pSndcmd
     */
    public java.lang.String getPSndcmd() {
        return pSndcmd;
    }


    /**
     * Sets the pSndcmd value for this NamechangeElement.
     * 
     * @param pSndcmd
     */
    public void setPSndcmd(java.lang.String pSndcmd) {
        this.pSndcmd = pSndcmd;
    }


    /**
     * Gets the pAdditonalparams value for this NamechangeElement.
     * 
     * @return pAdditonalparams
     */
    public java.lang.String getPAdditonalparams() {
        return pAdditonalparams;
    }


    /**
     * Sets the pAdditonalparams value for this NamechangeElement.
     * 
     * @param pAdditonalparams
     */
    public void setPAdditonalparams(java.lang.String pAdditonalparams) {
        this.pAdditonalparams = pAdditonalparams;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof NamechangeElement)) return false;
        NamechangeElement other = (NamechangeElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.pArea==null && other.getPArea()==null) || 
             (this.pArea!=null &&
              this.pArea.equals(other.getPArea()))) &&
            ((this.pSubscrtype==null && other.getPSubscrtype()==null) || 
             (this.pSubscrtype!=null &&
              this.pSubscrtype.equals(other.getPSubscrtype()))) &&
            ((this.pSubno==null && other.getPSubno()==null) || 
             (this.pSubno!=null &&
              this.pSubno.equals(other.getPSubno()))) &&
            ((this.pUsername==null && other.getPUsername()==null) || 
             (this.pUsername!=null &&
              this.pUsername.equals(other.getPUsername()))) &&
            ((this.pServices==null && other.getPServices()==null) || 
             (this.pServices!=null &&
              this.pServices.equals(other.getPServices()))) &&
            ((this.pTitle==null && other.getPTitle()==null) || 
             (this.pTitle!=null &&
              this.pTitle.equals(other.getPTitle()))) &&
            ((this.pFirstname==null && other.getPFirstname()==null) || 
             (this.pFirstname!=null &&
              this.pFirstname.equals(other.getPFirstname()))) &&
            ((this.pBin==null && other.getPBin()==null) || 
             (this.pBin!=null &&
              this.pBin.equals(other.getPBin()))) &&
            ((this.pBinbint==null && other.getPBinbint()==null) || 
             (this.pBinbint!=null &&
              this.pBinbint.equals(other.getPBinbint()))) &&
            ((this.pSecondname==null && other.getPSecondname()==null) || 
             (this.pSecondname!=null &&
              this.pSecondname.equals(other.getPSecondname()))) &&
            ((this.pThirdname==null && other.getPThirdname()==null) || 
             (this.pThirdname!=null &&
              this.pThirdname.equals(other.getPThirdname()))) &&
            ((this.pCname==null && other.getPCname()==null) || 
             (this.pCname!=null &&
              this.pCname.equals(other.getPCname()))) &&
            ((this.pLastname==null && other.getPLastname()==null) || 
             (this.pLastname!=null &&
              this.pLastname.equals(other.getPLastname()))) &&
            ((this.pHlrProfile==null && other.getPHlrProfile()==null) || 
             (this.pHlrProfile!=null &&
              this.pHlrProfile.equals(other.getPHlrProfile()))) &&
            ((this.pSndcmd==null && other.getPSndcmd()==null) || 
             (this.pSndcmd!=null &&
              this.pSndcmd.equals(other.getPSndcmd()))) &&
            ((this.pAdditonalparams==null && other.getPAdditonalparams()==null) || 
             (this.pAdditonalparams!=null &&
              this.pAdditonalparams.equals(other.getPAdditonalparams())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPArea() != null) {
            _hashCode += getPArea().hashCode();
        }
        if (getPSubscrtype() != null) {
            _hashCode += getPSubscrtype().hashCode();
        }
        if (getPSubno() != null) {
            _hashCode += getPSubno().hashCode();
        }
        if (getPUsername() != null) {
            _hashCode += getPUsername().hashCode();
        }
        if (getPServices() != null) {
            _hashCode += getPServices().hashCode();
        }
        if (getPTitle() != null) {
            _hashCode += getPTitle().hashCode();
        }
        if (getPFirstname() != null) {
            _hashCode += getPFirstname().hashCode();
        }
        if (getPBin() != null) {
            _hashCode += getPBin().hashCode();
        }
        if (getPBinbint() != null) {
            _hashCode += getPBinbint().hashCode();
        }
        if (getPSecondname() != null) {
            _hashCode += getPSecondname().hashCode();
        }
        if (getPThirdname() != null) {
            _hashCode += getPThirdname().hashCode();
        }
        if (getPCname() != null) {
            _hashCode += getPCname().hashCode();
        }
        if (getPLastname() != null) {
            _hashCode += getPLastname().hashCode();
        }
        if (getPHlrProfile() != null) {
            _hashCode += getPHlrProfile().hashCode();
        }
        if (getPSndcmd() != null) {
            _hashCode += getPSndcmd().hashCode();
        }
        if (getPAdditonalparams() != null) {
            _hashCode += getPAdditonalparams().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(NamechangeElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">namechangeElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PArea");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pArea"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PSubscrtype");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pSubscrtype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PSubno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pSubno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PUsername");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pUsername"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PServices");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pServices"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PTitle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pTitle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PFirstname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pFirstname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PBin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pBin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PBinbint");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pBinbint"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PSecondname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pSecondname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PThirdname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pThirdname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PCname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pCname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PLastname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pLastname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PHlrProfile");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pHlrProfile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PSndcmd");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pSndcmd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PAdditonalparams");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pAdditonalparams"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
