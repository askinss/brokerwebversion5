/**
 * HandlemultiorderElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types;

public class HandlemultiorderElement  implements java.io.Serializable {
    private java.lang.String pXml;

    public HandlemultiorderElement() {
    }

    public HandlemultiorderElement(
           java.lang.String pXml) {
           this.pXml = pXml;
    }


    /**
     * Gets the pXml value for this HandlemultiorderElement.
     * 
     * @return pXml
     */
    public java.lang.String getPXml() {
        return pXml;
    }


    /**
     * Sets the pXml value for this HandlemultiorderElement.
     * 
     * @param pXml
     */
    public void setPXml(java.lang.String pXml) {
        this.pXml = pXml;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HandlemultiorderElement)) return false;
        HandlemultiorderElement other = (HandlemultiorderElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.pXml==null && other.getPXml()==null) || 
             (this.pXml!=null &&
              this.pXml.equals(other.getPXml())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPXml() != null) {
            _hashCode += getPXml().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HandlemultiorderElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">handlemultiorderElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PXml");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pXml"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
