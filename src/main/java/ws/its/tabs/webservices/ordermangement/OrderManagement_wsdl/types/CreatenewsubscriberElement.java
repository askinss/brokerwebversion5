/**
 * CreatenewsubscriberElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types;

public class CreatenewsubscriberElement  implements java.io.Serializable {
    private java.lang.String pArea;

    private java.lang.String pSubscrtype;

    private java.lang.String pSubno;

    private java.lang.String pUsername;

    private java.lang.String pContract;

    private java.lang.String pServices;

    private java.lang.String pSimcard;

    private java.lang.String pCreateContrno;

    private java.lang.String pSubsidyFlag;

    private java.lang.String pPrepPost;

    private java.lang.String pTariffProfile;

    private java.lang.String pTitle;

    private java.lang.String pFirstname;

    private java.lang.String pBin;

    private java.lang.String pSecondname;

    private java.lang.String pBinbint;

    private java.lang.String pThirdname;

    private java.lang.String pLastname;

    private java.lang.String pCname;

    private java.lang.String pArtitle;

    private java.lang.String pArfirstname;

    private java.lang.String pArbin;

    private java.lang.String pArsecondname;

    private java.lang.String pArbinbint;

    private java.lang.String pArthirdname;

    private java.lang.String pArlastname;

    private java.lang.String pArcname;

    private java.lang.String pHlrProfile;

    private java.lang.String pSndcmd;

    private java.lang.String pAdditonalparams;

    public CreatenewsubscriberElement() {
    }

    public CreatenewsubscriberElement(
           java.lang.String pArea,
           java.lang.String pSubscrtype,
           java.lang.String pSubno,
           java.lang.String pUsername,
           java.lang.String pContract,
           java.lang.String pServices,
           java.lang.String pSimcard,
           java.lang.String pCreateContrno,
           java.lang.String pSubsidyFlag,
           java.lang.String pPrepPost,
           java.lang.String pTariffProfile,
           java.lang.String pTitle,
           java.lang.String pFirstname,
           java.lang.String pBin,
           java.lang.String pSecondname,
           java.lang.String pBinbint,
           java.lang.String pThirdname,
           java.lang.String pLastname,
           java.lang.String pCname,
           java.lang.String pArtitle,
           java.lang.String pArfirstname,
           java.lang.String pArbin,
           java.lang.String pArsecondname,
           java.lang.String pArbinbint,
           java.lang.String pArthirdname,
           java.lang.String pArlastname,
           java.lang.String pArcname,
           java.lang.String pHlrProfile,
           java.lang.String pSndcmd,
           java.lang.String pAdditonalparams) {
           this.pArea = pArea;
           this.pSubscrtype = pSubscrtype;
           this.pSubno = pSubno;
           this.pUsername = pUsername;
           this.pContract = pContract;
           this.pServices = pServices;
           this.pSimcard = pSimcard;
           this.pCreateContrno = pCreateContrno;
           this.pSubsidyFlag = pSubsidyFlag;
           this.pPrepPost = pPrepPost;
           this.pTariffProfile = pTariffProfile;
           this.pTitle = pTitle;
           this.pFirstname = pFirstname;
           this.pBin = pBin;
           this.pSecondname = pSecondname;
           this.pBinbint = pBinbint;
           this.pThirdname = pThirdname;
           this.pLastname = pLastname;
           this.pCname = pCname;
           this.pArtitle = pArtitle;
           this.pArfirstname = pArfirstname;
           this.pArbin = pArbin;
           this.pArsecondname = pArsecondname;
           this.pArbinbint = pArbinbint;
           this.pArthirdname = pArthirdname;
           this.pArlastname = pArlastname;
           this.pArcname = pArcname;
           this.pHlrProfile = pHlrProfile;
           this.pSndcmd = pSndcmd;
           this.pAdditonalparams = pAdditonalparams;
    }


    /**
     * Gets the pArea value for this CreatenewsubscriberElement.
     * 
     * @return pArea
     */
    public java.lang.String getPArea() {
        return pArea;
    }


    /**
     * Sets the pArea value for this CreatenewsubscriberElement.
     * 
     * @param pArea
     */
    public void setPArea(java.lang.String pArea) {
        this.pArea = pArea;
    }


    /**
     * Gets the pSubscrtype value for this CreatenewsubscriberElement.
     * 
     * @return pSubscrtype
     */
    public java.lang.String getPSubscrtype() {
        return pSubscrtype;
    }


    /**
     * Sets the pSubscrtype value for this CreatenewsubscriberElement.
     * 
     * @param pSubscrtype
     */
    public void setPSubscrtype(java.lang.String pSubscrtype) {
        this.pSubscrtype = pSubscrtype;
    }


    /**
     * Gets the pSubno value for this CreatenewsubscriberElement.
     * 
     * @return pSubno
     */
    public java.lang.String getPSubno() {
        return pSubno;
    }


    /**
     * Sets the pSubno value for this CreatenewsubscriberElement.
     * 
     * @param pSubno
     */
    public void setPSubno(java.lang.String pSubno) {
        this.pSubno = pSubno;
    }


    /**
     * Gets the pUsername value for this CreatenewsubscriberElement.
     * 
     * @return pUsername
     */
    public java.lang.String getPUsername() {
        return pUsername;
    }


    /**
     * Sets the pUsername value for this CreatenewsubscriberElement.
     * 
     * @param pUsername
     */
    public void setPUsername(java.lang.String pUsername) {
        this.pUsername = pUsername;
    }


    /**
     * Gets the pContract value for this CreatenewsubscriberElement.
     * 
     * @return pContract
     */
    public java.lang.String getPContract() {
        return pContract;
    }


    /**
     * Sets the pContract value for this CreatenewsubscriberElement.
     * 
     * @param pContract
     */
    public void setPContract(java.lang.String pContract) {
        this.pContract = pContract;
    }


    /**
     * Gets the pServices value for this CreatenewsubscriberElement.
     * 
     * @return pServices
     */
    public java.lang.String getPServices() {
        return pServices;
    }


    /**
     * Sets the pServices value for this CreatenewsubscriberElement.
     * 
     * @param pServices
     */
    public void setPServices(java.lang.String pServices) {
        this.pServices = pServices;
    }


    /**
     * Gets the pSimcard value for this CreatenewsubscriberElement.
     * 
     * @return pSimcard
     */
    public java.lang.String getPSimcard() {
        return pSimcard;
    }


    /**
     * Sets the pSimcard value for this CreatenewsubscriberElement.
     * 
     * @param pSimcard
     */
    public void setPSimcard(java.lang.String pSimcard) {
        this.pSimcard = pSimcard;
    }


    /**
     * Gets the pCreateContrno value for this CreatenewsubscriberElement.
     * 
     * @return pCreateContrno
     */
    public java.lang.String getPCreateContrno() {
        return pCreateContrno;
    }


    /**
     * Sets the pCreateContrno value for this CreatenewsubscriberElement.
     * 
     * @param pCreateContrno
     */
    public void setPCreateContrno(java.lang.String pCreateContrno) {
        this.pCreateContrno = pCreateContrno;
    }


    /**
     * Gets the pSubsidyFlag value for this CreatenewsubscriberElement.
     * 
     * @return pSubsidyFlag
     */
    public java.lang.String getPSubsidyFlag() {
        return pSubsidyFlag;
    }


    /**
     * Sets the pSubsidyFlag value for this CreatenewsubscriberElement.
     * 
     * @param pSubsidyFlag
     */
    public void setPSubsidyFlag(java.lang.String pSubsidyFlag) {
        this.pSubsidyFlag = pSubsidyFlag;
    }


    /**
     * Gets the pPrepPost value for this CreatenewsubscriberElement.
     * 
     * @return pPrepPost
     */
    public java.lang.String getPPrepPost() {
        return pPrepPost;
    }


    /**
     * Sets the pPrepPost value for this CreatenewsubscriberElement.
     * 
     * @param pPrepPost
     */
    public void setPPrepPost(java.lang.String pPrepPost) {
        this.pPrepPost = pPrepPost;
    }


    /**
     * Gets the pTariffProfile value for this CreatenewsubscriberElement.
     * 
     * @return pTariffProfile
     */
    public java.lang.String getPTariffProfile() {
        return pTariffProfile;
    }


    /**
     * Sets the pTariffProfile value for this CreatenewsubscriberElement.
     * 
     * @param pTariffProfile
     */
    public void setPTariffProfile(java.lang.String pTariffProfile) {
        this.pTariffProfile = pTariffProfile;
    }


    /**
     * Gets the pTitle value for this CreatenewsubscriberElement.
     * 
     * @return pTitle
     */
    public java.lang.String getPTitle() {
        return pTitle;
    }


    /**
     * Sets the pTitle value for this CreatenewsubscriberElement.
     * 
     * @param pTitle
     */
    public void setPTitle(java.lang.String pTitle) {
        this.pTitle = pTitle;
    }


    /**
     * Gets the pFirstname value for this CreatenewsubscriberElement.
     * 
     * @return pFirstname
     */
    public java.lang.String getPFirstname() {
        return pFirstname;
    }


    /**
     * Sets the pFirstname value for this CreatenewsubscriberElement.
     * 
     * @param pFirstname
     */
    public void setPFirstname(java.lang.String pFirstname) {
        this.pFirstname = pFirstname;
    }


    /**
     * Gets the pBin value for this CreatenewsubscriberElement.
     * 
     * @return pBin
     */
    public java.lang.String getPBin() {
        return pBin;
    }


    /**
     * Sets the pBin value for this CreatenewsubscriberElement.
     * 
     * @param pBin
     */
    public void setPBin(java.lang.String pBin) {
        this.pBin = pBin;
    }


    /**
     * Gets the pSecondname value for this CreatenewsubscriberElement.
     * 
     * @return pSecondname
     */
    public java.lang.String getPSecondname() {
        return pSecondname;
    }


    /**
     * Sets the pSecondname value for this CreatenewsubscriberElement.
     * 
     * @param pSecondname
     */
    public void setPSecondname(java.lang.String pSecondname) {
        this.pSecondname = pSecondname;
    }


    /**
     * Gets the pBinbint value for this CreatenewsubscriberElement.
     * 
     * @return pBinbint
     */
    public java.lang.String getPBinbint() {
        return pBinbint;
    }


    /**
     * Sets the pBinbint value for this CreatenewsubscriberElement.
     * 
     * @param pBinbint
     */
    public void setPBinbint(java.lang.String pBinbint) {
        this.pBinbint = pBinbint;
    }


    /**
     * Gets the pThirdname value for this CreatenewsubscriberElement.
     * 
     * @return pThirdname
     */
    public java.lang.String getPThirdname() {
        return pThirdname;
    }


    /**
     * Sets the pThirdname value for this CreatenewsubscriberElement.
     * 
     * @param pThirdname
     */
    public void setPThirdname(java.lang.String pThirdname) {
        this.pThirdname = pThirdname;
    }


    /**
     * Gets the pLastname value for this CreatenewsubscriberElement.
     * 
     * @return pLastname
     */
    public java.lang.String getPLastname() {
        return pLastname;
    }


    /**
     * Sets the pLastname value for this CreatenewsubscriberElement.
     * 
     * @param pLastname
     */
    public void setPLastname(java.lang.String pLastname) {
        this.pLastname = pLastname;
    }


    /**
     * Gets the pCname value for this CreatenewsubscriberElement.
     * 
     * @return pCname
     */
    public java.lang.String getPCname() {
        return pCname;
    }


    /**
     * Sets the pCname value for this CreatenewsubscriberElement.
     * 
     * @param pCname
     */
    public void setPCname(java.lang.String pCname) {
        this.pCname = pCname;
    }


    /**
     * Gets the pArtitle value for this CreatenewsubscriberElement.
     * 
     * @return pArtitle
     */
    public java.lang.String getPArtitle() {
        return pArtitle;
    }


    /**
     * Sets the pArtitle value for this CreatenewsubscriberElement.
     * 
     * @param pArtitle
     */
    public void setPArtitle(java.lang.String pArtitle) {
        this.pArtitle = pArtitle;
    }


    /**
     * Gets the pArfirstname value for this CreatenewsubscriberElement.
     * 
     * @return pArfirstname
     */
    public java.lang.String getPArfirstname() {
        return pArfirstname;
    }


    /**
     * Sets the pArfirstname value for this CreatenewsubscriberElement.
     * 
     * @param pArfirstname
     */
    public void setPArfirstname(java.lang.String pArfirstname) {
        this.pArfirstname = pArfirstname;
    }


    /**
     * Gets the pArbin value for this CreatenewsubscriberElement.
     * 
     * @return pArbin
     */
    public java.lang.String getPArbin() {
        return pArbin;
    }


    /**
     * Sets the pArbin value for this CreatenewsubscriberElement.
     * 
     * @param pArbin
     */
    public void setPArbin(java.lang.String pArbin) {
        this.pArbin = pArbin;
    }


    /**
     * Gets the pArsecondname value for this CreatenewsubscriberElement.
     * 
     * @return pArsecondname
     */
    public java.lang.String getPArsecondname() {
        return pArsecondname;
    }


    /**
     * Sets the pArsecondname value for this CreatenewsubscriberElement.
     * 
     * @param pArsecondname
     */
    public void setPArsecondname(java.lang.String pArsecondname) {
        this.pArsecondname = pArsecondname;
    }


    /**
     * Gets the pArbinbint value for this CreatenewsubscriberElement.
     * 
     * @return pArbinbint
     */
    public java.lang.String getPArbinbint() {
        return pArbinbint;
    }


    /**
     * Sets the pArbinbint value for this CreatenewsubscriberElement.
     * 
     * @param pArbinbint
     */
    public void setPArbinbint(java.lang.String pArbinbint) {
        this.pArbinbint = pArbinbint;
    }


    /**
     * Gets the pArthirdname value for this CreatenewsubscriberElement.
     * 
     * @return pArthirdname
     */
    public java.lang.String getPArthirdname() {
        return pArthirdname;
    }


    /**
     * Sets the pArthirdname value for this CreatenewsubscriberElement.
     * 
     * @param pArthirdname
     */
    public void setPArthirdname(java.lang.String pArthirdname) {
        this.pArthirdname = pArthirdname;
    }


    /**
     * Gets the pArlastname value for this CreatenewsubscriberElement.
     * 
     * @return pArlastname
     */
    public java.lang.String getPArlastname() {
        return pArlastname;
    }


    /**
     * Sets the pArlastname value for this CreatenewsubscriberElement.
     * 
     * @param pArlastname
     */
    public void setPArlastname(java.lang.String pArlastname) {
        this.pArlastname = pArlastname;
    }


    /**
     * Gets the pArcname value for this CreatenewsubscriberElement.
     * 
     * @return pArcname
     */
    public java.lang.String getPArcname() {
        return pArcname;
    }


    /**
     * Sets the pArcname value for this CreatenewsubscriberElement.
     * 
     * @param pArcname
     */
    public void setPArcname(java.lang.String pArcname) {
        this.pArcname = pArcname;
    }


    /**
     * Gets the pHlrProfile value for this CreatenewsubscriberElement.
     * 
     * @return pHlrProfile
     */
    public java.lang.String getPHlrProfile() {
        return pHlrProfile;
    }


    /**
     * Sets the pHlrProfile value for this CreatenewsubscriberElement.
     * 
     * @param pHlrProfile
     */
    public void setPHlrProfile(java.lang.String pHlrProfile) {
        this.pHlrProfile = pHlrProfile;
    }


    /**
     * Gets the pSndcmd value for this CreatenewsubscriberElement.
     * 
     * @return pSndcmd
     */
    public java.lang.String getPSndcmd() {
        return pSndcmd;
    }


    /**
     * Sets the pSndcmd value for this CreatenewsubscriberElement.
     * 
     * @param pSndcmd
     */
    public void setPSndcmd(java.lang.String pSndcmd) {
        this.pSndcmd = pSndcmd;
    }


    /**
     * Gets the pAdditonalparams value for this CreatenewsubscriberElement.
     * 
     * @return pAdditonalparams
     */
    public java.lang.String getPAdditonalparams() {
        return pAdditonalparams;
    }


    /**
     * Sets the pAdditonalparams value for this CreatenewsubscriberElement.
     * 
     * @param pAdditonalparams
     */
    public void setPAdditonalparams(java.lang.String pAdditonalparams) {
        this.pAdditonalparams = pAdditonalparams;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreatenewsubscriberElement)) return false;
        CreatenewsubscriberElement other = (CreatenewsubscriberElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.pArea==null && other.getPArea()==null) || 
             (this.pArea!=null &&
              this.pArea.equals(other.getPArea()))) &&
            ((this.pSubscrtype==null && other.getPSubscrtype()==null) || 
             (this.pSubscrtype!=null &&
              this.pSubscrtype.equals(other.getPSubscrtype()))) &&
            ((this.pSubno==null && other.getPSubno()==null) || 
             (this.pSubno!=null &&
              this.pSubno.equals(other.getPSubno()))) &&
            ((this.pUsername==null && other.getPUsername()==null) || 
             (this.pUsername!=null &&
              this.pUsername.equals(other.getPUsername()))) &&
            ((this.pContract==null && other.getPContract()==null) || 
             (this.pContract!=null &&
              this.pContract.equals(other.getPContract()))) &&
            ((this.pServices==null && other.getPServices()==null) || 
             (this.pServices!=null &&
              this.pServices.equals(other.getPServices()))) &&
            ((this.pSimcard==null && other.getPSimcard()==null) || 
             (this.pSimcard!=null &&
              this.pSimcard.equals(other.getPSimcard()))) &&
            ((this.pCreateContrno==null && other.getPCreateContrno()==null) || 
             (this.pCreateContrno!=null &&
              this.pCreateContrno.equals(other.getPCreateContrno()))) &&
            ((this.pSubsidyFlag==null && other.getPSubsidyFlag()==null) || 
             (this.pSubsidyFlag!=null &&
              this.pSubsidyFlag.equals(other.getPSubsidyFlag()))) &&
            ((this.pPrepPost==null && other.getPPrepPost()==null) || 
             (this.pPrepPost!=null &&
              this.pPrepPost.equals(other.getPPrepPost()))) &&
            ((this.pTariffProfile==null && other.getPTariffProfile()==null) || 
             (this.pTariffProfile!=null &&
              this.pTariffProfile.equals(other.getPTariffProfile()))) &&
            ((this.pTitle==null && other.getPTitle()==null) || 
             (this.pTitle!=null &&
              this.pTitle.equals(other.getPTitle()))) &&
            ((this.pFirstname==null && other.getPFirstname()==null) || 
             (this.pFirstname!=null &&
              this.pFirstname.equals(other.getPFirstname()))) &&
            ((this.pBin==null && other.getPBin()==null) || 
             (this.pBin!=null &&
              this.pBin.equals(other.getPBin()))) &&
            ((this.pSecondname==null && other.getPSecondname()==null) || 
             (this.pSecondname!=null &&
              this.pSecondname.equals(other.getPSecondname()))) &&
            ((this.pBinbint==null && other.getPBinbint()==null) || 
             (this.pBinbint!=null &&
              this.pBinbint.equals(other.getPBinbint()))) &&
            ((this.pThirdname==null && other.getPThirdname()==null) || 
             (this.pThirdname!=null &&
              this.pThirdname.equals(other.getPThirdname()))) &&
            ((this.pLastname==null && other.getPLastname()==null) || 
             (this.pLastname!=null &&
              this.pLastname.equals(other.getPLastname()))) &&
            ((this.pCname==null && other.getPCname()==null) || 
             (this.pCname!=null &&
              this.pCname.equals(other.getPCname()))) &&
            ((this.pArtitle==null && other.getPArtitle()==null) || 
             (this.pArtitle!=null &&
              this.pArtitle.equals(other.getPArtitle()))) &&
            ((this.pArfirstname==null && other.getPArfirstname()==null) || 
             (this.pArfirstname!=null &&
              this.pArfirstname.equals(other.getPArfirstname()))) &&
            ((this.pArbin==null && other.getPArbin()==null) || 
             (this.pArbin!=null &&
              this.pArbin.equals(other.getPArbin()))) &&
            ((this.pArsecondname==null && other.getPArsecondname()==null) || 
             (this.pArsecondname!=null &&
              this.pArsecondname.equals(other.getPArsecondname()))) &&
            ((this.pArbinbint==null && other.getPArbinbint()==null) || 
             (this.pArbinbint!=null &&
              this.pArbinbint.equals(other.getPArbinbint()))) &&
            ((this.pArthirdname==null && other.getPArthirdname()==null) || 
             (this.pArthirdname!=null &&
              this.pArthirdname.equals(other.getPArthirdname()))) &&
            ((this.pArlastname==null && other.getPArlastname()==null) || 
             (this.pArlastname!=null &&
              this.pArlastname.equals(other.getPArlastname()))) &&
            ((this.pArcname==null && other.getPArcname()==null) || 
             (this.pArcname!=null &&
              this.pArcname.equals(other.getPArcname()))) &&
            ((this.pHlrProfile==null && other.getPHlrProfile()==null) || 
             (this.pHlrProfile!=null &&
              this.pHlrProfile.equals(other.getPHlrProfile()))) &&
            ((this.pSndcmd==null && other.getPSndcmd()==null) || 
             (this.pSndcmd!=null &&
              this.pSndcmd.equals(other.getPSndcmd()))) &&
            ((this.pAdditonalparams==null && other.getPAdditonalparams()==null) || 
             (this.pAdditonalparams!=null &&
              this.pAdditonalparams.equals(other.getPAdditonalparams())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPArea() != null) {
            _hashCode += getPArea().hashCode();
        }
        if (getPSubscrtype() != null) {
            _hashCode += getPSubscrtype().hashCode();
        }
        if (getPSubno() != null) {
            _hashCode += getPSubno().hashCode();
        }
        if (getPUsername() != null) {
            _hashCode += getPUsername().hashCode();
        }
        if (getPContract() != null) {
            _hashCode += getPContract().hashCode();
        }
        if (getPServices() != null) {
            _hashCode += getPServices().hashCode();
        }
        if (getPSimcard() != null) {
            _hashCode += getPSimcard().hashCode();
        }
        if (getPCreateContrno() != null) {
            _hashCode += getPCreateContrno().hashCode();
        }
        if (getPSubsidyFlag() != null) {
            _hashCode += getPSubsidyFlag().hashCode();
        }
        if (getPPrepPost() != null) {
            _hashCode += getPPrepPost().hashCode();
        }
        if (getPTariffProfile() != null) {
            _hashCode += getPTariffProfile().hashCode();
        }
        if (getPTitle() != null) {
            _hashCode += getPTitle().hashCode();
        }
        if (getPFirstname() != null) {
            _hashCode += getPFirstname().hashCode();
        }
        if (getPBin() != null) {
            _hashCode += getPBin().hashCode();
        }
        if (getPSecondname() != null) {
            _hashCode += getPSecondname().hashCode();
        }
        if (getPBinbint() != null) {
            _hashCode += getPBinbint().hashCode();
        }
        if (getPThirdname() != null) {
            _hashCode += getPThirdname().hashCode();
        }
        if (getPLastname() != null) {
            _hashCode += getPLastname().hashCode();
        }
        if (getPCname() != null) {
            _hashCode += getPCname().hashCode();
        }
        if (getPArtitle() != null) {
            _hashCode += getPArtitle().hashCode();
        }
        if (getPArfirstname() != null) {
            _hashCode += getPArfirstname().hashCode();
        }
        if (getPArbin() != null) {
            _hashCode += getPArbin().hashCode();
        }
        if (getPArsecondname() != null) {
            _hashCode += getPArsecondname().hashCode();
        }
        if (getPArbinbint() != null) {
            _hashCode += getPArbinbint().hashCode();
        }
        if (getPArthirdname() != null) {
            _hashCode += getPArthirdname().hashCode();
        }
        if (getPArlastname() != null) {
            _hashCode += getPArlastname().hashCode();
        }
        if (getPArcname() != null) {
            _hashCode += getPArcname().hashCode();
        }
        if (getPHlrProfile() != null) {
            _hashCode += getPHlrProfile().hashCode();
        }
        if (getPSndcmd() != null) {
            _hashCode += getPSndcmd().hashCode();
        }
        if (getPAdditonalparams() != null) {
            _hashCode += getPAdditonalparams().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreatenewsubscriberElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">createnewsubscriberElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PArea");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pArea"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PSubscrtype");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pSubscrtype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PSubno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pSubno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PUsername");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pUsername"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PContract");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pContract"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PServices");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pServices"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PSimcard");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pSimcard"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PCreateContrno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pCreateContrno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PSubsidyFlag");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pSubsidyFlag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PPrepPost");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pPrepPost"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PTariffProfile");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pTariffProfile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PTitle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pTitle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PFirstname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pFirstname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PBin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pBin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PSecondname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pSecondname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PBinbint");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pBinbint"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PThirdname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pThirdname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PLastname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pLastname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PCname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pCname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PArtitle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pArtitle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PArfirstname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pArfirstname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PArbin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pArbin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PArsecondname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pArsecondname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PArbinbint");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pArbinbint"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PArthirdname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pArthirdname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PArlastname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pArlastname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PArcname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pArcname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PHlrProfile");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pHlrProfile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PSndcmd");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pSndcmd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PAdditonalparams");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pAdditonalparams"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
