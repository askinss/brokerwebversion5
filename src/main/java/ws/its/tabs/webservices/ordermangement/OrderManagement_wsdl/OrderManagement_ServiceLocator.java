/**
 * OrderManagement_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl;

import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.XMLUtility_old;

public class OrderManagement_ServiceLocator extends org.apache.axis.client.Service implements ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.OrderManagement_Service {
	private LoadAllProperties properties = new LoadAllProperties();
	
    public OrderManagement_ServiceLocator() {
    }


    public OrderManagement_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public OrderManagement_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }
    
    // Use to get a proxy class for OrderManagementSoapHttpPort
    //private java.lang.String OrderManagementSoapHttpPort_address = "http://"+serverAddress+":7779/TABSWEB/OrderManagementSoapHttpPort";
    private java.lang.String OrderManagementSoapHttpPort_address = properties.getProperty("OrderManagementSoapHttpPort_address");

    public java.lang.String getOrderManagementSoapHttpPortAddress() {
        return OrderManagementSoapHttpPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String OrderManagementSoapHttpPortWSDDServiceName = "OrderManagementSoapHttpPort";

    public java.lang.String getOrderManagementSoapHttpPortWSDDServiceName() {
        return OrderManagementSoapHttpPortWSDDServiceName;
    }

    public void setOrderManagementSoapHttpPortWSDDServiceName(java.lang.String name) {
        OrderManagementSoapHttpPortWSDDServiceName = name;
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.OrderManagement_PortType getOrderManagementSoapHttpPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
       System.out.println("Sending TABS request to "+OrderManagementSoapHttpPort_address);
        try {
            endpoint = new java.net.URL(OrderManagementSoapHttpPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getOrderManagementSoapHttpPort(endpoint);
    }

    public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.OrderManagement_PortType getOrderManagementSoapHttpPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.OrderManagementSoapHttpStub _stub = new ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.OrderManagementSoapHttpStub(portAddress, this);
            _stub.setPortName(getOrderManagementSoapHttpPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setOrderManagementSoapHttpPortEndpointAddress(java.lang.String address) {
        OrderManagementSoapHttpPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.OrderManagement_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.OrderManagementSoapHttpStub _stub = new ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.OrderManagementSoapHttpStub(new java.net.URL(OrderManagementSoapHttpPort_address), this);
                _stub.setPortName(getOrderManagementSoapHttpPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("OrderManagementSoapHttpPort".equals(inputPortName)) {
            return getOrderManagementSoapHttpPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl", "OrderManagement");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl", "OrderManagementSoapHttpPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("OrderManagementSoapHttpPort".equals(portName)) {
            setOrderManagementSoapHttpPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
