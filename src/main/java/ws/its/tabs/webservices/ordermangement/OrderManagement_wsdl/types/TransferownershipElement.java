/**
 * TransferownershipElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types;

public class TransferownershipElement  implements java.io.Serializable {
    private java.lang.String pArea;

    private java.lang.String pSubscrtype;

    private java.lang.String pSubno;

    private java.lang.String pUsername;

    private java.lang.String pServices;

    private java.lang.String pNewcontract;

    private java.lang.String pSpecifiedamount;

    private java.lang.String pHlrProfile;

    private java.lang.String pSndcmd;

    private java.lang.String pAdditonalparams;

    public TransferownershipElement() {
    }

    public TransferownershipElement(
           java.lang.String pArea,
           java.lang.String pSubscrtype,
           java.lang.String pSubno,
           java.lang.String pUsername,
           java.lang.String pServices,
           java.lang.String pNewcontract,
           java.lang.String pSpecifiedamount,
           java.lang.String pHlrProfile,
           java.lang.String pSndcmd,
           java.lang.String pAdditonalparams) {
           this.pArea = pArea;
           this.pSubscrtype = pSubscrtype;
           this.pSubno = pSubno;
           this.pUsername = pUsername;
           this.pServices = pServices;
           this.pNewcontract = pNewcontract;
           this.pSpecifiedamount = pSpecifiedamount;
           this.pHlrProfile = pHlrProfile;
           this.pSndcmd = pSndcmd;
           this.pAdditonalparams = pAdditonalparams;
    }


    /**
     * Gets the pArea value for this TransferownershipElement.
     * 
     * @return pArea
     */
    public java.lang.String getPArea() {
        return pArea;
    }


    /**
     * Sets the pArea value for this TransferownershipElement.
     * 
     * @param pArea
     */
    public void setPArea(java.lang.String pArea) {
        this.pArea = pArea;
    }


    /**
     * Gets the pSubscrtype value for this TransferownershipElement.
     * 
     * @return pSubscrtype
     */
    public java.lang.String getPSubscrtype() {
        return pSubscrtype;
    }


    /**
     * Sets the pSubscrtype value for this TransferownershipElement.
     * 
     * @param pSubscrtype
     */
    public void setPSubscrtype(java.lang.String pSubscrtype) {
        this.pSubscrtype = pSubscrtype;
    }


    /**
     * Gets the pSubno value for this TransferownershipElement.
     * 
     * @return pSubno
     */
    public java.lang.String getPSubno() {
        return pSubno;
    }


    /**
     * Sets the pSubno value for this TransferownershipElement.
     * 
     * @param pSubno
     */
    public void setPSubno(java.lang.String pSubno) {
        this.pSubno = pSubno;
    }


    /**
     * Gets the pUsername value for this TransferownershipElement.
     * 
     * @return pUsername
     */
    public java.lang.String getPUsername() {
        return pUsername;
    }


    /**
     * Sets the pUsername value for this TransferownershipElement.
     * 
     * @param pUsername
     */
    public void setPUsername(java.lang.String pUsername) {
        this.pUsername = pUsername;
    }


    /**
     * Gets the pServices value for this TransferownershipElement.
     * 
     * @return pServices
     */
    public java.lang.String getPServices() {
        return pServices;
    }


    /**
     * Sets the pServices value for this TransferownershipElement.
     * 
     * @param pServices
     */
    public void setPServices(java.lang.String pServices) {
        this.pServices = pServices;
    }


    /**
     * Gets the pNewcontract value for this TransferownershipElement.
     * 
     * @return pNewcontract
     */
    public java.lang.String getPNewcontract() {
        return pNewcontract;
    }


    /**
     * Sets the pNewcontract value for this TransferownershipElement.
     * 
     * @param pNewcontract
     */
    public void setPNewcontract(java.lang.String pNewcontract) {
        this.pNewcontract = pNewcontract;
    }


    /**
     * Gets the pSpecifiedamount value for this TransferownershipElement.
     * 
     * @return pSpecifiedamount
     */
    public java.lang.String getPSpecifiedamount() {
        return pSpecifiedamount;
    }


    /**
     * Sets the pSpecifiedamount value for this TransferownershipElement.
     * 
     * @param pSpecifiedamount
     */
    public void setPSpecifiedamount(java.lang.String pSpecifiedamount) {
        this.pSpecifiedamount = pSpecifiedamount;
    }


    /**
     * Gets the pHlrProfile value for this TransferownershipElement.
     * 
     * @return pHlrProfile
     */
    public java.lang.String getPHlrProfile() {
        return pHlrProfile;
    }


    /**
     * Sets the pHlrProfile value for this TransferownershipElement.
     * 
     * @param pHlrProfile
     */
    public void setPHlrProfile(java.lang.String pHlrProfile) {
        this.pHlrProfile = pHlrProfile;
    }


    /**
     * Gets the pSndcmd value for this TransferownershipElement.
     * 
     * @return pSndcmd
     */
    public java.lang.String getPSndcmd() {
        return pSndcmd;
    }


    /**
     * Sets the pSndcmd value for this TransferownershipElement.
     * 
     * @param pSndcmd
     */
    public void setPSndcmd(java.lang.String pSndcmd) {
        this.pSndcmd = pSndcmd;
    }


    /**
     * Gets the pAdditonalparams value for this TransferownershipElement.
     * 
     * @return pAdditonalparams
     */
    public java.lang.String getPAdditonalparams() {
        return pAdditonalparams;
    }


    /**
     * Sets the pAdditonalparams value for this TransferownershipElement.
     * 
     * @param pAdditonalparams
     */
    public void setPAdditonalparams(java.lang.String pAdditonalparams) {
        this.pAdditonalparams = pAdditonalparams;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TransferownershipElement)) return false;
        TransferownershipElement other = (TransferownershipElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.pArea==null && other.getPArea()==null) || 
             (this.pArea!=null &&
              this.pArea.equals(other.getPArea()))) &&
            ((this.pSubscrtype==null && other.getPSubscrtype()==null) || 
             (this.pSubscrtype!=null &&
              this.pSubscrtype.equals(other.getPSubscrtype()))) &&
            ((this.pSubno==null && other.getPSubno()==null) || 
             (this.pSubno!=null &&
              this.pSubno.equals(other.getPSubno()))) &&
            ((this.pUsername==null && other.getPUsername()==null) || 
             (this.pUsername!=null &&
              this.pUsername.equals(other.getPUsername()))) &&
            ((this.pServices==null && other.getPServices()==null) || 
             (this.pServices!=null &&
              this.pServices.equals(other.getPServices()))) &&
            ((this.pNewcontract==null && other.getPNewcontract()==null) || 
             (this.pNewcontract!=null &&
              this.pNewcontract.equals(other.getPNewcontract()))) &&
            ((this.pSpecifiedamount==null && other.getPSpecifiedamount()==null) || 
             (this.pSpecifiedamount!=null &&
              this.pSpecifiedamount.equals(other.getPSpecifiedamount()))) &&
            ((this.pHlrProfile==null && other.getPHlrProfile()==null) || 
             (this.pHlrProfile!=null &&
              this.pHlrProfile.equals(other.getPHlrProfile()))) &&
            ((this.pSndcmd==null && other.getPSndcmd()==null) || 
             (this.pSndcmd!=null &&
              this.pSndcmd.equals(other.getPSndcmd()))) &&
            ((this.pAdditonalparams==null && other.getPAdditonalparams()==null) || 
             (this.pAdditonalparams!=null &&
              this.pAdditonalparams.equals(other.getPAdditonalparams())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPArea() != null) {
            _hashCode += getPArea().hashCode();
        }
        if (getPSubscrtype() != null) {
            _hashCode += getPSubscrtype().hashCode();
        }
        if (getPSubno() != null) {
            _hashCode += getPSubno().hashCode();
        }
        if (getPUsername() != null) {
            _hashCode += getPUsername().hashCode();
        }
        if (getPServices() != null) {
            _hashCode += getPServices().hashCode();
        }
        if (getPNewcontract() != null) {
            _hashCode += getPNewcontract().hashCode();
        }
        if (getPSpecifiedamount() != null) {
            _hashCode += getPSpecifiedamount().hashCode();
        }
        if (getPHlrProfile() != null) {
            _hashCode += getPHlrProfile().hashCode();
        }
        if (getPSndcmd() != null) {
            _hashCode += getPSndcmd().hashCode();
        }
        if (getPAdditonalparams() != null) {
            _hashCode += getPAdditonalparams().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TransferownershipElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", ">transferownershipElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PArea");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pArea"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PSubscrtype");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pSubscrtype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PSubno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pSubno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PUsername");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pUsername"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PServices");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pServices"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PNewcontract");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pNewcontract"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PSpecifiedamount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pSpecifiedamount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PHlrProfile");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pHlrProfile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PSndcmd");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pSndcmd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PAdditonalparams");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ws/its/tabs/webservices/ordermangement/OrderManagement.wsdl/types/", "pAdditonalparams"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
