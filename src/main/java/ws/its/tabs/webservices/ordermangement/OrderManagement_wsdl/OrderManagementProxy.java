package ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl;

public class OrderManagementProxy implements ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.OrderManagement_PortType {
  private String _endpoint = null;
  private ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.OrderManagement_PortType orderManagement_PortType = null;
  
  public OrderManagementProxy() {
    _initOrderManagementProxy();
  }
  
  public OrderManagementProxy(String endpoint) {
    _endpoint = endpoint;
    _initOrderManagementProxy();
  }
  
  private void _initOrderManagementProxy() {
    try {
      orderManagement_PortType = (new ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.OrderManagement_ServiceLocator()).getOrderManagementSoapHttpPort();
      if (orderManagement_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)orderManagement_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)orderManagement_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (orderManagement_PortType != null)
      ((javax.xml.rpc.Stub)orderManagement_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.OrderManagement_PortType getOrderManagement_PortType() {
    if (orderManagement_PortType == null)
      _initOrderManagementProxy();
    return orderManagement_PortType;
  }
  
  public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.AddresschangeResponseElement addresschange(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.AddresschangeElement parameters) throws java.rmi.RemoteException{
    if (orderManagement_PortType == null)
      _initOrderManagementProxy();
    return orderManagement_PortType.addresschange(parameters);
  }
  
  public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronadminrequestResponseElement baronadminrequest(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronadminrequestElement parameters) throws java.rmi.RemoteException{
    if (orderManagement_PortType == null)
      _initOrderManagementProxy();
    return orderManagement_PortType.baronadminrequest(parameters);
  }
  
  public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronsubscriberrequestResponseElement baronsubscriberrequest(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.BaronsubscriberrequestElement parameters) throws java.rmi.RemoteException{
    if (orderManagement_PortType == null)
      _initOrderManagementProxy();
    return orderManagement_PortType.baronsubscriberrequest(parameters);
  }
  
  public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangemsisdnResponseElement changemsisdn(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangemsisdnElement parameters) throws java.rmi.RemoteException{
    if (orderManagement_PortType == null)
      _initOrderManagementProxy();
    return orderManagement_PortType.changemsisdn(parameters);
  }
  
  public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangepaymentresposibilityResponseElement changepaymentresposibility(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangepaymentresposibilityElement parameters) throws java.rmi.RemoteException{
    if (orderManagement_PortType == null)
      _initOrderManagementProxy();
    return orderManagement_PortType.changepaymentresposibility(parameters);
  }
  
  public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeprepostpaidResponseElement changeprepostpaid(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeprepostpaidElement parameters) throws java.rmi.RemoteException{
    if (orderManagement_PortType == null)
      _initOrderManagementProxy();
    return orderManagement_PortType.changeprepostpaid(parameters);
  }
  
  public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeserviceResponseElement changeservice(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeserviceElement parameters) throws java.rmi.RemoteException{
    if (orderManagement_PortType == null)
      _initOrderManagementProxy();
    return orderManagement_PortType.changeservice(parameters);
  }
  
  public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangesimcardResponseElement changesimcard(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangesimcardElement parameters) throws java.rmi.RemoteException{
    if (orderManagement_PortType == null)
      _initOrderManagementProxy();
    return orderManagement_PortType.changesimcard(parameters);
  }
  
  public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.CreatenewsubscriberResponseElement createnewsubscriber(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.CreatenewsubscriberElement parameters) throws java.rmi.RemoteException{
    if (orderManagement_PortType == null)
      _initOrderManagementProxy();
    return orderManagement_PortType.createnewsubscriber(parameters);
  }
  
  public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonadminrequestResponseElement disconnectonadminrequest(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonadminrequestElement parameters) throws java.rmi.RemoteException{
    if (orderManagement_PortType == null)
      _initOrderManagementProxy();
    return orderManagement_PortType.disconnectonadminrequest(parameters);
  }
  
  public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonsubscriberrequestResponseElement disconnectonsubscriberrequest(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.DisconnectonsubscriberrequestElement parameters) throws java.rmi.RemoteException{
    if (orderManagement_PortType == null)
      _initOrderManagementProxy();
    return orderManagement_PortType.disconnectonsubscriberrequest(parameters);
  }
  
  public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.HandlemultiorderResponseElement handlemultiorder(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.HandlemultiorderElement parameters) throws java.rmi.RemoteException{
    if (orderManagement_PortType == null)
      _initOrderManagementProxy();
    return orderManagement_PortType.handlemultiorder(parameters);
  }
  
  public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.NamechangeResponseElement namechange(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.NamechangeElement parameters) throws java.rmi.RemoteException{
    if (orderManagement_PortType == null)
      _initOrderManagementProxy();
    return orderManagement_PortType.namechange(parameters);
  }
  
  public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.Portpager2GsmResponseElement portpager2Gsm(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.Portpager2GsmElement parameters) throws java.rmi.RemoteException{
    if (orderManagement_PortType == null)
      _initOrderManagementProxy();
    return orderManagement_PortType.portpager2Gsm(parameters);
  }
  
  public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectbarResponseElement reconnectbar(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectbarElement parameters) throws java.rmi.RemoteException{
    if (orderManagement_PortType == null)
      _initOrderManagementProxy();
    return orderManagement_PortType.reconnectbar(parameters);
  }
  
  public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonadminrequestResponseElement reconnectonadminrequest(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonadminrequestElement parameters) throws java.rmi.RemoteException{
    if (orderManagement_PortType == null)
      _initOrderManagementProxy();
    return orderManagement_PortType.reconnectonadminrequest(parameters);
  }
  
  public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonsubscriberrequestResponseElement reconnectonsubscriberrequest(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReconnectonsubscriberrequestElement parameters) throws java.rmi.RemoteException{
    if (orderManagement_PortType == null)
      _initOrderManagementProxy();
    return orderManagement_PortType.reconnectonsubscriberrequest(parameters);
  }
  
  public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReinstallsubscriberResponseElement reinstallsubscriber(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ReinstallsubscriberElement parameters) throws java.rmi.RemoteException{
    if (orderManagement_PortType == null)
      _initOrderManagementProxy();
    return orderManagement_PortType.reinstallsubscriber(parameters);
  }
  
  public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TerminateonsubscriberrequestResponseElement terminateonsubscriberrequest(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TerminateonsubscriberrequestElement parameters) throws java.rmi.RemoteException{
    if (orderManagement_PortType == null)
      _initOrderManagementProxy();
    return orderManagement_PortType.terminateonsubscriberrequest(parameters);
  }
  
  public ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TransferownershipResponseElement transferownership(ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.TransferownershipElement parameters) throws java.rmi.RemoteException{
    if (orderManagement_PortType == null)
      _initOrderManagementProxy();
    return orderManagement_PortType.transferownership(parameters);
  }
  
  
}