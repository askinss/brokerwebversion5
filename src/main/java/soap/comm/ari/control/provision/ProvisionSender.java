/**
 * ProvisionSender.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package soap.comm.ari.control.provision;

public class ProvisionSender  implements java.io.Serializable {
    private java.lang.String id;

    private java.lang.String loginId;

    private java.lang.String name;

    private soap.comm.ari.control.provision.ReservedToken[] optionalTokens;

    private java.lang.String password;

    private java.lang.String sapsoldto;

    public ProvisionSender() {
    }

    public ProvisionSender(
           java.lang.String id,
           java.lang.String loginId,
           java.lang.String name,
           soap.comm.ari.control.provision.ReservedToken[] optionalTokens,
           java.lang.String password,
           java.lang.String sapsoldto) {
           this.id = id;
           this.loginId = loginId;
           this.name = name;
           this.optionalTokens = optionalTokens;
           this.password = password;
           this.sapsoldto = sapsoldto;
    }


    /**
     * Gets the id value for this ProvisionSender.
     * 
     * @return id
     */
    public java.lang.String getId() {
        return id;
    }


    /**
     * Sets the id value for this ProvisionSender.
     * 
     * @param id
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }


    /**
     * Gets the loginId value for this ProvisionSender.
     * 
     * @return loginId
     */
    public java.lang.String getLoginId() {
        return loginId;
    }


    /**
     * Sets the loginId value for this ProvisionSender.
     * 
     * @param loginId
     */
    public void setLoginId(java.lang.String loginId) {
        this.loginId = loginId;
    }


    /**
     * Gets the name value for this ProvisionSender.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this ProvisionSender.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the optionalTokens value for this ProvisionSender.
     * 
     * @return optionalTokens
     */
    public soap.comm.ari.control.provision.ReservedToken[] getOptionalTokens() {
        return optionalTokens;
    }


    /**
     * Sets the optionalTokens value for this ProvisionSender.
     * 
     * @param optionalTokens
     */
    public void setOptionalTokens(soap.comm.ari.control.provision.ReservedToken[] optionalTokens) {
        this.optionalTokens = optionalTokens;
    }


    /**
     * Gets the password value for this ProvisionSender.
     * 
     * @return password
     */
    public java.lang.String getPassword() {
        return password;
    }


    /**
     * Sets the password value for this ProvisionSender.
     * 
     * @param password
     */
    public void setPassword(java.lang.String password) {
        this.password = password;
    }


    /**
     * Gets the sapsoldto value for this ProvisionSender.
     * 
     * @return sapsoldto
     */
    public java.lang.String getSapsoldto() {
        return sapsoldto;
    }


    /**
     * Sets the sapsoldto value for this ProvisionSender.
     * 
     * @param sapsoldto
     */
    public void setSapsoldto(java.lang.String sapsoldto) {
        this.sapsoldto = sapsoldto;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProvisionSender)) return false;
        ProvisionSender other = (ProvisionSender) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.loginId==null && other.getLoginId()==null) || 
             (this.loginId!=null &&
              this.loginId.equals(other.getLoginId()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.optionalTokens==null && other.getOptionalTokens()==null) || 
             (this.optionalTokens!=null &&
              java.util.Arrays.equals(this.optionalTokens, other.getOptionalTokens()))) &&
            ((this.password==null && other.getPassword()==null) || 
             (this.password!=null &&
              this.password.equals(other.getPassword()))) &&
            ((this.sapsoldto==null && other.getSapsoldto()==null) || 
             (this.sapsoldto!=null &&
              this.sapsoldto.equals(other.getSapsoldto())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getLoginId() != null) {
            _hashCode += getLoginId().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getOptionalTokens() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOptionalTokens());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOptionalTokens(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPassword() != null) {
            _hashCode += getPassword().hashCode();
        }
        if (getSapsoldto() != null) {
            _hashCode += getSapsoldto().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProvisionSender.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "ProvisionSender"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginId");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "loginId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("optionalTokens");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "optionalTokens"));
        elemField.setXmlType(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "ReservedToken"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("password");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "password"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sapsoldto");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "sapsoldto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
