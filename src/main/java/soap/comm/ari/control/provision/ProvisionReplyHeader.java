/**
 * ProvisionReplyHeader.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package soap.comm.ari.control.provision;

public class ProvisionReplyHeader  implements java.io.Serializable {
    private java.lang.String majorErrCode;

    private java.lang.String majorErrDesc;

    private soap.comm.ari.control.provision.ReservedToken[] optionalTokens;

    private soap.comm.ari.control.provision.TransactionCode[] resultCodes;

    private soap.comm.ari.control.provision.ProvisionSender sender;

    private java.lang.String timeFormat;

    private java.lang.String timeStamp;

    public ProvisionReplyHeader() {
    }

    public ProvisionReplyHeader(
           java.lang.String majorErrCode,
           java.lang.String majorErrDesc,
           soap.comm.ari.control.provision.ReservedToken[] optionalTokens,
           soap.comm.ari.control.provision.TransactionCode[] resultCodes,
           soap.comm.ari.control.provision.ProvisionSender sender,
           java.lang.String timeFormat,
           java.lang.String timeStamp) {
           this.majorErrCode = majorErrCode;
           this.majorErrDesc = majorErrDesc;
           this.optionalTokens = optionalTokens;
           this.resultCodes = resultCodes;
           this.sender = sender;
           this.timeFormat = timeFormat;
           this.timeStamp = timeStamp;
    }


    /**
     * Gets the majorErrCode value for this ProvisionReplyHeader.
     * 
     * @return majorErrCode
     */
    public java.lang.String getMajorErrCode() {
        return majorErrCode;
    }


    /**
     * Sets the majorErrCode value for this ProvisionReplyHeader.
     * 
     * @param majorErrCode
     */
    public void setMajorErrCode(java.lang.String majorErrCode) {
        this.majorErrCode = majorErrCode;
    }


    /**
     * Gets the majorErrDesc value for this ProvisionReplyHeader.
     * 
     * @return majorErrDesc
     */
    public java.lang.String getMajorErrDesc() {
        return majorErrDesc;
    }


    /**
     * Sets the majorErrDesc value for this ProvisionReplyHeader.
     * 
     * @param majorErrDesc
     */
    public void setMajorErrDesc(java.lang.String majorErrDesc) {
        this.majorErrDesc = majorErrDesc;
    }


    /**
     * Gets the optionalTokens value for this ProvisionReplyHeader.
     * 
     * @return optionalTokens
     */
    public soap.comm.ari.control.provision.ReservedToken[] getOptionalTokens() {
        return optionalTokens;
    }


    /**
     * Sets the optionalTokens value for this ProvisionReplyHeader.
     * 
     * @param optionalTokens
     */
    public void setOptionalTokens(soap.comm.ari.control.provision.ReservedToken[] optionalTokens) {
        this.optionalTokens = optionalTokens;
    }


    /**
     * Gets the resultCodes value for this ProvisionReplyHeader.
     * 
     * @return resultCodes
     */
    public soap.comm.ari.control.provision.TransactionCode[] getResultCodes() {
        return resultCodes;
    }


    /**
     * Sets the resultCodes value for this ProvisionReplyHeader.
     * 
     * @param resultCodes
     */
    public void setResultCodes(soap.comm.ari.control.provision.TransactionCode[] resultCodes) {
        this.resultCodes = resultCodes;
    }


    /**
     * Gets the sender value for this ProvisionReplyHeader.
     * 
     * @return sender
     */
    public soap.comm.ari.control.provision.ProvisionSender getSender() {
        return sender;
    }


    /**
     * Sets the sender value for this ProvisionReplyHeader.
     * 
     * @param sender
     */
    public void setSender(soap.comm.ari.control.provision.ProvisionSender sender) {
        this.sender = sender;
    }


    /**
     * Gets the timeFormat value for this ProvisionReplyHeader.
     * 
     * @return timeFormat
     */
    public java.lang.String getTimeFormat() {
        return timeFormat;
    }


    /**
     * Sets the timeFormat value for this ProvisionReplyHeader.
     * 
     * @param timeFormat
     */
    public void setTimeFormat(java.lang.String timeFormat) {
        this.timeFormat = timeFormat;
    }


    /**
     * Gets the timeStamp value for this ProvisionReplyHeader.
     * 
     * @return timeStamp
     */
    public java.lang.String getTimeStamp() {
        return timeStamp;
    }


    /**
     * Sets the timeStamp value for this ProvisionReplyHeader.
     * 
     * @param timeStamp
     */
    public void setTimeStamp(java.lang.String timeStamp) {
        this.timeStamp = timeStamp;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProvisionReplyHeader)) return false;
        ProvisionReplyHeader other = (ProvisionReplyHeader) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.majorErrCode==null && other.getMajorErrCode()==null) || 
             (this.majorErrCode!=null &&
              this.majorErrCode.equals(other.getMajorErrCode()))) &&
            ((this.majorErrDesc==null && other.getMajorErrDesc()==null) || 
             (this.majorErrDesc!=null &&
              this.majorErrDesc.equals(other.getMajorErrDesc()))) &&
            ((this.optionalTokens==null && other.getOptionalTokens()==null) || 
             (this.optionalTokens!=null &&
              java.util.Arrays.equals(this.optionalTokens, other.getOptionalTokens()))) &&
            ((this.resultCodes==null && other.getResultCodes()==null) || 
             (this.resultCodes!=null &&
              java.util.Arrays.equals(this.resultCodes, other.getResultCodes()))) &&
            ((this.sender==null && other.getSender()==null) || 
             (this.sender!=null &&
              this.sender.equals(other.getSender()))) &&
            ((this.timeFormat==null && other.getTimeFormat()==null) || 
             (this.timeFormat!=null &&
              this.timeFormat.equals(other.getTimeFormat()))) &&
            ((this.timeStamp==null && other.getTimeStamp()==null) || 
             (this.timeStamp!=null &&
              this.timeStamp.equals(other.getTimeStamp())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMajorErrCode() != null) {
            _hashCode += getMajorErrCode().hashCode();
        }
        if (getMajorErrDesc() != null) {
            _hashCode += getMajorErrDesc().hashCode();
        }
        if (getOptionalTokens() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOptionalTokens());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOptionalTokens(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getResultCodes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResultCodes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResultCodes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSender() != null) {
            _hashCode += getSender().hashCode();
        }
        if (getTimeFormat() != null) {
            _hashCode += getTimeFormat().hashCode();
        }
        if (getTimeStamp() != null) {
            _hashCode += getTimeStamp().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProvisionReplyHeader.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "ProvisionReplyHeader"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("majorErrCode");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "majorErrCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("majorErrDesc");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "majorErrDesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("optionalTokens");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "optionalTokens"));
        elemField.setXmlType(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "ReservedToken"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultCodes");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "resultCodes"));
        elemField.setXmlType(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "TransactionCode"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sender");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "sender"));
        elemField.setXmlType(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "ProvisionSender"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timeFormat");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "timeFormat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timeStamp");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "timeStamp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
