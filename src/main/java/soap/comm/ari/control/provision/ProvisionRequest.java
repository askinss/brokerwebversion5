/**
 * ProvisionRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package soap.comm.ari.control.provision;

public class ProvisionRequest  implements java.io.Serializable {
    private soap.comm.ari.control.provision.ProvisionReqEntity[] body;

    private soap.comm.ari.control.provision.ProvisionReqHeader header;

    private soap.comm.ari.control.provision.ReservedToken[] optionalTokens;

    private java.lang.String productType;

    private java.lang.String sapsoldto;

    private java.lang.String transactionId;

    private java.lang.String transactionType;

    private java.lang.String version;

    public ProvisionRequest() {
    }

    public ProvisionRequest(
           soap.comm.ari.control.provision.ProvisionReqEntity[] body,
           soap.comm.ari.control.provision.ProvisionReqHeader header,
           soap.comm.ari.control.provision.ReservedToken[] optionalTokens,
           java.lang.String productType,
           java.lang.String sapsoldto,
           java.lang.String transactionId,
           java.lang.String transactionType,
           java.lang.String version) {
           this.body = body;
           this.header = header;
           this.optionalTokens = optionalTokens;
           this.productType = productType;
           this.sapsoldto = sapsoldto;
           this.transactionId = transactionId;
           this.transactionType = transactionType;
           this.version = version;
    }


    /**
     * Gets the body value for this ProvisionRequest.
     * 
     * @return body
     */
    public soap.comm.ari.control.provision.ProvisionReqEntity[] getBody() {
        return body;
    }


    /**
     * Sets the body value for this ProvisionRequest.
     * 
     * @param body
     */
    public void setBody(soap.comm.ari.control.provision.ProvisionReqEntity[] body) {
        this.body = body;
    }


    /**
     * Gets the header value for this ProvisionRequest.
     * 
     * @return header
     */
    public soap.comm.ari.control.provision.ProvisionReqHeader getHeader() {
        return header;
    }


    /**
     * Sets the header value for this ProvisionRequest.
     * 
     * @param header
     */
    public void setHeader(soap.comm.ari.control.provision.ProvisionReqHeader header) {
        this.header = header;
    }


    /**
     * Gets the optionalTokens value for this ProvisionRequest.
     * 
     * @return optionalTokens
     */
    public soap.comm.ari.control.provision.ReservedToken[] getOptionalTokens() {
        return optionalTokens;
    }


    /**
     * Sets the optionalTokens value for this ProvisionRequest.
     * 
     * @param optionalTokens
     */
    public void setOptionalTokens(soap.comm.ari.control.provision.ReservedToken[] optionalTokens) {
        this.optionalTokens = optionalTokens;
    }


    /**
     * Gets the productType value for this ProvisionRequest.
     * 
     * @return productType
     */
    public java.lang.String getProductType() {
        return productType;
    }


    /**
     * Sets the productType value for this ProvisionRequest.
     * 
     * @param productType
     */
    public void setProductType(java.lang.String productType) {
        this.productType = productType;
    }


    /**
     * Gets the sapsoldto value for this ProvisionRequest.
     * 
     * @return sapsoldto
     */
    public java.lang.String getSapsoldto() {
        return sapsoldto;
    }


    /**
     * Sets the sapsoldto value for this ProvisionRequest.
     * 
     * @param sapsoldto
     */
    public void setSapsoldto(java.lang.String sapsoldto) {
        this.sapsoldto = sapsoldto;
    }


    /**
     * Gets the transactionId value for this ProvisionRequest.
     * 
     * @return transactionId
     */
    public java.lang.String getTransactionId() {
        return transactionId;
    }


    /**
     * Sets the transactionId value for this ProvisionRequest.
     * 
     * @param transactionId
     */
    public void setTransactionId(java.lang.String transactionId) {
        this.transactionId = transactionId;
    }


    /**
     * Gets the transactionType value for this ProvisionRequest.
     * 
     * @return transactionType
     */
    public java.lang.String getTransactionType() {
        return transactionType;
    }


    /**
     * Sets the transactionType value for this ProvisionRequest.
     * 
     * @param transactionType
     */
    public void setTransactionType(java.lang.String transactionType) {
        this.transactionType = transactionType;
    }


    /**
     * Gets the version value for this ProvisionRequest.
     * 
     * @return version
     */
    public java.lang.String getVersion() {
        return version;
    }


    /**
     * Sets the version value for this ProvisionRequest.
     * 
     * @param version
     */
    public void setVersion(java.lang.String version) {
        this.version = version;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProvisionRequest)) return false;
        ProvisionRequest other = (ProvisionRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.body==null && other.getBody()==null) || 
             (this.body!=null &&
              java.util.Arrays.equals(this.body, other.getBody()))) &&
            ((this.header==null && other.getHeader()==null) || 
             (this.header!=null &&
              this.header.equals(other.getHeader()))) &&
            ((this.optionalTokens==null && other.getOptionalTokens()==null) || 
             (this.optionalTokens!=null &&
              java.util.Arrays.equals(this.optionalTokens, other.getOptionalTokens()))) &&
            ((this.productType==null && other.getProductType()==null) || 
             (this.productType!=null &&
              this.productType.equals(other.getProductType()))) &&
            ((this.sapsoldto==null && other.getSapsoldto()==null) || 
             (this.sapsoldto!=null &&
              this.sapsoldto.equals(other.getSapsoldto()))) &&
            ((this.transactionId==null && other.getTransactionId()==null) || 
             (this.transactionId!=null &&
              this.transactionId.equals(other.getTransactionId()))) &&
            ((this.transactionType==null && other.getTransactionType()==null) || 
             (this.transactionType!=null &&
              this.transactionType.equals(other.getTransactionType()))) &&
            ((this.version==null && other.getVersion()==null) || 
             (this.version!=null &&
              this.version.equals(other.getVersion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBody() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBody());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBody(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getHeader() != null) {
            _hashCode += getHeader().hashCode();
        }
        if (getOptionalTokens() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOptionalTokens());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOptionalTokens(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getProductType() != null) {
            _hashCode += getProductType().hashCode();
        }
        if (getSapsoldto() != null) {
            _hashCode += getSapsoldto().hashCode();
        }
        if (getTransactionId() != null) {
            _hashCode += getTransactionId().hashCode();
        }
        if (getTransactionType() != null) {
            _hashCode += getTransactionType().hashCode();
        }
        if (getVersion() != null) {
            _hashCode += getVersion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProvisionRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "ProvisionRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("body");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "body"));
        elemField.setXmlType(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "ProvisionReqEntity"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("header");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "header"));
        elemField.setXmlType(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "ProvisionReqHeader"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("optionalTokens");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "optionalTokens"));
        elemField.setXmlType(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "ReservedToken"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productType");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "productType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sapsoldto");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "sapsoldto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionId");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "transactionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionType");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "transactionType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("version");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "version"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
