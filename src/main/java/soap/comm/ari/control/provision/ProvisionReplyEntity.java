/**
 * ProvisionReplyEntity.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package soap.comm.ari.control.provision;

public class ProvisionReplyEntity  implements java.io.Serializable {
    private soap.comm.ari.control.provision.ProvisionDataItem[] items;

    private java.lang.String name;

    private soap.comm.ari.control.provision.ReservedToken[] optionalTokens;

    private soap.comm.ari.control.provision.TransactionCode resultCode;

    private soap.comm.ari.control.provision.ProvisionReplyEntity[] subEntities;

    private java.lang.String type;

    public ProvisionReplyEntity() {
    }

    public ProvisionReplyEntity(
           soap.comm.ari.control.provision.ProvisionDataItem[] items,
           java.lang.String name,
           soap.comm.ari.control.provision.ReservedToken[] optionalTokens,
           soap.comm.ari.control.provision.TransactionCode resultCode,
           soap.comm.ari.control.provision.ProvisionReplyEntity[] subEntities,
           java.lang.String type) {
           this.items = items;
           this.name = name;
           this.optionalTokens = optionalTokens;
           this.resultCode = resultCode;
           this.subEntities = subEntities;
           this.type = type;
    }


    /**
     * Gets the items value for this ProvisionReplyEntity.
     * 
     * @return items
     */
    public soap.comm.ari.control.provision.ProvisionDataItem[] getItems() {
        return items;
    }


    /**
     * Sets the items value for this ProvisionReplyEntity.
     * 
     * @param items
     */
    public void setItems(soap.comm.ari.control.provision.ProvisionDataItem[] items) {
        this.items = items;
    }


    /**
     * Gets the name value for this ProvisionReplyEntity.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this ProvisionReplyEntity.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the optionalTokens value for this ProvisionReplyEntity.
     * 
     * @return optionalTokens
     */
    public soap.comm.ari.control.provision.ReservedToken[] getOptionalTokens() {
        return optionalTokens;
    }


    /**
     * Sets the optionalTokens value for this ProvisionReplyEntity.
     * 
     * @param optionalTokens
     */
    public void setOptionalTokens(soap.comm.ari.control.provision.ReservedToken[] optionalTokens) {
        this.optionalTokens = optionalTokens;
    }


    /**
     * Gets the resultCode value for this ProvisionReplyEntity.
     * 
     * @return resultCode
     */
    public soap.comm.ari.control.provision.TransactionCode getResultCode() {
        return resultCode;
    }


    /**
     * Sets the resultCode value for this ProvisionReplyEntity.
     * 
     * @param resultCode
     */
    public void setResultCode(soap.comm.ari.control.provision.TransactionCode resultCode) {
        this.resultCode = resultCode;
    }


    /**
     * Gets the subEntities value for this ProvisionReplyEntity.
     * 
     * @return subEntities
     */
    public soap.comm.ari.control.provision.ProvisionReplyEntity[] getSubEntities() {
        return subEntities;
    }


    /**
     * Sets the subEntities value for this ProvisionReplyEntity.
     * 
     * @param subEntities
     */
    public void setSubEntities(soap.comm.ari.control.provision.ProvisionReplyEntity[] subEntities) {
        this.subEntities = subEntities;
    }


    /**
     * Gets the type value for this ProvisionReplyEntity.
     * 
     * @return type
     */
    public java.lang.String getType() {
        return type;
    }


    /**
     * Sets the type value for this ProvisionReplyEntity.
     * 
     * @param type
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProvisionReplyEntity)) return false;
        ProvisionReplyEntity other = (ProvisionReplyEntity) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.items==null && other.getItems()==null) || 
             (this.items!=null &&
              java.util.Arrays.equals(this.items, other.getItems()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.optionalTokens==null && other.getOptionalTokens()==null) || 
             (this.optionalTokens!=null &&
              java.util.Arrays.equals(this.optionalTokens, other.getOptionalTokens()))) &&
            ((this.resultCode==null && other.getResultCode()==null) || 
             (this.resultCode!=null &&
              this.resultCode.equals(other.getResultCode()))) &&
            ((this.subEntities==null && other.getSubEntities()==null) || 
             (this.subEntities!=null &&
              java.util.Arrays.equals(this.subEntities, other.getSubEntities()))) &&
            ((this.type==null && other.getType()==null) || 
             (this.type!=null &&
              this.type.equals(other.getType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getItems() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getItems());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getItems(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getOptionalTokens() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOptionalTokens());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOptionalTokens(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getResultCode() != null) {
            _hashCode += getResultCode().hashCode();
        }
        if (getSubEntities() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSubEntities());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSubEntities(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getType() != null) {
            _hashCode += getType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProvisionReplyEntity.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "ProvisionReplyEntity"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("items");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "items"));
        elemField.setXmlType(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "ProvisionDataItem"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("optionalTokens");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "optionalTokens"));
        elemField.setXmlType(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "ReservedToken"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultCode");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "resultCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "TransactionCode"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subEntities");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "subEntities"));
        elemField.setXmlType(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "ProvisionReplyEntity"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("type");
        elemField.setXmlName(new javax.xml.namespace.QName("java:provision.control.ari.comm.soap", "type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
