/**
 * ManHDMBalanceRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.www.bme.cbsinterface.cbs.businessmgr;

public class ManHDMBalanceRequest  implements java.io.Serializable {
    private java.lang.String subscriberNo;

    private int operationType;

    private java.lang.String newHDMEndTime;

    private java.lang.Integer extendHDMPeriod;

    public ManHDMBalanceRequest() {
    }

    public ManHDMBalanceRequest(
           java.lang.String subscriberNo,
           int operationType,
           java.lang.String newHDMEndTime,
           java.lang.Integer extendHDMPeriod) {
           this.subscriberNo = subscriberNo;
           this.operationType = operationType;
           this.newHDMEndTime = newHDMEndTime;
           this.extendHDMPeriod = extendHDMPeriod;
    }


    /**
     * Gets the subscriberNo value for this ManHDMBalanceRequest.
     * 
     * @return subscriberNo
     */
    public java.lang.String getSubscriberNo() {
        return subscriberNo;
    }


    /**
     * Sets the subscriberNo value for this ManHDMBalanceRequest.
     * 
     * @param subscriberNo
     */
    public void setSubscriberNo(java.lang.String subscriberNo) {
        this.subscriberNo = subscriberNo;
    }


    /**
     * Gets the operationType value for this ManHDMBalanceRequest.
     * 
     * @return operationType
     */
    public int getOperationType() {
        return operationType;
    }


    /**
     * Sets the operationType value for this ManHDMBalanceRequest.
     * 
     * @param operationType
     */
    public void setOperationType(int operationType) {
        this.operationType = operationType;
    }


    /**
     * Gets the newHDMEndTime value for this ManHDMBalanceRequest.
     * 
     * @return newHDMEndTime
     */
    public java.lang.String getNewHDMEndTime() {
        return newHDMEndTime;
    }


    /**
     * Sets the newHDMEndTime value for this ManHDMBalanceRequest.
     * 
     * @param newHDMEndTime
     */
    public void setNewHDMEndTime(java.lang.String newHDMEndTime) {
        this.newHDMEndTime = newHDMEndTime;
    }


    /**
     * Gets the extendHDMPeriod value for this ManHDMBalanceRequest.
     * 
     * @return extendHDMPeriod
     */
    public java.lang.Integer getExtendHDMPeriod() {
        return extendHDMPeriod;
    }


    /**
     * Sets the extendHDMPeriod value for this ManHDMBalanceRequest.
     * 
     * @param extendHDMPeriod
     */
    public void setExtendHDMPeriod(java.lang.Integer extendHDMPeriod) {
        this.extendHDMPeriod = extendHDMPeriod;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ManHDMBalanceRequest)) return false;
        ManHDMBalanceRequest other = (ManHDMBalanceRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.subscriberNo==null && other.getSubscriberNo()==null) || 
             (this.subscriberNo!=null &&
              this.subscriberNo.equals(other.getSubscriberNo()))) &&
            this.operationType == other.getOperationType() &&
            ((this.newHDMEndTime==null && other.getNewHDMEndTime()==null) || 
             (this.newHDMEndTime!=null &&
              this.newHDMEndTime.equals(other.getNewHDMEndTime()))) &&
            ((this.extendHDMPeriod==null && other.getExtendHDMPeriod()==null) || 
             (this.extendHDMPeriod!=null &&
              this.extendHDMPeriod.equals(other.getExtendHDMPeriod())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSubscriberNo() != null) {
            _hashCode += getSubscriberNo().hashCode();
        }
        _hashCode += getOperationType();
        if (getNewHDMEndTime() != null) {
            _hashCode += getNewHDMEndTime().hashCode();
        }
        if (getExtendHDMPeriod() != null) {
            _hashCode += getExtendHDMPeriod().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ManHDMBalanceRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "ManHDMBalanceRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriberNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "SubscriberNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operationType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "OperationType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("newHDMEndTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "NewHDMEndTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extendHDMPeriod");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "ExtendHDMPeriod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
