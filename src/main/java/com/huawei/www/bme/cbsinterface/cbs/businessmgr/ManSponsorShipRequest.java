/**
 * ManSponsorShipRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.www.bme.cbsinterface.cbs.businessmgr;

public class ManSponsorShipRequest  implements java.io.Serializable {
    private java.lang.String sponsorNo;

    private com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSponsorShipRequestBeneficiaryInfo beneficiaryInfo;

    private java.lang.String sponsoredNo;

    private int operationType;

    public ManSponsorShipRequest() {
    }

    public ManSponsorShipRequest(
           java.lang.String sponsorNo,
           com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSponsorShipRequestBeneficiaryInfo beneficiaryInfo,
           java.lang.String sponsoredNo,
           int operationType) {
           this.sponsorNo = sponsorNo;
           this.beneficiaryInfo = beneficiaryInfo;
           this.sponsoredNo = sponsoredNo;
           this.operationType = operationType;
    }


    /**
     * Gets the sponsorNo value for this ManSponsorShipRequest.
     * 
     * @return sponsorNo
     */
    public java.lang.String getSponsorNo() {
        return sponsorNo;
    }


    /**
     * Sets the sponsorNo value for this ManSponsorShipRequest.
     * 
     * @param sponsorNo
     */
    public void setSponsorNo(java.lang.String sponsorNo) {
        this.sponsorNo = sponsorNo;
    }


    /**
     * Gets the beneficiaryInfo value for this ManSponsorShipRequest.
     * 
     * @return beneficiaryInfo
     */
    public com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSponsorShipRequestBeneficiaryInfo getBeneficiaryInfo() {
        return beneficiaryInfo;
    }


    /**
     * Sets the beneficiaryInfo value for this ManSponsorShipRequest.
     * 
     * @param beneficiaryInfo
     */
    public void setBeneficiaryInfo(com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSponsorShipRequestBeneficiaryInfo beneficiaryInfo) {
        this.beneficiaryInfo = beneficiaryInfo;
    }


    /**
     * Gets the sponsoredNo value for this ManSponsorShipRequest.
     * 
     * @return sponsoredNo
     */
    public java.lang.String getSponsoredNo() {
        return sponsoredNo;
    }


    /**
     * Sets the sponsoredNo value for this ManSponsorShipRequest.
     * 
     * @param sponsoredNo
     */
    public void setSponsoredNo(java.lang.String sponsoredNo) {
        this.sponsoredNo = sponsoredNo;
    }


    /**
     * Gets the operationType value for this ManSponsorShipRequest.
     * 
     * @return operationType
     */
    public int getOperationType() {
        return operationType;
    }


    /**
     * Sets the operationType value for this ManSponsorShipRequest.
     * 
     * @param operationType
     */
    public void setOperationType(int operationType) {
        this.operationType = operationType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ManSponsorShipRequest)) return false;
        ManSponsorShipRequest other = (ManSponsorShipRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sponsorNo==null && other.getSponsorNo()==null) || 
             (this.sponsorNo!=null &&
              this.sponsorNo.equals(other.getSponsorNo()))) &&
            ((this.beneficiaryInfo==null && other.getBeneficiaryInfo()==null) || 
             (this.beneficiaryInfo!=null &&
              this.beneficiaryInfo.equals(other.getBeneficiaryInfo()))) &&
            ((this.sponsoredNo==null && other.getSponsoredNo()==null) || 
             (this.sponsoredNo!=null &&
              this.sponsoredNo.equals(other.getSponsoredNo()))) &&
            this.operationType == other.getOperationType();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSponsorNo() != null) {
            _hashCode += getSponsorNo().hashCode();
        }
        if (getBeneficiaryInfo() != null) {
            _hashCode += getBeneficiaryInfo().hashCode();
        }
        if (getSponsoredNo() != null) {
            _hashCode += getSponsoredNo().hashCode();
        }
        _hashCode += getOperationType();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ManSponsorShipRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "ManSponsorShipRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sponsorNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "SponsorNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beneficiaryInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "BeneficiaryInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", ">ManSponsorShipRequest>BeneficiaryInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sponsoredNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "SponsoredNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operationType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "OperationType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
