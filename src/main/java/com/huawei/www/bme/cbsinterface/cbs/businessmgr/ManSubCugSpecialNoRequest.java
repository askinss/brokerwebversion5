/**
 * ManSubCugSpecialNoRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.www.bme.cbsinterface.cbs.businessmgr;

public class ManSubCugSpecialNoRequest  implements java.io.Serializable {
    /* (*MSISDN*) */
    private java.lang.String subscriberNo;

    private int operationType;

    private java.math.BigInteger CUGID;

    private java.lang.String cugSpecialNo;

    private java.lang.String newCugSpecialNo;

    public ManSubCugSpecialNoRequest() {
    }

    public ManSubCugSpecialNoRequest(
           java.lang.String subscriberNo,
           int operationType,
           java.math.BigInteger CUGID,
           java.lang.String cugSpecialNo,
           java.lang.String newCugSpecialNo) {
           this.subscriberNo = subscriberNo;
           this.operationType = operationType;
           this.CUGID = CUGID;
           this.cugSpecialNo = cugSpecialNo;
           this.newCugSpecialNo = newCugSpecialNo;
    }


    /**
     * Gets the subscriberNo value for this ManSubCugSpecialNoRequest.
     * 
     * @return subscriberNo   * (*MSISDN*)
     */
    public java.lang.String getSubscriberNo() {
        return subscriberNo;
    }


    /**
     * Sets the subscriberNo value for this ManSubCugSpecialNoRequest.
     * 
     * @param subscriberNo   * (*MSISDN*)
     */
    public void setSubscriberNo(java.lang.String subscriberNo) {
        this.subscriberNo = subscriberNo;
    }


    /**
     * Gets the operationType value for this ManSubCugSpecialNoRequest.
     * 
     * @return operationType
     */
    public int getOperationType() {
        return operationType;
    }


    /**
     * Sets the operationType value for this ManSubCugSpecialNoRequest.
     * 
     * @param operationType
     */
    public void setOperationType(int operationType) {
        this.operationType = operationType;
    }


    /**
     * Gets the CUGID value for this ManSubCugSpecialNoRequest.
     * 
     * @return CUGID
     */
    public java.math.BigInteger getCUGID() {
        return CUGID;
    }


    /**
     * Sets the CUGID value for this ManSubCugSpecialNoRequest.
     * 
     * @param CUGID
     */
    public void setCUGID(java.math.BigInteger CUGID) {
        this.CUGID = CUGID;
    }


    /**
     * Gets the cugSpecialNo value for this ManSubCugSpecialNoRequest.
     * 
     * @return cugSpecialNo
     */
    public java.lang.String getCugSpecialNo() {
        return cugSpecialNo;
    }


    /**
     * Sets the cugSpecialNo value for this ManSubCugSpecialNoRequest.
     * 
     * @param cugSpecialNo
     */
    public void setCugSpecialNo(java.lang.String cugSpecialNo) {
        this.cugSpecialNo = cugSpecialNo;
    }


    /**
     * Gets the newCugSpecialNo value for this ManSubCugSpecialNoRequest.
     * 
     * @return newCugSpecialNo
     */
    public java.lang.String getNewCugSpecialNo() {
        return newCugSpecialNo;
    }


    /**
     * Sets the newCugSpecialNo value for this ManSubCugSpecialNoRequest.
     * 
     * @param newCugSpecialNo
     */
    public void setNewCugSpecialNo(java.lang.String newCugSpecialNo) {
        this.newCugSpecialNo = newCugSpecialNo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ManSubCugSpecialNoRequest)) return false;
        ManSubCugSpecialNoRequest other = (ManSubCugSpecialNoRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.subscriberNo==null && other.getSubscriberNo()==null) || 
             (this.subscriberNo!=null &&
              this.subscriberNo.equals(other.getSubscriberNo()))) &&
            this.operationType == other.getOperationType() &&
            ((this.CUGID==null && other.getCUGID()==null) || 
             (this.CUGID!=null &&
              this.CUGID.equals(other.getCUGID()))) &&
            ((this.cugSpecialNo==null && other.getCugSpecialNo()==null) || 
             (this.cugSpecialNo!=null &&
              this.cugSpecialNo.equals(other.getCugSpecialNo()))) &&
            ((this.newCugSpecialNo==null && other.getNewCugSpecialNo()==null) || 
             (this.newCugSpecialNo!=null &&
              this.newCugSpecialNo.equals(other.getNewCugSpecialNo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSubscriberNo() != null) {
            _hashCode += getSubscriberNo().hashCode();
        }
        _hashCode += getOperationType();
        if (getCUGID() != null) {
            _hashCode += getCUGID().hashCode();
        }
        if (getCugSpecialNo() != null) {
            _hashCode += getCugSpecialNo().hashCode();
        }
        if (getNewCugSpecialNo() != null) {
            _hashCode += getNewCugSpecialNo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ManSubCugSpecialNoRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "ManSubCugSpecialNoRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriberNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "SubscriberNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operationType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "OperationType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUGID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "CUGID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cugSpecialNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "CugSpecialNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("newCugSpecialNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "NewCugSpecialNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
