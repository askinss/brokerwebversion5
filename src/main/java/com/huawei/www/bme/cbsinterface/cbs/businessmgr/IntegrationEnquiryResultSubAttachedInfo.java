/**
 * IntegrationEnquiryResultSubAttachedInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.www.bme.cbsinterface.cbs.businessmgr;

public class IntegrationEnquiryResultSubAttachedInfo  implements java.io.Serializable {
    private java.lang.Integer chgMainProductTimes;

    private java.lang.Integer chgMainPackageTimes;

    private java.lang.Integer activeDays;

    public IntegrationEnquiryResultSubAttachedInfo() {
    }

    public IntegrationEnquiryResultSubAttachedInfo(
           java.lang.Integer chgMainProductTimes,
           java.lang.Integer chgMainPackageTimes,
           java.lang.Integer activeDays) {
           this.chgMainProductTimes = chgMainProductTimes;
           this.chgMainPackageTimes = chgMainPackageTimes;
           this.activeDays = activeDays;
    }


    /**
     * Gets the chgMainProductTimes value for this IntegrationEnquiryResultSubAttachedInfo.
     * 
     * @return chgMainProductTimes
     */
    public java.lang.Integer getChgMainProductTimes() {
        return chgMainProductTimes;
    }


    /**
     * Sets the chgMainProductTimes value for this IntegrationEnquiryResultSubAttachedInfo.
     * 
     * @param chgMainProductTimes
     */
    public void setChgMainProductTimes(java.lang.Integer chgMainProductTimes) {
        this.chgMainProductTimes = chgMainProductTimes;
    }


    /**
     * Gets the chgMainPackageTimes value for this IntegrationEnquiryResultSubAttachedInfo.
     * 
     * @return chgMainPackageTimes
     */
    public java.lang.Integer getChgMainPackageTimes() {
        return chgMainPackageTimes;
    }


    /**
     * Sets the chgMainPackageTimes value for this IntegrationEnquiryResultSubAttachedInfo.
     * 
     * @param chgMainPackageTimes
     */
    public void setChgMainPackageTimes(java.lang.Integer chgMainPackageTimes) {
        this.chgMainPackageTimes = chgMainPackageTimes;
    }


    /**
     * Gets the activeDays value for this IntegrationEnquiryResultSubAttachedInfo.
     * 
     * @return activeDays
     */
    public java.lang.Integer getActiveDays() {
        return activeDays;
    }


    /**
     * Sets the activeDays value for this IntegrationEnquiryResultSubAttachedInfo.
     * 
     * @param activeDays
     */
    public void setActiveDays(java.lang.Integer activeDays) {
        this.activeDays = activeDays;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof IntegrationEnquiryResultSubAttachedInfo)) return false;
        IntegrationEnquiryResultSubAttachedInfo other = (IntegrationEnquiryResultSubAttachedInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.chgMainProductTimes==null && other.getChgMainProductTimes()==null) || 
             (this.chgMainProductTimes!=null &&
              this.chgMainProductTimes.equals(other.getChgMainProductTimes()))) &&
            ((this.chgMainPackageTimes==null && other.getChgMainPackageTimes()==null) || 
             (this.chgMainPackageTimes!=null &&
              this.chgMainPackageTimes.equals(other.getChgMainPackageTimes()))) &&
            ((this.activeDays==null && other.getActiveDays()==null) || 
             (this.activeDays!=null &&
              this.activeDays.equals(other.getActiveDays())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getChgMainProductTimes() != null) {
            _hashCode += getChgMainProductTimes().hashCode();
        }
        if (getChgMainPackageTimes() != null) {
            _hashCode += getChgMainPackageTimes().hashCode();
        }
        if (getActiveDays() != null) {
            _hashCode += getActiveDays().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(IntegrationEnquiryResultSubAttachedInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", ">IntegrationEnquiryResult>SubAttachedInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chgMainProductTimes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "ChgMainProductTimes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chgMainPackageTimes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "ChgMainPackageTimes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activeDays");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "ActiveDays"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
