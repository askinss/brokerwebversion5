/**
 * QuerySubFamilyNoResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.www.bme.cbsinterface.cbs.businessmgr;

public class QuerySubFamilyNoResult  implements java.io.Serializable {
    private com.huawei.www.bme.cbsinterface.cbs.businessmgr.QuerySubFamilyNoResultFamilyNoInfo[] familyNoInfo;

    private com.huawei.www.bme.cbsinterface.cbs.businessmgr.QuerySubFamilyNoResultFNProduct FNProduct;

    public QuerySubFamilyNoResult() {
    }

    public QuerySubFamilyNoResult(
           com.huawei.www.bme.cbsinterface.cbs.businessmgr.QuerySubFamilyNoResultFamilyNoInfo[] familyNoInfo,
           com.huawei.www.bme.cbsinterface.cbs.businessmgr.QuerySubFamilyNoResultFNProduct FNProduct) {
           this.familyNoInfo = familyNoInfo;
           this.FNProduct = FNProduct;
    }


    /**
     * Gets the familyNoInfo value for this QuerySubFamilyNoResult.
     * 
     * @return familyNoInfo
     */
    public com.huawei.www.bme.cbsinterface.cbs.businessmgr.QuerySubFamilyNoResultFamilyNoInfo[] getFamilyNoInfo() {
        return familyNoInfo;
    }


    /**
     * Sets the familyNoInfo value for this QuerySubFamilyNoResult.
     * 
     * @param familyNoInfo
     */
    public void setFamilyNoInfo(com.huawei.www.bme.cbsinterface.cbs.businessmgr.QuerySubFamilyNoResultFamilyNoInfo[] familyNoInfo) {
        this.familyNoInfo = familyNoInfo;
    }

    public com.huawei.www.bme.cbsinterface.cbs.businessmgr.QuerySubFamilyNoResultFamilyNoInfo getFamilyNoInfo(int i) {
        return this.familyNoInfo[i];
    }

    public void setFamilyNoInfo(int i, com.huawei.www.bme.cbsinterface.cbs.businessmgr.QuerySubFamilyNoResultFamilyNoInfo _value) {
        this.familyNoInfo[i] = _value;
    }


    /**
     * Gets the FNProduct value for this QuerySubFamilyNoResult.
     * 
     * @return FNProduct
     */
    public com.huawei.www.bme.cbsinterface.cbs.businessmgr.QuerySubFamilyNoResultFNProduct getFNProduct() {
        return FNProduct;
    }


    /**
     * Sets the FNProduct value for this QuerySubFamilyNoResult.
     * 
     * @param FNProduct
     */
    public void setFNProduct(com.huawei.www.bme.cbsinterface.cbs.businessmgr.QuerySubFamilyNoResultFNProduct FNProduct) {
        this.FNProduct = FNProduct;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof QuerySubFamilyNoResult)) return false;
        QuerySubFamilyNoResult other = (QuerySubFamilyNoResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.familyNoInfo==null && other.getFamilyNoInfo()==null) || 
             (this.familyNoInfo!=null &&
              java.util.Arrays.equals(this.familyNoInfo, other.getFamilyNoInfo()))) &&
            ((this.FNProduct==null && other.getFNProduct()==null) || 
             (this.FNProduct!=null &&
              this.FNProduct.equals(other.getFNProduct())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFamilyNoInfo() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFamilyNoInfo());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFamilyNoInfo(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getFNProduct() != null) {
            _hashCode += getFNProduct().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(QuerySubFamilyNoResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "QuerySubFamilyNoResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("familyNoInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "FamilyNoInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", ">QuerySubFamilyNoResult>FamilyNoInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FNProduct");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "FNProduct"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", ">QuerySubFamilyNoResult>FNProduct"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
