/**
 * ManAccountExtResultAcctChgRecInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.www.bme.cbsinterface.cbs.accountmgr;

public class ManAccountExtResultAcctChgRecInfo  implements java.io.Serializable {
    private long accountBalanceId;

    private java.lang.String applytime;

    private java.lang.String expiretime;

    private java.lang.String oldExpiretime;

    public ManAccountExtResultAcctChgRecInfo() {
    }

    public ManAccountExtResultAcctChgRecInfo(
           long accountBalanceId,
           java.lang.String applytime,
           java.lang.String expiretime,
           java.lang.String oldExpiretime) {
           this.accountBalanceId = accountBalanceId;
           this.applytime = applytime;
           this.expiretime = expiretime;
           this.oldExpiretime = oldExpiretime;
    }


    /**
     * Gets the accountBalanceId value for this ManAccountExtResultAcctChgRecInfo.
     * 
     * @return accountBalanceId
     */
    public long getAccountBalanceId() {
        return accountBalanceId;
    }


    /**
     * Sets the accountBalanceId value for this ManAccountExtResultAcctChgRecInfo.
     * 
     * @param accountBalanceId
     */
    public void setAccountBalanceId(long accountBalanceId) {
        this.accountBalanceId = accountBalanceId;
    }


    /**
     * Gets the applytime value for this ManAccountExtResultAcctChgRecInfo.
     * 
     * @return applytime
     */
    public java.lang.String getApplytime() {
        return applytime;
    }


    /**
     * Sets the applytime value for this ManAccountExtResultAcctChgRecInfo.
     * 
     * @param applytime
     */
    public void setApplytime(java.lang.String applytime) {
        this.applytime = applytime;
    }


    /**
     * Gets the expiretime value for this ManAccountExtResultAcctChgRecInfo.
     * 
     * @return expiretime
     */
    public java.lang.String getExpiretime() {
        return expiretime;
    }


    /**
     * Sets the expiretime value for this ManAccountExtResultAcctChgRecInfo.
     * 
     * @param expiretime
     */
    public void setExpiretime(java.lang.String expiretime) {
        this.expiretime = expiretime;
    }


    /**
     * Gets the oldExpiretime value for this ManAccountExtResultAcctChgRecInfo.
     * 
     * @return oldExpiretime
     */
    public java.lang.String getOldExpiretime() {
        return oldExpiretime;
    }


    /**
     * Sets the oldExpiretime value for this ManAccountExtResultAcctChgRecInfo.
     * 
     * @param oldExpiretime
     */
    public void setOldExpiretime(java.lang.String oldExpiretime) {
        this.oldExpiretime = oldExpiretime;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ManAccountExtResultAcctChgRecInfo)) return false;
        ManAccountExtResultAcctChgRecInfo other = (ManAccountExtResultAcctChgRecInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.accountBalanceId == other.getAccountBalanceId() &&
            ((this.applytime==null && other.getApplytime()==null) || 
             (this.applytime!=null &&
              this.applytime.equals(other.getApplytime()))) &&
            ((this.expiretime==null && other.getExpiretime()==null) || 
             (this.expiretime!=null &&
              this.expiretime.equals(other.getExpiretime()))) &&
            ((this.oldExpiretime==null && other.getOldExpiretime()==null) || 
             (this.oldExpiretime!=null &&
              this.oldExpiretime.equals(other.getOldExpiretime())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getAccountBalanceId()).hashCode();
        if (getApplytime() != null) {
            _hashCode += getApplytime().hashCode();
        }
        if (getExpiretime() != null) {
            _hashCode += getExpiretime().hashCode();
        }
        if (getOldExpiretime() != null) {
            _hashCode += getOldExpiretime().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ManAccountExtResultAcctChgRecInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/accountmgr", ">ManAccountExtResult>AcctChgRecInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountBalanceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/accountmgr", "AccountBalanceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("applytime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/accountmgr", "Applytime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expiretime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/accountmgr", "Expiretime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oldExpiretime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/accountmgr", "OldExpiretime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
