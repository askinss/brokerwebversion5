/**
 * ManSubCugSpecialNoResultMsg.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.www.bme.cbsinterface.cbs.businessmgrmsg;

public class ManSubCugSpecialNoResultMsg  implements java.io.Serializable {
    private com.huawei.www.bme.cbsinterface.common.ResultHeader resultHeader;

    private com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSubCugSpecialNoResult manSubCugSpecialNoResult;

    public ManSubCugSpecialNoResultMsg() {
    }

    public ManSubCugSpecialNoResultMsg(
           com.huawei.www.bme.cbsinterface.common.ResultHeader resultHeader,
           com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSubCugSpecialNoResult manSubCugSpecialNoResult) {
           this.resultHeader = resultHeader;
           this.manSubCugSpecialNoResult = manSubCugSpecialNoResult;
    }


    /**
     * Gets the resultHeader value for this ManSubCugSpecialNoResultMsg.
     * 
     * @return resultHeader
     */
    public com.huawei.www.bme.cbsinterface.common.ResultHeader getResultHeader() {
        return resultHeader;
    }


    /**
     * Sets the resultHeader value for this ManSubCugSpecialNoResultMsg.
     * 
     * @param resultHeader
     */
    public void setResultHeader(com.huawei.www.bme.cbsinterface.common.ResultHeader resultHeader) {
        this.resultHeader = resultHeader;
    }


    /**
     * Gets the manSubCugSpecialNoResult value for this ManSubCugSpecialNoResultMsg.
     * 
     * @return manSubCugSpecialNoResult
     */
    public com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSubCugSpecialNoResult getManSubCugSpecialNoResult() {
        return manSubCugSpecialNoResult;
    }


    /**
     * Sets the manSubCugSpecialNoResult value for this ManSubCugSpecialNoResultMsg.
     * 
     * @param manSubCugSpecialNoResult
     */
    public void setManSubCugSpecialNoResult(com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSubCugSpecialNoResult manSubCugSpecialNoResult) {
        this.manSubCugSpecialNoResult = manSubCugSpecialNoResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ManSubCugSpecialNoResultMsg)) return false;
        ManSubCugSpecialNoResultMsg other = (ManSubCugSpecialNoResultMsg) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.resultHeader==null && other.getResultHeader()==null) || 
             (this.resultHeader!=null &&
              this.resultHeader.equals(other.getResultHeader()))) &&
            ((this.manSubCugSpecialNoResult==null && other.getManSubCugSpecialNoResult()==null) || 
             (this.manSubCugSpecialNoResult!=null &&
              this.manSubCugSpecialNoResult.equals(other.getManSubCugSpecialNoResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResultHeader() != null) {
            _hashCode += getResultHeader().hashCode();
        }
        if (getManSubCugSpecialNoResult() != null) {
            _hashCode += getManSubCugSpecialNoResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ManSubCugSpecialNoResultMsg.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgrmsg", ">ManSubCugSpecialNoResultMsg"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ResultHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/common", "ResultHeader"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("manSubCugSpecialNoResult");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ManSubCugSpecialNoResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "ManSubCugSpecialNoResult"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
