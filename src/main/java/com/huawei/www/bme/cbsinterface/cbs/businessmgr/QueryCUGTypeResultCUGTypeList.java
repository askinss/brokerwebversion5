/**
 * QueryCUGTypeResultCUGTypeList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.www.bme.cbsinterface.cbs.businessmgr;

public class QueryCUGTypeResultCUGTypeList  implements java.io.Serializable {
    private java.math.BigInteger CUGType;

    private java.lang.String CUGTypeName;

    public QueryCUGTypeResultCUGTypeList() {
    }

    public QueryCUGTypeResultCUGTypeList(
           java.math.BigInteger CUGType,
           java.lang.String CUGTypeName) {
           this.CUGType = CUGType;
           this.CUGTypeName = CUGTypeName;
    }


    /**
     * Gets the CUGType value for this QueryCUGTypeResultCUGTypeList.
     * 
     * @return CUGType
     */
    public java.math.BigInteger getCUGType() {
        return CUGType;
    }


    /**
     * Sets the CUGType value for this QueryCUGTypeResultCUGTypeList.
     * 
     * @param CUGType
     */
    public void setCUGType(java.math.BigInteger CUGType) {
        this.CUGType = CUGType;
    }


    /**
     * Gets the CUGTypeName value for this QueryCUGTypeResultCUGTypeList.
     * 
     * @return CUGTypeName
     */
    public java.lang.String getCUGTypeName() {
        return CUGTypeName;
    }


    /**
     * Sets the CUGTypeName value for this QueryCUGTypeResultCUGTypeList.
     * 
     * @param CUGTypeName
     */
    public void setCUGTypeName(java.lang.String CUGTypeName) {
        this.CUGTypeName = CUGTypeName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof QueryCUGTypeResultCUGTypeList)) return false;
        QueryCUGTypeResultCUGTypeList other = (QueryCUGTypeResultCUGTypeList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CUGType==null && other.getCUGType()==null) || 
             (this.CUGType!=null &&
              this.CUGType.equals(other.getCUGType()))) &&
            ((this.CUGTypeName==null && other.getCUGTypeName()==null) || 
             (this.CUGTypeName!=null &&
              this.CUGTypeName.equals(other.getCUGTypeName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCUGType() != null) {
            _hashCode += getCUGType().hashCode();
        }
        if (getCUGTypeName() != null) {
            _hashCode += getCUGTypeName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(QueryCUGTypeResultCUGTypeList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", ">QueryCUGTypeResult>CUGTypeList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUGType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "CUGType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUGTypeName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "CUGTypeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
