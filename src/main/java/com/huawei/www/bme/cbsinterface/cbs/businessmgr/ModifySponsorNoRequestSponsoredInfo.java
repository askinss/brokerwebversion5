/**
 * ModifySponsorNoRequestSponsoredInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.www.bme.cbsinterface.cbs.businessmgr;

public class ModifySponsorNoRequestSponsoredInfo  implements java.io.Serializable {
    /* (*MSISDN*) */
    private java.lang.String sponsoredNo;

    /* (*MSISDN*) */
    private java.lang.String newSponsoredNo;

    public ModifySponsorNoRequestSponsoredInfo() {
    }

    public ModifySponsorNoRequestSponsoredInfo(
           java.lang.String sponsoredNo,
           java.lang.String newSponsoredNo) {
           this.sponsoredNo = sponsoredNo;
           this.newSponsoredNo = newSponsoredNo;
    }


    /**
     * Gets the sponsoredNo value for this ModifySponsorNoRequestSponsoredInfo.
     * 
     * @return sponsoredNo   * (*MSISDN*)
     */
    public java.lang.String getSponsoredNo() {
        return sponsoredNo;
    }


    /**
     * Sets the sponsoredNo value for this ModifySponsorNoRequestSponsoredInfo.
     * 
     * @param sponsoredNo   * (*MSISDN*)
     */
    public void setSponsoredNo(java.lang.String sponsoredNo) {
        this.sponsoredNo = sponsoredNo;
    }


    /**
     * Gets the newSponsoredNo value for this ModifySponsorNoRequestSponsoredInfo.
     * 
     * @return newSponsoredNo   * (*MSISDN*)
     */
    public java.lang.String getNewSponsoredNo() {
        return newSponsoredNo;
    }


    /**
     * Sets the newSponsoredNo value for this ModifySponsorNoRequestSponsoredInfo.
     * 
     * @param newSponsoredNo   * (*MSISDN*)
     */
    public void setNewSponsoredNo(java.lang.String newSponsoredNo) {
        this.newSponsoredNo = newSponsoredNo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ModifySponsorNoRequestSponsoredInfo)) return false;
        ModifySponsorNoRequestSponsoredInfo other = (ModifySponsorNoRequestSponsoredInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sponsoredNo==null && other.getSponsoredNo()==null) || 
             (this.sponsoredNo!=null &&
              this.sponsoredNo.equals(other.getSponsoredNo()))) &&
            ((this.newSponsoredNo==null && other.getNewSponsoredNo()==null) || 
             (this.newSponsoredNo!=null &&
              this.newSponsoredNo.equals(other.getNewSponsoredNo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSponsoredNo() != null) {
            _hashCode += getSponsoredNo().hashCode();
        }
        if (getNewSponsoredNo() != null) {
            _hashCode += getNewSponsoredNo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ModifySponsorNoRequestSponsoredInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", ">ModifySponsorNoRequest>SponsoredInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sponsoredNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "SponsoredNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("newSponsoredNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "NewSponsoredNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
