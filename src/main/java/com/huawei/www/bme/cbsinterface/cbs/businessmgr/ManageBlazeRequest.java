/**
 * ManageBlazeRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.www.bme.cbsinterface.cbs.businessmgr;

public class ManageBlazeRequest  implements java.io.Serializable {
    private java.lang.Integer productID;

    private int operationType;

    private java.lang.String originalPartNo;

    private java.lang.String destionPartNo;

    public ManageBlazeRequest() {
    }

    public ManageBlazeRequest(
           java.lang.Integer productID,
           int operationType,
           java.lang.String originalPartNo,
           java.lang.String destionPartNo) {
           this.productID = productID;
           this.operationType = operationType;
           this.originalPartNo = originalPartNo;
           this.destionPartNo = destionPartNo;
    }


    /**
     * Gets the productID value for this ManageBlazeRequest.
     * 
     * @return productID
     */
    public java.lang.Integer getProductID() {
        return productID;
    }


    /**
     * Sets the productID value for this ManageBlazeRequest.
     * 
     * @param productID
     */
    public void setProductID(java.lang.Integer productID) {
        this.productID = productID;
    }


    /**
     * Gets the operationType value for this ManageBlazeRequest.
     * 
     * @return operationType
     */
    public int getOperationType() {
        return operationType;
    }


    /**
     * Sets the operationType value for this ManageBlazeRequest.
     * 
     * @param operationType
     */
    public void setOperationType(int operationType) {
        this.operationType = operationType;
    }


    /**
     * Gets the originalPartNo value for this ManageBlazeRequest.
     * 
     * @return originalPartNo
     */
    public java.lang.String getOriginalPartNo() {
        return originalPartNo;
    }


    /**
     * Sets the originalPartNo value for this ManageBlazeRequest.
     * 
     * @param originalPartNo
     */
    public void setOriginalPartNo(java.lang.String originalPartNo) {
        this.originalPartNo = originalPartNo;
    }


    /**
     * Gets the destionPartNo value for this ManageBlazeRequest.
     * 
     * @return destionPartNo
     */
    public java.lang.String getDestionPartNo() {
        return destionPartNo;
    }


    /**
     * Sets the destionPartNo value for this ManageBlazeRequest.
     * 
     * @param destionPartNo
     */
    public void setDestionPartNo(java.lang.String destionPartNo) {
        this.destionPartNo = destionPartNo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ManageBlazeRequest)) return false;
        ManageBlazeRequest other = (ManageBlazeRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.productID==null && other.getProductID()==null) || 
             (this.productID!=null &&
              this.productID.equals(other.getProductID()))) &&
            this.operationType == other.getOperationType() &&
            ((this.originalPartNo==null && other.getOriginalPartNo()==null) || 
             (this.originalPartNo!=null &&
              this.originalPartNo.equals(other.getOriginalPartNo()))) &&
            ((this.destionPartNo==null && other.getDestionPartNo()==null) || 
             (this.destionPartNo!=null &&
              this.destionPartNo.equals(other.getDestionPartNo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getProductID() != null) {
            _hashCode += getProductID().hashCode();
        }
        _hashCode += getOperationType();
        if (getOriginalPartNo() != null) {
            _hashCode += getOriginalPartNo().hashCode();
        }
        if (getDestionPartNo() != null) {
            _hashCode += getDestionPartNo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ManageBlazeRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "ManageBlazeRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "ProductID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operationType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "OperationType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("originalPartNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "OriginalPartNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("destionPartNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "DestionPartNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
