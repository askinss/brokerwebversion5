/**
 * ManSponsorShipRequestBeneficiaryInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.www.bme.cbsinterface.cbs.businessmgr;

public class ManSponsorShipRequestBeneficiaryInfo  implements java.io.Serializable {
    private java.lang.String beneficiaryNo;

    private java.lang.Integer limitQuota;

    private java.lang.String effectiveDate;

    private java.lang.String expireDate;

    public ManSponsorShipRequestBeneficiaryInfo() {
    }

    public ManSponsorShipRequestBeneficiaryInfo(
           java.lang.String beneficiaryNo,
           java.lang.Integer limitQuota,
           java.lang.String effectiveDate,
           java.lang.String expireDate) {
           this.beneficiaryNo = beneficiaryNo;
           this.limitQuota = limitQuota;
           this.effectiveDate = effectiveDate;
           this.expireDate = expireDate;
    }


    /**
     * Gets the beneficiaryNo value for this ManSponsorShipRequestBeneficiaryInfo.
     * 
     * @return beneficiaryNo
     */
    public java.lang.String getBeneficiaryNo() {
        return beneficiaryNo;
    }


    /**
     * Sets the beneficiaryNo value for this ManSponsorShipRequestBeneficiaryInfo.
     * 
     * @param beneficiaryNo
     */
    public void setBeneficiaryNo(java.lang.String beneficiaryNo) {
        this.beneficiaryNo = beneficiaryNo;
    }


    /**
     * Gets the limitQuota value for this ManSponsorShipRequestBeneficiaryInfo.
     * 
     * @return limitQuota
     */
    public java.lang.Integer getLimitQuota() {
        return limitQuota;
    }


    /**
     * Sets the limitQuota value for this ManSponsorShipRequestBeneficiaryInfo.
     * 
     * @param limitQuota
     */
    public void setLimitQuota(java.lang.Integer limitQuota) {
        this.limitQuota = limitQuota;
    }


    /**
     * Gets the effectiveDate value for this ManSponsorShipRequestBeneficiaryInfo.
     * 
     * @return effectiveDate
     */
    public java.lang.String getEffectiveDate() {
        return effectiveDate;
    }


    /**
     * Sets the effectiveDate value for this ManSponsorShipRequestBeneficiaryInfo.
     * 
     * @param effectiveDate
     */
    public void setEffectiveDate(java.lang.String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }


    /**
     * Gets the expireDate value for this ManSponsorShipRequestBeneficiaryInfo.
     * 
     * @return expireDate
     */
    public java.lang.String getExpireDate() {
        return expireDate;
    }


    /**
     * Sets the expireDate value for this ManSponsorShipRequestBeneficiaryInfo.
     * 
     * @param expireDate
     */
    public void setExpireDate(java.lang.String expireDate) {
        this.expireDate = expireDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ManSponsorShipRequestBeneficiaryInfo)) return false;
        ManSponsorShipRequestBeneficiaryInfo other = (ManSponsorShipRequestBeneficiaryInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.beneficiaryNo==null && other.getBeneficiaryNo()==null) || 
             (this.beneficiaryNo!=null &&
              this.beneficiaryNo.equals(other.getBeneficiaryNo()))) &&
            ((this.limitQuota==null && other.getLimitQuota()==null) || 
             (this.limitQuota!=null &&
              this.limitQuota.equals(other.getLimitQuota()))) &&
            ((this.effectiveDate==null && other.getEffectiveDate()==null) || 
             (this.effectiveDate!=null &&
              this.effectiveDate.equals(other.getEffectiveDate()))) &&
            ((this.expireDate==null && other.getExpireDate()==null) || 
             (this.expireDate!=null &&
              this.expireDate.equals(other.getExpireDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBeneficiaryNo() != null) {
            _hashCode += getBeneficiaryNo().hashCode();
        }
        if (getLimitQuota() != null) {
            _hashCode += getLimitQuota().hashCode();
        }
        if (getEffectiveDate() != null) {
            _hashCode += getEffectiveDate().hashCode();
        }
        if (getExpireDate() != null) {
            _hashCode += getExpireDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ManSponsorShipRequestBeneficiaryInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", ">ManSponsorShipRequest>BeneficiaryInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beneficiaryNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "BeneficiaryNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limitQuota");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "LimitQuota"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("effectiveDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "EffectiveDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expireDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "ExpireDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
