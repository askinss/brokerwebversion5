/**
 * ManSponsorShipResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.www.bme.cbsinterface.cbs.businessmgr;

public class ManSponsorShipResult  implements java.io.Serializable {
    private java.lang.String sponsorNo;

    private com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSponsorShipResultBeneficiaryInfo[] beneficiaryInfo;

    public ManSponsorShipResult() {
    }

    public ManSponsorShipResult(
           java.lang.String sponsorNo,
           com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSponsorShipResultBeneficiaryInfo[] beneficiaryInfo) {
           this.sponsorNo = sponsorNo;
           this.beneficiaryInfo = beneficiaryInfo;
    }


    /**
     * Gets the sponsorNo value for this ManSponsorShipResult.
     * 
     * @return sponsorNo
     */
    public java.lang.String getSponsorNo() {
        return sponsorNo;
    }


    /**
     * Sets the sponsorNo value for this ManSponsorShipResult.
     * 
     * @param sponsorNo
     */
    public void setSponsorNo(java.lang.String sponsorNo) {
        this.sponsorNo = sponsorNo;
    }


    /**
     * Gets the beneficiaryInfo value for this ManSponsorShipResult.
     * 
     * @return beneficiaryInfo
     */
    public com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSponsorShipResultBeneficiaryInfo[] getBeneficiaryInfo() {
        return beneficiaryInfo;
    }


    /**
     * Sets the beneficiaryInfo value for this ManSponsorShipResult.
     * 
     * @param beneficiaryInfo
     */
    public void setBeneficiaryInfo(com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSponsorShipResultBeneficiaryInfo[] beneficiaryInfo) {
        this.beneficiaryInfo = beneficiaryInfo;
    }

    public com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSponsorShipResultBeneficiaryInfo getBeneficiaryInfo(int i) {
        return this.beneficiaryInfo[i];
    }

    public void setBeneficiaryInfo(int i, com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSponsorShipResultBeneficiaryInfo _value) {
        this.beneficiaryInfo[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ManSponsorShipResult)) return false;
        ManSponsorShipResult other = (ManSponsorShipResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sponsorNo==null && other.getSponsorNo()==null) || 
             (this.sponsorNo!=null &&
              this.sponsorNo.equals(other.getSponsorNo()))) &&
            ((this.beneficiaryInfo==null && other.getBeneficiaryInfo()==null) || 
             (this.beneficiaryInfo!=null &&
              java.util.Arrays.equals(this.beneficiaryInfo, other.getBeneficiaryInfo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSponsorNo() != null) {
            _hashCode += getSponsorNo().hashCode();
        }
        if (getBeneficiaryInfo() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBeneficiaryInfo());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBeneficiaryInfo(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ManSponsorShipResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "ManSponsorShipResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sponsorNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "SponsorNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beneficiaryInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "BeneficiaryInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", ">ManSponsorShipResult>BeneficiaryInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
