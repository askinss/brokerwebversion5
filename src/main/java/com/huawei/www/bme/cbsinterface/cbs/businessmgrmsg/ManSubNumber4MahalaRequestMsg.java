/**
 * ManSubNumber4MahalaRequestMsg.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.www.bme.cbsinterface.cbs.businessmgrmsg;

public class ManSubNumber4MahalaRequestMsg  implements java.io.Serializable {
    private com.huawei.www.bme.cbsinterface.common.RequestHeader requestHeader;

    private com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSubNumber4MahalaRequest manSubNumber4MahalaRequest;

    public ManSubNumber4MahalaRequestMsg() {
    }

    public ManSubNumber4MahalaRequestMsg(
           com.huawei.www.bme.cbsinterface.common.RequestHeader requestHeader,
           com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSubNumber4MahalaRequest manSubNumber4MahalaRequest) {
           this.requestHeader = requestHeader;
           this.manSubNumber4MahalaRequest = manSubNumber4MahalaRequest;
    }


    /**
     * Gets the requestHeader value for this ManSubNumber4MahalaRequestMsg.
     * 
     * @return requestHeader
     */
    public com.huawei.www.bme.cbsinterface.common.RequestHeader getRequestHeader() {
        return requestHeader;
    }


    /**
     * Sets the requestHeader value for this ManSubNumber4MahalaRequestMsg.
     * 
     * @param requestHeader
     */
    public void setRequestHeader(com.huawei.www.bme.cbsinterface.common.RequestHeader requestHeader) {
        this.requestHeader = requestHeader;
    }


    /**
     * Gets the manSubNumber4MahalaRequest value for this ManSubNumber4MahalaRequestMsg.
     * 
     * @return manSubNumber4MahalaRequest
     */
    public com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSubNumber4MahalaRequest getManSubNumber4MahalaRequest() {
        return manSubNumber4MahalaRequest;
    }


    /**
     * Sets the manSubNumber4MahalaRequest value for this ManSubNumber4MahalaRequestMsg.
     * 
     * @param manSubNumber4MahalaRequest
     */
    public void setManSubNumber4MahalaRequest(com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSubNumber4MahalaRequest manSubNumber4MahalaRequest) {
        this.manSubNumber4MahalaRequest = manSubNumber4MahalaRequest;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ManSubNumber4MahalaRequestMsg)) return false;
        ManSubNumber4MahalaRequestMsg other = (ManSubNumber4MahalaRequestMsg) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.requestHeader==null && other.getRequestHeader()==null) || 
             (this.requestHeader!=null &&
              this.requestHeader.equals(other.getRequestHeader()))) &&
            ((this.manSubNumber4MahalaRequest==null && other.getManSubNumber4MahalaRequest()==null) || 
             (this.manSubNumber4MahalaRequest!=null &&
              this.manSubNumber4MahalaRequest.equals(other.getManSubNumber4MahalaRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRequestHeader() != null) {
            _hashCode += getRequestHeader().hashCode();
        }
        if (getManSubNumber4MahalaRequest() != null) {
            _hashCode += getManSubNumber4MahalaRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ManSubNumber4MahalaRequestMsg.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgrmsg", ">ManSubNumber4MahalaRequestMsg"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requestHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RequestHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/common", "RequestHeader"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("manSubNumber4MahalaRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ManSubNumber4MahalaRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "ManSubNumber4MahalaRequest"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
