/**
 * LoanResultLoanInformation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.www.bme.cbsinterface.cbs.accountmgr;

public class LoanResultLoanInformation  implements java.io.Serializable {
    private java.lang.Long loanAmount;

    private java.lang.Integer loanValidity;

    private java.lang.Long maxBalance;

    public LoanResultLoanInformation() {
    }

    public LoanResultLoanInformation(
           java.lang.Long loanAmount,
           java.lang.Integer loanValidity,
           java.lang.Long maxBalance) {
           this.loanAmount = loanAmount;
           this.loanValidity = loanValidity;
           this.maxBalance = maxBalance;
    }


    /**
     * Gets the loanAmount value for this LoanResultLoanInformation.
     * 
     * @return loanAmount
     */
    public java.lang.Long getLoanAmount() {
        return loanAmount;
    }


    /**
     * Sets the loanAmount value for this LoanResultLoanInformation.
     * 
     * @param loanAmount
     */
    public void setLoanAmount(java.lang.Long loanAmount) {
        this.loanAmount = loanAmount;
    }


    /**
     * Gets the loanValidity value for this LoanResultLoanInformation.
     * 
     * @return loanValidity
     */
    public java.lang.Integer getLoanValidity() {
        return loanValidity;
    }


    /**
     * Sets the loanValidity value for this LoanResultLoanInformation.
     * 
     * @param loanValidity
     */
    public void setLoanValidity(java.lang.Integer loanValidity) {
        this.loanValidity = loanValidity;
    }


    /**
     * Gets the maxBalance value for this LoanResultLoanInformation.
     * 
     * @return maxBalance
     */
    public java.lang.Long getMaxBalance() {
        return maxBalance;
    }


    /**
     * Sets the maxBalance value for this LoanResultLoanInformation.
     * 
     * @param maxBalance
     */
    public void setMaxBalance(java.lang.Long maxBalance) {
        this.maxBalance = maxBalance;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LoanResultLoanInformation)) return false;
        LoanResultLoanInformation other = (LoanResultLoanInformation) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loanAmount==null && other.getLoanAmount()==null) || 
             (this.loanAmount!=null &&
              this.loanAmount.equals(other.getLoanAmount()))) &&
            ((this.loanValidity==null && other.getLoanValidity()==null) || 
             (this.loanValidity!=null &&
              this.loanValidity.equals(other.getLoanValidity()))) &&
            ((this.maxBalance==null && other.getMaxBalance()==null) || 
             (this.maxBalance!=null &&
              this.maxBalance.equals(other.getMaxBalance())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoanAmount() != null) {
            _hashCode += getLoanAmount().hashCode();
        }
        if (getLoanValidity() != null) {
            _hashCode += getLoanValidity().hashCode();
        }
        if (getMaxBalance() != null) {
            _hashCode += getMaxBalance().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LoanResultLoanInformation.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/accountmgr", ">LoanResult>LoanInformation"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loanAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/accountmgr", "LoanAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loanValidity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/accountmgr", "LoanValidity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxBalance");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/accountmgr", "MaxBalance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
