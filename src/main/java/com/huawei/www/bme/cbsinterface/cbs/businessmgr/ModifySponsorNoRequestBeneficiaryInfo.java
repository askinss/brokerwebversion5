/**
 * ModifySponsorNoRequestBeneficiaryInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.www.bme.cbsinterface.cbs.businessmgr;

public class ModifySponsorNoRequestBeneficiaryInfo  implements java.io.Serializable {
    /* (*MSISDN*) */
    private java.lang.String beneficiaryNo;

    /* (*MSISDN*) */
    private java.lang.String newBeneficiaryNo;

    public ModifySponsorNoRequestBeneficiaryInfo() {
    }

    public ModifySponsorNoRequestBeneficiaryInfo(
           java.lang.String beneficiaryNo,
           java.lang.String newBeneficiaryNo) {
           this.beneficiaryNo = beneficiaryNo;
           this.newBeneficiaryNo = newBeneficiaryNo;
    }


    /**
     * Gets the beneficiaryNo value for this ModifySponsorNoRequestBeneficiaryInfo.
     * 
     * @return beneficiaryNo   * (*MSISDN*)
     */
    public java.lang.String getBeneficiaryNo() {
        return beneficiaryNo;
    }


    /**
     * Sets the beneficiaryNo value for this ModifySponsorNoRequestBeneficiaryInfo.
     * 
     * @param beneficiaryNo   * (*MSISDN*)
     */
    public void setBeneficiaryNo(java.lang.String beneficiaryNo) {
        this.beneficiaryNo = beneficiaryNo;
    }


    /**
     * Gets the newBeneficiaryNo value for this ModifySponsorNoRequestBeneficiaryInfo.
     * 
     * @return newBeneficiaryNo   * (*MSISDN*)
     */
    public java.lang.String getNewBeneficiaryNo() {
        return newBeneficiaryNo;
    }


    /**
     * Sets the newBeneficiaryNo value for this ModifySponsorNoRequestBeneficiaryInfo.
     * 
     * @param newBeneficiaryNo   * (*MSISDN*)
     */
    public void setNewBeneficiaryNo(java.lang.String newBeneficiaryNo) {
        this.newBeneficiaryNo = newBeneficiaryNo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ModifySponsorNoRequestBeneficiaryInfo)) return false;
        ModifySponsorNoRequestBeneficiaryInfo other = (ModifySponsorNoRequestBeneficiaryInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.beneficiaryNo==null && other.getBeneficiaryNo()==null) || 
             (this.beneficiaryNo!=null &&
              this.beneficiaryNo.equals(other.getBeneficiaryNo()))) &&
            ((this.newBeneficiaryNo==null && other.getNewBeneficiaryNo()==null) || 
             (this.newBeneficiaryNo!=null &&
              this.newBeneficiaryNo.equals(other.getNewBeneficiaryNo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBeneficiaryNo() != null) {
            _hashCode += getBeneficiaryNo().hashCode();
        }
        if (getNewBeneficiaryNo() != null) {
            _hashCode += getNewBeneficiaryNo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ModifySponsorNoRequestBeneficiaryInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", ">ModifySponsorNoRequest>BeneficiaryInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beneficiaryNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "BeneficiaryNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("newBeneficiaryNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "NewBeneficiaryNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
