/**
 * ManAccountExtRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.www.bme.cbsinterface.cbs.accountmgr;

public class ManAccountExtRequest  implements java.io.Serializable {
    private java.lang.String subscriberNo;

    private java.lang.String accountType;

    private java.lang.Integer numberOfDays;

    private java.lang.String newExpireTime;

    private com.huawei.www.bme.cbsinterface.cbs.accountmgr.AccountOperationType operationType;

    public ManAccountExtRequest() {
    }

    public ManAccountExtRequest(
           java.lang.String subscriberNo,
           java.lang.String accountType,
           java.lang.Integer numberOfDays,
           java.lang.String newExpireTime,
           com.huawei.www.bme.cbsinterface.cbs.accountmgr.AccountOperationType operationType) {
           this.subscriberNo = subscriberNo;
           this.accountType = accountType;
           this.numberOfDays = numberOfDays;
           this.newExpireTime = newExpireTime;
           this.operationType = operationType;
    }


    /**
     * Gets the subscriberNo value for this ManAccountExtRequest.
     * 
     * @return subscriberNo
     */
    public java.lang.String getSubscriberNo() {
        return subscriberNo;
    }


    /**
     * Sets the subscriberNo value for this ManAccountExtRequest.
     * 
     * @param subscriberNo
     */
    public void setSubscriberNo(java.lang.String subscriberNo) {
        this.subscriberNo = subscriberNo;
    }


    /**
     * Gets the accountType value for this ManAccountExtRequest.
     * 
     * @return accountType
     */
    public java.lang.String getAccountType() {
        return accountType;
    }


    /**
     * Sets the accountType value for this ManAccountExtRequest.
     * 
     * @param accountType
     */
    public void setAccountType(java.lang.String accountType) {
        this.accountType = accountType;
    }


    /**
     * Gets the numberOfDays value for this ManAccountExtRequest.
     * 
     * @return numberOfDays
     */
    public java.lang.Integer getNumberOfDays() {
        return numberOfDays;
    }


    /**
     * Sets the numberOfDays value for this ManAccountExtRequest.
     * 
     * @param numberOfDays
     */
    public void setNumberOfDays(java.lang.Integer numberOfDays) {
        this.numberOfDays = numberOfDays;
    }


    /**
     * Gets the newExpireTime value for this ManAccountExtRequest.
     * 
     * @return newExpireTime
     */
    public java.lang.String getNewExpireTime() {
        return newExpireTime;
    }


    /**
     * Sets the newExpireTime value for this ManAccountExtRequest.
     * 
     * @param newExpireTime
     */
    public void setNewExpireTime(java.lang.String newExpireTime) {
        this.newExpireTime = newExpireTime;
    }


    /**
     * Gets the operationType value for this ManAccountExtRequest.
     * 
     * @return operationType
     */
    public com.huawei.www.bme.cbsinterface.cbs.accountmgr.AccountOperationType getOperationType() {
        return operationType;
    }


    /**
     * Sets the operationType value for this ManAccountExtRequest.
     * 
     * @param operationType
     */
    public void setOperationType(com.huawei.www.bme.cbsinterface.cbs.accountmgr.AccountOperationType operationType) {
        this.operationType = operationType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ManAccountExtRequest)) return false;
        ManAccountExtRequest other = (ManAccountExtRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.subscriberNo==null && other.getSubscriberNo()==null) || 
             (this.subscriberNo!=null &&
              this.subscriberNo.equals(other.getSubscriberNo()))) &&
            ((this.accountType==null && other.getAccountType()==null) || 
             (this.accountType!=null &&
              this.accountType.equals(other.getAccountType()))) &&
            ((this.numberOfDays==null && other.getNumberOfDays()==null) || 
             (this.numberOfDays!=null &&
              this.numberOfDays.equals(other.getNumberOfDays()))) &&
            ((this.newExpireTime==null && other.getNewExpireTime()==null) || 
             (this.newExpireTime!=null &&
              this.newExpireTime.equals(other.getNewExpireTime()))) &&
            ((this.operationType==null && other.getOperationType()==null) || 
             (this.operationType!=null &&
              this.operationType.equals(other.getOperationType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSubscriberNo() != null) {
            _hashCode += getSubscriberNo().hashCode();
        }
        if (getAccountType() != null) {
            _hashCode += getAccountType().hashCode();
        }
        if (getNumberOfDays() != null) {
            _hashCode += getNumberOfDays().hashCode();
        }
        if (getNewExpireTime() != null) {
            _hashCode += getNewExpireTime().hashCode();
        }
        if (getOperationType() != null) {
            _hashCode += getOperationType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ManAccountExtRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/accountmgr", "ManAccountExtRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriberNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/accountmgr", "SubscriberNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/accountmgr", "AccountType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numberOfDays");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/accountmgr", "NumberOfDays"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("newExpireTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/accountmgr", "NewExpireTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operationType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/accountmgr", "OperationType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/accountmgr", "AccountOperationType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
