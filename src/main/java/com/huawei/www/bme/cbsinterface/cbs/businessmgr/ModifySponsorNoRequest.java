/**
 * ModifySponsorNoRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.www.bme.cbsinterface.cbs.businessmgr;

public class ModifySponsorNoRequest  implements java.io.Serializable {
    /* (*MSISDN*) */
    private java.lang.String sponsorNo;

    private int operationType;

    private com.huawei.www.bme.cbsinterface.cbs.businessmgr.ModifySponsorNoRequestBeneficiaryInfo beneficiaryInfo;

    private com.huawei.www.bme.cbsinterface.cbs.businessmgr.ModifySponsorNoRequestSponsoredInfo sponsoredInfo;

    public ModifySponsorNoRequest() {
    }

    public ModifySponsorNoRequest(
           java.lang.String sponsorNo,
           int operationType,
           com.huawei.www.bme.cbsinterface.cbs.businessmgr.ModifySponsorNoRequestBeneficiaryInfo beneficiaryInfo,
           com.huawei.www.bme.cbsinterface.cbs.businessmgr.ModifySponsorNoRequestSponsoredInfo sponsoredInfo) {
           this.sponsorNo = sponsorNo;
           this.operationType = operationType;
           this.beneficiaryInfo = beneficiaryInfo;
           this.sponsoredInfo = sponsoredInfo;
    }


    /**
     * Gets the sponsorNo value for this ModifySponsorNoRequest.
     * 
     * @return sponsorNo   * (*MSISDN*)
     */
    public java.lang.String getSponsorNo() {
        return sponsorNo;
    }


    /**
     * Sets the sponsorNo value for this ModifySponsorNoRequest.
     * 
     * @param sponsorNo   * (*MSISDN*)
     */
    public void setSponsorNo(java.lang.String sponsorNo) {
        this.sponsorNo = sponsorNo;
    }


    /**
     * Gets the operationType value for this ModifySponsorNoRequest.
     * 
     * @return operationType
     */
    public int getOperationType() {
        return operationType;
    }


    /**
     * Sets the operationType value for this ModifySponsorNoRequest.
     * 
     * @param operationType
     */
    public void setOperationType(int operationType) {
        this.operationType = operationType;
    }


    /**
     * Gets the beneficiaryInfo value for this ModifySponsorNoRequest.
     * 
     * @return beneficiaryInfo
     */
    public com.huawei.www.bme.cbsinterface.cbs.businessmgr.ModifySponsorNoRequestBeneficiaryInfo getBeneficiaryInfo() {
        return beneficiaryInfo;
    }


    /**
     * Sets the beneficiaryInfo value for this ModifySponsorNoRequest.
     * 
     * @param beneficiaryInfo
     */
    public void setBeneficiaryInfo(com.huawei.www.bme.cbsinterface.cbs.businessmgr.ModifySponsorNoRequestBeneficiaryInfo beneficiaryInfo) {
        this.beneficiaryInfo = beneficiaryInfo;
    }


    /**
     * Gets the sponsoredInfo value for this ModifySponsorNoRequest.
     * 
     * @return sponsoredInfo
     */
    public com.huawei.www.bme.cbsinterface.cbs.businessmgr.ModifySponsorNoRequestSponsoredInfo getSponsoredInfo() {
        return sponsoredInfo;
    }


    /**
     * Sets the sponsoredInfo value for this ModifySponsorNoRequest.
     * 
     * @param sponsoredInfo
     */
    public void setSponsoredInfo(com.huawei.www.bme.cbsinterface.cbs.businessmgr.ModifySponsorNoRequestSponsoredInfo sponsoredInfo) {
        this.sponsoredInfo = sponsoredInfo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ModifySponsorNoRequest)) return false;
        ModifySponsorNoRequest other = (ModifySponsorNoRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sponsorNo==null && other.getSponsorNo()==null) || 
             (this.sponsorNo!=null &&
              this.sponsorNo.equals(other.getSponsorNo()))) &&
            this.operationType == other.getOperationType() &&
            ((this.beneficiaryInfo==null && other.getBeneficiaryInfo()==null) || 
             (this.beneficiaryInfo!=null &&
              this.beneficiaryInfo.equals(other.getBeneficiaryInfo()))) &&
            ((this.sponsoredInfo==null && other.getSponsoredInfo()==null) || 
             (this.sponsoredInfo!=null &&
              this.sponsoredInfo.equals(other.getSponsoredInfo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSponsorNo() != null) {
            _hashCode += getSponsorNo().hashCode();
        }
        _hashCode += getOperationType();
        if (getBeneficiaryInfo() != null) {
            _hashCode += getBeneficiaryInfo().hashCode();
        }
        if (getSponsoredInfo() != null) {
            _hashCode += getSponsoredInfo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ModifySponsorNoRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "ModifySponsorNoRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sponsorNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "SponsorNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operationType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "OperationType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("beneficiaryInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "BeneficiaryInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", ">ModifySponsorNoRequest>BeneficiaryInfo"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sponsoredInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "SponsoredInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", ">ModifySponsorNoRequest>SponsoredInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
