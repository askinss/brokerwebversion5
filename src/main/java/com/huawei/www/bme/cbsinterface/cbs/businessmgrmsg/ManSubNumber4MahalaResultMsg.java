/**
 * ManSubNumber4MahalaResultMsg.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.www.bme.cbsinterface.cbs.businessmgrmsg;

public class ManSubNumber4MahalaResultMsg  implements java.io.Serializable {
    private com.huawei.www.bme.cbsinterface.common.ResultHeader resultHeader;

    private com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSubNumber4MahalaResult manSubNumber4MahalaResult;

    public ManSubNumber4MahalaResultMsg() {
    }

    public ManSubNumber4MahalaResultMsg(
           com.huawei.www.bme.cbsinterface.common.ResultHeader resultHeader,
           com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSubNumber4MahalaResult manSubNumber4MahalaResult) {
           this.resultHeader = resultHeader;
           this.manSubNumber4MahalaResult = manSubNumber4MahalaResult;
    }


    /**
     * Gets the resultHeader value for this ManSubNumber4MahalaResultMsg.
     * 
     * @return resultHeader
     */
    public com.huawei.www.bme.cbsinterface.common.ResultHeader getResultHeader() {
        return resultHeader;
    }


    /**
     * Sets the resultHeader value for this ManSubNumber4MahalaResultMsg.
     * 
     * @param resultHeader
     */
    public void setResultHeader(com.huawei.www.bme.cbsinterface.common.ResultHeader resultHeader) {
        this.resultHeader = resultHeader;
    }


    /**
     * Gets the manSubNumber4MahalaResult value for this ManSubNumber4MahalaResultMsg.
     * 
     * @return manSubNumber4MahalaResult
     */
    public com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSubNumber4MahalaResult getManSubNumber4MahalaResult() {
        return manSubNumber4MahalaResult;
    }


    /**
     * Sets the manSubNumber4MahalaResult value for this ManSubNumber4MahalaResultMsg.
     * 
     * @param manSubNumber4MahalaResult
     */
    public void setManSubNumber4MahalaResult(com.huawei.www.bme.cbsinterface.cbs.businessmgr.ManSubNumber4MahalaResult manSubNumber4MahalaResult) {
        this.manSubNumber4MahalaResult = manSubNumber4MahalaResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ManSubNumber4MahalaResultMsg)) return false;
        ManSubNumber4MahalaResultMsg other = (ManSubNumber4MahalaResultMsg) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.resultHeader==null && other.getResultHeader()==null) || 
             (this.resultHeader!=null &&
              this.resultHeader.equals(other.getResultHeader()))) &&
            ((this.manSubNumber4MahalaResult==null && other.getManSubNumber4MahalaResult()==null) || 
             (this.manSubNumber4MahalaResult!=null &&
              this.manSubNumber4MahalaResult.equals(other.getManSubNumber4MahalaResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResultHeader() != null) {
            _hashCode += getResultHeader().hashCode();
        }
        if (getManSubNumber4MahalaResult() != null) {
            _hashCode += getManSubNumber4MahalaResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ManSubNumber4MahalaResultMsg.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgrmsg", ">ManSubNumber4MahalaResultMsg"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ResultHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/common", "ResultHeader"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("manSubNumber4MahalaResult");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ManSubNumber4MahalaResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.huawei.com/bme/cbsinterface/cbs/businessmgr", "ManSubNumber4MahalaResult"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
