/**
 * RcvWriteBackMsg.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.mds.access.webservice.server.bean;

public class RcvWriteBackMsg  implements java.io.Serializable {
    private com.huawei.mds.access.webservice.server.bean.RcvWriteBackMsgRequestMessage requestMessage;

    public RcvWriteBackMsg() {
    }

    public RcvWriteBackMsg(
           com.huawei.mds.access.webservice.server.bean.RcvWriteBackMsgRequestMessage requestMessage) {
           this.requestMessage = requestMessage;
    }


    /**
     * Gets the requestMessage value for this RcvWriteBackMsg.
     * 
     * @return requestMessage
     */
    public com.huawei.mds.access.webservice.server.bean.RcvWriteBackMsgRequestMessage getRequestMessage() {
        return requestMessage;
    }


    /**
     * Sets the requestMessage value for this RcvWriteBackMsg.
     * 
     * @param requestMessage
     */
    public void setRequestMessage(com.huawei.mds.access.webservice.server.bean.RcvWriteBackMsgRequestMessage requestMessage) {
        this.requestMessage = requestMessage;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RcvWriteBackMsg)) return false;
        RcvWriteBackMsg other = (RcvWriteBackMsg) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.requestMessage==null && other.getRequestMessage()==null) || 
             (this.requestMessage!=null &&
              this.requestMessage.equals(other.getRequestMessage())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRequestMessage() != null) {
            _hashCode += getRequestMessage().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RcvWriteBackMsg.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://huawei.com/mds/access/webservice/server/bean", "RcvWriteBackMsg"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requestMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RequestMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://huawei.com/mds/access/webservice/server/bean", "RcvWriteBackMsgRequestMessage"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
