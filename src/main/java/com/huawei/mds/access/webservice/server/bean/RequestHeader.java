/**
 * RequestHeader.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.mds.access.webservice.server.bean;

public class RequestHeader  implements java.io.Serializable {
    private java.lang.String sysUser;

    private java.lang.String sysPassword;

    private java.lang.String debugFlag;

    private java.lang.String sessionID;

    private int[] traceIDs;

    public RequestHeader() {
    }

    public RequestHeader(
           java.lang.String sysUser,
           java.lang.String sysPassword,
           java.lang.String debugFlag,
           java.lang.String sessionID,
           int[] traceIDs) {
           this.sysUser = sysUser;
           this.sysPassword = sysPassword;
           this.debugFlag = debugFlag;
           this.sessionID = sessionID;
           this.traceIDs = traceIDs;
    }


    /**
     * Gets the sysUser value for this RequestHeader.
     * 
     * @return sysUser
     */
    public java.lang.String getSysUser() {
        return sysUser;
    }


    /**
     * Sets the sysUser value for this RequestHeader.
     * 
     * @param sysUser
     */
    public void setSysUser(java.lang.String sysUser) {
        this.sysUser = sysUser;
    }


    /**
     * Gets the sysPassword value for this RequestHeader.
     * 
     * @return sysPassword
     */
    public java.lang.String getSysPassword() {
        return sysPassword;
    }


    /**
     * Sets the sysPassword value for this RequestHeader.
     * 
     * @param sysPassword
     */
    public void setSysPassword(java.lang.String sysPassword) {
        this.sysPassword = sysPassword;
    }


    /**
     * Gets the debugFlag value for this RequestHeader.
     * 
     * @return debugFlag
     */
    public java.lang.String getDebugFlag() {
        return debugFlag;
    }


    /**
     * Sets the debugFlag value for this RequestHeader.
     * 
     * @param debugFlag
     */
    public void setDebugFlag(java.lang.String debugFlag) {
        this.debugFlag = debugFlag;
    }


    /**
     * Gets the sessionID value for this RequestHeader.
     * 
     * @return sessionID
     */
    public java.lang.String getSessionID() {
        return sessionID;
    }


    /**
     * Sets the sessionID value for this RequestHeader.
     * 
     * @param sessionID
     */
    public void setSessionID(java.lang.String sessionID) {
        this.sessionID = sessionID;
    }


    /**
     * Gets the traceIDs value for this RequestHeader.
     * 
     * @return traceIDs
     */
    public int[] getTraceIDs() {
        return traceIDs;
    }


    /**
     * Sets the traceIDs value for this RequestHeader.
     * 
     * @param traceIDs
     */
    public void setTraceIDs(int[] traceIDs) {
        this.traceIDs = traceIDs;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RequestHeader)) return false;
        RequestHeader other = (RequestHeader) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sysUser==null && other.getSysUser()==null) || 
             (this.sysUser!=null &&
              this.sysUser.equals(other.getSysUser()))) &&
            ((this.sysPassword==null && other.getSysPassword()==null) || 
             (this.sysPassword!=null &&
              this.sysPassword.equals(other.getSysPassword()))) &&
            ((this.debugFlag==null && other.getDebugFlag()==null) || 
             (this.debugFlag!=null &&
              this.debugFlag.equals(other.getDebugFlag()))) &&
            ((this.sessionID==null && other.getSessionID()==null) || 
             (this.sessionID!=null &&
              this.sessionID.equals(other.getSessionID()))) &&
            ((this.traceIDs==null && other.getTraceIDs()==null) || 
             (this.traceIDs!=null &&
              java.util.Arrays.equals(this.traceIDs, other.getTraceIDs())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSysUser() != null) {
            _hashCode += getSysUser().hashCode();
        }
        if (getSysPassword() != null) {
            _hashCode += getSysPassword().hashCode();
        }
        if (getDebugFlag() != null) {
            _hashCode += getDebugFlag().hashCode();
        }
        if (getSessionID() != null) {
            _hashCode += getSessionID().hashCode();
        }
        if (getTraceIDs() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTraceIDs());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTraceIDs(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RequestHeader.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://huawei.com/mds/access/webservice/server/bean", "RequestHeader"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sysUser");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SysUser"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sysPassword");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SysPassword"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debugFlag");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DebugFlag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SessionID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("traceIDs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TraceIDs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "TraceID"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
