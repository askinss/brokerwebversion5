/**
 * SendRequestMsgRequestMessage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.mds.access.webservice.server.bean;

public class SendRequestMsgRequestMessage  implements java.io.Serializable {
    private com.huawei.mds.access.webservice.server.bean.RequestHeader messageHeader;

    private com.huawei.mds.access.webservice.server.bean.RequestBean[] messageBody;

    public SendRequestMsgRequestMessage() {
    }

    public SendRequestMsgRequestMessage(
           com.huawei.mds.access.webservice.server.bean.RequestHeader messageHeader,
           com.huawei.mds.access.webservice.server.bean.RequestBean[] messageBody) {
           this.messageHeader = messageHeader;
           this.messageBody = messageBody;
    }


    /**
     * Gets the messageHeader value for this SendRequestMsgRequestMessage.
     * 
     * @return messageHeader
     */
    public com.huawei.mds.access.webservice.server.bean.RequestHeader getMessageHeader() {
        return messageHeader;
    }


    /**
     * Sets the messageHeader value for this SendRequestMsgRequestMessage.
     * 
     * @param messageHeader
     */
    public void setMessageHeader(com.huawei.mds.access.webservice.server.bean.RequestHeader messageHeader) {
        this.messageHeader = messageHeader;
    }


    /**
     * Gets the messageBody value for this SendRequestMsgRequestMessage.
     * 
     * @return messageBody
     */
    public com.huawei.mds.access.webservice.server.bean.RequestBean[] getMessageBody() {
        return messageBody;
    }


    /**
     * Sets the messageBody value for this SendRequestMsgRequestMessage.
     * 
     * @param messageBody
     */
    public void setMessageBody(com.huawei.mds.access.webservice.server.bean.RequestBean[] messageBody) {
        this.messageBody = messageBody;
    }

    public com.huawei.mds.access.webservice.server.bean.RequestBean getMessageBody(int i) {
        return this.messageBody[i];
    }

    public void setMessageBody(int i, com.huawei.mds.access.webservice.server.bean.RequestBean _value) {
        this.messageBody[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SendRequestMsgRequestMessage)) return false;
        SendRequestMsgRequestMessage other = (SendRequestMsgRequestMessage) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.messageHeader==null && other.getMessageHeader()==null) || 
             (this.messageHeader!=null &&
              this.messageHeader.equals(other.getMessageHeader()))) &&
            ((this.messageBody==null && other.getMessageBody()==null) || 
             (this.messageBody!=null &&
              java.util.Arrays.equals(this.messageBody, other.getMessageBody())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMessageHeader() != null) {
            _hashCode += getMessageHeader().hashCode();
        }
        if (getMessageBody() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMessageBody());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMessageBody(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SendRequestMsgRequestMessage.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://huawei.com/mds/access/webservice/server/bean", "SendRequestMsgRequestMessage"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("messageHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MessageHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://huawei.com/mds/access/webservice/server/bean", "RequestHeader"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("messageBody");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MessageBody"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://huawei.com/mds/access/webservice/server/bean", "RequestBean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
