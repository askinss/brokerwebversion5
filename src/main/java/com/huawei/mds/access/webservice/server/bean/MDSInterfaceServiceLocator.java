/**
 * MDSInterfaceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.mds.access.webservice.server.bean;

public class MDSInterfaceServiceLocator extends org.apache.axis.client.Service implements com.huawei.mds.access.webservice.server.bean.MDSInterfaceService {

    public MDSInterfaceServiceLocator() {
    }


    public MDSInterfaceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public MDSInterfaceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for MDSInterfacePort
    private java.lang.String MDSInterfacePort_address = "http://172.27.120.252:8090/provision";

    public java.lang.String getMDSInterfacePortAddress() {
        return MDSInterfacePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String MDSInterfacePortWSDDServiceName = "MDSInterfacePort";

    public java.lang.String getMDSInterfacePortWSDDServiceName() {
        return MDSInterfacePortWSDDServiceName;
    }

    public void setMDSInterfacePortWSDDServiceName(java.lang.String name) {
        MDSInterfacePortWSDDServiceName = name;
    }

    public com.huawei.mds.access.webservice.server.bean.MDSInterface getMDSInterfacePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(MDSInterfacePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getMDSInterfacePort(endpoint);
    }

    public com.huawei.mds.access.webservice.server.bean.MDSInterface getMDSInterfacePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.huawei.mds.access.webservice.server.bean.MDSInterfacePortBindingStub _stub = new com.huawei.mds.access.webservice.server.bean.MDSInterfacePortBindingStub(portAddress, this);
            _stub.setPortName(getMDSInterfacePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setMDSInterfacePortEndpointAddress(java.lang.String address) {
        MDSInterfacePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.huawei.mds.access.webservice.server.bean.MDSInterface.class.isAssignableFrom(serviceEndpointInterface)) {
                com.huawei.mds.access.webservice.server.bean.MDSInterfacePortBindingStub _stub = new com.huawei.mds.access.webservice.server.bean.MDSInterfacePortBindingStub(new java.net.URL(MDSInterfacePort_address), this);
                _stub.setPortName(getMDSInterfacePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("MDSInterfacePort".equals(inputPortName)) {
            return getMDSInterfacePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://huawei.com/mds/access/webservice/server/bean", "MDSInterfaceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://huawei.com/mds/access/webservice/server/bean", "MDSInterfacePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("MDSInterfacePort".equals(portName)) {
            setMDSInterfacePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
