/**
 * RcvWriteBackResultResultMessage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.mds.access.webservice.server.bean;

public class RcvWriteBackResultResultMessage  implements java.io.Serializable {
    private com.huawei.mds.access.webservice.server.bean.WriteBackResultBean messageBody;

    public RcvWriteBackResultResultMessage() {
    }

    public RcvWriteBackResultResultMessage(
           com.huawei.mds.access.webservice.server.bean.WriteBackResultBean messageBody) {
           this.messageBody = messageBody;
    }


    /**
     * Gets the messageBody value for this RcvWriteBackResultResultMessage.
     * 
     * @return messageBody
     */
    public com.huawei.mds.access.webservice.server.bean.WriteBackResultBean getMessageBody() {
        return messageBody;
    }


    /**
     * Sets the messageBody value for this RcvWriteBackResultResultMessage.
     * 
     * @param messageBody
     */
    public void setMessageBody(com.huawei.mds.access.webservice.server.bean.WriteBackResultBean messageBody) {
        this.messageBody = messageBody;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RcvWriteBackResultResultMessage)) return false;
        RcvWriteBackResultResultMessage other = (RcvWriteBackResultResultMessage) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.messageBody==null && other.getMessageBody()==null) || 
             (this.messageBody!=null &&
              this.messageBody.equals(other.getMessageBody())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMessageBody() != null) {
            _hashCode += getMessageBody().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RcvWriteBackResultResultMessage.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://huawei.com/mds/access/webservice/server/bean", "RcvWriteBackResultResultMessage"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("messageBody");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MessageBody"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://huawei.com/mds/access/webservice/server/bean", "WriteBackResultBean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
