/**
 * MDSInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.mds.access.webservice.server.bean;

public interface MDSInterface extends java.rmi.Remote {
    public com.huawei.mds.access.webservice.server.bean.SendRequestResult sendSyncReq(com.huawei.mds.access.webservice.server.bean.SendRequestMsg syncRequestMsg) throws java.rmi.RemoteException;
    public com.huawei.mds.access.webservice.server.bean.SendRequestResult sendAsyncReq(com.huawei.mds.access.webservice.server.bean.SendRequestMsg asyncRequestMsg) throws java.rmi.RemoteException;
    public com.huawei.mds.access.webservice.server.bean.RcvWriteBackResult rcvWriteBack(com.huawei.mds.access.webservice.server.bean.RcvWriteBackMsg writeBackMsg) throws java.rmi.RemoteException;
}
