/**
 * MDSInterfaceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.mds.access.webservice.server.bean;

public interface MDSInterfaceService extends javax.xml.rpc.Service {
    public java.lang.String getMDSInterfacePortAddress();

    public com.huawei.mds.access.webservice.server.bean.MDSInterface getMDSInterfacePort() throws javax.xml.rpc.ServiceException;

    public com.huawei.mds.access.webservice.server.bean.MDSInterface getMDSInterfacePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
