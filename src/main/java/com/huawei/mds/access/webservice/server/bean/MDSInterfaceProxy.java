package com.huawei.mds.access.webservice.server.bean;

public class MDSInterfaceProxy implements com.huawei.mds.access.webservice.server.bean.MDSInterface {
  private String _endpoint = null;
  private com.huawei.mds.access.webservice.server.bean.MDSInterface mDSInterface = null;
  
  public MDSInterfaceProxy() {
    _initMDSInterfaceProxy();
  }
  
  public MDSInterfaceProxy(String endpoint) {
    _endpoint = endpoint;
    _initMDSInterfaceProxy();
  }
  
  private void _initMDSInterfaceProxy() {
    try {
      mDSInterface = (new com.huawei.mds.access.webservice.server.bean.MDSInterfaceServiceLocator()).getMDSInterfacePort();
      if (mDSInterface != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)mDSInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)mDSInterface)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (mDSInterface != null)
      ((javax.xml.rpc.Stub)mDSInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.huawei.mds.access.webservice.server.bean.MDSInterface getMDSInterface() {
    if (mDSInterface == null)
      _initMDSInterfaceProxy();
    return mDSInterface;
  }
  
  public com.huawei.mds.access.webservice.server.bean.SendRequestResult sendSyncReq(com.huawei.mds.access.webservice.server.bean.SendRequestMsg syncRequestMsg) throws java.rmi.RemoteException{
    if (mDSInterface == null)
      _initMDSInterfaceProxy();
    return mDSInterface.sendSyncReq(syncRequestMsg);
  }
  
  public com.huawei.mds.access.webservice.server.bean.SendRequestResult sendAsyncReq(com.huawei.mds.access.webservice.server.bean.SendRequestMsg asyncRequestMsg) throws java.rmi.RemoteException{
    if (mDSInterface == null)
      _initMDSInterfaceProxy();
    return mDSInterface.sendAsyncReq(asyncRequestMsg);
  }
  
  public com.huawei.mds.access.webservice.server.bean.RcvWriteBackResult rcvWriteBack(com.huawei.mds.access.webservice.server.bean.RcvWriteBackMsg writeBackMsg) throws java.rmi.RemoteException{
    if (mDSInterface == null)
      _initMDSInterfaceProxy();
    return mDSInterface.rcvWriteBack(writeBackMsg);
  }
  
  
}