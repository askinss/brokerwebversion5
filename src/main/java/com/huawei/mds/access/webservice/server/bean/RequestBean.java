/**
 * RequestBean.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.mds.access.webservice.server.bean;

public class RequestBean  implements java.io.Serializable {
    private java.lang.String serial;

    private java.lang.String bizCode;

    private java.lang.String tranID;

    private java.lang.String time;

    private java.lang.String pri;

    private java.lang.String timeOut;

    private java.lang.String reservedExeTime;

    private com.huawei.mds.access.webservice.server.bean.ParaBean[] paraList;

    public RequestBean() {
    }

    public RequestBean(
           java.lang.String serial,
           java.lang.String bizCode,
           java.lang.String tranID,
           java.lang.String time,
           java.lang.String pri,
           java.lang.String timeOut,
           java.lang.String reservedExeTime,
           com.huawei.mds.access.webservice.server.bean.ParaBean[] paraList) {
           this.serial = serial;
           this.bizCode = bizCode;
           this.tranID = tranID;
           this.time = time;
           this.pri = pri;
           this.timeOut = timeOut;
           this.reservedExeTime = reservedExeTime;
           this.paraList = paraList;
    }


    /**
     * Gets the serial value for this RequestBean.
     * 
     * @return serial
     */
    public java.lang.String getSerial() {
        return serial;
    }


    /**
     * Sets the serial value for this RequestBean.
     * 
     * @param serial
     */
    public void setSerial(java.lang.String serial) {
        this.serial = serial;
    }


    /**
     * Gets the bizCode value for this RequestBean.
     * 
     * @return bizCode
     */
    public java.lang.String getBizCode() {
        return bizCode;
    }


    /**
     * Sets the bizCode value for this RequestBean.
     * 
     * @param bizCode
     */
    public void setBizCode(java.lang.String bizCode) {
        this.bizCode = bizCode;
    }


    /**
     * Gets the tranID value for this RequestBean.
     * 
     * @return tranID
     */
    public java.lang.String getTranID() {
        return tranID;
    }


    /**
     * Sets the tranID value for this RequestBean.
     * 
     * @param tranID
     */
    public void setTranID(java.lang.String tranID) {
        this.tranID = tranID;
    }


    /**
     * Gets the time value for this RequestBean.
     * 
     * @return time
     */
    public java.lang.String getTime() {
        return time;
    }


    /**
     * Sets the time value for this RequestBean.
     * 
     * @param time
     */
    public void setTime(java.lang.String time) {
        this.time = time;
    }


    /**
     * Gets the pri value for this RequestBean.
     * 
     * @return pri
     */
    public java.lang.String getPri() {
        return pri;
    }


    /**
     * Sets the pri value for this RequestBean.
     * 
     * @param pri
     */
    public void setPri(java.lang.String pri) {
        this.pri = pri;
    }


    /**
     * Gets the timeOut value for this RequestBean.
     * 
     * @return timeOut
     */
    public java.lang.String getTimeOut() {
        return timeOut;
    }


    /**
     * Sets the timeOut value for this RequestBean.
     * 
     * @param timeOut
     */
    public void setTimeOut(java.lang.String timeOut) {
        this.timeOut = timeOut;
    }


    /**
     * Gets the reservedExeTime value for this RequestBean.
     * 
     * @return reservedExeTime
     */
    public java.lang.String getReservedExeTime() {
        return reservedExeTime;
    }


    /**
     * Sets the reservedExeTime value for this RequestBean.
     * 
     * @param reservedExeTime
     */
    public void setReservedExeTime(java.lang.String reservedExeTime) {
        this.reservedExeTime = reservedExeTime;
    }


    /**
     * Gets the paraList value for this RequestBean.
     * 
     * @return paraList
     */
    public com.huawei.mds.access.webservice.server.bean.ParaBean[] getParaList() {
        return paraList;
    }


    /**
     * Sets the paraList value for this RequestBean.
     * 
     * @param paraList
     */
    public void setParaList(com.huawei.mds.access.webservice.server.bean.ParaBean[] paraList) {
        this.paraList = paraList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RequestBean)) return false;
        RequestBean other = (RequestBean) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.serial==null && other.getSerial()==null) || 
             (this.serial!=null &&
              this.serial.equals(other.getSerial()))) &&
            ((this.bizCode==null && other.getBizCode()==null) || 
             (this.bizCode!=null &&
              this.bizCode.equals(other.getBizCode()))) &&
            ((this.tranID==null && other.getTranID()==null) || 
             (this.tranID!=null &&
              this.tranID.equals(other.getTranID()))) &&
            ((this.time==null && other.getTime()==null) || 
             (this.time!=null &&
              this.time.equals(other.getTime()))) &&
            ((this.pri==null && other.getPri()==null) || 
             (this.pri!=null &&
              this.pri.equals(other.getPri()))) &&
            ((this.timeOut==null && other.getTimeOut()==null) || 
             (this.timeOut!=null &&
              this.timeOut.equals(other.getTimeOut()))) &&
            ((this.reservedExeTime==null && other.getReservedExeTime()==null) || 
             (this.reservedExeTime!=null &&
              this.reservedExeTime.equals(other.getReservedExeTime()))) &&
            ((this.paraList==null && other.getParaList()==null) || 
             (this.paraList!=null &&
              java.util.Arrays.equals(this.paraList, other.getParaList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSerial() != null) {
            _hashCode += getSerial().hashCode();
        }
        if (getBizCode() != null) {
            _hashCode += getBizCode().hashCode();
        }
        if (getTranID() != null) {
            _hashCode += getTranID().hashCode();
        }
        if (getTime() != null) {
            _hashCode += getTime().hashCode();
        }
        if (getPri() != null) {
            _hashCode += getPri().hashCode();
        }
        if (getTimeOut() != null) {
            _hashCode += getTimeOut().hashCode();
        }
        if (getReservedExeTime() != null) {
            _hashCode += getReservedExeTime().hashCode();
        }
        if (getParaList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getParaList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getParaList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RequestBean.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://huawei.com/mds/access/webservice/server/bean", "RequestBean"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serial");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Serial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bizCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BizCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tranID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TranID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("time");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Time"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Pri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timeOut");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TimeOut"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reservedExeTime");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ReservedExeTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paraList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ParaList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://huawei.com/mds/access/webservice/server/bean", "ParaBean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "Para"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
