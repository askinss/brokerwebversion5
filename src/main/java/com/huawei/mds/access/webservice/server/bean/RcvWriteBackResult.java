/**
 * RcvWriteBackResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.huawei.mds.access.webservice.server.bean;

public class RcvWriteBackResult  implements java.io.Serializable {
    private com.huawei.mds.access.webservice.server.bean.WriteBackResultBean[] resultMessage;

    public RcvWriteBackResult() {
    }

    public RcvWriteBackResult(
           com.huawei.mds.access.webservice.server.bean.WriteBackResultBean[] resultMessage) {
           this.resultMessage = resultMessage;
    }


    /**
     * Gets the resultMessage value for this RcvWriteBackResult.
     * 
     * @return resultMessage
     */
    public com.huawei.mds.access.webservice.server.bean.WriteBackResultBean[] getResultMessage() {
        return resultMessage;
    }


    /**
     * Sets the resultMessage value for this RcvWriteBackResult.
     * 
     * @param resultMessage
     */
    public void setResultMessage(com.huawei.mds.access.webservice.server.bean.WriteBackResultBean[] resultMessage) {
        this.resultMessage = resultMessage;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RcvWriteBackResult)) return false;
        RcvWriteBackResult other = (RcvWriteBackResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.resultMessage==null && other.getResultMessage()==null) || 
             (this.resultMessage!=null &&
              java.util.Arrays.equals(this.resultMessage, other.getResultMessage())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResultMessage() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResultMessage());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResultMessage(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RcvWriteBackResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://huawei.com/mds/access/webservice/server/bean", "RcvWriteBackResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ResultMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://huawei.com/mds/access/webservice/server/bean", "WriteBackResultBean"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "MessageBody"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
