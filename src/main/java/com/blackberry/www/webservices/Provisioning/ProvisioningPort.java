/**
 * ProvisioningPort.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.blackberry.www.webservices.Provisioning;

public interface ProvisioningPort extends java.rmi.Remote {
    public soap.comm.ari.control.provision.ProvisionReply echoTest(soap.comm.ari.control.provision.ProvisionRequest provisionRequest) throws java.rmi.RemoteException, ari.control.provision.ARIProcessingException;
    public soap.comm.ari.control.provision.ProvisionReply submitSync(soap.comm.ari.control.provision.ProvisionRequest provisionRequest) throws java.rmi.RemoteException, ari.control.provision.ARIProcessingException;
    public void submitAsync(soap.comm.ari.control.provision.ProvisionRequest provisionRequest) throws java.rmi.RemoteException, ari.control.provision.ARIProcessingException;
}
