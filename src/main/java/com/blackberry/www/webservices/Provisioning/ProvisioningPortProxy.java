package com.blackberry.www.webservices.Provisioning;

public class ProvisioningPortProxy implements com.blackberry.www.webservices.Provisioning.ProvisioningPort {
  private String _endpoint = null;
  private com.blackberry.www.webservices.Provisioning.ProvisioningPort provisioningPort = null;
  
  public ProvisioningPortProxy() {
    _initProvisioningPortProxy();
  }
  
  public ProvisioningPortProxy(String endpoint) {
    _endpoint = endpoint;
    _initProvisioningPortProxy();
  }
  
  private void _initProvisioningPortProxy() {
    try {
      provisioningPort = (new com.blackberry.www.webservices.Provisioning.ProvisioningLocator()).getProvisioningPort();
      if (provisioningPort != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)provisioningPort)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)provisioningPort)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (provisioningPort != null)
      ((javax.xml.rpc.Stub)provisioningPort)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.blackberry.www.webservices.Provisioning.ProvisioningPort getProvisioningPort() {
    if (provisioningPort == null)
      _initProvisioningPortProxy();
    return provisioningPort;
  }
  
  public soap.comm.ari.control.provision.ProvisionReply echoTest(soap.comm.ari.control.provision.ProvisionRequest provisionRequest) throws java.rmi.RemoteException, ari.control.provision.ARIProcessingException{
    if (provisioningPort == null)
      _initProvisioningPortProxy();
    return provisioningPort.echoTest(provisionRequest);
  }
  
  public soap.comm.ari.control.provision.ProvisionReply submitSync(soap.comm.ari.control.provision.ProvisionRequest provisionRequest) throws java.rmi.RemoteException, ari.control.provision.ARIProcessingException{
    if (provisioningPort == null)
      _initProvisioningPortProxy();
    return provisioningPort.submitSync(provisionRequest);
  }
  
  public void submitAsync(soap.comm.ari.control.provision.ProvisionRequest provisionRequest) throws java.rmi.RemoteException, ari.control.provision.ARIProcessingException{
    if (provisioningPort == null)
      _initProvisioningPortProxy();
    provisioningPort.submitAsync(provisionRequest);
  }
  
  
}