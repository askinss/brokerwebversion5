/**
 * Provisioning.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.blackberry.www.webservices.Provisioning;

public interface Provisioning extends javax.xml.rpc.Service {
    public java.lang.String getProvisioningPortAddress();

    public com.blackberry.www.webservices.Provisioning.ProvisioningPort getProvisioningPort() throws javax.xml.rpc.ServiceException;

    public com.blackberry.www.webservices.Provisioning.ProvisioningPort getProvisioningPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
