/**
 * ProvisioningLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.blackberry.www.webservices.Provisioning;

public class ProvisioningLocator extends org.apache.axis.client.Service implements com.blackberry.www.webservices.Provisioning.Provisioning {

    public ProvisioningLocator() {
    }


    public ProvisioningLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ProvisioningLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ProvisioningPort
    private java.lang.String ProvisioningPort_address = "https://provisioning.eu.blackberry.com/webservices/Provisioning";

    public java.lang.String getProvisioningPortAddress() {
        return ProvisioningPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ProvisioningPortWSDDServiceName = "ProvisioningPort";

    public java.lang.String getProvisioningPortWSDDServiceName() {
        return ProvisioningPortWSDDServiceName;
    }

    public void setProvisioningPortWSDDServiceName(java.lang.String name) {
        ProvisioningPortWSDDServiceName = name;
    }

    public com.blackberry.www.webservices.Provisioning.ProvisioningPort getProvisioningPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ProvisioningPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getProvisioningPort(endpoint);
    }

    public com.blackberry.www.webservices.Provisioning.ProvisioningPort getProvisioningPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.blackberry.www.webservices.Provisioning.ProvisioningPortStub _stub = new com.blackberry.www.webservices.Provisioning.ProvisioningPortStub(portAddress, this);
            _stub.setPortName(getProvisioningPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setProvisioningPortEndpointAddress(java.lang.String address) {
        ProvisioningPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.blackberry.www.webservices.Provisioning.ProvisioningPort.class.isAssignableFrom(serviceEndpointInterface)) {
                com.blackberry.www.webservices.Provisioning.ProvisioningPortStub _stub = new com.blackberry.www.webservices.Provisioning.ProvisioningPortStub(new java.net.URL(ProvisioningPort_address), this);
                _stub.setPortName(getProvisioningPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ProvisioningPort".equals(inputPortName)) {
            return getProvisioningPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.blackberry.com/webservices/Provisioning", "Provisioning");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.blackberry.com/webservices/Provisioning", "ProvisioningPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ProvisioningPort".equals(portName)) {
            setProvisioningPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
