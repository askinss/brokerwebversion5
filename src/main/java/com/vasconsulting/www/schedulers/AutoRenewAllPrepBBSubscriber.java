/**
 * This class is an auto renewal class that runs every first day of the month and 
 * automatically renews every subscriber on the Post paid BB plan on the local database
 * and on TABS
 * @author nnamdi Jibunoh
 * @Date 12-8-2011
 */
package com.vasconsulting.www.schedulers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.vasconsulting.www.domain.BillingPlan;
import com.vasconsulting.www.domain.ErrorDetail;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.CommandFactory;
import com.vasconsulting.www.invokers.CommandInvoker;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.CommandPropertiesUtility;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.LoadAllBillingPlanObjects;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.SendSmsToKannelService;

public class AutoRenewAllPrepBBSubscriber extends QuartzJobBean implements StatefulJob
{
	private HibernateUtility hibernateUtility = (HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
	private Logger logger = Logger.getLogger(AutoRenewAllPrepBBSubscriber.class);
	private LoadAllBillingPlanObjects billingPlanObjects = (LoadAllBillingPlanObjects)ContextLoaderImpl.getBeans("loadAllBillingPlanObjects");
	private SendSmsToKannelService smsService = (SendSmsToKannelService)ContextLoaderImpl.getBeans("smsService");
	private LoadAllProperties properties = (LoadAllProperties)ContextLoaderImpl.getBeans("loadProperties");
	private EmailTaskExecutor emailExecutor = (EmailTaskExecutor)ContextLoaderImpl.getBeans("myEmailTaskExecutor");
	private CommandFactory commandFactory = new CommandFactory();
	private CommandInvoker commandInvoker;
	
	
	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException
	{
		logger.info("Scheduler AutoRenewAllPrepBBSubscriber started");
		
		Collection<SubscriberDetail> allSubscriberDetails = hibernateUtility.getSubscribersExpiringToday();
		ArrayList<BillingPlanObjectUtility> allBillingPlans = billingPlanObjects.getAllBillingPlans();
		ArrayList<BillingPlan> allBillingPlansDB = hibernateUtility.getAllBillingPlans();
		System.out.println(allBillingPlansDB);
		StringBuilder messageToSend = new StringBuilder(">>>>>>>>>>>>>>> Renewal SUbscribers with issues <<<<<<<<<<<<<<<<<\n\n");
		
		for (BillingPlanObjectUtility billingPlanObjectUtility : allBillingPlans) {
			if (billingPlanObjectUtility.getShortCode().equalsIgnoreCase("renewalprepaidbb")){
				logger.info("Starting the auto renewal scheduler for this hour.");
				
				Iterator<SubscriberDetail> iterator = allSubscriberDetails.iterator();
				
				while ( iterator.hasNext()) {					
					SubscriberDetail subDetails = iterator.next();
					
					logger.info("Processing subscriber with msisdn = "+subDetails.getMsisdn());
					System.out.println("Processing subscriber with msisdn = "+subDetails.getMsisdn());
					
					ArrayList<CommandPropertiesUtility> allCommands = billingPlanObjectUtility.getCoomandObjects();
					
					commandInvoker = new CommandInvoker();
					
					StringBuilder builder = new StringBuilder();
					
					for (BillingPlan billplan : allBillingPlansDB)
					{
						if (billplan.getShortcode().equalsIgnoreCase(subDetails.getShortCode()))
						{
							builder.append(billplan.getCost()+":sep:"+billplan.getServices());
							logger.info("The receiverParam to use for "+subDetails.getMsisdn()+" is "+builder.toString());
						}
					}
					
					for (CommandPropertiesUtility commandPropertiesUtility : allCommands) {
						
						commandInvoker.addCommand(commandFactory.GetCommandInstance(commandPropertiesUtility.getCommand(), 
								"com.vasconsulting.www.interfaces.impl", builder.toString(), subDetails));
					}
					
					int invokerStatus = commandInvoker.provision();
					logger.info("The value returned from the Hourly class is "+invokerStatus);
					if (invokerStatus == 0)
					{
						smsService.sendMessageToKannel(billingPlanObjectUtility.getSuccessMessage(), subDetails.getMsisdn());
					}
					else
					{
						//smsService.sendMessageToKannel(properties.getProperty("renewalfailuremessage"), subDetails.getMsisdn());
						messageToSend.append("Subcriber with msisdn "+subDetails.getMsisdn()+"failed with code "+invokerStatus+"." +
								" Please rectify the subscriber manually.\n");
					}
				}		
				emailExecutor.sendQueuedEmails("Hourly Scheduler has completed for "+allSubscriberDetails.size()+" records  for this hour. ");
				emailExecutor.sendQueuedEmails(messageToSend.toString());
			}
		}	
		
		//TODO 1500: HANDLE UI INTIATED SUBSCRIBERS IN RENEWALS
	}
	
	
}
