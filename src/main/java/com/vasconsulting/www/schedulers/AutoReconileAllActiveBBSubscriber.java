package com.vasconsulting.www.schedulers;

import java.util.ArrayList;
import java.util.Iterator;

import net.rubyeye.xmemcached.MemcachedClient;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.vasconsulting.www.domain.FutureRenewal;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.BrokerService;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.LoadAllBillingPlanObjects;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.SendSmsToKannelService;
import com.vasconsulting.www.utility.StatusCodes;


public class AutoReconileAllActiveBBSubscriber extends QuartzJobBean implements
		StatefulJob {
	public BrokerService getBrokerService() {
		return brokerService;
	}

	public void setBrokerService(BrokerService brokerService) {
		this.brokerService = brokerService;
	}

	private ArrayList<SubscriberDetail> activeSubscribers;
	private Logger logger = Logger
			.getLogger(AutoReconileAllActiveBBSubscriber.class);
	private HibernateUtility hibernateUtility = (HibernateUtility) ContextLoaderImpl
			.getBeans("hibernateUtility");
	private SendSmsToKannelService smsService = new SendSmsToKannelService();
	private LoadAllProperties properties = (LoadAllProperties) ContextLoaderImpl
			.getBeans("loadProperties");
	private EmailTaskExecutor emailExecutor = (EmailTaskExecutor) ContextLoaderImpl
			.getBeans("myEmailTaskExecutor");
	private MemcachedClient memcachedClient = (MemcachedClient) ContextLoaderImpl.getBeans("memcachedClient");
	private BrokerService brokerService;
	private StringBuilder messageToSend;
	private SubscriberDetail subscriberDetail;
	private BillingPlanObjectUtility billingPlanObject;
	private ArrayList<BillingPlanObjectUtility> billingPlans;

	public void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		brokerService = new BrokerService();
		brokerService.setHibernateUtility(hibernateUtility);
		brokerService.setLoadProperties(properties);
		brokerService.setMemcachedClient(memcachedClient);
		brokerService.setMyEmailTaskExecutor(emailExecutor);
		brokerService.setSmsService(smsService);
		LoadAllBillingPlanObjects loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
				.getBeans("loadSchedulerBillingPlanObjects");
		setBrokerService(brokerService);
		logger.info("Starting Auto Reconciliation for Subscribers for "
				+ scheduleJobName());
		activeSubscribers = dueSubscribers();
		Iterator<SubscriberDetail> iterator = activeSubscribers.iterator();
		logger.info("Got " + activeSubscribers.size() + " Subscribers for "
				+ scheduleJobName());
		messageToSend = new StringBuilder(
				">>>>>>>>>>>>>>> Renewal Subscribers with issues <<<<<<<<<<<<<<<<<\n\n");
		while (iterator.hasNext()) {
			subscriberDetail = iterator.next();
			int response = StatusCodes.OTHER_ERRORS;
			brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		//	FutureRenewal futureRenewal = futureRenewalObject(subscriberDetail);
			try {
				logger.info("Processing subscriber with Msisdn: "
						+ subscriberDetail.getMsisdn() + " ,shortcode: "
						+ subscriberDetail.getImsi() + " imsi: ");
			//response = brokerService.scheduleJob(subscriberDetail
					//	.getShortCode() + "recon", subscriberDetail, false);
				
						response  = brokerService.activate("bbrecon", subscriberDetail);
				logger.info("Response from processing subscriber with msisdn: "
						+ subscriberDetail.getMsisdn() + " is " + response);
		//		processResponse(response);
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
		}
		//emailExecutor.sendQueuedEmails("Hourly Scheduler has completed for "
			//	+ activeSubscribers.size() + " records for this hour. ");
		logger.info("Hourly Scheduler has completed for "
				+ activeSubscribers.size() + " records for this hour. ");
		//emailExecutor.sendQueuedEmails(messageToSend.toString());

	}

	private FutureRenewal futureRenewalObject(SubscriberDetail subscriberDetail) {
		FutureRenewal futureRenewal = new FutureRenewal(); 
		ArrayList<FutureRenewal> futureRenewals = hibernateUtility
				.subscriberFutureRenewals(subscriberDetail.getMsisdn());
		logger.info(futureRenewals.size());
		if (!futureRenewals.isEmpty()
				&& properties.getProperty("allowFutureRenewal")
						.equalsIgnoreCase("true")) {
			futureRenewal.setId(futureRenewals.get(0).getId());
			futureRenewal.setMsisdn(futureRenewals.get(0).getMsisdn());
			futureRenewal
					.setPurchasedAt(futureRenewals.get(0).getPurchasedAt());
			futureRenewal.setShortCode(futureRenewals.get(0).getShortCode());
			subscriberDetail.setShortCode(futureRenewals.get(0).getShortCode());
			futureRenewal.setStatus(futureRenewals.get(0).getStatus());
			futureRenewal.setUsedAt(futureRenewals.get(0).getUsedAt());
			return futureRenewal;
		}
		return null;
	}

	private String scheduleJobName() {
		return "Reconcil Job";
	}

	private ArrayList<SubscriberDetail> dueSubscribers() {
		return hibernateUtility.getSubscribersActiveCriterias();
	}
	
	

	public SendSmsToKannelService getSmsService() {
		return smsService;
	}

	public void setSmsService(SendSmsToKannelService smsService) {
		this.smsService = smsService;
	}

	private void processResponse(int response) {
		smsService = (smsService == null) ? (SendSmsToKannelService) ContextLoaderImpl
				.getBeans("smsService") : smsService;

		if (response == StatusCodes.SHORTCODE_NOT_FOUND) {
			messageToSend
					.append("\nThe configuration file for the renewal module is either corrupt or not properly set. Please consult the documentation "
							+ "and then set the details for ("
							+ subscriberDetail.getMsisdn()
							+ ") with shortcode ("
							+ subscriberDetail.getShortCode() + ")\n");

		} else if (response != StatusCodes.SUCCESS) {
			messageToSend.append("Subcriber with msisdn "
					+ subscriberDetail.getMsisdn() + "failed with code "
					+ response + "."
					+ " Please rectify the subscriber manually.\n");
		}

	}

}


