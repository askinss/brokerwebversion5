package com.vasconsulting.www.schedulers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.CommandFactory;
import com.vasconsulting.www.invokers.CommandInvoker;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.CommandPropertiesUtility;
import com.vasconsulting.www.utility.HibernateUtilityImpl;
import com.vasconsulting.www.utility.LoadAllBillingPlanObjects;

public class SynchronizeSubscriberWithTABS extends QuartzJobBean
{
	private HibernateUtility hibernateUtility = new HibernateUtilityImpl();
	private Logger logger = Logger.getLogger(AutoRenewAllBBSubscriber.class);
	private LoadAllBillingPlanObjects billingPlanObjects = new LoadAllBillingPlanObjects();
	private CommandFactory commandFactory = new CommandFactory();
	private CommandInvoker commandInvoker = new CommandInvoker();
	
	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException
	{
		logger.info("Scheduler AutoRenewAllBBSubscriber started");
		
		Collection<SubscriberDetail> allSubscriberDetails = hibernateUtility.loadAllSubscriberDetails();
		ArrayList<BillingPlanObjectUtility> allBillingPlans = billingPlanObjects.getAllBillingPlans();
		
		for (BillingPlanObjectUtility billingPlanObjectUtility : allBillingPlans) {
			if (billingPlanObjectUtility.getShortCode().equalsIgnoreCase("synchronizepostpaidbb")){
				logger.info("Starting the scheduler that runs every other day of the month, to synchronize every subscriber");
				
				for (Iterator<SubscriberDetail> iterator = allSubscriberDetails.iterator(); iterator.hasNext();) {
					SubscriberDetail subDetails = iterator.next();
					ArrayList<CommandPropertiesUtility> allCommands = billingPlanObjectUtility.getCoomandObjects();
					
					for (CommandPropertiesUtility commandPropertiesUtility : allCommands) {
						
						commandInvoker.addCommand(commandFactory.GetCommandInstance(commandPropertiesUtility.getCommand(), 
								"com.vasconsulting.www.interfaces.impl", commandPropertiesUtility.getReceiver(), subDetails));
					}
					
					int invokerStatus = commandInvoker.provision();
					
					/*
					 * TODO: Do something when there is a failure during a renewal of a subscriber.
					 */
				}				
			}
		}			
	}

}
