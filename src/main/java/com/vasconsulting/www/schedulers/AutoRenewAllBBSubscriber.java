/**
 * This class is an auto renewal class that runs every first day of the month and 
 * automatically renews every subscriber on the Post paid BB plan on the local database
 * and on TABS
 * @author nnamdi Jibunoh
 * @Date 12-8-2011
 */
package com.vasconsulting.www.schedulers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.vasconsulting.www.domain.BillingPlan;
import com.vasconsulting.www.domain.ErrorDetail;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.CommandFactory;
import com.vasconsulting.www.invokers.CommandInvoker;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.CommandPropertiesUtility;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.HibernateUtilityImpl;
import com.vasconsulting.www.utility.LoadAllBillingPlanObjects;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.SendSmsToKannelService;
import com.vasconsulting.www.utility.StatusCodes;

public class AutoRenewAllBBSubscriber extends QuartzJobBean
{
	private HibernateUtility hibernateUtility = (HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
	private Logger logger = Logger.getLogger(AutoRenewAllBBSubscriber.class);
	private LoadAllBillingPlanObjects billingPlanObjects = (LoadAllBillingPlanObjects)ContextLoaderImpl.getBeans("loadAllBillingPlanObjects");
	private SendSmsToKannelService smsService = (SendSmsToKannelService)ContextLoaderImpl.getBeans("smsService");
	private LoadAllProperties properties = (LoadAllProperties)ContextLoaderImpl.getBeans("loadProperties");
	private EmailTaskExecutor emailExecutor = (EmailTaskExecutor)ContextLoaderImpl.getBeans("myEmailTaskExecutor");
	
	private CommandFactory commandFactory = new CommandFactory();
	private CommandInvoker commandInvoker;
	
	StringBuilder messageToSend = new StringBuilder(">>>>>>>>>>>>>>> Renewal Subscribers with issues <<<<<<<<<<<<<<<<<\n\n");
	StringBuilder processedNumbers = new StringBuilder();
	boolean didErrorOccur = false;
	
	
	/*@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException
	{
		logger.info("Scheduler AutoRenewAllBBSubscriber started");
		
		Collection<SubscriberDetail> allSubscriberDetails = hibernateUtility.loadAllSubscriberDetails();
		ArrayList<BillingPlanObjectUtility> allBillingPlans = billingPlanObjects.getAllBillingPlans();
		logger.info("Scheduler AutoRenewAllBBSubscriber started again");
		for (BillingPlanObjectUtility billingPlanObjectUtility : allBillingPlans) {
			if (billingPlanObjectUtility.getShortCode().equalsIgnoreCase("renewalpostpaidbb")){
				logger.info("Starting the scheduler that runs every other day of the month, to auto renew/deactivate every subscriber");
				
				Iterator<SubscriberDetail> iterator = allSubscriberDetails.iterator();
				
				while ( iterator.hasNext()) {					
					SubscriberDetail subDetails = iterator.next();
					
					logger.info("Processing subscriber with msisdn = "+subDetails.getMsisdn());
					
					ArrayList<CommandPropertiesUtility> allCommands = billingPlanObjectUtility.getCoomandObjects();
					
					commandInvoker = new CommandInvoker();
					
					for (CommandPropertiesUtility commandPropertiesUtility : allCommands) {
						
						commandInvoker.addCommand(commandFactory.GetCommandInstance(commandPropertiesUtility.getCommand(), 
								"com.vasconsulting.www.interfaces.impl", commandPropertiesUtility.getParam(), subDetails));
					}
					
					int invokerStatus = commandInvoker.provision();
					System.out.println("The status received is = "+invokerStatus);
					
					if(	invokerStatus == StatusCodes.INSUFFICENT_RENEWAL_BALANCE || 
							invokerStatus == StatusCodes.NO_AUTORENEWAL)
					{
						logger.info("Sending "+properties.getProperty(new Integer(invokerStatus).toString())+" to ["+subDetails.getMsisdn()+"]");
						smsService.sendMessageToKannel(properties.getProperty(new Integer(invokerStatus).toString()), subDetails.getMsisdn());
					}
					if (invokerStatus == 0)
					{
						logger.info("Sending "+billingPlanObjectUtility.getSuccessMessage()+" to ["+subDetails.getMsisdn()+"]");
						smsService.sendMessageToKannel(billingPlanObjectUtility.getSuccessMessage(), subDetails.getMsisdn());
					}
					else
					{
						didErrorOccur = true;
						messageToSend.append("Subcriber with msisdn "+subDetails.getMsisdn()+"failed with code "+invokerStatus+"." +
						" Please rectify the subscriber manually.\n");
						
						ErrorDetail errorDetail = new ErrorDetail();
						errorDetail.setDate_created(new GregorianCalendar());
						errorDetail.setDescription("Hourly scheduler failed to process this subscriber");
						errorDetail.setErrorCode(invokerStatus);
						errorDetail.setMsisdn(subDetails.getMsisdn());
						
						hibernateUtility.saveErrorLogs(errorDetail);
					}
				}
				emailExecutor.sendQueuedEmails("Hourly Scheduler completed for\n\n "+allSubscriberDetails.size()+" records  for this hour.");
				if (didErrorOccur)emailExecutor.sendQueuedEmails(messageToSend.toString());
			}
		}			
	}
	*/
	
	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException
	{
		logger.info("Scheduler AutoRenewAllBBSubscriber started");
		
		Collection<SubscriberDetail> allSubscriberDetails = hibernateUtility.getSubscribersExpiringToday();
		ArrayList<BillingPlanObjectUtility> allBillingPlans = billingPlanObjects.getAllBillingPlans();
		ArrayList<BillingPlan> allBillingPlansDB = hibernateUtility.getAllBillingPlans();
		StringBuilder messageToSend = new StringBuilder(">>>>>>>>>>>>>>> Renewal SUbscribers with issues <<<<<<<<<<<<<<<<<\n\n");
		
		for (BillingPlanObjectUtility billingPlanObjectUtility : allBillingPlans) {
			if (billingPlanObjectUtility.getShortCode().equalsIgnoreCase("renewalprepaidbb")){
				logger.info("Starting the auto renewal scheduler for this hour.");
				
				Iterator<SubscriberDetail> iterator = allSubscriberDetails.iterator();
				
				while ( iterator.hasNext()) {					
					SubscriberDetail subDetails = iterator.next();
					
					logger.info("Processing subscriber with msisdn = "+subDetails.getMsisdn());
					
					ArrayList<CommandPropertiesUtility> allCommands = billingPlanObjectUtility.getCoomandObjects();
					
					commandInvoker = new CommandInvoker();
					
					StringBuilder builder = new StringBuilder();
					
					for (BillingPlan billplan : allBillingPlansDB)
					{
						if (billplan.getShortcode().equalsIgnoreCase(subDetails.getShortCode()))
						{
							builder.append(billplan.getCost()+":sep:"+billplan.getServices());
							logger.info("The receiverParam to use for "+subDetails.getMsisdn()+" is "+builder.toString());
						}
					}
					
					for (CommandPropertiesUtility commandPropertiesUtility : allCommands) {
						
						commandInvoker.addCommand(commandFactory.GetCommandInstance(commandPropertiesUtility.getCommand(), 
								"com.vasconsulting.www.interfaces.impl", builder.toString(), subDetails));
					}
					
					int invokerStatus = commandInvoker.provision();
					logger.info("The value returned from the Hourly class is "+invokerStatus);
					if (invokerStatus == 0)
					{
						smsService.sendMessageToKannel(billingPlanObjectUtility.getSuccessMessage(), subDetails.getMsisdn());
					}
					else
					{
						//smsService.sendMessageToKannel(properties.getProperty("renewalfailuremessage"), subDetails.getMsisdn());
						messageToSend.append("Subcriber with msisdn "+subDetails.getMsisdn()+"failed with code "+invokerStatus+"." +
								" Please rectify the subscriber manually.\n");
					}
				}		
				emailExecutor.sendQueuedEmails("Hourly Scheduler has completed for "+allSubscriberDetails.size()+" records  for this hour. ");
				emailExecutor.sendQueuedEmails(messageToSend.toString());
			}
		}	
		
		//TODO 1500: HANDLE UI INTIATED SUBSCRIBERS IN RENEWALS
	}
	
	
	
	/**
	 * This method is used to get the last day of the month.
	 * @param noOfDays
	 * @return
	 */
	private GregorianCalendar getLastDateOfMonth(){
		int year = Calendar.getInstance().get(Calendar.YEAR);
		int month = Calendar.getInstance().get(Calendar.MONTH);
		int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
		
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.set(year, month, day);		
		return calendar;
	}
	
}
