package com.vasconsulting.www.schedulers;

import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.vasconsulting.www.domain.BbTestPromoFirstYes;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.RIMXMLResponseDetail;
import com.vasconsulting.www.utility.RIMXMLUtility;
import com.vasconsulting.www.utility.SendSmsToKannelService;
import com.vasconsulting.www.domain.TransactionLog;


public class DeactivatePromoSubscriber extends QuartzJobBean implements StatefulJob {

	private HibernateUtility hibernateUtility = (HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
	private Logger logger = Logger.getLogger(DeactivatePromoSubscriber.class);
	private SendSmsToKannelService smsService = (SendSmsToKannelService)ContextLoaderImpl.getBeans("smsService");
	private LoadAllProperties properties = (LoadAllProperties)ContextLoaderImpl.getBeans("loadProperties");
	private EmailTaskExecutor emailExecutor = (EmailTaskExecutor)ContextLoaderImpl.getBeans("myEmailTaskExecutor");
	private RIMXMLUtility rimUtility = new RIMXMLUtility();
	private TransactionLog transactionlog = new TransactionLog();

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try{
			Collection<BbTestPromoFirstYes> allPromoSubscriber = hibernateUtility.getFirstYesExpiringToday();
			logger.info("Deactivating Due Promo Subscribers...");
			Iterator<BbTestPromoFirstYes> iterator = allPromoSubscriber.iterator();
			logger.info("Got "+ allPromoSubscriber.size() + " Promo Subscribers to renew");
			while ( iterator.hasNext()) {
				BbTestPromoFirstYes promoSub = iterator.next();
				SubscriberDetail subDetail = new SubscriberDetail();
				subDetail.setImsi(promoSub.getImsi());
				subDetail.setServiceplan(promoSub.getServiceplan());
				subDetail.setServicetype(7);
				transactionlog.setDate_created(new GregorianCalendar());
				transactionlog.setService(promoSub.getServiceplan());
				transactionlog.setMsisdn(promoSub.getMsisdn());
				transactionlog.setShortcode("YES");
				try{
					if (deprovision(subDetail)){
						promoSub.setRimstatus("DEACTIVATED");
						promoSub.setDate_updated(new GregorianCalendar());
						hibernateUtility.updateBBPromoInitialYes(promoSub);
						transactionlog.setId(UUID.randomUUID().toString());
						transactionlog.setStatus("SUCCESSFUL");
						hibernateUtility.saveTransactionlog(transactionlog);
						smsService.sendMessageToKannel(properties.getProperty("endofpromomessage"), promoSub.getMsisdn());
					}else {
						transactionlog.setId(UUID.randomUUID().toString());
						transactionlog.setStatus("FAILED");
						hibernateUtility.saveTransactionlog(transactionlog);
					}
				}catch(Exception e){
					e.printStackTrace();
					continue;
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}


	private boolean deprovision(SubscriberDetail subDetail){
		try {			
			RIMXMLResponseDetail responseData = new RIMXMLResponseDetail();			
			responseData = rimUtility.cancelSubscriptionByIMSI(subDetail);
			int rimRespCode = new Integer(responseData.getErrorCode()).intValue();
			logger.info("Value from calling RIM is = " + rimRespCode);
			if (rimRespCode == 0
					|| (rimRespCode >= 21000 && rimRespCode <= 21500)) {
				subDetail.setStatus("Deactivated");
				return true;
			} 
			else {
				emailExecutor.sendQueuedEmails("Deactivation on RIM failed for: " + subDetail.getMsisdn());
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

}

