/**
 * This class is an auto renewal class that runs every first day of the month and 
 * automatically renews every subscriber on the Post paid BB plan on the local database
 * and on TABS
 * @author nnamdi Jibunoh
 * @Date 12-8-2011
 */
package com.vasconsulting.www.schedulers;

import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.domain.BbTestPromoFirstYes;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.SendSmsToKannelService;

public class NotifyExpiringPromoSubscriber extends QuartzJobBean {
	private HibernateUtility hibernateUtility = (HibernateUtility) ContextLoaderImpl
			.getBeans("hibernateUtility");
	private Logger logger = Logger
			.getLogger(NotifyExpiringPromoSubscriber.class);
	private LoadAllProperties properties = (LoadAllProperties) ContextLoaderImpl
			.getBeans("loadProperties");
	private SendSmsToKannelService smsService = (SendSmsToKannelService) ContextLoaderImpl
			.getBeans("smsService");

	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		logger.info("Scheduler NotifyExpiringPromoSubscriber started");
		String[] daysToNotify = properties.getProperty(
				"daystosendoutpromonotifications").split(",");
		String notificationMessage = properties
				.getProperty("promoexpirationnotificationmessage");
		String messageToBeSent = "";
		logger.info("Value of message template is " + notificationMessage);
		for (String noOfDays : daysToNotify) {
			ArrayList<BbTestPromoFirstYes> allPromoSubscribers = hibernateUtility.getFirstYesExpiringSoon(Integer.valueOf(noOfDays));
			logger.info("Value of returned subscribers is "+ allPromoSubscribers);
			if (allPromoSubscribers != null) {
				for (BbTestPromoFirstYes promoSub : allPromoSubscribers) {
					try{
					messageToBeSent = String.format(notificationMessage,
							Integer.valueOf(noOfDays), promoSub.getDue_date());
					logger.info("Notifiying " + promoSub.getMsisdn()
							+ " of BB renewal with this message "
							+ messageToBeSent);
					smsService.sendMessageToKannel(messageToBeSent,
							promoSub.getMsisdn());
					}catch(Exception e){
						e.printStackTrace();
						continue;
					}
				}
			}
		}

	}

}
