package com.vasconsulting.www.schedulers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.vasconsulting.www.domain.ErrorDetail;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.CommandFactory;
import com.vasconsulting.www.invokers.CommandInvoker;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.CommandPropertiesUtility;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.LoadAllBillingPlanObjects;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.RIMXMLResponseDetail;
import com.vasconsulting.www.utility.RIMXMLUtility;
import com.vasconsulting.www.utility.SendSmsToKannelService;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.utility.StatusCodes;
import com.vasconsulting.www.utility.Tabs6;
import com.vasconsulting.www.utility.XMLUtility;


public class AutoRenewAllPostHomiscoSubscriber extends QuartzJobBean implements StatefulJob {

	private HibernateUtility hibernateUtility = (HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
	private Logger logger = Logger.getLogger(AutoRenewAllPostHomiscoSubscriber.class);
	private LoadAllBillingPlanObjects billingPlanObjects = (LoadAllBillingPlanObjects)ContextLoaderImpl.getBeans("loadAllBillingPlanObjects");
	private SendSmsToKannelService smsService = (SendSmsToKannelService)ContextLoaderImpl.getBeans("smsService");
	private LoadAllProperties properties = (LoadAllProperties)ContextLoaderImpl.getBeans("loadProperties");
	private EmailTaskExecutor emailExecutor = (EmailTaskExecutor)ContextLoaderImpl.getBeans("myEmailTaskExecutor");
	private CommandFactory commandFactory = new CommandFactory();
	private RIMXMLUtility rimUtility = new RIMXMLUtility();
	private CommandInvoker commandInvoker;
	private int provisionStatus = -1;
	private ErrorDetail errorDetail;

	@Override
	protected void executeInternal(JobExecutionContext arg0)

			throws JobExecutionException {
		try{
			Collection<SubscriberDetail> allSubscriberDetails = hibernateUtility.loadAllPostpaidSubscriberDetailsDueForRenewal();
			ArrayList<BillingPlanObjectUtility> allBillingPlans = billingPlanObjects.getAllBillingPlans();
			logger.info("Loaded Billing Plans are: ");
			for (BillingPlanObjectUtility billingPlanObjectUtility : allBillingPlans) {
				logger.info(billingPlanObjectUtility.getShortCode());
			}
			int provisionStatus = -1;
			StringBuilder messageToSend = new StringBuilder(">>>>>>>>>>>>>>> Renewal Subscribers with issues <<<<<<<<<<<<<<<<<\n\n");
			logger.info("Starting Auto Renewal for Subscribers...");
			Iterator<SubscriberDetail> iterator = allSubscriberDetails.iterator();
			logger.info("Got "+ allSubscriberDetails.size() + " Subscribers to renew");
			while ( iterator.hasNext()) {
				SubscriberDetail subDetails = iterator.next();
				logger.info("Processing subscriber with Msisdn: "+subDetails.getMsisdn()+"and shortcode: "+subDetails.getShortCode());
				boolean stringfound = false;
				for (BillingPlanObjectUtility billingPlanObjectUtility : allBillingPlans) {		
					if (billingPlanObjectUtility.getShortCode().equalsIgnoreCase(subDetails.getShortCode().trim() + "sch")){
						stringfound = true;
						logger.info("Processing "+subDetails.getMsisdn());
						BillingPlanObjectUtility billingPlanObjectLive = (BillingPlanObjectUtility)ContextLoaderImpl.getBeans("billingPlanObject");				
						billingPlanObjectLive.setCost(billingPlanObjectUtility.getCost());
						billingPlanObjectLive.setStatus(billingPlanObjectUtility.getStatus());
						billingPlanObjectLive.setDescription(billingPlanObjectUtility.getDescription());
						billingPlanObjectLive.setExternalData(billingPlanObjectUtility.getExternalData());
						billingPlanObjectLive.setShortCode(billingPlanObjectUtility.getShortCode());
						billingPlanObjectLive.setSuccessMessage(billingPlanObjectUtility.getSuccessMessage());
						billingPlanObjectLive.setValidity(billingPlanObjectUtility.getValidity());
						billingPlanObjectLive.setCoomandObjects(billingPlanObjectUtility.getCoomandObjects());	
						ArrayList<CommandPropertiesUtility> commandProperties = billingPlanObjectUtility.getCoomandObjects();
						SubscriberDetail subscriberDetail = (SubscriberDetail)ContextLoaderImpl.getBeans("subscriberDetail");
						subscriberDetail.setId(subDetails.getId());
						subscriberDetail.setMsisdn(stripLeadingMsisdnPrefix(subDetails.getMsisdn()));
						subscriberDetail.setServiceplan(subDetails.getServiceplan());
						subscriberDetail.setDate_created(subDetails.getDate_created());
						subscriberDetail.setLast_subscription_date(subDetails.getLast_subscription_date());
						subscriberDetail.setServicetype(subDetails.getServicetype());
						subscriberDetail.setAutoRenew(subDetails.getAutoRenew());
						subscriberDetail.setNext_subscription_date(subDetails.getNext_subscription_date());
						subscriberDetail.setShortCode(subDetails.getShortCode());
						subscriberDetail.setImsi(subDetails.getImsi());
						subscriberDetail.setPostpaidSubscriber(1);
						subscriberDetail.setPrepaidSubscriber(0);

						commandInvoker = new CommandInvoker();

						for (CommandPropertiesUtility commandProps : commandProperties){
							try{
								commandInvoker.addCommand(commandFactory.GetCommandInstance(commandProps.getCommand(), "com.vasconsulting.www.interfaces.impl", 
										commandProps.getParam()));
							}catch(Exception e){
								e.printStackTrace();
							}
						}			

						provisionStatus = commandInvoker.provision();
						logger.info("Scheduler provisioning request result is "+provisionStatus+" for subscriber with MSISDN = "+subDetails.getMsisdn());
						if (!(provisionStatus == 0)){
							messageToSend.append("Subcriber with msisdn "+subDetails.getMsisdn()+"failed with code "+provisionStatus+"." +
									" Please rectify the subscriber manually.\n");
						}

					}
					if (stringfound) break;
				}
				 if(!stringfound) {
						String errorMessage = "The configuration file for the renewal module is either corrupt or not properly set. Please consult the documentation "
								+ "and then set the details for Prepaid Blackberry renewal.["
								+ subDetails.getMsisdn() + "], please add subscriber shortcode" +
										""+subDetails.getShortCode()+ "sch to the scheduler config";
						logger.error("Error: Error renewal setting for application not set");
						emailExecutor.sendQueuedEmails(errorMessage);						
					}
			}
			emailExecutor.sendQueuedEmails("Hourly Scheduler has completed for "+allSubscriberDetails.size()+" records for this hour. ");
			logger.info("Hourly Scheduler has completed for "+allSubscriberDetails.size()+" records for this hour. ");
			emailExecutor.sendQueuedEmails(messageToSend.toString());
		}catch(Exception e){
			e.printStackTrace();
		}

	}

	private void rollback(SubscriberDetail subDetails){
		ArrayList<BillingPlanObjectUtility> billingPlans = billingPlanObjects.getAllBillingPlans();
		for (BillingPlanObjectUtility billingPlanObjectUtility : billingPlans) {			

			if (billingPlanObjectUtility.getShortCode().equalsIgnoreCase("rollback"))
			{
				logger.info("Rollback started for subscriber "+subDetails.getMsisdn());
				SubscriberDetail subscriberDetail = (SubscriberDetail)ContextLoaderImpl.getBeans("subscriberDetail");
				subscriberDetail.setId(subDetails.getId());
				subscriberDetail.setMsisdn(stripLeadingMsisdnPrefix(subDetails.getMsisdn()));
				subscriberDetail.setServiceplan(subDetails.getServiceplan());
				subscriberDetail.setLast_subscription_date(subDetails.getLast_subscription_date());
				subscriberDetail.setServicetype(subDetails.getServicetype());
				subscriberDetail.setAutoRenew(1);
				subscriberDetail.setNext_subscription_date(subDetails.getNext_subscription_date());
				subscriberDetail.setShortCode(subDetails.getShortCode());
				subscriberDetail.setImsi(subDetails.getImsi());
				subscriberDetail.setPostpaidSubscriber(0);
				subscriberDetail.setPrepaidSubscriber(1);

				GregorianCalendar calendar = new GregorianCalendar();

				logger.info("======Finalized setup=======");

				ArrayList<CommandPropertiesUtility> commandProperties = billingPlanObjectUtility.getCoomandObjects();

				CommandInvoker commandInvoker = new CommandInvoker();

				for (CommandPropertiesUtility commandProps : commandProperties)
				{
					commandInvoker.addCommand(commandFactory.GetCommandInstance(commandProps.getCommand(), "com.vasconsulting.www.interfaces.impl", 
							commandProps.getParam()));						
				}

				provisionStatus = commandInvoker.provision();

				logger.info("Rollback request is "+provisionStatus+" for subscriber with MSISDN = "+subscriberDetail.getMsisdn());				

				if (provisionStatus != StatusCodes.SUCCESS)
				{	
					logger.error("The request to rollback "+subscriberDetail.getMsisdn()+" failed with code "+provisionStatus+". Please check subscriber " +
							"profile and make sure there is no problem.");

					errorDetail = new ErrorDetail();
					errorDetail.setDate_created(calendar);
					errorDetail.setDescription("Rollback Response from SMSHandler controller");
					errorDetail.setErrorCode(provisionStatus);
					errorDetail.setErrorType("");
					errorDetail.setMsisdn(subscriberDetail.getMsisdn());

					try{
						hibernateUtility.saveErrorLogs(errorDetail);
					}
					catch(Exception ex){
						ex.printStackTrace();
					}
				}
			}
		}
		
	}

	private GregorianCalendar getNextSubscriptionDate(int noOfDays){
		GregorianCalendar calendar1 = new GregorianCalendar();
		calendar1.add(GregorianCalendar.DAY_OF_MONTH, noOfDays);
		return calendar1;
	}

	private void deprovision(SubscriberDetail subDetail){
		try {			
			RIMXMLResponseDetail responseData = new RIMXMLResponseDetail();			
			responseData = rimUtility.cancelSubscriptionByIMSI(subDetail);
			int rimRespCode = new Integer(responseData.getErrorCode()).intValue();
			logger.info("Value from calling RIM is = " + rimRespCode);
			if (rimRespCode == 0
					|| (rimRespCode >= 21000 && rimRespCode <= 21500)) {
				subDetail.setStatus("Deactivated");
				hibernateUtility.updateSubscriberDetail(subDetail);
			} 
			else {
				emailExecutor.sendQueuedEmails("Deactivation on RIM failed for: " + subDetail.getMsisdn());
			}
		} catch (Exception e) {
			e.printStackTrace();
			//return StatusCodes.OTHER_ERRORS;
		}

	}

	private String stripLeadingMsisdnPrefix(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("+")){
			return Msisdn.substring(1, Msisdn.length());
		}
		else return msisdn;
	}
}

