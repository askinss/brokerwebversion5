package com.vasconsulting.www.schedulers;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BrokerService;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.LoadAllBillingPlanObjects;
import com.vasconsulting.www.utility.SendSmsToKannelService;
import com.vasconsulting.www.utility.StatusCodes;

public class AutoRenewAllPostpaidSubscriber extends QuartzJobBean implements
		StatefulJob {
	public BrokerService getBrokerService() {
		return brokerService;
	}

	public void setBrokerService(BrokerService brokerService) {
		this.brokerService = brokerService;
	}

	private ArrayList<SubscriberDetail> dueSubscribers;
	private Logger logger = Logger
			.getLogger(AutoRenewAllPostpaidSubscriber.class);
	private HibernateUtility hibernateUtility = (HibernateUtility) ContextLoaderImpl
			.getBeans("hibernateUtility");
	private SendSmsToKannelService smsService;
	private EmailTaskExecutor emailExecutor = (EmailTaskExecutor) ContextLoaderImpl
			.getBeans("myEmailTaskExecutor");
	private BrokerService brokerService;
	private StringBuilder messageToSend;
	private SubscriberDetail subscriberDetail;

	public void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		brokerService = (brokerService == null) ? (BrokerService) ContextLoaderImpl
				.getBeans("brokerService") : brokerService;
		LoadAllBillingPlanObjects loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
				.getBeans("loadSchedulerBillingPlanObjects");
		brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		logger.info("Starting Auto Renewal for Subscribers for "
				+ scheduleJobName());
		Iterator<SubscriberDetail> iterator = dueSubscribers().iterator();
		logger.info("Got " + dueSubscribers.size() + " Subscribers for "
				+ scheduleJobName());
		messageToSend = new StringBuilder(
				">>>>>>>>>>>>>>> Renewal Subscribers with issues <<<<<<<<<<<<<<<<<\n\n");
		while (iterator.hasNext()) {
			subscriberDetail = iterator.next();
			logger.info("Processing subscriber with Msisdn: "
					+ subscriberDetail.getMsisdn() + " ,shortcode: "
					+ subscriberDetail.getShortCode() + " serviceplan: "
					+ subscriberDetail.getServiceplan());
			int response = StatusCodes.OTHER_ERRORS;
			try {
				response = brokerService.scheduleJob(subscriberDetail
						.getShortCode() + "sch", subscriberDetail, false);
				logger.info("Reponse from processing subscriber with msisdn: "
						+ subscriberDetail.getMsisdn() + " is " + response);
				processResponse(response);
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
		}
		emailExecutor.sendQueuedEmails("Hourly Scheduler has completed for "
				+ dueSubscribers.size() + " records for this hour. ");
		logger.info("Hourly Scheduler has completed for "
				+ dueSubscribers.size() + " records for this hour. ");
		emailExecutor.sendQueuedEmails(messageToSend.toString());

	}

	private String scheduleJobName() {
		return "Postpaid Job";
	}

	private ArrayList<SubscriberDetail> dueSubscribers() {
		dueSubscribers = (ArrayList<SubscriberDetail>) hibernateUtility
				.loadAllPostpaidSubscriberDetailsDueForRenewal();
		return dueSubscribers;
	}

	public SendSmsToKannelService getSmsService() {
		return smsService;
	}

	public void setSmsService(SendSmsToKannelService smsService) {
		this.smsService = smsService;
	}

	private void processResponse(int response) {
		smsService = (smsService == null) ? (SendSmsToKannelService) ContextLoaderImpl
				.getBeans("smsService") : smsService;

		if (response == StatusCodes.SHORTCODE_NOT_FOUND) {
			messageToSend
					.append("\nThe configuration file for the renewal module is either corrupt or not properly set. Please consult the documentation "
							+ "and then set the details for ("
							+ subscriberDetail.getMsisdn()
							+ ") with shortcode ("
							+ subscriberDetail.getShortCode() + ")\n");

		} else if (response != StatusCodes.SUCCESS) {
			messageToSend.append("Subcriber with msisdn "
					+ subscriberDetail.getMsisdn() + "failed with code "
					+ response + "."
					+ " Please rectify the subscriber manually.\n");
		}

	}

}
