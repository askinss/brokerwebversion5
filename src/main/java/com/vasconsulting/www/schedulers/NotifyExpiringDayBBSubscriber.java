/**
 * This class is an auto renewal class that runs every first day of the month and 
 * automatically renews every subscriber on the Post paid BB plan on the local database
 * and on TABS
 * @author nnamdi Jibunoh
 * @Date 12-8-2011
 */
package com.vasconsulting.www.schedulers;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.domain.BillingPlan;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.SendSmsToKannelService;

public class NotifyExpiringDayBBSubscriber extends QuartzJobBean
{
	private HibernateUtility hibernateUtility = (HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
	private Logger logger = Logger.getLogger(NotifyExpiringDayBBSubscriber.class);
		
	private LoadAllProperties properties = (LoadAllProperties)ContextLoaderImpl.getBeans("loadProperties");
	private SendSmsToKannelService smsService = (SendSmsToKannelService)ContextLoaderImpl.getBeans("smsService");
	
		
	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException
	{
		logger.info("Scheduler NotifyExpiringDayBBSubscriber started");
		
		ArrayList<BillingPlan> allBillingPlansDB = hibernateUtility.getAllBillingPlans();
		String notificationMessage = properties.getProperty("renewaldaynotificationmessage");
		String messageToBeSent = "";
		
		logger.info("Value of message template is "+notificationMessage);
		
		ArrayList<SubscriberDetail> allSubscribers = hibernateUtility.getDaySubscribersExpiringSoon();
		logger.info("Value of returned subscribers is "+allSubscribers);
		if (allSubscribers != null)
		{
			for(SubscriberDetail subscriberDetail : allSubscribers)
			{
				for(BillingPlan billPlans : allBillingPlansDB)
				{
					if (billPlans.getShortcode().equalsIgnoreCase(subscriberDetail.getShortCode().trim()))
					{
						messageToBeSent = String.format(notificationMessage, 
								convertServiceToDisplayValue(subscriberDetail.getServiceplan()));
						
						logger.info("Notifiying "+subscriberDetail.getMsisdn()+" of BB renewal with this message "+messageToBeSent);
						
						smsService.sendMessageToKannel(messageToBeSent, subscriberDetail.getMsisdn());
					}
				}
			}
		}		
	
	}	
	
	private String convertServiceToDisplayValue(String serviceType){
		if (serviceType.trim().equalsIgnoreCase("Prepaid BIS Lite")) return "Messaging";
		else if (serviceType.trim().equalsIgnoreCase("Prepaid BIS Social")) return "Social";
		else if (serviceType.trim().equalsIgnoreCase("Prepaid Prosumer")) return "BIS";
		else if (serviceType.trim().equalsIgnoreCase("Prepaid Complete")) return "Complete";
		else return "BES";
		
	}
}
