/**
 * This class is an auto renewal class that runs every first day of the month and 
 * automatically renews every subscriber on the Post paid BB plan on the local database
 * and on TABS
 * @author nnamdi Jibunoh
 * @Date 12-8-2011
 */
package com.vasconsulting.www.schedulers;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.domain.BillingPlan;
import com.vasconsulting.www.domain.ErrorDetail;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.SendSmsToKannelService;

public class NotifyBBErrorsToAdministrator extends QuartzJobBean
{
	private HibernateUtility hibernateUtility = (HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
	private Logger logger = Logger.getLogger(NotifyBBErrorsToAdministrator.class);
		
	private EmailTaskExecutor myEmailTaskExecutor = (EmailTaskExecutor)ContextLoaderImpl.getBeans("myEmailTaskExecutor");
	
		
	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException
	{
		logger.info("Scheduler NotifyBBErrorsToAdministrator started");
		
		ArrayList<ErrorDetail> allBBErrors = hibernateUtility.getAllErrorLogs();
		
		StringBuilder messageToBeSent = new StringBuilder(">>>>>>>>>>>>>>>>>>>>>>>>>>>> Hourly Error report <<<<<<<<<<<<<<<<<<<<<<<<\n\n");
				
		for (ErrorDetail errorDetails : allBBErrors)
		{
			messageToBeSent.append(errorDetails.getDescription()+" for "+errorDetails.getMsisdn()+
					" exited with code "+errorDetails.getErrorCode()+"["+errorDetails.getErrorType()+"]\n");
			
			hibernateUtility.deleteErrorLogs(errorDetails);
		}
		myEmailTaskExecutor.sendQueuedEmails(messageToBeSent.toString());
		
	}		
}
