/**
 * This class is an auto renewal class that runs every first day of the month and 
 * automatically renews every subscriber on the Post paid BB plan on the local database
 * and on TABS
 * @author nnamdi Jibunoh
 * @Date 12-8-2011
 */
package com.vasconsulting.www.schedulers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.CommandFactory;
import com.vasconsulting.www.invokers.CommandInvoker;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.CommandPropertiesUtility;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.HibernateUtilityImpl;
import com.vasconsulting.www.utility.LoadAllBillingPlanObjects;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.SendSmsToKannelService;

public class AutoExtendAllBBSubscriber extends QuartzJobBean
{
	private HibernateUtility hibernateUtility = (HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
	private SendSmsToKannelService smsService = (SendSmsToKannelService)ContextLoaderImpl.getBeans("smsService");
	private Logger logger = Logger.getLogger(AutoExtendAllBBSubscriber.class);
	private LoadAllBillingPlanObjects billingPlanObjects = (LoadAllBillingPlanObjects)ContextLoaderImpl.getBeans("loadAllBillingPlanObjects");
	private LoadAllProperties properties = (LoadAllProperties)ContextLoaderImpl.getBeans("loadProperties");
	
	private CommandFactory commandFactory = new CommandFactory();
	private CommandInvoker commandInvoker;
	
	
	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException
	{
		logger.info("Scheduler AutoExtendAllBBSubscriber started");
		
		Collection<SubscriberDetail> allSubscriberDetails = hibernateUtility.loadAllSubscriberDetails();
		ArrayList<BillingPlanObjectUtility> allBillingPlans = billingPlanObjects.getAllBillingPlans();
		
		logger.info("The value of billingplans is = "+allBillingPlans);
		
		for (BillingPlanObjectUtility billingPlanObjectUtility : allBillingPlans) {
			if (billingPlanObjectUtility.getShortCode().equalsIgnoreCase("autorenewalpostpaidbb")){
				logger.info("Starting the scheduler that runs every first day of the month, to auto renew every subscriber");
				
				Iterator<SubscriberDetail> iterator = allSubscriberDetails.iterator();
				
				while ( iterator.hasNext()) {					
					SubscriberDetail subDetails = iterator.next();
					
					logger.info("Processing subscriber with msisdn = "+subDetails.getMsisdn());
					
					ArrayList<CommandPropertiesUtility> allCommands = billingPlanObjectUtility.getCoomandObjects();
					
					commandInvoker = new CommandInvoker();
					
					for (CommandPropertiesUtility commandPropertiesUtility : allCommands) {
						
						commandInvoker.addCommand(commandFactory.GetCommandInstance(commandPropertiesUtility.getCommand(), 
								"com.vasconsulting.www.interfaces.impl", commandPropertiesUtility.getParam(), subDetails));
					}
					
					int invokerStatus = commandInvoker.provision();
					
					if (invokerStatus == 0)
					{
						smsService.sendMessageToKannel(billingPlanObjectUtility.getSuccessMessage(), subDetails.getMsisdn());
					}
					else
					{
						smsService.sendMessageToKannel(properties.getProperty("renewalfailuremessage"), subDetails.getMsisdn());
					}
					
				}				
			}
		}			
	}
	
	/**
	 * This method is used to get the last day of the month.
	 * @param noOfDays
	 * @return
	 */
	private GregorianCalendar getLastDateOfMonth(){
		int year = Calendar.getInstance().get(Calendar.YEAR);
		int month = Calendar.getInstance().get(Calendar.MONTH);
		int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
		
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.set(year, month, day);		
		return calendar;
	}
	
}
