/**
 * This class is an auto renewal class that runs every first day of the month and 
 * automatically renews every subscriber on the Post paid BB plan on the local database
 * and on TABS
 * @author nnamdi Jibunoh
 * @Date 12-8-2011
 */
package com.vasconsulting.www.schedulers;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.vasconsulting.www.domain.BillingPlan;
import com.vasconsulting.www.domain.CDRSubscriptionDetail;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.OCSSubscriberUtility;
import com.vasconsulting.www.utility.SendSmsToKannelService;

public class GenerateAllBBCDRDetails extends QuartzJobBean implements StatefulJob
{
	private HibernateUtility hibernateUtility = (HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
	private Logger logger = Logger.getLogger(AutoRenewAllBBSubscriber.class);	
	private EmailTaskExecutor emailExecutor = (EmailTaskExecutor)ContextLoaderImpl.getBeans("myEmailTaskExecutor");
	private SendSmsToKannelService smsService = new SendSmsToKannelService();
	private LoadAllProperties properties = new LoadAllProperties();
	private OCSSubscriberUtility ocsUtility = new OCSSubscriberUtility();
	
	
	
	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException
	{
		logger.info("Scheduler GenerateAllBBCDRDetails started");
		
		ArrayList<CDRSubscriptionDetail> allCDRDetails = hibernateUtility.getAllSubscriptionCDR();
		
		StringBuilder messageToSendAirtel = new StringBuilder("");
		StringBuilder messageToSendWarid = new StringBuilder("");
		int counter = 0;
		
		logger.info("About to generate the CDR for a total of "+allCDRDetails.size()+" subscribers.");
		for (CDRSubscriptionDetail cdrDetail : allCDRDetails) 
		{
			if (ocsUtility.isSubscriberFromAirtel(cdrDetail.getMsisdn()))
			{
				String fullMessage = "05"+","+
					System.currentTimeMillis()+","+
					cdrDetail.getMsisdn()+","+
					cdrDetail.getImsi()+","+
					"2,"+
					String.format("%1$tY-%1$tm-%1$td",cdrDetail.getDate_created())+","+
					String.format("%1$tH:%1$tM:%1$tS",cdrDetail.getDate_created())+","+
					cdrDetail.getApnid()+","+
					"SUCCESS"+","+
					"0"+","+
					cdrDetail.getAmount()+","+
					","+
					0+"\n";			
				
				logger.info("Adding this CDR record to file "+fullMessage);
				
				try{
					cdrDetail.setStatus("PROCESSED");
					int status = hibernateUtility.updateSubscriptionCDR(cdrDetail);
					
					if (status == 0)messageToSendAirtel.append(fullMessage);
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
			else
			{				
				String fullMessage = counter++ +"|"+
						cdrDetail.getMsisdn()+"|"+
						"0|"+
						String.format("%1$tY%1$tm%1$td%1$tH%1$tM%1$tS",cdrDetail.getDate_created())+"|"+
						cdrDetail.getAmount()+"|"+
						"0\n";			
					
					logger.info("Adding this CDR record to file "+fullMessage);
					
					try{
						cdrDetail.setStatus("PROCESSED");
						int status = hibernateUtility.updateSubscriptionCDR(cdrDetail);
						
						if (status == 0)messageToSendWarid.append(fullMessage);
					}
					catch(Exception ex)
					{
						ex.printStackTrace();
					}
			}
		}	
		
		if (messageToSendAirtel.length() != 0)
		{
			try
			{
				FileWriter fstream = new FileWriter("/home/bblite/cdrs/"+
						String.format("%1$td_%1$tb_%1$tY_%1$tH_%1$tM_%1$tS",new GregorianCalendar())+".cdr");
				BufferedWriter out = new BufferedWriter(fstream);
				
				out.write(messageToSendAirtel.toString());
				  
				out.close();
			}
			catch(Exception ex)
			{
				smsService.sendMessageToKannel("Postpaid CDR file write error has occured, please check email or " +
						"search the logs for affected numbers.", properties.getProperty("adminMsisdn"));
				String errorMessage = "*********CRITICAL********\n" +
						"These Postpaid numbers have not being written to a cdr file. Please copy the following and create" +
						" a .cdr file on the Broker server at the path /home/bblite/cdrs/ \n\n"+messageToSendAirtel;
				logger.error(errorMessage);
				emailExecutor.sendQueuedEmails(errorMessage);
			}
		}
		if (messageToSendWarid.length() != 0)
		{
			try
			{
				FileWriter fstream = new FileWriter("/home/bblite/cdrs/warid/"+
						String.format("%1$td_%1$tb_%1$tY_%1$tH_%1$tM_%1$tS",new GregorianCalendar())+".cdr");
				BufferedWriter out = new BufferedWriter(fstream);
				
				out.write(messageToSendWarid.toString());
				  
				out.close();
			}
			catch(Exception ex)
			{
				smsService.sendMessageToKannel("Postpaid CDR file write error has occured, please check email or " +
						"search the logs for affected numbers.", properties.getProperty("adminMsisdn"));
				String errorMessage = "*********CRITICAL********\n" +
						"These Postpaid numbers have not being written to a cdr file. Please copy the following and create" +
						" a .cdr file on the Broker server at the path /home/bblite/cdrs/warid \n\n"+messageToSendWarid;
				logger.error(errorMessage);
				emailExecutor.sendQueuedEmails(errorMessage);
			}
		}
	}	
}
