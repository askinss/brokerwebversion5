/**
 * This class is an auto renewal class that runs every first day of the month and 
 * automatically renews every subscriber on the Post paid BB plan on the local database
 * and on TABS
 * @author nnamdi Jibunoh
 * @Date 12-8-2011
 */
package com.vasconsulting.www.schedulers;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.domain.BillingPlan;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.SendSmsToKannelService;

public class NotifyExpiringBBSubscriber extends QuartzJobBean
{
	private HibernateUtility hibernateUtility = (HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
	private Logger logger = Logger.getLogger(NotifyExpiringBBSubscriber.class);
		
	private LoadAllProperties properties = (LoadAllProperties)ContextLoaderImpl.getBeans("loadProperties");
	private SendSmsToKannelService smsService = (SendSmsToKannelService)ContextLoaderImpl.getBeans("smsService");
	
		
	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException
	{
		logger.info("Scheduler NotifyAllPrepBBSubscriber started");
		
		String [] daysToNotify = properties.getProperty("daystosendoutsmsnotifications").split(",");
		ArrayList<BillingPlan> allBillingPlansDB = hibernateUtility.getAllBillingPlans();
		String notificationMessage = properties.getProperty("renewalnotificationmessage");
		String messageToBeSent = "";
		
		logger.info("Value of message template is "+notificationMessage);
		
		for (String noOfDays : daysToNotify)
		{
			ArrayList<SubscriberDetail> allSubscribers = hibernateUtility.getSubscribersExpiringSoon(new Integer(noOfDays));
			logger.info("Value of returned subscribers is "+allSubscribers);
			if (allSubscribers != null)
			{
				for(SubscriberDetail subscriberDetail : allSubscribers)
				{
					if (Arrays.asList(properties.getProperty("nosmstoshortcode").split(",")).contains(subscriberDetail.getShortCode())) continue;
					for(BillingPlan billPlans : allBillingPlansDB)
					{
						if (billPlans.getShortcode().equalsIgnoreCase(subscriberDetail.getShortCode().trim()))
						{
							messageToBeSent = String.format(notificationMessage, 
									convertServiceToDisplayValue(subscriberDetail.getServiceplan()), noOfDays, 
									billPlans.getCost(), subscriberDetail.getNext_subscription_date());
							
							logger.info("Notifiying "+subscriberDetail.getMsisdn()+" of BB renewal with this message "+messageToBeSent);
							
							smsService.sendMessageToKannel(messageToBeSent, subscriberDetail.getMsisdn());
						}
					}
				}
			}
		}		
	
	}	
	
	private String convertServiceToDisplayValue(String serviceType){
		if (serviceType.trim().equalsIgnoreCase("Prepaid BIS Lite")) return "Messaging";
		else if (serviceType.trim().equalsIgnoreCase("Prepaid BIS Social")) return "Social";
		else if (serviceType.trim().equalsIgnoreCase("Prosumer B")) return "BIS";
		else if (serviceType.trim().equalsIgnoreCase("Prepaid Complete")) return "Complete";
		else if (serviceType.trim().equalsIgnoreCase("Prepaid Social Plan")) return "Complete Social";
		else return "BES";
		
	}
}
