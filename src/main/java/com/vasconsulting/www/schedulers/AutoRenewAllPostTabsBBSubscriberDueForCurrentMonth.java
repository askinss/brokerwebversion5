package com.vasconsulting.www.schedulers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.CommandFactory;
import com.vasconsulting.www.invokers.CommandInvoker;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.CommandPropertiesUtility;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.LoadAllBillingPlanObjects;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.RIMXMLResponseDetail;
import com.vasconsulting.www.utility.RIMXMLUtility;
import com.vasconsulting.www.utility.SendSmsToKannelService;
import com.vasconsulting.www.utility.StatusCodes;
import com.vasconsulting.www.utility.Tabs6;
import com.vasconsulting.www.utility.XMLUtility;

public class AutoRenewAllPostTabsBBSubscriberDueForCurrentMonth extends QuartzJobBean implements StatefulJob {
	
	private HibernateUtility hibernateUtility = (HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
	private Logger logger = Logger.getLogger(AutoRenewAllPostTabsBBSubscriberDueForCurrentMonth.class);
	private LoadAllBillingPlanObjects billingPlanObjects = (LoadAllBillingPlanObjects)ContextLoaderImpl.getBeans("loadAllBillingPlanObjects");
	private SendSmsToKannelService smsService = (SendSmsToKannelService)ContextLoaderImpl.getBeans("smsService");
	private LoadAllProperties properties = (LoadAllProperties)ContextLoaderImpl.getBeans("loadProperties");
	private EmailTaskExecutor emailExecutor = (EmailTaskExecutor)ContextLoaderImpl.getBeans("myEmailTaskExecutor");
	private CommandFactory commandFactory = new CommandFactory();
	private RIMXMLUtility rimUtility = new RIMXMLUtility();
	private XMLUtility xmlUtility = new XMLUtility();
	private CommandInvoker commandInvoker;

	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		try{
		Collection<SubscriberDetail> allPostSubscriberDetails = hibernateUtility.getPostpaidSubscribersDueForCurrentMonth();
		ArrayList<BillingPlanObjectUtility> allBillingPlans = billingPlanObjects.getAllBillingPlans();
		int provisionStatus = -1;
		StringBuilder messageToSend = new StringBuilder(">>>>>>>>>>>>>>> Renewal PostPaid Subscribers with issues <<<<<<<<<<<<<<<<<\n\n");
		for (BillingPlanObjectUtility billingPlanObjectUtility : allBillingPlans) {			
			if (billingPlanObjectUtility.getShortCode().equalsIgnoreCase("renewpostbb")){
				logger.info("Starting Auto Renewal for Postpaid subscribers...");
				Iterator<SubscriberDetail> iterator = allPostSubscriberDetails.iterator();
				logger.info("Got "+ allPostSubscriberDetails.size() + " Postpaid subscribers to renew");
				while ( iterator.hasNext()) {	
					
					SubscriberDetail subDetails = iterator.next();
					logger.info("Processing "+subDetails.getMsisdn());
					
					BillingPlanObjectUtility billingPlanObjectLive = (BillingPlanObjectUtility)ContextLoaderImpl.getBeans("billingPlanObject");				
					billingPlanObjectLive.setCost(billingPlanObjectUtility.getCost());
					billingPlanObjectLive.setStatus(billingPlanObjectUtility.getStatus());
					billingPlanObjectLive.setDescription(billingPlanObjectUtility.getDescription());
					billingPlanObjectLive.setExternalData(billingPlanObjectUtility.getExternalData());
					billingPlanObjectLive.setShortCode(billingPlanObjectUtility.getShortCode());
					billingPlanObjectLive.setSuccessMessage(billingPlanObjectUtility.getSuccessMessage());
					billingPlanObjectLive.setValidity(billingPlanObjectUtility.getValidity());
					billingPlanObjectLive.setCoomandObjects(billingPlanObjectUtility.getCoomandObjects());					
	
					ArrayList<CommandPropertiesUtility> commandProperties = billingPlanObjectUtility.getCoomandObjects();
					SubscriberDetail subscriberDetail = (SubscriberDetail)ContextLoaderImpl.getBeans("subscriberDetail");
					subscriberDetail.setId(subDetails.getId());
					subscriberDetail.setMsisdn(stripLeadingMsisdnPrefix(subDetails.getMsisdn()));
					subscriberDetail.setServiceplan(subDetails.getServiceplan());
					subscriberDetail.setLast_subscription_date(new GregorianCalendar());
					subscriberDetail.setServicetype(new Integer(billingPlanObjectUtility.getValidity()).intValue());
					subscriberDetail.setAutoRenew(1);
					subscriberDetail.setNext_subscription_date(getNextSubscriptionDate(new Integer(subDetails.getServicetype()).intValue()));
					subscriberDetail.setShortCode(subDetails.getShortCode());
					subscriberDetail.setImsi(subDetails.getImsi());
					subscriberDetail.setPostpaidSubscriber(1);
					subscriberDetail.setPrepaidSubscriber(0);
					subscriberDetail.setFirstname(subDetails.getFirstname());
					subscriberDetail.setLastname(subDetails.getLastname());
	
					commandInvoker = new CommandInvoker();

					for (CommandPropertiesUtility commandProps : commandProperties){
						try{
						commandInvoker.addCommand(commandFactory.GetCommandInstance(commandProps.getCommand(), "com.vasconsulting.www.interfaces.impl", 
								commandProps.getParam()));
						}catch(Exception e){
							e.printStackTrace();
						}
					}			

					provisionStatus = commandInvoker.provision();
					logger.info("Scheduler provisioning request result is "+provisionStatus+" for subscriber with MSISDN = "+subDetails.getMsisdn());
					if (provisionStatus == 0){
						smsService.sendMessageToKannel(billingPlanObjectUtility.getSuccessMessage(), subDetails.getMsisdn());
					}else if (provisionStatus == StatusCodes.SUBSCRIBER_STATE_OUT_OF_SYNC || provisionStatus == StatusCodes.SUBSCRIBER_STATE_OUT_OF_SYNC) continue;
					else
					{
						deprovision(subDetails);
						smsService.sendMessageToKannel(properties.getProperty("renewalfailuremessage"), subDetails.getMsisdn());
						messageToSend.append("Subcriber with msisdn "+subDetails.getMsisdn()+"failed with code "+provisionStatus+"." +
								" Please rectify the subscriber manually.\n");
					}
					emailExecutor.sendQueuedEmails("Hourly Scheduler has completed for "+allPostSubscriberDetails.size()+" Postpaid records for this hour. ");
					emailExecutor.sendQueuedEmails(messageToSend.toString());

				}
			}
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	private GregorianCalendar getNextSubscriptionDate(int noOfDays){
		GregorianCalendar calendar1 = new GregorianCalendar();
		calendar1.add(GregorianCalendar.DAY_OF_MONTH, noOfDays);
		return calendar1;
	}
	
	private void deprovision(SubscriberDetail subDetail){
		try {			
			RIMXMLResponseDetail responseData = new RIMXMLResponseDetail();			
			responseData = rimUtility.cancelSubscriptionByIMSI(subDetail);
			int rimRespCode = new Integer(responseData.getErrorCode()).intValue();
			logger.info("Value from calling RIM is = " + rimRespCode);
			if (rimRespCode == 0
					|| (rimRespCode >= 21000 && rimRespCode <= 21500)) {
				
				//Delete susbcriber from TABS if subscriber is a postpaid subscriber
					Tabs6 tabs = new Tabs6();
					//subscriber shortcode is stored against TABS equipID in the properties file
					try{ 
						String tabsResponse = tabs.deleteService(stripLeadingMsisdnPrefix(subDetail.getMsisdn()), properties.getProperty(subDetail.getShortCode()));
						logger.info("Response XML from tabs is: "+tabsResponse);
						String responseCode = xmlUtility.getChangeServiceResponseCode(tabsResponse);
						logger.info("Response code from TABS for deactivation is: " +responseCode);
					}catch(Exception e){
						e.printStackTrace();
						//return StatusCodes.OTHER_ERRORS_CHANGE_SERVICE;
					}
				subDetail.setStatus("Deactivated");
				hibernateUtility.updateSubscriberDetail(subDetail);
			} 
			else {
				emailExecutor.sendQueuedEmails("Deactivation on RIM failed for: " + subDetail.getMsisdn());
			}
		} catch (Exception e) {
			e.printStackTrace();
			//return StatusCodes.OTHER_ERRORS;
		}
		
	}
	
	private String stripLeadingMsisdnPrefix(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("+")){
			return Msisdn.substring(1, Msisdn.length());
		}
		else return msisdn;
	}
}

