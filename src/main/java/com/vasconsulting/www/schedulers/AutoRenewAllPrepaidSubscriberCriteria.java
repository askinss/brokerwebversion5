package com.vasconsulting.www.schedulers;

import java.util.ArrayList;
import java.util.Iterator;


import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;

public class AutoRenewAllPrepaidSubscriberCriteria extends QuartzJobBean implements
		StatefulJob {
	
	private ArrayList<SubscriberDetail> dueSubscribers;
	private Logger logger = Logger
			.getLogger(AutoRenewAllPrepaidSubscriberCriteria.class);
	private HibernateUtility hibernateUtility = (HibernateUtility) ContextLoaderImpl
			.getBeans("hibernateUtility");
	
	private SubscriberDetail subscriberDetail;

	public void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		
		logger.info("Starting Auto Renewal for Subscribers for "
				+ scheduleJobName());
		dueSubscribers = dueSubscribers();
		Iterator<SubscriberDetail> iterator = dueSubscribers.iterator();
		logger.info("Got " + dueSubscribers.size() + " Subscribers for "
				+ scheduleJobName());
		
		while (iterator.hasNext()) {
			subscriberDetail = iterator.next();
			logger.info("processing "+subscriberDetail);
		}
		logger.info("Got " + dueSubscribers.size() + " Subscribers for "
				+ scheduleJobName());
	}

	

	private String scheduleJobName() {
		return "Prepaid Job";
	}

	private ArrayList<SubscriberDetail> dueSubscribers() {
		//return hibernateUtility.getSubscribersExpiringToday();
		return hibernateUtility.getSubscribersExpiringTodayWithCriterias();
	}

	
	

}
