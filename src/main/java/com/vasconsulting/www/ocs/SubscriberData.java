package com.vasconsulting.www.ocs;

import java.util.ArrayList;

/**
 *
 * @author Nnamdi
 */
public class SubscriberData {

    private ArrayList<String> subscriber;

    public SubscriberData(ArrayList<String> subscriber) {
        this.subscriber = subscriber;
    }

    public SubscriberData() {
    }

    public ArrayList<String> getSubscriberDataList() {
        return subscriber;
    }

    public void setSubscriberDataList(ArrayList<String> subscriber) {
        this.subscriber = subscriber;
    }
}