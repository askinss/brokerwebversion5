package com.vasconsulting.www.ocs;

public class BasicInfo {

    private String imsi;
    private String msisdn;
    private String icsIndicator;
    private String cardType;
    private String nam;
    private String category;
    private String subAge;
    private String userCategory;
    private String HLRSN;
    private String anchorIndicator;

    public BasicInfo() {
        this.imsi=null;
    }

    public String getImsi() {
        return imsi;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public String getIcsIndicator() {
        return icsIndicator;
    }

    public String getCardType() {
        return cardType;
    }

    public String getNam() {
        return nam;
    }

    public String getCategory() {
        return category;
    }

    public String getSubAge() {
        return subAge;
    }

    public String getUserCategory() {
        return userCategory;
    }

    public String getHLRSN() {
        return HLRSN;
    }

    public String getAnchorIndicator() {
        return anchorIndicator;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public void setIcsIndicator(String icsIndicator) {
        this.icsIndicator = icsIndicator;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public void setNam(String nam) {
        this.nam = nam;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setSubAge(String subAge) {
        this.subAge = subAge;
    }

    public void setUserCategory(String userCategory) {
        this.userCategory = userCategory;
    }

    public void setHLRSN(String HLRSN) {
        this.HLRSN = HLRSN;
    }

    public void setAnchorIndicator(String anchorIndicator) {
        this.anchorIndicator = anchorIndicator;
    }

}
