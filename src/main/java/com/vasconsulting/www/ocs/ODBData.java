package com.vasconsulting.www.ocs;

import java.util.HashMap;

/**
 *
 * @author Nnamdi
 */
public class ODBData {

    private HashMap<String, String> odbInfo;

    public ODBData() {
    }

    public void setOdbInfo(HashMap<String, String> odbInfo) {
        this.odbInfo = odbInfo;
    }

    public HashMap<String, String> getOdbInfo() {
        return odbInfo;
    }

    @Override
    public String toString() {
        return odbInfo.toString();
    }

}
