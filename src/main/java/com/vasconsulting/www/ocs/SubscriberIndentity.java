package com.vasconsulting.www.ocs;

/**
 *
 * @author BENJI IBM
 */
public class SubscriberIndentity {

    private String msidn;
    private String imsi;
    private String connectionState;
    private String authd;
    private String nam;
    private String imeisv;

    /**
     * imsi is not initialised to allow only NULL checks on the object
     *
     */
    public SubscriberIndentity() {
        this.msidn = null;
        this.connectionState = null;
        this.authd = null;
        this.nam = null;
        this.imeisv = null;
        this.imsi=null;
    }

    public String getMsidn() {
        return msidn;
    }

    public void setMsidn(String msidn) {
        this.msidn = msidn;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getConnectionState() {
        return connectionState;
    }

    public void setConnectionState(String state) {
        this.connectionState = state;
    }

    public String getAuthd() {
        return authd;
    }

    public void setAuthd(String authd) {
        this.authd = authd;
    }

    public String getNam() {
        return nam;
    }

    public void setNam(String nam) {
        this.nam = nam;
    }

    public String getImeisv() {
        return imeisv;
    }

    public void setImeisv(String imeisv) {
        this.imeisv = imeisv;
    }

    @Override
    public String toString() {
        return "NAM:" + nam + "\n MSISDN:" + msidn;
    }
}