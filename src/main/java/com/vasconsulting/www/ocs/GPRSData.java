package com.vasconsulting.www.ocs;

import java.util.ArrayList;

/**
 *
 * @author Nnamdi
 */
public class GPRSData {

    private ArrayList<String> gprsValues;

    public ArrayList<String> getGprsValues() {
        return gprsValues;
    }

    public void setGprsValues(ArrayList<String> gprsValues) {
        this.gprsValues = gprsValues;
    }

    public GPRSData() {
    }

    @Override
    public String toString() {
        return gprsValues.toString();
    }

}
