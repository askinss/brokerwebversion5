package com.vasconsulting.www.ocs;

public class Subscriber {

    private BasicInfo basicInfo;
    private ODBData odbdata;
    private GPRSData gprsData;
    private String ID;
    private String Serial;
    private String RetCode;
    private String RetDesc;
    private String tplid;
    private String tplName;
    private String state;
    private String IC;
    private String OC;
    private String GPRSLOCK;
    private String EPSLOCK;
    private String NON3GPPLOCK;
    private SubscriberIndentity subscriberIndentity;
    private SubscriberData subscriberData;

    /**
     * no-arg constructor.
     */
    public Subscriber() {
        //avoid pointer exceptions on calling mutator methods of this Objects
        subscriberIndentity = new SubscriberIndentity();
        subscriberData = new SubscriberData();
        basicInfo = new BasicInfo();
        odbdata = new ODBData();
        gprsData = new GPRSData();
    }

    public SubscriberIndentity getSubscriberIndentity() {
        return subscriberIndentity;
    }

    public void setSubscriberIndentity(SubscriberIndentity subscriberIndentity) {
        this.subscriberIndentity = subscriberIndentity;
    }

    public SubscriberData getSubscriberData() {
        return subscriberData;
    }

    public void setSubscriberData(SubscriberData subscriberData) {
        this.subscriberData = subscriberData;
    }

    public String getIC() {
        return IC;
    }

    public void setIC(String IC) {
        this.IC = IC;
    }

    public String getOC() {
        return OC;
    }

    public void setOC(String OC) {
        this.OC = OC;
    }

    public String getGPRSLOCK() {
        return GPRSLOCK;
    }

    public void setGPRSLOCK(String GPRSLOCK) {
        this.GPRSLOCK = GPRSLOCK;
    }

    public String getEPSLOCK() {
        return EPSLOCK;
    }

    public void setEPSLOCK(String EPSLOCK) {
        this.EPSLOCK = EPSLOCK;
    }

    public String getNON3GPPLOCK() {
        return NON3GPPLOCK;
    }

    public void setNON3GPPLOCK(String NON3GPPLOCK) {
        this.NON3GPPLOCK = NON3GPPLOCK;
    }

    public String getTplid() {
        return tplid;
    }

    public void setTplid(String tplid) {
        this.tplid = tplid;
    }

    public String getTplName() {
        return tplName;
    }

    public void setTplName(String tplName) {
        this.tplName = tplName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public BasicInfo getBasicInfo() {
        return basicInfo;
    }

    public void setBasicInfo(BasicInfo basicInfo) {
        this.basicInfo = basicInfo;
    }

    public ODBData getOdbdata() {
        return odbdata;
    }

    public void setOdbdata(ODBData odbdata) {
        this.odbdata = odbdata;
    }

    public GPRSData getGprsData() {
        return gprsData;
    }

    public void setGprsData(GPRSData gprsData) {
        this.gprsData = gprsData;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getSerial() {
        return Serial;
    }

    public void setSerial(String Serial) {
        this.Serial = Serial;
    }

    public String getRetCode() {
        return RetCode;
    }

    public void setRetCode(String RetCode) {
        this.RetCode = RetCode;
    }

    public String getRetDesc() {
        return RetDesc;
    }

    public void setRetDesc(String RetDesc) {
        this.RetDesc = RetDesc;
    }

}
