package com.vasconsulting.www.ocs;

public class Token {

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value + " " + pos;
    }
    private int pos;
    private String value;
}
