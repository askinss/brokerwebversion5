package com.vasconsulting.www.ocs;

public class AucInfo {

    private String hrsn;
    private String imsi;
    private String k4sno;
    private String cardType;
    private String alg;
    private String amfsno;
    private String kiValue;

    //sets the default values on the HLR
    public AucInfo() {
        this.k4sno = "1";
        this.cardType = "SIM";
    }

    public String getKiValue() {
        return kiValue;
    }

    public void setKiValue(String kiValue) {
        this.kiValue = kiValue;
    }

    public String getHrsn() {
        return hrsn;
    }

    public void setHrsn(String hrsn) {
        this.hrsn = hrsn;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getK4sno() {
        return k4sno;
    }

    public void setK4sno(String k4sno) {
        this.k4sno = k4sno;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getAlg() {
        return alg;
    }

    public void setAlg(String alg) {
        this.alg = alg;
    }

    public String getAmfsno() {
        return amfsno;
    }

    public void setAmfsno(String amfsno) {
        this.amfsno = amfsno;
    }

    @Override
    public String toString() {
        return this.imsi;
    }

}
