package com.vasconsulting.www.utility;

import java.rmi.RemoteException;
import java.util.HashMap;

import com.vasconsulting.www.interfaces.TabsInterface;

import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKGProxy;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPreOrPostPaidElement;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPreOrPostPaidResponseElement;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscriberInformationElement;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscriberInformationResponseElement;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscribersServicesElement;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscribersServicesResponseElement;
import ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.OrderManagementProxy;
import ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeserviceElement;
import ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeserviceResponseElement;

public class Tabs6 implements TabsInterface {

	private LoadAllProperties properties;
	private XMLUtility xmlUtility;

	public XMLUtility getXmlUtility() {
		return xmlUtility;
	}

	public void setXmlUtility(XMLUtility xmlUtility) {
		this.xmlUtility = xmlUtility;
	}

	public LoadAllProperties getProperties() {
		return properties;
	}

	public void setProperties(LoadAllProperties properties) {
		this.properties = properties;
	}

	public String deleteService(String msisdn, String equipID) {
		return changeService(msisdn, equipID, "D");
	}
	
	public String changeService(String msisdn, String equipID){
		return changeService(msisdn, equipID, "I");
	}

	public String changeService(String msisdn, String equipID, String changeType) {
		OrderManagementProxy orderProxy = new OrderManagementProxy();
		ChangeserviceElement params = new ChangeserviceElement();
		ChangeserviceResponseElement responseElement;
		params.setPSubno(msisdnMinusCountryCode(msisdn));
		params.setPUsername(properties.getProperty("tabsUserName"));
		params.setPSubscrtype("G");
		params.setPArea("0");
		params.setPSubsidyFlag("N");
		params.setPSndcmd("Y");

		String serviceName = "<Service name=\"OrderManagement\" msgType=\"Input\"><Operation name=\"op1\" paramName=\"service\">"
				+ "<InputParams><SServiceList><SService><Action>"+changeType+"</Action><EquipID>"
				+ equipID
				+ "</EquipID><LoginId/><SerialNo/><SParamList><SParam><ParamName>"
				+ "</ParamName><ParamValue></ParamValue></SParam></SParamList></SService></SServiceList></InputParams></Operation></Service>";

		params.setPServices(serviceName);
		params.setPAdditonalparams("<?xml version=\"1.0\" encoding=\"UTF-8\"?><SParamList></SParamList>");
		try {
			responseElement = orderProxy.changeservice(params);
			String tabsResponse = responseElement.getResult();
			System.out.println("Unparsed Response from TABS is: "+tabsResponse);
			return parseTABSResponse(tabsResponse);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

	}

	public HashMap<String, String> getPreOrPost(String msisdn) {
		try {
			TBI_KPI_PKGProxy tbiProxy = new TBI_KPI_PKGProxy();
			// GetSubscribersServicesElement subParams = new
			// GetSubscribersServicesElement();
			GetPreOrPostPaidElement subParams = new GetPreOrPostPaidElement();
			subParams.setSubno(msisdnMinusCountryCode(msisdn));
			subParams.setArea("0");
			subParams.setSubscrType("G");
			GetPreOrPostPaidResponseElement responseElement;
			// subParams.setMaxrowcount("10");
			// GetSubscribersServicesResponseElement responseElement;
			responseElement = tbiProxy.getPreOrPostPaid(subParams);
			String tabsResponse = responseElement.getResult();
			System.out.println("Unparsed Response from TABS is: "+tabsResponse);
			return xmlUtility.processTABSResponse(tabsResponse);
			// responseElement;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	public String getService(String msisdn) {
		try {
			TBI_KPI_PKGProxy tbiProxy = new TBI_KPI_PKGProxy();
			GetSubscribersServicesElement subParams = new GetSubscribersServicesElement();
			subParams.setSubno(msisdnMinusCountryCode(msisdn));
			subParams.setArea("0");
			subParams.setSubscrType("G");
			subParams.setMaxrowcount("10");
			GetSubscribersServicesResponseElement responseElement;
			responseElement = tbiProxy.getSubscribersServices(subParams);
			return responseElement.getResult();
			// responseElement;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	public HashMap<String, String> getSubscriberInformation(String msisdn) {
		GetSubscriberInformationResponseElement responseElement;
		try {
			TBI_KPI_PKGProxy tbiProxy = new TBI_KPI_PKGProxy();
			GetSubscriberInformationElement subParams = new GetSubscriberInformationElement();
			subParams.setSubno(msisdnMinusCountryCode(msisdn));
			subParams.setArea("0");
			subParams.setSubscrType("G");
			responseElement = tbiProxy.getSubscriberInformation(subParams);
			String tabsResponse = responseElement.getResult();
			System.out.println("Unparsed response from TABS is: "+tabsResponse);
			return xmlUtility.processTABSResponse(tabsResponse);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	private String msisdnMinusCountryCode(String msisdn) {
		return msisdn.replaceAll(properties.getProperty("opcoCountryCode"), "");
	}
	
	public String parseTABSResponse(String tabsResponse)
	{
		xmlUtility = (xmlUtility == null) ? (new XMLUtility()) : xmlUtility;
		return xmlUtility.getChangeServiceResponseCode(tabsResponse);
	}
	
	public static void main(String[] args) {
		if (args.length < 2) {
			System.out.println("Supply the msisdn and EquipID after this..");
		} else {
			Tabs6 tabs = new Tabs6();
			System.out.println(tabs.getPreOrPost(args[0]));
			System.out.println(tabs.getService(args[0]));
			System.out.println(tabs.getSubscriberInformation(args[0]));
			System.out.println(tabs.changeService((args[0]), (args[1])));
			System.out.println(tabs.deleteService((args[0]), (args[1])));
		}
	}

}
