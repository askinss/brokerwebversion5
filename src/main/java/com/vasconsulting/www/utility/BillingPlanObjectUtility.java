/**
 * This class is a POJO object that maps directly to the configuration objects that is in the
 * application xml file. It is the starting point of the application execution.
 * @author nnamdi Jibunoh
 * @Comapany VAS Consulting
 */
package com.vasconsulting.www.utility;

import java.util.ArrayList;


public class BillingPlanObjectUtility {
	private String shortCode;
	private String description;
	private String validity;
	private String cost;
	private String planType;//prep or post
	private String access;//user or all
	private String successMessage;
	private String externalData;
	private String status;
	private ArrayList<CommandPropertiesUtility> coomandObjects;
	
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getValidity() {
		return validity;
	}
	public void setValidity(String validity) {
		this.validity = validity;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getSuccessMessage() {
		return successMessage;
	}
	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}
	public String getExternalData() {
		return externalData;
	}
	public void setExternalData(String externalData) {
		this.externalData = externalData;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String daValue) {
		this.status = daValue;
	}
	public ArrayList<CommandPropertiesUtility> getCoomandObjects() {
		return coomandObjects;
	}
	public void setCoomandObjects(ArrayList<CommandPropertiesUtility> coomandObjects) {
		this.coomandObjects = coomandObjects;
	}
	public String getPlanType() {
		return planType;
	}
	public void setPlanType(String planType) {
		this.planType = planType;
	}
	public String getAccess() {
		return access;
	}
	public void setAccess(String access) {
		this.access = access;
	}	
	
}
