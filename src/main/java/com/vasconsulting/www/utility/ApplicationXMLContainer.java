package com.vasconsulting.www.utility;

import java.util.ArrayList;
import java.util.List;


public class ApplicationXMLContainer
{
	private List<SubscriberState> subscriber = new ArrayList<SubscriberState>();

	public List<SubscriberState> getSubscriberState()
	{
		return subscriber;
	}

	public void setSubscriberState(List<SubscriberState> subscriberState)
	{
		this.subscriber = subscriberState;
	}
	
	
}
