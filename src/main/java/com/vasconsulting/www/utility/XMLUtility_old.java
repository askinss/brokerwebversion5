package com.vasconsulting.www.utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.XmlMappingException;



public class XMLUtility_old {
	private Node node;
	private Document document = null;
	private HashMap<String, String> hashMap = new HashMap<String, String>();
	List<Node> list = null;
	Logger logger = Logger.getLogger(XMLUtility_old.class); 
	private Marshaller marshaller;
	private Unmarshaller unmarshaller;
	private String fileName;	
	LoadAllProperties properties = new LoadAllProperties();
	
	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}

	public void setMarshaller(Marshaller marshaller)
	{
		this.marshaller = marshaller;
	}

	public void setUnmarshaller(Unmarshaller unmarshaller)
	{
		this.unmarshaller = unmarshaller;
	}

	public XMLUtility_old(){}
	
	@SuppressWarnings("unchecked")
	public HashMap<String, String> getAllSubscriberInformation(String tabsResponse){
		try {
			document = DocumentHelper.parseText(tabsResponse);
			List<Node> list = document.selectNodes("//ROWSET/ROW/STATUS");
			Iterator<Node> iterator = list.iterator();
			while(iterator.hasNext()){
				Node node = iterator.next();
				hashMap.put(node.getName(), node.getText());
			}			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return hashMap;
	}
	
	@SuppressWarnings("unchecked")
	public HashMap<String, String> processTABSResponse(String tabsResponse){
		try {
			hashMap = new HashMap<String, String>();
			document = DocumentHelper.parseText(tabsResponse);
			
			if (document.selectSingleNode("//error") != null && 
					document.selectSingleNode("//error").hasContent()){
				hashMap.put("errorResponse", 
						document.selectSingleNode("//error").getText());
			}
			
			List<Node> list = document.selectNodes("//ROWSET/ROW");			
			
			Iterator<Node> iterator = list.iterator();
			while(iterator.hasNext()){
				Node node = iterator.next();
				
				List<Node> childrenList = document.selectNodes(node.getUniquePath()+"/child::node()");
				
				for(Node node1 : childrenList){
					hashMap.put(node1.getName(), node1.getText());
				}				
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return hashMap;
	}
	
	
	@SuppressWarnings("unchecked")
	public HashMap<String, String> getHomiscoUSSDXmlAsMap(String ussdRequest){
		try {
			document = DocumentHelper.parseText(ussdRequest);			
			
			List<Node> list = document.selectNodes("//params/param/value/struct/member");			
			
			for(Node node : list){				
				List<Node> nodeName = document.selectNodes(node.getUniquePath()+"/name");
				List<Node> nodeValue = document.selectNodes(node.getUniquePath()+"/value/string");
				
				hashMap.put(nodeName.get(0).getText(), nodeValue.get(0).getText());											
			}		
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return hashMap;
	}
	
	/*public String generateHomiscoResponse(String sessionID, String sequence, String responseToSubscriber, String ussdMessageType)
	{
		StringBuilder response = new StringBuilder("<methodResponse><params><param><value><struct>");
		response.append("<member><name>SESSION_ID</name><value><string>"+sessionID+"</string></value></member>");
		response.append("<member><name>RESPONSE_CODE</name><value><string>0</string></value></member>");
		response.append("<member><name>END_OF_SESSION</name><value><string>True</string></value></member>");
		response.append("<member><name>SEQUENCE</name><value><string>"+sequence+"</string></value></member>");
		response.append("<member><name>REQUEST_TYPE</name><value><string>"+ussdMessageType+"</string></value></member>");
		response.append("<member><name>USSD_BODY</name><value><string>"+responseToSubscriber+"</string></value></member>");
		response.append("</struct></value></param></params></methodResponse>");
		
		logger.info("Subscriber response is = "+response.toString());
		return response.toString();
	}*/
	
	public String generateHomiscoResponse(String sequence, String responseToSubscriber, String ussdMessageType, String endSession)
	{
		StringBuilder response = new StringBuilder("<methodResponse><params><param><value><struct>");
		response.append("<member><name>RESPONSE_CODE</name><value><string>0</string></value></member>");
		response.append("<member><name>SEQUENCE</name><value><string>"+sequence+"</string></value></member>");
		response.append("<member><name>REQUEST_TYPE</name><value><string>"+ussdMessageType+"</string></value></member>");
		response.append("<member><name>USSD_BODY</name><value><string>"+responseToSubscriber+"</string></value></member>");
		response.append("<member><name>END_OF_SESSION</name><value><string>"+endSession+"</string></value></member>");
		response.append("</struct></value></param></params></methodResponse>");
		
		logger.info("Subscriber response is = "+response.toString());
		
		return response.toString();
	}
	
	@SuppressWarnings("unchecked")
	public HashMap<String, String> getAtosUSSDXmlAsMap(String ussdRequest){
		try {
			document = DocumentHelper.parseText(ussdRequest);			
			
			List<Node> list = document.selectNodes("//ussd/child::node()");	
			
			for(Node node : list){								
				hashMap.put(node.getName(), node.getText());											
			}		
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return hashMap;
	}
	
	public String generateAtosResponse(String responseToSubscriber, int ussdMessageType)
	{
		StringBuilder response = new StringBuilder("<ussd>");
		response.append("<type>"+ussdMessageType+"</type>");
		response.append("<msg>"+responseToSubscriber+"</msg>");
		response.append("</ussd>");
		
		logger.info("Subscriber response is = "+response.toString());
		return response.toString();
	}
	
	public String getChangeServiceResponseCode(String tabsResponse){
		try {
			document = DocumentHelper.parseText(tabsResponse);
			node = document.selectSingleNode("//Service/Operation/OutputParams/Code");
			
		} catch (DocumentException e) {			
			e.printStackTrace();
		}
		return node.getText();
	}
	
	public String getSubscriberInformation(String tabsResponse){		
		try {
			document = DocumentHelper.parseText(tabsResponse);
			node = document.selectSingleNode("//ROWSET/ROW/STATUS");
			
		} catch (DocumentException e) {			
			e.printStackTrace();
		}
		return node.getText();
	}
	
	public String getSubscriberType(String tabsResponse){		
		try {
			document = DocumentHelper.parseText(tabsResponse);
			node = document.selectSingleNode("//ROWSET/ROW/PREPOST_PAID");
			
			if (node == null){
				node = document.selectSingleNode("//error");
			}
		} catch (DocumentException e) {			
			e.printStackTrace();
		}
		return node.getText();
	}
	
	public HashMap<String, String> getUSSDConfigRootTag()
	{
		HashMap<String, String> attributeReturns = new HashMap<String, String>();
		Document configurationDocument;
		SAXReader saxReader = new SAXReader();
		Node elementNodes;
		try {
			configurationDocument = saxReader.read("applicationUSSDConfiguration.xml");
			elementNodes = configurationDocument.selectSingleNode("//ussd");
			
			attributeReturns = getNodeAttributes(elementNodes);		
		}
		catch(DocumentException ex)
		{
			ex.printStackTrace();
		}
		return attributeReturns;
	}
	
	@SuppressWarnings("unchecked")
	public HashMap<String, ArrayList<String>> getUSSDMessage(String nodeID)
	{
		StringBuilder stringBuilder = null;; 
		HashMap<String, String> attributeReturns = new HashMap<String, String>();
		HashMap<String, ArrayList<String>> finalResponse = new HashMap<String, ArrayList<String>>();
		ArrayList<String> childMenus = new ArrayList<String>();
		Document configurationDocument;
		SAXReader saxReader = new SAXReader();
		Node elementNodes;
		String language,refText = "";
		int counter = 0;
		
		try {
			configurationDocument = saxReader.read("applicationUSSDConfiguration.xml");
			
			elementNodes = configurationDocument.selectSingleNode("//menu[@id='"+nodeID+"']");
			
			language = getNodeAttributes(elementNodes).get("language");			
			
			List<Node> optionNodes = configurationDocument.selectNodes(elementNodes.getUniquePath()+"/option");
			
			for (Node node : optionNodes) {
				if (counter < 1){
					stringBuilder = new StringBuilder("Veuillez selectionner \n");
					counter++;
				}
				attributeReturns = getNodeAttributes(node);	
				
				if (!attributeReturns.get("child").equalsIgnoreCase("") && 
						!attributeReturns.get("page").equalsIgnoreCase(""))
				{
					childMenus.add(attributeReturns.get("child"));//build the child arraylist
					
					if (attributeReturns.containsKey("page"))
					{
						List<Node> pageNode = configurationDocument.selectNodes("//ussd/page[@id='"+attributeReturns.get("page")+"']");
						
						for (Node nodes : pageNode)
						{
							attributeReturns = getNodeAttributes(nodes);
							
							if (attributeReturns.get("language").equalsIgnoreCase(language))
								stringBuilder.append(attributeReturns.get("text")+"\n");//build the text to return to subscriber
						}	
					}
				}
				else
				{
					if (attributeReturns.containsKey("ref") && attributeReturns.containsKey("timeout") && 
							attributeReturns.containsKey("autoresponse"))
					{
						refText = attributeReturns.get("command")+":sep:"+attributeReturns.get("ref")+
						":sep:"+attributeReturns.get("timeout")+":sep:"+attributeReturns.get("autoresponse");
						break;
					}
				}
			}
			if (refText.equalsIgnoreCase(""))
				finalResponse.put(stringBuilder.toString(), childMenus);
			else 
			{
				ArrayList<String> temp = new ArrayList<String>();
				temp.add(refText);
				finalResponse.put("LEAFNODE", temp);
			}
		}
		catch(DocumentException ex)
		{
			ex.printStackTrace();
		}
		return finalResponse;
	}
		
	
	@SuppressWarnings("unchecked")
	public ArrayList<BillingPlanObjectUtility> loadApplicationConfigurationXML(){
		
		SAXReader saxReader = new SAXReader();
		List<Node> elementNodes;
		Document configurationDocument;
		HashMap<String, String> attributeReturns = new HashMap<String, String>();
		BillingPlanObjectUtility billingPlanObject;
		CommandPropertiesUtility commandproperties;
		ArrayList<CommandPropertiesUtility> commandContainer;
		ArrayList<BillingPlanObjectUtility> billContainer = new ArrayList<BillingPlanObjectUtility>();
		
		try {
			configurationDocument = saxReader.read("applicationConfiguration.xml");
			//configurationDocument = saxReader.read("applicationConfiguration.xml");
			elementNodes = configurationDocument.selectNodes("//billingplans/billingplan");
			
			Iterator<Node> iterator = elementNodes.iterator();
			
			while (iterator.hasNext()) {
				Node node = (Node) iterator.next();				
				List<Node> actionNodes = configurationDocument.selectNodes(node.getUniquePath()+"/billingcommand");
				
				attributeReturns = getNodeAttributes(node);
				
				billingPlanObject = new BillingPlanObjectUtility();
				
				billingPlanObject.setValidity(attributeReturns.get("validity"));
				billingPlanObject.setSuccessMessage(attributeReturns.get("successmessage"));
				billingPlanObject.setShortCode(attributeReturns.get("shortcode"));
				billingPlanObject.setExternalData(attributeReturns.get("externaldata1"));
				billingPlanObject.setDescription(attributeReturns.get("description"));
				billingPlanObject.setStatus(attributeReturns.get("status"));
				billingPlanObject.setCost(attributeReturns.get("cost"));
				
				commandContainer = new ArrayList<CommandPropertiesUtility>();
				
				
				Iterator<Node> iterator1 = actionNodes.iterator();
				while (iterator1.hasNext()){
					Node node1 = (Node) iterator1.next();
					commandproperties = new CommandPropertiesUtility();
					
					attributeReturns = getNodeAttributes(node1);
					
					commandproperties.setCommand(attributeReturns.get("command"));
					commandproperties.setParam(attributeReturns.get("param"));
					commandproperties.setReceiver(attributeReturns.get("receiver"));
					
					commandContainer.add(commandproperties);				
				}
				
				billingPlanObject.setCoomandObjects(commandContainer);
				
				billContainer.add(billingPlanObject);
			}
			
			return 	billContainer;
			
		} catch (DocumentException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private HashMap<String, String> getNodeAttributes( Node node )
	   {
	      Element e = (Element) node;
	      HashMap<String, String> container = new HashMap<String, String>();
	      
	      List<Attribute> list = e.attributes();
	      for (Attribute attribute : list)
	      {
	         container.put(attribute.getName(), attribute.getValue());
	      }
	      return container;
	   }
	
	/**
	 * This service is used to create a service name for a TABS change service request.
	 * The method takes an equipid and also an action to perform which is either a I or D for install or delete respectively.
	 * @param equipId
	 * @param action
	 * @return
	 */
	
	private String getTABSServiceName(String equipId, String action){
		
		return "<Service name\"OrderManagement\" msgType=\"Input\"><Operation name=\"op1\" paramName=\"service\">"+
		"<InputParams><SServiceList><SService><Action>"+action+
		"</Action><EquipID>"+equipId+
		"</EquipID><LoginId/><SerialNo/><SParamList><SParam><ParamName>"+
		"</ParamName><ParamValue></ParamValue></SParam></SParamList></SService>" +
		"</SServiceList></InputParams></Operation></Service>";
		
	}
	
	private String getAdditionalTABSParameter(){
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><SParamList></SParamList>";
	}
	
	
	public void saveSubscriberStateToXML(ApplicationXMLContainer container) throws 
	XmlMappingException, IOException
	{
		FileOutputStream out = null;		
		try
		{
			out = new FileOutputStream(fileName);
			this.marshaller.marshal(container, new StreamResult(out));				
		}
		finally
		{
			if (out != null)
				out.close();
				
		}
	}
	
	public SubscriberState getSubscriberDataByMsisdn(String msisdn)
	{
		List<SubscriberState> state = null;
		try {
			state = getSubscriberDataByMsisdn();
		} 
		catch (XmlMappingException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		if (state != null)
		{
			for (SubscriberState subscriberState : state)
			{
				if (subscriberState.getMsisdn().equalsIgnoreCase(msisdn))
				{
					logger.info("Found a match for this subscriber "+msisdn+" with details "+subscriberState.getSubdata());
					return subscriberState;
				}
			}
		}
		return new SubscriberState();
	}
	
	private List<SubscriberState> getSubscriberDataByMsisdn() throws 
	XmlMappingException, IOException
	{
		FileInputStream in = null;	
		ApplicationXMLContainer container = null;
		List<SubscriberState> containerValues = null;
		try
		{
			in = new FileInputStream(fileName);
			container = (ApplicationXMLContainer) this.unmarshaller.unmarshal(new StreamSource(in));
			
			if (container != null)
			{
				return container.getSubscriberState();
			}
			else {
				containerValues = new ArrayList<SubscriberState>();
				return containerValues;
			}
		}
		finally
		{
			if (in != null)
				in.close();
		}
	}
	
	public String getNextTABSServer()
	{
		String[] tabsRange = properties.getProperty("tabsServerRange").split(",");
		
		int a = 1;
		int b = 4;
		int result = new Double(Math.floor((Math.abs(a-b)+1) * Math.random()) + Math.min(a, b)).intValue();
		String response = "tabsapps0"+result;
		
		return response;
	}
	
}
