package com.vasconsulting.www.utility;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.GeneralSecurityException;
import java.util.Date;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Node;

import com.celtel.databundle.service.utilities.Utilities;
import com.vasconsulting.www.domain.ReconcilDetail;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.invokers.ContextLoaderImpl;

public class RIMXMLUtility
{
	private LoadAllProperties properties = new LoadAllProperties();
	private static Logger logger = Logger.getLogger(RIMXMLUtility.class.getName());;
	private Document document = null;
	private EncryptAndDecrypt ecnrypter = new EncryptAndDecrypt();
	
	public RIMXMLUtility()
	{
		//properties = (LoadAllProperties)ContextLoaderImpl.getBeans("loadProperties");
		//logger = 
	}
	
	private RIMXMLResponseDetail processRIMResponse(String response)
	{
		RIMXMLResponseDetail rimXMLResponseDetail = new RIMXMLResponseDetail();
		
		try
		{
			document = DocumentHelper.parseText(response);
			
			Node billingID = document.selectSingleNode("//ProvisioningReply/Body/ProvisioningEntity/ProvisioningDataItem[@name='BillingId']");
			
			Node serviceName = document.
					selectSingleNode("//ProvisioningReply/Body/ProvisioningEntity/ProvisioningEntity/ProvisioningDataItem[@name='ServiceName']");
			Node node = document.selectSingleNode("//ProvisioningReply/Body/ProvisioningEntity/ProvisioningEntity/TransactionCode");
			
			if (billingID != null)
				rimXMLResponseDetail.setImsi(billingID.getText());
			else {
				Node billingIDImsi = 
					document.selectSingleNode("//ProvisioningReply/Body/ProvisioningEntity/ProvisioningDataItem[@name='OldBillingId']");
								
				if (billingIDImsi == null)
				{
					billingIDImsi = document.selectSingleNode("//ProvisioningReply/Body/ProvisioningEntity/ProvisioningDataItem[@name='IMSI']");
				}
				
				rimXMLResponseDetail.setImsi(billingIDImsi.getText());
			}
			
			if (serviceName != null)
				rimXMLResponseDetail.setServiceName(serviceName.getText());
			
			if (node == null)
			{
				node = document.selectSingleNode("//ProvisioningReply/Body/ProvisioningEntity/TransactionCode");
			}
			
			Node nodeName = document.selectSingleNode(node.getUniquePath()+"/ErrorCode");
			Node nodeValue = document.selectSingleNode(node.getUniquePath()+"/ErrorDescription");
				
			if (nodeName != null && nodeValue != null)
			{
				rimXMLResponseDetail.setErrorCode(nodeName.getText());
				rimXMLResponseDetail.setErrorDescription(nodeValue.getText());	
			}
			else
			{
				nodeName = document.selectSingleNode(node.getUniquePath()+"/StatusCode");
				nodeValue = document.selectSingleNode(node.getUniquePath()+"/StatusDescription");
				
				if (nodeName != null && nodeValue != null)
				{
					rimXMLResponseDetail.setErrorCode(nodeName.getText());
					rimXMLResponseDetail.setErrorDescription(nodeValue.getText());	
				}
			}
		}
		catch(DocumentException ex)
		{
			ex.printStackTrace();
			return null;
		}
		System.out.println(rimXMLResponseDetail);
		return rimXMLResponseDetail;
	}
	
	
	
	
	public RIMXMLResponseDetail sendXMLRequestToRIM(String request)
	{
		URL url;
		URLConnection urlConnection;
		HttpURLConnection httpUrlConnection;
		String response = "";
		String line = null;
		
		try
		{
			//"https://zainga.provisioning.blackberry.com/ari/submitXML"
			//url = new URL("https://zainga.provisioning.blackberry.com/ari/submitXML");
			url = new URL(properties.getProperty("RIMxmlurl"));

			urlConnection = url.openConnection();

			if (urlConnection instanceof HttpURLConnection) {

				httpUrlConnection = (HttpURLConnection) urlConnection;

				// Disable automatic rediredtion to see the status header
				HttpURLConnection.setFollowRedirects(false);

				
				// Now set connection parameters
				httpUrlConnection.setDoOutput(true);
				httpUrlConnection.setDoInput(true);
				httpUrlConnection.setRequestMethod("POST");
				httpUrlConnection
						.setRequestProperty("Content-Type", "text/xml");
				httpUrlConnection.setRequestProperty("Content-Length",
						new Integer(request.length()).toString());
				httpUrlConnection.setReadTimeout(7000);
				httpUrlConnection.setConnectTimeout(7000);
				OutputStream out = httpUrlConnection.getOutputStream();
				Writer wout = new OutputStreamWriter(out);

				wout.write(request);

				wout.flush();
				wout.close();

				BufferedReader in = new BufferedReader(new InputStreamReader(
						httpUrlConnection.getInputStream()));

				while ((line = in.readLine()) != null) {
					response = response + line;

				}

				
				response  = response.substring(0,39)+response.substring(126);
				//System.out.println("RIM Response = "+response);
				logger.info("RIM Response = "+response);
				
				return processRIMResponse(response);
			}
			return null;
		}
		catch(MalformedURLException ex) 
		{
			ex.printStackTrace();
			return null;
		}
		catch(FileNotFoundException e){
			e.printStackTrace();
			return null;
		}
		catch(IOException ex) 
		{
			ex.printStackTrace();
			return null;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Method used to check the status of the supplied subscriber on RIM. This method uses Modify command and will return a 
	 * RIMXMLResponseDetail object on execution.
	 * @param subscriberDetail
	 * @return
	 */
	public RIMXMLResponseDetail checkRIMStatusByIMSI(SubscriberDetail subscriberDetail)
	{
		String transactionId = new Long(System.currentTimeMillis()).toString();
		String carrierLoginId = decrypt(properties.getProperty("RIMCarrierLoginId")); 
		String carrierPassword = decrypt(properties.getProperty("RIMCarrierPwd"));
		java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
		String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
		
		logger.info("The value of requested service is: "+subscriberDetail.getServicetype());
		logger.info("The value of requesting IMSI is: "+subscriberDetail.getImsi());
		logger.info("The Requesting channel is: USSD CODE");
		logger.info("The value of requesting MSISDN is: "+subscriberDetail.getMsisdn());
		logger.info("Service is : "+subscriberDetail.getServiceplan());
		
		String request = "<?xml version='1.0' ?><!DOCTYPE ProvisioningRequest SYSTEM 'ProvisioningRequest.dtd'>" +
				"<ProvisioningRequest TransactionId='"+transactionId+"' Version='1.2' TransactionType='Status' ProductType='BlackBerry'>" +
				"<Header><Sender id='101' name='WirelessCarrier'><Login>"+carrierLoginId+"</Login><Password>"+carrierPassword+"</Password>"+
        "</Sender><TimeStamp>"+timeStamp+"</TimeStamp></Header><Body><ProvisioningEntity name='subscriber'>" +
        "<ProvisioningDataItem name='BillingId'>"+subscriberDetail.getImsi()+"</ProvisioningDataItem></ProvisioningEntity>" +
        		"</Body></ProvisioningRequest>";
		
		//logger.info("XML sent to RIM to check status of "+subscriberDetail.getMsisdn()+" is = "+request);
		
		return sendXMLRequestToRIM(request);
	}
	
	public RIMXMLResponseDetail cancelSubscriptionByIMSI(SubscriberDetail subscriberDetail)
	{
		String transactionId = new Long(System.currentTimeMillis()).toString();
		String carrierLoginId = decrypt(properties.getProperty("RIMCarrierLoginId")); 
		String carrierPassword = decrypt(properties.getProperty("RIMCarrierPwd"));
		java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
		String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
		
		logger.info("The value of requested service is: "+subscriberDetail.getServicetype());
		logger.info("The value of requesting IMSI is: "+subscriberDetail.getImsi());
		logger.info("The Requesting channel is: USSD CODE");
		logger.info("The value of requesting MSISDN is: "+subscriberDetail.getMsisdn());
		logger.info("Service is : "+subscriberDetail.getServiceplan());
		
		String request = "<?xml version='1.0' ?><!DOCTYPE ProvisioningRequest SYSTEM 'ProvisioningRequest.dtd'>" +
				"<ProvisioningRequest TransactionId='"+transactionId+"' Version='1.2' TransactionType='Cancel' ProductType='BlackBerry'>" +
				"<Header><Sender id='101' name='WirelessCarrier'><Login>"+carrierLoginId+"</Login><Password>"+carrierPassword+"</Password>"+
        "</Sender><TimeStamp>"+timeStamp+"</TimeStamp></Header><Body><ProvisioningEntity name='subscriber'>" +
        "<ProvisioningDataItem name='BillingId'>"+subscriberDetail.getImsi()+"</ProvisioningDataItem> " + "<ProvisioningDataItem name='MSISDN'>"+subscriberDetail.getMsisdn()+"</ProvisioningDataItem></ProvisioningEntity>" +
        		"</Body></ProvisioningRequest>";
		
		//logger.info("XML sent to RIM to check status of "+subscriberDetail.getMsisdn()+" is = "+request);
		
		return sendXMLRequestToRIM(request);
	}
	
	
	public RIMXMLResponseDetail suspendSubscriptionByIMSI(SubscriberDetail subscriberDetail)
	{
		String transactionId = new Long(System.currentTimeMillis()).toString();
		String carrierLoginId = decrypt(properties.getProperty("RIMCarrierLoginId")); 
		String carrierPassword = decrypt(properties.getProperty("RIMCarrierPwd"));
		java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
		String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
		
		logger.info("The value of requested service is: "+subscriberDetail.getServicetype());
		logger.info("The value of requesting IMSI is: "+subscriberDetail.getImsi());
		logger.info("The Requesting channel is: USSD CODE");
		logger.info("The value of requesting MSISDN is: "+subscriberDetail.getMsisdn());
		logger.info("Service is : "+subscriberDetail.getServiceplan());
		
		String request = "<?xml version='1.0' ?><!DOCTYPE ProvisioningRequest SYSTEM 'ProvisioningRequest.dtd'>" +
				"<ProvisioningRequest TransactionId='"+transactionId+"' Version='1.2' TransactionType='Suspend' ProductType='BlackBerry'>" +
				"<Header><Sender id='101' name='WirelessCarrier'><Login>"+carrierLoginId+"</Login><Password>"+carrierPassword+"</Password>"+
        "</Sender><TimeStamp>"+timeStamp+"</TimeStamp></Header><Body><ProvisioningEntity name='subscriber'>" +
        "<ProvisioningDataItem name='BillingId'>"+subscriberDetail.getImsi()+"</ProvisioningDataItem> " + "<ProvisioningDataItem name='MSISDN'>"+subscriberDetail.getMsisdn()+"</ProvisioningDataItem></ProvisioningEntity>" +
        		"</Body></ProvisioningRequest>";
		
		//logger.info("XML sent to RIM to check status of "+subscriberDetail.getMsisdn()+" is = "+request);
		
		return sendXMLRequestToRIM(request);
	}
	
	/**
	 * This method is used to change the billingID of a subscriebr from the application user interface
	 * @param subscriberDetail
	 * @param imsi
	 * @return
	 */
	public RIMXMLResponseDetail modifyBillingIdentifier(SubscriberDetail subscriberDetail, String imsi)
	{
		String transactionId = new Long(System.currentTimeMillis()).toString();
		String carrierLoginId = decrypt(properties.getProperty("RIMCarrierLoginId")); 
		String carrierPassword = decrypt(properties.getProperty("RIMCarrierPwd"));
		java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
		String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
		
		logger.info("The value of Old IMSI is: "+subscriberDetail.getImsi());
		logger.info("The value of New IMSI is: "+imsi);
		
		String request = "<?xml version='1.0' ?><!DOCTYPE ProvisioningRequest SYSTEM 'ProvisioningRequest.dtd'>" +
				"<ProvisioningRequest TransactionId='"+transactionId+"' Version='1.2' TransactionType='Activate' ProductType='BlackBerry'>" +
				"<Header><Sender id='101' name='WirelessCarrier'><Login>"+carrierLoginId+"</Login><Password>"+carrierPassword+"</Password>"+
        "</Sender><TimeStamp>"+timeStamp+"</TimeStamp></Header><Body><ProvisioningEntity name='subscriber'>" +
        "<ProvisioningDataItem name='OldBillingId'>"+subscriberDetail.getImsi()+"</ProvisioningDataItem>"+
        "<ProvisioningDataItem name='IMSI'>"+imsi+"</ProvisioningDataItem></ProvisioningEntity>" +
        		"</Body></ProvisioningRequest>";
		
		
		return sendXMLRequestToRIM(request);
	}
	
	
	public RIMXMLResponseDetail modifyBillingIdentifier(SubscriberDetail subscriberDetail, ReconcilDetail reconcilDetail)
	{
		String transactionId = new Long(System.currentTimeMillis()).toString();
		String carrierLoginId = decrypt(properties.getProperty("RIMCarrierLoginId")); 
		String carrierPassword = decrypt(properties.getProperty("RIMCarrierPwd"));
		java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
		String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
		
		logger.info("The value of Old IMSI is: "+subscriberDetail.getMsisdn());
		logger.info("The value of Old IMSI is: "+subscriberDetail.getImsi());
		logger.info("The value of New IMSI is: "+reconcilDetail.getImsi());
		
		String request = "<?xml version='1.0' ?><!DOCTYPE ProvisioningRequest SYSTEM 'ProvisioningRequest.dtd'>" +
				"<ProvisioningRequest TransactionId='"+transactionId+"' Version='1.2' TransactionType='Modify' ProductType='BlackBerry'>" +
				"<Header><Sender id='101' name='WirelessCarrier'><Login>"+carrierLoginId+"</Login><Password>"+carrierPassword+"</Password>"+
        "</Sender><TimeStamp>"+timeStamp+"</TimeStamp></Header><Body><ProvisioningEntity name='subscriber'>" +
        "<ProvisioningDataItem name='OldBillingId'>"+subscriberDetail.getImsi()+"</ProvisioningDataItem>"+
        "<ProvisioningDataItem name='IMSI'>"+reconcilDetail.getImsi()+"</ProvisioningDataItem></ProvisioningEntity>" +
        		"</Body></ProvisioningRequest>";
		
		
		return sendXMLRequestToRIM(request);
	}
	
	
	public RIMXMLResponseDetail modifyNonBillingIdentifier(SubscriberDetail subscriberDetail)
	{
		String transactionId = new Long(System.currentTimeMillis()).toString();
		System.out.println(properties.getProperty("RIMCarrierLoginId"));
		String carrierLoginId = decrypt(properties.getProperty("RIMCarrierLoginId")); 
		String carrierPassword = decrypt(properties.getProperty("RIMCarrierPwd"));
		java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
		String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
		
		logger.info("The value of requested service is: "+subscriberDetail.getServicetype());
		logger.info("The value of requesting IMSI is: "+subscriberDetail.getImsi());
		logger.info("The value of requesting MSISDN is: "+subscriberDetail.getMsisdn());
		
		String request = "<?xml version='1.0' ?><!DOCTYPE ProvisioningRequest SYSTEM 'ProvisioningRequest.dtd'>" +
				"<ProvisioningRequest TransactionId='"+transactionId+"' Version='1.2' TransactionType='Modify' ProductType='BlackBerry'>" +
				"<Header><Sender id='101' name='WirelessCarrier'><Login>"+carrierLoginId+"</Login><Password>"+carrierPassword+"</Password>"+
        "</Sender><TimeStamp>"+timeStamp+"</TimeStamp></Header><Body><ProvisioningEntity name='subscriber'>" +
        "<ProvisioningDataItem name='IMSI'>"+subscriberDetail.getImsi()+"</ProvisioningDataItem>"+
        "<ProvisioningDataItem name='MSISDN'>"+subscriberDetail.getMsisdn()+"</ProvisioningDataItem>" +
        "<ProvisioningDataItem name='PIN'>"+subscriberDetail.getPin()+"</ProvisioningDataItem>" +
        "<ProvisioningDataItem name='IMEI'>"+subscriberDetail.getImei()+"</ProvisioningDataItem></ProvisioningEntity>" +
        		"</Body></ProvisioningRequest>";
		
		logger.info("XML sent to RIM to check status of "+subscriberDetail.getMsisdn()+" is = "+request);
		
		return sendXMLRequestToRIM(request);
	}
	
	/**
	 * Method used to activate the supplied subscriber service on RIM. This method uses Modify command and will return a 
	 * RIMXMLResponseDetail object on execution.
	 * @param subscriberDetail
	 * @return
	 */
	public RIMXMLResponseDetail activateRIMService(SubscriberDetail subscriberDetail)
	{
		String transactionId = new Long(System.currentTimeMillis()).toString();
		String carrierLoginId = decrypt(properties.getProperty("RIMCarrierLoginId")); 
		String carrierPassword = decrypt(properties.getProperty("RIMCarrierPwd"));
		java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
		String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
		
		logger.info("The value of requested service is: "+subscriberDetail.getServicetype());
		logger.info("The value of requesting IMSI is: "+subscriberDetail.getImsi());
		logger.info("The Requesting channel is: USSD CODE");
		logger.info("The value of requesting MSISDN is: "+subscriberDetail.getMsisdn());
		logger.info("Service is : "+subscriberDetail.getServiceplan());
		logger.info("URL is : "+ properties.getProperty("RIMxmlurl"));
		
		String request = "<?xml version='1.0'?><!DOCTYPE ProvisioningRequest SYSTEM 'ProvisioningRequest.dtd'>"+
				"<ProvisioningRequest TransactionId='"+transactionId+"' Version='1.2' TransactionType='Activate' ProductType='BlackBerry'>"+
				"<Header><Sender id='101' name='WirelessCarrier'><Login>"+carrierLoginId+"</Login><Password>"+carrierPassword+"</Password>"+
				"</Sender><TimeStamp>"+timeStamp+"</TimeStamp></Header><Body><ProvisioningEntity name='subscriber'>"+
            "<ProvisioningDataItem name='BillingId'>"+subscriberDetail.getImsi()+"</ProvisioningDataItem>"+"<ProvisioningDataItem name='MSISDN'>"+subscriberDetail.getMsisdn()+"</ProvisioningDataItem>"+"<ProvisioningDataItem name='ICCID'>8923306010191635916</ProvisioningDataItem><ProvisioningEntity name='service'>"+
               " <ProvisioningDataItem name='ServiceName'>"+subscriberDetail.getServiceplan().trim()+"</ProvisioningDataItem></ProvisioningEntity>"+
               "</ProvisioningEntity></Body></ProvisioningRequest>";
		
		logger.info("XML sent to RIM to activate "+subscriberDetail.getMsisdn()+" is ="+request);
		
		return sendXMLRequestToRIM(request);
	}
	
	
/**	public RIMXMLResponseDetail activatRIMService(SubscriberDetail subscriberDetail, ReconcilDetail reconcilDetail)
	{
		String transactionId = new Long(System.currentTimeMillis()).toString();
		String carrierLoginId = decrypt(properties.getProperty("RIMCarrierLoginId")); 
		String carrierPassword = decrypt(properties.getProperty("RIMCarrierPwd"));
		java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
		String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
		
		logger.info("The value of requested service is: "+subscriberDetail.getServicetype());
		logger.info("The value of requesting IMSI is: "+reconcilDetail.getImsi());
		logger.info("The Requesting channel is: USSD CODE");
		logger.info("The value of requesting MSISDN is: "+subscriberDetail.getMsisdn());
		logger.info("Service is : "+subscriberDetail.getServiceplan());
		
		String request = "<?xml version='1.0'?><!DOCTYPE ProvisioningRequest SYSTEM 'ProvisioningRequest.dtd'>"+
				"<ProvisioningRequest TransactionId='"+transactionId+"' Version='1.2' TransactionType='Activate' ProductType='BlackBerry'>"+
				"<Header><Sender id='101' name='WirelessCarrier'><Login>"+carrierLoginId+"</Login><Password>"+carrierPassword+"</Password>"+
				"</Sender><TimeStamp>"+timeStamp+"</TimeStamp></Header><Body><ProvisioningEntity name='subscriber'>"+
            "<ProvisioningDataItem name='BillingId'>"+reconcilDetail.getImsi()+"</ProvisioningDataItem><ProvisioningEntity name='service'>"+
               " <ProvisioningDataItem name='ServiceName'>"+subscriberDetail.getServiceplan().trim()+"</ProvisioningDataItem></ProvisioningEntity>"+
               "</ProvisioningEntity></Body></ProvisioningRequest>";
		
		logger.info("XML sent to RIM to activate "+subscriberDetail.getMsisdn()+" is ="+request);
		
		return sendXMLRequestToRIM(request);
	}
**/
	
	
	private String decrypt(String value) {
		try {
			return ecnrypter.decrypt(value);
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	
}
