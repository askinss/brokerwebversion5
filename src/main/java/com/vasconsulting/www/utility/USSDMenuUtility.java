package com.vasconsulting.www.utility;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.vasconsulting.www.domain.USSDMenuItemsGhana;
import com.vasconsulting.www.domain.USSDMenuSession;

@Repository
@Transactional
public class USSDMenuUtility 
{
	private SessionFactory sessionFactory;


	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<USSDMenuItemsGhana> getUssdMenu(String sequence_value, String ussd_body, String [] parent_id)
	{
		System.out.println("sequence_value = "+sequence_value+","+ "ussd_body = "+ussd_body+","+ "parent_id = "+parent_id.length);
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(USSDMenuItemsGhana.class)
				.add(Restrictions.eq("sequence_value", sequence_value))
				.add(Restrictions.eq("ussd_body", ussd_body));
		if (parent_id.length != 0) criteria.add(Restrictions.in("parent_id",parent_id));
		
		/*return (ArrayList<USSDMenuItemsGhana>)this.sessionFactory.getCurrentSession().
				createQuery("from USSDMenuItemsGhana where sequence_value = :seq and"
				+ " ussd_body = :ussd").setString("seq", sequence_value).setString("ussd", ussd_body).list();*/
		
		return (ArrayList<USSDMenuItemsGhana>)criteria.list();

	}
	
	@SuppressWarnings("unchecked")
	public String[] checkMsisdnSession(String msisdn)
	{
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(USSDMenuSession.class).add(Restrictions.eq("msisdn", msisdn));
		
		List<USSDMenuSession> menuSessions = criteria.list();
		if (menuSessions.isEmpty()) return new String[]{};
		else {
			
			USSDMenuSession menuSession = menuSessions.get(0);
			if (menuSession.getMenu_ids().endsWith(":sep:")) 
			{
				String tempValue = menuSession.getMenu_ids().substring(0, menuSession.getMenu_ids().length() - 5);
				System.out.println("The value of menu_ids to return for msisdn["+msisdn+"] = "+tempValue);
				return tempValue.split(":sep:");
			}				
			else return menuSession.getMenu_ids().split(":sep:");
		}
	}
	
	public int saveUssdSession(String msisdn, String menu_ids, String sequence_value)
	{
		USSDMenuSession ussdSession = new USSDMenuSession();
		ussdSession.setMenu_ids(menu_ids);
		ussdSession.setMsisdn(msisdn);
		ussdSession.setSequence_value(sequence_value);
		
		sessionFactory.getCurrentSession().saveOrUpdate(ussdSession);		
		return 0;
	}
	
	public int deleteUssdSession(String msisdn)
	{
		USSDMenuSession ussdSession = new USSDMenuSession();
		ussdSession.setMsisdn(msisdn);
		sessionFactory.getCurrentSession().delete(ussdSession);		
		return 0;
	}
}
