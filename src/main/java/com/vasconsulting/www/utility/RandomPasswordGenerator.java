package com.vasconsulting.www.utility;

import java.util.Random;

public class RandomPasswordGenerator {

	//Kannel safe characters are at http://www.csoft.co.uk/sms/character_sets/gsm.htm
	static char[] CHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@#$%&+".toCharArray();
	static char[] CAPS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
	static char[] SMALL = "abcdefghijklmnopqrstuvwxyz".toCharArray();
	static char[] SPECIALCHARS = "#%&+".toCharArray(); 
	
	public static char[] generateRandomPassword() {
		Random rand = new Random();
		int passwordLength = rand.nextInt(7) + 8; //generate password length between 8 and 15
		char[] pass = new char[passwordLength];
		for (int i = 0; i < passwordLength;i++){
			if (i == 0){
				pass[i] = CAPS[rand.nextInt(CAPS.length - 1)];
			} else if (i == 1){
				pass[i] = SPECIALCHARS[rand.nextInt(SPECIALCHARS.length - 1)];
			} else if (i == 3){
				pass[i] = SMALL[rand.nextInt(SMALL.length - 1)];
			} else if (i == 5){
				pass[i] = (char) (rand.nextInt(9) + 48);
			}else {pass[i] = CHARS[rand.nextInt(CHARS.length - 1)];}
		}
		return shuffleArray(pass);
	}
	
	  // Implementing FisherŠYates shuffle
	  static char[] shuffleArray(char[] ar)
	  {
	    Random rnd = new Random();
	    for (int i = ar.length - 1; i > 0; i--)
	    {
	      int index = rnd.nextInt(i + 1);
	      // Simple swap
	      char a = ar[index];
	      ar[index] = ar[i];
	      ar[i] = a;
	    }
	    return ar;
	  }

}
