package com.vasconsulting.www.utility;

import java.math.BigDecimal;

public class HomiscoResults
{
	private String status;
	private BigDecimal balance;
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	public BigDecimal getBalance()
	{
		return balance;
	}
	public void setBalance(BigDecimal balance)
	{
		this.balance = balance;
	}
	
	
}
