package com.vasconsulting.www.utility;

import java.io.InputStream;
import java.util.*;

import com.blackberry.www.webservices.Provisioning.ProvisioningPortProxy;
import com.vasconsulting.www.domain.SubscriberDetail;

import soap.comm.ari.control.provision.*;

import org.apache.log4j.*;

public class RIMQueryUtil {
	private static final int SUCCESS = 0;
	private static final int FAIL = -1;
	public static final int SERVICE_ALREADY_ACTIVE = 2;
	public static final int INSUFFICIENT_PERMISSIONS = 3;
	public static final int INVALID_DATA = 4;
	public static final int SYSTEM_ERROR = 5;
	private static Logger logger;
	private InputStream inputStream = null;
	private LoadAllProperties properties;
	
	public RIMQueryUtil(){
		//inputStream = this.getClass().getResourceAsStream("kannelconfig.properties");
		properties = new LoadAllProperties();
		logger = Logger.getLogger(RIMQueryUtil.class.getName());
	}
	
	public int suspendSubscription(SubscriberDetail subscriberDetail){
		/*
		 * List of suspended subscribers is passed to RIM server via agreed protocol
		 * RIM API will proceed to suspend the subscriber from the service.
		 */
		try{
			logger.info("received request to suspend subscription for "+subscriberDetail.getMsisdn()+" ...");
			String transactionId = new Long(System.currentTimeMillis()).toString();
			String carrierLoginId = properties.getProperty("RIMCarrierLoginId"); 
			String carrierPassword = properties.getProperty("RIMCarrierPwd");
			ProvisionReqEntity[] provisionRequestEntityArr = new ProvisionReqEntity[1];		
										
			ProvisionDataItem[] provisionDataItemArr = new ProvisionDataItem[1];
			ProvisionDataItem provisionDataItemThree = new ProvisionDataItem(subscriberDetail.getImsi(),"IMSI",null);
			provisionDataItemArr[0] = provisionDataItemThree;
			
			
			ProvisionReqEntity provisionRequestEntity = new ProvisionReqEntity(provisionDataItemArr,"service",null,null,"service");
			provisionRequestEntityArr[0] = provisionRequestEntity;				
						
			ProvisionSender provisionSender = new ProvisionSender("",carrierLoginId,"",null,carrierPassword,"");
			java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
			String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
			ProvisionReqHeader requestHeader = new ProvisionReqHeader(null,provisionSender,"CCYY-MM-DDThh:mm:ssTZD",timeStamp);
			ProvisionRequest provisionRequest = new ProvisionRequest(provisionRequestEntityArr,requestHeader,null,"BlackBerry","",transactionId,"Suspend","1.2");
			ProvisioningPortProxy proxy = new ProvisioningPortProxy();
			logger.info("sending provisioning request for "+subscriberDetail.getMsisdn()+". Action is SUSPEND.");
			ProvisionReply reply = proxy.submitSync(provisionRequest);
			//we should check that the reply matches our initial request
			System.out.println("request.loginId: "+provisionSender.getLoginId());
			if(reply != null){
				System.out.println("reply.getHeader(): "+reply.getHeader());
			}
			if(reply.getHeader() != null){
				System.out.println("reply.getHeader().getSender(): "+reply.getHeader().getSender());
			}
			if(reply.getHeader().getSender() != null){
				System.out.println("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
			}
			System.out.println("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
			System.out.println("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
			logger.debug("reply.getHeader().getMajorErrCode() for "+subscriberDetail.getMsisdn()+", : "+reply.getHeader().getMajorErrCode());
			//check for errors
			if(reply.getHeader().getMajorErrCode()!= null && !(reply.getHeader().getMajorErrCode().equalsIgnoreCase("0"))){
				//an error occured, complete or partial
				ProvisionReplyEntity[] prvReplyEntities = reply.getBody();
				for(int i=0; i<prvReplyEntities.length;i++){
					if(prvReplyEntities[i].getResultCode().getErrorCode() != null && !prvReplyEntities[i].getResultCode().getErrorCode().equalsIgnoreCase("0")){
						ProvisionDataItem[] dataItems = prvReplyEntities[i].getItems();
						for(int n = 0; n<dataItems.length; n++){
							System.out.println(dataItems[n].getName()+": "+dataItems[n].getData());
						}
						if(properties.getProperty(reply.getHeader().getMajorErrCode())!= null){
							System.out.println(prvReplyEntities[i].getResultCode().getErrorCode());
							logger.debug("Prv Reply Entity Error Code for "+subscriberDetail.getMsisdn()+", :"+prvReplyEntities[i].getResultCode().getErrorCode());
							logger.info("exiting suspendSubscription()...");
							return new Integer(properties.getProperty(reply.getHeader().getMajorErrCode())).intValue();
						}else{
							System.out.println(prvReplyEntities[i].getResultCode().getErrorCode());
							logger.debug("Prv Reply Entity Error Code for "+subscriberDetail.getMsisdn()+", :"+prvReplyEntities[i].getResultCode().getErrorCode());
							logger.info("exiting suspendSubscription()...");
							return FAIL;
						}					
					}
				}
				logger.info("exiting suspendSubscription()...");
				return FAIL;
				
			}else{
				//success
				//log successful request;
				logger.info("exiting suspendSubscription()...");
				return SUCCESS;
			}
			
		}catch(Exception e){
			e.printStackTrace();
			//error... Log exception
			logger.info("exiting suspendSubscription()...");
			return FAIL;
		}
	}
	
	
	public int checkBISSUbscriptionStatus(SubscriberDetail subscriberDetail)
	{
		
		try{//properties = new LoadAllProperties();
				logger.debug("activateSubscription() called..."+RIMQueryUtil.class.getName());
				System.out.println("activateSubscription() called..."+RIMQueryUtil.class.getName());
				String transactionId = new Long(System.currentTimeMillis()).toString();
				String carrierLoginId = properties.getProperty("RIMCarrierLoginId"); 
				String carrierPassword = properties.getProperty("RIMCarrierPwd");
				
				logger.info("The value of requested service is: "+subscriberDetail.getServicetype());
				logger.info("The value of requesting IMSI is: "+subscriberDetail.getImsi());
				logger.info("The Requesting channel is: USSD CODE");
				logger.info("The value of requesting MSISDN is: "+subscriberDetail.getMsisdn());
				logger.info("Service is : "+subscriberDetail.getServiceplan());
				
				
				ProvisionReqEntity[] provisionRequestEntityArr = new ProvisionReqEntity[1];
				
				ProvisionDataItem[] provisionDataItemArr = new ProvisionDataItem[1];
				
				ProvisionDataItem provisionDataItemThree = 
					new ProvisionDataItem(subscriberDetail.getImsi(),"BillingId",null);
				
				provisionDataItemArr[0] = provisionDataItemThree;
												
				ProvisionReqEntity provisionRequestEntity = 
					new ProvisionReqEntity(provisionDataItemArr,"Prosumer",null,null,"subscriber");
				
				
				provisionRequestEntityArr[0] = provisionRequestEntity;
				
				//Zain's going to provide us with most of this
				ProvisionSender provisionSender = new ProvisionSender("",carrierLoginId,"",null,carrierPassword,"");
				java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
				String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
				ProvisionReqHeader requestHeader = new ProvisionReqHeader(null,provisionSender,"CCYY-MM-DDThh:mm:ssTZD",timeStamp);
				ProvisionRequest provisionRequest = new ProvisionRequest(provisionRequestEntityArr,requestHeader,null,"BlackBerry","",transactionId,"Status","1.2");
				ProvisioningPortProxy proxy = new ProvisioningPortProxy();
				ProvisionReply reply = proxy.submitSync(provisionRequest);
				//we should check that the reply matches our initial request
				
				logger.info("request.loginId: "+provisionSender.getLoginId());
				logger.info("reply == ProductType: "+reply.getProductType());
				logger.info("reply == TransactionId"+reply.getTransactionId());
				logger.info("reply == TransactionType"+reply.getTransactionType());
				logger.info("reply == "+reply.getHeader().getSender());
				
				if(reply != null){
					System.out.println("reply.getHeader(): "+reply.getHeader());
					logger.info("reply.getHeader(): "+reply.getHeader());
				}
				if(reply.getHeader() != null){
					System.out.println("reply.getHeader().getSender(): "+reply.getHeader().getSender());
					logger.info("reply.getHeader().getSender(): "+reply.getHeader().getSender());
				}
				if(reply.getHeader().getSender() != null){
					System.out.println("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
					logger.info("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
				}
				logger.info("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				logger.info("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getBody());
				//check for errors				
				
				ProvisionReplyEntity[] prvReplyEntities1 = reply.getBody();
				
				//if (prvReplyEntities1.leng)
				
				
				for (ProvisionReplyEntity entity : prvReplyEntities1){
					logger.info(entity.getName());					
					
					logger.info("Current subscriber ["+subscriberDetail.getMsisdn()+
							"] status on RIM is "+entity.getResultCode().getStatusDescription());
					if (entity.getResultCode().getStatusDescription() != null &&
							entity.getResultCode().getStatusDescription().startsWith("Deactivated"))
					{
						logger.info(entity.getItems()[1].getData());
						logger.info("Subscriber can activate new service, proceeding");
						return SUCCESS;
					}
					else if (entity.getResultCode().getStatusDescription() != null && 
							entity.getResultCode().getStatusDescription().startsWith("Activated")) 
					{
						logger.info("Subscriber is already active on RIM so activation cannot continue. Aborting ....");
						return FAIL;
					}
					if (entity.getResultCode().getStatusDescription() == null )
					{
						logger.info("Subscriber not found on RIM, therefore can activate new service, proceeding");
						return SUCCESS;
					}
				}
				return FAIL;
								
				
			}catch(Exception e){
				e.printStackTrace();				
				//log exception				
				return 16;
			}
	
	}
	
	
	
	/*public HashMap<String,String> suspendSubscription(ArrayList<SubscriberDetail> suspendedSubscribersList){
		
		 * List of suspended subscribers is passed to RIM server via agreed protocol
		 * RIM API will proceed to suspend the subscriber from the service.
		 
		try{
			HashMap<String,String> resultsMap = new HashMap<String,String>();
			String transactionId = new Long(System.currentTimeMillis()).toString();
			String carrierLoginId = properties.getProperty("RIMCarrierLoginId"); 
			String carrierPassword = properties.getProperty("RIMCarrierPwd");
			Iterator<SubscriberDetail> it = suspendedSubscribersList.iterator();
			ProvisionReqEntity[] provisionRequestEntityArr = new ProvisionReqEntity[suspendedSubscribersList.size()];			
			int count = 0;
			SubscriberDetail subscriberDetail = new SubscriberDetail();
			while(it.hasNext()){
				subscriberDetail = (SubscriberDetail)it.next();
				logger.info("received request to suspend subscription for "+subscriberDetail.getMsisdn()+" ...");
				ProvisionDataItem[] provisionDataItemArr = new ProvisionDataItem[4];
				//Logic to contact RIM asking for suspension of currentSubscriber's IMEI
				ProvisionDataItem provisionDataItemOne = new ProvisionDataItem(subscriberDetail.getMsisdn(),"MSISDN",null);
				ProvisionDataItem provisionDataItemTwo = new ProvisionDataItem(subscriberDetail.getImei(),"IMEI",null);
				ProvisionDataItem provisionDataItemThree = new ProvisionDataItem(subscriberDetail.getImsi(),"IMSI",null);
				ProvisionDataItem provisionDataItemFour = new ProvisionDataItem(subscriberDetail.getPin(),"PIN",null);
				provisionDataItemArr[0] = provisionDataItemOne;
				provisionDataItemArr[1] = provisionDataItemTwo;
				provisionDataItemArr[2] = provisionDataItemThree;
				provisionDataItemArr[3] = provisionDataItemFour;
				ProvisionReqEntity provisionRequestEntity = new ProvisionReqEntity(provisionDataItemArr,"service",null,null,subscriberDetail.getServicetype());
				provisionRequestEntityArr[count] = provisionRequestEntity;
				count++;
			}
			//Celtel's going to provide us with most of this
			ProvisionSender provisionSender = new ProvisionSender("",carrierLoginId,"",null,carrierPassword,"");
			java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
			String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
			ProvisionReqHeader requestHeader = new ProvisionReqHeader(null,provisionSender,"CCYY-MM-DDThh:mm:ssTZD",timeStamp);
			ProvisionRequest provisionRequest = new ProvisionRequest(provisionRequestEntityArr,requestHeader,null,"BlackBerry","",transactionId,"Suspend","1.2");
			ProvisioningPortProxy proxy = new ProvisioningPortProxy();
			logger.info("sending provisioning request for "+subscriberDetail.getMsisdn()+". Action is SUSPEND.");
			ProvisionReply reply = proxy.submitSync(provisionRequest);
			//we should check that the reply matches our initial request
			System.out.println("request.loginId: "+provisionSender.getLoginId());
			if(reply != null){
				System.out.println("reply.getHeader(): "+reply.getHeader());
			}
			if(reply.getHeader() != null){
				System.out.println("reply.getHeader().getSender(): "+reply.getHeader().getSender());
			}
			if(reply.getHeader().getSender() != null){
				System.out.println("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
			}
			System.out.println("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
			System.out.println("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
			//check for errors
			
				//an error occured, complete or partial
				ProvisionReplyEntity[] prvReplyEntities = reply.getBody();
				String subscriberImsi = "";
				for(int i=0; i<prvReplyEntities.length;i++){
					if(prvReplyEntities[i].getResultCode().getErrorCode() != null && !prvReplyEntities[i].getResultCode().getErrorCode().equalsIgnoreCase("0")){
						ProvisionDataItem[] dataItems = prvReplyEntities[i].getItems();
						for(int n = 0; n<dataItems.length; n++){
							System.out.println(dataItems[n].getName()+": "+dataItems[n].getData());
						}
						System.out.println(prvReplyEntities[i].getResultCode().getErrorCode());
						subscriberImsi = dataItems[2].getData();
					}
					resultsMap.put("IMSI: "+subscriberImsi,prvReplyEntities[i].getResultCode().getErrorCode());
				}
				
				return resultsMap;
			
		}catch(Exception e){
			e.printStackTrace();
			//error... Log exception			
			return new HashMap<String,String>();
		}
		
	}*/
	
	
	public int activateSubscriptionByUSSDChannel(SubscriberDetail subscriberDetail){
				
		try{
				logger.debug("activateSubscription() called..."+RIMQueryUtil.class.getName());
				System.out.println("activateSubscription() called..."+RIMQueryUtil.class.getName());
				String transactionId = new Long(System.currentTimeMillis()).toString();
				String carrierLoginId = properties.getProperty("RIMCarrierLoginId"); 
				String carrierPassword = properties.getProperty("RIMCarrierPwd");
				
				logger.info("The value of requested service is: "+subscriberDetail.getServicetype());
				logger.info("The value of requesting IMSI is: "+subscriberDetail.getImsi());
				logger.info("The Requesting channel is: USSD CODE");
				logger.info("The value of requesting MSISDN is: "+subscriberDetail.getMsisdn());
				logger.info("Service is : "+subscriberDetail.getServiceplan());
				
				
				ProvisionReqEntity[] provisionRequestEntityArr = new ProvisionReqEntity[1];
				
				ProvisionDataItem[] provisionDataItemArr = new ProvisionDataItem[1];
				
				ProvisionDataItem provisionDataItemThree = 
					new ProvisionDataItem(subscriberDetail.getImsi(),"BillingId",null);
				
				provisionDataItemArr[0] = provisionDataItemThree;
				
								
				ProvisionDataItem[] provisionDataItemArr1 = new ProvisionDataItem[1];
				provisionDataItemArr1[0] = new ProvisionDataItem(subscriberDetail.getServiceplan().trim(),
						"ServiceName",null);
				ProvisionDataItem[] provisionDataItemArr2 = new ProvisionDataItem[1];
				provisionDataItemArr2[0] = new ProvisionDataItem("Internet Browsing","ServiceName",null);
				
				ProvisionReqEntity provisionRequestEntityTwo = 
					new ProvisionReqEntity(provisionDataItemArr1,"Prosumer Service",null,null,"service");
				ProvisionReqEntity provisionRequestEntityThree = 
					new ProvisionReqEntity(provisionDataItemArr2,"Prosumer Service",null,null,"service");
				
				ProvisionReqEntity[] provisionServicePH = new ProvisionReqEntity[2];				
				provisionServicePH[0] = provisionRequestEntityTwo;
				provisionServicePH[1] = provisionRequestEntityThree;
				
								
				ProvisionReqEntity provisionRequestEntity = 
					new ProvisionReqEntity(provisionDataItemArr,"Prosumer",null,provisionServicePH,"subscriber");
				
				
				provisionRequestEntityArr[0] = provisionRequestEntity;
				
				//Zain's going to provide us with most of this
				ProvisionSender provisionSender = new ProvisionSender("",carrierLoginId,"",null,carrierPassword,"");
				java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
				String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
				ProvisionReqHeader requestHeader = new ProvisionReqHeader(null,provisionSender,"CCYY-MM-DDThh:mm:ssTZD",timeStamp);
				ProvisionRequest provisionRequest = new ProvisionRequest(provisionRequestEntityArr,requestHeader,null,"BlackBerry","",transactionId,"Activate","1.2");
				ProvisioningPortProxy proxy = new ProvisioningPortProxy();
				ProvisionReply reply = proxy.submitSync(provisionRequest);
				//we should check that the reply matches our initial request
				
				logger.info("request.loginId: "+provisionSender.getLoginId());
				logger.info("reply == ProductType: "+reply.getProductType());
				logger.info("reply == TransactionId"+reply.getTransactionId());
				logger.info("reply == TransactionType"+reply.getTransactionType());
				logger.info("reply == "+reply.getHeader().getSender());
				
				if(reply != null){
					System.out.println("reply.getHeader(): "+reply.getHeader());
					logger.info("reply.getHeader(): "+reply.getHeader());
				}
				if(reply.getHeader() != null){
					System.out.println("reply.getHeader().getSender(): "+reply.getHeader().getSender());
					logger.info("reply.getHeader().getSender(): "+reply.getHeader().getSender());
				}
				if(reply.getHeader().getSender() != null){
					System.out.println("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
					logger.info("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
				}
				logger.info("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				logger.info("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getBody());
				//check for errors
				
				
				if(reply.getHeader().getMajorErrCode() != null && 
						!(reply.getHeader().getMajorErrCode().equalsIgnoreCase("0"))){
					String majorErrorCode = reply.getHeader().getMajorErrCode();
					ProvisionReplyEntity[] prvReplyEntities = reply.getBody();
					
					logger.info("===== An error has occured, about to determine severity of error.");
					
					//for(int i=0; i<prvReplyEntities.length;i++){
						
						for (ProvisionReplyEntity entity : prvReplyEntities) {
							ProvisionReplyEntity replyEntity[] =  entity.getSubEntities();
							if(properties.getProperty("majorerror."+majorErrorCode)!= null && 
									properties.getProperty("majorerror."+majorErrorCode).equalsIgnoreCase("warning")){
								logger.info("===== A warning has been raised by RIM, continuing with activation.");
								logger.info("Value of major error "+majorErrorCode);
								
								
								
								for (ProvisionReplyEntity entity2 : replyEntity) {
									if(new Integer(entity2.getResultCode().getErrorCode()).intValue() 
											== 21020 && new Integer(majorErrorCode).intValue() == 21000){
										
										logger.info("===== Warning is about state, return warning and continue as successful");																			
									}
									else{
										logger.info("===== A major error has occured, about to exit activate method.");										
																			
									}
									logger.debug("Value of error "+entity2.getResultCode().getErrorCode());
									logger.debug("Value of major error "+majorErrorCode);	
									return new Integer(entity2.getResultCode().getErrorCode()).intValue();
								}								
							}
							else{
								logger.info("Inside of Else block");
								
								for (ProvisionReplyEntity entity2 : replyEntity) {
									logger.info(entity2.getResultCode().getErrorCode());
									//return new Integer(entity2.getResultCode().getErrorCode()).intValue();
								}
								return new Integer(majorErrorCode).intValue();
							}
						}									
					return FAIL;
				}else{					
					return SUCCESS;
				}
				
				
			}catch(Exception e){
				e.printStackTrace();				
				//log exception				
				return FAIL;
			}
	
	}
	
	
	
	public int activateSubscriptionByUSSDChannel(SubscriberDetail subscriberDetail, String[] serviceNames){
		
		try{
				ProvisionReqEntity[] provisionServicePH = new ProvisionReqEntity[serviceNames.length];
				int counter = 0;
			
				logger.debug("activateSubscription() called..."+RIMQueryUtil.class.getName());			
				logger.info("The value of requested service length is: "+subscriberDetail.getServicetype());
				logger.info("The value of requesting IMSI is: "+subscriberDetail.getImsi());
				logger.info("The value of requesting MSISDN is: "+subscriberDetail.getMsisdn());
				logger.info("Service Name is : "+subscriberDetail.getServiceplan());
				
				String transactionId = new Long(System.currentTimeMillis()).toString();
				String carrierLoginId = properties.getProperty("RIMCarrierLoginId"); 
				String carrierPassword = properties.getProperty("RIMCarrierPwd");
				
				ProvisionReqEntity[] provisionRequestEntityArr = new ProvisionReqEntity[1];
				
				ProvisionDataItem[] provisionDataItemArr = new ProvisionDataItem[1];
				
				ProvisionDataItem provisionDataItemThree = 
					new ProvisionDataItem(subscriberDetail.getImsi(),"BillingId",null);
				
				provisionDataItemArr[0] = provisionDataItemThree;		
				
				/*
				 * Loop through the services and build an object wrapper in the process.
				 */				
				ProvisionDataItem[] provisionDataItemArr11;
				ProvisionReqEntity provisionRequestEntityTwo1;
				
				for (String string : serviceNames) {
					provisionDataItemArr11 = new ProvisionDataItem[1];
					provisionDataItemArr11[0] = new ProvisionDataItem(string.trim(), "ServiceName",null);
					
					provisionRequestEntityTwo1 = new ProvisionReqEntity(provisionDataItemArr11,"Prosumer Service",null,null,"service");
					
					provisionServicePH[counter] = provisionRequestEntityTwo1;
					counter++;
				}				
				
				
				ProvisionReqEntity provisionRequestEntity = 
					new ProvisionReqEntity(provisionDataItemArr,"Prosumer",null,provisionServicePH,"subscriber");
				
				
				provisionRequestEntityArr[0] = provisionRequestEntity;
				
				//Zain's going to provide us with most of this
				ProvisionSender provisionSender = new ProvisionSender("",carrierLoginId,"",null,carrierPassword,"");
				java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
				String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
				ProvisionReqHeader requestHeader = new ProvisionReqHeader(null,provisionSender,"CCYY-MM-DDThh:mm:ssTZD",timeStamp);
				ProvisionRequest provisionRequest = new ProvisionRequest(provisionRequestEntityArr,requestHeader,null,"BlackBerry","",transactionId,"Activate","1.2");
				ProvisioningPortProxy proxy = new ProvisioningPortProxy();
				ProvisionReply reply = proxy.submitSync(provisionRequest);
				//we should check that the reply matches our initial request
				
				logger.info("request.loginId: "+provisionSender.getLoginId());
				logger.info("reply == ProductType: "+reply.getProductType());
				logger.info("reply == TransactionId"+reply.getTransactionId());
				logger.info("reply == TransactionType"+reply.getTransactionType());
				logger.info("reply == "+reply.getHeader().getSender());
				
				if(reply != null){
					System.out.println("reply.getHeader(): "+reply.getHeader());
					logger.info("reply.getHeader(): "+reply.getHeader());
				}
				if(reply.getHeader() != null){
					System.out.println("reply.getHeader().getSender(): "+reply.getHeader().getSender());
					logger.info("reply.getHeader().getSender(): "+reply.getHeader().getSender());
				}
				if(reply.getHeader().getSender() != null){
					System.out.println("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
					logger.info("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
				}
				logger.info("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				logger.info("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getBody());
				//check for errors
				
				
				if(reply.getHeader().getMajorErrCode() != null && 
						!(reply.getHeader().getMajorErrCode().equalsIgnoreCase("0"))){
					String majorErrorCode = reply.getHeader().getMajorErrCode();
					ProvisionReplyEntity[] prvReplyEntities = reply.getBody();
					
					logger.info("===== An error has occured, about to determine severity of error.");
					
					//for(int i=0; i<prvReplyEntities.length;i++){
						
						for (ProvisionReplyEntity entity : prvReplyEntities) {
							ProvisionReplyEntity replyEntity[] =  entity.getSubEntities();
							if(properties.getProperty("majorerror."+majorErrorCode)!= null && 
									properties.getProperty("majorerror."+majorErrorCode).equalsIgnoreCase("warning")){
								logger.info("===== A warning has been raised by RIM, continuing with activation.");
								logger.info("Value of major error "+majorErrorCode);
								
								
								
								for (ProvisionReplyEntity entity2 : replyEntity) {
									if(new Integer(entity2.getResultCode().getErrorCode()).intValue() 
											== 21020 && new Integer(majorErrorCode).intValue() == 21000){
										
										logger.info("===== Warning is about state, return warning and continue as successful");																			
									}
									else{
										logger.info("===== A major error has occured, about to exit activate method.");										
																			
									}
									logger.debug("Value of error "+entity2.getResultCode().getErrorCode());
									logger.debug("Value of major error "+majorErrorCode);	
									return new Integer(entity2.getResultCode().getErrorCode()).intValue();
								}								
							}
							else{
								logger.info("Inside of Else block");
								
								for (ProvisionReplyEntity entity2 : replyEntity) {
									logger.info(entity2.getResultCode().getErrorCode());
									//return new Integer(entity2.getResultCode().getErrorCode()).intValue();
								}
								return new Integer(majorErrorCode).intValue();
							}
						}									
					return FAIL;
				}else{					
					return SUCCESS;
				}
				
				
			}catch(Exception e){
				e.printStackTrace();				
				//log exception				
				return FAIL;
			}
	
	}
	
	
	/**
	 * This method is used to activate a subscriber on only one service on RIM. This service is usually the services that donot use Internet
	 * Browsing, e.g BB Networking
	 * Added by Nnamdi Jibunoh on 27th-Mar-2011
	 * @param subscriberDetail
	 * @return status
	 */
	public int activateSingleServiceSubscription(SubscriberDetail subscriberDetail){
				
		try{
				logger.debug("activateSubscription() called..."+RIMQueryUtil.class.getName());
				System.out.println("activateSubscription() called..."+RIMQueryUtil.class.getName());
				String transactionId = new Long(System.currentTimeMillis()).toString();
				String carrierLoginId = properties.getProperty("RIMCarrierLoginId"); 
				String carrierPassword = properties.getProperty("RIMCarrierPwd");
				
				logger.info("The value of requested service is: "+subscriberDetail.getServicetype());
				logger.info("The value of requesting IMSI is: "+subscriberDetail.getImsi());
				logger.info("The Requesting channel is: USSD CODE");
				logger.info("The value of requesting MSISDN is: "+subscriberDetail.getMsisdn());
				logger.info("Service is : "+subscriberDetail.getServiceplan());
				
				
				ProvisionReqEntity[] provisionRequestEntityArr = new ProvisionReqEntity[1];
				
				ProvisionDataItem[] provisionDataItemArr = new ProvisionDataItem[1];
				
				ProvisionDataItem provisionDataItemThree = 
					new ProvisionDataItem(subscriberDetail.getImsi(),"BillingId",null);
				
				provisionDataItemArr[0] = provisionDataItemThree;
				
				
				ProvisionDataItem[] provisionDataItemArr1 = new ProvisionDataItem[1];
				provisionDataItemArr1[0] = new ProvisionDataItem(subscriberDetail.getServiceplan().trim(),
						"ServiceName",null);
				/*ProvisionDataItem[] provisionDataItemArr2 = new ProvisionDataItem[1];
				provisionDataItemArr2[0] = new ProvisionDataItem("Internet Browsing","ServiceName",null);*/
				
				ProvisionReqEntity provisionRequestEntityTwo = 
					new ProvisionReqEntity(provisionDataItemArr1,"Prosumer Service",null,null,"service");
				/*ProvisionReqEntity provisionRequestEntityThree = 
					new ProvisionReqEntity(provisionDataItemArr2,"Prosumer Service",null,null,"service");*/
				
				ProvisionReqEntity[] provisionServicePH = new ProvisionReqEntity[1];				
				provisionServicePH[0] = provisionRequestEntityTwo;
				//provisionServicePH[0] = provisionRequestEntityThree;
				
				
				ProvisionReqEntity provisionRequestEntity = 
					new ProvisionReqEntity(provisionDataItemArr,"Prosumer",null,provisionServicePH,"subscriber");
				
				
				provisionRequestEntityArr[0] = provisionRequestEntity;
				
				//Zain's going to provide us with most of this
				ProvisionSender provisionSender = new ProvisionSender("",carrierLoginId,"",null,carrierPassword,"");
				java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
				String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
				ProvisionReqHeader requestHeader = new ProvisionReqHeader(null,provisionSender,"CCYY-MM-DDThh:mm:ssTZD",timeStamp);
				ProvisionRequest provisionRequest = new ProvisionRequest(provisionRequestEntityArr,requestHeader,null,"BlackBerry","",transactionId,"Activate","1.2");
				ProvisioningPortProxy proxy = new ProvisioningPortProxy();
				ProvisionReply reply = proxy.submitSync(provisionRequest);
				//we should check that the reply matches our initial request
				
				logger.info("request.loginId: "+provisionSender.getLoginId());
				logger.info("reply == ProductType: "+reply.getProductType());
				logger.info("reply == TransactionId"+reply.getTransactionId());
				logger.info("reply == TransactionType"+reply.getTransactionType());
				logger.info("reply == "+reply.getHeader().getSender());
				
				if(reply != null){
					System.out.println("reply.getHeader(): "+reply.getHeader());
					logger.info("reply.getHeader(): "+reply.getHeader());
				}
				if(reply.getHeader() != null){
					System.out.println("reply.getHeader().getSender(): "+reply.getHeader().getSender());
					logger.info("reply.getHeader().getSender(): "+reply.getHeader().getSender());
				}
				if(reply.getHeader().getSender() != null){
					System.out.println("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
					logger.info("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
				}
				logger.info("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				logger.info("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getBody());
				//check for errors
				
				
				if(reply.getHeader().getMajorErrCode() != null && 
						!(reply.getHeader().getMajorErrCode().equalsIgnoreCase("0"))){
					String majorErrorCode = reply.getHeader().getMajorErrCode();
					ProvisionReplyEntity[] prvReplyEntities = reply.getBody();
					
					logger.info("===== An error has occured, about to determine severity of error.");
					
					//for(int i=0; i<prvReplyEntities.length;i++){
						
						for (ProvisionReplyEntity entity : prvReplyEntities) {
							ProvisionReplyEntity replyEntity[] =  entity.getSubEntities();
							if(properties.getProperty("majorerror."+majorErrorCode)!= null && 
									properties.getProperty("majorerror."+majorErrorCode).equalsIgnoreCase("warning")){
								logger.info("===== A warning has been raised by RIM, continuing with activation.");
								logger.info("Value of major error "+majorErrorCode);
								
								
								
								for (ProvisionReplyEntity entity2 : replyEntity) {
									if(new Integer(entity2.getResultCode().getErrorCode()).intValue() 
											== 21020 && new Integer(majorErrorCode).intValue() == 21000){
										
										logger.info("===== Warning is about state, return warning and continue as successful");																			
									}
									else{
										logger.info("===== A major error has occured, about to exit activate method.");										
																			
									}
									logger.debug("Value of error "+entity2.getResultCode().getErrorCode());
									logger.debug("Value of major error "+majorErrorCode);	
									return new Integer(entity2.getResultCode().getErrorCode()).intValue();
								}								
							}
							else{
								logger.info("Inside of Else block");
								
								for (ProvisionReplyEntity entity2 : replyEntity) {
									logger.info(entity2.getResultCode().getErrorCode());
									//return new Integer(entity2.getResultCode().getErrorCode()).intValue();
								}
								return new Integer(majorErrorCode).intValue();
							}
						}									
					return FAIL;
				}else{					
					return SUCCESS;
				}
				
				
			}catch(Exception e){
				e.printStackTrace();				
				//log exception				
				return FAIL;
			}
	
	}
	
	
	
	
	public int activateSubscription(SubscriberDetail subscriberDetail){
		/*
		 * List of subscribers to be activated on BlackBerry service on RIM
		 */
		
		try{
				System.out.println("activateSubscription()...");
				logger.debug("activateSubscription() called..."+RIMQueryUtil.class.getName());
				System.out.println("activateSubscription() called..."+RIMQueryUtil.class.getName());
				String transactionId = new Long(System.currentTimeMillis()).toString();
				String carrierLoginId = properties.getProperty("RIMCarrierLoginId"); 
				String carrierPassword = properties.getProperty("RIMCarrierPwd");
				logger.info("The value of requested service is: "+subscriberDetail.getServicetype());
				logger.info("The value of requested service is: "+subscriberDetail.getServiceplan());
				logger.info("The value of requesting IMSI is: "+subscriberDetail.getImsi());
				logger.info("The value of requesting PIN is: "+subscriberDetail.getPin());
				logger.info("The value of requesting MSISDN is: "+subscriberDetail.getMsisdn());
				
				
				/*String servicePlan = subscriberDetail.getServiceplan().trim();
				if (subscriberDetail.getServicetype().contains("Enterprise") || 
						subscriberDetail.getServicetype().startsWith("Enterprise")){
					subscriberDetail.getServicetype().trim();
					if ("Enterprise".equalsIgnoreCase(subscriberDetail.getServicetype()))
						servicePlan = "Enterprise";
				}*/
				
				ProvisionReqEntity[] provisionRequestEntityArr = new ProvisionReqEntity[1];
				//ProvisionReqEntity pro = new ProvisionReqEntity();
				
				ProvisionDataItem[] provisionDataItemArr = new ProvisionDataItem[1];
				//ProvisionDataItem[] provisionDataItemArr = new ProvisionDataItem[4];
				//Logic to contact RIM asking for suspension of currentSubscriber's IMEI
				/*ProvisionDataItem provisionDataItemOne = new ProvisionDataItem(subscriberDetail.getMsisdn(),"MSISDN",null);
				ProvisionDataItem provisionDataItemTwo = new ProvisionDataItem(subscriberDetail.getImei(),"IMEI",null);*/
				ProvisionDataItem provisionDataItemThree = new ProvisionDataItem(subscriberDetail.getImsi(),"IMSI",null);
				//ProvisionDataItem provisionDataItemFour = new ProvisionDataItem(subscriberDetail.getPin(),"PIN",null);
				//provisionDataItemArr[0] = provisionDataItemOne;
				provisionDataItemArr[0] = provisionDataItemThree;
				/*provisionDataItemArr[1] = provisionDataItemTwo;
				provisionDataItemArr[2] = provisionDataItemThree;
				provisionDataItemArr[3] = provisionDataItemFour;*/
				
				ProvisionDataItem[] provisionDataItemArr1 = new ProvisionDataItem[1];
				provisionDataItemArr1[0] = new ProvisionDataItem(subscriberDetail.getServiceplan().trim(),"ServiceName",null);
				ProvisionDataItem[] provisionDataItemArr2 = new ProvisionDataItem[1];
				provisionDataItemArr2[0] = new ProvisionDataItem("Internet Browsing","ServiceName",null);
				
				ProvisionReqEntity provisionRequestEntityTwo = 
					new ProvisionReqEntity(provisionDataItemArr1,"Prosumer Service",null,null,"service");
				ProvisionReqEntity provisionRequestEntityThree = 
					new ProvisionReqEntity(provisionDataItemArr2,"Prosumer Service",null,null,"service");
				
				ProvisionReqEntity[] provisionServicePH = new ProvisionReqEntity[2];				
				provisionServicePH[0] = provisionRequestEntityTwo;
				provisionServicePH[1] = provisionRequestEntityThree;
				
				
				ProvisionReqEntity provisionRequestEntity = 
					new ProvisionReqEntity(provisionDataItemArr,"Prosumer",null,provisionServicePH,"subscriber");
				
				
				provisionRequestEntityArr[0] = provisionRequestEntity;
				
				//Zain's going to provide us with most of this
				ProvisionSender provisionSender = new ProvisionSender("",carrierLoginId,"",null,carrierPassword,"");
				java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
				String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
				ProvisionReqHeader requestHeader = new ProvisionReqHeader(null,provisionSender,"CCYY-MM-DDThh:mm:ssTZD",timeStamp);
				ProvisionRequest provisionRequest = new ProvisionRequest(provisionRequestEntityArr,requestHeader,null,"BlackBerry","",transactionId,"Activate","1.2");
				ProvisioningPortProxy proxy = new ProvisioningPortProxy();
				ProvisionReply reply = proxy.submitSync(provisionRequest);
				//we should check that the reply matches our initial request
				
				logger.info("request.loginId: "+provisionSender.getLoginId());
				logger.info("reply == ProductType: "+reply.getProductType());
				logger.info("reply == TransactionId"+reply.getTransactionId());
				logger.info("reply == TransactionType"+reply.getTransactionType());
				logger.info("reply == "+reply.getHeader().getSender());
				
				if(reply != null){
					System.out.println("reply.getHeader(): "+reply.getHeader());
					logger.info("reply.getHeader(): "+reply.getHeader());
				}
				if(reply.getHeader() != null){
					System.out.println("reply.getHeader().getSender(): "+reply.getHeader().getSender());
					logger.info("reply.getHeader().getSender(): "+reply.getHeader().getSender());
				}
				if(reply.getHeader().getSender() != null){
					System.out.println("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
					logger.info("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
				}
				System.out.println("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				System.out.println("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				logger.info("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				//check for errors
								
				if(reply.getHeader().getMajorErrCode()!= null && !(reply.getHeader().getMajorErrCode().equalsIgnoreCase("0"))){
					String majorErrorCode = reply.getHeader().getMajorErrCode();
					ProvisionReplyEntity[] prvReplyEntities = reply.getBody();
					
					System.out.println("===== An error has occured, about to determine severity of error.");
					
					//for(int i=0; i<prvReplyEntities.length;i++){
						
						for (ProvisionReplyEntity entity : prvReplyEntities) {
							
							if(properties.getProperty("majorerror."+majorErrorCode)!= null && properties.getProperty("majorerror."+majorErrorCode).equalsIgnoreCase("warning")){
								logger.info("===== A warning has been raised by RIM, continuing with activation.");
								logger.info("Value of major error "+majorErrorCode);
								
								ProvisionReplyEntity replyEntity[] =  entity.getSubEntities();
								
								for (ProvisionReplyEntity entity2 : replyEntity) {
									if(new Integer(entity2.getResultCode().getErrorCode()).intValue() 
											== 21020 && new Integer(majorErrorCode).intValue() == 21000){
										
										System.out.println("===== Warning is about state, return warning and continue as successful");																			
									}
									else{
										System.out.println("===== A major error has occured, about to exit activate method.");										
																			
									}
									System.out.println("Value of error "+entity2.getResultCode().getErrorCode());
									System.out.println("Value of major error "+majorErrorCode);	
									return new Integer(entity2.getResultCode().getErrorCode()).intValue();
								}								
							}
							else{
								ProvisionReplyEntity replyEntity[] =  entity.getSubEntities();
								
								for (ProvisionReplyEntity entity2 : replyEntity) {
									return new Integer(entity2.getResultCode().getErrorCode()).intValue();
								}
								return new Integer(majorErrorCode).intValue();
							}
						}
						
					
					//}										
					return FAIL;
				}else{					
					return SUCCESS;
				}
				
				
			}catch(Exception e){
				e.printStackTrace();				
				//log exception				
				return FAIL;
			}
	}
	
	/*public HashMap<String,String> activateSubscription(ArrayList<SubscriberDetail> subscriberList){
		
		 * List of subscribers to be activated on BlackBerry service on RIM
		 
			try{
				HashMap<String,String> resultsMap = new HashMap<String,String>();
				logger.debug("activateSubscription() called..."+RIMQueryUtil.class.getName());
				System.out.println("activateSubscription() called..."+RIMQueryUtil.class.getName());
				String transactionId = new Long(System.currentTimeMillis()).toString();
				String carrierLoginId = properties.getProperty("RIMCarrierLoginId"); 
				String carrierPassword = properties.getProperty("RIMCarrierPwd");
				Iterator<SubscriberDetail> it = subscriberList.iterator();
				ProvisionReqEntity[] provisionRequestEntityArr = new ProvisionReqEntity[subscriberList.size()];
				ProvisionReqEntity[] provisionRequestEntityArrTwo = new ProvisionReqEntity[subscriberList.size()];
				int count = 0;
				while(it.hasNext()){
					System.out.println("activateSubscription()...Count: "+(count+1));
					SubscriberDetail subscriberDetail = (SubscriberDetail)it.next();
					ProvisionDataItem[] provisionDataItemArr = new ProvisionDataItem[4];
					//Logic to contact RIM asking for suspension of currentSubscriber's IMEI
					ProvisionDataItem provisionDataItemOne = new ProvisionDataItem(subscriberDetail.getMsisdn(),"MSISDN",null);
					ProvisionDataItem provisionDataItemTwo = new ProvisionDataItem(subscriberDetail.getImei(),"IMEI",null);
					ProvisionDataItem provisionDataItemThree = new ProvisionDataItem(subscriberDetail.getImsi(),"IMSI",null);
					ProvisionDataItem provisionDataItemFour = new ProvisionDataItem(subscriberDetail.getPin(),"PIN",null);
					provisionDataItemArr[0] = provisionDataItemOne;
					provisionDataItemArr[1] = provisionDataItemTwo;
					provisionDataItemArr[2] = provisionDataItemThree;
					provisionDataItemArr[3] = provisionDataItemFour;
					ProvisionReqEntity provisionRequestEntity = new ProvisionReqEntity(provisionDataItemArr,"service",null,null,subscriberDetail.getServicetype());
					ProvisionReqEntity provisionRequestEntityTwo = new ProvisionReqEntity(provisionDataItemArr,"service",null,null,"Internet Browsing");
					provisionRequestEntityArr[count] = provisionRequestEntity;
					provisionRequestEntityArrTwo[count] = provisionRequestEntityTwo;
					count++;
				}
				//Zain's going to provide us with most of this
				ProvisionSender provisionSender = new ProvisionSender("",carrierLoginId,"",null,carrierPassword,"");
				java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
				String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
				ProvisionReqHeader requestHeader = new ProvisionReqHeader(null,provisionSender,"CCYY-MM-DDThh:mm:ssTZD",timeStamp);
				ProvisionRequest provisionRequest = new ProvisionRequest(provisionRequestEntityArr,requestHeader,null,"BlackBerry","",transactionId,"Activate","1.2");
				ProvisionRequest provisionRequestTwo = new ProvisionRequest(provisionRequestEntityArrTwo,requestHeader,null,"BlackBerry","",transactionId,"Activate","1.2");
				ProvisioningPortProxy proxy = new ProvisioningPortProxy();
				ProvisionReply reply = proxy.submitSync(provisionRequest);
				ProvisionReply secondReply = proxy.submitSync(provisionRequestTwo);
				//we should check that the reply matches our initial request
				System.out.println("request.loginId: "+provisionSender.getLoginId());
				System.out.println("reply: "+reply);
				logger.info("request.loginId: "+provisionSender.getLoginId());
				logger.info("reply: "+reply);
				if(reply != null){
					System.out.println("reply.getHeader(): "+reply.getHeader());
					logger.info("reply.getHeader(): "+reply.getHeader());
				}
				if(reply.getHeader() != null){
					System.out.println("reply.getHeader().getSender(): "+reply.getHeader().getSender());
					logger.info("reply.getHeader().getSender(): "+reply.getHeader().getSender());
				}
				if(reply.getHeader().getSender() != null){
					System.out.println("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
					logger.info("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
				}
				System.out.println("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				System.out.println("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				logger.info("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				//check for errors
				String subscriberImsi = "";
					//an error occured, complete or partial
					ProvisionReplyEntity[] prvReplyEntities = reply.getBody();
					for(int i=0; i<prvReplyEntities.length;i++){
						if(prvReplyEntities[i].getResultCode().getErrorCode() != null && !prvReplyEntities[i].getResultCode().getErrorCode().equalsIgnoreCase("0")){
							ProvisionDataItem[] dataItems = prvReplyEntities[i].getItems();
							for(int n = 0; n<dataItems.length; n++){
								System.out.println(dataItems[n].getName()+": "+dataItems[n].getData());
								logger.info(dataItems[n].getName()+": "+dataItems[n].getData());
							}
							subscriberImsi = dataItems[2].getData();
							System.out.println(prvReplyEntities[i].getResultCode().getErrorCode());
							logger.info(prvReplyEntities[i].getResultCode().getErrorCode());
						}
						resultsMap.put("IMSI: "+subscriberImsi,prvReplyEntities[i].getResultCode().getErrorCode());
					}					
					return resultsMap;
			}catch(Exception e){
				e.printStackTrace();				
				//log exception
				
				return new HashMap<String,String>();
			}
	}*/
	
	public int resumeSubscription(SubscriberDetail subscriberDetail){
		/*
		 * List of subscribers to be activated on BlackBerry service on RIM
		 */
			try{
				String transactionId = new Long(System.currentTimeMillis()).toString();
				String carrierLoginId = properties.getProperty("RIMCarrierLoginId"); 
				String carrierPassword = properties.getProperty("RIMCarrierPwd");
				ProvisionReqEntity[] provisionRequestEntityArr = new ProvisionReqEntity[1];			
				
				ProvisionDataItem[] provisionDataItemArr = new ProvisionDataItem[1];
				ProvisionDataItem provisionDataItemThree = new ProvisionDataItem(subscriberDetail.getImsi(),"IMSI",null);
				provisionDataItemArr[0] = provisionDataItemThree;
				ProvisionReqEntity provisionRequestEntity = new ProvisionReqEntity(provisionDataItemArr,"service",null,null,"service");
				provisionRequestEntityArr[0] = provisionRequestEntity;				
				
				//Celtel's going to provide us with most of this
				ProvisionSender provisionSender = new ProvisionSender("",carrierLoginId,"",null,carrierPassword,"");
				java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
				String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
				ProvisionReqHeader requestHeader = new ProvisionReqHeader(null,provisionSender,"CCYY-MM-DDThh:mm:ssTZD",timeStamp);
				ProvisionRequest provisionRequest = new ProvisionRequest(provisionRequestEntityArr,requestHeader,null,"BlackBerry","",transactionId,"Resume","1.2");
				ProvisioningPortProxy proxy = new ProvisioningPortProxy();
				ProvisionReply reply = proxy.submitSync(provisionRequest);
				//we should check that the reply matches our initial request
				System.out.println("request.loginId: "+provisionSender.getLoginId());
				//System.out.println("reply.loginId: "+reply.getHeader().getSender().getLoginId());
				//check for errors
				if(reply.getHeader().getMajorErrCode()!= null && !(reply.getHeader().getMajorErrCode().equalsIgnoreCase("0"))){
					//an error occured, complete or partial
//					Added by Nnamdi Jibunoh.
					String majorErrorCode = reply.getHeader().getMajorErrCode();
					ProvisionReplyEntity[] prvReplyEntities = reply.getBody();
					for(int i=0; i<prvReplyEntities.length;i++){
						if(prvReplyEntities[i].getResultCode().getErrorCode() != null && !prvReplyEntities[i].getResultCode().getErrorCode().equalsIgnoreCase("0")){
							ProvisionDataItem[] dataItems = prvReplyEntities[i].getItems();
							for(int n = 0; n<dataItems.length; n++){
								System.out.println(dataItems[n].getName()+": "+dataItems[n].getData());
							}
							if(properties.getProperty(reply.getHeader().getMajorErrCode())!= null){
								System.out.println(prvReplyEntities[i].getResultCode().getErrorCode());
								//Added by Nnamdi Jibunoh
								if(new Integer(prvReplyEntities[i].getResultCode().getErrorCode()).intValue() == 21030 
										&& new Integer(majorErrorCode).intValue() == 21000)
										return SUCCESS;
								return new Integer(properties.getProperty(reply.getHeader().getMajorErrCode())).intValue();
							}else{
								System.out.println(prvReplyEntities[i].getResultCode().getErrorCode());
//								Added by Nnamdi Jibunoh
								if(new Integer(prvReplyEntities[i].getResultCode().getErrorCode()).intValue()
										== 21030 && new Integer(majorErrorCode).intValue() == 21000)
										return SUCCESS;
								return FAIL;
							}
						}
					}
					return FAIL;
				}else{
					//success
					//log successful request;					
					return SUCCESS;
				}
			}catch(Exception e){
				e.printStackTrace();				
				//log exception
				
				return FAIL;
			}
	}
	
	
	public int modifySubscriptionIMSI(SubscriberDetail oldSubscriberDetail,String newImsi){
		/*
		 * List of subscribers to be activated on BlackBerry service on RIM
		 */
		
		try{
				logger.info("modifySubscriptionIMSI() called..."+RIMQueryUtil.class.getName());
				
				String transactionId = new Long(System.currentTimeMillis()).toString();
				String carrierLoginId = properties.getProperty("RIMCarrierLoginId"); 
				String carrierPassword = properties.getProperty("RIMCarrierPwd");
				
				
				ProvisionReqEntity[] provisionRequestEntityArr = new ProvisionReqEntity[1];
				
				ProvisionDataItem[] provisionDataItemArr = new ProvisionDataItem[3];
				
				ProvisionDataItem provisionDataItemOne = new ProvisionDataItem(oldSubscriberDetail.getImsi(),"OldBillingId",null);
				ProvisionDataItem provisionDataItemTwo = new ProvisionDataItem(newImsi,"IMSI",null);
				ProvisionDataItem provisionDataItemThree = new ProvisionDataItem(oldSubscriberDetail.getMsisdn(),"MSISDN",null);
				provisionDataItemArr[0] = provisionDataItemOne;
				provisionDataItemArr[1] = provisionDataItemTwo;
				provisionDataItemArr[2] = provisionDataItemThree;
											
				
				ProvisionReqEntity provisionRequestEntity = 
					new ProvisionReqEntity(provisionDataItemArr,"Prosumer",null,null,"subscriber");
				
				
				provisionRequestEntityArr[0] = provisionRequestEntity;
				
				//Zain's going to provide us with most of this
				ProvisionSender provisionSender = new ProvisionSender("",carrierLoginId,"",null,carrierPassword,"");
				java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
				String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
				ProvisionReqHeader requestHeader = new ProvisionReqHeader(null,provisionSender,"CCYY-MM-DDThh:mm:ssTZD",timeStamp);
				ProvisionRequest provisionRequest = new ProvisionRequest(provisionRequestEntityArr,requestHeader,null,"BlackBerry","",transactionId,"Modify","1.2");
				ProvisioningPortProxy proxy = new ProvisioningPortProxy();
				ProvisionReply reply = proxy.submitSync(provisionRequest);
				//we should check that the reply matches our initial request
				
				logger.info("request.loginId: "+provisionSender.getLoginId());
				logger.info("reply == ProductType: "+reply.getProductType());
				logger.info("reply == TransactionId"+reply.getTransactionId());
				logger.info("reply == TransactionType"+reply.getTransactionType());
				logger.info("reply == "+reply.getHeader().getSender());
				
				if(reply != null){
					logger.info("reply.getHeader(): "+reply.getHeader());
				}
				if(reply.getHeader() != null){
					logger.info("reply.getHeader().getSender(): "+reply.getHeader().getSender());
				}
				if(reply.getHeader().getSender() != null){
					logger.info("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
				}
				logger.info("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				logger.info("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				//check for errors
								
				if(reply.getHeader().getMajorErrCode()!= null && !(reply.getHeader().getMajorErrCode().equalsIgnoreCase("0"))){
					String majorErrorCode = reply.getHeader().getMajorErrCode();
					ProvisionReplyEntity[] prvReplyEntities = reply.getBody();
					//System.out.println(prvReplyEntities);
					
					System.out.println("===== An error has occured, about to determine severity of error.");
					
					//for(int i=0; i<prvReplyEntities.length;i++){
						
						for (ProvisionReplyEntity entity : prvReplyEntities) {
							
							if(properties.getProperty("majorerror."+majorErrorCode)!= null && properties.getProperty("majorerror."+majorErrorCode).equalsIgnoreCase("warning")){
								logger.info("===== A warning has been raised by RIM, continuing with activation.");
								logger.info("Value of major error "+majorErrorCode);
								
								ProvisionReplyEntity replyEntity[] =  entity.getSubEntities();
								
								for (ProvisionReplyEntity entity2 : replyEntity) {
									if(new Integer(entity2.getResultCode().getErrorCode()).intValue() 
											== 21020 && new Integer(majorErrorCode).intValue() == 21000){
										
										System.out.println("===== Warning is about state, return warning and continue as successful");																			
									}
									else{
										System.out.println("===== A major error has occured, about to exit activate method.");										
										System.out.println("here2");							
									}
									System.out.println("Value of error "+entity2.getResultCode().getErrorCode());
									System.out.println("Value of major error "+majorErrorCode);	
									return new Integer(entity2.getResultCode().getErrorCode()).intValue();
								}
								System.out.println("here3");
							}
							else{
								return new Integer(entity.getResultCode().getErrorCode());
								
							}
						}					
					//}										
					return FAIL;
				}else{					
					return SUCCESS;
				}
				
				
			}catch(Exception e){
				e.printStackTrace();				
				//log exception				
				return FAIL;
			}
	}
	
	/*public int modifySubscriptionIMSI(SubscriberDetail oldSubscriberDetail,String newImsi){
		
		 * List of subscribers to be activated on BlackBerry service on RIM
		 
		
		try{
				logger.debug("modifySubscriptionIMSI() called..."+RIMQueryUtil.class.getName());
				
				String transactionId = new Long(System.currentTimeMillis()).toString();
				String carrierLoginId = properties.getProperty("RIMCarrierLoginId"); 
				String carrierPassword = properties.getProperty("RIMCarrierPwd");
				
				String servicePlan = oldSubscriberDetail.getServiceplan().trim();
								
				
				ProvisionReqEntity[] provisionRequestEntityArr = new ProvisionReqEntity[1];
				
				ProvisionDataItem[] provisionDataItemArr = new ProvisionDataItem[2];
				//Logic to contact RIM asking for suspension of currentSubscriber's IMEI
				ProvisionDataItem provisionDataItemOne = new ProvisionDataItem(oldSubscriberDetail.getImsi(),"OldBillingId",null);
				ProvisionDataItem provisionDataItemTwo = new ProvisionDataItem(newImsi,"IMSI",null);
				provisionDataItemArr[0] = provisionDataItemOne;
				provisionDataItemArr[1] = provisionDataItemTwo;
				
				ProvisionDataItem[] provisionDataItemArr1 = new ProvisionDataItem[1];
				provisionDataItemArr1[0] = new ProvisionDataItem(servicePlan,"ServiceName",null);
				
				ProvisionReqEntity provisionRequestEntityTwo = new ProvisionReqEntity(provisionDataItemArr1,"Prosumer Service",null,null,"service");
				
				ProvisionReqEntity[] provisionServicePH = new ProvisionReqEntity[1];				
				provisionServicePH[0] = provisionRequestEntityTwo;
				
				
				ProvisionReqEntity provisionRequestEntity = 
					new ProvisionReqEntity(provisionDataItemArr,"Prosumer",null,provisionServicePH,"subscriber");
				
				
				provisionRequestEntityArr[0] = provisionRequestEntity;
				
				//Zain's going to provide us with most of this
				ProvisionSender provisionSender = new ProvisionSender("",carrierLoginId,"",null,carrierPassword,"");
				java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
				String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
				ProvisionReqHeader requestHeader = new ProvisionReqHeader(null,provisionSender,"CCYY-MM-DDThh:mm:ssTZD",timeStamp);
				ProvisionRequest provisionRequest = new ProvisionRequest(provisionRequestEntityArr,requestHeader,null,"BlackBerry","",transactionId,"Modify","1.2");
				ProvisioningPortProxy proxy = new ProvisioningPortProxy();
				ProvisionReply reply = proxy.submitSync(provisionRequest);
				//we should check that the reply matches our initial request
				
				logger.info("request.loginId: "+provisionSender.getLoginId());
				logger.info("reply == ProductType: "+reply.getProductType());
				logger.info("reply == TransactionId"+reply.getTransactionId());
				logger.info("reply == TransactionType"+reply.getTransactionType());
				logger.info("reply == "+reply.getHeader().getSender());
				
				if(reply != null){
					System.out.println("reply.getHeader(): "+reply.getHeader());
					logger.info("reply.getHeader(): "+reply.getHeader());
				}
				if(reply.getHeader() != null){
					System.out.println("reply.getHeader().getSender(): "+reply.getHeader().getSender());
					logger.info("reply.getHeader().getSender(): "+reply.getHeader().getSender());
				}
				if(reply.getHeader().getSender() != null){
					System.out.println("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
					logger.info("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
				}
				System.out.println("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				System.out.println("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				logger.info("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				//check for errors
								
				if(reply.getHeader().getMajorErrCode()!= null && !(reply.getHeader().getMajorErrCode().equalsIgnoreCase("0"))){
					String majorErrorCode = reply.getHeader().getMajorErrCode();
					ProvisionReplyEntity[] prvReplyEntities = reply.getBody();
					//System.out.println(prvReplyEntities);
					
					System.out.println("===== An error has occured, about to determine severity of error.");
					
					//for(int i=0; i<prvReplyEntities.length;i++){
						
						for (ProvisionReplyEntity entity : prvReplyEntities) {
							
							if(properties.getProperty("majorerror."+majorErrorCode)!= null && properties.getProperty("majorerror."+majorErrorCode).equalsIgnoreCase("warning")){
								logger.info("===== A warning has been raised by RIM, continuing with activation.");
								logger.info("Value of major error "+majorErrorCode);
								
								ProvisionReplyEntity replyEntity[] =  entity.getSubEntities();
								
								for (ProvisionReplyEntity entity2 : replyEntity) {
									if(new Integer(entity2.getResultCode().getErrorCode()).intValue() 
											== 21020 && new Integer(majorErrorCode).intValue() == 21000){
										
										System.out.println("===== Warning is about state, return warning and continue as successful");																			
									}
									else{
										System.out.println("===== A major error has occured, about to exit activate method.");										
										System.out.println("here2");							
									}
									System.out.println("Value of error "+entity2.getResultCode().getErrorCode());
									System.out.println("Value of major error "+majorErrorCode);	
									return new Integer(entity2.getResultCode().getErrorCode()).intValue();
								}
								System.out.println("here3");
							}
							else{
								ProvisionReplyEntity replyEntity[] =  entity.getSubEntities();
								
								for (ProvisionReplyEntity entity2 : replyEntity) {
									return new Integer(entity2.getResultCode().getErrorCode()).intValue();
								}
								return new Integer(majorErrorCode).intValue();
							}
						}
						
					
					//}										
					return FAIL;
				}else{					
					return SUCCESS;
				}
				
				
			}catch(Exception e){
				e.printStackTrace();				
				//log exception				
				return FAIL;
			}
	}*/
	
	
	
	
	/*public HashMap<String,String> resumeSubscription(ArrayList<SubscriberDetail> subscriberList){
		
		 * List of subscribers to be activated on BlackBerry service on RIM
		 
			try{
				HashMap<String,String> resultsMap = new HashMap<String,String>();
				String transactionId = new Long(System.currentTimeMillis()).toString();
				String carrierLoginId = properties.getProperty("RIMCarrierLoginId"); 
				String carrierPassword = properties.getProperty("RIMCarrierPwd");
				Iterator<SubscriberDetail> it = subscriberList.iterator();
				ProvisionReqEntity[] provisionRequestEntityArr = new ProvisionReqEntity[subscriberList.size()];			
				int count = 0;
				while(it.hasNext()){
					SubscriberDetail subscriberDetail = (SubscriberDetail)it.next();
					ProvisionDataItem[] provisionDataItemArr = new ProvisionDataItem[4];
					//Logic to contact RIM asking for suspension of currentSubscriber's IMEI
					ProvisionDataItem provisionDataItemOne = new ProvisionDataItem(subscriberDetail.getMsisdn(),"MSISDN",null);
					ProvisionDataItem provisionDataItemTwo = new ProvisionDataItem(subscriberDetail.getImei(),"IMEI",null);
					ProvisionDataItem provisionDataItemThree = new ProvisionDataItem(subscriberDetail.getImsi(),"IMSI",null);
					ProvisionDataItem provisionDataItemFour = new ProvisionDataItem(subscriberDetail.getPin(),"PIN",null);
					provisionDataItemArr[0] = provisionDataItemOne;
					provisionDataItemArr[1] = provisionDataItemTwo;
					provisionDataItemArr[2] = provisionDataItemThree;
					provisionDataItemArr[3] = provisionDataItemFour;
					ProvisionReqEntity provisionRequestEntity = new ProvisionReqEntity(provisionDataItemArr,"service",null,null,subscriberDetail.getServicetype());
					provisionRequestEntityArr[count] = provisionRequestEntity;
					count++;
				}
				//Celtel's going to provide us with most of this
				ProvisionSender provisionSender = new ProvisionSender("",carrierLoginId,"",null,carrierPassword,"");
				java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
				String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
				ProvisionReqHeader requestHeader = new ProvisionReqHeader(null,provisionSender,"CCYY-MM-DDThh:mm:ssTZD",timeStamp);
				ProvisionRequest provisionRequest = new ProvisionRequest(provisionRequestEntityArr,requestHeader,null,"BlackBerry","",transactionId,"Resume","1.2");
				ProvisioningPortProxy proxy = new ProvisioningPortProxy();
				ProvisionReply reply = proxy.submitSync(provisionRequest);
				//we should check that the reply matches our initial request
				System.out.println("request.loginId: "+provisionSender.getLoginId());
				System.out.println("reply.loginId: "+reply.getHeader().getSender().getLoginId());
				//check for errors
				String subscriberImsi = "";
					//an error occured, complete or partial
					ProvisionReplyEntity[] prvReplyEntities = reply.getBody();
					for(int i=0; i<prvReplyEntities.length;i++){
						if(prvReplyEntities[i].getResultCode().getErrorCode() != null && !prvReplyEntities[i].getResultCode().getErrorCode().equalsIgnoreCase("0")){
							ProvisionDataItem[] dataItems = prvReplyEntities[i].getItems();
							for(int n = 0; n<dataItems.length; n++){
								System.out.println(dataItems[n].getName()+": "+dataItems[n].getData());
							}
							subscriberImsi = dataItems[2].getData();
							System.out.println(prvReplyEntities[i].getResultCode().getErrorCode());
						}
						resultsMap.put("IMSI: "+subscriberImsi,prvReplyEntities[i].getResultCode().getErrorCode());
					}
				return resultsMap;
			}catch(Exception e){
				e.printStackTrace();				
				//log exception
				
				return new HashMap<String,String>();
			}
	}*/
	
	public int cancelSubscription(SubscriberDetail subscriberDetail){
		
		/*
		 * List of subscribers to be activated on BlackBerry service on RIM
		 */
			try{
				System.out.println("cancelSubscription() called..."+RIMQueryUtil.class.getName());
				logger.info("cancelSubscription() called..."+RIMQueryUtil.class.getName());
				String transactionId = new Long(System.currentTimeMillis()).toString();
				String carrierLoginId = properties.getProperty("RIMCarrierLoginId"); 
				String carrierPassword = properties.getProperty("RIMCarrierPwd");
				
				ProvisionReqEntity[] provisionRequestEntityArr = new ProvisionReqEntity[1];
				
				ProvisionDataItem[] provisionDataItemArr = new ProvisionDataItem[1];
				
				ProvisionDataItem provisionDataItemThree = 
					new ProvisionDataItem(subscriberDetail.getImsi(),"IMSI",null);
				
				provisionDataItemArr[0] = provisionDataItemThree;
				
				
				ProvisionDataItem[] provisionDataItemArr1 = new ProvisionDataItem[1];
				provisionDataItemArr1[0] = new ProvisionDataItem(subscriberDetail.getServiceplan().trim(),
						"ServiceName",null);
				
				
				ProvisionReqEntity provisionRequestEntityTwo = 
					new ProvisionReqEntity(provisionDataItemArr1,"Prosumer Service",null,null,"service");
								
				ProvisionReqEntity[] provisionServicePH = new ProvisionReqEntity[1];				
				provisionServicePH[0] = provisionRequestEntityTwo;
				
				
				ProvisionReqEntity provisionRequestEntity = 
					new ProvisionReqEntity(provisionDataItemArr,"Prosumer",null,provisionServicePH,"subscriber");
				
				
				provisionRequestEntityArr[0] = provisionRequestEntity;
					
				//Zain's going to provide us with most of this
				ProvisionSender provisionSender = new ProvisionSender("",carrierLoginId,"",null,carrierPassword,"");
				java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
				String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
				ProvisionReqHeader requestHeader = new ProvisionReqHeader(null,provisionSender,"CCYY-MM-DDThh:mm:ssTZD",timeStamp);
				ProvisionRequest provisionRequest = new ProvisionRequest(provisionRequestEntityArr,requestHeader,null,"BlackBerry","",transactionId,"Cancel","1.2");
				ProvisioningPortProxy proxy = new ProvisioningPortProxy();
				ProvisionReply reply = proxy.submitSync(provisionRequest);
				//we should check that the reply matches our initial request
				System.out.println("request.loginId: "+provisionSender.getLoginId());
				logger.info("request.loginId: "+provisionSender.getLoginId());
				
				if(reply != null){
					System.out.println("reply.getHeader(): "+reply.getHeader());
					logger.info("reply.getHeader(): "+reply.getHeader());
				}
				if(reply.getHeader() != null){
					System.out.println("reply.getHeader().getSender(): "+reply.getHeader().getSender());
					logger.info("reply.getHeader().getSender(): "+reply.getHeader().getSender());
				}
				if(reply.getHeader().getSender() != null){
					System.out.println("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
					logger.info("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
				}
				System.out.println("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				System.out.println("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				logger.info("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				//logger.info("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getResultCodes().length);
				
				//check for errors
				if(reply.getHeader().getMajorErrCode()!= null && 
						!(reply.getHeader().getMajorErrCode().equalsIgnoreCase("0"))){
					String majorErrorCode = reply.getHeader().getMajorErrCode();
					ProvisionReplyEntity[] prvReplyEntities = reply.getBody();
					
					logger.info("===== An error has occured, about to determine severity of error.");
					
					//for(int i=0; i<prvReplyEntities.length;i++){
					
						for (ProvisionReplyEntity entity : prvReplyEntities) {
							ProvisionReplyEntity replyEntity[] =  entity.getSubEntities();
							
							if(properties.getProperty("majorerror."+majorErrorCode)!= null && 
									properties.getProperty("majorerror."+majorErrorCode).equalsIgnoreCase("warning")){
								logger.info("===== A warning has been raised by RIM, continuing with activation.");
								logger.info("Value of major error "+majorErrorCode);
															
								for (ProvisionReplyEntity entity2 : replyEntity) {
									if(new Integer(entity2.getResultCode().getErrorCode()).intValue() 
											== 21020 && new Integer(majorErrorCode).intValue() == 21000){
										
										logger.info("===== Warning is about state, return warning and continue as successful");																			
									}
									else{
										logger.info("===== A major error has occured, about to exit activate method.");										
																			
									}
									logger.info("Value of error "+entity2.getResultCode().getErrorCode());
									logger.info("Value of major error "+majorErrorCode);	
									return new Integer(entity2.getResultCode().getErrorCode()).intValue();
								}								
							}
							else{
								logger.info("Inside of else");
								
								for (ProvisionReplyEntity entity2 : replyEntity) {
									logger.info(entity2.getResultCode().getErrorCode());
									//return new Integer(entity2.getResultCode().getErrorCode()).intValue();
								}
								return new Integer(majorErrorCode).intValue();
							}
						}									
					return FAIL;
				}else{					
					return SUCCESS;
				}
			}catch(Exception e){
				e.printStackTrace();
				//log exception				
				return FAIL;
			}
		
	}
	
	
	public int cancelSubscription(String imsi, String serviceName){
		
		/*
		 * List of subscribers to be activated on BlackBerry service on RIM
		 */
			try{
				System.out.println("cancelSubscription() called..."+RIMQueryUtil.class.getName());
				logger.info("cancelSubscription() called..."+RIMQueryUtil.class.getName());
				String transactionId = new Long(System.currentTimeMillis()).toString();
				String carrierLoginId = properties.getProperty("RIMCarrierLoginId"); 
				String carrierPassword = properties.getProperty("RIMCarrierPwd");
				
				ProvisionReqEntity[] provisionRequestEntityArr = new ProvisionReqEntity[1];
				
				ProvisionDataItem[] provisionDataItemArr = new ProvisionDataItem[1];
				
				ProvisionDataItem provisionDataItemThree = 
					new ProvisionDataItem(imsi,"IMSI",null);
				
				provisionDataItemArr[0] = provisionDataItemThree;
				
				
				ProvisionDataItem[] provisionDataItemArr1 = new ProvisionDataItem[1];
				provisionDataItemArr1[0] = new ProvisionDataItem(serviceName.trim(),
						"ServiceName",null);
				
				
				ProvisionReqEntity provisionRequestEntityTwo = 
					new ProvisionReqEntity(provisionDataItemArr1,"Prosumer Service",null,null,"service");
				
				
				ProvisionReqEntity[] provisionServicePH = new ProvisionReqEntity[1];				
				provisionServicePH[0] = provisionRequestEntityTwo;
				
				
				ProvisionReqEntity provisionRequestEntity = 
					new ProvisionReqEntity(provisionDataItemArr,"Prosumer",null,provisionServicePH,"subscriber");
				
				
				provisionRequestEntityArr[0] = provisionRequestEntity;
					
				//Zain's going to provide us with most of this
				ProvisionSender provisionSender = new ProvisionSender("",carrierLoginId,"",null,carrierPassword,"");
				java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
				String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
				ProvisionReqHeader requestHeader = new ProvisionReqHeader(null,provisionSender,"CCYY-MM-DDThh:mm:ssTZD",timeStamp);
				ProvisionRequest provisionRequest = new ProvisionRequest(provisionRequestEntityArr,requestHeader,null,"BlackBerry","",transactionId,"Cancel","1.2");
				ProvisioningPortProxy proxy = new ProvisioningPortProxy();
				ProvisionReply reply = proxy.submitSync(provisionRequest);
				//we should check that the reply matches our initial request
				System.out.println("request.loginId: "+provisionSender.getLoginId());
				logger.info("request.loginId: "+provisionSender.getLoginId());
				
				if(reply != null){
					System.out.println("reply.getHeader(): "+reply.getHeader());
					logger.info("reply.getHeader(): "+reply.getHeader());
				}
				if(reply.getHeader() != null){
					System.out.println("reply.getHeader().getSender(): "+reply.getHeader().getSender());
					logger.info("reply.getHeader().getSender(): "+reply.getHeader().getSender());
				}
				if(reply.getHeader().getSender() != null){
					System.out.println("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
					logger.info("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
				}
				System.out.println("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				System.out.println("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				logger.info("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				//logger.info("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getResultCodes().length);
				
				//check for errors
				if(reply.getHeader().getMajorErrCode()!= null && 
						!(reply.getHeader().getMajorErrCode().equalsIgnoreCase("0"))){
					String majorErrorCode = reply.getHeader().getMajorErrCode();
					ProvisionReplyEntity[] prvReplyEntities = reply.getBody();
					
					logger.info("===== An error has occured, about to determine severity of error.");
					
					//for(int i=0; i<prvReplyEntities.length;i++){
					
						for (ProvisionReplyEntity entity : prvReplyEntities) {
							ProvisionReplyEntity replyEntity[] =  entity.getSubEntities();
							
							if(properties.getProperty("majorerror."+majorErrorCode)!= null && 
									properties.getProperty("majorerror."+majorErrorCode).equalsIgnoreCase("warning")){
								logger.info("===== A warning has been raised by RIM, continuing with activation.");
								logger.info("Value of major error "+majorErrorCode);
															
								for (ProvisionReplyEntity entity2 : replyEntity) {
									if(new Integer(entity2.getResultCode().getErrorCode()).intValue() 
											== 21020 && new Integer(majorErrorCode).intValue() == 21000){
										
										logger.info("===== Warning is about state, return warning and continue as successful");																			
									}
									else{
										logger.info("===== A major error has occured, about to exit activate method.");										
																			
									}
									logger.info("Value of error "+entity2.getResultCode().getErrorCode());
									logger.info("Value of major error "+majorErrorCode);	
									return new Integer(entity2.getResultCode().getErrorCode()).intValue();
								}								
							}
							else{
								logger.info("Inside of else");
								
								for (ProvisionReplyEntity entity2 : replyEntity) {
									logger.info(entity2.getResultCode().getErrorCode());
									//return new Integer(entity2.getResultCode().getErrorCode()).intValue();
								}
								return new Integer(majorErrorCode).intValue();
							}
						}									
					return FAIL;
				}else{					
					return SUCCESS;
				}
			}catch(Exception e){
				e.printStackTrace();
				//log exception				
				return FAIL;
			}
		
	}
	
	
	
	/*public HashMap<String,String> cancelSubscription(ArrayList<SubscriberDetail> subscriberList){
		
		 * List of subscribers to be activated on BlackBerry service on RIM
		 
			try{
				HashMap<String,String> resultsMap = new HashMap<String,String>();
				System.out.println("cancelSubscription() called..."+RIMQueryUtil.class.getName());
				logger.info("cancelSubscription() called..."+RIMQueryUtil.class.getName());
				String transactionId = new Long(System.currentTimeMillis()).toString();
				String carrierLoginId = properties.getProperty("RIMCarrierLoginId"); 
				String carrierPassword = properties.getProperty("RIMCarrierPwd");
				Iterator<SubscriberDetail> it = subscriberList.iterator();
				ProvisionReqEntity[] provisionRequestEntityArr = new ProvisionReqEntity[subscriberList.size()];			
				int count = 0;
				while(it.hasNext()){
					SubscriberDetail subscriberDetail = (SubscriberDetail)it.next();
					ProvisionDataItem[] provisionDataItemArr = new ProvisionDataItem[4];
					//Logic to contact RIM asking for suspension of currentSubscriber's IMEI
					ProvisionDataItem provisionDataItemOne = new ProvisionDataItem(subscriberDetail.getMsisdn(),"MSISDN",null);
					ProvisionDataItem provisionDataItemTwo = new ProvisionDataItem(subscriberDetail.getImei(),"IMEI",null);
					ProvisionDataItem provisionDataItemThree = new ProvisionDataItem(subscriberDetail.getImsi(),"IMSI",null);
					ProvisionDataItem provisionDataItemFour = new ProvisionDataItem(subscriberDetail.getPin(),"PIN",null);
					provisionDataItemArr[0] = provisionDataItemOne;
					provisionDataItemArr[1] = provisionDataItemTwo;
					provisionDataItemArr[2] = provisionDataItemThree;
					provisionDataItemArr[3] = provisionDataItemFour;
					ProvisionReqEntity provisionRequestEntity = new ProvisionReqEntity(provisionDataItemArr,"service",null,null,subscriberDetail.getServicetype());
					//ProvisionReqEntity provisionRequestEntity = new ProvisionReqEntity(provisionDataItemArr,"service",null,null,"");
					provisionRequestEntityArr[count] = provisionRequestEntity;
					count++;
				}
				//Zain's going to provide us with most of this
				ProvisionSender provisionSender = new ProvisionSender("",carrierLoginId,"",null,carrierPassword,"");
				java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
				String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
				ProvisionReqHeader requestHeader = new ProvisionReqHeader(null,provisionSender,"CCYY-MM-DDThh:mm:ssTZD",timeStamp);
				ProvisionRequest provisionRequest = new ProvisionRequest(provisionRequestEntityArr,requestHeader,null,"BlackBerry","",transactionId,"Cancel","1.2");
				ProvisioningPortProxy proxy = new ProvisioningPortProxy();
				ProvisionReply reply = proxy.submitSync(provisionRequest);
				//we should check that the reply matches our initial request
				System.out.println("request.loginId: "+provisionSender.getLoginId());
				logger.info("request.loginId: "+provisionSender.getLoginId());
				if(reply != null){
					System.out.println("reply.getHeader(): "+reply.getHeader());
					logger.info("reply.getHeader(): "+reply.getHeader());
				}
				if(reply.getHeader() != null){
					System.out.println("reply.getHeader().getSender(): "+reply.getHeader().getSender());
					logger.info("reply.getHeader().getSender(): "+reply.getHeader().getSender());
				}
				if(reply.getHeader().getSender() != null){
					System.out.println("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
					logger.info("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
				}
				System.out.println("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				System.out.println("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				logger.info("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				
				//check for errors
				String subscriberImsi = "";
					//an error occured, complete or partial
					ProvisionReplyEntity[] prvReplyEntities = reply.getBody();
					for(int i=0; i<prvReplyEntities.length;i++){
						if(prvReplyEntities[i].getResultCode().getErrorCode() != null && !prvReplyEntities[i].getResultCode().getErrorCode().equalsIgnoreCase("0")){
							ProvisionDataItem[] dataItems = prvReplyEntities[i].getItems();
							for(int n = 0; n<dataItems.length; n++){
								System.out.println(dataItems[n].getName()+": "+dataItems[n].getData());
								logger.info(dataItems[n].getName()+": "+dataItems[n].getData());
							}
							subscriberImsi = dataItems[2].getData();
							System.out.println(prvReplyEntities[i].getResultCode().getErrorCode());
							logger.info(prvReplyEntities[i].getResultCode().getErrorCode());
						}
						resultsMap.put("IMSI: "+subscriberImsi,prvReplyEntities[i].getResultCode().getErrorCode());
					}
				return resultsMap;
			}catch(Exception e){
				e.printStackTrace();				
				//log exception
				
				return new HashMap<String,String>();
			}
		
	}*/

	public int updateSubscriberInfo(SubscriberDetail oldSubscriberDetail, SubscriberDetail newSubscriberDetail){
		int status = 0;
		try{			
			status = cancelSubscription(oldSubscriberDetail);
			status = activateSubscription(newSubscriberDetail);
			logger.info("Updated subscriber information for "+oldSubscriberDetail.getLastname()+", "+
					oldSubscriberDetail.getFirstname()+" "+oldSubscriberDetail.getMiddlename()+", with MSISDN:"+oldSubscriberDetail.getMsisdn()+", IMSI:"+oldSubscriberDetail.getImsi()+", IMEI:"+oldSubscriberDetail.getImei()+
					", PIN: "+oldSubscriberDetail.getPin());
			logger.info("New subscriber information for "+oldSubscriberDetail.getLastname()+", "+
					oldSubscriberDetail.getFirstname()+" "+oldSubscriberDetail.getMiddlename()+", with MSISDN:"+newSubscriberDetail.getMsisdn()+", IMSI:"+newSubscriberDetail.getImsi()+", IMEI:"+newSubscriberDetail.getImei()+
					", PIN: "+newSubscriberDetail.getPin());
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return status;
	}
	
	public int modifySubscription(SubscriberDetail subscriberDetail){
		/*
		 * List of subscribers to be activated on BlackBerry service on RIM
		 */
		
		try{
				logger.info("modifySubscriptionIMSI() called..."+RIMQueryUtil.class.getName());
				
				String transactionId = new Long(System.currentTimeMillis()).toString();
				String carrierLoginId = properties.getProperty("RIMCarrierLoginId"); 
				String carrierPassword = properties.getProperty("RIMCarrierPwd");
				
				
				ProvisionReqEntity[] provisionRequestEntityArr = new ProvisionReqEntity[1];
				
				ProvisionDataItem[] provisionDataItemArr = new ProvisionDataItem[4];
				
				ProvisionDataItem provisionDataItemOne = new ProvisionDataItem(subscriberDetail.getImsi(),"IMSI",null);
				ProvisionDataItem provisionDataItemTwo = new ProvisionDataItem(subscriberDetail.getMsisdn(),"MSISDN",null);
				ProvisionDataItem provisionDataItemThree = new ProvisionDataItem(subscriberDetail.getPin(),"PIN",null);
				ProvisionDataItem provisionDataItemFour = new ProvisionDataItem(subscriberDetail.getImei(),"IMEI",null);
				provisionDataItemArr[0] = provisionDataItemOne;
				provisionDataItemArr[1] = provisionDataItemTwo;
				provisionDataItemArr[2] = provisionDataItemThree;
				provisionDataItemArr[3] = provisionDataItemFour;
							
				
				ProvisionReqEntity provisionRequestEntity = 
					new ProvisionReqEntity(provisionDataItemArr,"Prosumer",null,null,"subscriber");
				
				
				provisionRequestEntityArr[0] = provisionRequestEntity;
				
				//Zain's going to provide us with most of this
				ProvisionSender provisionSender = new ProvisionSender("",carrierLoginId,"",null,carrierPassword,"");
				java.sql.Timestamp now = new java.sql.Timestamp(new Date().getTime());
				String timeStamp = (now.toString().substring(0, 10).concat("T")).concat((now.toString().substring(11,19)).concat("Z"));
				ProvisionReqHeader requestHeader = new ProvisionReqHeader(null,provisionSender,"CCYY-MM-DDThh:mm:ssTZD",timeStamp);
				ProvisionRequest provisionRequest = new ProvisionRequest(provisionRequestEntityArr,requestHeader,null,"BlackBerry","",transactionId,"Modify","1.2");
				ProvisioningPortProxy proxy = new ProvisioningPortProxy();
				ProvisionReply reply = proxy.submitSync(provisionRequest);
				//we should check that the reply matches our initial request
				
				logger.info("request.loginId: "+provisionSender.getLoginId());
				logger.info("reply == ProductType: "+reply.getProductType());
				logger.info("reply == TransactionId"+reply.getTransactionId());
				logger.info("reply == TransactionType"+reply.getTransactionType());
				logger.info("reply == "+reply.getHeader().getSender());
				
				if(reply != null){
					logger.info("reply.getHeader(): "+reply.getHeader());
				}
				if(reply.getHeader() != null){
					logger.info("reply.getHeader().getSender(): "+reply.getHeader().getSender());
				}
				if(reply.getHeader().getSender() != null){
					logger.info("reply.getHeader().getSender().getLoginId(): "+reply.getHeader().getSender().getLoginId());
				}
				logger.info("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				logger.info("reply.getHeader().getTimeStamp(): "+reply.getHeader().getTimeStamp());
				logger.info("reply.getHeader().getMajorErrCode(): "+reply.getHeader().getMajorErrCode());
				//check for errors
								
				if(reply.getHeader().getMajorErrCode()!= null && !(reply.getHeader().getMajorErrCode().equalsIgnoreCase("0"))){
					String majorErrorCode = reply.getHeader().getMajorErrCode();
					ProvisionReplyEntity[] prvReplyEntities = reply.getBody();
					//System.out.println(prvReplyEntities);
					
					System.out.println("===== An error has occured, about to determine severity of error.");
					
					//for(int i=0; i<prvReplyEntities.length;i++){
						
						for (ProvisionReplyEntity entity : prvReplyEntities) {
							
							if(properties.getProperty("majorerror."+majorErrorCode)!= null && properties.getProperty("majorerror."+majorErrorCode).equalsIgnoreCase("warning")){
								logger.info("===== A warning has been raised by RIM, continuing with activation.");
								logger.info("Value of major error "+majorErrorCode);
								
								ProvisionReplyEntity replyEntity[] =  entity.getSubEntities();
								
								for (ProvisionReplyEntity entity2 : replyEntity) {
									if(new Integer(entity2.getResultCode().getErrorCode()).intValue() 
											== 21020 && new Integer(majorErrorCode).intValue() == 21000){
										
										System.out.println("===== Warning is about state, return warning and continue as successful");																			
									}
									else{
										System.out.println("===== A major error has occured, about to exit activate method.");										
										System.out.println("here2");							
									}
									System.out.println("Value of error "+entity2.getResultCode().getErrorCode());
									System.out.println("Value of major error "+majorErrorCode);	
									return new Integer(entity2.getResultCode().getErrorCode()).intValue();
								}
								System.out.println("here3");
							}
							else{
								return new Integer(entity.getResultCode().getErrorCode());
								
							}
						}
						
					
					//}										
					return FAIL;
				}else{					
					return SUCCESS;
				}
				
				
			}catch(Exception e){
				e.printStackTrace();				
				//log exception				
				return FAIL;
			}
	}

	
}
