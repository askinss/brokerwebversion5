package com.vasconsulting.www.utility;
import java.rmi.RemoteException;
import java.util.Date;

import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKGProxy;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPreOrPostPaidElement;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPreOrPostPaidResponseElement;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscribersServicesElement;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscribersServicesResponseElement;
import ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.OrderManagementProxy;
import ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeserviceElement;
import ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeserviceResponseElement;


public class Tabs {
	
	public String deleteService(String msisdn, String servicename){
		OrderManagementProxy orderProxy = new OrderManagementProxy();
		ChangeserviceElement params = new ChangeserviceElement();
		ChangeserviceResponseElement responseElement;
		params.setPSubno(msisdn);
		params.setPUsername("BBROKER2");//dynamic from props file
		params.setPSubscrtype("G");
		params.setPArea("0");
		params.setPSubsidyFlag("N");
		params.setPSndcmd("Y");
		
		String serviceName = "<Service name=\"OrderManagement\" msgType=\"Input\"><Operation name=\"op1\" paramName=\"service\">"+
			"<InputParams><SServiceList><SService><Action>D</Action><EquipID>"+ servicename + "</EquipID><LoginId/><SerialNo/><SParamList><SParam><ParamName>"+
			"</ParamName><ParamValue></ParamValue></SParam></SParamList></SService></SServiceList></InputParams></Operation></Service>";	
		
		params.setPServices(serviceName);
		params.setPAdditonalparams("<?xml version=\"1.0\" encoding=\"UTF-8\"?><SParamList></SParamList>");
		try {			
			responseElement = orderProxy.changeservice(params);
			return responseElement.getResult();
		}catch(Exception e){
			e.printStackTrace();
			return "";
		}
		
	}
	
	public String changeService(String msisdn, String equipID){
		OrderManagementProxy orderProxy = new OrderManagementProxy();
		ChangeserviceElement params = new ChangeserviceElement();
		ChangeserviceResponseElement responseElement;
		params.setPSubno("689103361");
		params.setPUsername("BBROKER2");//dynamic from props file
		params.setPSubscrtype("G");
		params.setPArea("0");
		params.setPSubsidyFlag("N");
		params.setPSndcmd("Y");
		
		String serviceName = "<Service name=\"OrderManagement\" msgType=\"Input\"><Operation name=\"op1\" paramName=\"service\">"+
			"<InputParams><SServiceList><SService><Action>I</Action><EquipID>BLKBERY</EquipID><LoginId/><SerialNo/><SParamList><SParam><ParamName>"+
			"</ParamName><ParamValue></ParamValue></SParam></SParamList></SService></SServiceList></InputParams></Operation></Service>";	
		
		params.setPServices(serviceName);
		params.setPAdditonalparams("<?xml version=\"1.0\" encoding=\"UTF-8\"?><SParamList></SParamList>");
		try {			
			responseElement = orderProxy.changeservice(params);
			return responseElement.getResult();
		}catch(Exception e){
			e.printStackTrace();
			return "";
		}
		
	}
	
	public String getPreOrPost(){
		try {
			TBI_KPI_PKGProxy tbiProxy = new TBI_KPI_PKGProxy();
			//GetSubscribersServicesElement subParams = new GetSubscribersServicesElement();
			GetPreOrPostPaidElement subParams = new GetPreOrPostPaidElement();
			subParams.setSubno("689103361");
			subParams.setArea("0");
			subParams.setSubscrType("G");
			GetPreOrPostPaidResponseElement responseElement;
			//subParams.setMaxrowcount("10");
			//GetSubscribersServicesResponseElement responseElement;
			responseElement = tbiProxy.getPreOrPostPaid(subParams);
			//responseElement = tbiProxy.getSubscribersServices(subParams);
			return responseElement.getResult();
			 //responseElement;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}
	
	public String getService(String msisdn){
		try {
			TBI_KPI_PKGProxy tbiProxy = new TBI_KPI_PKGProxy();
			GetSubscribersServicesElement subParams = new GetSubscribersServicesElement();
			subParams.setSubno(msisdn);
			subParams.setArea("0");
			subParams.setSubscrType("G");
			subParams.setMaxrowcount("10");
			GetSubscribersServicesResponseElement responseElement;
			responseElement = tbiProxy.getSubscribersServices(subParams);
			return responseElement.getResult();
			 //responseElement;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

}