/**
getUser * This is the data access service layer of the application. This class is responsible for all database access functions.
 * It currently connects to the local database system and also connects to the TABS database that is used to load subscriber credit limit.
 * @author nnamdi Jibunoh
 * @date 8-8-2011
 */
package com.vasconsulting.www.utility;

import java.math.BigDecimal;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
//import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;
import com.vasconsulting.www.domain.ActivityLogger;
import com.vasconsulting.www.domain.BbTestPromoFirstYes;
import com.vasconsulting.www.domain.BbTestPromoInitial;
import com.vasconsulting.www.domain.BillingPlan;
import com.vasconsulting.www.domain.CDRSubscriptionDetail;
import com.vasconsulting.www.domain.DeductionLog;
import com.vasconsulting.www.domain.ErrorDetail;
import com.vasconsulting.www.domain.FutureRenewal;
import com.vasconsulting.www.domain.IMEIWhitelist;
import com.vasconsulting.www.domain.MSISDNWhitelist;
import com.vasconsulting.www.domain.MenuSession;
import com.vasconsulting.www.domain.MenuTree;
import com.vasconsulting.www.domain.ReconStatus;
import com.vasconsulting.www.domain.ReconcilDetail;
import com.vasconsulting.www.domain.RequestTracker;
import com.vasconsulting.www.domain.RolesDetail;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.SubscriberMenuStatus;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.domain.Ttspromo;
import com.vasconsulting.www.domain.Users;
import com.vasconsulting.www.interfaces.HibernateUtility;

@Repository
@Transactional

public class HibernateUtilityImpl implements HibernateUtility
{
	private SessionFactory sessionFactory;
	private SessionFactory sessionFactoryTabs;


	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}

	
	public void setSessionFactoryTabs(SessionFactory sessionFactoryTabs)
	{
		this.sessionFactoryTabs = sessionFactoryTabs;
	}

	public int saveSubscriber(SubscriberDetail subscriberDetail)
	{
		sessionFactory.getCurrentSession().save(subscriberDetail);		
		return 0;
	}

	public int saveTransactionlog(TransactionLog transactionLog)
	{
		this.sessionFactory.getCurrentSession().save(transactionLog);	
		
		return 0;
	}
	
	public int saveReconStatus(ReconStatus reconStatusDb)
	{
		this.sessionFactory.getCurrentSession().save(reconStatusDb);	
		
		return 0;
	}
	
	

	@SuppressWarnings("unchecked")
	public Collection<TransactionLog> getSubscriberTransactionLog(String msisdn)
	{
		return this.sessionFactory.getCurrentSession().createQuery("from TransactionLog where msisdn = :msisdn order by date_created desc")
				.setString("msisdn", msisdn)
				.list();
	}

	@SuppressWarnings("unchecked")
	public Collection<SubscriberDetail> getSubscriberInformation(
			SubscriberDetail subscriberDetail)
			{
		return this.sessionFactory.getCurrentSession().createQuery("from SubscriberDetail where msisdn = :msisdn")
				.setString("msisdn", subscriberDetail.getMsisdn())
				.list();
			}

	
	
	public String getSubscriberTABSCreditLimit(String msisdn)
	{
		@SuppressWarnings("unchecked")
		Collection<String> response = this.sessionFactoryTabs.getCurrentSession().
		createSQLQuery("select credit_limit from POST_SUBS_PROFILE where subno = :msisdn")
		.setString("msisdn", stripLeadingMsisdnPrefix(msisdn)).list();

		if (response.iterator().hasNext())
			return response.iterator().next();
		else return null;
	}

	private String stripLeadingMsisdnPrefix(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("0")){
			return Msisdn.substring(1, Msisdn.length());
		}
		else if (Msisdn.startsWith("234")){
			return Msisdn.substring(3, Msisdn.length());
		}
		else if(Msisdn.startsWith("+234")){
			return Msisdn.substring(4, Msisdn.length());
		}
		else return Msisdn;
	}

	public int updateSubscriberDetail(SubscriberDetail subscriebrDetail)
	{
		this.sessionFactory.getCurrentSession().update(subscriebrDetail);
		return 0;
	}

	@SuppressWarnings("unchecked")
	public Collection<SubscriberDetail> loadAllSubscriberDetails()
	{
		return this.sessionFactory.getCurrentSession().createQuery("from SubscriberDetail where postpaidSubscriber = 1").list();
	}

	@SuppressWarnings("unchecked")
	public Collection<SubscriberDetail> loadAllPostpaidSubscriberDetailsDueForRenewal()
	{
		return this.sessionFactory.getCurrentSession().createQuery("from SubscriberDetail where postpaidSubscriber = 1 and " +
				"statusid = 'Active' and next_subscription_date <= sysdate").list();
	}

	@SuppressWarnings("unchecked")
	public Collection<SubscriberDetail> loadAllSubscriberDetailsDueForActivation()
	{
		return this.sessionFactory.getCurrentSession().createQuery("from SubscriberDetail where postpaidSubscriber = 1 and " +
				"statusid != 'Active'").list();
	}

	@SuppressWarnings("unchecked")
	public SubscriberDetail getSubscriberInformation(String param,	String paramValue)
	{
		System.out.println("from SubscriberDetail where "+param+" = :paramValue, "+paramValue);
		Collection<SubscriberDetail> subs = this.sessionFactory.getCurrentSession().
				createQuery("from SubscriberDetail where "+param+" = :paramValue").
				setString("paramValue", paramValue).
				list();

		for (SubscriberDetail subss : subs)
		{
			return subss;
		}

		return null;
	}
	
	public ReconcilDetail getReconSubInformation(String param, String paramValue)
	  {
	    System.out.println("from ReconcilDetail where " + param + " = :paramValue, " + paramValue);
	    Collection subs = this.sessionFactory.getCurrentSession()
	      .createQuery("from ReconcilDetail where " + param + " = :paramValue")
	      .setString("paramValue", paramValue)
	      .list();

	    Iterator localIterator = subs.iterator(); if (localIterator.hasNext()) { ReconcilDetail subss = (ReconcilDetail)localIterator.next();

	      return subss;
	    }

	    return null;
	  }
	
	
	
	  public ReconcilDetail getIMSIFromDBByMsisdn(String msisdn)
	  {
	    ArrayList response = new ArrayList();
	    try
	    {
	      response = (ArrayList)this.sessionFactory.getCurrentSession().createQuery("from ReconcilDetail where msisdn = :msisdn").setString("msisdn", msisdn).list();
	    }
	    catch (HibernateException e)
	    {
	      e.printStackTrace();
	    }
	    if (response.isEmpty()) return null;

	    return (ReconcilDetail)response.get(0);
	  }
	  
	  //Added for reconstatus table for reconciliation reporting
	  public ReconStatus getReconStatusInformation(String msisdn)
	  {
	    ArrayList response = new ArrayList();
	    try
	    {
	      response = (ArrayList)this.sessionFactory.getCurrentSession().createQuery("from ReconStatus where msisdn = :msisdn").setString("msisdn", msisdn).list();
	    }
	    catch (HibernateException e)
	    {
	      e.printStackTrace();
	    }
	    if (response.isEmpty()) return null;

	    return (ReconStatus)response.get(0);
	  }
	  
	
	  
	  

	@SuppressWarnings("unchecked")
	public ArrayList<SubscriberDetail> getSubscribersExpiringSoon(int noOfDays)
	{
		System.out.println("Notification day is = "+noOfDays);

		/*ORACLE*/
		String requestQuery = "from SubscriberDetail where statusid = 'Active' and trunc(current_date+ :noOfDays)  = " +
				"trunc(next_subscription_date) and servicetype != 1 and prepaidsubscriber = 1";

		/* MYSQL*/
		/*String requestQuery = " from SubscriberDetail where statusid = 'Active' and " +
		"datediff(DATE_FORMAT(next_subscription_date, '%Y-%m-%d'),DATE_FORMAT(current_date, '%Y-%m-%d')) = :noOfDays";*/

		System.out.println("Query is = "+requestQuery);

		ArrayList<SubscriberDetail> subs = (ArrayList<SubscriberDetail>)this.sessionFactory.getCurrentSession().
				createQuery(requestQuery).setInteger("noOfDays", noOfDays).list();


		if (subs.isEmpty())
		{
			return new ArrayList<SubscriberDetail>();
		}
		else return subs;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<SubscriberDetail> getDaySubscribersExpiringSoon()
	{

		/*ORACLE*/
		String requestQuery = "from SubscriberDetail where statusid = 'Active' and trunc(current_date)  >= " +
				"trunc(next_subscription_date) and servicetype = 1";

		/* MYSQL*/
		/*String requestQuery = " from SubscriberDetail where statusid = 'Active' and " +
		"datediff(DATE_FORMAT(next_subscription_date, '%Y-%m-%d'),DATE_FORMAT(current_date, '%Y-%m-%d')) = :noOfDays";*/

		System.out.println("Query is = "+requestQuery);

		ArrayList<SubscriberDetail> subs = (ArrayList<SubscriberDetail>)this.sessionFactory.getCurrentSession().
				createQuery(requestQuery).list();


		if (subs.isEmpty())
		{
			return new ArrayList<SubscriberDetail>();
		}
		else return subs;
	}

	public int logService(ActivityLogger logger)
	{
		this.sessionFactory.getCurrentSession().save(logger);
		return 0;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<SubscriberDetail> getActivationReportTabular(int daysToRetrieve)
	{
		ArrayList<SubscriberDetail> queryResult = new ArrayList<SubscriberDetail>();

		if (daysToRetrieve == 1)
			queryResult = (ArrayList<SubscriberDetail>)this.sessionFactory.getCurrentSession().
			getNamedQuery("com.vasconsulting.www.domain.SubscriberDetail.GetDailyFreshActivationQuery").
			list();
		else
			queryResult = (ArrayList<SubscriberDetail>)this.sessionFactory.getCurrentSession().
			getNamedQuery("com.vasconsulting.www.domain.SubscriberDetail.GetAllFreshActivationQuery").
			setInteger("noOfDaysBack", daysToRetrieve).
			list();				

		return queryResult;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<SubscriberDetail> getRenewalReportTabular()
	{
		ArrayList<SubscriberDetail> queryResult = new ArrayList<SubscriberDetail>();

		queryResult = (ArrayList<SubscriberDetail>)this.sessionFactory.getCurrentSession().
				getNamedQuery("com.vasconsulting.www.domain.SubscriberDetail.GetDailyRenewalsQuery").
				list();

		return queryResult;
	}

	@SuppressWarnings("unchecked")
	public int getReportSumation(String columnName, int daysToRetrieve)
	{
		ArrayList<Integer> queryResult = new ArrayList<Integer>();

		if (columnName.equalsIgnoreCase("date_created"))
		{
			if (daysToRetrieve == 1)
			{
				queryResult = (ArrayList<Integer>)this.sessionFactory.getCurrentSession().
						getNamedQuery("com.vasconsulting.www.domain.SubscriberDetail.GetDailyActivationsSumationQuery").
						list();
			}
			else
			{
				queryResult = (ArrayList<Integer>)this.sessionFactory.getCurrentSession().
						getNamedQuery("com.vasconsulting.www.domain.SubscriberDetail.GetAllActivationsSumationQuery").
						setInteger("noOfDaysBack", daysToRetrieve).				
						list();
			}
		}
		else
		{
			queryResult = (ArrayList<Integer>)this.sessionFactory.getCurrentSession().
					getNamedQuery("com.vasconsulting.www.domain.SubscriberDetail.GetDailyRenewalsQuery").
					list();
		}	

		if (queryResult.isEmpty()) return 0;
		else return queryResult.size();
	}

	@SuppressWarnings("unchecked")
	public SubscriberMenuStatus getSubscriberMenuByMsisdn(String msisdn)
	{
		Collection<SubscriberMenuStatus> collection = 
				this.sessionFactory.getCurrentSession().createQuery("from SubscriberMenuStatus where msisdn = :msisdn").
				setString("msisdn", msisdn).list();

		if (collection != null && !collection.isEmpty())
		{
			System.out.println("The value returned is "+collection.iterator().next());
			return collection.iterator().next();
		}
		else 
			return new SubscriberMenuStatus();
	}

	public int saveSubscriberMenuByMsisdn(SubscriberMenuStatus subMenu)
	{
		this.sessionFactory.getCurrentSession().saveOrUpdate(subMenu);
		return 0;
	}

	public int removeSubscriberMenuByMsisdn(SubscriberMenuStatus subMenu)
	{
		this.sessionFactory.getCurrentSession().delete(subMenu);
		return 0;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<BillingPlan> getAllBillingPlans()
	{
		String requestQuery = "from BillingPlan where status = 1";

		System.out.println("Query is = "+requestQuery);

		ArrayList<BillingPlan> subs = (ArrayList<BillingPlan>)this.sessionFactory.getCurrentSession().
				createQuery(requestQuery).list();				

		if (subs.isEmpty())
		{
			return new ArrayList<BillingPlan>();
		}
		else return subs;
	}

	public MSISDNWhitelist getWhiteListedMSISDN(String msisdn){
		String requestQuery = "from MSISDNWhitelist where msisdn = :msisdn and status = 'available'";

		System.out.println("Query is = "+requestQuery);

		@SuppressWarnings("unchecked")
		ArrayList<MSISDNWhitelist> subs = (ArrayList<MSISDNWhitelist>)this.sessionFactory.getCurrentSession().
		createQuery(requestQuery).setString("msisdn", msisdn).list();
		if(subs.isEmpty()){
			return null;
		}else{
			MSISDNWhitelist msisdnWhiteList = subs.get(0);
			return msisdnWhiteList;
		}
	}

	public boolean isMSISDNWhitelisted(String msisdn)
	{
		MSISDNWhitelist msisdnWhiteList = getWhiteListedMSISDN(msisdn);

		if (msisdnWhiteList == null)
		{
			return false;
		}
		else return true;
	}

	public IMEIWhitelist getWhiteListedIMEI(String imei){
		String requestQuery = "from IMEIWhitelist imeiList where imeiList.imei LIKE :imei and imeiList.status = 'available'";
		System.out.println("Query is = "+requestQuery);
		String first14digitsfromIMEI = imei.substring(0, 13);
		System.out.println("First 14 digits from IMEI is: "+ first14digitsfromIMEI);
		@SuppressWarnings("unchecked")
		ArrayList<IMEIWhitelist> subs = (ArrayList<IMEIWhitelist>)this.sessionFactory.getCurrentSession().
		createQuery(requestQuery).setString("imei", first14digitsfromIMEI + "%").list();
		if(subs.isEmpty()){
			return null;
		}else{
			IMEIWhitelist imeiWhiteList = subs.get(0);
			return imeiWhiteList;
		}

	}

	public boolean isIMEIWhitelisted(String imei)
	{
		IMEIWhitelist imeiWhiteList = getWhiteListedIMEI(imei);

		if (imeiWhiteList == null)
		{
			return false;
		}
		else return true;
	}

	public BbTestPromoInitial getPreloadedSubscriber(String msisdn){
		String requestQuery = "from BbTestPromoInitial where msisdn = :msisdn and status = 'available'";
		System.out.println("Query is = "+requestQuery);
		@SuppressWarnings("unchecked")
		ArrayList<BbTestPromoInitial> subs = (ArrayList<BbTestPromoInitial>)this.sessionFactory.getCurrentSession().
		createQuery(requestQuery).setString("msisdn", msisdn).list();
		if (subs.isEmpty()){
			return null;
		}else{
			BbTestPromoInitial bbTestPromoInitialMsisdn = subs.get(0);
			return bbTestPromoInitialMsisdn;
		}
	}

	public BbTestPromoFirstYes getSubscriberFirstYes(String msisdn){
		String requestQuery = "from BbTestPromoInitial where msisdn = :msisdn";
		System.out.println("Query is = "+requestQuery);
		@SuppressWarnings("unchecked")
		ArrayList<BbTestPromoFirstYes> subs = (ArrayList<BbTestPromoFirstYes>)this.sessionFactory.getCurrentSession().
		createQuery(requestQuery).setString("msisdn", msisdn).list();
		if (subs.isEmpty()){
			return null;
		}else{
			BbTestPromoFirstYes bbTestPromoFirstYes = subs.get(0);
			return bbTestPromoFirstYes;
		}
	}

	public ArrayList<BbTestPromoFirstYes> getFirstYesExpiringSoon(int noOfDays){
		String requestQuery = "from BbTestPromoFirstYes where trunc(sysdate + :noOfDays) = trunc(due_date) and rimstatus = 'Active'";
		System.out.println("Query is = "+requestQuery);
		@SuppressWarnings("unchecked")
		ArrayList<BbTestPromoFirstYes> subs = (ArrayList<BbTestPromoFirstYes>)this.sessionFactory.getCurrentSession().
		createQuery(requestQuery).setInteger("noOfDays", noOfDays).list();
		return subs;
	}

	public ArrayList<BbTestPromoFirstYes> getFirstYesExpiringToday(){
		String requestQuery = "from BbTestPromoFirstYes where trunc(sysdate) = trunc(due_date) and rimstatus = 'Active'";
		System.out.println("Query is = "+requestQuery);
		@SuppressWarnings("unchecked")
		ArrayList<BbTestPromoFirstYes> subs = (ArrayList<BbTestPromoFirstYes>)this.sessionFactory.getCurrentSession().
		createQuery(requestQuery).list();
		return subs;
	}



	@SuppressWarnings("rawtypes")
	public ArrayList<SubscriberDetail> getSubscribersExpiringToday(){
		System.out.println("Inside of the getSubscribersExpiringToday method");
		String requestQuery = "select id,firstname,middlename,lastname,email,pin,imei,msisdn," +
				"imsi,date_created," +
				"next_subscription_date,last_subscription_date," +
				"statusid,serviceplanid,servicetype," +
				"prepaidsubscriber,postpaidsubscriber,shortcode,autorenew" +
				" from subscriber where next_subscription_date " +
				"<= current_date and statusid = 'Active' and prepaidsubscriber = 1";
		try {
			System.out.println("Query is = "+requestQuery);
			ArrayList container =  (ArrayList)this.sessionFactory.getCurrentSession().
					createSQLQuery(requestQuery).list();
			ArrayList<SubscriberDetail> arrayResultFiltered = new ArrayList<SubscriberDetail>();
			Calendar myCal = new GregorianCalendar();
			Calendar myCal1 = new GregorianCalendar();
			Calendar myCal2 = new GregorianCalendar();
			System.out.println(container.size());
			for (int count=0; count < container.size(); count++){
				SubscriberDetail subDetail = new SubscriberDetail();
				Object[] obj = (Object[])container.get(count);
				System.out.println("ID is: "+obj[0]);
				subDetail.setId((String)(obj[0]));
				System.out.println("Firstname is: "+obj[1]);
				subDetail.setFirstname((String)(obj[1]));
				System.out.println("Middlename is: "+obj[2]);
				subDetail.setMiddlename((String)(obj[2]));
				System.out.println("Lastname is: "+obj[3]);
				subDetail.setLastname((String)(obj[3]));
				System.out.println("email is: "+obj[4]);
				subDetail.setEmail((String)(obj[4]));
				System.out.println("Pin is: "+obj[5]);
				subDetail.setPin((String)(obj[5]));
				System.out.println("IMEI is: "+obj[6]);
				subDetail.setImei((String)(obj[6]));
				System.out.println("MSISDN is: "+obj[7]);
				subDetail.setMsisdn((String)obj[7]);
				System.out.println("IMSI is: "+obj[8]);
				subDetail.setImsi((String)obj[8]);
				myCal.setTime((java.util.Date)obj[9]);
				System.out.println("Date_Created is: "+obj[9]);
				System.out.println("New Date_Created is: "+myCal);
				subDetail.setDate_created(myCal);
				myCal1.setTime((java.util.Date)obj[10]);
				subDetail.setNext_subscription_date(myCal1);
				myCal2.setTime((java.util.Date)obj[11]);
				System.out.println("New Next_subscription_date is: "+myCal1);
				subDetail.setLast_subscription_date(myCal2);
				subDetail.setStatus((String)obj[12]);
				subDetail.setServiceplan((String)obj[13]);
				System.out.println("Servicetype is: "+obj[14]);
				subDetail.setServicetype(((BigDecimal)obj[14]).intValue());
				System.out.println("Prepaidsubscriber is: "+obj[15]);
				subDetail.setPrepaidSubscriber(((BigDecimal)obj[15]).intValue());
				System.out.println("Postpaidsubscriber is: "+obj[16]);
				subDetail.setPostpaidSubscriber(((BigDecimal)obj[16]).intValue());
				System.out.println("Shortcode is: "+obj[17]);
				subDetail.setShortCode((String)(obj[17]));
				System.out.println("Autorenew is: "+obj[18]);
				subDetail.setAutoRenew(((BigDecimal)obj[18]).intValue());
				arrayResultFiltered.add(subDetail);
				System.out.println("Subscriber Details are: "+subDetail);
			}
			return arrayResultFiltered;
		}
		catch (Exception e) {
			e.printStackTrace();
		};
		return null;
	}
	
	
	
	@SuppressWarnings({ "unchecked" })
	public ArrayList<SubscriberDetail> getSubscribersExpiringTodayWithCriterias(){
		System.out.println("Inside of the getSubscribersExpiringTodayWithHibernate method");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SubscriberDetail.class);

		criteria.add(Restrictions.eq("prepaidSubscriber", 1)).add(Restrictions.eq("status", "Active")).
		add(Restrictions.le("next_subscription_date", new GregorianCalendar()));

		return (ArrayList<SubscriberDetail>)criteria.list();
	}
	
	@SuppressWarnings({ "unchecked" })
	public ArrayList<SubscriberDetail> getSubscribersActiveCriterias(){
		System.out.println("Inside of the getSubscribersExpiringTodayWithHibernate method");
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SubscriberDetail.class);

		criteria.add(Restrictions.eq("status", "Active"));
		

		return (ArrayList<SubscriberDetail>)criteria.list();
	}
	
	
	
public int createSwap(ReconcilDetail reconcildetail,SubscriberDetail subscriberDetail){
		
	try{
		System.out.println("Imsi is " +reconcildetail.getImsi()+ " msisdn" +subscriberDetail.getMsisdn());
		SQLQuery sqlQuery = this.sessionFactory.getCurrentSession().createSQLQuery("update subscriber set imsi = :imsi" + " where msisdn = :msisdn");
		sqlQuery.setParameter("imsi", reconcildetail.getImsi());
		sqlQuery.setParameter("msisdn",subscriberDetail.getMsisdn());
		int result = sqlQuery.executeUpdate();
		return result;
		
		
		
	  } catch(Exception e) 
	   {
		
		e.printStackTrace();
		return -1;
		
	 }
	
}

 public int updateReconStatus(ReconcilDetail reconcildetail,SubscriberDetail subscriberDetail){
	
	try{
		
		//String hql = "INSERT INTO Employee(firstName, lastName, salary)"  +  "SELECT firstName, lastName, salary FROM old_employee";
		System.out.println("New Imsi is " +reconcildetail.getImsi()+ " msisdn" +subscriberDetail.getMsisdn() + "Old imsi : " +subscriberDetail.getImsi() );
		SQLQuery sqlQuery = this.sessionFactory.getCurrentSession().createSQLQuery("insert into table reconstatus ( msisdn, newimsi, oldimsi, status, transdate) values ( :msisdn, :newimsi , :oldimsi, :status, :transdate)");
		
		
		/**
		sqlQuery.setParameter("msisdn",subscriberDetail.getMsisdn());
		sqlQuery.setParameter("newimsi", reconcildetail.getImsi());
		//sqlQuery.setParameter("oldimsi",subscriberDetail.getImsi());
		sqlQuery.setString("oldimsi", subscriberDetail.getImsi());
		sqlQuery.setString("status", "Activated");
		//sqlQuery.setParameter("transdate", "sysdate");
		sqlQuery.setParameter("transdate", new java.util.GregorianCalendar().getTime());
		**/
		int result = sqlQuery.executeUpdate();
		return result;
		
		
	  } catch(Exception e) 
	   {
		
		e.printStackTrace();
		return -1;
		
	 }
	
}

	
	

	public int createPostSubscriberHomisco(String msisdn, String productType, String cost, String userID)
	{
		/*SQLQuery sqlQuery = this.sessionFactoryTabs.getCurrentSession().createSQLQuery("DECLARE @r1 SMALLINT " +
                            "EXEC [dbo].[UpdateMobileProduct] :msisdn, :productType, :cost, :userID,@Result Output,0 SELECT @r1");*/
		try{
			System.out.println("Msisdn="+msisdn+", productType="+productType+", Cost="+cost+", UserID="+userID);
			SQLQuery sqlQuery = this.sessionFactoryTabs.getCurrentSession().createSQLQuery("DECLARE @r1 SMALLINT EXEC " +
					"UpdateMobileProduct :msisdn, :productType, :cost, :userID, @r1 OUTPUT,1 SELECT @r1");
			sqlQuery.setString("msisdn", msisdn);
			sqlQuery.setString("productType", productType);
			sqlQuery.setString("cost", cost);
			sqlQuery.setString("userID", userID);

			@SuppressWarnings("unchecked")
			Collection<Short> response = sqlQuery.list();

			System.out.println(response);

			if (response.iterator().hasNext())
				return new Short(response.iterator().next()).intValue();
			else return -100;
		}catch(Exception e){
			e.printStackTrace();
			return -100;
		}
	}

	public int saveErrorLogs(ErrorDetail errorDetails)
	{
		this.sessionFactory.getCurrentSession().save(errorDetails);
		return 0;
	}

	public int deleteErrorLogs(ErrorDetail errorDetails)
	{
		this.sessionFactory.getCurrentSession().delete(errorDetails);
		return 0;
	}

	public ArrayList<ErrorDetail> getAllErrorLogs()
	{
		System.out.println("Inside of the getAllErrorLogs method");
		String requestQuery = "from ErrorDetail";

		System.out.println("Query is = "+requestQuery);

		@SuppressWarnings("unchecked")
		ArrayList<ErrorDetail> errors = (ArrayList<ErrorDetail>)this.sessionFactory.getCurrentSession().
				createQuery(requestQuery).list();				

		if (errors.isEmpty())
		{
			return new ArrayList<ErrorDetail>();
		}
		else return errors;
	}

	public int saveRequestTracker(RequestTracker tracker) {
		this.sessionFactory.getCurrentSession().save(tracker);
		return 0;
	}

	public int updateIMEIWhitelist(IMEIWhitelist imeiRow) {
		this.sessionFactory.getCurrentSession().update(imeiRow);
		return 0;
	}

	public int updateMSISDNWhitelist(MSISDNWhitelist msisdnRow) {
		this.sessionFactory.getCurrentSession().update(msisdnRow);
		return 0;
	}

	@SuppressWarnings("unchecked")
	public Ttspromo getTTSPromoDetails(String msisdn){
		ArrayList<Ttspromo> ttsPromo =  (ArrayList<Ttspromo>) this.sessionFactory.getCurrentSession().
				createQuery("from Ttspromo where msisdn = :msisdn").setString("msisdn", msisdn).list();
		if (ttsPromo.isEmpty()){
			return null;
		}else{
			Ttspromo ttspromo = ttsPromo.get(0);
			return ttspromo;
		}
	}


	public int saveTTSPromoDetails(Ttspromo ttsPromo){
		this.sessionFactory.getCurrentSession().saveOrUpdate(ttsPromo);
		return 0;
	}

	public int updateBBPromoInitial(BbTestPromoInitial bbTestPromoInitial){
		this.sessionFactory.getCurrentSession().update(bbTestPromoInitial);
		return 0;
	}

	public int updateBBPromoInitialYes(BbTestPromoFirstYes bbTestPromoFirstYes){
		this.sessionFactory.getCurrentSession().update(bbTestPromoFirstYes);
		return 0;
	}

	public BbTestPromoFirstYes getBBPromoInitialYesStatusByMSISDN(String msisdn){
		String requestQuery = "from BbTestPromoFirstYes where msisdn = :msisdn and status = 0";
		@SuppressWarnings("unchecked")
		ArrayList<BbTestPromoFirstYes> subs = (ArrayList<BbTestPromoFirstYes>)this.sessionFactory.getCurrentSession().
		createQuery(requestQuery).setString("msisdn", msisdn).list();
		if (subs.isEmpty()){
			return null;
		} else {
			BbTestPromoFirstYes bbTestPromo = subs.get(0);
			return bbTestPromo;
		}
	}

	public int saveBBPromoInitialYes(BbTestPromoFirstYes bbTestPromoFirstYes){
		this.sessionFactory.getCurrentSession().save(bbTestPromoFirstYes);
		return 0;
	}

	public HomiscoResults getPostpaidSubscriberCreditLimit(String msisdn)
	{
		System.out.println("MSISDN = "+msisdn+", and the query to run is EXEC EX_SAPCCheckMobileStatus :msisdn");
		SQLQuery sqlQuery = this.sessionFactoryTabs.getCurrentSession().createSQLQuery("EXEC EX_SAPCCheckMobileStatus :msisdn");
		sqlQuery.setString("msisdn", msisdn);
		@SuppressWarnings("unchecked")
		Collection<Object> response = sqlQuery.list();
		Iterator<Object> iterator = response.iterator();
		Object tempValues [] = null;
		HomiscoResults homes = new HomiscoResults();
		if (iterator.hasNext())
		{
			tempValues = (Object[]) iterator.next();
			homes.setStatus((String)tempValues[0]);
			homes.setBalance((BigDecimal)tempValues[1]);
		}

		System.out.println("Value returned from the database {"+tempValues[0]+","+tempValues[1]+"} for MSISDN="+msisdn);
		return homes;
	}

	public int savePostpaidCDR(CDRSubscriptionDetail cdrDetails)
	{
		this.sessionFactory.getCurrentSession().save(cdrDetails);
		return 0;
	}

	public ArrayList<CDRSubscriptionDetail> getAllSubscriptionCDR()
	{
		System.out.println("Inside of the getAllSubscriptionCDR method");
		String requestQuery = "from CDRSubscriptionDetail where status = \'NEW\'";

		System.out.println("Query is = "+requestQuery);

		@SuppressWarnings("unchecked")
		ArrayList<CDRSubscriptionDetail> errors = (ArrayList<CDRSubscriptionDetail>)this.sessionFactory.getCurrentSession().
		createQuery(requestQuery).list();				

		if (errors.isEmpty())
		{
			return new ArrayList<CDRSubscriptionDetail>();
		}
		else return errors;
	}

	public int updateSubscriptionCDR(CDRSubscriptionDetail subscriebrDetail)
	{
		this.sessionFactory.getCurrentSession().update(subscriebrDetail);
		return 0;
	}

	public ArrayList<SubscriberDetail> getSubscriberThatExpiredSomeDaysAgo(int noOfDays) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SubscriberDetail.class);
		criteria.add(Restrictions.ge("next_subscription_date",TimeUtilities.startOfDay(-noOfDays))) //
		.add(Restrictions.eq("status", "Deactivated")).add(Restrictions.ne("postpaidSubscriber", 1));
		@SuppressWarnings("unchecked")
		ArrayList<SubscriberDetail> subs = (ArrayList<SubscriberDetail>) criteria.list();
		return subs;
	}
	
	public ArrayList<SubscriberDetail> getPostpaidSubscribersDueForCurrentMonth() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(SubscriberDetail.class);
		criteria.add(Restrictions.le("next_subscription_date",TimeUtilities.firstDayOfNextMonth()))
		.add(Restrictions.eq("status", "Active")).add(Restrictions.eq("postpaidSubscriber", 1));
		@SuppressWarnings("unchecked")
		ArrayList<SubscriberDetail> subs = (ArrayList<SubscriberDetail>) criteria.list();
		return subs;
	}

	public int saveFutureRenewal(FutureRenewal futureRenewal) {
		this.sessionFactory.getCurrentSession().save(futureRenewal);
		return 0;
	}

	/** 
	 * Relies on Hibernate to return the ArrayList in the order the futurerenewal was purchased
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<FutureRenewal> subscriberFutureRenewals(String msisdn) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(FutureRenewal.class);
		criteria.add(Restrictions.eq("msisdn", msisdn)).add(Restrictions.eq("status", "Available")).addOrder(Order.asc("purchasedAt"));
		return (ArrayList<FutureRenewal>)criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<FutureRenewal> allSubscriberFutureRenewals(String msisdn) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(FutureRenewal.class);
		criteria.add(Restrictions.eq("msisdn", msisdn)).addOrder(Order.asc("purchasedAt"));
		return (ArrayList<FutureRenewal>)criteria.list();
	}
	
//	@SuppressWarnings("unchecked")
//	public Collection<SubscriberDetail> loadAllPostpaidSubscriberDetailsDueForRenewal()
//	{
//		return this.sessionFactory.getCurrentSession().createQuery("from SubscriberDetail where postpaidSubscriber = 1 and " +
//				"statusid = 'Active' and next_subscription_date <= sysdate").list();
//	}
	
	

	
	public int updateFutureRenewal(FutureRenewal futureRenewal) {
		this.sessionFactory.getCurrentSession().update(futureRenewal);
		return 0;
	}
	
	
	
	

	public Users getUserDetailByUsername(String username)
	{
		System.out.println("searching user with username: "+username);
		@SuppressWarnings("unchecked")
		ArrayList<Users> response = (ArrayList<Users>) this.sessionFactory.getCurrentSession().
		createQuery("from Users where username = :msisdn").setString("msisdn", username).list();
		if (response.isEmpty()) return null;
		else
		{
			return response.get(0);
		}
	}
	
	@SuppressWarnings("unchecked")
	public Users getUserDetailByUsername(Users user)
	{
		user.setPassword(user.getPassword());
		user.setEncryptedPassword(user.getEncryptedPassword());
		ArrayList<Users> response = new ArrayList<Users>();
		try {
			response = (ArrayList<Users>) this.sessionFactory.getCurrentSession().
			createQuery("from Users where username = :msisdn and encryptedPassword = :password")
			.setString("password", user.getEncryptedPassword())
			.setString("msisdn", user.getUsername())
			.list();
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		if (response.isEmpty()) return null;
		else
		{
			return response.get(0);
		}
	}
	
	public int updateBrokerUserDetail(Users user)
	{
		try {
			this.sessionFactory.getCurrentSession().update(user);
			return 0;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 1010;
		}
	}
	
	public int saveBrokerUserDetail(Users users)
	{
		this.sessionFactory.getCurrentSession().save(users);
		
		return 0;
	}
	
	public ArrayList<Users> getAllUserDetails()
	{
		System.out.println("Inside of the getAllUserDetails method");
		String requestQuery = "from Users";
				
		System.out.println("Query is = "+requestQuery);
		
		@SuppressWarnings("unchecked")
		ArrayList<Users> errors = (ArrayList<Users>)this.sessionFactory.getCurrentSession().
		createQuery(requestQuery).list();				
		
		if (errors.isEmpty())
		{
			return new ArrayList<Users>();
		}
		else return errors;
	}
	
	public Collection<RolesDetail> getAllRoles()
	{
		return this.sessionFactory.getCurrentSession().createQuery("from RolesDetail").list();
	}
	
	@SuppressWarnings("unchecked")
	public Collection<SubscriberDetail> getSubscriberInformationByImsi(
			String imsi)
	{
		return this.sessionFactory.getCurrentSession().createQuery("from SubscriberDetail where imsi = :imsi")
		.setString("imsi", imsi)
		.list();
	}
	
	
	
	public ArrayList<DeductionLog> getRevenueReportByDetail(String service, int noOfDays, int type, String shortCode)
	{
		System.out.println("service = "+service+",noOfDays = "+noOfDays+",type ="+type+",shortCode ="+shortCode);
		String query = "select imsi,msisdn,service,date_created,shortcode " +
		"from deductionlog where service = :service and trunc(date_created) >= trunc(current_date - :noofdays) " +
		"and isprepaidsubscriber = :type and shortcode = :shortcode order by service";
		
		System.out.println("The query to excute is as follows : "+query);
		
		SQLQuery sqlQuery = this.sessionFactoryTabs.getCurrentSession().createSQLQuery(query);
		sqlQuery.setString("service", service);
		sqlQuery.setInteger("noofdays", noOfDays);
		sqlQuery.setInteger("type", type);
		sqlQuery.setString("shortcode", shortCode);
		
		/*String query = "select imsi,msisdn,service,date_created,shortcode " +
		"from deductionlog order by service";
		
		System.out.println("The query to excute is as follows : "+query);
		
		SQLQuery sqlQuery = this.sessionFactoryTabs.getCurrentSession().createSQLQuery(query);*/
		
		@SuppressWarnings("unchecked")
		Collection<Object> response = sqlQuery.list();//sqlQuery.list();
		
		Iterator<Object> iterator = response.iterator();
		
		Object tempValues [] = null;
		ArrayList<DeductionLog> responseLog = new ArrayList<DeductionLog>();
		Calendar myCal = new GregorianCalendar();
		
		while (iterator.hasNext())
		{
			DeductionLog log = new DeductionLog();
			tempValues = (Object[]) iterator.next();
			
			log.setImsi((String)tempValues[0]);
			log.setMsisdn((String)tempValues[1]);
			log.setService((String)tempValues[2]);
			
			myCal.setTime((Date)tempValues[3]);			
			log.setDate_created(myCal);
			
			log.setShortcode((String)tempValues[4]);
			
			responseLog.add(log);
		}
		for (DeductionLog deduct : responseLog)
		{
			System.out.println("New Date = "+deduct.getDate_created());
		}
		return responseLog;
	}
	
	public ArrayList<Users> getUserDetailsById(String userId)
	{
		System.out.println("Inside of the getUserDetailsById method");
		String requestQuery = "from Users where id = :userid";
				
		System.out.println("Query is = "+requestQuery);
		
		@SuppressWarnings("unchecked")
		ArrayList<Users> errors = (ArrayList<Users>)this.sessionFactory.getCurrentSession().
		createQuery(requestQuery).setString("userid", userId).list();				
		
		if (errors.isEmpty())
		{
			return new ArrayList<Users>();
		}
		else
		{
			return errors;
		}
	}
	
	public ArrayList<DeductionLog> getRevenueReportBySummation(int noOfDays)
	{
		String query = "select count(id), sum(amount),service,date_created from deductionlog where trunc(date_created)" +
				">= trunc(current_date - :noofdays) group by service, date_created";
		
		System.out.println("The query to excute is as follows : "+query);
		
		SQLQuery sqlQuery = this.sessionFactoryTabs.getCurrentSession().createSQLQuery(query);
		sqlQuery.setInteger("noofdays", noOfDays);
		
		
		@SuppressWarnings("unchecked")
		Collection<Object> response = sqlQuery.list();//sqlQuery.list();
		
		Iterator<Object> iterator = response.iterator();
		
		Object tempValues [] = null;
		ArrayList<DeductionLog> responseLog = new ArrayList<DeductionLog>();
		Calendar myCal = new GregorianCalendar();
		
		while (iterator.hasNext())
		{
			DeductionLog log = new DeductionLog();
			tempValues = (Object[]) iterator.next();
			
			log.setId(((BigDecimal)tempValues[0]).toString());
			log.setAmount(((BigDecimal)tempValues[1]).doubleValue());
			log.setService((String)tempValues[2]);
			
			myCal.setTime((Date)tempValues[3]);
			log.setDate_created(myCal);
			
			responseLog.add(log);
		}
				
		return responseLog;
	}
	
	public int updateUserdetailById(Users user)
	{
		System.out.println("Inside of the updateUserdetailById method");
		try {
			this.sessionFactory.getCurrentSession().update(user);
			return 0;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}
	
	public ArrayList<String> getAllShortcodesForReport()
	{
		String query = "select distinct shortcode from deductionlog";
		
		System.out.println("The query to excute is as follows : "+query);
		
		SQLQuery sqlQuery = this.sessionFactoryTabs.getCurrentSession().createSQLQuery(query);
		
		@SuppressWarnings("unchecked")
		Collection<Object> response = sqlQuery.list();//sqlQuery.list();
		
		Iterator<Object> iterator = response.iterator();
		
		String tempValues = null;
		ArrayList<String> responseLog = new ArrayList<String>();
		
		while (iterator.hasNext())
		{
			
			tempValues = (String) iterator.next();			
			
			responseLog.add(tempValues);
		}
		
		return responseLog;
	}
	
	public ArrayList<String> getAllServicesForReport()
	{
		String query = "select distinct service from deductionlog";
		
		System.out.println("The query to excute is as follows : "+query);
		
		SQLQuery sqlQuery = this.sessionFactoryTabs.getCurrentSession().createSQLQuery(query);
		
		@SuppressWarnings("unchecked")
		Collection<Object> response = sqlQuery.list();//sqlQuery.list();
		
		Iterator<Object> iterator = response.iterator();
		
		String tempValues = null;
		ArrayList<String> responseLog = new ArrayList<String>();
		
		while (iterator.hasNext())
		{
			
			tempValues = (String) iterator.next();			
			
			responseLog.add(tempValues);
		}
		
		return responseLog;
	}
	
	public int deleteUserdetailById(Users user)
	{
		System.out.println("Inside of the deleteUserdetailById method");
		
		this.sessionFactory.getCurrentSession().delete(user);
		return 0;
	}


	@SuppressWarnings("unchecked")
	public MenuSession isThereAValidMenuSession(String msisdn) {
		
		ArrayList<MenuSession> response = new ArrayList<MenuSession>();
		try 
		{
			response = (ArrayList<MenuSession>) this.sessionFactory.getCurrentSession().
			createQuery("from MenuSession where msisdn = :msisdn").setString("msisdn", msisdn).list();
		} 
		catch (HibernateException e) 
		{
			e.printStackTrace();
		} 
		if (response.isEmpty()) return null;
		else
		{
			return response.get(0);
		}
	}


	@SuppressWarnings("unchecked")
	public MenuSession isThereAValidMenuSession(String msisdn, String sessionid) {
		ArrayList<MenuSession> response = new ArrayList<MenuSession>();
		try 
		{
			response = (ArrayList<MenuSession>) this.sessionFactory.getCurrentSession().
			createQuery("from MenuSession where msisdn = :msisdn and sessionid = :sessionid").setString("msisdn", msisdn)
			.setString("sessionid", sessionid).list();
		} 
		catch (HibernateException e) 
		{
			e.printStackTrace();
		} 
		if (response.isEmpty()) return null;
		else
		{
			return response.get(0);
		}
	}


	public int saveMenuSession(MenuSession menuSession) {
		// TODO Auto-generated method stub
		this.sessionFactory.getCurrentSession().save(menuSession);
		return 0;
	}


	public int updateMenuSession(MenuSession menuSession) {
		// TODO Auto-generated method stub
		this.sessionFactory.getCurrentSession().update(menuSession);
		return 0;
	}


	@SuppressWarnings("unchecked")
	public ArrayList<MenuTree> getMenuToDisplay(String parent_id) {
		ArrayList<MenuTree> response = new ArrayList<MenuTree>();
		try 
		{
			response = (ArrayList<MenuTree>) this.sessionFactory.getCurrentSession().
			createQuery("from MenuTree where parent_id = :parent").setString("parent", parent_id).list();
		} 
		catch (HibernateException e) 
		{
			e.printStackTrace();
		} 
		if (response.isEmpty()) return null;
		else
		{
			return response;
		}
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<MenuTree> getMenuToDisplay(String parent_id, String serialnos) {
		System.out.println("parent_id = "+parent_id+", serialnos="+serialnos);
		ArrayList<MenuTree> response = new ArrayList<MenuTree>();
		ArrayList<MenuTree> response1 = new ArrayList<MenuTree>();
		ArrayList<MenuTree> response2 = new ArrayList<MenuTree>();
		try 
		{
			response = (ArrayList<MenuTree>) this.sessionFactory.getCurrentSession().
					createQuery("from MenuTree where parent_id = :parent order by rownumber").setString("parent", parent_id).list();
			
			if (response.isEmpty())
			{
				return null;
			}
			else
			{
				for (MenuTree menuTree : response) {
					
					if (menuTree.getSerialno().equalsIgnoreCase(serialnos))
					{
						response2 = (ArrayList<MenuTree>) this.sessionFactory.getCurrentSession().
								createQuery("from MenuTree where parent_id = :id").setString("id", menuTree.getId()).list();
						
						if (response2.isEmpty())
						{
							continue;
						}
						else
						{							
							for (MenuTree menuTree1 : response2) {
								response1.add(menuTree1);
							}							
						}
					}
					else continue;
				}
			}
		} 
		catch (HibernateException e) 
		{
			e.printStackTrace();
		} 
		if (response1.isEmpty()) return null;
		else
		{
			return response1;
		}
	}
	
	
	public int deleteMenuTreeByMsisdn(MenuSession menuTree) {
		// TODO Auto-generated method stub
		this.sessionFactory.getCurrentSession().delete(menuTree);
		return 0;
	}

	
	public int saveDeductionLogs(DeductionLog deductionLog)
	{
		this.sessionFactory.getCurrentSession().save(deductionLog);
		return 0;
	}


	
}
