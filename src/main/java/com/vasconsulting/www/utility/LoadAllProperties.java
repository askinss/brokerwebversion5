/**
 * This class is used to load the configuration file of the application. This class can be called by
 * any other class that wants to load any property in the application.
 * @author Nnamdi Jibunoh
 */
package com.vasconsulting.www.utility;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class LoadAllProperties {
	private InputStream inputStream;
	private Logger logger;
	private Properties properties;
	private String propertiesFile;
	
	public LoadAllProperties(){
		 
		inputStream = this.getClass().getClassLoader().getResourceAsStream("config/applicationconfig.properties");
		properties  = new Properties();
		logger = Logger.getLogger(LoadAllProperties.class);
		 
		try {
			properties.load(inputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("@@@@An IOException has occured while loading properties file@@@@", e);
		}
	}
	
	public String getProperty(String property){
		return properties.getProperty(property);
	}

	public String getPropertiesFile() {
		return propertiesFile;
	}

	public void setPropertiesFile(String propertiesFile) {
		this.propertiesFile = propertiesFile;
	}
}
