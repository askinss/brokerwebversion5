package com.vasconsulting.www.utility;

public class SubscriberState {
	
	private String msisdn;
	private String subdata;
	
	public String getMsisdn()
	{
		return msisdn;
	}
	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}
	public String getSubdata()
	{
		return subdata;
	}
	public void setSubdata(String subdata)
	{
		this.subdata = subdata;
	}
	
		
}
