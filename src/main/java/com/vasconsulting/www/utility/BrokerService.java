package com.vasconsulting.www.utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

import net.rubyeye.xmemcached.MemcachedClient;
import net.rubyeye.xmemcached.exception.MemcachedException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.vasconsulting.www.domain.ErrorDetail;
import com.vasconsulting.www.domain.FutureRenewal;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;

public class BrokerService {
	private LoadAllBillingPlanObjects loadAllBillingPlanObjects;

	public void setLoadAllBillingPlanObjects(
			LoadAllBillingPlanObjects loadAllBillingPlanObjects) {
		this.loadAllBillingPlanObjects = loadAllBillingPlanObjects;
	}

	private BillingPlanObjectUtility billingPlanObject;
	private ArrayList<BillingPlanObjectUtility> billingPlans;
	private LoadAllProperties loadProperties;

	public void setLoadProperties(LoadAllProperties loadProperties) {
		this.loadProperties = loadProperties;
	}

	private boolean isProvisioningShortcode = false;
	private boolean isScheduledJob = false;
	private MemcachedClient memcachedClient;
	private ErrorDetail errorDetail;
	private int provisionStatus;
	private HibernateUtility hibernateUtility;
	private EmailTaskExecutor myEmailTaskExecutor;
	private SendSmsToKannelService smsService;
	private FutureRenewal futureRenewal;

	public void setFutureRenewal(FutureRenewal futureRenewal) {
		this.futureRenewal = futureRenewal;
	}

	private Logger logger = Logger.getLogger(BrokerService.class);

	public SendSmsToKannelService getSmsService() {
		return smsService;
	}

	public void setSmsService(SendSmsToKannelService smsService) {
		this.smsService = smsService;
	}

	public HibernateUtility getHibernateUtility() {
		return hibernateUtility;
	}

	@Autowired
	public void setHibernateUtility(HibernateUtility hibernateUtility) {
		this.hibernateUtility = hibernateUtility;
	}

	public EmailTaskExecutor getMyEmailTaskExecutor() {
		return myEmailTaskExecutor;
	}

	public void setMyEmailTaskExecutor(EmailTaskExecutor myEmailTaskExecutor) {
		this.myEmailTaskExecutor = myEmailTaskExecutor;
	}

	public LoadAllProperties getProperties() {
		return loadProperties;
	}

	public void setProperties(LoadAllProperties properties) {
		this.loadProperties = properties;
	}


	public MemcachedClient getMemcachedClient() {
		return memcachedClient;
	}

	public void setMemcachedClient(MemcachedClient memcachedClient) {
		this.memcachedClient = memcachedClient;
	}


	public int simswap(SubscriberDetail subscriberDetail) {
		int respCode = provision("simswap", false, subscriberDetail, false);
		if (respCode == StatusCodes.SUCCESS)
			return StatusCodes.SUCCESS;
		else if (respCode == StatusCodes.SUBSCRIBER_NOT_ALLOWED
				|| respCode == StatusCodes.POSTPAID_SUBSCRIBER_NOT_ALLOWED
				|| respCode == StatusCodes.PREPAID_SUBSCRIBER_NOT_ALLOWED) {
			logger.info("Sim swap response code is: " + respCode);
			return respCode;
		} else
			return provision("hardSimSwap", false, subscriberDetail, false);
	}

	public int imsiswap(SubscriberDetail subscriberDetail) {
		int respCode = provision("simswap", false, subscriberDetail, false);
		if (respCode == StatusCodes.SUCCESS)
			return StatusCodes.SUCCESS;
		else if (respCode == StatusCodes.SUBSCRIBER_NOT_ALLOWED
				|| respCode == StatusCodes.POSTPAID_SUBSCRIBER_NOT_ALLOWED
				|| respCode == StatusCodes.PREPAID_SUBSCRIBER_NOT_ALLOWED) {
			logger.info("Imsi recon swap response code is: " + respCode);
			return respCode;
		} else
			return provision("hardSimSwap", false, subscriberDetail, false);
	}
	
	public int deactivate(SubscriberDetail subscriberDetail) {
		provisionStatus = provision("deactivate", false, subscriberDetail, false);
		if (provisionStatus == StatusCodes.SUCCESS) {
			sendSMS(loadProperties.getProperty("successfulDeactivationMessage"),
					subscriberDetail);
		}
		return provisionStatus;
	}

	public int activate(String shortCode, SubscriberDetail subscriberDetail) {
		isProvisioningShortcode = true;
		logger.info("Commenced processing " + shortCode + " for "
				+ subscriberDetail.getMsisdn());
		boolean futureRenewalSyncStarted = false;
		provisionStatus = provision(shortCode, true, subscriberDetail, false);
		logger.info("Response from provisioning pre future renewal is: "
				+ provisionStatus + " for "+subscriberDetail.getMsisdn());
		if (loadProperties.getProperty("allowFutureRenewal").equalsIgnoreCase(
				"true")
				&& (provisionStatus == StatusCodes.SUBSCRIBER_CANNOT_MIGRATE_WHILE_ACTIVE || provisionStatus == StatusCodes.SUBCRIBER_IS_ALREADY_IN_ACTIVE_STATE)
				&& (subscriberDetail.getPostpaidSubscriber() != 1)) {
			logger.info("About to provision Future Renewal for subscriber with MSISDN: "
					+ subscriberDetail.getMsisdn()
					+ subscriberDetail.getShortCode());
			futureRenewalSyncStarted = true;
			provisionStatus = syncSubForFutureRenewal(subscriberDetail);
		}
		if ((provisionStatus != StatusCodes.SUCCESS)
				&& !futureRenewalSyncStarted) {
			if (!isNoRollBackShortCode(shortCode)
					&& isRollBackErrorCode(provisionStatus)) {
				isProvisioningShortcode = false;
				rollback(subscriberDetail);
			}
			sendSMS(loadProperties.getProperty(String.valueOf(provisionStatus)),
					subscriberDetail.getMsisdn(), shortCode);
		}
		return provisionStatus;
	}

	private void sendSuccessMessage(
			BillingPlanObjectUtility billingPlanObjectUtility, int status, SubscriberDetail subscriberDetail) {
		if (!billingPlanObjectUtility.getSuccessMessage().equalsIgnoreCase("")
				&& (status == StatusCodes.SUCCESS)) {
			sendSMS(billingPlanObjectUtility.getSuccessMessage(),
					subscriberDetail);
		}
	}

	private void sendSMS(String message, String phoneNumber, String shortCode) {
		if (isShortCodeAllowedToReceiveSMS(shortCode)) {
			smsService.sendMessageToKannel(message, phoneNumber);
		}
	}

	private void sendSMS(String message, SubscriberDetail subscriberDetail) {
		if (isShortCodeAllowedToReceiveSMS(subscriberDetail.getShortCode()))
			smsService.sendMessageToKannel(message, subscriberDetail.getMsisdn());
	}

	private boolean isShortCodeAllowedToReceiveSMS(String shortcode) {
		return !Arrays.asList(
				loadProperties.getProperty("nosmstoshortcode").toLowerCase()
						.split(",")).contains(shortcode.toLowerCase());
	}

	public int scheduleJob(String shortCode, SubscriberDetail subscriberDetail, boolean zeroRate) {
		isProvisioningShortcode = true;
		isScheduledJob = true;		
		int response = provision(shortCode, true, subscriberDetail, zeroRate);
		logger.info("Scheduler Provisioning Response for provisioning "
				+ subscriberDetail.getMsisdn() + " is " + response);
		if (!(isNoRollBackScheduler(response, subscriberDetail))) {
			rollback(subscriberDetail);
		} else if (response == StatusCodes.SUCCESS) {
			if (billingPlanObject != null) {
				if (billingPlanObject.getSuccessMessage() != null
						&& !billingPlanObject.getSuccessMessage()
								.equalsIgnoreCase("")) {
					logger.info("Sending "
							+ getBillingPlanObject().getSuccessMessage()
							+ "to: " + subscriberDetail.getMsisdn());
					smsService.sendMessageToKannel(getBillingPlanObject()
							.getSuccessMessage(), subscriberDetail.getMsisdn());
				}
			}
		}
		futureRenewal.setStatus(null);
		return response;
	}

	boolean isNoRollBackScheduler(int response, SubscriberDetail subscriberDetail) {
		futureRenewal = (futureRenewal == null) ? new FutureRenewal()
				: futureRenewal;
		if (futureRenewal.getStatus() == null
				&& response != StatusCodes.SUCCESS
				&& !isNoRollBackErrorCode(response)) {
			return false;
		} else {
			logger.info("Skipping rollback for " + subscriberDetail.getMsisdn());
			return true;
		}

	}

	void rollback(SubscriberDetail subscriberDetail) {
		try {
			logger.info("About to commence rollback for "
					+ subscriberDetail.getMsisdn());
			isProvisioningShortcode = false;
			provision("rollback", false, subscriberDetail, false);
			errorDetail = new ErrorDetail();
			errorDetail.setId(UUID.randomUUID().toString());
			errorDetail.setDate_created(new GregorianCalendar());
			errorDetail.setDescription("Provisioning Response msisdn: "
					+ subscriberDetail.getMsisdn() + " on shortcode: "
					+ subscriberDetail.getShortCode());
			errorDetail.setErrorCode(provisionStatus);
			errorDetail.setErrorType("");
			errorDetail.setMsisdn(subscriberDetail.getMsisdn());
			hibernateUtility.saveErrorLogs(errorDetail);
		} catch (Exception e) {
			sendFailureEmail(subscriberDetail);
			e.printStackTrace();
		}
	}

	private boolean isNoRollBackShortCode(String shortCode) {
		return Arrays.asList(
				loadProperties.getProperty("norollbackshortcodes")
						.toLowerCase().split(",")).contains(
				shortCode.toLowerCase());
	}

	private boolean isRollBackErrorCode(int errorCode) {
		return Arrays.asList(
				loadProperties.getProperty("rollbackerrorcodes").toLowerCase()
						.split(","))
				.contains(new Integer(errorCode).toString());
	}

	boolean isNoRollBackErrorCode(int errorCode) {
		return Arrays.asList(
				loadProperties.getProperty("norollbackerrorcodes")
						.toLowerCase().split(",")).contains(
				new Integer(errorCode).toString());
	}

	private void sendFailureEmail(SubscriberDetail subscriberDetail) {
		try {
			myEmailTaskExecutor.sendQueuedEmails("Provisioning request for "
					+ subscriberDetail.getMsisdn() + " exited with error code "
					+ provisionStatus + ". Please check and rectify manually");
		} catch (Exception e) {
			e.printStackTrace(); // Nobody reads emails
		}

	}

	public int provision(String shortcode, boolean isNormalShortcode, SubscriberDetail subscriberDetail, boolean zeroRate) {
		billingPlans = loadAllBillingPlanObjects.getAllBillingPlans();
		memcachedClient = (memcachedClient == null) ? (MemcachedClient) ContextLoaderImpl
				.getBeans("memcachedClient") : memcachedClient;
		int response = StatusCodes.OTHER_ERRORS;
		boolean shortcodeFound = false;
		for (BillingPlanObjectUtility billingPlanObjectUtility : billingPlans) {
			if (billingPlanObjectUtility.getShortCode().equalsIgnoreCase(
					shortcode.trim())) {
				synchronized (billingPlanObjectUtility) {
					try {
						if (cacheMsisdn(shortcode, subscriberDetail) != 0) {
							return StatusCodes.SUBSCRIBER_IN_SESSION;
						}
						BillingPlanObjectUtility preCommands = billingPlanObjectUtility;
						String cost = billingPlanObjectUtility.getCost();
						if (zeroRate){
							billingPlanObjectUtility.setCost("0");
						}
						if (isNormalShortcode) {
							subscriberDetail = createSubscriberSessionObject(billingPlanObjectUtility,subscriberDetail);
						}
						response = runCommands(billingPlanObjectUtility, subscriberDetail);
						billingPlanObjectUtility = preCommands; //keeping billing Plan in pristine state
						billingPlanObjectUtility.setCost(cost);
						this.billingPlanObject = billingPlanObjectUtility;
						deleteCachedMsisdn(subscriberDetail);
					} catch (Exception e) {
						e.printStackTrace();
					}
					sendSuccessMessage(billingPlanObjectUtility, response, subscriberDetail);
				}
				shortcodeFound = true;
				break;
			}
		}
		if (!shortcodeFound)
			response = StatusCodes.SHORTCODE_NOT_FOUND;
		return response;

	}

	private SubscriberDetail createSubscriberSessionObject(BillingPlanObjectUtility billingPlanObjectUtility, SubscriberDetail sub) {
		sub.setMsisdn(stripLeadingMsisdnPrefix(sub.getMsisdn()));
		if (isScheduledJob){
			sub.setShortCode(billingPlanObjectUtility.getShortCode().toLowerCase().replace("sch", ""));
		}
		else 
		{
			sub.setShortCode(billingPlanObjectUtility.getShortCode());
			sub.setAutoRenew(1);
			sub.setPrepaidSubscriber(1);
		}
		logger.info("This is billingPlan Validity: "+billingPlanObjectUtility
				.getValidity());
		
		sub.setServicetype(new Integer(billingPlanObjectUtility.getValidity()));
		logger.info("This is the serviceType " + sub.getServicetype() +" set on " + sub.getMsisdn());
		
		return sub;
	}

	private int cacheMsisdn(String shortcode, SubscriberDetail subscriberDetail) {
		try {
			if (!memcachedClient.add(subscriberDetail.getMsisdn(), 1800,
					subscriberDetail.getMsisdn())) { // Cache expiry is set for
														// 30minutes
				logger.info("Subscriber with msisdn: "
						+ subscriberDetail.getMsisdn()
						+ " has a pending request");
				return StatusCodes.SUBSCRIBER_IN_SESSION;
			}
		} catch (TimeoutException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (MemcachedException e) {
			e.printStackTrace();
		}
		return 0;
	}

	private void deleteCachedMsisdn(SubscriberDetail subscriberDetail) {
		try {
			memcachedClient.delete(subscriberDetail.getMsisdn());
			logger.info("Successfully deleted Cached MSISDN "
					+ subscriberDetail.getMsisdn() + " from cache");
		} catch (TimeoutException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (MemcachedException e) {
			e.printStackTrace();
		}
	}

	public LoadAllBillingPlanObjects getLoadSimSwapBillingPlanObjects() {
		return loadAllBillingPlanObjects;
	}

	public void setLoadSimSwapBillingPlanObjects(
			LoadAllBillingPlanObjects loadSimSwapBillingPlanObjects) {
		this.loadAllBillingPlanObjects = loadSimSwapBillingPlanObjects;
	}

	private int runCommands(BillingPlanObjectUtility billingPlanObjectUtility, SubscriberDetail subscriberDetail) {
		int response = StatusCodes.OTHER_ERRORS;

		ArrayList<CommandPropertiesUtility> commandProperties = billingPlanObjectUtility.getCoomandObjects();
		for (CommandPropertiesUtility commandProps : commandProperties) {
			try {
				Command command = (Command) ContextLoaderImpl
						.getBeans(commandProps.getCommand());
				logger.info("Configured receiver is: "
						+ commandProps.getReceiver());
				logger.info("Configured param is: " + commandProps.getParam());
				logger.info("Configured cost for "
						+ billingPlanObjectUtility.getShortCode() + " is "
						+ billingPlanObjectUtility.getCost()
						+ " to be provisioned for "
						+ subscriberDetail.getMsisdn() + "for " 
						+ subscriberDetail.getServicetype() + " no of days " 
						+ " at running command "
						+ command.getClass());
				command.setReceiverParameters(commandProps.getParam());
				command.setReceiver(commandProps.getReceiver());
				command.setSusbcriberDetail(subscriberDetail);
				command.setBillingPlanObject(billingPlanObjectUtility);
				response = command.execute();
			} catch (Exception e) {
				e.printStackTrace();
				response = StatusCodes.OTHER_ERRORS;
			}
			if (response > StatusCodes.SUCCESS) {
				return response;
			} else
				continue;
		}
		return response;
	}

	public int syncSubForFutureRenewal(SubscriberDetail subscriberDetailFuture) {
		int response = StatusCodes.OTHER_ERRORS;
		BillingPlanObjectUtility billingOrig = new BillingPlanObjectUtility();
		for (BillingPlanObjectUtility billingPlanObjectUtility : billingPlans) {
			if (billingPlanObjectUtility.getShortCode().equalsIgnoreCase(
					subscriberDetailFuture.getShortCode())) {
				billingOrig = billingPlanObjectUtility;
				break;
			}
		}
		for (BillingPlanObjectUtility billingPlanObjectUtility : billingPlans) {
			if (billingPlanObjectUtility.getShortCode().equalsIgnoreCase(
					"futurerenewal")) {
				// Ensure successmessage(removing battery) is not sent for
				// future renewal
				billingOrig.setSuccessMessage(billingPlanObjectUtility.getSuccessMessage());
				billingOrig.setCoomandObjects(billingPlanObjectUtility
						.getCoomandObjects());
				response = runCommands(billingOrig, subscriberDetailFuture);
				break;
			}
		}
		return response;
	}

	private String stripLeadingMsisdnPrefix(String msisdn) {
		if (msisdn.startsWith("+")) {
			return msisdn.substring(1, msisdn.length());
		} else
			return msisdn;
	}

	public BillingPlanObjectUtility getBillingPlanObject() {
		return billingPlanObject;
	}

}
