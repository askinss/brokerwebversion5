/**
 * This class is used to check the type of subscriber. If the subscriber is a POSTPAID or PREPAID subscriber
 * It returns 0 if subscriber is postpaid and 500 if prepaid.
 * @author nnamdi Jibunoh
 */
package com.vasconsulting.www.utility;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.log4j.Logger;


public class InvokeExternalURLCommandImpl {
	private String receiverParams;
	private HttpClient httpClient = new HttpClient();
	private XMLUtility xmlUtility = new  XMLUtility();
	Logger logger = Logger.getLogger(InvokeExternalURLCommandImpl.class);
		
	/**
	 * This method will execute and call an external url to execute the requested functional.
	 * @author Nnamdi Jibunoh
	 * @param
	 */
	public int execute(String url, String msisdn, String timeout, String autoResponse) {
				
		logger.info("Execute called on InvokeExternalURLCommandImpl for subscriber wit MSISDN = "+msisdn);
		logger.info("The value of the URL is "+url+ ", the timeout is "+timeout);
		
		url = url.replaceAll("%26", "&");
		
		httpClient = new HttpClient();
						
		url = String.format(url, msisdn);
				
		GetMethod method = new GetMethod(url);
		method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, 
	    		new DefaultHttpMethodRetryHandler(2, false));
		method.getParams().setParameter(HttpMethodParams.SO_TIMEOUT, (new Integer(timeout) * 100));//new Integer(timeout) * 100
		
		try {
			
			int statusCode = httpClient.executeMethod(method);		
			byte[] responseBody = method.getResponseBody();
			
			logger.info("The response from call is "+statusCode+" and the value of HttpStatus.SC_OK = "+HttpStatus.SC_OK);
			
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			logger.info("Returning zero");
			return 0;
		}
		
		
		return 0;			
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
		
	private String stripLeadingMsisdnPrefix(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("0")){
			return Msisdn.substring(1, Msisdn.length());
		}
		else if (Msisdn.startsWith("234")){
			return Msisdn.substring(3, Msisdn.length());
		}
		else if(Msisdn.startsWith("+234")){
			return Msisdn.substring(4, Msisdn.length());
		}
		else return Msisdn;
	}

		

}
