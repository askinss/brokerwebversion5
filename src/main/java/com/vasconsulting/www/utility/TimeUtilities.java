package com.vasconsulting.www.utility;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class TimeUtilities {

	public static GregorianCalendar startOfDay(int noOfDays){
		GregorianCalendar day = new GregorianCalendar();
		day.set(GregorianCalendar.HOUR, 0);
		day.set(GregorianCalendar.MINUTE, 0);
		day.set(GregorianCalendar.SECOND, 0);
		day.set(GregorianCalendar.MILLISECOND, 0);
		day.set(GregorianCalendar.AM_PM, GregorianCalendar.AM);
		day.add(GregorianCalendar.DAY_OF_MONTH, noOfDays);
		System.out.println("Date calculated is: "+day.getTime());
		return day;
	}
	
	public static GregorianCalendar firstDayOfNextMonth(){
		GregorianCalendar day = dayOfNextMonth(1);
		return day;
	}
	
	public static GregorianCalendar dayOfNextMonth(int day){
		GregorianCalendar cal = new GregorianCalendar();
		cal.set(GregorianCalendar.MONTH, (cal.get(Calendar.MONTH) + 1));
		cal.set(GregorianCalendar.DAY_OF_MONTH, day);
		return cal;
	}
	
}
