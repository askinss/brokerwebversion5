package com.vasconsulting.www.utility;

import java.util.ArrayList;
import java.util.HashMap;

public class SubscriberUSSDConversation
{	
	private HashMap<String, ArrayList<String>> container = new HashMap<String, ArrayList<String>>();

	public ArrayList<String> getMsisdnFromContainer(String msisdn)
	{
		return container.get(msisdn);
	}
	
	public boolean isMsisdnInContainer(String msisdn)
	{
		return container.containsKey(msisdn);
	}
	
	public HashMap<String, ArrayList<String>> getContainerContent()
	{
		return container;
	}

	public void saveInContainer(String msisdn, ArrayList<String> value)
	{
		if (!container.containsKey(msisdn))
			this.container.put(msisdn, value);
		else 
		{
			this.container.remove(msisdn);
			this.container.put(msisdn, value);
		}
	}
	
	public int getCurrentSizeOfContainer()
	{
		return this.container.size();
	}
	
	public void removeFromContainer(String msisdn)
	{
		if (container.containsKey(msisdn))
		{
			container.remove(msisdn);
		}
	}
}
