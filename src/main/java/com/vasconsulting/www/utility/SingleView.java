package com.vasconsulting.www.utility;

import java.rmi.RemoteException;
import java.util.HashMap;

import com.intecbilling.tresoap._2_0.USSD.CHANGESERVICE;
import com.intecbilling.tresoap._2_0.USSD.CHANGESERVICEResponseType;
import com.intecbilling.tresoap._2_0.USSD.FetchAvailableUsageLimit;
import com.intecbilling.tresoap._2_0.USSD.FetchAvailableUsageLimitResp;
import com.intecbilling.tresoap._2_0.USSD.CSSParam;
import com.intecbilling.tresoap._2_0.USSD.CSSParamList;
import com.intecbilling.tresoap._2_0.USSD.CSSService;
import com.intecbilling.tresoap._2_0.USSD.CSSServiceList;
import com.intecbilling.tresoap._2_0.USSD.PCSServices;
import com.intecbilling.tresoap._2_0.USSD_IN.USSD_INPortTypeProxy;
import com.intecbilling.tresoap.fault._2_0.ConnectionFaultDetail;
import com.vasconsulting.www.interfaces.SingleInterface;


public class SingleView implements SingleInterface {
	private LoadAllProperties properties;

	private CHANGESERVICEResponseType responseType;

	public LoadAllProperties getProperties() {
		return properties;
	}

	public void setProperties(LoadAllProperties properties){
		this.properties = properties;
	}

	private XMLUtility xmlUtility;

	public XMLUtility getXmlUtility() {
		return xmlUtility;
	}

	public void setXmlUtility(XMLUtility xmlUtility) {
		this.xmlUtility = xmlUtility;
	}

	public String deleteService(String msisdn, String equipID) {
		return deleteService(msisdn, equipID, "D");
	}

	public String changeService(String msisdn, String equipID){
		return changeService(msisdn, equipID, "I");
	}


	public String changeService(String msisdn, String equipID, String changeType) {
		USSD_INPortTypeProxy proxy = new USSD_INPortTypeProxy();
		CHANGESERVICE service = new CHANGESERVICE();
		service.setPArea("0");
		service.setPSubscrType("G");
		service.setPSubNo(msisdn);
		service.setPUsername(properties.getProperty("singleViewUserName")); //Load username dynamically from properties file

		//Sets XML to be sent
		PCSServices pService = new PCSServices();

		//Service lists
		CSSServiceList []params = new CSSServiceList[1];

		//Array to hold the param list
		CSSParamList []params1 = new CSSParamList[1];

		CSSServiceList service1 = new CSSServiceList();

		CSSService service11 = new CSSService();
		service11.setEquipID(equipID);
		service11.setAction(changeType);
		service11.setSerialNo("1");

		//Optional Fields
		CSSParam pp = new CSSParam();
		pp.setParamName("OFFER");
		pp.setParamValue(equipID); 	//This has to be set with the equipID supplied
		CSSParamList ss = new CSSParamList();
		ss.setSParam(pp);
		params1[0] = ss;

		service11.setSParamList(params1);
		service1.setSService(service11);
		params[0] = service1;
		service.setPAdditionalparams(params1);
		pService.setXMLServices(params);
		service.setPServices(pService);

		try {
			responseType =
			proxy.CHANGESERVICE_Update(service);
			String singleResponse = responseType.getService().getOperation().getOutputParams().getDescription();
			System.out.println("Response from SingleView is " + singleResponse);
			return singleResponse;
		}  catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return " ";
		}


	}

	public String deleteService(String msisdn, String equipID, String changeType) {
		USSD_INPortTypeProxy proxy = new USSD_INPortTypeProxy();
		CHANGESERVICE service = new CHANGESERVICE();
		service.setPArea("0");
		service.setPSubscrType("G");
		service.setPSubNo(msisdn);
		service.setPUsername(properties.getProperty("singleViewUserName"));

		//Sets XML to be sent
		PCSServices pService = new PCSServices();

		//Service lists
		CSSServiceList []params = new CSSServiceList[1];

		//Array to hold the param list
		CSSParamList []params1 = new CSSParamList[1];

		CSSServiceList service1 = new CSSServiceList();

		CSSService service11 = new CSSService();
		service11.setEquipID(equipID);
		service11.setAction(changeType);
		service11.setSerialNo("1");

		//Optional Fields
		CSSParam pp = new CSSParam();
		pp.setParamName("OFFER");
		pp.setParamValue(equipID);
		CSSParamList ss = new CSSParamList();
		ss.setSParam(pp);
		params1[0] = ss;

		service11.setSParamList(params1);
		service1.setSService(service11);
		params[0] = service1;
		service.setPAdditionalparams(params1);
		pService.setXMLServices(params);
		service.setPServices(pService);

		try {
			responseType =
			proxy.CHANGESERVICE_Update(service);
			String singleResponse = responseType.getService().getOperation().getOutputParams().getDescription();
			System.out.println("Response from SingleView is " + singleResponse);
			return singleResponse;
		}  catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}

	}

	public String getSubscriberInformation(String msisdn) {
		USSD_INPortTypeProxy proxy = new USSD_INPortTypeProxy();
		FetchAvailableUsageLimit limit = new FetchAvailableUsageLimit();
		FetchAvailableUsageLimitResp limitResp;

		limit.setSUBSCRIBER_NO(msisdn);

		try {

			limitResp = proxy.fetchAvailableUsageLimit_Fetch(limit);

			String singleResponse = limitResp.getMESSAGE();
			return  singleResponse;

		}
		catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return  "";
	}


	public static void main(String[] args) {
		if (args.length < 2) {
			System.out.println("Supply the msisdn and EquipID after this..");
		} else {
			SingleView singleView = new SingleView();
			System.out.println(singleView.getSubscriberInformation((args[0])));
			System.out.println(singleView.changeService((args[0]), (args[1])));
			System.out.println(singleView.deleteService((args[0]), (args[1])));

		}
	}



}



