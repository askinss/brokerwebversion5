package com.vasconsulting.www.utility;

import org.springframework.core.task.TaskExecutor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

public class EmailTaskExecutor
{
	private JavaMailSenderImpl mailSender;
	private SimpleMailMessage mailMessage;
	private TaskExecutor taskExecutor;
	
	public void setMailSender(JavaMailSenderImpl mailSender)
	{
		this.mailSender = mailSender;
	}
	public void setMailMessage(SimpleMailMessage mailMessage)
	{
		this.mailMessage = mailMessage;
	}
	public void setTaskExecutor(TaskExecutor taskExecutor)
	{
		this.taskExecutor = taskExecutor;
	}
	
	private class MailSenderTaskImpl implements Runnable {
		private String message;
		private String[] receipent;
		private boolean isSingleReceipient = true;
		
		public MailSenderTaskImpl(String message)
		{
			this.message = message;
		}
		public MailSenderTaskImpl(String message, String[] receipent)
		{
			this.message = message;
			this.receipent = receipent;
			this.isSingleReceipient = false;
		}
		public void run()
		{
			if (isSingleReceipient)
			{
				mailMessage.setText(this.message);
				mailSender.send(mailMessage);	
			}
			else
			{
				mailMessage.setText(this.message);
				mailMessage.setTo(this.receipent);
				mailSender.send(mailMessage);	
			}
		}
	}
	
	public void sendQueuedEmails(String messageText)
	{
		taskExecutor.execute(new MailSenderTaskImpl(messageText));
	}
	
	public void sendQueuedEmails(String messageText, String[] receipent)
	{
		taskExecutor.execute(new MailSenderTaskImpl(messageText, receipent));
	}
}
