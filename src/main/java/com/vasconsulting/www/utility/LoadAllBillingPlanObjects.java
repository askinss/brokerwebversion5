/**
 *This class is called by Spring to load the application configuration file from the application
 *xml. After the xml file is loaded into memory, other classes can now call the getAllBillingPlanObject
 *method to get the loaded plans.
 *The class will be given an application scope in Spring.
 *@author nnamdi Jibunoh
 */
package com.vasconsulting.www.utility;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public class LoadAllBillingPlanObjects
{
	private XMLUtility xmlUtility;
	private ArrayList<BillingPlanObjectUtility> allBillingPlans = new ArrayList<BillingPlanObjectUtility>();
	
	public void setXmlUtility(XMLUtility xmlUtility)
	{
		this.xmlUtility = xmlUtility;
	}

	public void getAllBillingPlanObjects() throws FileNotFoundException{
		allBillingPlans = xmlUtility.loadApplicationConfigurationXML();
	}

	public ArrayList<BillingPlanObjectUtility> getAllBillingPlans()
	{
		return allBillingPlans;
	}
	
	
}
