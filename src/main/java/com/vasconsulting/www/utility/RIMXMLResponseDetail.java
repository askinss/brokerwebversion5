package com.vasconsulting.www.utility;

public class RIMXMLResponseDetail
{
	private String errorCode;
	private String errorDescription;
	private String serviceName;
	private String imsi;
	public String getErrorCode()
	{
		return errorCode;
	}
	public void setErrorCode(String errorCode)
	{
		this.errorCode = errorCode;
	}
	public String getErrorDescription()
	{
		return errorDescription;
	}
	public void setErrorDescription(String errorDescription)
	{
		this.errorDescription = errorDescription;
	}
	public String getServiceName()
	{
		return serviceName;
	}
	public void setServiceName(String serviceName)
	{
		this.serviceName = serviceName;
	}
	public String getImsi()
	{
		return imsi;
	}
	public void setImsi(String imsi)
	{
		this.imsi = imsi;
	}
	
	public String toString()
	{
		return this.imsi+", "+this.serviceName+", "+this.errorCode+", "+this.errorDescription;
	}
}
