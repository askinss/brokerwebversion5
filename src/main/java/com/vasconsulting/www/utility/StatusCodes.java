package com.vasconsulting.www.utility;

public class StatusCodes
{	
	public final static int SUCCESS = 0;
	public final static int PREPAID_SUBSCRIBER = 500;
	public final static int POSTPAID_SUBSCRIBER = 501;
	
	//General Errors
	public final static int INVALID_MESSAGE = 1000;
	public final static int ERROR_SETTING_AUTORENEWAL = 1001;
	public final static int OTHER_ERRORS = 1010;
	public final static int WRONG_STATUS_FORMAT = 1011;
	public final static int IMSI_AND_MSISDN_CANNOT_BE_EMPTY = 1012;
	public final static int SUBCRIBER_IS_NOT_ON_LEGAL_STATE = 1013;
	public final static int SUBCRIBER_IS_ALREADY_IN_ACTIVE_STATE = 1014;
	public final static int NO_SUBSCRIBER_FOUND_WITH_DETAILS= 1015;
	public final static int SUBSCRIBER_CANNOT_MIGRATE_WHILE_ACTIVE= 1016;
	public final static int SUBSCRIBER_SERVICECLASS_NOT_ALLOWED = 1017;
	public final static int WRONG_AUTORESPONSE_STATUS_CONFIG_FORMAT = 1018;
	public final static int WRONG_AUTORESPONSE_MODE_CONFIG_FORMAT = 1019;
	public final static int OTHER_ERROR_RENEWAL = 1021;
	public final static int INSUFFICENT_RENEWAL_BALANCE = 1022;
	public final static int POSTPAID_SUBSCRIBER_NOT_ALLOWED = 1023;
	public final static int NO_AUTORENEWAL = 1024;
	public final static int PREPAID_SUBSCRIBER_NOT_ALLOWED = 1027;
	
	//TABS Error Range
	public final static int BARRED_SUBSCRIBER = 1020;
	public final static int IMSI_NOT_FOUND = 1030;
	public final static int INSUFFICENT_BALANCE = 1040;
	public final static int OTHER_ERRORS_IMSI = 1050;
	public final static int OTHER_ERRORS_CHANGE_SERVICE = 1060;
	public final static int WRONG_SERVICEVALUE_FORMAT = 1070;
	public final static int NO_CREDIT_LIMIT = 1080;
	
	//RIM Range of errors
	public final static int WRONG_SERVICENAME_FORMAT = 3000;
	
	//IN Range of errors
	public final static int OTHER_ERRORS_IN = 4000;
	public final static int WRONG_DAVALUE_FORMAT = 4010;
	public final static int ERROR_CHECKING_SUB_BALANCE = 4020;
	public final static int ERROR_NO_AMT_TO_DEDUCT = 4030;
	public final static int OTHER_ERRORS_IN_DEDUCTION = 4000;
	
	//Local Database Errors
	public final static int SUBSCRIBER_NOT_IN_DB = 5000;
	
	//Promo Errors
	public final static int SUBSCRIBER_NOT_ALLOWED = 7000;

	public static final int SUBSCRIBER_DISABLED_AUTORENEWAL = 1025;

	
	//Homisco Errors
	//Homisco specific errors
	public final static int INSUFFICENT_POSTPAID_BALANCE = 6050;
	public final static int POSTPAID_NO_BARRING_ERROR = 6051;
	public final static int POSTPAID_DISCONNECTED_ERROR = 6052;
	public final static int POSTPAID_NOT_POSTPAID_ERROR = 6054;
	public final static int POSTPAID_NOT_ACTIVE_ERROR = 6055;
	public final static int OTHER_ERRORS_SETTING_HOMISCO = 6000;
	public final static int INVALID_MSISDN_SETTING_HOMISCO = 6001;
	public final static int MINUTE_BUNDLE_SETTING_HOMISCO = 6002;
	public final static int PREPAID_SUB_SETTING_HOMISCO = 6003;
	
	public final static int PREVIOUS_REQUEST_IN_PROCESS = 8000;
	public static final int INSUFFICENT_BALANCE_CHECK = 1090;
	public static final int INACTIVE_SUBSCRIBER = 7001;
	
	public final static int EULOGY_DUPLICATE_MSISDN = 9000;
	public final static int EULOGY_WRONG_COMPONENT_ID = 9001;
	public final static int EULOGY_WRONG_COMPONENT_ID_OR_MEMBER_ID = 9002;
	public final static int EULOGY_WRONG_FLAG = 9003;
	public final static int EULOGY_NULL_COMPONENT_ID = 9004;
	public final static int EULOGY_MSISDN_DOES_NOT_EXIST = 9005;
	public final static int EULOGY_MSISDN_SUCCESSFULLY_DETACHED = 9006;
	public static final int SHORTCODE_NOT_FOUND = 7002;
	public static final int SUBSCRIBER_IN_SESSION = 7010;
	public static final int FUTURE_RENEWAL_ENABLED = 7020;
	public static final int SUBSCRIBER_STATE_OUT_OF_SYNC = 7030;
	public static final int SWAPACTIVE =  77702;
	
}
