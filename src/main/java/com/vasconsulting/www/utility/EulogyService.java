/**
 * This is the data access service layer of the application. This class is responsible for all database access functions.
 * It currently connects to the local database system and also connects to the TABS database that is used to load subscriber credit limit.
 * @author nnamdi Jibunoh
 * @date 8-8-2011
 */
package com.vasconsulting.www.utility;


import java.util.List;
import java.util.ArrayList;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.hibernate.HibernateException;
import org.hibernate.cfg.Configuration;


public class EulogyService
{
	Configuration config1 = new Configuration().configure("config/eulogy.cfg.xml");
	private ThreadLocal<Session> session = new ThreadLocal<Session>();

	private Session currentSession() throws HibernateException
	{
		Session sess = (Session)session.get();

		if ((sess == null) || (!sess.isOpen()))
		{
			sess = this.config1.buildSessionFactory().openSession();

			session.set(sess);
		}
		return sess;
	}

	public List<String> checkPostSubscriberEulogy(final String msisdn){
		final List<String> subStatus = new ArrayList<String>();
		try {
			currentSession().doWork(new Work() {
				public void execute(Connection conn) throws SQLException {
					CallableStatement st = conn.prepareCall("begin roamflex.GETMSISDNSTATUS(?,?,?); end;");
					st.setString(1, msisdn);
					st.registerOutParameter(2, Types.VARCHAR);
					st.registerOutParameter(3, Types.VARCHAR);                                      
					st.execute();
					subStatus.add(st.getString(2));
					subStatus.add(st.getString(3));
				}                               
			});
			System.out.println(subStatus);
			return subStatus;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}finally {
			currentSession().close();
		}            
	}

	public List<String> updateEulogySubDetails(final String msisdn, final String flag, 
			final String memberId, final String componentId){
		final List<String> responseCode = new ArrayList<String>();
		try {
			currentSession().doWork(new Work() {
				public void execute(Connection conn) throws SQLException {
					CallableStatement st = conn.prepareCall("begin roamflex.P_Offline_provisioning(?,?,?,?,?); end;");
					st.setString(1, msisdn);
					st.setString(2, flag);
					st.setString(3, memberId);
					st.setString(4, componentId);
					st.registerOutParameter(5, Types.VARCHAR);                                     
					st.execute();
					responseCode.add(st.getString(5));
				}                               
			});
			System.out.println(responseCode);
			return responseCode;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}finally {
			currentSession().close();
		}               
	}

	public static void main(String[] args){
		EulogyService eulogy = new EulogyService();
		System.out.println("Supply msisdn, flag, member_id, component_id as options");
		System.out.println("Response from eulogy is: "+eulogy.updateEulogySubDetails(args[0], args[1], args[2], args[3]));
		System.out.println("Subscriber eulogy status is: "+eulogy.checkPostSubscriberEulogy(args[0]));
	}
	
	private final String msisdn (String msisdn){
		return msisdn.substring(2,msisdn.length());	
	}

}
