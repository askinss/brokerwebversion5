package com.vasconsulting.www.utility;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5
{
  public static String getDigest(String password)
  {
    byte[] defaultBytes = password.trim().getBytes();
    MessageDigest algorithm;
    StringBuffer hexString = new StringBuffer();
	try {
		algorithm = MessageDigest.getInstance("MD5");
	
    algorithm.reset();
    algorithm.update(defaultBytes);
    byte[] messageDigest = algorithm.digest();
    
    for (int i = 0; i < messageDigest.length; ++i)
      hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
	
	} catch (NoSuchAlgorithmException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    return hexString.toString();
  } 
  
  public static String getHardDigest(String password)
  {
	MessageDigest md = null;
	try {
		md = MessageDigest.getInstance("MD5");
	} catch (NoSuchAlgorithmException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
      md.update(password.getBytes());

      byte byteData[] = md.digest();

      //convert the byte to hex format method 1
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < byteData.length; i++) {
       sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
      }

      System.out.println("Digest(in hex format):: " + sb.toString());

      //convert the byte to hex format method 2
      StringBuffer hexString = new StringBuffer();
  	for (int i=0;i<byteData.length;i++) {
  		String hex=Integer.toHexString(0xff & byteData[i]);
 	     	if(hex.length()==1) hexString.append('0');
 	     	hexString.append(hex);
  	}
  	return hexString.toString();
  }
}