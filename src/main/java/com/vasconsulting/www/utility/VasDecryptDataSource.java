package com.vasconsulting.www.utility;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.apache.commons.dbcp.BasicDataSource;

import com.vasconsulting.www.interfaces.EncrypterInterface;

public class VasDecryptDataSource extends BasicDataSource implements EncrypterInterface {
	private String username;
	private String password;
	private EncryptAndDecrypt ecnrypter = new EncryptAndDecrypt();
	
	public String getUsername() {
		return decrypt(username);
	}

	private String decrypt(String value) {
		try {
			return ecnrypter.decrypt(value);
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void setUsername(String username) {
		this.username = decrypt(username);
		super.username = this.username;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public void setPassword(String password) {
		this.password = decrypt(password);
		super.password = this.password;
	}
	
	

}
