package com.vasconsulting.www.utility;

public class StatusCodesUI
{	
	public final static int SUCCESS = 0;
	public final static String SUCCESS_MESSAGE = "Requested Operation was completed successfully";
	
	//User authentication errors are in the 100 range
	public final static int USER_ALREADY_EXIST = 100;
	public final static String USER_ALREADY_EXIST_MESSAGE = "A user with the same email already exists";
	public final static int USER_EMAIL_MSISDN_CANNOT_BE_EMPTY = 101;
	public final static String USER_EMAIL_MSISDN_CANNOT_BE_EMPTY_MESSAGE = "User needs an email address to activate this service";
	public final static int NO_USER_FOUND = 102;
	public final static String NO_USER_FOUND_MESSAGE = "User details was not found, Please check the email address.";
	public final static int USER_ALREADY_AUTHENTICATED = 103;
	public final static String USER_ALREADY_AUTHENTICATED_MESSAGE = "User is already authenticated. No need to attempt login.";
	
	//REST Service errors
	public final static int SUBSCRIBER_IMSI_CANNOT_BE_EMPTY = 200;
	public final static String SUBSCRIBER_IMSI_CANNOT_BE_EMPTY_MESSAGE = "Subscriber MSISDN cannot be empty for this request";
	public final static int SUBSCRIBER_MSISDN_CANNOT_BE_EMPTY = 201;
	public final static String SUBSCRIBER_MSISDN_CANNOT_BE_EMPTY_MESSAGE = "Subscriber MSISDN cannot be empty for this request";
	public final static int SUBSCRIBER_NOT_FOUND = 202;
	public final static int SUBSCRIBER_NOT_ACTIVE = 203;
	public final static String SUBSCRIBER_NOT_FOUND_MESSAGE = "Subscriber Details not found. Please check the supplied value and try again";
	
	//catastophic errors that the application cannot return from starts from 1000
	public final static int OTHER_ERRORS = 1000;
	public final static String OTHER_ERRORS_MESSAGE = "GENERAL ERROR";
	
	// other error messages/status
	public final static int USER_RESET_PASSWORD=120;
	public final static String USER_RESET_PASSWORD_MESSAGE="The user needs to reset his/her password";
}