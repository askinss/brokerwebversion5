package com.vasconsulting.www.utility;

import java.io.IOException;
import java.rmi.RemoteException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.huawei.mds.access.webservice.server.bean.MDSInterfaceProxy;
import com.huawei.mds.access.webservice.server.bean.ParaBean;
import com.huawei.mds.access.webservice.server.bean.ParaListBean;
import com.huawei.mds.access.webservice.server.bean.RequestBean;
import com.huawei.mds.access.webservice.server.bean.ResultBean;
import com.huawei.mds.access.webservice.server.bean.SendRequestMsg;
import com.huawei.mds.access.webservice.server.bean.SendRequestMsgRequestMessage;
import com.huawei.mds.access.webservice.server.bean.SendRequestResult;
import com.huawei.mds.access.webservice.server.bean.SendRequestResultResultMessage;
import com.huawei.www.bme.cbsinterface.cbs.accountmgr.AdjustAccountRequest;
import com.huawei.www.bme.cbsinterface.cbs.accountmgr.CBSInterfaceAccountMgrProxy;
import com.huawei.www.bme.cbsinterface.cbs.accountmgr.ModifyAcctFeeType;
import com.huawei.www.bme.cbsinterface.cbs.accountmgrmsg.AdjustAccountRequestMsg;
import com.huawei.www.bme.cbsinterface.cbs.accountmgrmsg.AdjustAccountResultMsg;
import com.huawei.www.bme.cbsinterface.cbs.businessmgr.CBSInterfaceBusinessMgrProxy;
import com.huawei.www.bme.cbsinterface.cbs.businessmgr.SubscribeAppendantProductRequest;
import com.huawei.www.bme.cbsinterface.cbs.businessmgr.SubscribeAppendantProductRequestProduct;
import com.huawei.www.bme.cbsinterface.cbs.businessmgr.ValidMode;
import com.huawei.www.bme.cbsinterface.cbs.businessmgrmsg.SubscribeAppendantProductRequestMsg;
import com.huawei.www.bme.cbsinterface.cbs.businessmgrmsg.SubscribeAppendantProductResultMsg;
import com.huawei.www.bme.cbsinterface.common.RequestHeader;
import com.huawei.www.bme.cbsinterface.common.RequestHeaderRequestType;
import com.huawei.www.bme.cbsinterface.common.SessionEntityType;
import com.vasconsulting.www.ocs.BasicInfo;
import com.vasconsulting.www.ocs.GPRSData;
import com.vasconsulting.www.ocs.ODBData;
import com.vasconsulting.www.ocs.Subscriber;
import com.vasconsulting.www.ocs.SubscriberData;
import com.vasconsulting.www.ocs.SubscriberIndentity;
import com.vasconsulting.www.ocs.Token;

import ug.co.waridtel.BBBrokerServiceSoapProxy;
import ug.co.waridtel.ObjectInfo;

public class OCSSubscriberUtility {
	private boolean isSubscriberAirtel = true;
	private boolean isSubscriberPrepaid = true;
	private Logger logger = Logger.getLogger(OCSSubscriberUtility.class); 
	private LoadAllProperties properties = new LoadAllProperties();
	private EncryptAndDecrypt ecnrypter = new EncryptAndDecrypt();
	
	private String decrypt(String value) {
		try {
			return ecnrypter.decrypt(value);
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public boolean isSubscriberFromAirtel(String msisdn)
	{
		if (msisdn.startsWith("+25675") || msisdn.startsWith("25675") || msisdn.startsWith("075"))
		{
			return isSubscriberAirtel;
		}
		else
		{
			isSubscriberAirtel = false;
			return isSubscriberAirtel;
		}
	}
	
	public ObjectInfo getSubscriberInfoOCS(String msisdn)
	{
		BBBrokerServiceSoapProxy subscriberInfo = new BBBrokerServiceSoapProxy();
		
		try {
			return subscriberInfo.getSubscriberProfile(msisdn);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public Boolean isSubscriberPrepaid(String msisdn)
	{
		BBBrokerServiceSoapProxy subscriberInfo = new BBBrokerServiceSoapProxy();
		
		try {
			ObjectInfo subscriberDetail = subscriberInfo.getSubscriberProfile(msisdn);
			
			if ((subscriberDetail.getIsprecharged() == 1) || (subscriberDetail.getIsprecharged() == 100 && 
					subscriberDetail.getCurrentCredit() == 0))
			{
				isSubscriberPrepaid = true;
				return isSubscriberPrepaid;
			}
			isSubscriberPrepaid = false;
			return isSubscriberPrepaid;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public SubscribeAppendantProductResultMsg deductServiceAirtimeByProductID(String msisdn, String productID)
	{
		CBSInterfaceBusinessMgrProxy subscriberAccount = new CBSInterfaceBusinessMgrProxy();
		
		SubscribeAppendantProductRequestMsg subscriberAppendant = new SubscribeAppendantProductRequestMsg();
		RequestHeader requestHeader = new RequestHeader();
		requestHeader.setCommandId("SubscribeAppendantProduct");
		requestHeader.setVersion("1");
		requestHeader.setTransactionId("1");
		requestHeader.setSequenceId("1");
		requestHeader.setRequestType(RequestHeaderRequestType.Event);
		requestHeader.setSerialNo(UUID.randomUUID().toString());
		
		SessionEntityType sessionEntity = new SessionEntityType();
		decrypt(properties.getProperty("OCSUserName"));
		decrypt(properties.getProperty("OCSPassword"));
		sessionEntity.setRemoteAddress("10.4.28.13");
		
		requestHeader.setSessionEntity(sessionEntity);
		
		SubscribeAppendantProductRequest subscribeAppendantProductRequest = new SubscribeAppendantProductRequest();
		subscribeAppendantProductRequest.setSubscriberNo(stripLeadingMsisdnPlus(msisdn));
		
		SubscribeAppendantProductRequestProduct products [] = new SubscribeAppendantProductRequestProduct[1];
		SubscribeAppendantProductRequestProduct product1 = new SubscribeAppendantProductRequestProduct();
		product1.setId("3002073");
		product1.setValidMode(ValidMode.fromString("4050000"));
		
		products[0] = product1;
		
		subscribeAppendantProductRequest.setProduct(products);		
		
		subscriberAppendant.setRequestHeader(requestHeader);
		subscriberAppendant.setSubscribeAppendantProductRequest(subscribeAppendantProductRequest);
		
		try {
			SubscribeAppendantProductResultMsg result = subscriberAccount.subscribeAppendantProduct(subscriberAppendant);
			logger.info("The value of attaching a ProductID to subscriber ="+msisdn+" is ="+result);
			return result;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public AdjustAccountResultMsg deductServiceAirtime(String msisdn, long amountToDeduct, String externalData)
	{
		CBSInterfaceAccountMgrProxy subAcct = new CBSInterfaceAccountMgrProxy();
		logger.info("MSISDN = "+msisdn+", amountToDeduct = "+amountToDeduct+", externalData = "+externalData);
		try {			
			//Build Header details
			AdjustAccountRequestMsg request = new AdjustAccountRequestMsg();
			RequestHeader headers = new RequestHeader();
			headers.setCommandId("AdjustAccount");
			headers.setVersion("1");
			headers.setTransactionId("1");
			headers.setSequenceId("1");
			headers.setOperatorID(externalData);
			
			headers.setRequestType(RequestHeaderRequestType.Event);
			
			SessionEntityType sessionEntity = new SessionEntityType();
			sessionEntity.setName("sts");//properties
			sessionEntity.setPassword("TDbbk01");//properties
			sessionEntity.setRemoteAddress("10.4.28.13");//properties
			
			headers.setSessionEntity(sessionEntity);
			headers.setSerialNo(generateSerial());//UUID.randomUUID().toString()
			
			//Request details
			AdjustAccountRequest adjustAccountRequest =  new AdjustAccountRequest();
			logger.info("MSISDN = "+stripLeadingMsisdnPlus(msisdn));
			adjustAccountRequest.setSubscriberNo(stripLeadingMsisdnPlus(msisdn));//"704000013"
			adjustAccountRequest.setOperateType(2);
			
			ModifyAcctFeeType[] modifyAcctFeeList = new ModifyAcctFeeType[1];
			ModifyAcctFeeType fee1 = new ModifyAcctFeeType();
			fee1.setAccountType("2000");
			fee1.setCurrAcctChgAmt(amountToDeduct);
			
			modifyAcctFeeList[0] = fee1;
			adjustAccountRequest.setModifyAcctFeeList(modifyAcctFeeList);
			
			request.setRequestHeader(headers);
			request.setAdjustAccountRequest(adjustAccountRequest);
			
			AdjustAccountResultMsg response = subAcct.adjustAccount(request);
			
			logger.info("Response from Deduction is = "+response);
			logger.info("Response from Deduction is = "+response.getResultHeader().getResultCode());
			logger.info("Response from Deduction is = "+response.getResultHeader().getResultDesc());
			
			return response;
			
		} 
		catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return null;
		}
	}
	
	public Subscriber getSubscriberHLRDetails(String msisdn) throws RemoteException
	{
		 Subscriber subscriber = new Subscriber();
		 MDSInterfaceProxy mdsInterfacePort =  new MDSInterfaceProxy();

	        // this the object encapsulates the whole SOAP Message
	        SendRequestMsg sendRequestMsg = new SendRequestMsg();
	        SendRequestMsgRequestMessage sendRequestMsgRequestMessage = new SendRequestMsgRequestMessage();

	        com.huawei.mds.access.webservice.server.bean.RequestHeader messageHeader = new 
	        		com.huawei.mds.access.webservice.server.bean.RequestHeader();
	        messageHeader.setSysUser("Ibm_prov");
	        messageHeader.setSysPassword("Ibm_prov12$");
	        messageHeader.setDebugFlag("2");

	        //set the message header
	        sendRequestMsgRequestMessage.setMessageHeader(messageHeader); 

	        RequestBean messageBody = new RequestBean();
	        messageBody.setSerial(generateSerial());
	        messageBody.setBizCode("GETSUB");

	       /* ParaListBean paraListBean = new ParaListBean();
	        List<ParaBean> paraList = paraListBean.getPara();*/
	        ParaBean []paraBeanList = new ParaBean[1];

	        ParaBean paraBean;
	        paraBean = new ParaBean();
	        paraBean.setName("MSISDN");
	        paraBean.setValue(stripLeadingMsisdnPlus(msisdn));
	        
	        paraBeanList[0] = paraBean;

	        messageBody.setParaList(paraBeanList);

	        RequestBean[] requestBeans = new RequestBean[1];
	        requestBeans[0] = messageBody;//set the message Body

	        //set message body
	        sendRequestMsgRequestMessage.setMessageBody(requestBeans);

	        sendRequestMsg.setRequestMessage(sendRequestMsgRequestMessage);

	        //response
	        SendRequestResult response = mdsInterfacePort.sendSyncReq(sendRequestMsg);
	        
	        System.out.println("the value of response is = "+response);
	        System.out.println("The desc = "+response.getResultMessage().getMessageBody()[0].getRetDesc());

	        SendRequestResultResultMessage resultMessage = response.getResultMessage();
	        ResultBean[] responseBody = resultMessage.getMessageBody();

	        for (ResultBean resultBean : responseBody) {
				
			    subscriber.setID(resultBean.getID());
	            subscriber.setSerial(resultBean.getSerial());
	            subscriber.setRetCode(resultBean.getRetCode());
	            subscriber.setRetDesc(resultBean.getRetDesc());

	           // ParaListBean retParaList = resultBean.getRetParaList();
	            ParaBean[] para = resultBean.getRetParaList();

	            //Logger.getLogger(ProvisionMethods.class.getName()).log(Level.INFO, "GET SUB FOR {0}", msisdn);
	            //List<ParaBean> para = retParaList.getPara();

	            if (para.length > 1) {
	                //HW HLR Subscriber
	                //Iterator<ParaBean> iterator1 = para.iterator();
	                for (ParaBean pBean : para) {
						
					    if (pBean.getName().equals("GPRS Data")) {
	                        subscriber = parseGPRSData(subscriber, pBean.getValue());
	                    }
	                    if (pBean.getName().equals("ODB Data")) {
	                        subscriber = parseODBData(subscriber, pBean.getValue());
	                    }
	                    if (pBean.getName().equalsIgnoreCase("O-CSI")) {
	                        subscriber = parsePrepaidData(subscriber, pBean.getValue());
	                    }
	                    if (pBean.getName().startsWith("BASIC")) {
	                        subscriber = parserBasicInfo(subscriber, pBean.getValue());
	                    }
	                    if (pBean.getName().startsWith("LOCK")) {
	                        subscriber = parseLockData(subscriber, pBean.getValue());
	                    }
	                    //this is an E HLR Subscriber
	                    if (pBean.getName().equals("RESULT")) {
	                        ArrayList<Token> tokenBox = tokenizeHLRResultString(pBean.getValue());
	                        subscriber = createSubscriber(tokenBox, subscriber);
	                    }
	                }
	            } 
	            else if (para.length == 1) 
	            {
	                //E HLR Subscriber in case the response return the OLD  response
	                if (para.length != 0) {
	                	for (ParaBean paraBean1 : para) {
	                        //ParaBean paraBean1 = iterator1.next();
	                        //System.out.println(paraBean1.getValue());
	                        ArrayList<Token> tokenBox = tokenizeHLRResultString(paraBean1.getValue());
	                        subscriber = createSubscriber(tokenBox, subscriber);
	                    }
	                }

	            }
	        }

	        return subscriber;
	    
	}
	
	public String[] getAirtelSubscriberIMSIAndType(String msisdn)
	{
		String[] response = new String[2];
		
		response[0] = null;
		
		try {
			logger.info("Getting the IMSI for Airtel number ="+stripLeadingMsisdnPlus(msisdn));
			
			
			Subscriber resp = getSubscriberHLRDetails(stripLeadingMsisdnPlus(msisdn));
			System.out.println("Response from Huawei HLR interface = "+resp.getRetCode());
			if (resp == null || !resp.getRetCode().equals("U000"))
			//if (resp == null  || null == resp.getSubscriberIndentity().getImsi())
			{
				response = null;
				logger.info("Call to get IMSI details for ["+msisdn+"] failed returning a null set");
				return response;
			}
			logger.info("Value returned from OCS = "+resp);
			//logger.info(resp.getBasicInfo().getImsi());
			
			try
			{
				if (resp.getBasicInfo().getImsi() != null)
				{
					response[0] = resp.getBasicInfo().getImsi();
					logger.info("response[0] = resp.getBasicInfo().getImsi()");
				}
				else 
				{
					response[0] = resp.getSubscriberIndentity().getImsi();
					logger.info("response[0] = resp.getSubscriberIndentity().getImsi();");
				}				
			}
			catch(NullPointerException ex)
			{				
				response[0] = resp.getSubscriberIndentity().getImsi();
				logger.info("response[0] = resp.getSubscriberIndentity().getImsi();");
				logger.info("Catch block");
			}
			
			if (response[0] == null)
			{
				throw new NullPointerException();
			}
			
			response[1] = "POST";//Give it a default value of Post
			
			String [] temp = properties.getProperty("prepaidimsiranges").split(",");
			
			for (String tempValue : temp)
			{
				if (response[0].startsWith(tempValue))
				{
					response[1] = "PREP";
					logger.info("MSISDN ["+msisdn+"] is a Prepaid number, exiting the loop");
					break;
				}
			}
						
			logger.info("Returning the details "+response[0]+" | "+response[1]+" for MSISDN ["+msisdn+"]");
			return response;
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		}
		catch(NullPointerException ex)
		{
			ex.printStackTrace();
			return null;
		}
		
		
	}
	
	
	
	
	
	
	
	
	private String generateSerial() {
        //randomise the serial number for each request
        char alphabet[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
            'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

        String serialGenerated;
        Random random = new Random();
        serialGenerated = alphabet[random.nextInt(alphabet.length)] + "" + random.nextInt();

        return serialGenerated;
    }
	
	private ArrayList<Token> tokenizeHLRResultString(String value) {
        StringTokenizer stringTokenizer = new StringTokenizer(value);//read the value into a StringTokenizer
        ArrayList<Token> tokenBox = new ArrayList<Token>(); //get a collection that will hold the Token Objects
        int idx = 1;

        while (stringTokenizer.hasMoreElements()) {
            Token token = new Token();//create a new Token Object
            token.setValue(stringTokenizer.nextToken());
            token.setPos(idx);
            tokenBox.add(token);
            idx++;
            //System.out.println(token);
        }
        return tokenBox;
    }
	
	private Subscriber createSubscriber(ArrayList<Token> tokenBox, Subscriber subscriber) {

        SubscriberIndentity subscriberIndentity = readSubscriberIdentity(tokenBox);

        SubscriberData subscriberData = readSubscriberData(tokenBox);

        subscriber.setSubscriberData(subscriberData);
        subscriber.setSubscriberIndentity(subscriberIndentity);
        return subscriber;
    }
	
	private SubscriberIndentity readSubscriberIdentity(ArrayList<Token> tokenBox) {
        SubscriberIndentity subscriberIndentity = new SubscriberIndentity();
        Iterator<Token> boxIterator = tokenBox.iterator();

        while (boxIterator.hasNext()) {
            Token token = boxIterator.next();

            //set up the Subscriber Indentity
            int pos = token.getPos();
            String value = token.getValue();

            //set MSISDN
            if (pos == 7) {
                subscriberIndentity.setMsidn(value);
            } else if (pos == 8) {
                //set imsi
                subscriberIndentity.setImsi(value);
            } else if (pos == 9) {
                //set connection state
                subscriberIndentity.setConnectionState(value);
            } else if (pos == 10) {
                //set authd
                subscriberIndentity.setAuthd(value);
            } else if (value.equals("0") || value.equals("1")) {
                //set nam state
                subscriberIndentity.setNam(value);
            } else if (pos == 14 && !value.equals("SUBSCRIBER")) {
                //set imeisv
                subscriberIndentity.setImeisv(value);
            }
        }
        return subscriberIndentity;
    }
	
	private SubscriberData readSubscriberData(ArrayList<Token> tokenBox) {

        SubscriberData subscriberData = new SubscriberData();

        ArrayList<String> data = new ArrayList<String>();//collection to hold this data

        Iterator<Token> boxIterator = tokenBox.iterator();
        Iterator<Token> boxIterator2 = tokenBox.iterator();

        int startIdx = 0, endIdx = 0;

        while (boxIterator.hasNext()) {
            Token token = boxIterator.next();
            String value = token.getValue();
            int idx = token.getPos();

            if (value.equals("SUD")) {
                startIdx = idx;
            }

            if (value.equals("AMSISDN")) {
                endIdx = idx;
            }
        }//end while 

        //pick the values between those Ranges
        while (boxIterator2.hasNext()) {
            Token token = boxIterator2.next();
            String value = token.getValue();
            int pos = token.getPos();

            if (value.equals("AMSISDN")) {
                break;
            }

            if (pos > startIdx) {
                data.add(value);
            }
        }//end of while2
        subscriberData.setSubscriberDataList(data);
        return subscriberData;
    }
	
	
	private Subscriber parseLockData(Subscriber subscriber, String data) {
        data = data.replaceAll("\\=", " ");

        StringTokenizer stTokenizer = new StringTokenizer(data.trim());
        ArrayList<String> infoList = new ArrayList<String>();
        while (stTokenizer.hasMoreTokens()) {
            infoList.add(stTokenizer.nextToken());

        }
        Object[] infoArray = infoList.toArray();

        for (int i = 0; i < infoArray.length;) {
            try {
                String info = (String) infoArray[i];
                String value = (String) infoArray[++i];

                if (info.equals("IC")) {
                    subscriber.setIC(value);
                }

                if (info.equals("OC")) {
                    subscriber.setOC(value);
                }

                if (info.equals("GPRSLOCK")) {
                    subscriber.setGPRSLOCK(value);
                }

                if (info.equals("EPSLOCK")) {
                    subscriber.setEPSLOCK(value);
                }

                if (info.equals("NON3GPPLOCK")) {
                    subscriber.setNON3GPPLOCK(value);
                }
            } catch (ArrayIndexOutOfBoundsException ex) {
                //ignore this
            }
        }
        return subscriber;
    }

    private Subscriber parsePrepaidData(Subscriber subscriber, String data) {
        data = data.replaceAll("\\=", " ");

        StringTokenizer stTokenizer = new StringTokenizer(data.trim());
        ArrayList<String> infoList = new ArrayList<String>();
        while (stTokenizer.hasMoreTokens()) {
            infoList.add(stTokenizer.nextToken());

        }
        Object[] infoArray = infoList.toArray();

        for (int i = 0; i < infoArray.length;) {
            try {
                String info = (String) infoArray[i];
                String value = (String) infoArray[++i];

                if (info.equals("TPLID")) {
                    subscriber.setTplid(value);
                }

                if (info.equals("TPLNAME")) {
                    subscriber.setTplName(value);
                }
                if (info.equals("STATE")) {
                    subscriber.setState(value);
                }
            } catch (ArrayIndexOutOfBoundsException e) {
            }
        }
        return subscriber;
    }

    private Subscriber parserBasicInfo(Subscriber subscriber, String data) {
        BasicInfo basicInfo = new BasicInfo();
        ArrayList<String> infoList = new ArrayList<String>();

        data = data.replaceAll("\\=", " ");
        StringTokenizer stTokenizer = new StringTokenizer(data.trim());

        while (stTokenizer.hasMoreTokens()) {
            infoList.add(stTokenizer.nextToken());
        }

        Object[] infoArray = infoList.toArray();

        for (int i = 0; i < infoArray.length;) {
            try {
                String info = (String) infoArray[i];
                String value = (String) infoArray[++i];

                if (info.equals("HLRSN")) {
                    basicInfo.setHLRSN(value);
                }

                if (info.equals("IMSI")) {
                    basicInfo.setImsi(value);
                }

                if (info.equals("ISDN")) {
                    basicInfo.setMsisdn(value);
                }

                if (info.equals("ICS-Indicator")) {
                    basicInfo.setIcsIndicator(value);
                }
                if (info.equals("ANCHOR-Indicator")) {
                    basicInfo.setAnchorIndicator(value);
                }
                if (info.equals("CardType")) {
                    basicInfo.setCardType(value);
                }

                if (info.equals("NAM")) {
                    basicInfo.setNam(value);
                }

                if (info.equals("CATEGORY")) {
                    basicInfo.setCategory(value);
                }

                if (info.equals("SUB_AGE")) {
                    basicInfo.setSubAge(value);
                }

                if (info.equals("USERCATEGORY")) {
                    basicInfo.setUserCategory(value);
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                //We might need to ignore this 
            }
        }
        subscriber.setBasicInfo(basicInfo);
        return subscriber;
    }

    private Subscriber parseODBData(Subscriber subscriber, String data) {
        ODBData odbData = new ODBData();
        HashMap<String, String> odbInfo = new HashMap<String, String>();
        ArrayList<String> odbList = new ArrayList<String>();

        data = data.replaceAll("\\=", " ");
        StringTokenizer stTokenizer = new StringTokenizer(data.trim());

        while (stTokenizer.hasMoreTokens()) {
            odbList.add(stTokenizer.nextToken());
        }

        Object[] odbArray = odbList.toArray();

        for (int i = 0; i < odbArray.length; i++) {
            try {
                String key = (String) odbArray[i];
                String value = (String) odbArray[++i];

                odbInfo.put(key, value);
            } catch (ArrayIndexOutOfBoundsException ex) {
                //ignnore this Exception
            }
        }

        //make a check if the array List and return NO GPRS Data
        if (odbInfo.isEmpty()) {
            odbInfo.put("ODB DATA", "NO VALUS FOUND");
        }
        odbData.setOdbInfo(odbInfo);
        subscriber.setOdbdata(odbData);
        return subscriber;
    }

    private Subscriber parseGPRSData(Subscriber subscriber, String data) {
        GPRSData gprsData = new GPRSData();
        ArrayList<String> gprsValues = new ArrayList<String>();
        ArrayList<String> temp = new ArrayList<String>();

        data = data.replaceAll("\\=", " ");
        StringTokenizer stTokenizer = new StringTokenizer(data.trim());

        while (stTokenizer.hasMoreTokens()) {
            String nextToken = stTokenizer.nextToken();
            temp.add(nextToken);
        }

        Object[] holder = temp.toArray();

        for (int i = 0; i < holder.length;) {
            try {
                String key = (String) holder[i];
                String value = (String) holder[++i];

                //populate the GPRS DATA
                if (key.equals("APN")) {
                    gprsValues.add(value);
                }
            } catch (ArrayIndexOutOfBoundsException e) {
            }
        }

        //make a check if the array List and return NO GPRS Data
        if (gprsValues.isEmpty()) {
            gprsValues.add("NO GPRS DATA FOUND");
        }
        gprsData.setGprsValues(gprsValues);

        subscriber.setGprsData(gprsData);

        return subscriber;
    }
	
	private String stripLeadingMsisdnPlus(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("0")){
			return Msisdn.substring(1, Msisdn.length());
		}
		else if(Msisdn.startsWith("+256")){
			return Msisdn.substring(4, Msisdn.length());
		}
		else if(Msisdn.startsWith("256")){
			return Msisdn.substring(3, Msisdn.length());
		}
		else return Msisdn;
	}
	
}
