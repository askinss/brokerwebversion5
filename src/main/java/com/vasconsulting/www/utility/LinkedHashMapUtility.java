/**
 * 
 */
package com.vasconsulting.www.utility;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import com.vasconsulting.www.domain.RolesDetail;
import com.vasconsulting.www.domain.UserLoginDetails;
import com.vasconsulting.www.domain.Users;

/**
 * @author Ayotunde
 *
 */
public class LinkedHashMapUtility 
{
	/*Accepts a LinkedHashMap and converts to a UsersLoginDetails object*/
	@SuppressWarnings("rawtypes")
	public static UserLoginDetails ConvertToUserLoginDetail(LinkedHashMap map)
	{
		System.out.println("converting map to userlogindetail..");
		UserLoginDetails userLoginDetails = new UserLoginDetails();
		
		String id = (String)map.get("id");
		String userId = (String)map.get("userID");
		String sessionID = (String)map.get("sessionID");
		String date_created = new Long((Long)map.get("date_created")).toString();
		String email = (String)map.get("email");
		String userType = (String)map.get("usertype");
		String roleid = (String)map.get("roleid");
		
		if(date_created!=null)
		{
			Calendar calendar = Calendar.getInstance( );
			userLoginDetails.setDate_created(calendar);
		}
		userLoginDetails.setId(id);
		userLoginDetails.setUserID(userId);
		userLoginDetails.setSessionID(sessionID);
		userLoginDetails.setEmail(email);
		userLoginDetails.setUsertype(userType);
		userLoginDetails.setRoleid(roleid);
		// we get an arraylist from this then the hashmap for the usertype role collection
		/*ArrayList roleMap = (ArrayList)map.get("userType");
		// we convert the hashmap to roledetail object
		RolesDetail rolesDetail = LinkedHashMapUtility.ConvertToRoleDetails((LinkedHashMap)roleMap.get(0));
		// create a Set and add this rolesdetail object to the collection
		if(rolesDetail!=null)
		{
			Set<RolesDetail> roleSet=new HashSet<RolesDetail>(); 
			roleSet.add(rolesDetail);
			
			userLoginDetails.setUserType(roleSet);
		}*/
		return userLoginDetails;
	}
	
	
	public static Users convertToUserDetails(LinkedHashMap<String, String> dataToConvert)
	{
		System.out.println("Converting map to User details");
		
		Users user = new Users();
		
		user.setId(dataToConvert.get("id"));
		user.setEmail(dataToConvert.get("email"));
		
		return user;
	}
	
	
	/*Accepts a LinkedHashMap and converts to a RolesDetail object*/
	public static RolesDetail ConvertToRoleDetails(LinkedHashMap map)
	{
		System.out.println("converting map to roledetails..");
		RolesDetail rolesDetail = new RolesDetail();
		rolesDetail.setId((String)map.get("id"));
		rolesDetail.setRolename((String)map.get("rolename"));
		rolesDetail.setDescription((String)map.get("description"));
		System.out.println("roledetails "+rolesDetail.getRolename()+ " retrieved...");
		return rolesDetail;
	}
}
