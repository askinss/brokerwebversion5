package com.vasconsulting.www.utility;


import java.math.BigDecimal;
import java.util.Collection;
import java.util.Iterator;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.HibernateException;
import org.hibernate.cfg.Configuration;


public class HomiscoService
{
	Configuration config1 = new Configuration().configure("config/homisco.cfg.xml");
	private ThreadLocal<Session> session = new ThreadLocal<Session>();

	private Session currentSession() throws HibernateException
	{
		Session sess = (Session)session.get();

		if ((sess == null) || (!sess.isOpen()))
		{
			sess = this.config1.buildSessionFactory().openSession();

			session.set(sess);
		}
		return sess;
	}

	public int createPostSubscriberHomisco(String msisdn, String productType, String cost, String userID)
	{
		try{
			System.out.println("Msisdn="+msisdn+", productType="+productType+", Cost="+cost+", UserID="+userID);
			SQLQuery sqlQuery = currentSession().createSQLQuery("DECLARE @r1 SMALLINT EXEC " +
					"UpdateMobileProduct :msisdn, :productType, :cost, :userID, @r1 OUTPUT,1 SELECT @r1");
			sqlQuery.setString("msisdn", msisdn);
			sqlQuery.setString("productType", productType);
			sqlQuery.setString("cost", cost);
			sqlQuery.setString("userID", userID);

			@SuppressWarnings("unchecked")
			Collection<Short> response = sqlQuery.list();

			System.out.println(response);

			if (response.iterator().hasNext())
				return new Short(response.iterator().next()).intValue();
			else return -100;
		}catch(Exception e){
			e.printStackTrace();
			return -100;
		}finally {
			currentSession().close();
		}
	}

	public HomiscoResults getPostpaidSubscriberCreditLimit(String msisdn)
	{
		System.out.println("MSISDN = "+msisdn+", and the query to run is EXEC EX_SAPCCheckMobileStatus :msisdn");
		SQLQuery sqlQuery = currentSession().createSQLQuery("EXEC EX_SAPCCheckMobileStatus :msisdn");
		sqlQuery.setString("msisdn", msisdn);

		@SuppressWarnings("unchecked")
		Collection<Object> response = sqlQuery.list();//sqlQuery.list();

		Iterator<Object> iterator = response.iterator();
		Object tempValues [] = null;
		HomiscoResults homes = new HomiscoResults();
		if (iterator.hasNext())
		{
			tempValues = (Object[]) iterator.next();
			homes.setStatus((String)tempValues[0]);
			homes.setBalance((BigDecimal)tempValues[1]);
		}

		System.out.println("Value returned from the database {"+tempValues[0]+","+tempValues[1]+"} for MSISDN="+msisdn);

		currentSession().close();

		return homes;
	}
	
	public static void main(String[] args){
		System.out.println(new HomiscoService().getPostpaidSubscriberCreditLimit("260978772609"));
	}

}
