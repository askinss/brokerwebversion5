package com.vasconsulting.www.utility;

import com.vasconsulting.www.domain.ActivityLogger;
import com.vasconsulting.www.domain.DeductionLog;
import com.vasconsulting.www.domain.FutureRenewal;
import com.vasconsulting.www.domain.ResponseDetails;
import com.vasconsulting.www.domain.RolesDetail;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.domain.UserLoginDetails;
import com.vasconsulting.www.domain.Users;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.misc.BASE64Decoder;

public class LoginRestControllerUtility
{
  private Logger logger = Logger.getLogger(LoginRestControllerUtility.class);

  private ResponseDetails response = new ResponseDetails();
  private RIMXMLResponseDetail rimStatus = new RIMXMLResponseDetail();
  private ArrayList<BillingPlanObjectUtility> billingPlans;
  private LoadAllBillingPlanObjects allBillingPlans;
  private RIMXMLUtility rimXMLUtility;
  private HibernateUtility hibernateUtility;
  private TransactionLog transactionLog = new TransactionLog();
  private LoadAllProperties properties;
  private EmailTaskExecutor emailExecutor;
  private BrokerService brokerService;
  private String randomPassword;
  private SimpleMailMessage mailMessage;
  private SendSmsToKannelService sendSmsToKannelService;
  private ActivityLogger logon = new ActivityLogger();
  private EncryptAndDecrypt encryptAndDecrypt = new EncryptAndDecrypt();

  public void setSendSmsToKannelService(SendSmsToKannelService sendSmsToKannelService)
  {
    this.sendSmsToKannelService = sendSmsToKannelService;
  }

  public void setMailMessage(SimpleMailMessage mailMessage) {
    this.mailMessage = mailMessage;
  }

  public void setRandomPassword(String randomPassword) {
    this.randomPassword = randomPassword;
  }

  public void setBrokerService(BrokerService brokerService) {
    this.brokerService = brokerService;
  }

  public void setMyEmailTaskExecutor(EmailTaskExecutor myEmailTaskExecutor) {
    this.emailExecutor = myEmailTaskExecutor;
  }

  public void setRimXMLUtility(RIMXMLUtility rimXMLUtility) {
    this.rimXMLUtility = rimXMLUtility;
  }

  public void setAllBillingPlans(LoadAllBillingPlanObjects allBillingPlans)
  {
    this.allBillingPlans = allBillingPlans;
  }

  public void setProperties(LoadAllProperties properties) {
    this.properties = properties;
  }

  public void setHibernateUtility(HibernateUtility hibernateUtility)
  {
    this.hibernateUtility = hibernateUtility;
  }

  public ResponseDetails resetPasswordUserDetails(Users userDetails, HttpServletRequest request)
  {
    this.logger.info("Request to create a update User with email " + 
      userDetails.getEmail() + " has been received");
    this.response = new ResponseDetails();
    try
    {
      this.logger.info("The details of the user received to update is as follows - New Password: " + 
        userDetails.getPassword());

      this.logger.info("searching for user " + userDetails.getUsername());
      Users userDetailSearch = this.hibernateUtility
        .getUserDetailByUsername(userDetails.getUsername());
      if (userDetailSearch != null) {
        this.logger.info("user found, setting new password: " + 
          userDetails.getPassword() + ", on old password: " + 
          userDetailSearch.getPassword());

        userDetailSearch.setPassword(userDetails.getPassword());
        userDetailSearch.setIsdefaultpassword("NO");

        this.hibernateUtility.updateBrokerUserDetail(userDetailSearch);
        this.logger.info("password reset successfully for user " + 
          userDetails.getUsername());
      } else {
        this.logger.info("user not found..exiting..");

        this.response.setStatusCode(102);
        this.response.setDescription("User details was not found, Please check the email address.");
        return this.response;
      }
      try {
        this.logger.info("sending email/sms notification to the user");

        this.emailExecutor.sendQueuedEmails("Your New Password: " + 
          userDetails.getPassword());
      }
      catch (Exception ex)
      {
        this.logger.info(ex.getMessage());
      }
      this.response.setStatusCode(0);
      this.response.setDescription("User " + userDetails.getUsername() + 
        " updated successfully");

      return this.response;
    } catch (Exception ex) {
      ex.printStackTrace();
      this.response.setDescription(ex.getMessage());
    }return this.response;
  }

  public ResponseDetails updateUserDetails(Users userDetails, HttpServletRequest request)
  {
    this.logger.info("Request to  update User with email " + 
      userDetails.getEmail() + " has been received");
    this.response = new ResponseDetails();
    try
    {
      if ((userDetails.getEmail() == null) || 
        (userDetails.getEmail()
        .equalsIgnoreCase("")) || 
        (userDetails.getMsisdn() == null) || 
        (userDetails
        .getMsisdn().equalsIgnoreCase(""))) {
        this.response.setStatusCode(101);
        this.response.setDescription("User needs an email address to activate this service");

        this.logger.info("101, User needs an email address to activate this service");

        return this.response;
      }

      this.logger.info("The details of the user received to update is as follows - Name : " + 
        userDetails.getFirstname() + 
        " " + 
        userDetails.getLastname() + 
        " with email : " + 
        userDetails.getEmail());

      this.logger.info("updating User " + userDetails.getUsername());
      this.hibernateUtility.updateBrokerUserDetail(userDetails);
      try
      {
        this.emailExecutor.sendQueuedEmails("Your Updated Password: " + 
          userDetails.getPassword());
      }
      catch (Exception ex)
      {
        this.logger.info(ex.getMessage());
      }
      this.response.setStatusCode(0);
      this.response.setDescription("User " + userDetails.getUsername() + 
        " updated successfully");

      return this.response;
    } catch (Exception ex) {
      ex.printStackTrace();
    }return this.response;
  }

  public ResponseDetails saveUserDetails(Users userDetails, HttpServletRequest request)
  {
    this.logger.info("Request to create a new User with email " + 
      userDetails.getEmail() + " has been received");
    this.response = new ResponseDetails();
    try
    {
      if ((userDetails.getEmail() == null) || 
        (userDetails.getEmail()
        .equalsIgnoreCase("")) || 
        (userDetails.getMsisdn() == null) || 
        (userDetails
        .getMsisdn().equalsIgnoreCase(""))) {
        this.response.setStatusCode(101);
        this.response.setDescription("User needs an email address to activate this service");

        return this.response;
      }

      this.logger.info("The details of the user received to save is as follows - Name : " + 
        userDetails.getFirstname() + 
        " " + 
        userDetails.getLastname() + 
        " with email : " + 
        userDetails.getEmail());

      this.logger.info("Broker Saving User " + userDetails.getUsername());

      userDetails.setPassword(userDetails.getPassword());
      int response_code = this.hibernateUtility
        .saveBrokerUserDetail(userDetails);

      this.logger.info("Database response [" + response_code + 
        "] from saving user " + userDetails.getFirstname());
      if (response_code == 0)
      {
        try
        {
          this.sendSmsToKannelService.sendMessageToKannel("Your Password: " + 
            userDetails.getPassword(), userDetails.getMsisdn());

          String[] emails = { userDetails.getEmail() };

          //String password = this.encryptAndDecrypt.decrypt(userDetails.getEncryptedPassword());
          String password = this.encryptAndDecrypt.decrypt(userDetails.getPassword());
          this.logger.info("UserNew password" + password);

          String newusermailbodyMessage = String.format(
            this.properties.getProperty("newusermailbody"), new Object[] { 
            userDetails.getFirstname(), userDetails.getUsername(), password, 
            this.properties.getProperty("webserviceURL") + 
            "/blackberry/login" });

          this.logger.info("About to send" + newusermailbodyMessage);
          this.mailMessage.setSubject("BB Broker New User Alert");
          this.emailExecutor.setMailMessage(this.mailMessage);
          this.emailExecutor.sendQueuedEmails(newusermailbodyMessage, emails);

          this.logon.setAction("Creation");
          this.logon.setDescription("NewUserCreation");
          this.logon.setUsername(userDetails.getUsername());

          Calendar calendar2 = new GregorianCalendar();
          System.out.println("TIme got here" + calendar2);
          this.logon.setDate_created(calendar2);

          int i = this.hibernateUtility.logService(this.logon);
        }
        catch (Exception ex)
        {
          this.logger.info(ex.getMessage());
        }
        this.response.setStatusCode(response_code);
        this.response.setDescription("User " + userDetails.getUsername() + 
          " created successfully");
      }
      return this.response;
    } catch (Exception ex) {
      ex.printStackTrace();
    }return this.response;
  }

  public ResponseDetails provisionSubscriber(SubscriberDetail subscriberDetails, Users userDetail, HttpServletRequest userRequest)
  {
    this.logger.info("Request to provision Subscriber has been received with msisdn: " + 
      subscriberDetails.getMsisdn() + 
      " shortcode: " + 
      subscriberDetails.getShortCode());
    this.response = new ResponseDetails();
    this.response.setStatusCode(1000);
    this.response.setDescription("GENERAL ERROR");
    try
    {
      if ((subscriberDetails.getMsisdn() == null) || 
        (subscriberDetails.getMsisdn().equalsIgnoreCase(""))) {
        this.response.setStatusCode(201);
        this.response.setDescription("Subscriber MSISDN cannot be empty for this request");
        return this.response;
      }
      LoadAllBillingPlanObjects loadAllBillingPlanObjects = (LoadAllBillingPlanObjects)
        ContextLoaderImpl.getBeans("loadAllBillingPlanObjects");
      this.brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
      int provisionStatus = this.brokerService.activate(subscriberDetails
        .getShortCode(), subscriberDetails);
      this.response.setStatusCode(provisionStatus);
      this.response.setDescription(this.properties.getProperty(String.valueOf(provisionStatus)));
      this.response.setClassInstance(null);

      this.logon.setAction("Activation");
      this.logon.setDescription("Activate Subscriber Using GUI");
      this.logon.setUsername(userDetail.getUsername());
      this.logon.setMsisdn(subscriberDetails.getMsisdn());
      Calendar calendar2 = new GregorianCalendar();
      System.out.println("TIme got here" + calendar2);
      this.logon.setDate_created(calendar2);
      this.logon.setHost_ip(userRequest.getRemoteAddr());
      int hresp = this.hibernateUtility.logService(this.logon);

      System.out.println("insert userdetails in ActivityLogger for  " + userDetail.getUsername() + "and " + subscriberDetails.getMsisdn());
      System.out.println("Commit activation  to audittrail  returns " + hresp);
      System.out.println("Commit activation  to audittrail  returns " + userDetail.getUsername());

      return this.response;
    }
    catch (Exception ex)
    {
      this.response.setStatusCode(1000);
      this.response.setDescription("GENERAL ERROR");
    }return this.response;
  }

  public ResponseDetails saveSubscriberDetailsPost(SubscriberDetail subscriberDetails)
  {
    this.logger.info("Request to create a new Postpaid Subscriber has been received");
    this.response = new ResponseDetails();
    this.response.setStatusCode(1000);
    this.response.setDescription("GENERAL ERROR");
    try
    {
      if ((subscriberDetails.getMsisdn() == null) || 
        (subscriberDetails.getMsisdn().equalsIgnoreCase(""))) {
        this.response.setStatusCode(201);
        this.response.setDescription("Subscriber MSISDN cannot be empty for this request");
        return this.response;
      }
      LoadAllBillingPlanObjects loadAllBillingPlanObjects = (LoadAllBillingPlanObjects)
        ContextLoaderImpl.getBeans("loadAllBillingPlanObjects");
      this.brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
      int provisionStatus = this.brokerService.activate(subscriberDetails
        .getShortCode(), subscriberDetails);
      this.response.setStatusCode(provisionStatus);
      this.response.setDescription(String.format(this.properties.getProperty(String.valueOf(provisionStatus)), new Object[] { this.brokerService.getBillingPlanObject().getDescription() }));
      this.response.setClassInstance(null);
      return this.response;
    } catch (Exception ex) {
      this.response.setStatusCode(1000);
      this.response.setDescription("GENERAL ERROR");
    }return this.response;
  }

  @ResponseBody
  public ResponseDetails saveSubscriberDetails(SubscriberDetail subscriberDetails)
  {
    this.logger.info("Request to create a new Subscriber has been received");
    this.response = new ResponseDetails();

    this.response.setStatusCode(1000);
    this.response.setDescription("GENERAL ERROR");
    try
    {
      if ((subscriberDetails.getMsisdn() == null) || (subscriberDetails.getMsisdn().equalsIgnoreCase("")))
      {
        this.response.setStatusCode(201);
        this.response.setDescription("Subscriber MSISDN cannot be empty for this request");

        return this.response;
      }

      this.logger.info("The details of the subscriber received to save is as follows - Name : " + subscriberDetails.getFirstname() + " " + 
        subscriberDetails.getLastname() + " with email : " + subscriberDetails.getEmail());

      LoadAllBillingPlanObjects loadAllBillingPlanObjects = (LoadAllBillingPlanObjects)
        ContextLoaderImpl.getBeans("loadAllBillingPlanObjects");
      this.brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);

      int provisionStatus = this.brokerService.activate(subscriberDetails.getShortCode(), subscriberDetails);
      this.response.setStatusCode(provisionStatus);
      this.response.setDescription(String.format(this.properties.getProperty(String.valueOf(provisionStatus)), new Object[] { 
        this.brokerService.getBillingPlanObject().getDescription() }));
      this.response.setClassInstance(null);
      return this.response;
    }
    catch (Exception ex)
    {
      this.response.setStatusCode(1000);
      this.response.setDescription("GENERAL ERROR");
    }return this.response;
  }

  public ResponseDetails deactivateSubscriber(SubscriberDetail subscriberDetails)
  {
    Users userDetail = new Users();

    this.logger.info("Request to deactivate Subscriber has been received");
    this.response = new ResponseDetails();
    this.response.setStatusCode(1000);
    this.response.setDescription("");
    try {
      if ((subscriberDetails.getMsisdn() == null) || 
        (subscriberDetails.getMsisdn().equalsIgnoreCase(""))) {
        this.response.setStatusCode(201);
        this.response.setDescription("Subscriber MSISDN cannot be empty for this request");
      }
      try
      {
        LoadAllBillingPlanObjects loadAllBillingPlanObjects = (LoadAllBillingPlanObjects)
          ContextLoaderImpl.getBeans("loadDeactivateBillingPlanObjects");
        this.brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
        int provisionStatus = this.brokerService.deactivate(subscriberDetails);
        this.response.setStatusCode(provisionStatus);
        this.response.setDescription("Requested Operation was completed successfully");

        System.out.println("insert userdetails in ActivityLogger for  " + userDetail.getUsername() + "and " + subscriberDetails.getMsisdn());
        this.logon = new ActivityLogger();

        this.logon.setAction("Deactivation");
        this.logon.setDescription("Deactivate Subscriber GUI");
        this.logon.setUsername(userDetail.getUsername());
        this.logon.setMsisdn(subscriberDetails.getMsisdn());
        this.logon.setDate_created(new GregorianCalendar());

        int hresp = this.hibernateUtility.logService(this.logon);

        System.out.println("Commit deactivation  to audittrail  returns " + hresp);
        System.out.println("Commit deactivation  to audittrail  returns " + userDetail.getUsername());
      }
      catch (Exception e)
      {
        e.printStackTrace();
        this.response.setStatusCode(1000);
        this.response.setDescription("GENERAL ERROR");
      }
    } catch (Exception e) {
      e.printStackTrace();
      this.response.setStatusCode(1000);
      this.response.setDescription("GENERAL ERROR");
    }
    return this.response;
  }

  public ResponseDetails authenticateUserById(Users userDetail, HttpServletRequest userRequest, HttpSession httpSession)
  {
    this.logger.info("Request to authenticate a new user has been received.");
    this.logger.info("Getting the user's request info");

    String userName = userDetail.getUsername();
    String client_ip = userRequest.getRemoteAddr();
    this.logon = new ActivityLogger();

    this.response = new ResponseDetails();
    UserLoginDetails responseData = new UserLoginDetails();

    this.logger.info("Client IP: " + client_ip);
    try {
      this.logger.info("value of hibernate = " + this.hibernateUtility);
      Users userDetailSearch = this.hibernateUtility
        .getUserDetailByUsername(userDetail);

      if (userDetailSearch == null) {
        this.logger.info("User not found in the application returning code 102");

        this.response.setStatusCode(102);
        this.response.setDescription("User details was not found, Please check the email address.");
      } else {
        this.logger.info("User found in the application returning code 0");

        responseData.setId(UUID.randomUUID().toString());
        responseData
          .setDate_created(userDetailSearch.getDate_created());
        responseData.setSessionID(UUID.randomUUID().toString());
        responseData.setEmail(userDetailSearch.getEmail());
        responseData.setUserID(userDetailSearch.getId());
        responseData.setUsertype(userDetailSearch.getUsertype());
        responseData.setRoleid(userDetailSearch.getRoleid());
        this.logger.info("role types set successfully..");
        this.response.setUserLoginDetails(responseData);
        this.response.setClassInstance(responseData);
        this.response.setStatusCode(0);
        this.response.setDescription("Requested Operation was completed successfully");

        this.logon.setAction("User Login");
        this.logon.setDescription("Successful Login");
        this.logon.setUsername(userDetail.getUsername());
        this.logon.setMsisdn("Agent");
        Calendar calendar2 = new GregorianCalendar();
        System.out.println("TIme got here" + calendar2);
        this.logon.setDate_created(calendar2);
        this.logon.setHost_ip(userRequest.getRemoteAddr());
        int hresp = this.hibernateUtility.logService(this.logon);

        System.out.println("insert userdetails in ActivityLogger for  " + userDetail.getUsername());
        System.out.println("Commit Login to audittrail  returns " + hresp);

        this.logger.info("isdefault passsword value " + 
          userDetailSearch.getIsdefaultpassword());

        if (userDetailSearch.getIsdefaultpassword().equalsIgnoreCase(
          "YES")) {
          this.logger.info("user needs to reset password..");
          this.logger.info("setting response status code to 120");

          this.response.setStatusCode(120);
          this.response.setDescription("The user needs to reset his/her password");
        }
      }
      this.logger.info("User session created and returned successfully.");
      return this.response;
    } catch (Exception ex) {
      ex.printStackTrace();
      this.response.setStatusCode(1010);
      this.response.setDescription("Error authenticating User. Please try again later.");
    }return this.response;
  }

  public SubscriberDetail getSubscriberByType(String searchType, String searchValue)
  {
    try
    {
      this.logger.info("Request to load the values of search " + searchType + 
        "/" + searchValue);
      SubscriberDetail sub = null;

      sub = this.hibernateUtility.getSubscriberInformation(searchType, 
        searchValue);
      this.logger.info("value of sub = " + sub);

      if (sub == null) {
        SubscriberDetail subb = new SubscriberDetail();
        subb.setId(null);
        return subb;
      }

      return sub;
    } catch (Exception ex) {
      ex.printStackTrace();
    }return null;
  }

  public ArrayList<Users> getAllUserDetails(HttpServletRequest request)
  {
    try
    {
      this.logger.info("Request to load the users on the system has been received");

      ArrayList sub = null;

      sub = this.hibernateUtility.getAllUserDetails();
      this.logger.info("value of sub = " + sub);

      if (sub == null) {
        return new ArrayList();
      }

      return sub;
    } catch (Exception ex) {
      ex.printStackTrace();
    }return new ArrayList();
  }

  public ResponseDetails getSubscriberTransactionLog(String msisdn, HttpServletRequest request)
    throws IOException
  {
    try
    {
      ResponseDetails response = new ResponseDetails();

      Collection transactionsLog = this.hibernateUtility
        .getSubscriberTransactionLog(msisdn);

      if (transactionsLog != null) {
        response.setStatusCode(0);
        response.setDescription("Requested Operation was completed successfully");
        response.setClassInstance(transactionsLog);
      } else {
        response.setStatusCode(102);
        response.setDescription("User details was not found, Please check the email address.");
        response.setClassInstance(new SubscriberDetail());
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      this.response.setStatusCode(1000);
      this.response.setDescription("GENERAL ERROR");
    } finally {
      return this.response;
    }
  }

  public ArrayList<FutureRenewal> getAdvancedSubscription(String msisdn)
  {
    ArrayList futurenewal = new ArrayList();
    this.logger.info("request to show Advanced Subscription for " + msisdn);
    try {
      futurenewal = this.hibernateUtility.allSubscriberFutureRenewals(msisdn);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return futurenewal;
  }

  public ResponseDetails getAvailablePlans(String searchBy)
  {
    this.logger.info("Call to load the available plans has been made to the broker, sort by " + 
      searchBy);
    try
    {
      this.response = new ResponseDetails();

      this.billingPlans = this.allBillingPlans.getAllBillingPlans();

      Map plans = new LinkedHashMap();
      String[] accessRoles = null;

      for (BillingPlanObjectUtility billingPlanObjectUtility : this.billingPlans) {
        if (billingPlanObjectUtility.getAccess() != null)
        {
          accessRoles = billingPlanObjectUtility.getAccess()
            .toLowerCase().split(",");
          if (Arrays.asList(accessRoles).contains(searchBy.toLowerCase())) {
            plans.put(billingPlanObjectUtility.getShortCode(), 
              billingPlanObjectUtility.getDescription());
          }
        }

      }

      this.response.setStatusCode(0);
      this.response.setDescription("Requested Operation was completed successfully");
      this.response.setClassInstance(plans);
    } catch (Exception ex) {
      this.response.setStatusCode(1000);
      this.response.setDescription("GENERAL ERROR");
      ex.printStackTrace();
    } finally {
      return this.response;
    }
  }

  public ResponseDetails getAvailablePlansByType(String searchBy)
  {
    this.logger.info("Call to load the available plans by type has been made to the broker, sort by " + 
      searchBy);
    try
    {
      this.response = new ResponseDetails();

      this.billingPlans = this.allBillingPlans.getAllBillingPlans();

      Map plans = new LinkedHashMap();
      String[] accessRoles = null;

      for (BillingPlanObjectUtility billingPlanObjectUtility : this.billingPlans) {
        if (billingPlanObjectUtility.getAccess() != null)
        {
          accessRoles = billingPlanObjectUtility.getAccess()
            .toLowerCase().split(",");
          if (Arrays.asList(accessRoles).contains(searchBy.toLowerCase())) {
            plans.put(billingPlanObjectUtility.getShortCode(), 
              billingPlanObjectUtility.getDescription());
          }
        }
      }

      this.response.setStatusCode(0);
      this.response.setDescription("Requested Operation was completed successfully");
      this.response.setClassInstance(plans);
    } catch (Exception ex) {
      this.response.setStatusCode(1000);
      this.response.setDescription("GENERAL ERROR");
      ex.printStackTrace();
    } finally {
      return this.response;
    }
  }

  public ResponseDetails getAvailableRoles(HttpServletRequest request)
  {
    this.logger.info("Call to load the available roles has been made to the broker");
    try
    {
      this.response = new ResponseDetails();

      Collection<RolesDetail> responseRoles = this.hibernateUtility
        .getAllRoles();
      this.logger.info("The value of the returned roles is " + responseRoles);

      Map roles = new LinkedHashMap();

      for (RolesDetail tempResponseRoles : responseRoles) {
        roles.put(tempResponseRoles.getId(), 
          tempResponseRoles.getRolename());
      }

      this.response.setStatusCode(0);
      this.response.setDescription("Requested Operation was completed successfully");
      this.response.setClassInstance(roles);
    } catch (Exception ex) {
      this.response.setStatusCode(1000);
      this.response.setDescription("GENERAL ERROR");
      ex.printStackTrace();
    } finally {
      return this.response;
    }
  }

  public ResponseDetails modifyBBSubscriberNonBillingIdentifier(SubscriberDetail userDetail)
  {
    this.logger.info("in device change action..");
    ResponseDetails userDetailData = new ResponseDetails();
    if ((!"".equals(userDetail.getImsi())) || 
      (!"".equals(userDetail.getMsisdn()))) {
      this.logger.info("Subscriber " + userDetail + 
        " has requested for a no identifier RIM update");
      try
      {
        this.rimStatus = this.rimXMLUtility
          .modifyNonBillingIdentifier(userDetail);
        if (!this.rimStatus.getErrorCode().equalsIgnoreCase("0")) {
          if (new Integer(this.rimStatus.getErrorCode()).intValue() >= 21000) {
            if (new Integer(
              this.rimStatus.getErrorCode()).intValue() > 21500);
          } } else { int hibernateStatus = this.hibernateUtility
            .updateSubscriberDetail(userDetail);

          TransactionLog transactionLog = new TransactionLog();

          transactionLog.setDate_created(new GregorianCalendar());
          transactionLog.setDescription("Non-billing ID SWAP");
          transactionLog.setMsisdn(userDetail.getMsisdn());
          transactionLog.setStatus(userDetail.getStatus());
          transactionLog.setService(userDetail.getServiceplan());

          this.hibernateUtility.saveTransactionlog(transactionLog);

          this.response.setStatusCode(0);
          this.response.setDescription("Requested Operation was completed successfully");

          return this.response;
        }

        this.response.setStatusCode(new Integer(this.rimStatus.getErrorCode()).intValue());
        this.response.setDescription(this.rimStatus.getErrorDescription());

        return this.response;
      }
      catch (Exception ex) {
        ex.printStackTrace();
        this.response.setStatusCode(1000);
        this.response.setDescription("GENERAL ERROR");

        return this.response;
      }
    }

    this.response.setStatusCode(200);
    this.response.setDescription("Subscriber MSISDN cannot be empty for this request");

    return this.response;
  }

  public ResponseDetails modifyBBSubscriberBillingIdentifier(String oldImsi, String newImsi)
  {
    ResponseDetails response = new ResponseDetails();

    this.logger.info("modifyBBSubscriberBillingIdentifier method called to swap old IMSI [" + 
      oldImsi + "] to new IMSI [" + newImsi + "]");

    this.logger.info("searching for subscrber with imsi: " + oldImsi);
    if (this.hibernateUtility.getSubscriberInformationByImsi(oldImsi) != null) {
      this.logger.info("subscriber found..proceed to swapping");
      SubscriberDetail subscriber = 
        (SubscriberDetail)this.hibernateUtility
        .getSubscriberInformationByImsi(oldImsi).iterator().next();
      this.logger.info("subcriber " + subscriber.getEmail() + ", " + 
        subscriber.getMsisdn() + " with imsi " + 
        subscriber.getImsi() + " found..");

      if (subscriber.getStatus().equalsIgnoreCase("Active")) {
        LoadAllBillingPlanObjects loadAllBillingPlanObjects = (LoadAllBillingPlanObjects)
          ContextLoaderImpl.getBeans("loadSimSwapBillingPlanObjects");
        this.brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
        int responseCode = this.brokerService.simswap(subscriber);
        response.setStatusCode(responseCode);
        response.setDescription("Requested Operation was completed successfully");
        return response;
      }
      response.setStatusCode(203);
      response.setDescription("Sim Swap cannot be done for a subscriber that is not active...");
      return response;
    }

    return response;
  }

  private ResponseDetails getUserDetailsFromRequest(HttpServletRequest servletRequest)
    throws IOException
  {
    Enumeration enums = servletRequest.getHeaderNames();
    while (enums.hasMoreElements()) {
      String headerNames = (String)enums.nextElement();
      this.logger.debug("Header Name =  : " + headerNames + " : " + 
        servletRequest.getHeader(headerNames));
    }
    String userDetails = servletRequest.getHeader("authorization");

    String userDetailsFreeForm = new String(
      new BASE64Decoder().decodeBuffer(userDetails));

    ResponseDetails response = new ResponseDetails();
    response.setStatusCode(0);
    response.setDescription("Requested Operation was completed successfully");
    response.setClassInstance("eg335289475905j57fd47s");

    return response;
  }

  /**
  @ResponseBody
  public ResponseDetails getServicePlanByShortCode(String shortCode, HttpServletRequest request)
    throws IOException
  {
    this.logger.info("Call to load the available plans has been made to bb broker");
    try
    {
      this.response = new ResponseDetails();

      this.billingPlans = this.allBillingPlans.getAllBillingPlans();

      Map plans = new LinkedHashMap();
      String[] accessRoles = null;
      int j;
      int i;
      for (Iterator localIterator = this.billingPlans.iterator(); localIterator.hasNext(); 
        i < j)
      {
        BillingPlanObjectUtility billingPlanObjectUtility = (BillingPlanObjectUtility)localIterator.next();

        if (shortCode.equalsIgnoreCase(billingPlanObjectUtility
          .getShortCode())) {
          this.response.setClassInstance(billingPlanObjectUtility);
          break;
        }

        accessRoles = billingPlanObjectUtility.getAccess().split(",");
        String[] arrayOfString1;
        j = (arrayOfString1 = accessRoles).length; i = 0; continue; String tempRole = arrayOfString1[i];
        if (shortCode.equalsIgnoreCase(tempRole))
          plans.put(billingPlanObjectUtility.getShortCode(), 
            billingPlanObjectUtility.getDescription());
        i++;
      }

      this.response.setStatusCode(0);
      this.response.setDescription("Requested Operation was completed successfully");
    } catch (Exception ex) {
      this.response.setStatusCode(1000);
      this.response.setDescription("GENERAL ERROR");
      ex.printStackTrace();
    } finally {
      return this.response;
    }
  }
**/
  public ArrayList<DeductionLog> getAllUsageReport(String service_search, String type_search, String days_search, String shortcode_search)
  {
    try
    {
      this.logger.info("Request to load the users on the system has been received");

      ArrayList sub = null;

      sub = this.hibernateUtility.getRevenueReportByDetail(service_search, 
        new Integer(days_search).intValue(), new Integer(type_search).intValue(), 
        shortcode_search);
      this.logger.info("===========================");
      this.logger.info("===========================");

      if (sub == null) {
        return new ArrayList();
      }

      return sub;
    } catch (Exception ex) {
      ex.printStackTrace();
    }return new ArrayList();
  }

  public ArrayList<Users> getUserDetailById(String searchBy, HttpServletRequest request)
  {
    try
    {
      this.logger.info("Request to load the user details on the system has been received");

      ArrayList sub = this.hibernateUtility
        .getUserDetailsById(searchBy);
      this.logger.info("===========================");
      this.logger.info("===========================");

      return sub;
    } catch (Exception ex) {
      ex.printStackTrace();
    }return new ArrayList();
  }

  public ArrayList<DeductionLog> getAllMarketingUsageReport(String days_search)
  {
    try
    {
      this.logger.info("Request to load the users on the system has been received");

      ArrayList sub = null;

      sub = this.hibernateUtility.getRevenueReportBySummation(new Integer(
        days_search).intValue());
      this.logger.info("===========================");
      this.logger.info("===========================");
      this.logger.info("value of sub = " + sub);

      return sub;
    } catch (Exception ex) {
      ex.printStackTrace();
    }return new ArrayList();
  }

  public ResponseDetails getAvailableShortcodeReport()
  {
    this.logger.info("Call to load the available shortcodes for reports has been made to the broker");
    try
    {
      this.response = new ResponseDetails();

      Collection responseRoles = this.hibernateUtility
        .getAllShortcodesForReport();
      this.logger.info("The value of the returned roles is " + responseRoles);

      this.response.setStatusCode(0);
      this.response.setDescription("Requested Operation was completed successfully");
      this.response.setClassInstance(responseRoles);
    } catch (Exception ex) {
      this.response.setStatusCode(1000);
      this.response.setDescription("GENERAL ERROR");
      ex.printStackTrace();
    } finally {
      return this.response;
    }
  }

  public ResponseDetails getAvailableServiceReport()
  {
    this.logger.info("Call to load the available services for reports has been made to the broker");
    try
    {
      this.response = new ResponseDetails();

      Collection responseRoles = this.hibernateUtility
        .getAllServicesForReport();
      this.logger.info("The value of the returned roles is " + responseRoles);

      this.response.setStatusCode(0);
      this.response.setDescription("Requested Operation was completed successfully");
      this.response.setClassInstance(responseRoles);
    } catch (Exception ex) {
      this.response.setStatusCode(1000);
      this.response.setDescription("GENERAL ERROR");
      ex.printStackTrace();
    } finally {
      return this.response;
    }
  }

  public ResponseDetails deleteUserById(String userId, HttpServletRequest request)
  {
    this.logger.info("Call to delete a user with ID = " + userId + 
      " has been made to the broker");
    try
    {
      this.response = new ResponseDetails();
      int status = 0;

      Collection<Users> responseRoles = this.hibernateUtility
        .getUserDetailsById(userId);
      this.logger.info("The value of the returned user is " + responseRoles);

      for (Users tempResponseRoles : responseRoles) {
        status = this.hibernateUtility
          .deleteUserdetailById(tempResponseRoles);

        if (status == 0) {
          this.response.setStatusCode(0);
          this.response.setDescription("Requested Operation was completed successfully");
        } else {
          this.response.setStatusCode(1000);
          this.response.setDescription("GENERAL ERROR");
        }
      }
    } catch (Exception ex) {
      this.response.setStatusCode(1000);
      this.response.setDescription("GENERAL ERROR");
      ex.printStackTrace();
    } finally {
      return this.response;
    }
  }

  public ResponseDetails resetUserById(String userId, HttpServletRequest request)
  {
    this.logger.info("Call to reset a user with ID = " + userId + 
      " has been made to the broker");
    try
    {
      this.response = new ResponseDetails();
      int status = 0;

      Collection<Users> responseRoles = this.hibernateUtility
        .getUserDetailsById(userId);
      this.logger.info("The value of the returned user is " + responseRoles);

      for (Users tempResponseRoles : responseRoles) {
        this.logger.info("About to generate password");

        tempResponseRoles.setPassword(this.randomPassword);

        status = this.hibernateUtility
          .updateUserdetailById(tempResponseRoles);

        if (status == 0) {
          this.response.setStatusCode(0);
          this.response.setDescription("Requested Operation was completed successfully");

          String resetusermailbodyMessage = String.format(
            this.properties.getProperty("resetusermailbody"), new Object[] { 
            tempResponseRoles.getUsername(), this.randomPassword, 
            this.properties.getProperty("webserviceURL") + 
            "/blackberry/login" });

          this.sendSmsToKannelService.sendMessageToKannel(resetusermailbodyMessage, tempResponseRoles.getMsisdn());
          String[] receipentList = new String[1];
          receipentList[0] = tempResponseRoles.getEmail();

          this.logger.info("About to send this email to " + receipentList);
          this.logger.info("About to send this email" + 
            resetusermailbodyMessage);
          this.mailMessage.setSubject("BB Broker Password Reset");
          this.emailExecutor.setMailMessage(this.mailMessage);
          this.emailExecutor.sendQueuedEmails(resetusermailbodyMessage, 
            receipentList);
        } else {
          this.response.setStatusCode(1000);
          this.response.setDescription("GENERAL ERROR");
        }
      }
    } catch (Exception ex) {
      this.response.setStatusCode(1000);
      this.response.setDescription("GENERAL ERROR");
      ex.printStackTrace();
    } finally {
      return this.response;
    }
  }

  public ResponseDetails unlockUserById(String userId)
  {
    this.logger.info("Call to unlock a user with ID = " + userId + 
      " has been made to the broker");
    try
    {
      this.response = new ResponseDetails();
      int status = 0;

      Collection<Users> responseRoles = this.hibernateUtility
        .getUserDetailsById(userId);
      this.logger.info("The value of the returned user is " + responseRoles);

      for (Users tempResponseRoles : responseRoles)
      {
        status = this.hibernateUtility
          .updateUserdetailById(tempResponseRoles);

        if (status == 0) {
          this.response.setStatusCode(0);
          this.response.setDescription("Requested Operation was completed successfully");
        }
        else {
          this.response.setStatusCode(1000);
          this.response.setDescription("GENERAL ERROR");
        }
      }
    } catch (Exception ex) {
      this.response.setStatusCode(1000);
      this.response.setDescription("GENERAL ERROR");
      ex.printStackTrace();
    } finally {
      return this.response;
    }
  }
  
  
  
  public ResponseDetails updateUserById(String userId, HttpServletRequest request)
  {
    this.logger.info("Call to update a user with ID = " + userId + 
      " has been made to the broker");
    try
    {
      this.response = new ResponseDetails();
      int status = 0;

      Collection<Users> responseRoles = this.hibernateUtility
        .getUserDetailsById(userId);
      this.logger.info("The value of the returned user is " + responseRoles);

      for (Users tempResponseRoles : responseRoles) {
        this.logger.info("About to update userdetails");

        tempResponseRoles.setPassword(this.randomPassword);

        status = this.hibernateUtility
          .updateUserdetailById(tempResponseRoles);

        if (status == 0) {
          this.response.setStatusCode(0);
          this.response.setDescription("Requested Operation was completed successfully");

          String resetusermailbodyMessage = String.format(
            this.properties.getProperty("resetusermailbody"), new Object[] { 
            tempResponseRoles.getUsername(), this.randomPassword, 
            this.properties.getProperty("webserviceURL") + 
            "/blackberry/login" });

          this.sendSmsToKannelService.sendMessageToKannel(resetusermailbodyMessage, tempResponseRoles.getMsisdn());
          String[] receipentList = new String[1];
          receipentList[0] = tempResponseRoles.getEmail();

          this.logger.info("About to send this email to " + receipentList);
          this.logger.info("About to send this email" + 
            resetusermailbodyMessage);
          this.mailMessage.setSubject("BB Broker Password Reset");
          this.emailExecutor.setMailMessage(this.mailMessage);
          this.emailExecutor.sendQueuedEmails(resetusermailbodyMessage, 
            receipentList);
        } else {
          this.response.setStatusCode(1000);
          this.response.setDescription("GENERAL ERROR");
        }
      }
    } catch (Exception ex) {
      this.response.setStatusCode(1000);
      this.response.setDescription("GENERAL ERROR");
      ex.printStackTrace();
    } finally {
      return this.response;
    }
  }
  
  
  public ResponseDetails editUserById(String userId, HttpServletRequest request)
  {
    this.logger.info("Call to edit a user with ID = " + userId + 
      " has been made to the broker");
    try
    {
      this.response = new ResponseDetails();
      int status = 0;

      Collection<Users> responseRoles = this.hibernateUtility
        .getUserDetailsById(userId);
      this.logger.info("The value of the returned user is " + responseRoles);

      for (Users tempResponseRoles : responseRoles) {
        this.logger.info("About to edit userdetails");

       // tempResponseRoles.setPassword(this.randomPassword);

        //status = this.hibernateUtility.updateUserdetailById(tempResponseRoles);

       
    }
    }catch (Exception ex) {
      this.response.setStatusCode(1000);
      this.response.setDescription("GENERAL ERROR");
      ex.printStackTrace();
    } finally {
      return this.response;
    }
  }
}

