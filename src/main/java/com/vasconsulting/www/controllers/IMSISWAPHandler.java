package com.vasconsulting.www.controllers;

import java.io.IOException;

import net.rubyeye.xmemcached.MemcachedClient;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import sun.misc.BASE64Decoder;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.RequestTracker;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BrokerService;
import com.vasconsulting.www.utility.LoadAllBillingPlanObjects;


@SuppressWarnings("restriction")
@Controller
public class IMSISWAPHandler {	

	
       private HibernateUtility hibernateUtility;
		private int provisionStatus = -1;
		@Autowired
		private BrokerService brokerService;
		@Autowired
		private TransactionLog transactionlog;
		@Autowired
		private MemcachedClient memcachedClient;

		@Autowired
		public void setHibernateUtility(HibernateUtility hibernateUtility)
		{
			this.hibernateUtility = hibernateUtility;
		}
		
		@Autowired
		public void setBrokerService(BrokerService brokerService) {
			this.brokerService = brokerService;
		}

		Logger logger = Logger.getLogger(SMSHandler.class);

		private String stripLeadingMsisdnPrefix(String msisdn){
			String Msisdn = msisdn;
			if (msisdn.startsWith("+")){
				return Msisdn.substring(1, Msisdn.length());
			}
			else return msisdn;
		}

		public int provisionMethod(String msisdn, String imsi){
			logger.info("In the provision simswap method the details are "+msisdn+"::"+imsi);
			saveSwapRequest(msisdn, imsi);
			SubscriberDetail subscriberDetail = new SubscriberDetail();
			subscriberDetail.setMsisdn(stripLeadingMsisdnPrefix(msisdn));
			subscriberDetail.setImsi(imsi);
			LoadAllBillingPlanObjects loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
					.getBeans("loadAllBillingPlanObjects");
			brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
			provisionStatus = brokerService.imsiswap(subscriberDetail);
			return provisionStatus;
		}
		


		@RequestMapping(value="/simwapservice", method = RequestMethod.GET)
		@ResponseBody
		public ResponseEntity<String> provisionImsiSwap(@RequestParam("msisdn") String msisdn, @RequestParam("imsi") String imsi) throws IOException
		{
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<String>(String.valueOf(provisionMethod(msisdn, imsi)), responseHeaders, HttpStatus.OK);
		}

		

		private void saveSwapRequest(String msisdn, String msg){
			RequestTracker request = new RequestTracker();
			request.setMsisdn(msisdn);
			request.setShortCode(msg.toLowerCase());
			try{
				hibernateUtility.saveRequestTracker(request);
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
		}
		

	}


