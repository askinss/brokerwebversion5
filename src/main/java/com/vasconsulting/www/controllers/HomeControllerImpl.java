package com.vasconsulting.www.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.client.RestTemplate;

import com.vasconsulting.www.domain.DeductionLog;
import com.vasconsulting.www.domain.FutureRenewal;
import com.vasconsulting.www.domain.ResponseDetails;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.UserLoginDetails;
import com.vasconsulting.www.domain.Users;
import com.vasconsulting.www.interfaces.RenewalCommand;
import com.vasconsulting.www.utility.LinkedHashMapUtility;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.LoginRestControllerUtility;


@Controller
//@SessionAttributes("subscriberDetail") // The name of the object I'll be storing, which must be consistently used everywhere else 
public class HomeControllerImpl
{
	private Logger logger = Logger.getLogger(HomeControllerImpl.class);
	private RestTemplate restTemplate = new RestTemplate();
	private LoadAllProperties properties = new LoadAllProperties();
	@Autowired
	public void setLoginRest(LoginRestControllerUtility loginRest) {
		this.loginRest = loginRest;
	}


	private LoginRestControllerUtility loginRest;
	
	/**A confirmation page for the request**/
	@RequestMapping(value = "/home/confirmation", method = RequestMethod.GET)
	public String Confirmation()
	{	
		System.out.println("confirmed details...");
		return "home/confirmation";
	}
	
	
	/* 
	 * this previews the subscriber detail information before creating(provisioning) this subscriber 
	 * */
	@RequestMapping(value = "/preview", method = RequestMethod.GET)
	public String previewSubscriberDetail(Model model, HttpSession httpSession)
	{	
		// we first check if the userDetails session exist
		if(httpSession.getAttribute("userDetail")!=null)
		{
			logger.info("previewing details...");
		
			try
			{
				SubscriberDetail subscriberDetail = (SubscriberDetail)httpSession.getAttribute("savedSubscriberDetail");
		
				if(subscriberDetail!= null)
				{
					System.out.println("In subscriber Detail preview for Subscriber "+subscriberDetail.getEmail());
				}
				model.addAttribute("subscriberDetail",subscriberDetail);
			}
			catch(Exception ex)
			{
				System.out.println(ex.getMessage()+"\n-----------------------------------------------------\n");//ex.printStackTrace();
			}
			// returns the jsp file in the home folder
			return "home/preview"; //it was preview only before
		}
		else
		{
			// we redirect to the login page
			httpSession.invalidate();
			return "redirect:/login";
		}
	}
	
	
	@RequestMapping(value="/notification", method = RequestMethod.GET)
	public String logoutUser(HttpSession httpSession)
	{
		if(httpSession.getAttribute("userDetail")!=null)
		{
			return "notification";
		}
		else
		{
			// we redirect to the login page
			httpSession.invalidate();
			return "redirect:/login";
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/allreports", method = RequestMethod.GET)
	public String loadSubscriberReports(Map<String, Object> model, HttpSession httpSession)
	{
		if(httpSession.getAttribute("userDetail")!=null)
		{
			logger.info("Getting all usage reports from utility class");			
			ArrayList<DeductionLog> billingPlansResponse1 = loginRest.getAllUsageReport("Prepaid Prosumer", 
					new Integer(1).toString(),"bismonth", new Integer(0).toString());
			
			logger.info("Returned report details = "+billingPlansResponse1);
			model.put("userlist", billingPlansResponse1);
									
			// we retrieve the serviceplans for this uer
			ResponseDetails billingPlansResponse = loginRest.getAvailableShortcodeReport();
			
			logger.info("test map = "+(ArrayList<String>)billingPlansResponse.getClassInstance());
			model.put("shortcodeList", (ArrayList<String>)billingPlansResponse.getClassInstance());
						
			billingPlansResponse = loginRest.getAvailableServiceReport();
			
			logger.info("test map = "+(ArrayList<String>)billingPlansResponse.getClassInstance());
			model.put("serviceplanreportList", (ArrayList<String>)billingPlansResponse.getClassInstance());
			
			httpSession.setAttribute("tab", "admin");
			
			return "home/report";
		}
		else
		{
			// we redirect to the login page
			httpSession.invalidate();
			return "redirect:/login";
		}
	}
	
	//@RequestMapping(value="/advancedsubscription/{msisdn}", method = RequestMethod.GET)
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/{msisdn}", method = RequestMethod.GET)
	public String loadAdvancedSubscriptionReports(Map<String, Object> model, @PathVariable("msisdn") String msisdn, HttpSession httpSession)
	{
		if(httpSession.getAttribute("userDetail")!=null)
		{
			logger.info("About to retrieve the advanced subscription details for "+msisdn);					
			
			ArrayList<FutureRenewal> billingPlansResponse1 = loginRest.getAdvancedSubscription(msisdn);	
			//ResponseDetails billingPlansResponse = restTemplate.getForObject(url, ResponseDetails.class);
			logger.info("Returned report details = "+billingPlansResponse1);
						
			model.put("userlist", billingPlansResponse1);
						
			
			httpSession.setAttribute("tab", "admin");
			
			return "home/advancedsubscription";
		}
		else
		{
			// we redirect to the login page
			httpSession.invalidate();
			return "redirect:/login";
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/allmarketingreports", method = RequestMethod.GET)
	public String loadMarketingSubscriberReports(Map<String, Object> model, HttpSession httpSession)
	{
		if(httpSession.getAttribute("userDetail")!=null)
		{
			System.out.println("getting All marketing reports");
			
			ArrayList<DeductionLog> billingPlansResponse1 = loginRest.getAllMarketingUsageReport(new Integer(1).toString());
			
			//ResponseDetails billingPlansResponse = restTemplate.getForObject(url, ResponseDetails.class);
			logger.info("Returned report details = "+billingPlansResponse1);
			model.put("userlist", billingPlansResponse1);
						
			
			httpSession.setAttribute("tab", "admin");
			
			return "home/marketingreport";
		}
		else
		{
			// we redirect to the login page
			httpSession.invalidate();
			return "redirect:/login";
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/allreports", method = RequestMethod.POST)
	public String reLoadSubscriberReports(Map<String, Object> model, @RequestParam("service_search") String service_search
			, @RequestParam("type_search") String type_search, @RequestParam("days_search") String days_search
			, @RequestParam("shortcode_search") String shortcode_search, HttpSession httpSession)
	{
		if(httpSession.getAttribute("userDetail")!=null)
		{
			
			System.out.println("getting url for all usage reports");
			
			ArrayList<DeductionLog> billingPlansResponse1 = loginRest.getAllUsageReport(service_search,new Integer(type_search).toString(), 
					shortcode_search, new Integer(days_search).toString());
			
			
			model.put("userlist", billingPlansResponse1);
			
			
			//load the report search bar with the data from the WS - shortcodes
			ResponseDetails billingPlansResponse = loginRest.getAvailableShortcodeReport();
			
			logger.info("test map = "+(ArrayList<String>)billingPlansResponse.getClassInstance());
			model.put("shortcodeList", (ArrayList<String>)billingPlansResponse.getClassInstance());
			
			//load the report search bar with the data from the WS - Services
			billingPlansResponse = loginRest.getAvailableServiceReport();
			
			logger.info("test map = "+(ArrayList<String>)billingPlansResponse.getClassInstance());
			model.put("serviceplanreportList", (ArrayList<String>)billingPlansResponse.getClassInstance());
			
			httpSession.setAttribute("tab", "admin");
			
			return "home/report";
		}
		else
		{
			// we redirect to the login page
			httpSession.invalidate();
			return "redirect:/login";
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/allmarketingreports", method = RequestMethod.POST)
	public String reLoadMarketingSubscriberReports(Map<String, Object> model, @RequestParam("days_search") String days_search
			, HttpSession httpSession)
	{
		if(httpSession.getAttribute("userDetail")!=null)
		{
			System.out.println("getting all marketing reports for "+days_search+" days back");
			
			ArrayList<DeductionLog> billingPlansResponse1 = loginRest.getAllMarketingUsageReport(new Integer(days_search).toString());
			
			//ResponseDetails billingPlansResponse = restTemplate.getForObject(url, ResponseDetails.class);
			logger.info("Returned report details = "+billingPlansResponse1);
			model.put("userlist", billingPlansResponse1);
						
			
			httpSession.setAttribute("tab", "admin");
			
			return "home/marketingreport";
		}
		else
		{
			// we redirect to the login page
			httpSession.invalidate();
			return "redirect:/login";
		}
	}
	
	
	/*this action is called via a submit post from the preview page..*/
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/home/preview", method = RequestMethod.POST)
	public String completeSavingSubscriberDetail(HttpSession httpSession)
	{	
		if(httpSession.getAttribute("savedSubscriberDetail")!=null)
		{
			SubscriberDetail subscriberDetail = (SubscriberDetail)httpSession.getAttribute("savedSubscriberDetail");

			try
			{
				logger.info("Saving subscriber detail "+subscriberDetail.getFirstname()+" with plan :"+subscriberDetail.getServiceplan());
				
				ResponseDetails billingPlansResponse = getAvailablePlansFromWSByType(httpSession);
				
				//this is the left part of the URL that will be called. It defaults to the Prepaid Version
				String requestType = "savesubscriberdetail";
				
				if(billingPlansResponse != null)
				{
					logger.info("Response code: "+billingPlansResponse.getStatusCode()+", Description: "+billingPlansResponse.getDescription());
					// we get the map value from this response
					logger.info("retrieving plans BY type from web service Response");
			
					Object responseServicePlan = billingPlansResponse.getClassInstance();
					if(responseServicePlan != null)
					{
						Map<String,String> serviceplan = (Map<String, String>) responseServicePlan;
						
						Iterator<String> iterator = serviceplan.keySet().iterator();
						
						while(iterator.hasNext())
						{
							String tempValue = iterator.next();				
							if (tempValue.equalsIgnoreCase(subscriberDetail.getShortCode()))
							{
								if (!serviceplan.get(tempValue).equalsIgnoreCase("PREP"))
								{
									requestType = "savesubscriberdetailpost";
									break;
								}
								
							}
						}
					}
				}
				ResponseDetails responseDetails;
				
				if (requestType.equalsIgnoreCase("savesubscriberdetailpost"))
				{
					responseDetails = loginRest.saveSubscriberDetailsPost((SubscriberDetail)httpSession.getAttribute("savedSubscriberDetail"));
				}
				else
				{
					responseDetails = loginRest.saveSubscriberDetails((SubscriberDetail)httpSession.getAttribute("savedSubscriberDetail"));
				}
				//ResponseDetails responseDetails =  restTemplate.postForObject(properties.getProperty("webserviceURL")+"/broker/"+requestType, (SubscriberDetail)httpSession.getAttribute("savedSubscriberDetail"), ResponseDetails.class);
				
				if(responseDetails != null)
				{
					if(responseDetails.getStatusCode() == 0)
					{
						System.out.println("Subscriber detail saved successfully... ");
						if(responseDetails.getClassInstance()!=null)
						{
							// we add this detail to a session savedSubscriberDetail
							System.out.println("Save Response [Status "+responseDetails.getStatusCode()+"], [Description "+responseDetails.getDescription());
						}
						httpSession.setAttribute("message", responseDetails.getDescription());
						return "redirect:/notification";
					}
					else
					{
						// we print an error message
						// or redirect to the error page
						logger.info("Status code: "+responseDetails.getStatusCode());
						logger.info("Description: "+responseDetails.getDescription());
						httpSession.setAttribute("message", properties.getProperty(new Integer(responseDetails.getStatusCode()).toString()));
						return "redirect:/notification";
					}
				}
				else
				{
					//logger.info("Status code: "+responseDetails.getStatusCode());
					logger.info("Description: "+responseDetails.getDescription());
					httpSession.setAttribute("message", responseDetails.getDescription());
					return "redirect:/notification";
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				httpSession.setAttribute("message", "An error has occured while processing your request. Please try again or" +
						"contact the administrator if it persists. ");
				return "redirect:/notification";
			}			
		}
		else
		{
			// we redirect to the login page
			httpSession.invalidate();
			return "redirect:/login";
		}
	}
	/**
	 * This method is used to retrieve the home page of the logged in user, it will show the links and the things that the user can access
	 * @param username
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String loadAuthenticatedUserDetail(Model model, HttpServletRequest request, HttpSession httpSession)
	{
		if(httpSession.getAttribute("userDetail")!=null)
		{
			
			ResponseDetails billingPlansResponse = getAvailablePlansFromWS(httpSession);
			
			if(billingPlansResponse!=null)
			{
				System.out.println("Response code: "+billingPlansResponse.getStatusCode()+", Description: "+billingPlansResponse.getDescription());
				// we get the map value from this response
				System.out.println("retrieving plans from web service Response");
		
				Object responseServicePlan = billingPlansResponse.getClassInstance();
				if(responseServicePlan!=null)
				{
					System.out.println("Object type in the response object: "+responseServicePlan.getClass().getName());
					Map<String,String> serviceplan = (Map<String, String>) responseServicePlan;
					System.out.println("MAP value: "+serviceplan);
					model.addAttribute("serviceplanList",serviceplan);
				}
			}
			
			SubscriberDetail subscriberDetail=new SubscriberDetail();
		
			model.addAttribute("subscriberdetail", subscriberDetail);
			httpSession.removeAttribute("message");
			httpSession.setAttribute("tab", "subscription");
			//return "home/home";
			return "home";
		}
		else
		{
			// we redirect to the login page
			httpSession.invalidate();
			return "redirect:/login";
		}
	}
	
	
	private ResponseDetails getAvailablePlansFromWS(HttpSession httpSession)
	{
		System.out.println("getting available plans from WS");
		
		//add a json converter so that the class that we are sending is converted to JSON for us
		//restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
		
		// we retrieve the serviceplans for this uer
		Users users = (Users)httpSession.getAttribute("userDetail");
		logger.info("user login type: "+users.getUsertype());
		
		ResponseDetails billingPlansResponse = loginRest.getAvailablePlansByType(users.getUsertype());
		
		return billingPlansResponse;
	}
	
	private ResponseDetails getAvailablePlansFromWSByType(HttpSession httpSession)
	{
		System.out.println("getting available plans from WS by type");
		
		//add a json converter so that the class that we are sending is converted to JSON for us
		//restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
		
		// we retrieve the serviceplans for this uer
		Users users = (Users)httpSession.getAttribute("userDetail");
		logger.info("user login type: "+users.getUsertype());
		ResponseDetails billingPlansResponse = loginRest.getAvailablePlansByType(users.getUsertype());
		
		return billingPlansResponse;
	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/home", method = RequestMethod.POST)
	public String saveSubscriberDetail(@Valid SubscriberDetail subscriberDetail, BindingResult bindingResult, Model model, HttpSession httpSession)
	{	
		logger.info("INFO: Saving Subscriber "+subscriberDetail.getFirstname()+" ..");
				
		/*
		if(bindingResult.hasErrors()) 
		{
			System.out.println("WARNING: binding result has errors "+bindingResult.getAllErrors().get(0).getDefaultMessage());
			
			return "";
		}
		*/
		
		ResponseDetails reponsePlans = getAvailablePlansFromWS(httpSession);
		
		if (reponsePlans != null)
		{
			Map<String, String> classInstance = (Map<String, String>)reponsePlans.getClassInstance();
			
			Iterator<String> iterator = classInstance.keySet().iterator();
			
			while(iterator.hasNext())
			{
				String tempValue = iterator.next();				
				if (tempValue.equalsIgnoreCase(subscriberDetail.getShortCode()))
				{
					logger.info("Setting the service plan for subscriber ["+subscriberDetail.getMsisdn()+"] to "+classInstance.get(tempValue));
					subscriberDetail.setServiceplan(classInstance.get(tempValue));
				}
			}
			
			//This is to make sure that the msisdn that is used is correct and in the country format 
			subscriberDetail.setMsisdn(normalizeMsisdnPrefix(subscriberDetail.getMsisdn()));
		}
		
		httpSession.setAttribute("savedSubscriberDetail", subscriberDetail);
		// model.addAttribute("subscriberDetail", subscriberDetail);
		// we redirect to the preview page
		//return "redirect:/preview";
		return "redirect:/preview";
	}
	
	
	@RequestMapping(value = "/adduser", method = RequestMethod.POST)
	public String addUserCommand(@Valid Users user, BindingResult bindingResult, Model model, HttpSession httpSession, 
			HttpServletRequest request)
	{	
		Users tempUser = new Users();
		tempUser.setPassword("");
		tempUser.setEmail("");
		tempUser.setIsdefaultpassword("YES");
		
		model.addAttribute("message", "");
		model.addAttribute("userDetail",tempUser);
		
		logger.info("INFO: Saving new user "+user.getFirstname()+" .. and Email :"+user.getEmail());
		user.setDate_created(Calendar.getInstance());
		
		Map<String, String> roles =  getAvailableRoles(request);
		
		user.setUsertype(roles.get(user.getRoleid()));
		
		// we call the url to post
		//String url = properties.getProperty("webserviceURL")+"/broker/saveuserdetail";
	    logger.info("Saving User "+user.getFirstname()+" with role :"+user.getUsertype());
		
	    // set this user to 'new' just for kicks :)
	    user.setIsdefaultpassword("YES");
		ResponseDetails responseDetails =  loginRest.saveUserDetails(user, request);
		if(responseDetails!=null)
		{
			if(responseDetails.getStatusCode()==0)
			{
				logger.info("User "+user.getFirstname()+" saved successfully...");
				
				model.addAttribute("message", responseDetails.getDescription());
				httpSession.setAttribute("tab", "admin");
				
				if(responseDetails.getClassInstance()!=null)
				{
					// we add this detail to a session savedSubscriberDetail
					System.out.println("Save Response [Status "+responseDetails.getStatusCode()+"], [Description "+responseDetails.getDescription());
				}
			}
			else
			{
				// we print an error message
				// or redirect to the error page
				logger.info("Status code: "+responseDetails.getStatusCode());
				logger.info("Description: "+responseDetails.getDescription());
				model.addAttribute("message", responseDetails.getDescription()+ "STATUS :"+responseDetails.getStatusCode());
				model.addAttribute("userDetail", user);
			}
			
		}
		return "home/add";
	}
	
	
	@RequestMapping(value = "/updateuser", method = RequestMethod.POST)
	public String updateUserCommand(@Valid Users user, BindingResult bindingResult, Model model, HttpSession httpSession, 
			HttpServletRequest request, String userId)
	{	
		//Users tempUser = new Users();
		//tempUser.setPassword("");
		//tempUser.setEmail("");
		//tempUser.setIsdefaultpassword("NO");
		//tempUser.setFirstname(userId);
		//tempUser.setLastname("");
		//tempUser.setUsertype("");
		//userId = users.getId();
		
		
		//model.addAttribute("message", "");
		//model.addAttribute("userDetail",tempUser);
		
		logger.info("INFO: Updating user "+user.getFirstname()+" .. and Email :"+user.getEmail() + " and " + user.getId());
		user.setDate_created(Calendar.getInstance());
		
		Map<String, String> roles =  getAvailableRoles(request);
		
		//user.setUsertype(roles.get(user.getRoleid()));
		
		// we call the url to post
		//String url = properties.getProperty("webserviceURL")+"/broker/saveuserdetail";
	    logger.info("Saving User "+user.getFirstname()+" with role :"+user.getUsertype());
		
	    // set this user to 'new' just for kicks :)
	   // user.setIsdefaultpassword("N");
		//ResponseDetails responseDetails =  loginRest.updateUserById(userId, request);
		ResponseDetails responseDetails =  loginRest.updateUserDetails(user, request);
		if(responseDetails!=null)
		{
			if(responseDetails.getStatusCode()==0)
			{
				logger.info("User "+user.getFirstname()+" saved successfully...");
				
				model.addAttribute("message", responseDetails.getDescription());
				httpSession.setAttribute("tab", "admin");
				
				if(responseDetails.getClassInstance()!=null)
				{
					// we add this detail to a session savedSubscriberDetail
					System.out.println("Save Response [Status "+responseDetails.getStatusCode()+"], [Description "+responseDetails.getDescription());
				}
			}
			else
			{
				// we print an error message
				// or redirect to the error page
				logger.info("Status code: "+responseDetails.getStatusCode());
				logger.info("Description: "+responseDetails.getDescription());
				model.addAttribute("message", responseDetails.getDescription()+ "STATUS :"+responseDetails.getStatusCode());
				model.addAttribute("userDetail", user);
			}
		}
		return "home/add";
	}
	
	
	private Map<String, String> getAvailableRoles(HttpServletRequest request)
	{
		String url = properties.getProperty("webserviceURL")+"/broker/getavailableroles";
	    logger.info("Loading all the roles that are available on the Broker UI");
	
		ResponseDetails reponseDetails = loginRest.getAvailableRoles(request);
		Map<String, String> roles = new LinkedHashMap<String, String>();
		
		if (reponseDetails != null)
		{
			roles = (Map<String, String>)reponseDetails.getClassInstance();
			logger.info("The roles that are on the system are "+roles);
			
		}
		return roles;
	}
	
	
	@RequestMapping(value = "/home/{userId}", method = RequestMethod.GET)
	public String loadUserDetailsByID(Model model, @PathVariable("userId") String userId, HttpSession httpSession, HttpServletRequest request)
	{
		logger.info("Service to load a user has been called..");
		if(httpSession.getAttribute("userDetail") != null)
		{
			String url = properties.getProperty("webserviceURL")+"/broker/getuserdetailbyid?searchby={searchby}";
		    logger.info("Loading all the details of matching user that are available on the Broker UI");
		    
		   @SuppressWarnings("unchecked")
		   ArrayList users = loginRest.getUserDetailById(userId, request);
		   
		   if (users.size() > 0)
		   {
			   logger.info("The value of the returned user is = "+users.get(0));
			  
			   model.addAttribute("usersDetail", LinkedHashMapUtility.convertToUserDetails((LinkedHashMap)users.get(0)));
			   
			   return "updateuser";
		   }
		   else
		   {
			   httpSession.setAttribute("message", "No user details was found matching your search. Please  ensure that you have entered the " +
			   		"valid user id.");
			   return "redirect:/notification";
		   }		  
		}
		else
		{
			httpSession.invalidate();
			return "redirect:/login";
		}
		
	}
	
	
	
	@RequestMapping(value = "/reset/{userId}", method = RequestMethod.GET)
	public String resetUserDetailsByID(Model model, @PathVariable("userId") String userId, HttpSession httpSession, HttpServletRequest request)
	{
		logger.info("Service to reset a user with ID = "+userId+" has been called..");
		if(httpSession.getAttribute("userDetail") != null)
		{
			String url = properties.getProperty("webserviceURL")+"/broker/resetuserbyid?userid={userId}";
		    logger.info("Loading all the details of matching user that are available on the Broker UI");
		    
		   @SuppressWarnings("unchecked")
		   ResponseDetails usersResponse = loginRest.resetUserById(userId, request);
		   
		   logger.info("The value of the returned user is = "+usersResponse.getStatusCode());
			  
		   httpSession.setAttribute("message", "User password has been reset successfully");
		   return "redirect:/notification";
		   		     
		}
		else
		{
			httpSession.invalidate();
			return "redirect:/login";
		}
		
	}
	
	
	
	
	@RequestMapping(value = "/delete/{userId}", method = RequestMethod.GET)
	public String deleteUserDetailsByID(Model model, @PathVariable("userId") String userId, HttpSession httpSession, HttpServletRequest request)
	{
		logger.info("Service to delete a user with ID = "+userId+" has been called..");
		if(httpSession.getAttribute("userDetail") != null)
		{
			String url = properties.getProperty("webserviceURL")+"/broker/deleteuserbyid?userid={userId}";
		    logger.info("Loading all the details of matching user that are available on the Broker UI");
		    
		   @SuppressWarnings("unchecked")
		   ResponseDetails usersResponse = loginRest.deleteUserById(userId, request);
		   
		   logger.info("The value of the returned user is = "+usersResponse.getStatusCode());
			  
		   httpSession.setAttribute("message", "User password has been reset successfully");
		   return "redirect:/notification";
		   		     
		}
		else
		{
			httpSession.invalidate();
			return "redirect:/login";
		}
		
	}
	
	
	
	
	@RequestMapping(value = "/unlock/{userId}", method = RequestMethod.GET)
	public String unlockUserDetailsByID(Model model, @PathVariable("userId") String userId, HttpSession httpSession)
	{
		logger.info("Service to load a user has been called..");
		if(httpSession.getAttribute("userDetail") != null)
		{
			String url = properties.getProperty("webserviceURL")+"/broker/unlockuserbyid?userid={userId}";
		    logger.info("Loading all the details of matching user that are available on the Broker UI");
		    
		   @SuppressWarnings("unchecked")
		   ResponseDetails usersResponse = loginRest.unlockUserById(userId);
		   
		   logger.info("The value of the returned user is = "+usersResponse.getStatusCode());
			  
		   httpSession.setAttribute("message", "User password has been reset successfully");
		   return "redirect:/notification";
		   		     
		}
		else
		{
			httpSession.invalidate();
			return "redirect:/login";
		}
		
	}
	
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/edit/{userId}", method = RequestMethod.GET)
	public String editUserDetailsByID(Model model,@PathVariable("userId") String userId, HttpSession httpSession, HttpServletRequest request)
	{
		logger.info("Service to edit a user with ID has been called..");
		if(httpSession.getAttribute("userDetail") != null)
		{
			
			
			
			//userDetail.setId(userId);
			
			logger.info("Loadinguserid on Broker UI :" +userId);
			//userDetail.getId();
			//userDetail.getUsername();
			//userDetail.getEmail();			
		    //userDetail.setPassword("");
			//userDetail.setEmail("");	
			
			ArrayList<Users> users = loginRest.getUserDetailById(userId, request);
			
			for(Users obj : users){

				   System.out.println("Username :: " + obj.getUsername());
				   System.out.println("Email :: " + obj.getEmail());
				   System.out.println("password :: " + obj.getPassword());
				   
				   model.addAttribute("userDetail", obj);
				
				}
            
			
			logger.info("The response that is received from the call to getUserDetailById service" + users.toString());
		

			ResponseDetails reponseDetails = loginRest.getAvailableRoles(request);
			Map<String, String> roles = new LinkedHashMap<String, String>();
			
			if (reponseDetails != null)
			{
				roles = (Map<String, String>)reponseDetails.getClassInstance();
				logger.info("The roles that are on the system are "+roles);
			}
						
		    
		    model.addAttribute("roles",roles);
		    
			httpSession.setAttribute("tabs", "admin");
			
			
			return "home/edit";
			
		}		
		else
		{
			httpSession.invalidate();
			return "redirect:/login";
		}		
		
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addUser(Model model, HttpServletRequest request, HttpSession httpSession)
	{
		logger.info("Service to add a new user has been called..");
		if(httpSession.getAttribute("userDetail") != null)
		{
			Users userDetail= new Users();
			userDetail.setIsdefaultpassword("YES");
			userDetail.setPassword("");
			userDetail.setEmail("");
			
			String url = properties.getProperty("webserviceURL")+"/broker/getavailableroles";
		    logger.info("Loading all the roles that are available on the Broker UI");
		
			ResponseDetails reponseDetails = loginRest.getAvailableRoles(request);
			Map<String, String> roles = new LinkedHashMap<String, String>();
			
			if (reponseDetails != null)
			{
				roles = (Map<String, String>)reponseDetails.getClassInstance();
				logger.info("The roles that are on the system are "+roles);
			}
			
			httpSession.removeAttribute("message");//
			
			model.addAttribute("userDetail", userDetail);
			model.addAttribute("roles",roles);
			
			httpSession.setAttribute("tab", "admin");
			
			return "home/add";
		}
		else
		{
			httpSession.invalidate();
			return "redirect:/login";
		}
	}
	
	private String normalizeMsisdnPrefix(String msisdn){
		String Msisdn = msisdn;
		String countryCode = properties.getProperty("countryCode");
		if (msisdn.startsWith("0")){
			return countryCode+Msisdn;
		}
		else if(Msisdn.startsWith("+"+countryCode)){
			return Msisdn.substring(1, Msisdn.length());
		}
		else return Msisdn;
	}
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	    binder.registerCustomEditor(GregorianCalendar.class, new CustomDateEditor(
	            dateFormat, false));
	}
	
}
