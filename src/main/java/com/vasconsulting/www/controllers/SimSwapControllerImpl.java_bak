/**
 * 
 */
package com.vasconsulting.www.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.vasconsulting.www.domain.ResponseDetails;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.Users;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.LoginRestControllerUtility;

/**
 * @author Nnamdi
 *
 */
@Controller
public class SimSwapControllerImpl 
{
	private Logger logger = Logger.getLogger(SimSwapControllerImpl.class);
	//private RestTemplate restTemplate = new RestTemplate();
	private LoadAllProperties properties = new LoadAllProperties();
	@Autowired
	public void setLoginRest(LoginRestControllerUtility loginRest) {
		this.loginRest = loginRest;
	}


	private LoginRestControllerUtility loginRest;
	
	
	private ResponseDetails getAvailablePlansFromWSByType(HttpSession httpSession)
	{
		System.out.println("Getting all plans by billing plans by type... ");
		
		//add a json converter so that the class that we are sending is converted to JSON for us
		//restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
		
		// we retrieve the serviceplans for this uer
		Users users = (Users)httpSession.getAttribute("userDetail");
		logger.info("user login type: "+users.getUsertype());
		//ResponseDetails billingPlansResponse = restTemplate.getForObject(url, ResponseDetails.class, users.getUsertype());
		ResponseDetails billingPlansResponse = loginRest.getAvailablePlansByType(users.getUsertype());
		
		return billingPlansResponse;
	}
	
	@RequestMapping(value = "/activateaction", method = RequestMethod.POST)
	public String processActivation(HttpSession httpSession, Model model)
	{
		logger.info("posting Activate request");
		logger.info("getting Subscriber Detail from session..");
		SubscriberDetail subdetail = (SubscriberDetail)httpSession.getAttribute("subscriberDetail");
		if(subdetail!=null)
		{
			subdetail.setStatus("ACTIVE");
			String url = properties.getProperty("webserviceURL")+"/broker/activatesubscriber";
			logger.info("activate subscriber webservice call: "+url);			
			
			//restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
			try
			{
				logger.info("Saving subscriber detail : "+subdetail);
				
				ResponseDetails billingPlansResponse = getAvailablePlansFromWSByType(httpSession);
				
				//this is the left part of the URL that will be called. It defaults to the Prepaid Version
				String requestType = "savesubscriberdetail";
				
				if(billingPlansResponse != null)
				{
					logger.info("Response code: "+billingPlansResponse.getStatusCode()+", Description: "+billingPlansResponse.getDescription());
					// we get the map value from this response
					logger.info("retrieving plans BY type from web service Response");
			
					Object responseServicePlan = billingPlansResponse.getClassInstance();
					if(responseServicePlan != null)
					{
						Map<String,String> serviceplan = (Map<String, String>) responseServicePlan;
						
						Iterator<String> iterator = serviceplan.keySet().iterator();
						
						while(iterator.hasNext())
						{
							String tempValue = iterator.next();				
							if (tempValue.equalsIgnoreCase(subdetail.getShortCode()))
							{
								
								if (!serviceplan.get(tempValue).equalsIgnoreCase("PREP"))
								{
									logger.info("This = "+serviceplan.get(tempValue));
									requestType = "savesubscriberdetailpost";
									break;
								}
								
							}
						}
					}
				}
				
				ResponseDetails responseDetails;
				if (requestType.equalsIgnoreCase("savesubscriberdetailpost"))
				{
					responseDetails = loginRest.saveSubscriberDetailsPost((SubscriberDetail)httpSession.getAttribute("subscriberDetail"));
				}
				else
				{
					responseDetails = loginRest.saveSubscriberDetails((SubscriberDetail)httpSession.getAttribute("subscriberDetail"));
				}
				//ResponseDetails responseDetails =  restTemplate.postForObject(properties.getProperty("webserviceURL")+"/broker/"+requestType, subdetail, ResponseDetails.class);
				if(responseDetails != null)
				{
					if(responseDetails.getStatusCode() == 0)
					{
						System.out.println("Subscriber detail saved successfully... ");
						if(responseDetails.getClassInstance()!=null)
						{
							// we add this detail to a session savedSubscriberDetail
							System.out.println("Save Response [Status "+responseDetails.getStatusCode()+"], [Description "+responseDetails.getDescription());
						}
						httpSession.setAttribute("message", responseDetails.getDescription());
						return "redirect:/notification";
					}
					else
					{
						// we print an error message
						// or redirect to the error page
						logger.info("Status code: "+responseDetails.getStatusCode());
						logger.info("Description: "+responseDetails.getDescription());
						httpSession.setAttribute("message", properties.getProperty(new Integer(responseDetails.getStatusCode()).toString()));
						return "redirect:/notification";
					}
				}
				else
				{
					//logger.info("Status code: "+responseDetails.getStatusCode());
					logger.info("Description: "+responseDetails.getDescription());
					httpSession.setAttribute("message", responseDetails.getDescription());
					return "redirect:/notification";
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				httpSession.setAttribute("message", "An error has occured while processing your request. Please try again or" +
						"contact the administrator if it persists. ");
				return "redirect:/notification";
			}			
		}
		else
		{
			httpSession.invalidate();
			return "redirect:/login";
		}
		//model.addAttribute("subscriberdetail", subdetail);
		//return "home/detail";
	}
	
	
	@RequestMapping(value = "/deactivateaction", method = RequestMethod.POST)
	public String processDeActivation(HttpSession httpSession, Model model)
	{
		if(httpSession.getAttribute("subscriberDetail")!=null)
		{
			logger.info("posting De-Activate request");
			logger.info("getting Subscriber Detail from session..");
			SubscriberDetail subdetail = (SubscriberDetail)httpSession.getAttribute("subscriberDetail");
			
			ResponseDetails responseDetails =  loginRest.deactivateSubscriber(subdetail);
			
			
			if (responseDetails != null)
			{
				httpSession.setAttribute("message", responseDetails.getDescription());
				return "home/notification";
			}
			else
			{
				httpSession.setAttribute("message", "There was an error while process your reqiest. Please contact the administrator if the issue persists");
				return "home/notification";
			}
						
		}
		else
		{
			httpSession.invalidate();
			return "redirect:/login";
		}
	}
	
	
	@RequestMapping(value = "/changeserviceaction", method = RequestMethod.POST)
	public String processChangeService(@Valid SubscriberDetail subscriberDetail, HttpSession httpSession, Model model)
	{
		logger.info("posting Change service request");
		logger.info("getting Subscriber Detail from request and storing in Session..");
		
		httpSession.setAttribute("subscriberDetail", subscriberDetail);
		return "redirect:/home/preview";
	}
	
	
		@RequestMapping(value = "/processsimswap", method = RequestMethod.POST)
		public String processSubscriberDetail(@RequestParam("new_imsi") String new_imsi, HttpSession httpSession)
		{
			if(httpSession.getAttribute("userDetail")!=null)
			{			
				SubscriberDetail subdetail = (SubscriberDetail)httpSession.getAttribute("subscriberDetail");				
				logger.info("Request to perform sim swap for MSISDN ["+subdetail.getMsisdn()+"] has been received");
				
				logger.debug("processing simswap for subscriber detail with imsi "+new_imsi+"...");
				
				//String url = 
				//String url = properties.getProperty("webserviceURL")+"/broker/simswap?oldImsi={oldImsi}&newImsi={newImsi}";
				//String url = properties.getProperty("webserviceURL")+"/broker/simswap?oldImsi="+subdetail.getImsi()+"&newImsi="+new_imsi+"";
				//logger.info("sim swap webservice call: "+url);
				
				ResponseDetails responseDetail = loginRest.modifyBBSubscriberBillingIdentifier(subdetail.getImsi(), new_imsi);
				
				if(responseDetail != null)
				{
					if(responseDetail.getStatusCode() == 0)
					{
						System.out.println("Subscriber simswap saved successfully... ");
						if(responseDetail.getClassInstance()!=null)
						{
							// we add this detail to a session savedSubscriberDetail
							System.out.println("Save Response [Status "+responseDetail.getStatusCode()+"], [Description "+responseDetail.getDescription());
						}
						httpSession.setAttribute("message", responseDetail.getDescription());
						return "redirect:/notification";
					}
					else
					{
						// we print an error message
						// or redirect to the error page
						logger.info("Status code: "+responseDetail.getStatusCode());
						logger.info("Description: "+responseDetail.getDescription());
						httpSession.setAttribute("message", properties.getProperty(new Integer(responseDetail.getStatusCode()).toString()));
						return "redirect:/notification";
					}
				}
				else
				{
					//logger.info("Status code: "+responseDetails.getStatusCode());
					logger.info("Description: "+responseDetail.getDescription());
					httpSession.setAttribute("message", responseDetail.getDescription());
					return "redirect:/notification";
				}
			}
			else
			{
				httpSession.invalidate();
				return "redirect:/login";
			}
		}
		
		
		@RequestMapping(value = "/processdevice", method = RequestMethod.POST)
		public String processDeviceChange(HttpSession httpSession, Model model, HttpServletRequest request)
		{
			logger.info("processing device change..");
			if(httpSession.getAttribute("subscriberDetail") != null)
			{
				SubscriberDetail subscriberDetail = (SubscriberDetail)httpSession.getAttribute("subscriberDetail");
				logger.info("device change for subscriber "+subscriberDetail.getMsisdn()+" with IMIS "+subscriberDetail.getImsi()+" initiated..");
				//String url = properties.getProperty("webserviceURL")+"/broker/devicechange";
				
				subscriberDetail.setPin(request.getParameter("new_pin"));
				subscriberDetail.setImei(request.getParameter("new_imei"));
				
				
				loginRest.modifyBBSubscriberNonBillingIdentifier(subscriberDetail);
				httpSession.setAttribute("message", "Device change was successful");
				return "redirect:/notification";
			}
			else
			{
				logger.info("no subscriber information saved in session..exiting..");
				
				SubscriberDetail subDetail = new SubscriberDetail();
				model.addAttribute("subscriberdetail", subDetail);
				
				return "home/home";
			}
			
		}
		
		
		@RequestMapping(value = "/simswap", method = RequestMethod.GET)
		public String loadSimSwap(Model model, HttpServletRequest request, HttpSession httpSession)
		{
			if(httpSession.getAttribute("userDetail")!=null)
			{
				// we get the subscriber detail from the session
				if(httpSession.getAttribute("subscriberDetail") != null)
				{
					SubscriberDetail subscriberDetail = (SubscriberDetail)httpSession.getAttribute("subscriberDetail");
					logger.info("subscriber detail "+subscriberDetail.getImsi());
					model.addAttribute("subscriberdetail", subscriberDetail);
				
					return "home/simswap";
				}
				else
				{
					logger.info("no subscriber information saved in session..exiting..");
					
					SubscriberDetail subDetail = new SubscriberDetail();
					model.addAttribute("subscriberdetail", subDetail);
					
					return "home/home";
				}
			}
			else
			{
				// we redirect to the login page
				httpSession.invalidate();
				return "redirect:/login";
			}
		}
		@RequestMapping(value = "/devicechange", method = RequestMethod.GET)
		public String loadDeviceChange(Model model, HttpServletRequest request, HttpSession httpSession)
		{
			if(httpSession.getAttribute("userDetail")!=null)
			{
				// we get the subscriber detail from the session
				if(httpSession.getAttribute("subscriberDetail")!=null)
				{
					SubscriberDetail subscriberDetail = (SubscriberDetail)httpSession.getAttribute("subscriberDetail");
					logger.info("subscriber detail "+subscriberDetail.getImei());
					model.addAttribute("subscriberDetail", subscriberDetail);
				
					return "home/devicechange";
				}
				else
				{
					SubscriberDetail subDetail = new SubscriberDetail();
					model.addAttribute("subscriberDetail", subDetail);
					
					return "home/home";
				}
			}
			else
			{
				logger.info("no user session detected..login please");
				httpSession.invalidate();
				// we redirect to the login page
				return "redirect:/login";
			}
		}
		@RequestMapping(value = "/deactivate", method = RequestMethod.GET)
		public String loadDeActivateSubscriber(Model model, HttpServletRequest request, HttpSession httpSession)
		{
			if(httpSession.getAttribute("userDetail")!=null)
			{
				// we get the subscriber detail from the session
				if(httpSession.getAttribute("subscriberDetail")!=null)
				{
					logger.info("subscriber detail "+httpSession.getAttribute("subscriberDetail"));
					model.addAttribute("subscriberdetail", httpSession.getAttribute("subscriberDetail"));
				
					return "home/deactivate";
				}
				else
				{
					SubscriberDetail subDetail = new SubscriberDetail();
					model.addAttribute("subscriberdetail", subDetail);
					
					return "home/home";
				}
			}
			else
			{
				logger.info("no user session detected..login please");
				// we redirect to the login page
				httpSession.invalidate();
				return "redirect:/login";
			}
		}
		
		
		@RequestMapping(value = "/changeservice", method = RequestMethod.GET)
		public String loadChangeService(Model model, HttpServletRequest request, HttpSession httpSession)
		{
			if(httpSession.getAttribute("userDetail")!=null)
			{
				// we get the subscriber detail from the session
				if(httpSession.getAttribute("subscriberDetail")!=null)
				{
					SubscriberDetail subscriberDetail = (SubscriberDetail)httpSession.getAttribute("subscriberDetail");
					logger.info("subscriber detail "+subscriberDetail.getImei());
					model.addAttribute("subscriberdetail", subscriberDetail);
				
					//String url = properties.getProperty("webserviceURL")+"/broker/getavailableplans?searchby={searchby}";
					// we also get the serviceplans
					logger.info("retrieving service plans...");
				
					//restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
					
					// we retrieve the serviceplans for this user
					Users users = (Users)httpSession.getAttribute("userDetail");
					logger.info("user login type: "+users.getUsertype());
					ResponseDetails billingPlansResponse = loginRest.getAvailablePlans(users.getUsertype());
					if(billingPlansResponse!=null)
					{
						System.out.println("Response code: "+billingPlansResponse.getStatusCode()+", Description: "+billingPlansResponse.getDescription());
						// we get the map value from this response
						System.out.println("retrieving plans from web service Response");
				
						Object responseServicePlan = billingPlansResponse.getClassInstance();
						if(responseServicePlan!=null)
						{
							System.out.println("Object type in the response object: "+responseServicePlan.getClass().getName());
							Map<String,String> serviceplan = (Map<String, String>) responseServicePlan;
							System.out.println("MAP value: "+serviceplan);
							model.addAttribute("serviceplanList",serviceplan);
						}
					}
					return "home/changeservice";
				}
				else
				{
					SubscriberDetail subDetail = new SubscriberDetail();
					model.addAttribute("subscriberdetail", subDetail);
					
					return "home/home";
				}
			}
			else
			{
				logger.info("no user session detected..login please");
				httpSession.invalidate();
				// we redirect to the login page
				return "redirect:/login";
			}
		}
		
		@RequestMapping(value = "/changeservice", method = RequestMethod.POST)
		public String changeService(@RequestParam("shortCode") String shortcode, Model model, HttpSession httpSession)
		{
			if(httpSession.getAttribute("userDetail") != null)
			{
				SubscriberDetail subdetail = (SubscriberDetail)httpSession.getAttribute("subscriberDetail");
				
				subdetail.setStatus("Active");
				subdetail.setShortCode(shortcode);
				String url = properties.getProperty("webserviceURL")+"/broker/activatesubscriber";
				logger.info("activate subscriber webservice call: "+url);			
				
				//restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
				try
				{
					logger.info("Changing subscriber detail "+subdetail.getFirstname()+" with plan :"+subdetail.getShortCode());
					
					ResponseDetails billingPlansResponse = getAvailablePlansFromWSByType(httpSession);
					
					//this is the left part of the URL that will be called. It defaults to the Prepaid Version
					String requestType = "savesubscriberdetail";
					
					if(billingPlansResponse != null)
					{
						logger.info("Response code: "+billingPlansResponse.getStatusCode()+", Description: "+billingPlansResponse.getDescription());
						// we get the map value from this response
						logger.info("retrieving plans BY type from web service Response");
				
						Object responseServicePlan = billingPlansResponse.getClassInstance();
						if(responseServicePlan != null)
						{
							Map<String,String> serviceplan = (Map<String, String>) responseServicePlan;
							logger.info("Value of serviceplan"+responseServicePlan);
							Iterator<String> iterator = serviceplan.keySet().iterator();
							
							while(iterator.hasNext())
							{
								String tempValue = iterator.next();		
								
								if (tempValue.equalsIgnoreCase(subdetail.getShortCode()))
								{
									logger.info("Value of tempValue ="+tempValue);
									if (!serviceplan.get(tempValue).startsWith("Prep"))
									{
										requestType = "savesubscriberdetailpost";
										break;
									}
									
								}
							}
						}
					}
					
					ResponseDetails responseDetails;
					if (requestType.equalsIgnoreCase("savesubscriberdetailpost"))
					{
						responseDetails = loginRest.saveSubscriberDetailsPost(subdetail);
					}
					else
					{
						responseDetails = loginRest.saveSubscriberDetails(subdetail);
					}
					//ResponseDetails responseDetails =  restTemplate.postForObject(properties.getProperty("webserviceURL")+"/broker/"+requestType, subdetail, ResponseDetails.class);
					if(responseDetails != null)
					{
						if(responseDetails.getStatusCode() == 0)
						{
							System.out.println("Subscriber detail saved successfully... ");
							if(responseDetails.getClassInstance()!=null)
							{
								// we add this detail to a session savedSubscriberDetail
								System.out.println("Save Response [Status "+responseDetails.getStatusCode()+"], [Description "+responseDetails.getDescription());
							}
							httpSession.setAttribute("message", responseDetails.getDescription());
							return "redirect:/notification";
						}
						else
						{
							// we print an error message
							// or redirect to the error page
							logger.info("Status code: "+responseDetails.getStatusCode());
							logger.info("Description: "+responseDetails.getDescription());
							httpSession.setAttribute("message", properties.getProperty(new Integer(responseDetails.getStatusCode()).toString()));
							return "redirect:/notification";
						}
					}
					else
					{
						//logger.info("Status code: "+responseDetails.getStatusCode());
						logger.info("Description: "+responseDetails.getDescription());
						httpSession.setAttribute("message", responseDetails.getDescription());
						return "redirect:/notification";
					}
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
					httpSession.setAttribute("message", "An error has occured while processing your request. Please try again or" +
							"contact the administrator if it persists. ");
					return "redirect:/notification";
				}			
			}
			else
			{
				logger.info("no user session detected..login please");
				httpSession.invalidate();
				// we redirect to the login page
				return "redirect:/login";
			}
		}
		
		
		@RequestMapping(value = "/activate", method = RequestMethod.GET)
		public String loadActivateSubscriber(Model model, HttpServletRequest request, HttpSession httpSession)
		{
			if(httpSession.getAttribute("userDetail")!=null)
			{
				// we get the subscriber detail from the session
				if(httpSession.getAttribute("subscriberDetail")!=null)
				{
					SubscriberDetail subscriberDetail = (SubscriberDetail)httpSession.getAttribute("subscriberDetail");
					logger.info("subscriber detail "+subscriberDetail);
					model.addAttribute("subscriberDetail", subscriberDetail);
				
					return "home/activate";
				}
				else
				{
					SubscriberDetail subDetail = new SubscriberDetail();
					model.addAttribute("subscriberDetail", subDetail);
					
					return "home/home";
				}
			}
			else
			{
				logger.info("no user session detected..login please");
				httpSession.invalidate();
				// we redirect to the login page
				return "redirect:/login";
			}
		}
		
		
	// this is posted by the search form when a search occurs
	@RequestMapping(value = "/getdetail", method = RequestMethod.POST)
	public String searchSubscriberDetail(@RequestParam("search_type") String search_type, @RequestParam("search_value") String search_value,
			Model model, HttpSession httpSession)
	{
		String _search_type=search_type , _search_value = search_value;
		System.out.println("search type: "+search_type+", searchvalue :"+_search_value);
		logger.debug("in getdetail request");

		Map<String, String> varS = new HashMap<String, String>();
		varS.put("search_type",_search_type);
		varS.put("search_value", _search_value);
		//restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
		
		SubscriberDetail responseDetails = loginRest.getSubscriberByType(search_type, search_value);
		
		logger.info("Response Detail:="+responseDetails.getId()+";");
		if(null != responseDetails.getMsisdn())
		{
			logger.info("Response Detail: "+responseDetails);
				
			logger.info("subcriber detail: "+responseDetails);
			logger.info("adding this subscriber to the session");
					
			httpSession.setAttribute("subscriberDetail", responseDetails);
			model.addAttribute("subscriberDetail",responseDetails);	
		}	
		else
		{
			httpSession.setAttribute("message", "No results found for "+_search_value+". Please check the entered value and try again.");
			return "home/notification";	
		}
		return "home/detail";
		/*if(httpSession.getAttribute("userDetail") != null)
		{
			
		}
		else
		{
			logger.info("no user session detected..login please");
			httpSession.invalidate();
			// we redirect to the login page
			return "redirect:/login";
		}*/
	}
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	    binder.registerCustomEditor(GregorianCalendar.class, new CustomDateEditor(
	            dateFormat, false));
	}
}
