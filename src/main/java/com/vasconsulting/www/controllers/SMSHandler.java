package com.vasconsulting.www.controllers;

import java.io.IOException;

import net.rubyeye.xmemcached.MemcachedClient;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import sun.misc.BASE64Decoder;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.RequestTracker;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BrokerService;
import com.vasconsulting.www.utility.LoadAllBillingPlanObjects;

@SuppressWarnings("restriction")
@Controller
public class SMSHandler
{

	private HibernateUtility hibernateUtility;
	private int provisionStatus = -1;
	@Autowired
	private BrokerService brokerService;
	@Autowired
	private TransactionLog transactionlog;
	@Autowired
	private MemcachedClient memcachedClient;

	@Autowired
	public void setHibernateUtility(HibernateUtility hibernateUtility)
	{
		this.hibernateUtility = hibernateUtility;
	}
	
	@Autowired
	public void setBrokerService(BrokerService brokerService) {
		this.brokerService = brokerService;
	}

	Logger logger = Logger.getLogger(SMSHandler.class);

	private String stripLeadingMsisdnPrefix(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("+")){
			return Msisdn.substring(1, Msisdn.length());
		}
		else return msisdn;
	}

	public int provisionMethod(String msisdn, String msg){
		logger.info("In the provision method the details are "+msisdn+"::"+msg);
		saveRequest(msisdn, msg);
		SubscriberDetail subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn(stripLeadingMsisdnPrefix(msisdn));
		subscriberDetail.setShortCode(msg);
		LoadAllBillingPlanObjects loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
				.getBeans("loadAllBillingPlanObjects");
		brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		provisionStatus = brokerService.activate(msg, subscriberDetail);
		return provisionStatus;
	}
	

	@RequestMapping(value="/smsservice", method = RequestMethod.GET)
	public void provisionSubscriber(@RequestParam("msisdn") String msisdn, @RequestParam("msg") String msg) throws IOException
	{
		logger.info("The request is sent in from msisdn = "+msisdn);
		provisionMethod(msisdn, msg);
	}

	@RequestMapping(value="/mamoservice", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> provisionMamoSubscriber(@RequestParam("msisdn") String msisdn, @RequestParam("msg") String msg) throws IOException
	{
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setContentType(MediaType.TEXT_PLAIN);
		return new ResponseEntity<String>(String.valueOf(provisionMethod(msisdn, msg)), responseHeaders, HttpStatus.OK);
	}

	@RequestMapping(value="/smppussdservice", method = RequestMethod.GET)
	public void provisionSMPPSubscriber(@RequestParam("msisdn") String msisdn, @RequestParam("msg") String msg)
	{
		try {
			String subscriber = msisdn;
			String msgValue = "";

			logger.info("Raw SMS from Kannel to process, supplied details are MSISDN ["+subscriber+"] and " +
					"SMS message ["+msg+"]");

			String [] requestTemp = msg.split("\\|");

			for (String requestTempValue : requestTemp)
			{
				if (requestTempValue.startsWith("msg:"))
				{
					//msg:c3RhdHVz
					msgValue = requestTempValue.substring(4);				
				}
			}
			logger.info("Substring returned from match is = "+msgValue);		
			String message = new String(new BASE64Decoder().decodeBuffer(msgValue));
			provisionMethod(msisdn, message);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void saveRequest(String msisdn, String msg){
		RequestTracker request = new RequestTracker();
		request.setMsisdn(msisdn);
		request.setShortCode(msg.toLowerCase());
		try{
			hibernateUtility.saveRequestTracker(request);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}
	

}
