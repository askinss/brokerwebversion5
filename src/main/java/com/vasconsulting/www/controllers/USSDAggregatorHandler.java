package com.vasconsulting.www.controllers;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.RequestTracker;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.CommandFactory;
import com.vasconsulting.www.invokers.CommandInvoker;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.CommandPropertiesUtility;
import com.vasconsulting.www.utility.InvokeExternalURLCommandImpl;
import com.vasconsulting.www.utility.LoadAllBillingPlanObjects;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.StatusCodes;
import com.vasconsulting.www.utility.SubscriberUSSDConversation;
import com.vasconsulting.www.utility.XMLUtility;

@Controller
public class USSDAggregatorHandler
{
	
	private LoadAllBillingPlanObjects billingPlanObjects;
	private CommandInvoker commandInvoker;
	private CommandFactory commandFactory;
	private XMLUtility xmlUtility;
	private HibernateUtility hibernateUtility;
	//private GregorianCalendar calendar = new GregorianCalendar();
	private InvokeExternalURLCommandImpl urlInvoker = new InvokeExternalURLCommandImpl();
	Logger logger = Logger.getLogger(USSDAggregatorHandler.class);
	
	SubscriberUSSDConversation subscriberInSession;
	private LoadAllProperties properties;
	
	@Autowired
	public void setProperties(LoadAllProperties properties)
	{
		this.properties = properties;
	}
	@Autowired
	public void setXmlUtility(XMLUtility xmlUtility)
	{
		this.xmlUtility = xmlUtility;
	}

	@Autowired
	public void setCommandFactory(CommandFactory commandFactory)
	{
		this.commandFactory = commandFactory;
	}
		
	@Autowired
	public void setCommandInvoker(CommandInvoker commandInvoker)
	{
		this.commandInvoker = commandInvoker;
	}

	@Autowired
	public void setBillingPlanObjects(LoadAllBillingPlanObjects billingPlanObjects)
	{
		this.billingPlanObjects = billingPlanObjects;
	}
	
	private GregorianCalendar getNextSubscriptionDate(int noOfDays){
		GregorianCalendar calendar1 = new GregorianCalendar();
		calendar1.add(GregorianCalendar.DAY_OF_MONTH, new Integer(noOfDays).intValue());
		return calendar1;
	}
	
	/**
	 * This method is used to get the last day of the month.
	 * @param noOfDays
	 * @return
	 */
	private GregorianCalendar getLastDateOfMonth(){
		int year = Calendar.getInstance().get(Calendar.YEAR);
		int month = Calendar.getInstance().get(Calendar.MONTH);
		int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
		
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.set(year, month, day);		
		return calendar;
	}
	
	
	@RequestMapping(value="/ussdpostservice", method = RequestMethod.POST)
	@ResponseBody
	public void generateSubscriberUSSDMenu(@RequestBody String request, Writer writer, HttpSession session) throws IOException
	{
		logger.info("Raw XML dump for request = "+request);
		HashMap<String , String> ussdRequestMap = xmlUtility.getHomiscoUSSDXmlAsMap(request);//Homisco	
		
		String msisdn = ussdRequestMap.get("MOBILE_NUMBER");//Homisco	
		String responseFromFileText = "";	
		String msg;
		
		/*if (ussdRequestMap.containsKey("USSD_BODY"))
			msg = ussdRequestMap.get("USSD_BODY");
		else msg = ussdRequestMap.get("SERVICE_KEY");*/
		
		if (session.getAttribute("subscriberData") != null)
			subscriberInSession = (SubscriberUSSDConversation)session.getAttribute("subscriberData");
		else 
			subscriberInSession = new SubscriberUSSDConversation();
		
		if (ussdRequestMap.containsKey("SERVICE_KEY") && !ussdRequestMap.containsKey("USSD_BODY"))
		{
			msg = ussdRequestMap.get("SERVICE_KEY");
		}
		else
		{
			logger.info("subscriberInSession.isMsisdnInContainer(msisdn) = "+subscriberInSession.isMsisdnInContainer(msisdn));
			logger.info("subscriberInSession.getMsisdnFromContainer(msisdn) = "+subscriberInSession.getMsisdnFromContainer(msisdn));
			if (ussdRequestMap.get("USSD_BODY").equalsIgnoreCase("1") && !subscriberInSession.isMsisdnInContainer(msisdn))
			{
				msg = ussdRequestMap.get("SERVICE_KEY");
			}
			else if (ussdRequestMap.get("USSD_BODY").equalsIgnoreCase("1") && subscriberInSession.isMsisdnInContainer(msisdn))
			{
				msg = ussdRequestMap.get("USSD_BODY");
			}
			else if(!ussdRequestMap.get("USSD_BODY").equalsIgnoreCase("1") && subscriberInSession.isMsisdnInContainer(msisdn))
			{
				logger.info("setting msg to ussdRequestMap.get(USSD_BODY) = "+ussdRequestMap.get("USSD_BODY"));
				msg = ussdRequestMap.get("USSD_BODY");
			}
			else if(!ussdRequestMap.get("USSD_BODY").equalsIgnoreCase("1") && !subscriberInSession.isMsisdnInContainer(msisdn))
			{
				logger.info("setting msg to ussdRequestMap.get(SERVICE_KEY) = "+ussdRequestMap.get("SERVICE_KEY")+", " +
						"subscriberInSession.isMsisdnInContainer(msisdn) = false");
				msg = ussdRequestMap.get("SERVICE_KEY");
			}
			else {
				logger.info("Else block is executed");
				msg = ussdRequestMap.get("SERVICE_KEY");
			}
		}
			
		logger.info("New USSD message received to process : "+ussdRequestMap);
		
		if (msg.equalsIgnoreCase(properties.getProperty("ussdmenurootmenukeyword")))
		{
			HashMap<String, ArrayList<String>> responseFromConfFile = 
				xmlUtility.getUSSDMessage(xmlUtility.getUSSDConfigRootTag().get("root"));
			
			Iterator<String> iterator = responseFromConfFile.keySet().iterator();
			if (iterator.hasNext())
				responseFromFileText = iterator.next();
			
			if (!responseFromFileText.equalsIgnoreCase("LEAFNODE"))
			{
				subscriberInSession.saveInContainer(msisdn, responseFromConfFile.get(responseFromFileText));
				logger.info("Total number of subscribers accessing the USSD Menu is = "+subscriberInSession.getCurrentSizeOfContainer());
				session.setAttribute("subscriberData", subscriberInSession);
				//writer.write(xmlUtility.generateHomiscoResponse("1", responseFromFileText, "REQUEST", "FALSE"));
				writer.write(xmlUtility.generateHomiscoResponse("1", ussdRequestMap.get("SEQUENCE"),responseFromFileText, "REQUEST", "FALSE"));
			}
			else
			{				
				String [] paramValues = responseFromFileText.split(":sep:");
				
				
				subscriberInSession.removeFromContainer(msisdn);
				session.setAttribute("subscriberData", subscriberInSession);
				
				writer.write(xmlUtility.generateHomiscoResponse(ussdRequestMap.get("SESSION_ID"),ussdRequestMap.get("SEQUENCE"), 
						responseFromFileText, "RESPONSE", "FALSE"));
				/*writer.write(xmlUtility.generateHomiscoResponse(ussdRequestMap.get("SESSION_ID"), responseFromFileText, 
						ussdRequestMap.get("SEQUENCE"), "RESPONSE"));*/
				
				//TODO:1000 implement logic to call the ref portion of the xml.
			}		
		}
		else
		{
			if (msisdn.length() == 9) msisdn = "243"+msisdn;
			
			ArrayList<String> sessionState = subscriberInSession.getMsisdnFromContainer(msisdn);
			
			if (new Integer(msg) > sessionState.size())
			{
				subscriberInSession.removeFromContainer(msisdn);
				session.setAttribute("subscriberData", subscriberInSession);
				
				writer.write(xmlUtility.generateHomiscoResponse(ussdRequestMap.get("SESSION_ID"), ussdRequestMap.get("SEQUENCE"),  
						"Invalid Selection. Please start again", "RESPONSE", "FALSE"));
				/*writer.write(xmlUtility.generateHomiscoResponse(ussdRequestMap.get("SEQUENCE"),  
						"Invalid Selection. Please start again", "RESPONSE", "FALSE"));*/
				return;
			}
			
			responseFromFileText = sessionState.get((new Integer(msg).intValue()) - 1);
			
			HashMap<String, ArrayList<String>> responseFromConfFile = xmlUtility.getUSSDMessage(responseFromFileText);
			
			Iterator<String> iterator = responseFromConfFile.keySet().iterator();
			if (iterator.hasNext())
				responseFromFileText = iterator.next();
			
			if (!responseFromFileText.equalsIgnoreCase("LEAFNODE"))
			{
				subscriberInSession.saveInContainer(msisdn, responseFromConfFile.get(responseFromFileText));
				logger.info("Total number of subscribers accessing the USSD Menu is = "+subscriberInSession.getCurrentSizeOfContainer());
				session.setAttribute("subscriberData", subscriberInSession);
				writer.write(xmlUtility.generateHomiscoResponse(ussdRequestMap.get("SESSION_ID"), 
						ussdRequestMap.get("SEQUENCE"), responseFromFileText,"REQUEST", "FALSE"));
				/*writer.write(xmlUtility.generateHomiscoResponse(ussdRequestMap.get("SEQUENCE"), responseFromFileText, 
						"REQUEST", "FALSE"));*/
			}
			else
			{
				//TODO:1000 implement logic to call the ref portion of the xml.
				ArrayList<String> responseFromConfFileFinal = responseFromConfFile.get(responseFromFileText);
				
				responseFromFileText = responseFromConfFileFinal.get(0);
				
				String [] paramValues = responseFromFileText.split(":sep:");
				
				subscriberInSession.removeFromContainer(msisdn);
				session.setAttribute("subscriberData", subscriberInSession);
				
				urlInvoker.execute(paramValues[1], msisdn, paramValues[2], paramValues[3]);				
				
				//writer.write(xmlUtility.generateHomiscoResponse(ussdRequestMap.get("SESSION_ID"), responseFromFileText, "RESPONSE"));
				writer.write(xmlUtility.generateHomiscoResponse(ussdRequestMap.get("SESSION_ID"), ussdRequestMap.get("SEQUENCE"),  
						paramValues[3], "RESPONSE", "TRUE"));
				/*writer.write(xmlUtility.generateHomiscoResponse(ussdRequestMap.get("SEQUENCE"),  
						paramValues[3], "RESPONSE", "TRUE"));*/
			}
		}
	}
	
	
	@RequestMapping(value="/ussdxmlgetservice", method = RequestMethod.GET)
	public void provisionSubscriber(@RequestParam("msisdn") String msisdn, @RequestParam("msg") String msg, HttpServletResponse response)
	{
        RequestTracker request = new RequestTracker();
        request.setMsisdn(msisdn);
        request.setShortCode(msg.toLowerCase());
        TransactionLog transactionlog = new TransactionLog();
		transactionlog.setShortcode(msg);
		transactionlog.setMsisdn(msisdn);
		transactionlog.setDescription("SHORTCODE");
		transactionlog.setDate_created(new GregorianCalendar());
        try{
                hibernateUtility.saveRequestTracker(request);
        }
        catch(Exception ex){
                ex.printStackTrace();
        }
		ArrayList<BillingPlanObjectUtility> billingPlans = billingPlanObjects.getAllBillingPlans();
		
		int provisionStatus = -1;
		
		for (BillingPlanObjectUtility billingPlanObjectUtility : billingPlans) {
			
			if (billingPlanObjectUtility.getShortCode().trim().equalsIgnoreCase(msg.trim()))
			{
				logger.info("Billing Plan match found for shortcode : "+msg+" from subscriber "+msisdn);
				logger.info("=====Setting up the subscriber and the selected plan in Session=====");
				
				BillingPlanObjectUtility billingPlanObjectLive = (BillingPlanObjectUtility)ContextLoaderImpl.getBeans("billingPlanObject");				
				billingPlanObjectLive.setCost(billingPlanObjectUtility.getCost());
				billingPlanObjectLive.setStatus(billingPlanObjectUtility.getStatus());
				billingPlanObjectLive.setDescription(billingPlanObjectUtility.getDescription());
				billingPlanObjectLive.setExternalData(billingPlanObjectUtility.getExternalData());
				billingPlanObjectLive.setShortCode(billingPlanObjectUtility.getShortCode());
				billingPlanObjectLive.setSuccessMessage(billingPlanObjectUtility.getSuccessMessage());
				billingPlanObjectLive.setValidity(billingPlanObjectUtility.getValidity());
				billingPlanObjectLive.setCoomandObjects(billingPlanObjectUtility.getCoomandObjects());
				
				GregorianCalendar calendar = new GregorianCalendar();
				SubscriberDetail subscriberDetail = (SubscriberDetail)ContextLoaderImpl.getBeans("subscriberDetail");
				subscriberDetail.setMsisdn(msisdn);
				subscriberDetail.setLast_subscription_date(calendar);
				subscriberDetail.setDate_created(calendar);
				subscriberDetail.setNext_subscription_date(getLastDateOfMonth());
				
				
				logger.info("======Finalized setup=======");
				
				ArrayList<CommandPropertiesUtility> commandProperties = billingPlanObjectUtility.getCoomandObjects();
				
				for (CommandPropertiesUtility commandProps : commandProperties)
				{
					commandInvoker.addCommand(commandFactory.GetCommandInstance(commandProps.getCommand(), "com.vasconsulting.www.interfaces.impl", 
							commandProps.getParam()));					
				}
				
				provisionStatus = commandInvoker.provision();
				
				logger.info("Provision request result is "+provisionStatus+" for subscriber with MSISDN = "+msisdn);
				
				try
				{
					if (provisionStatus != StatusCodes.SUCCESS)
					{
						response.getWriter().write(properties.getProperty(new Integer(provisionStatus).toString()));
						logger.error("The request to provision "+msisdn+" failed with code "+provisionStatus);
						/**
						 * TODO:
						 * Implement what should be done in case of error/success.
						 */
					}
					else
					{
						response.getWriter().write("<ussd><type>3</type><msg>"+billingPlanObjectUtility.getSuccessMessage()+"</msg></ussd>");
						transactionlog.setStatus("SUCCESSFUL");
						hibernateUtility.saveTransactionlog(transactionlog);
					}
				}
				catch(IOException ex){
					ex.printStackTrace();
				}
			}
		}
	}
	
	@RequestMapping(value="/ussdxmlservice", method = RequestMethod.POST)
	@ResponseBody
	public void generateSubscriberUSSDMenuGhana(@RequestBody String request, Writer writer, HttpSession session) throws IOException
	{
		logger.info("The raw xml that is received is = "+request);
		HashMap<String , String> ussdRequestMap = xmlUtility.getHomiscoUSSDXmlAsMap(request);//Homisco	
		
		String msisdn = ussdRequestMap.get("MOBILE_NUMBER");//Homisco	
		String responseFromFileText = "";	
		String msg;
		
		if (ussdRequestMap.containsKey("USSD_BODY"))
			msg = ussdRequestMap.get("USSD_BODY");
		else msg = ussdRequestMap.get("SERVICE_KEY");		
			
		logger.info("New USSD message received to process : "+ussdRequestMap);
		
		if (session.getAttribute("subscriberData") != null)
			subscriberInSession = (SubscriberUSSDConversation)session.getAttribute("subscriberData");
		else 
			subscriberInSession = new SubscriberUSSDConversation();
		
		if (msg.equalsIgnoreCase(properties.getProperty("ussdmenurootmenukeyword")))
		{
			HashMap<String, ArrayList<String>> responseFromConfFile = 
				xmlUtility.getUSSDMessage(xmlUtility.getUSSDConfigRootTag().get("root"));
			
			Iterator<String> iterator = responseFromConfFile.keySet().iterator();
			if (iterator.hasNext())
				responseFromFileText = iterator.next();
			
			if (!responseFromFileText.equalsIgnoreCase("LEAFNODE"))
			{
				subscriberInSession.saveInContainer(msisdn, responseFromConfFile.get(responseFromFileText));
				logger.info("Total number of subscribers accessing the USSD Menu is = "+subscriberInSession.getCurrentSizeOfContainer());
				session.setAttribute("subscriberData", subscriberInSession);
				writer.write(xmlUtility.generateHomiscoResponse("1", responseFromFileText, "REQUEST", "FALSE"));
			}
			else
			{				
				String [] paramValues = responseFromFileText.split(":sep:");
				
				
				subscriberInSession.removeFromContainer(msisdn);
				session.setAttribute("subscriberData", subscriberInSession);
				
				writer.write(xmlUtility.generateHomiscoResponse(ussdRequestMap.get("SESSION_ID"), responseFromFileText, 
						ussdRequestMap.get("SEQUENCE"), "RESPONSE"));
				
				//TODO:1000 implement logic to call the ref portion of the xml.
			}		
		}
		else
		{
			if (msisdn.length() == 9) msisdn = "233"+msisdn;
			
			ArrayList<String> sessionState = subscriberInSession.getMsisdnFromContainer(msisdn);
			if ( sessionState == null ) sessionState = new ArrayList<String>();
			if (new Integer(msg) > sessionState.size())
			{
				subscriberInSession.removeFromContainer(msisdn);
				session.setAttribute("subscriberData", subscriberInSession);
				
				writer.write(xmlUtility.generateHomiscoResponse(ussdRequestMap.get("SEQUENCE"),  
						"Invalid Selection. Please start again", "RESPONSE", "FALSE"));
				return;
			}
			
			responseFromFileText = sessionState.get((new Integer(msg).intValue()) - 1);
			
			HashMap<String, ArrayList<String>> responseFromConfFile = xmlUtility.getUSSDMessage(responseFromFileText);
			
			Iterator<String> iterator = responseFromConfFile.keySet().iterator();
			if (iterator.hasNext())
				responseFromFileText = iterator.next();
			
			if (!responseFromFileText.equalsIgnoreCase("LEAFNODE"))
			{
				subscriberInSession.saveInContainer(msisdn, responseFromConfFile.get(responseFromFileText));
				logger.info("Total number of subscribers accessing the USSD Menu is = "+subscriberInSession.getCurrentSizeOfContainer());
				session.setAttribute("subscriberData", subscriberInSession);
				writer.write(xmlUtility.generateHomiscoResponse(ussdRequestMap.get("SEQUENCE"), responseFromFileText, 
						"REQUEST", "FALSE"));
			}
			else
			{
				//TODO:1000 implement logic to call the ref portion of the xml.
				ArrayList<String> responseFromConfFileFinal = responseFromConfFile.get(responseFromFileText);
				
				responseFromFileText = responseFromConfFileFinal.get(0);
				
				String [] paramValues = responseFromFileText.split(":sep:");
				
				subscriberInSession.removeFromContainer(msisdn);
				session.setAttribute("subscriberData", subscriberInSession);
				
				urlInvoker.execute(paramValues[1], msisdn, paramValues[2], paramValues[3]);				
				
				//writer.write(xmlUtility.generateHomiscoResponse(ussdRequestMap.get("SESSION_ID"), responseFromFileText, "RESPONSE"));
				writer.write(xmlUtility.generateHomiscoResponse(ussdRequestMap.get("SEQUENCE"),  
						paramValues[3], "RESPONSE", "TRUE"));
			}
		}
	}
	
}
