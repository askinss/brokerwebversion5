package com.vasconsulting.www.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vasconsulting.www.domain.DeductionLog;
import com.vasconsulting.www.domain.FutureRenewal;
import com.vasconsulting.www.domain.ResponseDetails;
import com.vasconsulting.www.domain.RolesDetail;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.domain.UserLoginDetails;
import com.vasconsulting.www.domain.Users;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.BrokerService;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.LoadAllBillingPlanObjects;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.MD5;
import com.vasconsulting.www.utility.RIMXMLResponseDetail;
import com.vasconsulting.www.utility.RIMXMLUtility;
import com.vasconsulting.www.utility.SendSmsToKannelService;
import com.vasconsulting.www.utility.StatusCodes;
import com.vasconsulting.www.utility.StatusCodesUI;

import sun.misc.BASE64Decoder;

@Controller
public class SearchServiceControllerImpl {
	private Logger logger = Logger.getLogger(SearchServiceControllerImpl.class);
	@SuppressWarnings("rawtypes")
	private ResponseDetails response = new ResponseDetails();
	private RIMXMLResponseDetail rimStatus = new RIMXMLResponseDetail();
	private ArrayList<BillingPlanObjectUtility> billingPlans;
	private LoadAllBillingPlanObjects allBillingPlans;
	private RIMXMLUtility rimXMLUtility;
	private HibernateUtility hibernateUtility;
	private TransactionLog transactionLog = new TransactionLog();
	private LoadAllProperties properties;
	private EmailTaskExecutor emailExecutor;
	private BrokerService brokerService;
	private String randomPassword;
	private SimpleMailMessage mailMessage;
	private SendSmsToKannelService sendSmsToKannelService;
	
	
	@Autowired
	public void setSendSmsToKannelService(
			SendSmsToKannelService sendSmsToKannelService) {
		this.sendSmsToKannelService = sendSmsToKannelService;
	}

	@Autowired
	public void setMailMessage(SimpleMailMessage mailMessage) {
		this.mailMessage = mailMessage;
	}

	@Autowired
	public void setRandomPassword(String randomPassword) {
		this.randomPassword = randomPassword;
	}

	@Autowired
	public void setBrokerService(BrokerService brokerService) {
		this.brokerService = brokerService;
	}

	@Autowired
	public void setMyEmailTaskExecutor(EmailTaskExecutor myEmailTaskExecutor) {
		this.emailExecutor = myEmailTaskExecutor;
	}

	@Autowired
	public void setRimXMLUtility(RIMXMLUtility rimXMLUtility) {
		this.rimXMLUtility = rimXMLUtility;
	}

	@Autowired
	public void setHibernateUtility(HibernateUtility hibernateUtility) {
		this.hibernateUtility = hibernateUtility;
	}

	@Autowired
	public void setAllBillingPlans(LoadAllBillingPlanObjects allBillingPlans) {
		this.allBillingPlans = allBillingPlans;
	}

	@Autowired
	public void setProperties(LoadAllProperties properties) {
		this.properties = properties;
	}

	

	@RequestMapping(value = "/getsubscriberdetails/{search_type}/{search_value}", method = RequestMethod.GET)
	public @ResponseBody
	SubscriberDetail getSubscriberByType(@PathVariable("search_type") String searchType,
			@PathVariable("search_value") String searchValue, HttpServletRequest request) throws IOException {
		try {
			logger.info("Request to load the values of search " + searchType
					+ "/" + searchValue);
			SubscriberDetail sub = null;

			sub = hibernateUtility.getSubscriberInformation(searchType,
					searchValue);
			logger.info("value of sub = " + sub);

			if (sub == null) {
				SubscriberDetail subb = new SubscriberDetail();
				subb.setId(null);
				return subb;
			}

			return sub;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	/**
	 * This is used to get the details of all the users that are on the
	 * application
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/getalluserdetail/{}", method = RequestMethod.GET)
	public @ResponseBody
	ArrayList<Users> getAllUserDetails(HttpServletRequest request)
			throws IOException {
		try {
			logger.info("Request to load the users on the system has been received");
			// Authenticate the user in the system before you allow them access
			// to the function.

			// query the database to see if the subscriberdetail is returned
			ArrayList<Users> sub = null;

			sub = hibernateUtility.getAllUserDetails();
			logger.info("value of sub = " + sub);

			if (sub == null) {
				sub = new ArrayList<Users>();
				return sub;
			}

			return sub;
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ArrayList<Users>();
		}
	}

	

}