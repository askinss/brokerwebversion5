/**
 * 
 */
package com.vasconsulting.www.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.vasconsulting.www.domain.ResponseDetails;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.Users;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.LoginRestControllerUtility;

@Controller
public class UserManagementControllerImpl 
{
	private Logger logger = Logger.getLogger(UserManagementControllerImpl.class);
	private RestTemplate restTemplate = new RestTemplate();
	private LoadAllProperties properties = new LoadAllProperties();
	
	@Autowired
	public void setLoginRest(LoginRestControllerUtility loginRest) {
		this.loginRest = loginRest;
	}


	private LoginRestControllerUtility loginRest;
	
	// this is posted by the search form when a search occurs
		@RequestMapping(value = "/resetpassword", method = RequestMethod.POST)
		public String resetPassword(@RequestParam("password_current") String password_current, @RequestParam("password_new") String password_new,
				@RequestParam("password_new1") String password_new1, Model model, HttpSession httpSession, HttpServletRequest request)
		{
			logger.info("resetting password values new: "+password_new+", old: "+password_current);
			if(password_new.equals(password_new1)==false)
			{
				model.addAttribute("message","The new Passwords that you have entered do not match. Please correct and try again");
				return "home/change";
			}
			
			
			String url =properties.getProperty("webserviceURL")+"/broker/resetpassword";
			if(httpSession.getAttribute("userDetail")!=null)
			{
				// get the user from this session
				Users user = (Users)httpSession.getAttribute("userDetail");
				
				if(!password_current.equals(user.getPassword()))
				{
					model.addAttribute("message","Current Password is wrong. Please enter correctly and try again");
					httpSession.setAttribute("tab", "account");
					return "home/change";
				}
				
				if(password_new.equals(user.getPassword()))
				{
					model.addAttribute("message","You cannot use the same password as your current password. Please enter a new one and try again");
					httpSession.setAttribute("tab", "account");
					return "home/change";
				}
				
				// set the new password and set the 'isdefault' flag to NO
				logger.info("user email: "+user.getEmail()+", msisdn: "+user.getMsisdn());
				user.setPassword(password_new);
				user.setIsdefaultpassword("NO");
				
				logger.debug("resetting password...");
				restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
				
				ResponseDetails responseDetails = loginRest.resetPasswordUserDetails(user, request);
				if(responseDetails!=null)
				{
					if(responseDetails.getStatusCode()==0)
					{
						// invalidate session
						httpSession.invalidate();
						// successfully reset password
						// we redirect to the login page..
						
						return "redirect:/login";
					}
					else
					{
						// error occured
						// we display the error message on the page
						model.addAttribute("message", responseDetails.getDescription()+", "+responseDetails.getStatusCode());
					}
				}
			}
			else
			{
				return "redirect:/login";
			}
			return "home/change";
		}
	
	@RequestMapping(value = "/changepassword", method = RequestMethod.GET)
	public String changePassword(Model model, HttpServletRequest request, HttpSession httpSession)
	{
		if(httpSession.getAttribute("userDetail")!=null)
		{
			Users userDetail= new Users();
		
			model.addAttribute("userDetail", userDetail);
			httpSession.setAttribute("message", "");
			httpSession.setAttribute("tab", "account");
			return "home/change";
		}
		else
		{
			// we redirect to the login page
			httpSession.invalidate();
			return "redirect:/login";
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/manageusers", method = RequestMethod.GET)
	public String manageUers(Map<String, Object> model, HttpServletRequest request, HttpSession httpSession)
	{
		if(httpSession.getAttribute("userDetail")!=null)
		{
			String url = properties.getProperty("webserviceURL")+"/broker/getalluserdetail";
			System.out.println("getting url for billing plans... "+url);
			
			// we retrieve the users from the webservice			
			ArrayList<Users> allUsers = loginRest.getAllUserDetails(request);
			
			logger.info("The response that is received from the call to getalluserdetail service"+allUsers);
		
			model.put("allusers", allUsers);
			
			httpSession.setAttribute("message", "");
			httpSession.setAttribute("tab", "admin");
			return "home/manageusers";
		}
		else
		{
			// we redirect to the login page
			return "redirect:/login";
		}
	}
	
}
