package com.vasconsulting.www.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import javax.servlet.http.HttpServletResponse;

import net.rubyeye.xmemcached.MemcachedClient;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.vasconsulting.www.domain.MenuSession;
import com.vasconsulting.www.domain.MenuTree;
import com.vasconsulting.www.domain.RequestTracker;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BrokerService;
import com.vasconsulting.www.utility.LoadAllBillingPlanObjects;
import com.vasconsulting.www.utility.LoadAllProperties;

@Controller
public class USSDHandlerUganda 
{
	
	private HibernateUtility hibernateUtility;
	private int provisionStatus = -1;
	@Autowired
	private BrokerService brokerService;
	@Autowired
	private TransactionLog transactionlog;
	@Autowired
	private MemcachedClient memcachedClient;
	private LoadAllProperties properties;

	@Autowired
	public void setHibernateUtility(HibernateUtility hibernateUtility)
	{
		this.hibernateUtility = hibernateUtility;
	}
	
	@Autowired
	public void setBrokerService(BrokerService brokerService) {
		this.brokerService = brokerService;
	}
	
	@Autowired
	public void setProperties(LoadAllProperties properties)
	{
		this.properties = properties;
	}

	Logger logger = Logger.getLogger(USSDHandlerUganda.class);
	
	
	public int provisionMethod(String msisdn, String msg){
		logger.info("In the provision method the details are "+msisdn+"::"+msg);
		saveRequest(msisdn, msg);
		SubscriberDetail subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn(stripLeadingMsisdnPrefix(msisdn));
		subscriberDetail.setShortCode(msg);
		LoadAllBillingPlanObjects loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
				.getBeans("loadAllBillingPlanObjects");
		brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		provisionStatus = brokerService.activate(msg, subscriberDetail);
		return provisionStatus;
	}
	
	
	@RequestMapping(value="/ussdmenuservice", method = RequestMethod.GET)
	public void provisionSubscriber(@RequestParam("msisdn") String msisdn, @RequestParam("msg") String msg, 
			@RequestParam("sessionid") String sessionid, HttpServletResponse responseToGateway) throws IOException
	{
		//check if the subscriber already has a valid session (shouldn't be null)
		logger.info("Value of MSISDN = "+msisdn+", Value of MSG = "+msg+", Value of SESSIONID = "+sessionid);
		
		MenuSession menuSessionMsisdn = hibernateUtility.isThereAValidMenuSession(msisdn);
		
		if (msg.equalsIgnoreCase("#"))//this should be true when a user selects an invalid option
		{
			
			//The subscriber has selected to cancel the action so exit.
			responseToGateway.addHeader("Content-Type", "UTF-8");
			responseToGateway.addHeader("FreeFlow", "FB");
			responseToGateway.getWriter().write("To access the menu again kindly redial the code.");
			logger.info("The user wants to quit the menu...");
			
			hibernateUtility.deleteMenuTreeByMsisdn(menuSessionMsisdn);
			
			return;
		}
		
			
		StringBuilder messageToSend = new StringBuilder("BLACKBERRY MENU");
		ArrayList<MenuTree> menuTree;
		MenuSession menuSessionObj = new MenuSession();
		
		logger.info("properties.getProperty(\"ussdmenurootmenukeyword\") = "+properties.getProperty("ussdmenurootmenukeyword"));
		
		if (menuSessionMsisdn != null)
		{
			//check if this session is valid else save the request and then serve the menu
			MenuSession menuSessionMsisdnAndSession = hibernateUtility.isThereAValidMenuSession(msisdn, sessionid);
			if(menuSessionMsisdnAndSession != null)
			{
				if(menuSessionMsisdn.getParent_id().trim().equalsIgnoreCase(properties.getProperty("ussdmenurootmenukeyword")))
				{
					menuTree = hibernateUtility.getMenuToDisplay(menuSessionMsisdn.getParent_id(), msg);
					logger.info("Calling the method = getMenuToDisplay(menuSessionMsisdn.getParent_id(), msg)");
					
				}
				else
				{
					menuTree = hibernateUtility.getMenuToDisplay(menuSessionMsisdn.getParent_id());
					logger.info("Calling the method = getMenuToDisplay(msg)");
					logger.info("Value of menuTree = "+menuTree);
				}
				
				if (menuTree == null)//this should be true when a user selects an invalid option
				{
					messageToSend.append("\nYou have selected an invalid option. Please start again.");
					responseToGateway.addHeader("Content-Type", "UTF-8");
					responseToGateway.addHeader("FreeFlow", "FB");
					responseToGateway.getWriter().write(messageToSend.toString());
					
					return;
				}
				
				for (MenuTree menuTree2 : menuTree) {
					
					try
					{
						if (!menuTree2.getShortcode().equals(""))//if there is something in shortcode that means that this is a leafnode
						{
							logger.info("Activating "+menuTree2.getShortcode()+msg+" for ["+msisdn+"]");
							int status = -1;
							
							if (menuTree2.getShortcode().equalsIgnoreCase("status") || menuTree2.getShortcode().equalsIgnoreCase("stop")) 
							{
								status = provisionMethod(msisdn, menuTree2.getShortcode());
								
							}
							else 
							{
								status = provisionMethod(msisdn, menuTree2.getShortcode()+msg);
							}
							
							//delete the msisdn from session
							hibernateUtility.deleteMenuTreeByMsisdn(menuSessionMsisdnAndSession);
							
							if (status == 0)
							{
								responseToGateway.addHeader("Content-Type", "UTF-8");
								responseToGateway.addHeader("FreeFlow", "FB");
								responseToGateway.getWriter().write(properties.getProperty("successfulresponse"));
							}
							else
							{
								responseToGateway.addHeader("Content-Type", "UTF-8");
								responseToGateway.addHeader("FreeFlow", "FB");
								responseToGateway.getWriter().write("Dear subscriber, your request is being processed. You will receive an SMS when done");
							}
							return;
						}
					}
					catch(NullPointerException ex)
					{
						messageToSend.append("\n"+menuTree2.getSerialno()+". "+menuTree2.getBb_shortcode());
						
						menuSessionObj.setDate_created(new GregorianCalendar());
						menuSessionObj.setMsisdn(msisdn);
						menuSessionObj.setSessionid(sessionid);
						menuSessionObj.setParent_id(menuTree2.getId());
						menuSessionObj.setCurrent_msg(msg);
						menuSessionObj.setId(menuSessionMsisdnAndSession.getId());
						
						hibernateUtility.updateMenuSession(menuSessionObj);
						
					}					
				}				
				
			}
			else
			{
				//update the session in the database and then serve the menu
				menuSessionMsisdn.setDate_created(new GregorianCalendar());
				menuSessionMsisdn.setMsisdn(msisdn);
				menuSessionMsisdn.setSessionid(sessionid);
				menuSessionMsisdn.setParent_id(properties.getProperty("ussdmenurootmenukeyword"));
				menuSessionMsisdn.setCurrent_msg(msg);
				menuSessionObj.setId(menuSessionMsisdn.getId());
				
				hibernateUtility.updateMenuSession(menuSessionObj);
				
				//get the root menu and then iterate through and serve that to the user
				menuTree = hibernateUtility.getMenuToDisplay(properties.getProperty("ussdmenurootmenukeyword"));
				
				for (MenuTree menuTree2 : menuTree) {
					messageToSend.append("\n"+menuTree2.getSerialno()+". "+menuTree2.getBb_shortcode());
				}				
			}
		}
		else
		{
			//save the request and then serve the menu
			menuSessionObj.setDate_created(new GregorianCalendar());
			menuSessionObj.setMsisdn(msisdn);
			menuSessionObj.setSessionid(sessionid);
			menuSessionObj.setParent_id(properties.getProperty("ussdmenurootmenukeyword"));
			menuSessionObj.setCurrent_msg(msg);
			
			hibernateUtility.saveMenuSession(menuSessionObj);
			
			//get the root menu and then iterate through and serve that to the user
			menuTree = hibernateUtility.getMenuToDisplay(properties.getProperty("ussdmenurootmenukeyword"));
			
			for (MenuTree menuTree2 : menuTree) {
				messageToSend.append("\n"+menuTree2.getSerialno()+". "+menuTree2.getBb_shortcode());
			}			
		}
		
		responseToGateway.addHeader("Content-Type", "UTF-8");
		responseToGateway.addHeader("FreeFlow", "FC");
		responseToGateway.getWriter().write(messageToSend.toString());
		
		//provisionMethod(msisdn, msg);
	}

	/*@RequestMapping(value="/ussdmenuservice", method = RequestMethod.GET)
	public void provisionSubscriber(@RequestParam("msisdn") String msisdn, @RequestParam("msg") String msg, 
			@RequestParam("sessionid") String sessionid, HttpServletResponse responseToGateway) throws IOException
	{
		//check if the subscriber already has a valid session (shouldn't be null)
		logger.info("Value of MSISDN = "+msisdn+", Value of MSG = "+msg+", Value of SESSIONID = "+sessionid);
		
		if (msg.equalsIgnoreCase("#"))//this should be true when a user selects an invalid option
		{
			
			//The subscriber has selected to cancel the action so exit.
			responseToGateway.addHeader("Content-Type", "UTF-8");
			responseToGateway.addHeader("FreeFlow", "FB");
			responseToGateway.getWriter().write("To access the menu again kindly redial the code.");
			logger.info("The user wants to quit the menu...");
			
			return;
		}
		
		MenuSession menuSessionMsisdn = hibernateUtility.isThereAValidMenuSession(msisdn);	
		StringBuilder messageToSend = new StringBuilder("BLACKBERRY MENU");
		ArrayList<MenuTree> menuTree;
		MenuSession menuSessionObj = new MenuSession();
		
		logger.info("properties.getProperty(\"ussdmenurootmenukeyword\") = "+properties.getProperty("ussdmenurootmenukeyword"));
		
		if (menuSessionMsisdn != null)
		{
			//check if this session is valid else save the request and then serve the menu
			MenuSession menuSessionMsisdnAndSession = hibernateUtility.isThereAValidMenuSession(msisdn, sessionid);
			if(menuSessionMsisdnAndSession != null)
			{
				if(menuSessionMsisdn.getParent_id().trim().equalsIgnoreCase(properties.getProperty("ussdmenurootmenukeyword")))
				{
					menuTree = hibernateUtility.getMenuToDisplay(menuSessionMsisdn.getParent_id(), msg);
					logger.info("menuSessionMsisdn.getParent_id() = "+menuSessionMsisdn.getParent_id() + "msg="+msg);
					logger.info("Calling the method = getMenuToDisplay(menuSessionMsisdn.getParent_id(), msg)");
					logger.info("Value of menuTree = "+menuTree);
				}
				else
				{
					menuTree = hibernateUtility.getMenuToDisplay(menuSessionMsisdn.getParent_id());
					logger.info("Calling the method = getMenuToDisplay(msg)");
					logger.info("Value of menuTree = "+menuTree);
				}
				
				if (menuTree == null)//this should be true when a user selects an invalid option
				{
					messageToSend.append("\nYou have selected an invalid option. Please start again.");
					responseToGateway.addHeader("Content-Type", "UTF-8");
					responseToGateway.addHeader("FreeFlow", "FB");
					responseToGateway.getWriter().write(messageToSend.toString());
					
					return;
				}
				
				for (MenuTree menuTree2 : menuTree) {
					
					try
					{
						if (!menuTree2.getShortcode().equals(""))//if there is something in shortcode that means that this is a leafnode
						{
							logger.info("Activating "+menuTree2.getShortcode()+msg+" for ["+msisdn+"]");
							int status = -1;
							
							if (menuTree2.getShortcode().equalsIgnoreCase("status") || menuTree2.getShortcode().equalsIgnoreCase("stop")) 
							{
								status = provisionMethod(msisdn, menuTree2.getShortcode());
								
							}
							else 
							{
								status = provisionMethod(msisdn, menuTree2.getShortcode()+msg);
							}
							
							if (status == 0)
							{
								responseToGateway.addHeader("Content-Type", "UTF-8");
								responseToGateway.addHeader("FreeFlow", "FB");
								responseToGateway.getWriter().write(properties.getProperty("successfulresponse"));
							}
							else
							{
								responseToGateway.addHeader("Content-Type", "UTF-8");
								responseToGateway.addHeader("FreeFlow", "FB");
								responseToGateway.getWriter().write("Dear subscriber, your request is being processed. You will receive an SMS when done");
							}
							return;
						}
					}
					catch(NullPointerException ex)
					{
						messageToSend.append("\n"+menuTree2.getSerialno()+". "+menuTree2.getBb_shortcode());
						
						menuSessionObj.setDate_created(new GregorianCalendar());
						menuSessionObj.setMsisdn(msisdn);
						menuSessionObj.setSessionid(sessionid);
						menuSessionObj.setParent_id(menuTree2.getId());
						menuSessionObj.setCurrent_msg(msg);
						menuSessionObj.setId(menuSessionMsisdnAndSession.getId());
						
						hibernateUtility.updateMenuSession(menuSessionObj);
						
					}					
				}				
				
			}
			else
			{
				//update the session in the database and then serve the menu
				menuSessionMsisdn.setDate_created(new GregorianCalendar());
				menuSessionMsisdn.setMsisdn(msisdn);
				menuSessionMsisdn.setSessionid(sessionid);
				menuSessionMsisdn.setParent_id(properties.getProperty("ussdmenurootmenukeyword"));
				menuSessionMsisdn.setCurrent_msg(msg);
				menuSessionObj.setId(menuSessionMsisdn.getId());
				
				hibernateUtility.updateMenuSession(menuSessionObj);
				
				//get the root menu and then iterate through and serve that to the user
				menuTree = hibernateUtility.getMenuToDisplay(properties.getProperty("ussdmenurootmenukeyword"));
				
				for (MenuTree menuTree2 : menuTree) {
					messageToSend.append("\n"+menuTree2.getSerialno()+". "+menuTree2.getBb_shortcode());
				}				
			}
		}
		else
		{
			//save the request and then serve the menu
			menuSessionObj.setDate_created(new GregorianCalendar());
			menuSessionObj.setMsisdn(msisdn);
			menuSessionObj.setSessionid(sessionid);
			menuSessionObj.setParent_id(properties.getProperty("ussdmenurootmenukeyword"));
			menuSessionObj.setCurrent_msg(msg);
			
			hibernateUtility.saveMenuSession(menuSessionObj);
			
			//get the root menu and then iterate through and serve that to the user
			menuTree = hibernateUtility.getMenuToDisplay(properties.getProperty("ussdmenurootmenukeyword"));
			
			for (MenuTree menuTree2 : menuTree) {
				messageToSend.append("\n"+menuTree2.getSerialno()+". "+menuTree2.getBb_shortcode());
			}			
		}
		
		responseToGateway.addHeader("Content-Type", "UTF-8");
		responseToGateway.addHeader("FreeFlow", "FC");
		responseToGateway.getWriter().write(messageToSend.toString());
		
		//provisionMethod(msisdn, msg);
	}*/
	
	
	private String stripLeadingMsisdnPrefix(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("+"))
		{
			return Msisdn.substring(1, Msisdn.length());
		}
		else return msisdn;
	}
	
	
	private void saveRequest(String msisdn, String msg){
		RequestTracker request = new RequestTracker();
		request.setMsisdn(msisdn);
		request.setShortCode(msg.toLowerCase());
		try{
			hibernateUtility.saveRequestTracker(request);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}

}
