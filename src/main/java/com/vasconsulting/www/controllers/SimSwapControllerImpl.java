package com.vasconsulting.www.controllers;

import com.vasconsulting.www.domain.ResponseDetails;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.Users;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.LoginRestControllerUtility;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SimSwapControllerImpl
{
  private Logger logger = Logger.getLogger(SimSwapControllerImpl.class);

  private LoadAllProperties properties = new LoadAllProperties();
  private LoginRestControllerUtility loginRest;

  @Autowired
  public void setLoginRest(LoginRestControllerUtility loginRest)
  {
    this.loginRest = loginRest;
  }

  private ResponseDetails getAvailablePlansFromWSByType(HttpSession httpSession)
  {
    System.out.println("Getting all plans by billing plans by type... ");

    Users users = (Users)httpSession.getAttribute("userDetail");
    this.logger.info("user login type: " + users.getUsertype());

    ResponseDetails billingPlansResponse = this.loginRest.getAvailablePlansByType(users.getUsertype());

    return billingPlansResponse;
  }

  @RequestMapping(value={"/activateaction"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public String processActivation(HttpSession httpSession, Model model)
  {
    this.logger.info("posting Activate request");
    this.logger.info("getting Subscriber Detail from session..");
    SubscriberDetail subdetail = (SubscriberDetail)httpSession.getAttribute("subscriberDetail");
    if (subdetail != null)
    {
      subdetail.setStatus("ACTIVE");
      String url = this.properties.getProperty("webserviceURL") + "/broker/activatesubscriber";
      this.logger.info("activate subscriber webservice call: " + url);
      try
      {
        this.logger.info("Saving subscriber detail : " + subdetail);

        ResponseDetails billingPlansResponse = getAvailablePlansFromWSByType(httpSession);

        String requestType = "savesubscriberdetail";

        if (billingPlansResponse != null)
        {
          this.logger.info("Response code: " + billingPlansResponse.getStatusCode() + ", Description: " + billingPlansResponse.getDescription());

          this.logger.info("retrieving plans BY type from web service Response");

          Object responseServicePlan = billingPlansResponse.getClassInstance();
          if (responseServicePlan != null)
          {
            Map serviceplan = (Map)responseServicePlan;

            Iterator iterator = serviceplan.keySet().iterator();

            while (iterator.hasNext())
            {
              String tempValue = (String)iterator.next();
              if (tempValue.equalsIgnoreCase(subdetail.getShortCode()))
              {
                if (!((String)serviceplan.get(tempValue)).equalsIgnoreCase("PREP"))
                {
                  this.logger.info("This = " + (String)serviceplan.get(tempValue));
                  requestType = "savesubscriberdetailpost";
                  break;
                }
              }
            }
          }
        }
        ResponseDetails responseDetails;
       // ResponseDetails responseDetails;
        if (requestType.equalsIgnoreCase("savesubscriberdetailpost"))
        {
          responseDetails = this.loginRest.saveSubscriberDetailsPost((SubscriberDetail)httpSession.getAttribute("subscriberDetail"));
        }
        else
        {
          responseDetails = this.loginRest.saveSubscriberDetails((SubscriberDetail)httpSession.getAttribute("subscriberDetail"));
        }

        if (responseDetails != null)
        {
          if (responseDetails.getStatusCode() == 0)
          {
            System.out.println("Subscriber detail saved successfully... ");
            if (responseDetails.getClassInstance() != null)
            {
              System.out.println("Save Response [Status " + responseDetails.getStatusCode() + "], [Description " + responseDetails.getDescription());
            }
            httpSession.setAttribute("message", responseDetails.getDescription());
            return "redirect:/notification";
          }

          this.logger.info("Status code: " + responseDetails.getStatusCode());
          this.logger.info("Description: " + responseDetails.getDescription());
          httpSession.setAttribute("message", this.properties.getProperty(new Integer(responseDetails.getStatusCode()).toString()));
          return "redirect:/notification";
        }

        this.logger.info("Description: " + responseDetails.getDescription());
        httpSession.setAttribute("message", responseDetails.getDescription());
        return "redirect:/notification";
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
        httpSession.setAttribute("message", "An error has occured while processing your request. Please try again orcontact the administrator if it persists. ");

        return "redirect:/notification";
      }

    }

    httpSession.invalidate();
    return "redirect:/login";
  }

  @RequestMapping(value={"/deactivateaction"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public String processDeActivation(HttpSession httpSession, Model model)
  {
    if (httpSession.getAttribute("subscriberDetail") != null)
    {
      this.logger.info("posting De-Activate request");
      this.logger.info("getting Subscriber Detail from session..");
      SubscriberDetail subdetail = (SubscriberDetail)httpSession.getAttribute("subscriberDetail");

      ResponseDetails responseDetails = this.loginRest.deactivateSubscriber(subdetail);

      if (responseDetails != null)
      {
        httpSession.setAttribute("message", responseDetails.getDescription());
        return "home/notification";
      }

      httpSession.setAttribute("message", "There was an error while process your reqiest. Please contact the administrator if the issue persists");
      return "home/notification";
    }

    httpSession.invalidate();
    return "redirect:/login";
  }

  @RequestMapping(value={"/changeserviceaction"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public String processChangeService(@Valid SubscriberDetail subscriberDetail, HttpSession httpSession, Model model)
  {
    this.logger.info("posting Change service request");
    this.logger.info("getting Subscriber Detail from request and storing in Session..");

    httpSession.setAttribute("subscriberDetail", subscriberDetail);
    return "redirect:/home/preview";
  }

  @RequestMapping(value={"/processsimswap"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public String processSubscriberDetail(@RequestParam("new_imsi") String new_imsi, HttpSession httpSession)
  {
    if (httpSession.getAttribute("userDetail") != null)
    {
      SubscriberDetail subdetail = (SubscriberDetail)httpSession.getAttribute("subscriberDetail");
      this.logger.info("Request to perform sim swap for MSISDN [" + subdetail.getMsisdn() + "] has been received");

      this.logger.debug("processing simswap for subscriber detail with imsi " + new_imsi + "...");

      ResponseDetails responseDetail = this.loginRest.modifyBBSubscriberBillingIdentifier(subdetail.getImsi(), new_imsi);

      if (responseDetail != null)
      {
        if (responseDetail.getStatusCode() == 0)
        {
          System.out.println("Subscriber simswap saved successfully... ");
          if (responseDetail.getClassInstance() != null)
          {
            System.out.println("Save Response [Status " + responseDetail.getStatusCode() + "], [Description " + responseDetail.getDescription());
          }
          httpSession.setAttribute("message", responseDetail.getDescription());
          return "redirect:/notification";
        }

        this.logger.info("Status code: " + responseDetail.getStatusCode());
        this.logger.info("Description: " + responseDetail.getDescription());
        httpSession.setAttribute("message", this.properties.getProperty(new Integer(responseDetail.getStatusCode()).toString()));
        return "redirect:/notification";
      }

      this.logger.info("Description: " + responseDetail.getDescription());
      httpSession.setAttribute("message", responseDetail.getDescription());
      return "redirect:/notification";
    }

    httpSession.invalidate();
    return "redirect:/login";
  }

  @RequestMapping(value={"/processdevice"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public String processDeviceChange(HttpSession httpSession, Model model, HttpServletRequest request)
  {
    this.logger.info("processing device change..");
    if (httpSession.getAttribute("subscriberDetail") != null)
    {
      SubscriberDetail subscriberDetail = (SubscriberDetail)httpSession.getAttribute("subscriberDetail");
      this.logger.info("device change for subscriber " + subscriberDetail.getMsisdn() + " with IMIS " + subscriberDetail.getImsi() + " initiated..");

      subscriberDetail.setPin(request.getParameter("new_pin"));
      subscriberDetail.setImei(request.getParameter("new_imei"));

      this.loginRest.modifyBBSubscriberNonBillingIdentifier(subscriberDetail);
      httpSession.setAttribute("message", "Device change was successful");
      return "redirect:/notification";
    }

    this.logger.info("no subscriber information saved in session..exiting..");

    SubscriberDetail subDetail = new SubscriberDetail();
    model.addAttribute("subscriberdetail", subDetail);

    return "home/home";
  }

  @RequestMapping(value={"/simswap"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public String loadSimSwap(Model model, HttpServletRequest request, HttpSession httpSession)
  {
    if (httpSession.getAttribute("userDetail") != null)
    {
      if (httpSession.getAttribute("subscriberDetail") != null)
      {
        SubscriberDetail subscriberDetail = (SubscriberDetail)httpSession.getAttribute("subscriberDetail");
        this.logger.info("subscriber detail " + subscriberDetail.getImsi());
        model.addAttribute("subscriberdetail", subscriberDetail);

        return "home/simswap";
      }

      this.logger.info("no subscriber information saved in session..exiting..");

      SubscriberDetail subDetail = new SubscriberDetail();
      model.addAttribute("subscriberdetail", subDetail);

      return "home/home";
    }

    httpSession.invalidate();
    return "redirect:/login";
  }

  @RequestMapping(value={"/devicechange"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public String loadDeviceChange(Model model, HttpServletRequest request, HttpSession httpSession)
  {
    if (httpSession.getAttribute("userDetail") != null)
    {
      if (httpSession.getAttribute("subscriberDetail") != null)
      {
        SubscriberDetail subscriberDetail = (SubscriberDetail)httpSession.getAttribute("subscriberDetail");
        this.logger.info("subscriber detail " + subscriberDetail.getImei());
        model.addAttribute("subscriberDetail", subscriberDetail);

        return "home/devicechange";
      }

      SubscriberDetail subDetail = new SubscriberDetail();
      model.addAttribute("subscriberDetail", subDetail);

      return "home/home";
    }

    this.logger.info("no user session detected..login please");
    httpSession.invalidate();

    return "redirect:/login";
  }

  @RequestMapping(value={"/deactivate"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public String loadDeActivateSubscriber(Model model, HttpServletRequest request, HttpSession httpSession)
  {
    if (httpSession.getAttribute("userDetail") != null)
    {
      if (httpSession.getAttribute("subscriberDetail") != null)
      {
        this.logger.info("subscriber detail " + httpSession.getAttribute("subscriberDetail"));
        model.addAttribute("subscriberdetail", httpSession.getAttribute("subscriberDetail"));

        return "home/deactivate";
      }

      SubscriberDetail subDetail = new SubscriberDetail();
      model.addAttribute("subscriberdetail", subDetail);

      return "home/home";
    }

    this.logger.info("no user session detected..login please");

    httpSession.invalidate();
    return "redirect:/login";
  }

  @RequestMapping(value={"/changeservice"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public String loadChangeService(Model model, HttpServletRequest request, HttpSession httpSession)
  {
    if (httpSession.getAttribute("userDetail") != null)
    {
      if (httpSession.getAttribute("subscriberDetail") != null)
      {
        SubscriberDetail subscriberDetail = (SubscriberDetail)httpSession.getAttribute("subscriberDetail");
        this.logger.info("subscriber detail " + subscriberDetail.getImei());
        model.addAttribute("subscriberdetail", subscriberDetail);

        this.logger.info("retrieving service plans...");

        Users users = (Users)httpSession.getAttribute("userDetail");
        this.logger.info("user login type: " + users.getUsertype());
        ResponseDetails billingPlansResponse = this.loginRest.getAvailablePlans(users.getUsertype());
        if (billingPlansResponse != null)
        {
          System.out.println("Response code: " + billingPlansResponse.getStatusCode() + ", Description: " + billingPlansResponse.getDescription());

          System.out.println("retrieving plans from web service Response");

          Object responseServicePlan = billingPlansResponse.getClassInstance();
          if (responseServicePlan != null)
          {
            System.out.println("Object type in the response object: " + responseServicePlan.getClass().getName());
            Map serviceplan = (Map)responseServicePlan;
            System.out.println("MAP value: " + serviceplan);
            model.addAttribute("serviceplanList", serviceplan);
          }
        }
        return "home/changeservice";
      }

      SubscriberDetail subDetail = new SubscriberDetail();
      model.addAttribute("subscriberdetail", subDetail);

      return "home/home";
    }

    this.logger.info("no user session detected..login please");
    httpSession.invalidate();

    return "redirect:/login";
  }

  @RequestMapping(value={"/changeservice"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public String changeService(@RequestParam("shortCode") String shortcode, Model model, HttpSession httpSession)
  {
    if (httpSession.getAttribute("userDetail") != null)
    {
      SubscriberDetail subdetail = (SubscriberDetail)httpSession.getAttribute("subscriberDetail");

      subdetail.setStatus("Active");
      subdetail.setShortCode(shortcode);
      String url = this.properties.getProperty("webserviceURL") + "/broker/activatesubscriber";
      this.logger.info("activate subscriber webservice call: " + url);
      try
      {
        this.logger.info("Changing subscriber detail " + subdetail.getFirstname() + " with plan :" + subdetail.getShortCode());

        ResponseDetails billingPlansResponse = getAvailablePlansFromWSByType(httpSession);

        String requestType = "savesubscriberdetail";

        if (billingPlansResponse != null)
        {
          this.logger.info("Response code: " + billingPlansResponse.getStatusCode() + ", Description: " + billingPlansResponse.getDescription());

          this.logger.info("retrieving plans BY type from web service Response");

          Object responseServicePlan = billingPlansResponse.getClassInstance();
          if (responseServicePlan != null)
          {
            Map serviceplan = (Map)responseServicePlan;
            this.logger.info("Value of serviceplan" + responseServicePlan);
            Iterator iterator = serviceplan.keySet().iterator();

            while (iterator.hasNext())
            {
              String tempValue = (String)iterator.next();

              if (tempValue.equalsIgnoreCase(subdetail.getShortCode()))
              {
                this.logger.info("Value of tempValue =" + tempValue);
                if (!((String)serviceplan.get(tempValue)).startsWith("Prep"))
                {
                  requestType = "savesubscriberdetailpost";
                  break;
                }
              }
            }
          }
        }
        ResponseDetails responseDetails;
     //   ResponseDetails responseDetails;
        if (requestType.equalsIgnoreCase("savesubscriberdetailpost"))
        {
          responseDetails = this.loginRest.saveSubscriberDetailsPost(subdetail);
        }
        else
        {
          responseDetails = this.loginRest.saveSubscriberDetails(subdetail);
        }

        if (responseDetails != null)
        {
          if (responseDetails.getStatusCode() == 0)
          {
            System.out.println("Subscriber detail saved successfully... ");
            if (responseDetails.getClassInstance() != null)
            {
              System.out.println("Save Response [Status " + responseDetails.getStatusCode() + "], [Description " + responseDetails.getDescription());
            }
            httpSession.setAttribute("message", responseDetails.getDescription());
            return "redirect:/notification";
          }

          this.logger.info("Status code: " + responseDetails.getStatusCode());
          this.logger.info("Description: " + responseDetails.getDescription());
          httpSession.setAttribute("message", this.properties.getProperty(new Integer(responseDetails.getStatusCode()).toString()));
          return "redirect:/notification";
        }

        this.logger.info("Description: " + responseDetails.getDescription());
        httpSession.setAttribute("message", responseDetails.getDescription());
        return "redirect:/notification";
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
        httpSession.setAttribute("message", "An error has occured while processing your request. Please try again orcontact the administrator if it persists. ");

        return "redirect:/notification";
      }

    }

    this.logger.info("no user session detected..login please");
    httpSession.invalidate();

    return "redirect:/login";
  }

  @RequestMapping(value={"/activate"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public String loadActivateSubscriber(Model model, HttpServletRequest request, HttpSession httpSession)
  {
    if (httpSession.getAttribute("userDetail") != null)
    {
      if (httpSession.getAttribute("subscriberDetail") != null)
      {
        SubscriberDetail subscriberDetail = (SubscriberDetail)httpSession.getAttribute("subscriberDetail");
        this.logger.info("subscriber detail " + subscriberDetail);
        model.addAttribute("subscriberDetail", subscriberDetail);

        return "home/activate";
      }

      SubscriberDetail subDetail = new SubscriberDetail();
      model.addAttribute("subscriberDetail", subDetail);

      return "home/home";
    }

    this.logger.info("no user session detected..login please");
    httpSession.invalidate();

    return "redirect:/login";
  }

  @RequestMapping(value={"/getdetail"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public String searchSubscriberDetail(@RequestParam("search_type") String search_type, @RequestParam("search_value") String search_value, Model model, HttpSession httpSession)
  {
    String _search_type = search_type; String _search_value = search_value;
    System.out.println("search type: " + search_type + ", searchvalue :" + _search_value);
    this.logger.debug("in getdetail request");

    Map varS = new HashMap();
    varS.put("search_type", _search_type);
    varS.put("search_value", _search_value);

    SubscriberDetail responseDetails = this.loginRest.getSubscriberByType(search_type, search_value);

    this.logger.info("Response Detail:=" + responseDetails.getId() + ";");
    if (responseDetails.getMsisdn() != null)
    {
      this.logger.info("Response Detail: " + responseDetails);

      this.logger.info("subcriber detail: " + responseDetails);
      this.logger.info("adding this subscriber to the session");

      httpSession.setAttribute("subscriberDetail", responseDetails);
      model.addAttribute("subscriberDetail", responseDetails);
    }
    else
    {
      httpSession.setAttribute("message", "No results found for " + _search_value + ". Please check the entered value and try again.");
      return "home/notification";
    }
    return "home/detail";
  }

  @InitBinder
  protected void initBinder(WebDataBinder binder)
  {
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    binder.registerCustomEditor(GregorianCalendar.class, new CustomDateEditor(
      dateFormat, false));
  }
}