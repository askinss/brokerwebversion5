package com.vasconsulting.www.controllers;

import java.util.Iterator;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.vasconsulting.www.domain.ResponseClass;
import com.vasconsulting.www.domain.ResponseDetails;
import com.vasconsulting.www.domain.RolesDetail;
import com.vasconsulting.www.domain.UserLoginDetails;
import com.vasconsulting.www.domain.Users;
import com.vasconsulting.www.utility.LinkedHashMapUtility;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.LoginRestControllerUtility;

@Controller
public class LoginControllerImpl
{
	private Logger logger = Logger.getLogger(LoginControllerImpl.class);
	private RestTemplate restTemplate = new RestTemplate();
	private LoadAllProperties properties = new LoadAllProperties();
	@Autowired
	public void setLoginRest(LoginRestControllerUtility loginRest) {
		this.loginRest = loginRest;
	}


	private LoginRestControllerUtility loginRest;			
	/**
	 * This method is used to load the first page that the user sees to login to the application.
	 * The format for the URL will be like http://ip:port/blackberry/login, and the request has to be a GET
	 * @param model
	 * @return String
	 */
	@RequestMapping(value="/login", method = RequestMethod.GET)
	public String displayLoginForm(Model model)
	{
		model.addAttribute(new Users());		
		return "login/loginform";
	}
	/*
	 * this action handles the sign out functionality in the UI
	 * */
	@RequestMapping(value="/signout", method = RequestMethod.GET)
	public String logoutUser(HttpSession httpSession)
	{
		if(httpSession.getAttribute("userDetail")!=null)
		{
			// we invalidate the user's session
			httpSession.invalidate();
		}
		return "redirect:/login";
	}
	/**
	 * This method is used to authenticate a user in the application, it will redirect to the home page and also set the 
	 * session id details of the user.
	 * @param users
	 * @param bindingResult
	 * @return String
	 */
	@RequestMapping(value="/login", method = RequestMethod.POST)
	public String authenticateUser(@Valid Users users, BindingResult bindingResult, HttpServletRequest request, 
			HttpSession httpSession, Model model)
	{
		System.out.println("Signing in with Username "+users.getUsername()+", with Password "+users.getPassword());
		
		logger.info("The value of bindingResult.hasErrors() = "+bindingResult.hasErrors());
		if(bindingResult.hasErrors()) 
		{
			return "login/loginform";
		}
		
			//call the REST service to get the details of the user that is trying to login at this point, if successful then 
			//redirect to the logged in page else bounce.
			
			//add a json converter so that the class that we are sending is converted to JSON for us
			restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
		
			ResponseDetails userLoginDetails = loginRest.authenticateUserById(users, request, httpSession);
			//ResponseDetails userLoginDetails = restTemplate.postForObject(properties.getProperty("webserviceURL")+"/broker/authenticateuserbyid", users, ResponseDetails.class);
			logger.info("request to authtenticate succesfuly made");
			
			if(userLoginDetails==null)	
			{
				logger.info("Error signing in for user: "+users.getUsername());
				
				// an invalid user; redirect to the appropriate page
				/*System.out.println("Credentials supplied not valid..");
				ResponseClass responseClass=new ResponseClass();
				
				responseClass.setStatusCode(userLoginDetails.getStatusCode());
				responseClass.setStatusMessage(userLoginDetails.getDescription());
				
				System.out.println("Response - Status code: "+userLoginDetails.getStatusCode()+" , Description: "+userLoginDetails.getDescription());
				System.out.println("Status code: "+responseClass.getStatusCode()+" , Description: "+responseClass.getStatusMessage());
				model.addAttribute("responseDetail", responseClass);*/
				return "redirect:/login";
			}
			else
			{
				logger.info("login status message "+userLoginDetails.getStatusCode());
				// we create a session for this user
				if(userLoginDetails.getStatusCode()==0)
				{
					// user logged in...
					if(userLoginDetails.getClassInstance()!=null)
					{
						logger.info("user detail type: "+userLoginDetails.getClassInstance().getClass().getName());
						logger.info("logged in user object data: "+userLoginDetails.getClassInstance());
						//LinkedHashMap user_login = (LinkedHashMap) userLoginDetails.getClassInstance();
						//logger.info("getting data from hashed map..");
						UserLoginDetails user_login_detail = (UserLoginDetails)userLoginDetails.getClassInstance();
						logger.info("User's logged in details: "+user_login_detail.getEmail()+" with Session ID "+user_login_detail.getSessionID()+" and login type:"+(user_login_detail.getUsertype()));
						users.setUsertype(user_login_detail.getUsertype());
						users.setFirstname(user_login_detail.getFullName());
						users.setEmail(user_login_detail.getEmail());
						
					}
					System.out.println("user logged in; creating user session and redirecting to home");
					System.out.println("user "+users.getUsername()+" logged in; creating user session and redirecting to home");
					
					logger.info("user type: "+users.getUsertype());
					httpSession.setAttribute("userDetail", users);
					httpSession.setMaxInactiveInterval(new Integer(properties.getProperty("inactivitytimelimit")));
				}
				if(userLoginDetails.getStatusCode()==120)
				{
					// redirect to the change password form
					logger.info("status code is "+userLoginDetails.getStatusCode()+" ,redirecting to /changepassword action..");
					// user logged in...
					if(userLoginDetails.getClassInstance()!=null)
					{
						logger.info("user detail type: "+userLoginDetails.getClassInstance().getClass().getName());
						logger.info("logged in user object data: "+userLoginDetails.getClassInstance());
						LinkedHashMap user_login = (LinkedHashMap) userLoginDetails.getClassInstance();
						logger.info("getting data from hashed map..");
						UserLoginDetails user_login_detail = LinkedHashMapUtility.ConvertToUserLoginDetail(user_login);
						logger.info("User's logged in details: "+user_login_detail.getEmail()+" with Session ID "+user_login_detail.getSessionID()+" and login type:"+user_login_detail.getUsertype());
						users.setUsertype(user_login_detail.getUsertype());
					}
					
					logger.info("user type: "+users.getUsertype());
					httpSession.setAttribute("userDetail", users);
					httpSession.setAttribute("tab", "account");
					httpSession.setMaxInactiveInterval(new Integer(properties.getProperty("inactivitytimelimit")));
					return "redirect:/changepassword";
				}
			}
		httpSession.setAttribute("tab", "subscription");
		return "redirect:/home";
		//return "redirect:/home";
	}
}
