/**
 * 
 */
package com.vasconsulting.www.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.vasconsulting.www.domain.ResponseDetails;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.Users;
import com.vasconsulting.www.utility.LoginRestControllerUtility;

/**
 * @author Ayotunde
 *
 */
@Controller
public class ReportManagementControllerImpl 
{
	private Logger logger = Logger.getLogger(ReportManagementControllerImpl.class);
	private RestTemplate restTemplate = new RestTemplate();
	@Autowired
	public void setLoginRest(LoginRestControllerUtility loginRest) {
		this.loginRest = loginRest;
	}


	private LoginRestControllerUtility loginRest;
	
	
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/totalusagereport", method = RequestMethod.GET)
	public String totalUsageReport(Map<String, Object> model, HttpServletRequest request, HttpSession httpSession)
	{
		if(httpSession.getAttribute("userDetail")!=null)
		{
			//String url = "http://localhost:8018/broker/getalluserdetail";
			//System.out.println("getting url for billing plans... "+url);
			
			// we retrieve the users from the webservice			
			//ArrayList<Users> allUsers = restTemplate.getForObject(url, ArrayList.class);
			ArrayList<Users> allUsers = loginRest.getAllUserDetails(request);
			
			logger.info("The response that is received from the call to getalluserdetail service"+allUsers);
		
			model.put("allusers", allUsers);
			
			httpSession.setAttribute("message", "");
			return "home/totalusagereport";
		}
		else
		{
			// we redirect to the login page
			return "redirect:/login";
		}
	}
	
}
