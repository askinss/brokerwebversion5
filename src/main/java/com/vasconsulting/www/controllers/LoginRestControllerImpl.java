package com.vasconsulting.www.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vasconsulting.www.domain.DeductionLog;
import com.vasconsulting.www.domain.FutureRenewal;
import com.vasconsulting.www.domain.ResponseDetails;
import com.vasconsulting.www.domain.RolesDetail;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.domain.UserLoginDetails;
import com.vasconsulting.www.domain.Users;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.BrokerService;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.LoadAllBillingPlanObjects;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.MD5;
import com.vasconsulting.www.utility.RIMXMLResponseDetail;
import com.vasconsulting.www.utility.RIMXMLUtility;
import com.vasconsulting.www.utility.SendSmsToKannelService;
import com.vasconsulting.www.utility.StatusCodes;
import com.vasconsulting.www.utility.StatusCodesUI;

import sun.misc.BASE64Decoder;

@Controller
public class LoginRestControllerImpl {
	private Logger logger = Logger.getLogger(LoginRestControllerImpl.class);
	@SuppressWarnings("rawtypes")
	private ResponseDetails response = new ResponseDetails();
	private RIMXMLResponseDetail rimStatus = new RIMXMLResponseDetail();
	private ArrayList<BillingPlanObjectUtility> billingPlans;
	private LoadAllBillingPlanObjects allBillingPlans;
	private RIMXMLUtility rimXMLUtility;
	private HibernateUtility hibernateUtility;
	private TransactionLog transactionLog = new TransactionLog();
	private LoadAllProperties properties;
	private EmailTaskExecutor emailExecutor;
	private BrokerService brokerService;
	private String randomPassword;
	private SimpleMailMessage mailMessage;
	private SendSmsToKannelService sendSmsToKannelService;
	
	
	@Autowired
	public void setSendSmsToKannelService(
			SendSmsToKannelService sendSmsToKannelService) {
		this.sendSmsToKannelService = sendSmsToKannelService;
	}

	@Autowired
	public void setMailMessage(SimpleMailMessage mailMessage) {
		this.mailMessage = mailMessage;
	}

	@Autowired
	public void setRandomPassword(String randomPassword) {
		this.randomPassword = randomPassword;
	}

	@Autowired
	public void setBrokerService(BrokerService brokerService) {
		this.brokerService = brokerService;
	}

	@Autowired
	public void setMyEmailTaskExecutor(EmailTaskExecutor myEmailTaskExecutor) {
		this.emailExecutor = myEmailTaskExecutor;
	}

	@Autowired
	public void setRimXMLUtility(RIMXMLUtility rimXMLUtility) {
		this.rimXMLUtility = rimXMLUtility;
	}

	@Autowired
	public void setHibernateUtility(HibernateUtility hibernateUtility) {
		this.hibernateUtility = hibernateUtility;
	}

	@Autowired
	public void setAllBillingPlans(LoadAllBillingPlanObjects allBillingPlans) {
		this.allBillingPlans = allBillingPlans;
	}

	@Autowired
	public void setProperties(LoadAllProperties properties) {
		this.properties = properties;
	}

	/**
	 * This method is used to reset the password user details
	 * 
	 * @param userDetails
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/resetpassword", method = RequestMethod.POST)
	public @ResponseBody
	ResponseDetails resetPasswordUserDetails(@RequestBody Users userDetails,
			HttpServletRequest request) {
		logger.info("Request to create a update User with email "
				+ userDetails.getEmail() + " has been received");
		response = new ResponseDetails();

		try {
			logger.info("The details of the user received to update is as follows - New Password: "
					+ userDetails.getPassword());
			// we get the user object
			// query for the user
			logger.info("searching for user " + userDetails.getUsername());
			Users userDetailSearch = hibernateUtility
					.getUserDetailByUsername(userDetails.getUsername());
			if (userDetailSearch != null) {
				logger.info("user found, setting new password: "
						+ userDetails.getPassword() + ", on old password: "
						+ userDetailSearch.getPassword());
				// we set the new password for this user
				userDetailSearch.setPassword(userDetails.getPassword());
				userDetailSearch.setIsdefaultpassword("NO");
				// update this object
				hibernateUtility.updateBrokerUserDetail(userDetailSearch);
				logger.info("password reset successfully for user "
						+ userDetails.getUsername());
			} else {
				logger.info("user not found..exiting..");

				response.setStatusCode(StatusCodesUI.NO_USER_FOUND);
				response.setDescription(StatusCodesUI.NO_USER_FOUND_MESSAGE);
				return response;
			}
			try {
				logger.info("sending email/sms notification to the user");
				// if successful, we send an email to this user
				// ----------------------------------------------------
				emailExecutor.sendQueuedEmails("Your New Password: "
						+ userDetails.getPassword());

				// -----------------------------------------------------
			} catch (Exception ex) {
				logger.info(ex.getMessage());
			}
			response.setStatusCode(StatusCodesUI.SUCCESS);
			response.setDescription("User " + userDetails.getUsername()
					+ " updated successfully");

			return response;
		} catch (Exception ex) {
			ex.printStackTrace();
			response.setDescription(ex.getMessage());
			return response;
		}
	}

	/**
	 * This method is used to save the user details from the create new user
	 * form of the application
	 * 
	 * @param userDetails
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/updateuserdetail", method = RequestMethod.POST)
	public @ResponseBody
	ResponseDetails updateUserDetails(@RequestBody Users userDetails,
			HttpServletRequest request) {
		logger.info("Request to create a update User with email "
				+ userDetails.getEmail() + " has been received");
		response = new ResponseDetails();

		try {
			if ((userDetails.getEmail() == null || userDetails.getEmail()
					.equalsIgnoreCase(""))
					|| (userDetails.getMsisdn() == null || userDetails
							.getMsisdn().equalsIgnoreCase(""))) {
				response.setStatusCode(StatusCodesUI.USER_EMAIL_MSISDN_CANNOT_BE_EMPTY);
				response.setDescription(StatusCodesUI.USER_EMAIL_MSISDN_CANNOT_BE_EMPTY_MESSAGE);

				logger.info(StatusCodesUI.USER_EMAIL_MSISDN_CANNOT_BE_EMPTY
						+ ", "
						+ StatusCodesUI.USER_EMAIL_MSISDN_CANNOT_BE_EMPTY_MESSAGE);
				return response;
			}

			logger.info("The details of the user received to update is as follows - Name : "
					+ userDetails.getFirstname()
					+ " "
					+ userDetails.getLastname()
					+ " with email : "
					+ userDetails.getEmail());

			// save the details and return a responsedetail
			logger.info("updating User " + userDetails.getUsername());
			this.hibernateUtility.updateBrokerUserDetail(userDetails);

			try {
				// if successful, we send an email to this user
				// ----------------------------------------------------
				emailExecutor.sendQueuedEmails("Your New Password: "
						+ userDetails.getPassword());

				// -----------------------------------------------------
			} catch (Exception ex) {
				logger.info(ex.getMessage());
			}
			response.setStatusCode(StatusCodesUI.SUCCESS);
			response.setDescription("User " + userDetails.getUsername()
					+ " updated successfully");

			return response;
		} catch (Exception ex) {
			ex.printStackTrace();
			return response;
		}
	}

	/**
	 * This method is used to save the user details from the create new user
	 * form of the application
	 * 
	 * @param userDetails
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/saveuserdetail", method = RequestMethod.POST)
	public @ResponseBody
	ResponseDetails saveUserDetails(@RequestBody Users userDetails,
			HttpServletRequest request) {
		logger.info("Request to create a new User with email "
				+ userDetails.getEmail() + " has been received");
		response = new ResponseDetails();

		try {
			if ((userDetails.getEmail() == null || userDetails.getEmail()
					.equalsIgnoreCase(""))
					|| (userDetails.getMsisdn() == null || userDetails
							.getMsisdn().equalsIgnoreCase(""))) {
				response.setStatusCode(StatusCodesUI.USER_EMAIL_MSISDN_CANNOT_BE_EMPTY);
				response.setDescription(StatusCodesUI.USER_EMAIL_MSISDN_CANNOT_BE_EMPTY_MESSAGE);

				return response;
			}

			logger.info("The details of the user received to save is as follows - Name : "
					+ userDetails.getFirstname()
					+ " "
					+ userDetails.getLastname()
					+ " with email : "
					+ userDetails.getEmail());

			// check if the user already exists with the same email or username
			// save the details and return a responsedetail
			logger.info("Saving User " + userDetails.getUsername());
			userDetails.setPassword(randomPassword);
			int response_code = this.hibernateUtility
					.saveBrokerUserDetail(userDetails);

			logger.info("Database response [" + response_code
					+ "] from saving user " + userDetails.getFirstname());
			if (response_code == 0) {
				try {
					// if successful, we send an email to this user
					// ----------------------------------------------------
					sendSmsToKannelService.sendMessageToKannel("Your Password: "
							+ userDetails.getPassword(), userDetails.getMsisdn());
					emailExecutor.sendQueuedEmails("Your Password: "
							+ userDetails.getPassword());

					// -----------------------------------------------------
				} catch (Exception ex) {
					logger.info(ex.getMessage());
				}
				response.setStatusCode(response_code);
				response.setDescription("User " + userDetails.getUsername()
						+ " created successfully");
			}
			return response;
		} catch (Exception ex) {
			ex.printStackTrace();
			return response;
		}
	}

	/**
	 * This is used to save a (prepaid?) subscriber details to the application
	 * 
	 * @param subscriberDetails
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/savesubscriberdetail", method = RequestMethod.POST)
	public @ResponseBody
	ResponseDetails provisionSubscriber(
			@RequestBody SubscriberDetail subscriberDetails,
			HttpServletRequest request) {
		logger.info("Request to provision Subscriber has been received with msisdn: "
				+ subscriberDetails.getMsisdn()
				+ " shortcode: "
				+ subscriberDetails.getShortCode());
		response = new ResponseDetails();
		response.setStatusCode(StatusCodesUI.OTHER_ERRORS);
		response.setDescription("");
		int provisionStatus;

		try {
			if (subscriberDetails.getMsisdn() == null
					|| subscriberDetails.getMsisdn().equalsIgnoreCase("")) {
				response.setStatusCode(StatusCodesUI.SUBSCRIBER_MSISDN_CANNOT_BE_EMPTY);
				response.setDescription(StatusCodesUI.SUBSCRIBER_MSISDN_CANNOT_BE_EMPTY_MESSAGE);
				return response;
			}
			LoadAllBillingPlanObjects loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
					.getBeans("loadAllBillingPlanObjects");
			brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
			provisionStatus = brokerService.activate(subscriberDetails
					.getShortCode(),subscriberDetails);
			response.setStatusCode(provisionStatus);
			response.setDescription(properties.getProperty(String.valueOf(provisionStatus)));
			response.setClassInstance(null);
			return response;
		} catch (Exception ex) {
			response.setStatusCode(StatusCodesUI.OTHER_ERRORS);
			response.setDescription(ex.getMessage());
			return response;
		}
	}

	/**
	 * This is used to save a (prepaid?) subscriber details to the application
	 * 
	 * @param subscriberDetails
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/savesubscriberdetailpost", method = RequestMethod.POST)
	public @ResponseBody
	ResponseDetails saveSubscriberDetailsPost(
			@RequestBody SubscriberDetail subscriberDetails,
			HttpServletRequest request) {
		logger.info("Request to create a new Postpaid Subscriber has been received");
		response = new ResponseDetails();
		response.setStatusCode(StatusCodesUI.OTHER_ERRORS);
		response.setDescription("");

		try {
			if (subscriberDetails.getMsisdn() == null
					|| subscriberDetails.getMsisdn().equalsIgnoreCase("")) {
				response.setStatusCode(StatusCodesUI.SUBSCRIBER_MSISDN_CANNOT_BE_EMPTY);
				response.setDescription(StatusCodesUI.SUBSCRIBER_MSISDN_CANNOT_BE_EMPTY_MESSAGE);
				return response;
			}
			LoadAllBillingPlanObjects loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
					.getBeans("loadAllBillingPlanObjects");
			brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
			int provisionStatus = brokerService.activate(subscriberDetails
					.getShortCode(),subscriberDetails);
			response.setStatusCode(provisionStatus);
			response.setDescription(String.format(properties.getProperty(String.valueOf(provisionStatus)), brokerService.getBillingPlanObject().getDescription()));
			response.setClassInstance(null);
			return response;
		} catch (Exception ex) {
			response.setStatusCode(StatusCodesUI.OTHER_ERRORS);
			response.setDescription(ex.getMessage());
			return response;
		}
	}

	@RequestMapping(value = "/deactivatesubscriberbymsisdn", method = RequestMethod.POST)
	public @ResponseBody
	ResponseDetails deactivateSubscriber(
			@RequestBody SubscriberDetail subscriberDetails,
			HttpServletRequest request) {
		logger.info("Request to deactivate Subscriber has been received");
		response = new ResponseDetails();
		response.setStatusCode(StatusCodesUI.OTHER_ERRORS);
		response.setDescription("");
		try {
			if (subscriberDetails.getMsisdn() == null
					|| subscriberDetails.getMsisdn().equalsIgnoreCase("")) {
				response.setStatusCode(StatusCodesUI.SUBSCRIBER_MSISDN_CANNOT_BE_EMPTY);
				response.setDescription(StatusCodesUI.SUBSCRIBER_MSISDN_CANNOT_BE_EMPTY_MESSAGE);
			}

			try {
				LoadAllBillingPlanObjects loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
						.getBeans("loadDeactivateBillingPlanObjects");
				brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
				int provisionStatus = brokerService.deactivate(subscriberDetails);
				response.setStatusCode(provisionStatus);
				response.setDescription(StatusCodesUI.SUCCESS_MESSAGE);
			} catch (Exception e) {
				e.printStackTrace();
				response.setStatusCode(StatusCodesUI.OTHER_ERRORS);
				response.setDescription(StatusCodesUI.OTHER_ERRORS_MESSAGE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatusCode(StatusCodesUI.OTHER_ERRORS);
			response.setDescription(StatusCodesUI.OTHER_ERRORS_MESSAGE);
		}
		return response;

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/authenticateuserbyid", method = RequestMethod.POST)
	public @ResponseBody
	ResponseDetails authenticateUserById(@RequestBody Users userDetail,
			HttpServletRequest userRequest, HttpSession httpSession)
			throws IOException {
		logger.info("Request to authenticate a new user has been received.");
		logger.info("Getting the user's request info");

		String userName = userDetail.getUsername();
		String client_ip = userRequest.getRemoteAddr();

		response = new ResponseDetails();
		UserLoginDetails responseData = new UserLoginDetails();

		logger.info("Client IP: " + client_ip);
		try {
			Users userDetailSearch = hibernateUtility
					.getUserDetailByUsername(userDetail);

			if (userDetailSearch == null) {
				logger.info("User not found in the application returning code "
						+ StatusCodesUI.NO_USER_FOUND);
				response.setStatusCode(StatusCodesUI.NO_USER_FOUND);
				response.setDescription(StatusCodesUI.NO_USER_FOUND_MESSAGE);
			} else {
				logger.info("User found in the application returning code "
						+ StatusCodesUI.SUCCESS);
				responseData.setId(UUID.randomUUID().toString());
				responseData
						.setDate_created(userDetailSearch.getDate_created());
				responseData.setSessionID(UUID.randomUUID().toString());
				responseData.setEmail(userDetailSearch.getEmail());
				responseData.setUserID(userDetailSearch.getId());
				responseData.setUsertype(userDetailSearch.getUsertype());
				responseData.setRoleid(userDetailSearch.getRoleid());
				logger.info("role types set successfully..");
				response.setUserLoginDetails(responseData);
				response.setClassInstance(responseData);
				response.setStatusCode(StatusCodesUI.SUCCESS);
				response.setDescription(StatusCodesUI.SUCCESS_MESSAGE);

				logger.info("isdefault passsword value "
						+ userDetailSearch.getIsdefaultpassword());
				if (userDetailSearch.getIsdefaultpassword().equalsIgnoreCase(
						"YES")) {
					logger.info("user needs to reset password..");
					logger.info("setting response status code to "
							+ StatusCodesUI.USER_RESET_PASSWORD);
					response.setStatusCode(StatusCodesUI.USER_RESET_PASSWORD);
					response.setDescription(StatusCodesUI.USER_RESET_PASSWORD_MESSAGE);
				}
			}
			logger.info("User session created and returned successfully.");
			return response;
		} catch (Exception ex) {
			ex.printStackTrace();
			response.setStatusCode(StatusCodes.OTHER_ERRORS);
			response.setDescription("Error authenticating User. Please try again later.");
			return response;
		}
	}

	@RequestMapping(value = "/getsubscriberdetail", method = RequestMethod.GET)
	public @ResponseBody
	SubscriberDetail getSubscriberByType(
			@RequestParam("search_type") String searchType,
			@RequestParam("search_value") String searchValue,
			HttpServletRequest request) throws IOException {
		try {
			logger.info("Request to load the values of search " + searchType
					+ "/" + searchValue);
			SubscriberDetail sub = null;

			sub = hibernateUtility.getSubscriberInformation(searchType,
					searchValue);
			logger.info("value of sub = " + sub);

			if (sub == null) {
				SubscriberDetail subb = new SubscriberDetail();
				subb.setId(null);
				return subb;
			}

			return sub;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	/**
	 * This is used to get the details of all the users that are on the
	 * application
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/getalluserdetail", method = RequestMethod.GET)
	public @ResponseBody
	ArrayList<Users> getAllUserDetails(HttpServletRequest request)
			throws IOException {
		try {
			logger.info("Request to load the users on the system has been received");
			// Authenticate the user in the system before you allow them access
			// to the function.

			// query the database to see if the subscriberdetail is returned
			ArrayList<Users> sub = null;

			sub = hibernateUtility.getAllUserDetails();
			logger.info("value of sub = " + sub);

			if (sub == null) {
				sub = new ArrayList<Users>();
				return sub;
			}

			return sub;
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ArrayList<Users>();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "finally" })
	@RequestMapping(value = "/getsubscribertransactionlog", method = RequestMethod.GET)
	public @ResponseBody
	ResponseDetails getSubscriberTransactionLog(
			@RequestParam("msisdn") String msisdn, HttpServletRequest request)
			throws IOException {
		try {
			ResponseDetails response = new ResponseDetails();

			Collection<TransactionLog> transactionsLog = hibernateUtility
					.getSubscriberTransactionLog(msisdn);

			if (transactionsLog != null) {
				response.setStatusCode(StatusCodesUI.SUCCESS);
				response.setDescription(StatusCodesUI.SUCCESS_MESSAGE);
				response.setClassInstance(transactionsLog);
			} else {
				response.setStatusCode(StatusCodesUI.NO_USER_FOUND);
				response.setDescription(StatusCodesUI.NO_USER_FOUND_MESSAGE);
				response.setClassInstance(new SubscriberDetail());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			response.setStatusCode(StatusCodesUI.OTHER_ERRORS);
			response.setDescription(StatusCodesUI.OTHER_ERRORS_MESSAGE);
		} finally {
			return response;
		}
	}

	@RequestMapping(value = "/getAdvancedSubscription", method = RequestMethod.GET)
	public @ResponseBody
	ArrayList<FutureRenewal> getAdvancedSubscription(
			@RequestParam("msisdn") String msisdn, HttpServletRequest request)
			throws IOException {
		ArrayList<FutureRenewal> futurenewal = new ArrayList<FutureRenewal>();
		logger.info("request to show Advanced Subscription for " + msisdn);
		try {
			futurenewal = hibernateUtility.allSubscriberFutureRenewals(msisdn);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return futurenewal;
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "finally" })
	@RequestMapping(value = "/getavailableplans", method = RequestMethod.GET)
	public @ResponseBody
	ResponseDetails getAvailablePlans(
			@RequestParam("searchby") String searchBy,
			HttpServletRequest request) throws IOException {
		logger.info("Call to load the available plans has been made to the broker, sort by "
				+ searchBy);

		// Authentication has to be done to make sure that the user is really
		// authorized to make this call
		try {
			response = new ResponseDetails();

			billingPlans = allBillingPlans.getAllBillingPlans();

			Map<String, String> plans = new LinkedHashMap<String, String>();
			String[] accessRoles = null;

			for (BillingPlanObjectUtility billingPlanObjectUtility : billingPlans) {
				if (billingPlanObjectUtility.getAccess() == null)
					continue;
				accessRoles = billingPlanObjectUtility.getAccess()
						.toLowerCase().split(",");
				if (Arrays.asList(accessRoles).contains(searchBy.toLowerCase())) {
					plans.put(billingPlanObjectUtility.getShortCode(),
							billingPlanObjectUtility.getDescription());
					continue;
				}

			}

			response.setStatusCode(StatusCodesUI.SUCCESS);
			response.setDescription(StatusCodesUI.SUCCESS_MESSAGE);
			response.setClassInstance(plans);
		} catch (Exception ex) {
			response.setStatusCode(StatusCodesUI.OTHER_ERRORS);
			response.setDescription(StatusCodesUI.OTHER_ERRORS_MESSAGE);
			ex.printStackTrace();
		} finally {
			return response;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "finally" })
	@RequestMapping(value = "/getavailableplansbytype/{searchby}", method = RequestMethod.GET)
	public @ResponseBody
	ResponseDetails getAvailablePlansByType(@PathVariable("searchby") String searchBy, 	HttpServletRequest request) throws IOException {
		logger.info("Call to load the available plans by type has been made to the broker, sort by "
				+ searchBy);

		try {
			response = new ResponseDetails();

			billingPlans = allBillingPlans.getAllBillingPlans();

			Map<String, String> plans = new LinkedHashMap<String, String>();
			String[] accessRoles = null;

			for (BillingPlanObjectUtility billingPlanObjectUtility : billingPlans) {
				if (billingPlanObjectUtility.getAccess() == null)
					continue;
				accessRoles = billingPlanObjectUtility.getAccess()
						.toLowerCase().split(",");
				if (Arrays.asList(accessRoles).contains(searchBy.toLowerCase())) {
					plans.put(billingPlanObjectUtility.getShortCode(),
							billingPlanObjectUtility.getDescription());
					continue;
				}
			}

			response.setStatusCode(StatusCodesUI.SUCCESS);
			response.setDescription(StatusCodesUI.SUCCESS_MESSAGE);
			response.setClassInstance(plans);
		} catch (Exception ex) {
			response.setStatusCode(StatusCodesUI.OTHER_ERRORS);
			response.setDescription(StatusCodesUI.OTHER_ERRORS_MESSAGE);
			ex.printStackTrace();
		} finally {
			return response;
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/getavailableroles", method = RequestMethod.GET)
	public @ResponseBody
	ResponseDetails getAvailableRoles(HttpServletRequest request)
			throws IOException {
		logger.info("Call to load the available roles has been made to the broker");

		// Authentication has to be done to make sure that the user is really
		// authorized to make this call
		try {
			response = new ResponseDetails();

			Collection<RolesDetail> responseRoles = hibernateUtility
					.getAllRoles();
			logger.info("The value of the returned roles is " + responseRoles);

			Map<String, String> roles = new LinkedHashMap<String, String>();

			for (RolesDetail tempResponseRoles : responseRoles) {
				roles.put(tempResponseRoles.getId(),
						tempResponseRoles.getRolename());
			}

			response.setStatusCode(StatusCodesUI.SUCCESS);
			response.setDescription(StatusCodesUI.SUCCESS_MESSAGE);
			response.setClassInstance(roles);
		} catch (Exception ex) {
			response.setStatusCode(StatusCodesUI.OTHER_ERRORS);
			response.setDescription(StatusCodesUI.OTHER_ERRORS_MESSAGE);
			ex.printStackTrace();
		} finally {
			return response;
		}
	}

	/**
	 * Device change command
	 **/
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/devicechange", method = RequestMethod.POST)
	public @ResponseBody
	ResponseDetails modifyBBSubscriberNonBillingIdentifier(
			@RequestBody SubscriberDetail userDetail, HttpServletRequest request)
			throws IOException {
		logger.info("in device change action..");
		ResponseDetails userDetailData = new ResponseDetails();
		if (!"".equals(userDetail.getImsi())
				|| !"".equals(userDetail.getMsisdn())) {
			logger.info("Subscriber " + userDetail
					+ " has requested for a no identifier RIM update");

			try {
				rimStatus = rimXMLUtility
						.modifyNonBillingIdentifier(userDetail);
				if (rimStatus.getErrorCode().equalsIgnoreCase("0")
						|| ((new Integer(rimStatus.getErrorCode()) >= 21000 && (new Integer(
								rimStatus.getErrorCode()) <= 21500)))) {
					int hibernateStatus = hibernateUtility
							.updateSubscriberDetail(userDetail);
					if (hibernateStatus != 0) {
						// TODO: Implement code that will notify an
						// administrator of failure
					}
					// Log this transaction to table
					TransactionLog transactionLog = new TransactionLog();

					transactionLog.setDate_created(new GregorianCalendar());
					transactionLog.setDescription("Non-billing ID SWAP");
					transactionLog.setMsisdn(userDetail.getMsisdn());
					transactionLog.setStatus(userDetail.getStatus());
					transactionLog.setService(userDetail.getServiceplan());

					hibernateUtility.saveTransactionlog(transactionLog);

					response.setStatusCode(StatusCodesUI.SUCCESS);
					response.setDescription(StatusCodesUI.SUCCESS_MESSAGE);

					return response;
				} else {
					// Load the error from the properties file and then also
					// load the message part of the error and send back
					response.setStatusCode(new Integer(rimStatus.getErrorCode()));
					response.setDescription(rimStatus.getErrorDescription());

					return response;
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				response.setStatusCode(StatusCodesUI.OTHER_ERRORS);
				response.setDescription(StatusCodesUI.OTHER_ERRORS_MESSAGE);

				return response;
			}

		} else {
			response.setStatusCode(StatusCodesUI.SUBSCRIBER_IMSI_CANNOT_BE_EMPTY);
			response.setDescription(StatusCodesUI.SUBSCRIBER_IMSI_CANNOT_BE_EMPTY_MESSAGE);

			return response;
		}
	}

	/**
	 * SIM SWAP command
	 **/
	@RequestMapping(value = "/simswap", method = RequestMethod.GET)
	public @ResponseBody
	ResponseDetails modifyBBSubscriberBillingIdentifier(
			@RequestParam("oldImsi") String oldImsi,
			@RequestParam("oldImsi") String newImsi, HttpServletRequest request)
			throws IOException {
		ResponseDetails response = new ResponseDetails();

		logger.info("modifyBBSubscriberBillingIdentifier method called to swap old IMSI ["
				+ oldImsi + "] to new IMSI [" + newImsi + "]");
		// get the subscriber using this new imsi
		logger.info("searching for subscrber with imsi: " + oldImsi);
		if (hibernateUtility.getSubscriberInformationByImsi(oldImsi) != null) {
			logger.info("subscriber found..proceed to swapping");
			SubscriberDetail subscriber = hibernateUtility
					.getSubscriberInformationByImsi(oldImsi).iterator().next();
			logger.info("subcriber " + subscriber.getEmail() + ", "
					+ subscriber.getMsisdn() + " with imsi "
					+ subscriber.getImsi() + " found..");

			if (subscriber.getStatus().equalsIgnoreCase("Active")) {
				LoadAllBillingPlanObjects loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
						.getBeans("loadSimSwapBillingPlanObjects");
				brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
				int responseCode = brokerService.simswap(subscriber);
				response.setStatusCode(responseCode);
				response.setDescription(StatusCodesUI.SUCCESS_MESSAGE);
				return response;
			} else {
				response.setStatusCode(StatusCodesUI.SUBSCRIBER_NOT_ACTIVE);
				response.setDescription("Sim Swap cannot be done for a subscriber that is not active...");
				return response;
			}
		}

		return response;
	}

	private ResponseDetails getUserDetailsFromRequest(
			HttpServletRequest servletRequest) throws IOException {
		Enumeration<String> enums = servletRequest.getHeaderNames();
		while (enums.hasMoreElements()) {
			String headerNames = enums.nextElement();
			logger.debug("Header Name =  : " + headerNames + " : "
					+ servletRequest.getHeader(headerNames));
		}
		String userDetails = servletRequest.getHeader("authorization");

		String userDetailsFreeForm = new String(
				new BASE64Decoder().decodeBuffer(userDetails));

		ResponseDetails response = new ResponseDetails();
		response.setStatusCode(StatusCodesUI.SUCCESS);
		response.setDescription(StatusCodesUI.SUCCESS_MESSAGE);
		response.setClassInstance("eg335289475905j57fd47s");

		return response;
		// query the active sessions table to see if the sessionid has an active
		// session in the application

	}

	@SuppressWarnings({ "rawtypes", "unchecked", "finally" })
	@RequestMapping(value = "/getplanbyshortcode", method = RequestMethod.GET)
	public @ResponseBody
	ResponseDetails getServicePlanByShortCode(
			@RequestParam("shortCode") String shortCode,
			HttpServletRequest request) throws IOException {
		logger.info("Call to load the available plans has been made to bb broker");

		try {
			response = new ResponseDetails();

			billingPlans = allBillingPlans.getAllBillingPlans();

			// BillingPlanObjectUtility billingPlanObjectUtilityObj = null;

			Map<String, String> plans = new LinkedHashMap<String, String>();
			String[] accessRoles = null;

			for (BillingPlanObjectUtility billingPlanObjectUtility : billingPlans) {
				if (shortCode.equalsIgnoreCase(billingPlanObjectUtility
						.getShortCode())) {
					response.setClassInstance(billingPlanObjectUtility);
					break;
				}

				accessRoles = billingPlanObjectUtility.getAccess().split(",");

				for (String tempRole : accessRoles) {
					if (shortCode.equalsIgnoreCase(tempRole)) {
						plans.put(billingPlanObjectUtility.getShortCode(),
								billingPlanObjectUtility.getDescription());

						continue;
					}
				}
			}

			response.setStatusCode(StatusCodesUI.SUCCESS);
			response.setDescription(StatusCodesUI.SUCCESS_MESSAGE);
		} catch (Exception ex) {
			response.setStatusCode(StatusCodesUI.OTHER_ERRORS);
			response.setDescription(StatusCodesUI.OTHER_ERRORS_MESSAGE);
			ex.printStackTrace();
		} finally {
			return response;
		}
	}

	/**
	 * This is used to get the details of all the users that are on the
	 * application
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/getallusagereportdetails", method = RequestMethod.GET)
	public @ResponseBody
	ArrayList<DeductionLog> getAllUsageReport(
			@RequestParam("service_search") String service_search,
			@RequestParam("type_search") String type_search,
			@RequestParam("days_search") String days_search,
			@RequestParam("shortcode_search") String shortcode_search,
			HttpServletRequest request) throws IOException {
		try {
			logger.info("Request to load the users on the system has been received");
			// Authenticate the user in the system before you allow them access
			// to the function.

			// query the database to see if the subscriberdetail is returned
			ArrayList<DeductionLog> sub = null;

			sub = hibernateUtility.getRevenueReportByDetail(service_search,
					new Integer(days_search), new Integer(type_search),
					shortcode_search);
			logger.info("===========================");
			logger.info("===========================");

			if (sub == null) {
				sub = new ArrayList<DeductionLog>();
				return sub;
			}

			return sub;
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ArrayList<DeductionLog>();
		}
	}

	/**
	 * This is used to get the details of all the users that are on the
	 * application
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/getuserdetailbyid", method = RequestMethod.GET)
	public @ResponseBody
	ArrayList<Users> getUserDetailById(
			@RequestParam("searchby") String searchBy,
			HttpServletRequest request) throws IOException {
		try {
			logger.info("Request to load the user details on the system has been received");
			// Authenticate the user in the system before you allow them access
			// to the function.

			// query the database to see if the subscriberdetail is returned

			ArrayList<Users> sub = hibernateUtility
					.getUserDetailsById(searchBy);
			logger.info("===========================");
			logger.info("===========================");

			return sub;
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ArrayList<Users>();
		}
	}

	/**
	 * This is used to get the details of all the users that are on the
	 * application
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/getallmarketingusagereportdetails", method = RequestMethod.GET)
	public @ResponseBody
	ArrayList<DeductionLog> getAllMarketingUsageReport(
			@RequestParam("days_search") String days_search,
			HttpServletRequest request) throws IOException {
		try {
			logger.info("Request to load the users on the system has been received");
			// Authenticate the user in the system before you allow them access
			// to the function.

			// query the database to see if the subscriberdetail is returned
			ArrayList<DeductionLog> sub = null;

			sub = hibernateUtility.getRevenueReportBySummation(new Integer(
					days_search));
			logger.info("===========================");
			logger.info("===========================");
			logger.info("value of sub = " + sub);

			return sub;
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ArrayList<DeductionLog>();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "finally" })
	@RequestMapping(value = "/getavailableshortcodesreport", method = RequestMethod.GET)
	public @ResponseBody
	ResponseDetails getAvailableShortcodeReport(HttpServletRequest request)
			throws IOException {
		logger.info("Call to load the available shortcodes for reports has been made to the broker");

		// Authentication has to be done to make sure that the user is really
		// authorized to make this call
		try {
			response = new ResponseDetails();

			Collection<String> responseRoles = hibernateUtility
					.getAllShortcodesForReport();
			logger.info("The value of the returned roles is " + responseRoles);

			response.setStatusCode(StatusCodesUI.SUCCESS);
			response.setDescription(StatusCodesUI.SUCCESS_MESSAGE);
			response.setClassInstance(responseRoles);
		} catch (Exception ex) {
			response.setStatusCode(StatusCodesUI.OTHER_ERRORS);
			response.setDescription(StatusCodesUI.OTHER_ERRORS_MESSAGE);
			ex.printStackTrace();
		} finally {
			return response;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "finally" })
	@RequestMapping(value = "/getavailableervicesreport", method = RequestMethod.GET)
	public @ResponseBody
	ResponseDetails getAvailableServiceReport(HttpServletRequest request)
			throws IOException {
		logger.info("Call to load the available services for reports has been made to the broker");

		// Authentication has to be done to make sure that the user is really
		// authorized to make this call
		try {
			response = new ResponseDetails();

			Collection<String> responseRoles = hibernateUtility
					.getAllServicesForReport();
			logger.info("The value of the returned roles is " + responseRoles);

			response.setStatusCode(StatusCodesUI.SUCCESS);
			response.setDescription(StatusCodesUI.SUCCESS_MESSAGE);
			response.setClassInstance(responseRoles);
		} catch (Exception ex) {
			response.setStatusCode(StatusCodesUI.OTHER_ERRORS);
			response.setDescription(StatusCodesUI.OTHER_ERRORS_MESSAGE);
			ex.printStackTrace();
		} finally {
			return response;
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/deleteuserbyid", method = RequestMethod.GET)
	public @ResponseBody
	ResponseDetails deleteUserById(@RequestParam("userid") String userId,
			HttpServletRequest request) throws IOException {
		logger.info("Call to delete a user with ID = " + userId
				+ " has been made to the broker");

		// Authentication has to be done to make sure that the user is really
		// authorized to make this call
		try {
			response = new ResponseDetails();
			int status = 0;

			Collection<Users> responseRoles = hibernateUtility
					.getUserDetailsById(userId);
			logger.info("The value of the returned user is " + responseRoles);

			for (Users tempResponseRoles : responseRoles) {
				status = hibernateUtility
						.deleteUserdetailById(tempResponseRoles);

				if (status == 0) {
					response.setStatusCode(StatusCodesUI.SUCCESS);
					response.setDescription(StatusCodesUI.SUCCESS_MESSAGE);
				} else {
					response.setStatusCode(StatusCodesUI.OTHER_ERRORS);
					response.setDescription(StatusCodesUI.OTHER_ERRORS_MESSAGE);
				}
			}
		} catch (Exception ex) {
			response.setStatusCode(StatusCodesUI.OTHER_ERRORS);
			response.setDescription(StatusCodesUI.OTHER_ERRORS_MESSAGE);
			ex.printStackTrace();
		} finally {
			return response;
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/resetuserbyid", method = RequestMethod.GET)
	public @ResponseBody
	ResponseDetails resetUserById(@RequestParam("userid") String userId,
			HttpServletRequest request) throws IOException {
		logger.info("Call to reset a user with ID = " + userId
				+ " has been made to the broker");

		// Authentication has to be done to make sure that the user is really
		// authorized to make this call
		try {
			response = new ResponseDetails();
			int status = 0;

			Collection<Users> responseRoles = hibernateUtility
					.getUserDetailsById(userId);
			logger.info("The value of the returned user is " + responseRoles);

			for (Users tempResponseRoles : responseRoles) {
				logger.info("About to generate password");
				// update the user password and save the update
				tempResponseRoles.setPassword(randomPassword);

				status = hibernateUtility
						.updateUserdetailById(tempResponseRoles);

				if (status == 0) {
					response.setStatusCode(StatusCodesUI.SUCCESS);
					response.setDescription(StatusCodesUI.SUCCESS_MESSAGE);

					String resetusermailbodyMessage = String.format(
							properties.getProperty("resetusermailbody"),
							tempResponseRoles.getLastname(), randomPassword,
							properties.getProperty("webserviceURL")
									+ "/blackberry/login");

					sendSmsToKannelService.sendMessageToKannel(resetusermailbodyMessage, tempResponseRoles.getMsisdn());
					String[] receipentList = new String[1];
					receipentList[0] = tempResponseRoles.getEmail();

					logger.info("About to send this email to " + receipentList);
					logger.info("About to send this email"
							+ resetusermailbodyMessage);
					mailMessage.setSubject("BB Broker Password Reset");
					emailExecutor.setMailMessage(mailMessage);
					emailExecutor.sendQueuedEmails(resetusermailbodyMessage,
							receipentList);
				} else {
					response.setStatusCode(StatusCodesUI.OTHER_ERRORS);
					response.setDescription(StatusCodesUI.OTHER_ERRORS_MESSAGE);
				}
			}
		} catch (Exception ex) {
			response.setStatusCode(StatusCodesUI.OTHER_ERRORS);
			response.setDescription(StatusCodesUI.OTHER_ERRORS_MESSAGE);
			ex.printStackTrace();
		} finally {
			return response;
		}
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/unlockuserbyid", method = RequestMethod.GET)
	public @ResponseBody
	ResponseDetails unlockUserById(@RequestParam("userid") String userId,
			HttpServletRequest request) throws IOException {
		logger.info("Call to unlock a user with ID = " + userId
				+ " has been made to the broker");

		// Authentication has to be done to make sure that the user is really
		// authorized to make this call
		try {
			response = new ResponseDetails();
			int status = 0;

			Collection<Users> responseRoles = hibernateUtility
					.getUserDetailsById(userId);
			logger.info("The value of the returned user is " + responseRoles);

			for (Users tempResponseRoles : responseRoles) {
				// TODO: update the islocked parameter
				status = hibernateUtility
						.updateUserdetailById(tempResponseRoles);

				if (status == 0) {
					response.setStatusCode(StatusCodesUI.SUCCESS);
					response.setDescription(StatusCodesUI.SUCCESS_MESSAGE);

				} else {
					response.setStatusCode(StatusCodesUI.OTHER_ERRORS);
					response.setDescription(StatusCodesUI.OTHER_ERRORS_MESSAGE);
				}
			}
		} catch (Exception ex) {
			response.setStatusCode(StatusCodesUI.OTHER_ERRORS);
			response.setDescription(StatusCodesUI.OTHER_ERRORS_MESSAGE);
			ex.printStackTrace();
		} finally {
			return response;
		}
	}

}
