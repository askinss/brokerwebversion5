package com.vasconsulting.www.controllers;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vasconsulting.www.domain.ErrorDetail;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.CommandFactory;
import com.vasconsulting.www.invokers.CommandInvoker;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.CommandPropertiesUtility;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.LoadAllBillingPlanObjects;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.StatusCodes;
import com.vasconsulting.www.utility.XMLUtility;

@Controller
public class USSDHandler
{
	
	private LoadAllBillingPlanObjects billingPlanObjects;
	private CommandInvoker commandInvoker;
	private CommandFactory commandFactory;
	private EmailTaskExecutor myEmailTaskExecutor;
	private XMLUtility xmlUtility;
	private GregorianCalendar calendar = new GregorianCalendar();
	Logger logger = Logger.getLogger(USSDHandler.class);
	private ErrorDetail errorDetail;
	private HibernateUtility hibernateUtility;	
	private TransactionLog transactionLog = new TransactionLog();
	private LoadAllProperties properties;
	
	@Autowired
	public void setProperties(LoadAllProperties properties)
	{
		this.properties = properties;
	}
	
	@Autowired
	public void setHibernateUtility(HibernateUtility hibernateUtility)
	{
		this.hibernateUtility = hibernateUtility;
	}
	
	@Autowired
	public void setXmlUtility(XMLUtility xmlUtility)
	{
		this.xmlUtility = xmlUtility;
	}
	
	@Autowired
	public void setMyEmailTaskExecutor(EmailTaskExecutor myEmailTaskExecutor)
	{
		this.myEmailTaskExecutor = myEmailTaskExecutor;
	}
	
	@Autowired
	public void setCommandFactory(CommandFactory commandFactory)
	{
		this.commandFactory = commandFactory;
	}
		
	@Autowired
	public void setCommandInvoker(CommandInvoker commandInvoker)
	{
		this.commandInvoker = commandInvoker;
	}

	@Autowired
	public void setBillingPlanObjects(LoadAllBillingPlanObjects billingPlanObjects)
	{
		this.billingPlanObjects = billingPlanObjects;
	}
	
	private GregorianCalendar getNextSubscriptionDate(int noOfDays){
		GregorianCalendar calendar1 = new GregorianCalendar();
		calendar1.add(GregorianCalendar.DAY_OF_MONTH, new Integer(noOfDays).intValue());
		return calendar1;
	}
	
	private String stripLeadingMsisdnPrefix(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("+")){
			return Msisdn.substring(1, Msisdn.length());
		}
		else return msisdn;
	}
	
	/**
	 * This method is used to get the last day of the month.
	 * @param noOfDays
	 * @return
	 */
	private GregorianCalendar getLastDateOfMonth(){
		int year = Calendar.getInstance().get(Calendar.YEAR);
		int month = Calendar.getInstance().get(Calendar.MONTH);
		int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
		
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.set(year, month, day);		
		return calendar;
	}
	
	
	@RequestMapping(value="/ussdserviceuganda", method = RequestMethod.POST)
	@ResponseBody
	public void generateSubscriberUSSDMenu(@RequestParam("msisdn") String msisdn, @RequestParam("msg") String msg,
			HttpServletResponse responseToGateway) throws IOException
	{
		/*logger.info(request);//msisdn=265994675363&type=1&msg=*655*101%23
		String requestParams[] = request.split("&");
		Map<String, String> requestParamsValue = new HashMap<String, String>();
		
		for (String string : requestParams)
		{
			String requestParamsValue1[] = string.split("=");
			requestParamsValue.put(requestParamsValue1[0], requestParamsValue1[1]);
		}
		
		String msg = requestParamsValue.get("msg").replace("%23", "#");*/
		/**
		 * This next line is commented to accomodate the menu in UG
		 */
		//msg = "*"+msg+"#";
		logger.info("Subscriber "+msisdn+" has sent in the request "+msg);
		System.out.println("Subscriber "+msisdn+" has sent in the request "+msg);
		
		transactionLog.setId(UUID.randomUUID().toString());
		transactionLog.setMsisdn(msisdn);
		transactionLog.setShortcode(msg);
		transactionLog.setDate_created(new GregorianCalendar());
		transactionLog.setDescription("SHORTCODE");
		transactionLog.setStatus("SUCCESSFUL");
		
		hibernateUtility.saveTransactionlog(transactionLog);
		
		ArrayList<BillingPlanObjectUtility> billingPlans = billingPlanObjects.getAllBillingPlans();
		
		int provisionStatus = -1;
		boolean messageFound = false;
		
		for (BillingPlanObjectUtility billingPlanObjectUtility : billingPlans) {
			
			if (billingPlanObjectUtility.getShortCode().trim().equalsIgnoreCase(msg.trim()))
			{
				messageFound = true;
				logger.info("Billing Plan match found for shortcode : "+msg+" from subscriber "+msisdn);
				logger.info("=====Setting up the subscriber and the selected plan in Session=====");
				
				BillingPlanObjectUtility billingPlanObjectLive = (BillingPlanObjectUtility)ContextLoaderImpl.getBeans("billingPlanObject");				
				billingPlanObjectLive.setCost(billingPlanObjectUtility.getCost());
				billingPlanObjectLive.setStatus(billingPlanObjectUtility.getStatus());
				billingPlanObjectLive.setDescription(billingPlanObjectUtility.getDescription());
				billingPlanObjectLive.setExternalData(billingPlanObjectUtility.getExternalData());
				billingPlanObjectLive.setShortCode(billingPlanObjectUtility.getShortCode());
				billingPlanObjectLive.setSuccessMessage(billingPlanObjectUtility.getSuccessMessage());
				billingPlanObjectLive.setValidity(billingPlanObjectUtility.getValidity());
				billingPlanObjectLive.setCoomandObjects(billingPlanObjectUtility.getCoomandObjects());
				
				SubscriberDetail subscriberDetail = (SubscriberDetail)ContextLoaderImpl.getBeans("subscriberDetail");
				subscriberDetail.setMsisdn(stripLeadingMsisdnPrefix(msisdn));
				subscriberDetail.setServicetype(new Integer(billingPlanObjectUtility.getValidity()).intValue());
				subscriberDetail.setLast_subscription_date(calendar);
				subscriberDetail.setDate_created(calendar);
				subscriberDetail.setNext_subscription_date(getNextSubscriptionDate(new Integer(billingPlanObjectUtility.getValidity())));
				subscriberDetail.setPrepaidSubscriber(1);
				subscriberDetail.setAutoRenew(1);
				subscriberDetail.setServicetype(new Integer(billingPlanObjectUtility.getValidity()));
				subscriberDetail.setShortCode(billingPlanObjectUtility.getShortCode());
				
				
				logger.info("======Finalized setup=======");
				
				ArrayList<CommandPropertiesUtility> commandProperties = billingPlanObjectUtility.getCoomandObjects();
				
				commandInvoker = new CommandInvoker();
				
				for (CommandPropertiesUtility commandProps : commandProperties)
				{
					commandInvoker.addCommand(commandFactory.GetCommandInstance(commandProps.getCommand(), "com.vasconsulting.www.interfaces.impl", 
							commandProps.getParam()));					
				}
				
				provisionStatus = commandInvoker.provision();
				
				logger.info("Provision request result is "+provisionStatus+" for subscriber with MSISDN = "+msisdn);				
				
				if (provisionStatus != StatusCodes.SUCCESS)
				{
					responseToGateway.addHeader("FreeFlow", "FB");
					responseToGateway.addHeader("Charge", "N");
					responseToGateway.getWriter().
					write( properties.getProperty(new Integer(provisionStatus).toString()));
					
					logger.error("The request to provision "+msisdn+" failed with code "+provisionStatus);
					
					ErrorDetail errorDetails = new ErrorDetail();
					errorDetails.setMsisdn(msisdn);
					errorDetails.setErrorCode(provisionStatus);
					errorDetails.setDate_created(new GregorianCalendar());
					errorDetails.setDescription("Shortcode request["+msg+"]failed with code "+ provisionStatus
							+". Please check and ensure the subscriber is in sync");
					
					hibernateUtility.saveErrorLogs(errorDetails);
					
					/*myEmailTaskExecutor.sendQueuedEmails("The request to provision "+
							msisdn+ "failed with code"+ provisionStatus+". Please" +
							"make sure that the subscribers profile is in Sync with state on RIM and in the database.");*/
				}
				else
				{
					responseToGateway.addHeader("FreeFlow", "FB");
					responseToGateway.addHeader("Charge", "N");
					responseToGateway.getWriter().
					write(billingPlanObjectUtility.getSuccessMessage());
				}
			}
		}
		
		if (!messageFound)
		{
			logger.info("No match found for shortcode "+msg+" sent in by "+msisdn);
			responseToGateway.addHeader("FreeFlow", "FB");
			responseToGateway.addHeader("Charge", "N");
			responseToGateway.getWriter().write(properties. getProperty("incorrectkeywordmessage"));
		}
		
	}
	
	
	
	@RequestMapping(value="/ussdservicev2", method = RequestMethod.GET)
	public void provisionSubscriberv2(@RequestParam("msisdn") String msisdn, @RequestParam("msg") String msg, Writer writer)
	{
		ArrayList<BillingPlanObjectUtility> billingPlans = billingPlanObjects.getAllBillingPlans();
		logger.info("New request received to process msg= "+msg+" from msisdn="+msisdn);
		int provisionStatus = -1;
		boolean stringFound = false;
		
		for (BillingPlanObjectUtility billingPlanObjectUtility : billingPlans) {		
			
			if (billingPlanObjectUtility.getShortCode().equalsIgnoreCase(msg))
			{
				stringFound = true;
				BillingPlanObjectUtility billingPlanObjectLive = (BillingPlanObjectUtility)ContextLoaderImpl.getBeans("billingPlanObject");				
				billingPlanObjectLive.setCost(billingPlanObjectUtility.getCost());
				billingPlanObjectLive.setStatus(billingPlanObjectUtility.getStatus());
				billingPlanObjectLive.setDescription(billingPlanObjectUtility.getDescription());
				billingPlanObjectLive.setExternalData(billingPlanObjectUtility.getExternalData());
				billingPlanObjectLive.setShortCode(billingPlanObjectUtility.getShortCode());
				billingPlanObjectLive.setSuccessMessage(billingPlanObjectUtility.getSuccessMessage());
				billingPlanObjectLive.setValidity(billingPlanObjectUtility.getValidity());
				billingPlanObjectLive.setCoomandObjects(billingPlanObjectUtility.getCoomandObjects());
				
				GregorianCalendar calendar = new GregorianCalendar();
				
				SubscriberDetail subscriberDetail = (SubscriberDetail)ContextLoaderImpl.getBeans("subscriberDetail");
				subscriberDetail.setMsisdn(stripLeadingMsisdnPrefix(msisdn));
				subscriberDetail.setLast_subscription_date(calendar);
				subscriberDetail.setServicetype(new Integer(billingPlanObjectUtility.getValidity()).intValue());
				subscriberDetail.setDate_created(calendar);
				subscriberDetail.setAutoRenew(1);
				subscriberDetail.setNext_subscription_date(getNextSubscriptionDate(new Integer(billingPlanObjectUtility.getValidity()).intValue()));
				subscriberDetail.setShortCode(billingPlanObjectUtility.getShortCode());
				
				logger.info("Value of shortcode["+subscriberDetail.getMsisdn()+"] = "+subscriberDetail.getShortCode());
				logger.info("Value of Next_subscription_date["+subscriberDetail.getMsisdn()+"] = "+subscriberDetail.getNext_subscription_date());
				
				
				logger.info("======Finalized setup=======");
				
				ArrayList<CommandPropertiesUtility> commandProperties = billingPlanObjectUtility.getCoomandObjects();
				
				CommandInvoker commandInvoker = new CommandInvoker();
				
				for (CommandPropertiesUtility commandProps : commandProperties)
				{
					commandInvoker.addCommand(commandFactory.GetCommandInstance(commandProps.getCommand(), "com.vasconsulting.www.interfaces.impl", 
							commandProps.getParam()));	
										
				}
				
				provisionStatus = commandInvoker.provision();
				
				logger.info("Provision request result is "+provisionStatus+" for subscriber with MSISDN = "+msisdn);
				
				try
				{
					if (provisionStatus != StatusCodes.SUCCESS)
					{
						
						writer.write(properties.getProperty(new Integer(provisionStatus).toString()));
						
						logger.error("Provisioning request for "+msisdn+" exited with error code "+provisionStatus
								+". Please check and rectify manually");
						
						errorDetail = new ErrorDetail();
						errorDetail.setDate_created(calendar);
						errorDetail.setDescription("Provisioning Response from USSDHandler controller");
						errorDetail.setErrorCode(provisionStatus);
						errorDetail.setErrorType("");
						errorDetail.setMsisdn(msisdn);
						
						try{
							hibernateUtility.saveErrorLogs(errorDetail);
						}
						catch(Exception ex){
							myEmailTaskExecutor.sendQueuedEmails("Provisioning request for "+msisdn+" exited with error code "+provisionStatus
								+". Please check and rectify manually");
						}
					}
					else
					{
						writer.write(billingPlanObjectUtility.getSuccessMessage());
						return;
					}
				}
				catch(IOException ex){
					ex.printStackTrace();
				}
			}				
		}
		if (!stringFound){
			try {
				writer.write(properties.getProperty("incorrectkeywordmessage"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	
	@RequestMapping(value="/ussdservice", method = RequestMethod.GET)
	public void provisionSubscriber(@RequestParam("msisdn") String msisdn, @RequestParam("msg") String msg, Writer writer)
	{
		ArrayList<BillingPlanObjectUtility> billingPlans = billingPlanObjects.getAllBillingPlans();
		logger.info("New request received to process msg= "+msg+" from msisdn="+msisdn);
		int provisionStatus = -1;
		boolean stringFound = false;
		
		for (BillingPlanObjectUtility billingPlanObjectUtility : billingPlans) {		
			
			if (billingPlanObjectUtility.getShortCode().equalsIgnoreCase(msg))
			{
				stringFound = true;
				BillingPlanObjectUtility billingPlanObjectLive = (BillingPlanObjectUtility)ContextLoaderImpl.getBeans("billingPlanObject");				
				billingPlanObjectLive.setCost(billingPlanObjectUtility.getCost());
				billingPlanObjectLive.setStatus(billingPlanObjectUtility.getStatus());
				billingPlanObjectLive.setDescription(billingPlanObjectUtility.getDescription());
				billingPlanObjectLive.setExternalData(billingPlanObjectUtility.getExternalData());
				billingPlanObjectLive.setShortCode(billingPlanObjectUtility.getShortCode());
				billingPlanObjectLive.setSuccessMessage(billingPlanObjectUtility.getSuccessMessage());
				billingPlanObjectLive.setValidity(billingPlanObjectUtility.getValidity());
				billingPlanObjectLive.setCoomandObjects(billingPlanObjectUtility.getCoomandObjects());
				
				GregorianCalendar calendar = new GregorianCalendar();
				
				SubscriberDetail subscriberDetail = (SubscriberDetail)ContextLoaderImpl.getBeans("subscriberDetail");
				subscriberDetail.setMsisdn(stripLeadingMsisdnPrefix(msisdn));
				subscriberDetail.setLast_subscription_date(calendar);
				subscriberDetail.setServicetype(new Integer(billingPlanObjectUtility.getValidity()).intValue());
				subscriberDetail.setDate_created(calendar);
				subscriberDetail.setAutoRenew(1);
				subscriberDetail.setNext_subscription_date(getNextSubscriptionDate(new Integer(billingPlanObjectUtility.getValidity()).intValue()));
				subscriberDetail.setShortCode(billingPlanObjectUtility.getShortCode());
				
				logger.info("Value of shortcode["+subscriberDetail.getMsisdn()+"] = "+subscriberDetail.getShortCode());
				logger.info("Value of Next_subscription_date["+subscriberDetail.getMsisdn()+"] = "+subscriberDetail.getNext_subscription_date());
				
				
				logger.info("======Finalized setup=======");
				
				ArrayList<CommandPropertiesUtility> commandProperties = billingPlanObjectUtility.getCoomandObjects();
				
				CommandInvoker commandInvoker = new CommandInvoker();
				
				for (CommandPropertiesUtility commandProps : commandProperties)
				{
					commandInvoker.addCommand(commandFactory.GetCommandInstance(commandProps.getCommand(), "com.vasconsulting.www.interfaces.impl", 
							commandProps.getParam()));	
										
				}
				
				provisionStatus = commandInvoker.provision();
				
				logger.info("Provision request result is "+provisionStatus+" for subscriber with MSISDN = "+msisdn);
				
				try
				{
					if (provisionStatus != StatusCodes.SUCCESS)
					{
						
						writer.write(xmlUtility.generateAtosResponse(
								properties.getProperty(new Integer(provisionStatus).toString()),3));
						
						logger.error("Provisioning request for "+msisdn+" exited with error code "+provisionStatus
								+". Please check and rectify manually");
						
						errorDetail = new ErrorDetail();
						errorDetail.setDate_created(calendar);
						errorDetail.setDescription("Provisioning Response from USSDHandler controller");
						errorDetail.setErrorCode(provisionStatus);
						errorDetail.setErrorType("");
						errorDetail.setMsisdn(msisdn);
						
						try{
							hibernateUtility.saveErrorLogs(errorDetail);
						}
						catch(Exception ex){
							myEmailTaskExecutor.sendQueuedEmails("Provisioning request for "+msisdn+" exited with error code "+provisionStatus
								+". Please check and rectify manually");
						}
					}
					else
					{
						writer.write(xmlUtility.generateAtosResponse(billingPlanObjectUtility.getSuccessMessage(),3));
						return;
					}
				}
				catch(IOException ex){
					ex.printStackTrace();
				}
			}				
		}
		if (!stringFound){
			try {
				writer.write(xmlUtility.generateAtosResponse(
						properties.getProperty("incorrectkeywordmessage"),3));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/*@RequestMapping(value="/ussdpostservice", method = RequestMethod.POST)
	@ResponseBody
	public void provisionSubscriberByPost(@RequestBody String msg, Writer writer)
	{
		
		logger.info("The value sent in from the Gateway is "+msg);
		HashMap<String , String> ussdRequestMap = xmlUtility.getHomiscoUSSDXmlAsMap(msg);
				
		String msisdn = ussdRequestMap.get("MOBILE_NUMBER");
		String sequence = ussdRequestMap.get("SEQUENCE");
		String sessionid = ussdRequestMap.get("SESSION_ID");
		StringBuilder messageSent = new StringBuilder("*"+ussdRequestMap.get("SERVICE_KEY"));
		
		if (ussdRequestMap.containsKey("USSD_BODY")){
			messageSent.append("*"+ussdRequestMap.get("USSD_BODY")+"#");
		}
		else
		{
			messageSent.append("#");
		}
						
		ArrayList<BillingPlanObjectUtility> billingPlans = billingPlanObjects.getAllBillingPlans();
		logger.info("New request received to process msg= "+messageSent.toString()+" from msisdn ="+msisdn);
		System.out.println("New request received to process msg= "+messageSent.toString()+" from msisdn ="+msisdn);
		
		int provisionStatus = -1;
		boolean stringFound = false;
		String message = messageSent.toString();
		
		for (BillingPlanObjectUtility billingPlanObjectUtility : billingPlans) {		
			
			if (billingPlanObjectUtility.getShortCode().equalsIgnoreCase(message))
			{
				stringFound = true;
				BillingPlanObjectUtility billingPlanObjectLive = (BillingPlanObjectUtility)ContextLoaderImpl.getBeans("billingPlanObject");				
				billingPlanObjectLive.setCost(billingPlanObjectUtility.getCost());
				billingPlanObjectLive.setStatus(billingPlanObjectUtility.getStatus());
				billingPlanObjectLive.setDescription(billingPlanObjectUtility.getDescription());
				billingPlanObjectLive.setExternalData(billingPlanObjectUtility.getExternalData());
				billingPlanObjectLive.setShortCode(billingPlanObjectUtility.getShortCode());
				billingPlanObjectLive.setSuccessMessage(billingPlanObjectUtility.getSuccessMessage());
				billingPlanObjectLive.setValidity(billingPlanObjectUtility.getValidity());
				billingPlanObjectLive.setCoomandObjects(billingPlanObjectUtility.getCoomandObjects());
				
				GregorianCalendar calendar = new GregorianCalendar();
				
				SubscriberDetail subscriberDetail = (SubscriberDetail)ContextLoaderImpl.getBeans("subscriberDetail");
				subscriberDetail.setMsisdn(stripLeadingMsisdnPrefix(msisdn));
				subscriberDetail.setLast_subscription_date(calendar);
				subscriberDetail.setServicetype(new Integer(billingPlanObjectUtility.getValidity()).intValue());
				subscriberDetail.setDate_created(calendar);
				subscriberDetail.setAutoRenew(1);
				subscriberDetail.setNext_subscription_date(getNextSubscriptionDate(new Integer(billingPlanObjectUtility.getValidity()).intValue()));
				subscriberDetail.setShortCode(billingPlanObjectUtility.getShortCode());
				subscriberDetail.setPrepaidSubscriber(1);
				
				logger.info("Value of shortcode["+subscriberDetail.getMsisdn()+"] = "+subscriberDetail.getShortCode());
				logger.info("Value of Next_subscription_date["+subscriberDetail.getMsisdn()+"] = "+subscriberDetail.getNext_subscription_date());
				
				
				logger.info("======Finalized setup=======");
				
				ArrayList<CommandPropertiesUtility> commandProperties = billingPlanObjectUtility.getCoomandObjects();
				
				CommandInvoker commandInvoker = new CommandInvoker();
				
				for (CommandPropertiesUtility commandProps : commandProperties)
				{
					commandInvoker.addCommand(commandFactory.GetCommandInstance(commandProps.getCommand(), "com.vasconsulting.www.interfaces.impl", 
							commandProps.getParam()));	
										
				}
				
				provisionStatus = commandInvoker.provision();
				
				logger.info("Provision request result is "+provisionStatus+" for subscriber with MSISDN = "+msisdn);
				
				try
				{
					if (provisionStatus != StatusCodes.SUCCESS)
					{
						
						writer.write(xmlUtility.generateHomiscoResponse(sessionid, sequence, 
								properties.getProperty(new Integer(provisionStatus).toString()), "FALSE"));
						
						logger.error("Provisioning request for "+msisdn+" exited with error code "+provisionStatus
								+". Please check and rectify manually");
						
						errorDetail = new ErrorDetail();
						errorDetail.setDate_created(calendar);
						errorDetail.setDescription("Provisioning Response from USSDHandler controller");
						errorDetail.setErrorCode(provisionStatus);
						errorDetail.setErrorType("");
						errorDetail.setMsisdn(msisdn);
						
						try{
							hibernateUtility.saveErrorLogs(errorDetail);
						}
						catch(Exception ex){
							myEmailTaskExecutor.sendQueuedEmails("Provisioning request for "+msisdn+" exited with error code "+provisionStatus
								+". Please check and rectify manually");
						}
					}
					else
					{
						writer.write(xmlUtility.generateHomiscoResponse(sessionid, sequence, billingPlanObjectUtility.getSuccessMessage(), "FALSE"));
						
						return;
					}
				}
				catch(IOException ex){
					ex.printStackTrace();
				}
			}				
		}
		if (!stringFound){
			try {
				writer.write(xmlUtility.generateHomiscoResponse(sessionid, sequence, properties.getProperty("incorrectkeywordmessage"), "FALSE"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}*/
	
}
