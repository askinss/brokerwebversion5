package com.vasconsulting.www.services;


import java.util.ArrayList;
import java.util.Collection;

import javax.jws.WebService;

import com.vasconsulting.www.domain.ActivityLogger;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;

@WebService
public interface ServiceDeliveryPlatform {
	//load the subscribers details
	public SubscriberDetail getSubscriberDetails(String param, String paramValue);
	//load subscribers transaction log details
	public Collection<TransactionLog> getSubscriberTransactionLog(String msisdn);
	//load subscriber last provisioning log entry
	public void getSubscriberLastProvisioningActions();
	//general purpose provisioning method
	
	public int provisionSubscriber(String msisdn, String message);
	
	//blackberry specific provisioning method	
	public int provisionBBSubscriber(SubscriberDetail subscriberDetail, String billingPlan);
	//suspend the subscribers details on RIM - Blackberry
	public int suspendBBSubscriber(String msisdn, String message);
	//resume the subscribers details on RIM - Blackberry
	public int resumeBBSubscriber(String msisdn, String message);
	//de-activate the subscribers details on RIM - Blackberry
	public int deProvisionBBSubscriber(SubscriberDetail subscriber);
	//modify the subscribers non billing details on RIM - Blackberry
	public int modifyBBSubscriberNonBillingIdentifier(SubscriberDetail subscriber);
	//modify the subscribers billing details on RIM - Blackberry
	public int modifyBBSubscriberBillingIdentifier(SubscriberDetail subscriber, String newImsi);
	//modify the subscribers demographic details on RIM - Blackberry
	public int modifyBBSubscriberDemographicDetails(SubscriberDetail subscriber);
	//load the billing plans in the system
	public ArrayList<BillingPlanObjectUtility> loadBillingPlan();
	
	public int logService(ActivityLogger logger);
	
	public int createReportSumation(String reportName);
	
	public ArrayList<SubscriberDetail> createReportTabular(String reportName);
	
	public int provisionPrepBBSubscriber(SubscriberDetail subscriber, String billingPlan);

}
