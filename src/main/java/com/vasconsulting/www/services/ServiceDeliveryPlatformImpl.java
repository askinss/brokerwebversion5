package com.vasconsulting.www.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;

import javax.jws.WebService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.vasconsulting.www.domain.ActivityLogger;
import com.vasconsulting.www.domain.ErrorDetail;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.domain.RequestTracker;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.CommandFactory;
import com.vasconsulting.www.invokers.CommandInvoker;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.BrokerService;
import com.vasconsulting.www.utility.CommandPropertiesUtility;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.LoadAllBillingPlanObjects;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.RIMQueryUtil;
import com.vasconsulting.www.utility.RIMXMLResponseDetail;
import com.vasconsulting.www.utility.RIMXMLUtility;
import com.vasconsulting.www.utility.SendSmsToKannelService;
import com.vasconsulting.www.utility.StatusCodes;
import com.vasconsulting.www.utility.XMLUtility;

@WebService(endpointInterface = "com.vasconsulting.www.services.ServiceDeliveryPlatform")
public class ServiceDeliveryPlatformImpl implements ServiceDeliveryPlatform {
	private HibernateUtility hibernateUtility;
	private LoadAllBillingPlanObjects allBillingPlans;
	private ArrayList<BillingPlanObjectUtility> billingPlans;
	private Logger logger = Logger.getLogger(ServiceDeliveryPlatformImpl.class);
	private RIMQueryUtil rimQueryUtility;
	private RIMXMLUtility rimUtility = new RIMXMLUtility();
	private RIMXMLResponseDetail rimStatus;
	private CommandInvoker commandInvoker;
	private CommandFactory commandFactory;
	XMLUtility xmlUtility = new XMLUtility();
	private SendSmsToKannelService smsService;
	private LoadAllProperties properties;
	private BrokerService brokerService;


	@Autowired
	public void setBrokerService(BrokerService brokerService) {
		this.brokerService = brokerService;
	}

	private EmailTaskExecutor emailExecutor;
	private ErrorDetail errorDetail;

	public void setMyEmailTaskExecutor(EmailTaskExecutor myEmailTaskExecutor) {
		this.emailExecutor = myEmailTaskExecutor;
	}

	public void setProperties(LoadAllProperties properties) {
		this.properties = properties;
	}

	public void setSmsService(SendSmsToKannelService smsService) {
		this.smsService = smsService;
	}

	public void setRimQueryUtility(RIMQueryUtil rimQueryUtility) {
		this.rimQueryUtility = rimQueryUtility;
	}

	public void setCommandFactory(CommandFactory commandFactory) {
		this.commandFactory = commandFactory;
	}

	public void setCommandInvoker(CommandInvoker commandInvoker) {
		this.commandInvoker = commandInvoker;
	}

	public void setAllBillingPlans(LoadAllBillingPlanObjects allBillingPlans) {
		this.allBillingPlans = allBillingPlans;
	}

	public void setHibernateUtility(HibernateUtility hibernateUtility) {
		this.hibernateUtility = hibernateUtility;
	}

	public SubscriberDetail getSubscriberDetails(String prameter,
			String parameterValue) {
		return this.hibernateUtility.getSubscriberInformation(prameter,
				parameterValue);
	}

	public Collection<TransactionLog> getSubscriberTransactionLog(String msisdn) {
		return hibernateUtility.getSubscriberTransactionLog(msisdn);
	}

	public void getSubscriberLastProvisioningActions() {
		// TODO Auto-generated method stub

	}

	public int provisionSubscriber(String msisdn, String message) {
		return StatusCodes.INVALID_MESSAGE;
	}

	public int provisionPrepBBSubscriber(SubscriberDetail subscriber,
			String billingPlan) {
		try{
			if (subscriber.getMsisdn().equalsIgnoreCase(""))
				return StatusCodes.OTHER_ERRORS;
			return brokerService.activate(billingPlan, subscriber);
		}catch(Exception e){
			logger.info("There was a general failure in the processing of this subscriber "
					+ subscriber.getMsisdn()
					+ ". Please check subscriber "
					+ "details and manually process if applicable or try again.");
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;
		}

	}

	public int provisionBBSubscriber(SubscriberDetail subscriber,
			String billingPlan) {
		try {
			if (subscriber.getMsisdn().equalsIgnoreCase(""))
				return StatusCodes.OTHER_ERRORS;
			int provisionStatus = brokerService.activate(billingPlan, subscriber);
			return provisionStatus;
		} catch (Exception ex) {
			logger.info("There was a general failure in the processing of this subscriber "
					+ subscriber.getMsisdn()
					+ ". Please check subscriber "
					+ "details and manually process if applicable or try again.");
			ex.printStackTrace();
			/**
			 * TODO: Implement what should be done in case of error/success.
			 */
			return StatusCodes.OTHER_ERRORS;
		}

	}

	public int suspendBBSubscriber(String msisdn, String message) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int resumeBBSubscriber(String msisdn, String message) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int deProvisionBBSubscriber(SubscriberDetail subscriber) {
		try {
			int respCode = brokerService.deactivate(subscriber);
			return respCode;
		} catch (Exception e) {
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;
		}
	}

	public int modifyBBSubscriberNonBillingIdentifier(
			SubscriberDetail subscriber) {
		if (!"".equals(subscriber.getImsi())
				|| !"".equals(subscriber.getMsisdn())) {
			int rimStatus = -1;

			RIMXMLResponseDetail responseData = rimUtility
					.modifyNonBillingIdentifier(subscriber);
			rimStatus = new Integer(responseData.getErrorCode()).intValue();

			if (rimStatus == 0 || (rimStatus >= 21000 && rimStatus <= 21500)) {
				int hibernateStatus = hibernateUtility
						.updateSubscriberDetail(subscriber);
				if (hibernateStatus != 0) {
					// TODO: Implement code that will notify an administrator of
					// failure
				}
				// Log this transaction to table
				TransactionLog transactionLog = new TransactionLog();

				transactionLog.setDate_created(new GregorianCalendar());
				transactionLog.setDescription("Non-billing ID SWAP");
				transactionLog.setMsisdn(subscriber.getMsisdn());
				transactionLog.setStatus(subscriber.getStatus());
				transactionLog.setService(subscriber.getServiceplan());

				hibernateUtility.saveTransactionlog(transactionLog);
				return StatusCodes.SUCCESS;
			} else
				return StatusCodes.OTHER_ERRORS;
		} else
			return StatusCodes.IMSI_AND_MSISDN_CANNOT_BE_EMPTY;
	}

	public int modifyBBSubscriberBillingIdentifier(SubscriberDetail subscriber,
			String newImsi) {
		try {
		if (!"".equals(subscriber.getImsi()) || !"".equals(newImsi)) {
			return brokerService.simswap(subscriber);
		} else
			return StatusCodes.IMSI_AND_MSISDN_CANNOT_BE_EMPTY;
		}catch(Exception e){
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;
		}
	}

	private String stripLeadingMsisdnPrefix(String msisdn) {
		String Msisdn = msisdn;
		if (msisdn.startsWith("0")) {
			return Msisdn.substring(1, Msisdn.length());
		} else if (Msisdn.startsWith("255")) {
			return Msisdn.substring(3, Msisdn.length());
		} else if (Msisdn.startsWith("+255")) {
			return Msisdn.substring(4, Msisdn.length());
		} else
			return Msisdn;
	}

	public int modifyBBSubscriberDemographicDetails(SubscriberDetail subscriber) {
		if (!"".equals(subscriber.getImsi())
				|| !"".equals(subscriber.getMsisdn())) {
			return hibernateUtility.updateSubscriberDetail(subscriber);
		} else
			return StatusCodes.IMSI_AND_MSISDN_CANNOT_BE_EMPTY;
	}

	public ArrayList<BillingPlanObjectUtility> loadBillingPlan() {
		logger.info("Request to get all billing plans has been received.");
		ArrayList<BillingPlanObjectUtility> billingplans = allBillingPlans.getAllBillingPlans();
		for (BillingPlanObjectUtility billingplan : billingplans){
			logger.info("Shortcode retrieved is: "+billingplan.getShortCode());
		}
		return allBillingPlans.getAllBillingPlans();
	}

	/**
	 * This method is used to get the next subscription date of a subscribers
	 * lifecycle based on the supplied validity.
	 * 
	 * @param noOfDays
	 * @return
	 */
	private GregorianCalendar getNextSubscriptionDate(int noOfDays) {
		GregorianCalendar calendar1 = new GregorianCalendar();
		calendar1.add(GregorianCalendar.DAY_OF_MONTH,
				new Integer(noOfDays).intValue());
		return calendar1;
	}

	/**
	 * This method is used to get the last day of the month.
	 * 
	 * @param noOfDays
	 * @return
	 */
	private GregorianCalendar getLastDateOfMonth() {
		GregorianCalendar calendar = new GregorianCalendar();
		int dayOfMonth = calendar
				.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);

		int daysToAdd = dayOfMonth
				- calendar.get(GregorianCalendar.DAY_OF_MONTH);

		calendar.add(GregorianCalendar.DAY_OF_MONTH, daysToAdd);
		return calendar;
	}

	private GregorianCalendar getDateOfMonth(int numberOfDays) {
		GregorianCalendar calendar = new GregorianCalendar();

		calendar.add(GregorianCalendar.DAY_OF_MONTH, numberOfDays);
		return calendar;
	}

	public int logService(ActivityLogger log) {
		logger.info("Request to save all audit request has been received.");
		return hibernateUtility.logService(log);
	}

	public ArrayList<SubscriberDetail> createReportTabular(String reportName) {
		logger.info("Received a new report request named " + reportName);
		if (reportName.equalsIgnoreCase("dailyrenewreport")) {
			return hibernateUtility.getRenewalReportTabular();
		} else if (reportName.equalsIgnoreCase("monthlycreatereport")) {
			ArrayList<SubscriberDetail> allSubscribers = hibernateUtility
					.getActivationReportTabular(30);
			logger.info("Number of records returned is "
					+ allSubscribers.size());
			return allSubscribers;
		} else if (reportName.equalsIgnoreCase("weeklycreatereport")) {
			return hibernateUtility.getActivationReportTabular(7);
		} else {
			return hibernateUtility.getActivationReportTabular(1);
		}
	}

	public int createReportSumation(String reportName) {
		if (reportName.equalsIgnoreCase("dailyrenewreport")) {
			return hibernateUtility.getReportSumation("next_subscription_date",
					1);
		} else if (reportName.equalsIgnoreCase("monthlycreatereport")) {
			return hibernateUtility.getReportSumation("date_created", 30);
		} else if (reportName.equalsIgnoreCase("weeklycreatereport")) {
			return hibernateUtility.getReportSumation("date_created", 7);
		} else {
			return hibernateUtility.getReportSumation("date_created", 1);
		}
	}

}
