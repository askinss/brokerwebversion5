package com.vasconsulting.www.domain;

import java.util.Calendar;
import java.util.UUID;

public class DeductionLog {

        private String id;
        private String msisdn;
        private String imsi;
        
        public String getImsi()
		{
			return imsi;
		}


		public void setImsi(String imsi)
		{
			this.imsi = imsi;
		}


		private String service;
		private String shortcode;
        private double amount;
        private int isprepaidsubscriber;
        
        public int getIsprepaidsubscriber()
		{
			return isprepaidsubscriber;
		}


		public void setIsprepaidsubscriber(int isprepaidsubscriber)
		{
			this.isprepaidsubscriber = isprepaidsubscriber;
		}


		private Calendar date_created;
        


        public String getService()
        {
                return service;
        }


        public void setService(String service)
        {
                this.service = service;
        }


        public DeductionLog(){}
        
                
        public String getId() {          
                return (id == null)? UUID.randomUUID().toString() : this.id;
        }
        public void setId(String id) {
                this.id = id;
        }
        
        
        public String getMsisdn() {
                return msisdn;
        }

        public void setMsisdn(String msisdn) {
                this.msisdn = msisdn;
        }
        
        public Calendar getDate_created()
        {
                return this.date_created;
        }

        public void setDate_created(Calendar date_created)
        {
                this.date_created = date_created;
        }

        public double getAmount()
        {
                return amount;
        }
        
        public void setAmount(double amount)
        {
                this.amount = amount;
        }
        

        public String getShortcode()
		{
			return shortcode;
		}


		public void setShortcode(String shortcode)
		{
			this.shortcode = shortcode;
		}


		@Override
        public String toString() {
                String s = "MSISDN:" + msisdn +"; Service:"+service+"; Amount:"+amount+"; Date:"+date_created;
                return s;
        }

}