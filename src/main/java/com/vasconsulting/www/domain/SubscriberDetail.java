package com.vasconsulting.www.domain;

import java.util.Calendar;
import java.util.UUID;

public class SubscriberDetail{
	
	private String id;
	private String firstname;
	private String middlename;
	private String lastname;
	private String email;
	private String serviceplan;
	private String status;
	private String pin;
	private String imei;
	private String msisdn;
	private String imsi;
	private int servicetype;
	private Calendar date_created;
	private Calendar next_subscription_date;
	private Calendar last_subscription_date; 
	private int prepaidSubscriber;//takes 0 for no and 1 for yes - this is mutually exclusive to postpaidsubscriber column
	private int postpaidSubscriber;//takes 0 for no and 1 for yes
	private int autoRenew;	//takes 0 for no and 1 for yes
	private String shortCode;
	
	public String getShortCode()
	{
		return shortCode;
	}


	public void setShortCode(String shortCode)
	{
		this.shortCode = shortCode;
	}


	public int getAutoRenew() {
		return autoRenew;
	}


	public void setAutoRenew(int autoRenew) {
		this.autoRenew = autoRenew;
	}


	public int getPrepaidSubscriber() {
		return (this.prepaidSubscriber != 0)?this.prepaidSubscriber:0;
	}


	public void setPrepaidSubscriber(int prepaidSubscriber) {
		this.prepaidSubscriber = prepaidSubscriber;
	}


	public int getPostpaidSubscriber() {
		return (this.postpaidSubscriber != 0)?this.postpaidSubscriber:0;
	}


	public void setPostpaidSubscriber(int postpaidSubscriber) {
		this.postpaidSubscriber = postpaidSubscriber;
	}

	public int getServicetype() {
		return servicetype;
	}


	public void setServicetype(int servicetype) {
		this.servicetype = servicetype;
	}


	public SubscriberDetail(){}
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getId() {		 
		return (id == null)? UUID.randomUUID().toString() : this.id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	public String getServiceplan() {
		return serviceplan;
	}
	public void setServiceplan(String serviceplan) {
		this.serviceplan = serviceplan;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public Calendar getDate_created() {
		return date_created;
	}

	public void setDate_created(Calendar date_created) {
		this.date_created = date_created;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public Calendar getLast_subscription_date() {
		return last_subscription_date;
	}

	public void setLast_subscription_date(Calendar last_subscription_date) {
		this.last_subscription_date = last_subscription_date;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public Calendar getNext_subscription_date() {
		return next_subscription_date;
	}

	public void setNext_subscription_date(Calendar next_subscription_date) {
		this.next_subscription_date = next_subscription_date;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}


	public String getImsi() {
		return imsi;
	}


	public void setImsi(String imsi) {
		this.imsi = imsi;
	}


	@Override
	public String toString() {
		String s = "NAME:" + firstname + " " + middlename + "," + lastname +"\nPIN:" + pin + "\nIMSI:" + 
		imsi + "\nMSISDN:" + msisdn	+"\nIMEI:" + imei+"\nServiceType:" + servicetype+"\nServicePlan:" + 
		serviceplan;
		return s;
	}
	
}
