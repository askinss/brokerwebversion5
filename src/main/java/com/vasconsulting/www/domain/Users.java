package com.vasconsulting.www.domain;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.UUID;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.vasconsulting.www.utility.EncryptAndDecrypt;


public class Users
{
	private String id;
	private String firstname;
	private String lastname;
	private String encryptedPassword;
	private EncryptAndDecrypt encryptAndDecrypt = new EncryptAndDecrypt();
	
	@Size(min=3, max=20, message="Username must be between 6 to 30 characters long.")
	@Pattern(regexp="^[a-zA-Z0-9]+$", message="Username must be alphanumeric with no spaces")
	private String username;	
	
	@Pattern(regexp="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,4}", message="Invalid email address.")
	private String email;
	private String msisdn;
	private Calendar date_created;
	
	@Size(min=8, message="The password must be at least 6 characters long.")
	@Pattern(regexp="^.*(?=.{8,})(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&+]).*$", 
	message="Password must be at least 8 chars, contain at least one digit, one lower and one upper " +
			"alpha xter and at least one special xter (@#$%&+ etc.)")
	private String password;
	private String isdefaultpassword;
	private String islocked;
	private String usertype;
	
	private String roleid;
	
	public String getIslocked()
	{
		return islocked;
	}
	public void setIslocked(String islocked)
	{
		this.islocked = islocked;
	}
	public String getUsername()
	{
		return username;
	}
	public void setUsername(String username)
	{
		this.username = username;
	}
	public String getUsertype()
	{
		return usertype;
	}
	public void setUsertype(String usertype)
	{
		this.usertype = usertype;
	}
	public String getId()
	{
		return (id == null)? UUID.randomUUID().toString() : this.id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public String getFirstname()
	{
		return firstname;
	}
	public void setFirstname(String firstname)
	{
		this.firstname = firstname;
	}
	public String getLastname()
	{
		return lastname;
	}
	public void setLastname(String lastname)
	{
		this.lastname = lastname;
	}
	public String getEmail()
	{
		return email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	public Calendar getDate_created()
	{
		return (date_created == null) ? new GregorianCalendar() : date_created;
	}
	public void setDate_created(Calendar date_created)
	{
		this.date_created = date_created;
	}
	
	public String getPassword()
	{
		return password;
	}
	
	public void setPassword(String password)
	{
		this.password = password;
		try {
			this.encryptedPassword = encryptAndDecrypt.encrypt(this.password);
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
		
	public String getEncryptedPassword() {
		return encryptedPassword;
	}
	
	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}
	
	public String getMsisdn()
	{
		return msisdn;
	}
	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}
	public String getIsdefaultpassword()
	{
		return (isdefaultpassword == null)? "YES" : this.isdefaultpassword;
	}
	public void setIsdefaultpassword(String isdefaultpassword)
	{
		this.isdefaultpassword = isdefaultpassword;
	}
	public String toString()
	{
		return "{MSISDN="+msisdn+", FIRSTNAME="+firstname+", LASTNAME="+lastname+", EMAIL="+email+"}";
	}
	public String getRoleid()
	{
		return roleid;
	}
	public void setRoleid(String rolesid)
	{
		this.roleid = rolesid;
	}
		
	
}