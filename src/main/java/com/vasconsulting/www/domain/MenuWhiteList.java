package com.vasconsulting.www.domain;

import com.vasconsulting.www.utility.MD5;

public class MenuWhiteList{
	
	private String id;
	
	private String msisdn;
	
	
	public MenuWhiteList(){}
	
		
	public String getId() {		 
		return (id == null)? MD5.getDigest(System.currentTimeMillis()+""):this.id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}



	@Override
	public String toString() {
		String s = "MSISDN:" + msisdn;
		return s;
	}
	
}
