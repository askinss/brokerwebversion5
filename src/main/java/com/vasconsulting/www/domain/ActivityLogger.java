package com.vasconsulting.www.domain;

import java.util.Calendar;
import java.util.UUID;

public class ActivityLogger
{
	private String id;
	private String action;
	private String description;
	private String username;
	private Calendar date_created;
	private String host_ip;
	private String msisdn;
	
	public String getId()
	{
		return (id == null)? UUID.randomUUID().toString() : this.id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public String getAction()
	{
		return action;
	}
	public void setAction(String action)
	{
		this.action = action;
	}
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	public String getUsername()
	{
		return username;
	}
	public void setUsername(String username)
	{
		this.username = username;
	}
	public Calendar getDate_created()
	{
		return date_created;
	}
	public void setDate_created(Calendar date_created)
	{
		this.date_created = date_created;
	}
	public String getHost_ip()
	{
		return host_ip;
	}
	public void setHost_ip(String host_ip)
	{
		this.host_ip = host_ip;
	}
	public String getMsisdn()
	{
		return msisdn;
	}
	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}
	
}
