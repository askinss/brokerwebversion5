package com.vasconsulting.www.domain;

import com.vasconsulting.www.utility.MD5;

public class BillingPlan {
	private String id;
	private String shortcode;
	private String description;
	private int validity;
	private String davalue;
	private String cost;
	private String services;
	private int status;
	
	public String getId() {
		return (id == null)? MD5.getDigest(System.currentTimeMillis()+""):this.id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getShortcode() {
		return shortcode;
	}
	public void setShortcode(String shortcode) {
		this.shortcode = shortcode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getValidity() {
		return validity;
	}
	public void setValidity(int validity) {
		this.validity = validity;
	}
	public String getDavalue() {
		return davalue;
	}
	public void setDavalue(String davalue) {
		this.davalue = davalue;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getServices() {
		return services;
	}
	public void setServices(String services) {
		this.services = services;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
		
}
