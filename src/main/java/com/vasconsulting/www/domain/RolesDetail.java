package com.vasconsulting.www.domain;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class RolesDetail
{
	private String id;
	private String rolename;
	private String description;
	
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	public String getId()
	{
		return (id == null)? UUID.randomUUID().toString() : this.id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public String getRolename()
	{
		return rolename;
	}
	public void setRolename(String rolename)
	{
		this.rolename = rolename;
	}
	
	public String toString()
	{
		return "{ID : "+id+", ROLENAME : "+rolename+"}";
	}
}