package com.vasconsulting.www.domain;


/**
 * This is a class that is used to return the status of a request to a client.
 * @author nnamdi
 *
 */
public class ResponseDetails
{
	private int statusCode;
	private String description;	
	private Object classInstance;
	
	private UserLoginDetails userLoginDetails;
	
	public UserLoginDetails getUserLoginDetails() 
	{
		return userLoginDetails;
	}
	public void setUserLoginDetails(UserLoginDetails userLoginDetails) 
	{
		this.userLoginDetails = userLoginDetails;
	}
	public Object getClassInstance()
	{
		return classInstance;
	}
	public void setClassInstance(Object classInstance)
	{
		this.classInstance = classInstance;
	}
	public int getStatusCode()
	{
		return statusCode;
	}
	public void setStatusCode(int statusCode)
	{
		this.statusCode = statusCode;
	}
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
				
}