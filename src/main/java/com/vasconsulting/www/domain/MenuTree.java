package com.vasconsulting.www.domain;

import java.util.Calendar;
import java.util.UUID;

public class MenuTree {
	private String id;
	private String parent_id;
	private String bb_shortcode;
	private String shortcode;
	private String serialno;
	private int rownumber;
	
	public String getId() {
		return (this.id == null || this.id.equalsIgnoreCase(""))? 
                UUID.randomUUID().toString() : this.id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getParent_id() {
		return parent_id;
	}
	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}
	public String getBb_shortcode() {
		return bb_shortcode;
	}
	public void setBb_shortcode(String bb_shortcode) {
		this.bb_shortcode = bb_shortcode;
	}
	public String getShortcode() {
		return shortcode;
	}
	public void setShortcode(String shortcode) {
		this.shortcode = shortcode;
	}
	public String getSerialno() {
		return serialno;
	}
	public void setSerialno(String serialno) {
		this.serialno = serialno;
	}
	
	
	
	public int getRownumber() {
		return rownumber;
	}
	public void setRownumber(int rownumber) {
		this.rownumber = rownumber;
	}
	public String toString()
	{
		return "id="+id+"paren_id="+parent_id;
	}
	
}
