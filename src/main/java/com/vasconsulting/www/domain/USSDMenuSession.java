package com.vasconsulting.www.domain;

public class USSDMenuSession 
{
	private String msisdn;
	private String sequence_value;
	private String menu_ids;
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getSequence_value() {
		return sequence_value;
	}
	public void setSequence_value(String sequence_value) {
		this.sequence_value = sequence_value;
	}
	public String getMenu_ids() {
		return menu_ids;
	}
	public void setMenu_ids(String menu_ids) {
		this.menu_ids = menu_ids;
	}
	
	
}
