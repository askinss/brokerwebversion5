package com.vasconsulting.www.domain;

import java.util.GregorianCalendar;
import java.util.UUID;

public class FutureRenewal {
	private String id;
	private String msisdn;
	private String shortCode;
	private GregorianCalendar purchasedAt;
	private GregorianCalendar usedAt;
	private String status;

	public String getId() {
		return (id == null)? UUID.randomUUID().toString() : this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public GregorianCalendar getPurchasedAt() {
		return purchasedAt;
	}

	public void setPurchasedAt(GregorianCalendar purchasedAt) {
		this.purchasedAt = purchasedAt;
	}

	public GregorianCalendar getUsedAt() {
		return usedAt;
	}

	public void setUsedAt(GregorianCalendar usedAt) {
		this.usedAt = usedAt;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

//	public String toString() {
//		return "msisdn: " + this.msisdn +  " shortCode: "
//				+ this.shortCode + " purchasedAt: "
//				+ this.purchasedAt.getTime().toString() + " status: " + this.status.toString();
//	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
}
