package com.vasconsulting.www.domain;

import java.util.Calendar;
import java.util.UUID;

public class Ttspromo {
	private String id;
	private String msisdn;
	private String imei;
	private Calendar date_created;
	private Calendar last_subscription_date;
	private Calendar next_subscription_date;
	private String imsi;
	private int usage_count;
	
	public String getId() {
        return (this.id == null || this.id.equalsIgnoreCase(""))? 
                UUID.randomUUID().toString() : this.id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public Calendar getDate_created() {
		return date_created;
	}
	public void setDate_created(Calendar date_created) {
		this.date_created = date_created;
	}
	public Calendar getLast_subscription_date() {
		return last_subscription_date;
	}
	public void setLast_subscription_date(Calendar last_subscription_date) {
		this.last_subscription_date = last_subscription_date;
	}
	public Calendar getNext_subscription_date() {
		return next_subscription_date;
	}
	public void setNext_subscription_date(Calendar next_subscription_date) {
		this.next_subscription_date = next_subscription_date;
	}
	public int getUsage_count() {
		return usage_count;
	}
	public void setUsage_count(int usage_count) {
		this.usage_count = usage_count;
	}
	public String getImsi() {
		return imsi;
	}
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}
}
