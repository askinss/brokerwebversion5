package com.vasconsulting.www.domain;

import java.util.Calendar;
import java.util.UUID;

public class BbTestPromoFirstYes {
	private String id;
	private String msisdn;
	private String imsi;
	private String serviceplan;
	private Calendar date_created;
	private Calendar date_updated;
	private Calendar due_date;
	private int status;
	private String rimstatus;
	
	public String getId() {
		return (this.id == null || this.id.equalsIgnoreCase(""))? 
                UUID.randomUUID().toString() : this.id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public Calendar getDate_created() {
		return date_created;
	}
	public void setDate_created(Calendar date_created) {
		this.date_created = date_created;
	}
	public Calendar getDate_updated() {
		return date_updated;
	}
	public void setDate_updated(Calendar date_updated) {
		this.date_updated = date_updated;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Calendar getDue_date() {
		return due_date;
	}
	public void setDue_date(Calendar due_date) {
		this.due_date = due_date;
	}
	public String getImsi() {
		return imsi;
	}
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}
	public String getServiceplan() {
		return serviceplan;
	}
	public void setServiceplan(String serviceplan) {
		this.serviceplan = serviceplan;
	}
	public String getRimstatus() {
		return rimstatus;
	}
	public void setRimstatus(String rimstatus) {
		this.rimstatus = rimstatus;
	}
}
