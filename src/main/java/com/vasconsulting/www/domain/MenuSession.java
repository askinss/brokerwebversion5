package com.vasconsulting.www.domain;

import java.util.Calendar;
import java.util.UUID;

public class MenuSession {
	private String id;
	private String msisdn;
	private String sessionid;	
	private String current_msg;
	private Calendar date_created;
	private String parent_id;
	
	public String getId() {
		return (this.id == null || this.id.equalsIgnoreCase(""))? 
                UUID.randomUUID().toString() : this.id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSessionid() {
		return sessionid;
	}
	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public Calendar getDate_created() {
		return date_created;
	}
	public void setDate_created(Calendar date_created) {
		this.date_created = date_created;
	}
	public String getCurrent_msg() {
		return current_msg;
	}
	public void setCurrent_msg(String current_msg) {
		this.current_msg = current_msg;
	}
	public String getParent_id() {
		return parent_id;
	}
	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}
	
	
	
}
