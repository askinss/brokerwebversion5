package com.vasconsulting.www.domain;

import java.util.GregorianCalendar;
import java.util.UUID;

public class ReconStatus {

	private String id;
	private String msisdn;
	private String newimsi;
	private String oldimsi;
	private GregorianCalendar transdate;
	private String status;

	public String getId() {
		return (id == null)? UUID.randomUUID().toString() : this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	
	public String getNewimsi() {
		return newimsi;
	}

	public void setNewimsi(String newimsi) {
		this.newimsi = newimsi;
	}
	
	public String getOldimsi() {
		return oldimsi;
	}

	public void setOldimsi(String oldimsi) {
		this.oldimsi = oldimsi;
	}	
	

	public GregorianCalendar getTransdate() {
		return transdate;
	}

	public void setTransdate(GregorianCalendar transdate) {
		this.transdate = transdate;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
