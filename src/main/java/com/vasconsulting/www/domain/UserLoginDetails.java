package com.vasconsulting.www.domain;

import java.util.Calendar;
import java.util.UUID;

/**
 * This is a class that is used to hold the login details of a user.
 * @author nnamdi
 *
 */
public class UserLoginDetails
{
	private String id;
	private String fullName;
	private String userID;
	private String sessionID;
	private String usertype;
	private String roleid;
	private Calendar date_created;
	
	public String getUserID()
	{
		return userID;
	}
	public void setUserID(String userID)
	{
		this.userID = userID;
	}
	public String getSessionID()
	{
		return sessionID;
	}
	public void setSessionID(String sessionID)
	{
		this.sessionID = sessionID;
	}
	
	
	public String getId()
	{
		return (id == null)? UUID.randomUUID().toString() : this.id;
	}
	public void setId(String pid)
	{
		this.id = pid;
	}
	
	
	public String getEmail()
	{
		return fullName;
	}
	public void setEmail(String userid)
	{
		this.fullName = userid;
	}
	
	public Calendar getDate_created()
	{
		return date_created;
	}
	public void setDate_created(Calendar date_created)
	{
		this.date_created = date_created;
	}
	public String getFullName()
	{
		return fullName;
	}
	public void setFullName(String fullName)
	{
		this.fullName = fullName;
	}
	public String getUsertype()
	{
		return usertype;
	}
	public void setUsertype(String usertype)
	{
		this.usertype = usertype;
	}
	public String getRoleid()
	{
		return roleid;
	}
	public void setRoleid(String roleid)
	{
		this.roleid = roleid;
	}
	
	
	
}