package com.vasconsulting.www.domain;

public class SubscriberMenuStatus {
	private String msisdn;
	private String menuroot;
	private String ussdbody;
	private String oldussdbody;
	
	
	public String getMsisdn()
	{
		return msisdn;
	}
	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}
	public String getMenuroot()
	{
		return menuroot;
	}
	public void setMenuroot(String menuroot)
	{
		this.menuroot = menuroot;
	}
	public String getUssdbody()
	{
		return ussdbody;
	}
	public void setUssdbody(String ussdbody)
	{
		this.ussdbody = ussdbody;
	}
	public String getOldussdbody()
	{
		return oldussdbody;
	}
	public void setOldussdbody(String oldussdbody)
	{
		this.oldussdbody = oldussdbody;
	}
	
	
	
		
}
