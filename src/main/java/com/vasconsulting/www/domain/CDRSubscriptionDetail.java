package com.vasconsulting.www.domain;

import java.util.Calendar;

import com.vasconsulting.www.utility.MD5;

public class CDRSubscriptionDetail{
	
	private String id;
	private String serviceName;
	public String getServiceName()
	{
		return serviceName;
	}
	public void setServiceName(String serviceName)
	{
		this.serviceName = serviceName;
	}
	private String msisdn;
	private String imsi;
	private int amount;
	private String apnid;
	private String shortCode;
	private int subscriberType;
	private Calendar date_created;
	private String source;
	private String status;
	
	
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	public String getId() {		 
		return (id == null)? MD5.getDigest(System.currentTimeMillis()+""):this.id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMsisdn()
	{
		return msisdn;
	}
	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}
	public String getImsi()
	{
		return imsi;
	}
	public void setImsi(String imsi)
	{
		this.imsi = imsi;
	}
	public int getAmount()
	{
		return amount;
	}
	public void setAmount(int amount)
	{
		this.amount = amount;
	}
	public String getApnid()
	{
		return apnid;
	}
	public void setApnid(String apnid)
	{
		this.apnid = apnid;
	}
	public String getShortCode()
	{
		return shortCode;
	}
	public void setShortCode(String shortCode)
	{
		this.shortCode = shortCode;
	}
	public int getSubscriberType()
	{
		return subscriberType;
	}
	public void setSubscriberType(int subscriberType)
	{
		this.subscriberType = subscriberType;
	}
	public Calendar getDate_created()
	{
		return date_created;
	}
	public void setDate_created(Calendar date_created)
	{
		this.date_created = date_created;
	}
	public String getSource()
	{
		return source;
	}
	public void setSource(String source)
	{
		this.source = source;
	}
		
}
