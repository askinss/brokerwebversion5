/**
 * 
 */
package com.vasconsulting.www.domain;

/**
 * @author Ayotunde
 *
 */
public class ResponseClass 
{
	private int statusCode;
	public int getStatusCode()
	{
		return statusCode;
	}
	public void setStatusCode(int statusCode)
	{
		this.statusCode = statusCode;
	}
	private String statusMessage;
	public String getStatusMessage()
	{
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage)
	{
		this.statusMessage = statusMessage;
	}
}
