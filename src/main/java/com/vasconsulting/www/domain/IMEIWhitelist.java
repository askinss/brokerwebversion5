package com.vasconsulting.www.domain;

import java.util.Calendar;
import java.util.UUID;

public class IMEIWhitelist {
	
	private String id;
	private String imei;
	private Calendar date_created;
	private Calendar date_updated;
	private String status;
	private String used_by_msisdn;
	
	public String getId() {
		return (this.id == null || this.id.equalsIgnoreCase(""))? 
                UUID.randomUUID().toString() : this.id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	
	public Calendar getDate_created() {
		return date_created;
	}
	public void setDate_created(Calendar date_created) {
		this.date_created = date_created;
	}
	public Calendar getDate_updated() {
		return date_updated;
	}
	public void setDate_updated(Calendar date_updated) {
		this.date_updated = date_updated;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getUsed_by_msisdn() {
		return used_by_msisdn;
	}
	public void setUsed_by_msisdn(String used_by_msisdn) {
		this.used_by_msisdn = used_by_msisdn;
	}
	
}
