package com.vasconsulting.www.domain;

import java.util.Calendar;
import java.util.UUID;

public class ErrorDetail{
	
	private String id;
	
	private String msisdn;
	private Calendar date_created;
	private String errorType;
	private int errorCode;
	private String description;
	
	
	public int getErrorCode()
	{
		return errorCode;
	}


	public void setErrorCode(int errorCode)
	{
		this.errorCode = errorCode;
	}


	public String getDescription()
	{
		return description;
	}


	public void setDescription(String description)
	{
		this.description = description;
	}


	public Calendar getDate_created()
	{
		return date_created;
	}


	public void setDate_created(Calendar date_created)
	{
		this.date_created = date_created;
	}


	public String getErrorType()
	{
		return errorType;
	}


	public void setErrorType(String errorType)
	{
		this.errorType = errorType;
	}


	public ErrorDetail(){}
	
		
	public String getId() {		 
		return (id == null)? UUID.randomUUID().toString() : this.id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}



	@Override
	public String toString() {
		String s = "MSISDN:" + msisdn;
		return s;
	}
	
}
