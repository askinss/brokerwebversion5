package com.vasconsulting.www.domain;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.UUID;


public class RequestTracker {
        private Calendar date_created;
        private String msisdn;
        private String shortCode;
        private String id;
        
        public String getMsisdn(){
                return msisdn;
        }
        
        public String getShortCode(){
                return shortCode;
        }
        
        public void setShortCode(String shortCode)
        {
                this.shortCode = shortCode;
        }
        
        public void setMsisdn(String msisdn)
        {
                this.msisdn = msisdn;
        }
        
        public Calendar getDate_created() {
                return (this.date_created == null || this.id.equalsIgnoreCase(""))? 
                                new GregorianCalendar() : this.date_created;
        }

        public void setDate_created(Calendar date_created) {
                this.date_created = date_created;
        }

        public String getId() {
                return (this.id == null || this.id.equalsIgnoreCase(""))? 
                                UUID.randomUUID().toString() : this.id;
        }

        public void setId(String id) {
                this.id = id;
        }
}