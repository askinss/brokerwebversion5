package com.vasconsulting.www.domain;

import java.util.UUID;

public class USSDMenuItemsGhana 
{
	private String id;
	private String sequence_value;
	private String ussd_body;
	private String display_text;
	private String url;
	private int serial_nos;
	private String parent_id;
	
	public String getId() 
	{
		return (id == null)? UUID.randomUUID().toString() : this.id;
	}
	public void setId(String id) 
	{
		this.id = id;
	}
	public String getSequence_value() 
	{
		return sequence_value;
	}
	public void setSequence_value(String sequence_value) 
	{
		this.sequence_value = sequence_value;
	}
	public String getUssd_body() {
		return ussd_body;
	}
	public void setUssd_body(String ussd_body) 
	{
		this.ussd_body = ussd_body;
	}
	public String getDisplay_text() 
	{
		return display_text;
	}
	public void setDisplay_text(String display_text) 
	{
		this.display_text = display_text;
	}
	
	
	public String getUrl() 
	{
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	public int getSerial_nos() {
		return serial_nos;
	}
	public void setSerial_nos(int serial_nos) {
		this.serial_nos = serial_nos;
	}
	
	public String getParent_id() {
		return parent_id;
	}
	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}
	public String toString()
	{
		return String.format("USSD Details [id = %s, sequence = %s, ussd_body = %s, display_text = %s]", id, sequence_value, 
				ussd_body, display_text);
	}
}
