package com.vasconsulting.www.interfaces;

import java.util.HashMap;

public interface TabsInterface {
	public String deleteService(String msisdn, String equiqID);
	public String changeService(String msisdn, String equipID);
	public HashMap<String, String> getPreOrPost(String msisdn);
	public String getService(String msisdn);
	public HashMap<String,String> getSubscriberInformation(String msisdn);

}
