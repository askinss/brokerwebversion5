/**
 * This is a helper supper class for all renewal command classes. It defines an 
 * setSubscriber method that will be used inside of the commands to determine the 
 * subscriber to be provisioned
 * @author Nnamdi Jibunoh
 * Date created 18th july 2011
 * Company VAS Consulting
 */
package com.vasconsulting.www.interfaces;

import com.vasconsulting.www.domain.SubscriberDetail;

public interface RenewalCommand extends Command{	
	public void setSubscriber(SubscriberDetail subscriber);
}
