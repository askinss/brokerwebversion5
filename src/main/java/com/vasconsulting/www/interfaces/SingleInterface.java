package com.vasconsulting.www.interfaces;

public interface SingleInterface {
	
	public String changeService(String msisdn, String equipID);

	public String deleteService(String msisdn, String equipID);

	public String getSubscriberInformation(String msisdn);
		

}

