package com.vasconsulting.www.interfaces;

public interface EncrypterInterface {	
	public String getUsername();
	public void setUsername(String userName);
	public String getPassword();
	public void setPassword(String passWord);
}
