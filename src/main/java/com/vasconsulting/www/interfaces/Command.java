/**
 * This is a supper class for all command classes. It defines an execute method that will
 * allow all command implementation receive receiver implementations and perform specific 
 * task on the receiver objects by calling the execute method.
 * @author Nnamdi Jibunoh
 * Date created 18th july 2011
 * Company VAS Consulting
 */
package com.vasconsulting.www.interfaces;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;

public interface Command {
	public int execute();
	public void setReceiverParameters(String receiverParams);
	public void setReceiver(String receiver);
	public int logTransaction();
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail);
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject);
}
