package com.vasconsulting.www.interfaces;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;

import com.vasconsulting.www.domain.ActivityLogger;
import com.vasconsulting.www.domain.BbTestPromoFirstYes;
import com.vasconsulting.www.domain.BbTestPromoInitial;
import com.vasconsulting.www.domain.BillingPlan;
import com.vasconsulting.www.domain.DeductionLog;
import com.vasconsulting.www.domain.ErrorDetail;
import com.vasconsulting.www.domain.FutureRenewal;
import com.vasconsulting.www.domain.IMEIWhitelist;
import com.vasconsulting.www.domain.MSISDNWhitelist;
import com.vasconsulting.www.domain.ReconStatus;
//import com.vasconsulting.www.domain.MenuSession;
//import com.vasconsulting.www.domain.MenuTree;
import com.vasconsulting.www.domain.RequestTracker;
import com.vasconsulting.www.domain.RolesDetail;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.SubscriberMenuStatus;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.domain.Ttspromo;
import com.vasconsulting.www.domain.CDRSubscriptionDetail;
import com.vasconsulting.www.domain.Users;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.HomiscoResults;
import com.vasconsulting.www.domain.ReconcilDetail;

public interface HibernateUtility
{
	public int saveSubscriber(SubscriberDetail subscriberDetail);
	public int createSwap(ReconcilDetail reconcilDetail,SubscriberDetail subscriberDetail);
	public int updateReconStatus(ReconcilDetail reconcilDetail,SubscriberDetail subscriberDetail);	
	public int saveTransactionlog(TransactionLog transactionLog);
	public Collection<SubscriberDetail> getSubscriberInformation(SubscriberDetail subscriberDetail);
	public SubscriberDetail getSubscriberInformation(String param, String paramValue);
	//public abstract Collection<ReconcilDetail> getReconSubInformationByImsi(String paramString);
	//public abstract Collection<ReconcilDetail> getReconSubInformation(ReconcilDetail paramReconcilDetail);
	public String getSubscriberTABSCreditLimit(String msisdn);
	public int updateSubscriberDetail(SubscriberDetail subscriebrDetail);
	public Collection<TransactionLog> getSubscriberTransactionLog(String msisdn);
	public Collection<SubscriberDetail> loadAllSubscriberDetails();
	public Collection<SubscriberDetail> loadAllSubscriberDetailsDueForActivation();
	public Collection<SubscriberDetail> loadAllPostpaidSubscriberDetailsDueForRenewal();
	public int logService(ActivityLogger logger);
	public int getReportSumation(String columnName, int daysToRetrieve);
	public ArrayList<SubscriberDetail> getRenewalReportTabular();
	public ArrayList<SubscriberDetail> getActivationReportTabular(int daysToRetrieve);
	public ArrayList<SubscriberDetail> getSubscribersExpiringSoon(int noOfDays);
	public ArrayList<SubscriberDetail> getSubscribersExpiringToday();
	public ArrayList<BbTestPromoFirstYes> getFirstYesExpiringSoon(int noOfDays);
	public ArrayList<BbTestPromoFirstYes> getFirstYesExpiringToday();	
	public ArrayList<SubscriberDetail> getDaySubscribersExpiringSoon();
	public SubscriberMenuStatus getSubscriberMenuByMsisdn(String msisdn);
	public int saveSubscriberMenuByMsisdn(SubscriberMenuStatus subMenu);
	public int removeSubscriberMenuByMsisdn(SubscriberMenuStatus subMenu);
	public ArrayList<BillingPlan> getAllBillingPlans();
	public boolean isMSISDNWhitelisted(String msisdn);
	public boolean isIMEIWhitelisted(String imei);
	public int saveErrorLogs(ErrorDetail errorDetails);
	public ArrayList<ErrorDetail> getAllErrorLogs();
	public int deleteErrorLogs(ErrorDetail errorDetails);
	public int saveRequestTracker(RequestTracker request);
	public int updateIMEIWhitelist(IMEIWhitelist imeiRow);
	public int updateMSISDNWhitelist(MSISDNWhitelist msisdnRow);
	public int saveTTSPromoDetails(Ttspromo ttsPromo);
	public BbTestPromoInitial getPreloadedSubscriber(String msisdn);
	public int updateBBPromoInitial(BbTestPromoInitial bbTestPromoInitial);
	public BbTestPromoFirstYes getSubscriberFirstYes(String msisdn);
	public int updateBBPromoInitialYes(BbTestPromoFirstYes bbTestPromoFirstYes);
	public MSISDNWhitelist getWhiteListedMSISDN(String msisdn);
	public IMEIWhitelist getWhiteListedIMEI(String imei);
	public int saveBBPromoInitialYes(BbTestPromoFirstYes bbTestPromoFirstYes);
	public BbTestPromoFirstYes getBBPromoInitialYesStatusByMSISDN(String msisdn);
	public Ttspromo getTTSPromoDetails(String msisdn);
	public int createPostSubscriberHomisco(String stripLeadingMsisdnPlus,
			String string, String string2, String string3);
	public HomiscoResults getPostpaidSubscriberCreditLimit(String msisdn);
	public int savePostpaidCDR(CDRSubscriptionDetail cdrDetails);
	public int updateSubscriptionCDR(CDRSubscriptionDetail subscriebrDetail);
	public ArrayList<CDRSubscriptionDetail> getAllSubscriptionCDR();
	public ArrayList<SubscriberDetail> getSubscriberThatExpiredSomeDaysAgo(int noOfDays);
	public int saveFutureRenewal(FutureRenewal futureRenewal);
	public ArrayList<FutureRenewal> subscriberFutureRenewals(String msisdn);
	public int updateFutureRenewal(FutureRenewal futureRenewal);
	public ArrayList<SubscriberDetail> getPostpaidSubscribersDueForCurrentMonth();
	public Users getUserDetailByUsername(String username);
	public Users getUserDetailByUsername(Users user);
	public int updateBrokerUserDetail(Users userDetailSearch);
	public int saveBrokerUserDetail(Users userDetails);
	public ArrayList<Users> getAllUserDetails();
	public Collection<RolesDetail> getAllRoles();
	public Collection<SubscriberDetail> getSubscriberInformationByImsi(
			String oldImsi);
	public ArrayList<DeductionLog> getRevenueReportByDetail(String service, int noOfDays, int type, String shortCode);
	public ArrayList<Users> getUserDetailsById(String userId);
	public ArrayList<DeductionLog> getRevenueReportBySummation(int noOfDays);
	public int updateUserdetailById(Users Id);
	public ArrayList<String> getAllShortcodesForReport();
	public ArrayList<String> getAllServicesForReport();
	public int deleteUserdetailById(Users user);
	public ArrayList<FutureRenewal> allSubscriberFutureRenewals(String msisdn);
	
	public ArrayList<SubscriberDetail> getSubscribersExpiringTodayWithCriterias();
	public ArrayList<SubscriberDetail> getSubscribersActiveCriterias();
	
	//public MenuSession isThereAValidMenuSession(String msisdn);
	//public MenuSession isThereAValidMenuSession(String msisdn, String sessionid);
	//public int saveMenuSession(MenuSession menuSession);
	//public int updateMenuSession(MenuSession menuSession);
	//public ArrayList<MenuTree> getMenuToDisplay(String parent_id);
	//public ArrayList<MenuTree> getMenuToDisplay(String parent_id, String serialnos);
	//public int deleteMenuTreeByMsisdn(MenuSession menuTree);
	
	public int saveDeductionLogs(DeductionLog deductionLog);
	public abstract ReconcilDetail getIMSIFromDBByMsisdn(String string);
	public abstract ReconStatus getReconStatusInformation(String msisdn);
	public int saveReconStatus(ReconStatus reconStatusDb);
	
	
	
	
}
