package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.UCIPServiceRequestManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;

public class SetSubscriberPSOBitCommandImpl implements Command {
	private UCIPServiceRequestManager ucipConnector = new UCIPServiceRequestManager();
	private TransactionLog transactionLog;
	private SubscriberDetail subscriberDetail;
	private HibernateUtility hibernateUtility;
	Map<String, String> ucipReturns = new HashMap<String, String>();
	private String receiverParams;
	String[] psoBitValuePairs = null;
	int psoBitID = 1,psoBitValue = 1;
	Logger logger = Logger.getLogger(SetSubscriberPSOBitCommandImpl.class);
	
	/**
	 * This is the entry method for this command instance.
	 * The sole purpose of this execute method is to setup a subscribers PSOBit values as specified in the billingplan xml document
	 */
	@SuppressWarnings("unchecked")
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		
		logger.info("Execute called on SetSubscriberPSOBitCommandImpl for subscriber with msisdn "+subscriberDetail.getMsisdn());
		
		String[] psoBitValues = receiverParams.split(",");
		
		for (String string : psoBitValues) {
			
			psoBitValuePairs = string.split(":");
			
			if (psoBitValuePairs != null && psoBitValuePairs.length > 0){	
				
				if (psoBitValuePairs.length == 2)
				{
					psoBitID = new Integer(psoBitValuePairs[0]).intValue();
					psoBitValue = new Integer(psoBitValuePairs[1]).intValue();
				}
				else if(psoBitValuePairs.length == 1)
				{
					psoBitID = new Integer(psoBitValuePairs[0]).intValue();
					psoBitValue = 1;
				}				
				
				if (psoBitValue == 0 || psoBitValue == 1)
				{
					ucipReturns = ucipConnector.updateSubscriberServiceOffering(subscriberDetail.getMsisdn(), psoBitID, psoBitValue);
				}
				else
				{
					ucipReturns = ucipConnector.updateSubscriberServiceOffering(subscriberDetail.getMsisdn(), psoBitID, 1);
				}
			}			
			else{
				logger.info("SetSubscriberPSOBitCommandImpl command was called for subscriber but no PSOBit ID:Value pair was " +
						"specified. No action was taken.");
			}
		}		
		
		transactionLog = new TransactionLog();
		transactionLog.setDate_created(new GregorianCalendar());		
		transactionLog.setMsisdn(subscriberDetail.getMsisdn());
		transactionLog.setService(subscriberDetail.getServiceplan());
		transactionLog.setShortcode("");
		
		if (ucipReturns.get("responseCode").equalsIgnoreCase("0")) 
		{
			transactionLog.setStatus("SUCCESSFUL");
			transactionLog.setDescription("SET PSOBIT");
		}			
		else if(ucipReturns.get("responseCode").equalsIgnoreCase("102"))
		{
			transactionLog.setShortcode("FAILED");
			transactionLog.setDescription("SET PSOBIT : NOT A VALID SUBSCRIBER");
		}
		else
		{
			transactionLog.setShortcode("FAILED");
			transactionLog.setDescription("SET PSOBIT : OTHER ERRORS");
		}
		
		hibernateUtility.saveTransactionlog(transactionLog);
		
		return new Integer(ucipReturns.get("responseCode")).intValue();
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		// TODO Auto-generated method stub
		
	}
	

}
