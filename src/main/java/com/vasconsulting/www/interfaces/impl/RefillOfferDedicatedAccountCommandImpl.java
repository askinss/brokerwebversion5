package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.UCIPServiceRequestManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.RIMQueryUtil;
import com.vasconsulting.www.utility.RIMXMLResponseDetail;
import com.vasconsulting.www.utility.RIMXMLUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class RefillOfferDedicatedAccountCommandImpl implements Command {
	private UCIPServiceRequestManager ucipConnector;
	private HibernateUtility hibernateUtility;
	protected SubscriberDetail subscriberDetail;
	private RIMXMLResponseDetail rimStatus;
	protected String receiverParams;
	private BillingPlanObjectUtility billingPlanObject;
	Logger logger = Logger.getLogger(RefillOfferDedicatedAccountCommandImpl.class);

	protected TransactionLog transactionLog;

	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = (HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		ucipConnector = (ucipConnector == null) ? (UCIPServiceRequestManager)ContextLoaderImpl.getBeans("ucipConnector") : ucipConnector;
		
		logger.info("Execute called on RefillOfferDedicatedAccountCommandImpl for subscriber with msisdn "+subscriberDetail.getMsisdn());
		
		setUpTransaction();
		
		String [] refillIdToSet = receiverParams.split(":"); //refillProfileID:AMOUNT
		
		if (receiverParams == null || receiverParams == "")
			return StatusCodes.WRONG_SERVICENAME_FORMAT;

		try
		{
			Map<String, String> ucipResponse = ucipConnector.refillSubscriberServiceOffer(subscriberDetail.getMsisdn(), 
					refillIdToSet[0], refillIdToSet[1]);
			
			logger.info("Response from AIR - refill command ["+subscriberDetail.getMsisdn()+"] is "+ucipResponse);

			if (ucipResponse.get("responseCode").equalsIgnoreCase("0"))
			{
				transactionLog.setStatus("SUCCESSFUL");
				hibernateUtility.saveTransactionlog(transactionLog);
				
				return StatusCodes.SUCCESS;
			}
			else
			{
				transactionLog.setStatus("FAILED");
				transactionLog.setId(UUID.randomUUID().toString());
				transactionLog.setDescription("REFILL SETUP");
				transactionLog.setService(receiverParams);
				transactionLog.setShortcode(billingPlanObject.getShortCode());
				hibernateUtility.saveTransactionlog(transactionLog);
				
				return StatusCodes.OTHER_ERRORS;
			}
			
			

		}
		catch(Exception e){
			transactionLog.setStatus("FAILED");
			transactionLog.setId(UUID.randomUUID().toString());
			transactionLog.setDescription("REFILL SETUP");
			transactionLog.setService(receiverParams);
			transactionLog.setShortcode(billingPlanObject.getShortCode());
			hibernateUtility.saveTransactionlog(transactionLog);			
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;

		}
	}
	
	protected void setUpTransaction(){
		transactionLog = (transactionLog == null) ? new TransactionLog() : transactionLog;
		transactionLog.setDate_created(new GregorianCalendar());
		transactionLog.setDescription("REFILL SETUP");
		transactionLog.setMsisdn(subscriberDetail.getMsisdn());
		transactionLog.setService(receiverParams);
		transactionLog.setShortcode(billingPlanObject.getShortCode());
	}

	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;

	}
	

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public TransactionLog getTransactionLog() {
		return transactionLog;
	}

	public void setTransactionLog(TransactionLog transactionLog) {
		this.transactionLog = transactionLog;
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}
}
