package com.vasconsulting.www.interfaces.impl;


public class GetSubscriberIMSIEMACommandSimSwapImpl extends GetSubscriberIMSIEMACommandImpl {
	
	@Override
	protected String subscriberType(){
		System.out.println("This is the postpaid subscriber: "+subscriberDetail.getPostpaidSubscriber());
		if (subscriberDetail.getPostpaidSubscriber() == 1){
			return "postpaid";
		}else return "prepaid";
	}

}
