/**
 * This class is used to get the subscriber IMSI. If the subscriber IMSI is found, it is set in the session available subscriberDetail bean
 * and returns 0 if subscriber else it returns imsi not found error.
 * @author nnamdi Jibunoh
 */
package com.vasconsulting.www.interfaces.impl;

import java.rmi.RemoteException;
import java.util.GregorianCalendar;
import java.util.HashMap;

import org.apache.log4j.Logger;

import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKGProxy;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPreOrPostPaidElement;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPreOrPostPaidResponseElement;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.HibernateUtilityImpl;
import com.vasconsulting.www.utility.StatusCodes;
import com.vasconsulting.www.utility.XMLUtility;

public class GetSubscriberIMSICommandImpl implements Command {
	private TBI_KPI_PKGProxy tbiProxy = new TBI_KPI_PKGProxy();
	private SubscriberDetail subscriberDetail;
	private BillingPlanObjectUtility billingPlanObject;
	private XMLUtility xmlUtility;
	private TransactionLog transactionLog;
	private HibernateUtility hibernateUtility;
	private Logger logger = Logger.getLogger(GetSubscriberIMSICommandImpl.class);
	private String subscriberImsi;
	
	public GetSubscriberIMSICommandImpl(){
		
	}
	/**
	 * This method will execute and perform service change on TABS based on the supplied
	 * receiver. The method will log at the end of the method execution the state of the 
	 * transaction run.
	 * @author Nnamdi Jibunoh
	 * @param
	 */
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */

		xmlUtility = 
			(XMLUtility)ContextLoaderImpl.getBeans("xmlUtility");
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		
		logger.info("Execute called on GetSubscriberIMSICommandImpl for subscriber with msisdn "+subscriberDetail.getMsisdn());
		
		GetPreOrPostPaidElement subParams = new GetPreOrPostPaidElement();
		subParams.setSubno(stripLeadingMsisdnPrefix(subscriberDetail.getMsisdn()));
		subParams.setArea("0");
		subParams.setSubscrType("G");
		
		try {
			GetPreOrPostPaidResponseElement responseElement = tbiProxy.getPreOrPostPaid(subParams);
			
			HashMap<String, String> tabsResponse = xmlUtility.processTABSResponse(responseElement.getResult());
			
			transactionLog = new TransactionLog();
			transactionLog.setDate_created(new GregorianCalendar());		
			transactionLog.setMsisdn(subscriberDetail.getMsisdn());
			transactionLog.setShortcode(billingPlanObject.getShortCode());			
			transactionLog.setDescription("GET IMSI");
			
			if (tabsResponse.get("errorResponse") != null)
			{
				transactionLog.setStatus("FAILED");
				transactionLog.setService("");
				hibernateUtility.saveTransactionlog(transactionLog);
				logger.info("Error retrieveing IMSI, returning status "+StatusCodes.OTHER_ERRORS_IMSI);
				return StatusCodes.OTHER_ERRORS_IMSI;
			}
			else if(!tabsResponse.get("IMSI_NUMBER").equalsIgnoreCase("")) 
			{
				subscriberImsi = tabsResponse.get("IMSI_NUMBER");
				
				transactionLog.setStatus("SUCCESSFUL");
				transactionLog.setService(subscriberImsi);
				hibernateUtility.saveTransactionlog(transactionLog);
				
				subscriberDetail.setImsi(subscriberImsi);
				logger.info("IMSI value is "+subscriberImsi+" for subscriber "+subscriberDetail.getMsisdn());
				return StatusCodes.SUCCESS;
			}
			else 
			{
				transactionLog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionLog);
				logger.info("Error retrieveing IMSI, returning status "+StatusCodes.IMSI_NOT_FOUND);
				return StatusCodes.IMSI_NOT_FOUND;
			}
			
		} catch (RemoteException e) {
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;
		}		
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	private String stripLeadingMsisdnPrefix(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("0")){
			return Msisdn.substring(1, Msisdn.length());
		}
		else if (Msisdn.startsWith("234")){
			return Msisdn.substring(3, Msisdn.length());
		}
		else if(Msisdn.startsWith("+234")){
			return Msisdn.substring(4, Msisdn.length());
		}
		else return Msisdn;
	}
	public void setReceiverParameters(String receiverParams)
	{
		// TODO Auto-generated method stub
		
	}
	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}
	

}
