package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.interfaces.TabsInterface;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.StatusCodes;

public class RemoveSubscriberTABSServiceCommandImpl implements Command {
	private HibernateUtility hibernateUtility;
	private SubscriberDetail subscriberDetail;
	private Logger logger = Logger.getLogger(RemoveSubscriberTABSServiceCommandImpl.class);
	private String receiverParam;
	private TransactionLog transactionlog;
	private TabsInterface tabs;
	private BillingPlanObjectUtility billingPlanObject;
	private LoadAllProperties properties;
	
		
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = (hibernateUtility == null) ? 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility") : hibernateUtility;
		tabs = (tabs == null) ? (TabsInterface)ContextLoaderImpl.getBeans("tabs") : tabs;
		properties = (properties == null) ? (LoadAllProperties)ContextLoaderImpl.getBeans("properties") : properties;

		
		logger.info("Execute called on RemoveSubscriberTABSServiceCommandImpl for subscriber with MSISDN = "+subscriberDetail.getMsisdn());
		
		String responseCode = "";
		transactionlog = new TransactionLog();
		transactionlog.setDate_created(new GregorianCalendar());
		transactionlog.setMsisdn(subscriberDetail.getMsisdn());
		transactionlog.setDescription("Delete TABS Service");
		transactionlog.setShortcode(subscriberDetail.getShortCode());
		
		try {
			responseCode = tabs.deleteService(subscriberDetail.getMsisdn(),equipID());
			logger.info("Response from TABS = "+responseCode);
			
			if (responseCode != null){				
				transactionlog.setService(equipID());
				transactionlog.setStatus("SUCCESSFUL");				
				hibernateUtility.saveTransactionlog(transactionlog);				
				logger.info("RemoveSubscriberTABSServiceCommandImpl for subscriber "+subscriberDetail.getMsisdn()+" returned with code "+responseCode);
			}
			else{
				transactionlog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionlog);
				transactionlog.setId(UUID.randomUUID().toString());
				transactionlog.setDescription("SHORTCODE");
				hibernateUtility.saveTransactionlog(transactionlog);
			}			
		} catch (Exception e) {
			transactionlog.setStatus("FAILED");
			hibernateUtility.saveTransactionlog(transactionlog);
			transactionlog.setId(UUID.randomUUID().toString());
			transactionlog.setDescription("SHORTCODE");
			hibernateUtility.saveTransactionlog(transactionlog);			
			e.printStackTrace();
		}
		return StatusCodes.SUCCESS;
	}

	public void setHibernateUtility(HibernateUtility hibernateUtility) {
		this.hibernateUtility = hibernateUtility;
	}

	public void setTabs(TabsInterface tabs) {
		this.tabs = tabs;
	}

	public void setProperties(LoadAllProperties properties) {
		this.properties = properties;
	}

	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParam = receiverParams;
	}
	
	private String stripLeadingMsisdnPrefix(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("0")){
			return Msisdn.substring(1, Msisdn.length());
		}
		else if (Msisdn.startsWith("255")){
			return Msisdn.substring(3, Msisdn.length());
		}
		else if(Msisdn.startsWith("+255")){
			return Msisdn.substring(4, Msisdn.length());
		}
		else return Msisdn;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	private String equipID(){
		return properties.getProperty(subscriberDetail.getServiceplan().replaceAll("\\s", "_").toLowerCase());
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;
	}

}
