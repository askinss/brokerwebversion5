package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.UCIPServiceRequestManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class UpdateDedicatedAccountUCandUTCommandImpl implements Command {
	private UCIPServiceRequestManager ucipConnector;
	private HibernateUtility hibernateUtility;
	protected SubscriberDetail subscriberDetail;
	protected String receiverParams;
	private BillingPlanObjectUtility billingPlanObject;
	Logger logger = Logger.getLogger(UpdateDedicatedAccountUCandUTCommandImpl.class);

	protected TransactionLog transactionLog;

	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = (HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		ucipConnector = (ucipConnector == null) ? (UCIPServiceRequestManager)ContextLoaderImpl.getBeans("ucipConnector") : ucipConnector;
		
		logger.info("Execute called on RefillOfferDedicatedAccountCommandImpl for subscriber with msisdn "+subscriberDetail.getMsisdn());
		
		setUpTransaction();
		
		
		if (receiverParams == null || receiverParams == "")
			return StatusCodes.WRONG_SERVICENAME_FORMAT;

		try
		{
			Map<String, String> ucipResponse = ucipConnector.updateSubscriberUTandUCUnlimited(subscriberDetail.getMsisdn());
			
			logger.info("Response from AIR - Update UT/UC command ["+subscriberDetail.getMsisdn()+"] is "+ucipResponse);

			if (ucipResponse.get("responseCode").equalsIgnoreCase("0"))
			{
				transactionLog.setStatus("SUCCESSFUL");
				hibernateUtility.saveTransactionlog(transactionLog);
				
				return StatusCodes.SUCCESS;
			}
			else
			{
				transactionLog.setStatus("FAILED");
				transactionLog.setId(UUID.randomUUID().toString());
				transactionLog.setDescription("UPDATE UT/UC SETUP");
				transactionLog.setService(receiverParams);
				transactionLog.setShortcode(billingPlanObject.getShortCode());
				hibernateUtility.saveTransactionlog(transactionLog);
				
				return StatusCodes.OTHER_ERRORS;
			}
			
			

		}
		catch(Exception e){
			transactionLog.setStatus("FAILED");
			transactionLog.setId(UUID.randomUUID().toString());
			transactionLog.setDescription("UPDATE UT/UC SETUP");
			transactionLog.setService(receiverParams);
			transactionLog.setShortcode(billingPlanObject.getShortCode());
			hibernateUtility.saveTransactionlog(transactionLog);			
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;

		}
	}
	
	protected void setUpTransaction(){
		transactionLog = (transactionLog == null) ? new TransactionLog() : transactionLog;
		transactionLog.setDate_created(new GregorianCalendar());
		transactionLog.setDescription("UPDATE UT/UC SETUP");
		transactionLog.setMsisdn(subscriberDetail.getMsisdn());
		transactionLog.setService(receiverParams);
		transactionLog.setShortcode(billingPlanObject.getShortCode());
	}

	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;

	}
	

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public TransactionLog getTransactionLog() {
		return transactionLog;
	}

	public void setTransactionLog(TransactionLog transactionLog) {
		this.transactionLog = transactionLog;
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}
}
