/**
 * This class is used to check the type of subscriber. If the subscriber is a POSTPAID or PREPAID subscriber
 * It returns 0 if subscriber is postpaid and 500 if prepaid.
 * @author nnamdi Jibunoh
 */
package com.vasconsulting.www.interfaces.impl;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.SendSmsToKannelService;

public class SendPrepSubscriberAutoResponseDateFutureRenewalCommandImpl implements Command
{

	private SubscriberDetail subscriberDetail;
	private String receiverParams;
	private SendSmsToKannelService smsService;
	private LoadAllProperties properties;
	private String messageToSend = "";
	private BillingPlanObjectUtility billingPlanObject;
	Logger logger = Logger.getLogger(SendPrepSubscriberAutoResponseDateFutureRenewalCommandImpl.class);

	/**
	 * 
	 * 
	 * @author Nnamdi Jibunoh
	 * @param
	 */
	public int execute()
	{
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		smsService = 
			(SendSmsToKannelService)ContextLoaderImpl.getBeans("smsService");

		logger.info("Execute called on SetSubscriberAutoRenewCommandImpl for subscriber with MSISDN = "
				+ subscriberDetail.getMsisdn());

		//messageToSend = properties.getProperty(receiverParams);
		
		messageToSend = String.format(receiverParams, billingPlanObject.getDescription());
		
		logger.info("Sending future renewal text "+messageToSend+" to "+subscriberDetail.getMsisdn());
		
		smsService.sendMessageToKannel(messageToSend, subscriberDetail.getMsisdn());		
		
		return 0;

	}

	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;

	}

	public int rollBack()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;
		
	}

}
