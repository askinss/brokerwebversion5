package com.vasconsulting.www.interfaces.impl;

import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import org.apache.log4j.Logger;

import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKGProxy;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscriberInformationElement;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscriberInformationResponseElement;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.interfaces.RenewalCommand;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.HibernateUtilityImpl;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.RIMQueryUtil;
import com.vasconsulting.www.utility.StatusCodes;
import com.vasconsulting.www.utility.XMLUtility;

public class RenewSubscriberDBRecordCommandImpl implements RenewalCommand
{
	@SuppressWarnings("unused")
	private String receiverParams;
	private SubscriberDetail subscriberDetail;
	private HibernateUtility hibernateUtility = (HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
	EmailTaskExecutor emailExecutor = (EmailTaskExecutor)ContextLoaderImpl.getBeans("myEmailTaskExecutor");
	Logger logger = Logger.getLogger(RenewSubscriberDBRecordCommandImpl.class);
	private int actionResponse;
	private XMLUtility xmlUtility = new XMLUtility();
	private TBI_KPI_PKGProxy tbiProxy = new TBI_KPI_PKGProxy();
	private RIMQueryUtil rimUtility = new RIMQueryUtil();
	private int rimStatus;
	private String errorMessage = "";
	private String[] receiverParamsValue;
	
	/**
	 * This method is used to get the last day of the month.
	 * @param noOfDays
	 * @return
	 */
	private GregorianCalendar getLastDateOfMonth(){
		int year = Calendar.getInstance().get(Calendar.YEAR);
		int month = Calendar.getInstance().get(Calendar.MONTH);
		int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
		
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.set(year, month, day);		
		return calendar;
	}
	
	public int execute()
	{
		logger.info("Execute called on RenewSubscriberDBRecordCommandImpl for subscriber with MSISDN = "+subscriberDetail.getMsisdn());
		
		String currentStatus = subscriberDetail.getStatus();
		String servicePlanName = subscriberDetail.getServiceplan();
		
		GetSubscriberInformationElement subParams = new GetSubscriberInformationElement();
		subParams.setSubno(stripLeadingMsisdnPrefix(subscriberDetail.getMsisdn()));
		
		try {
			GetSubscriberInformationResponseElement responseElement = tbiProxy.getSubscriberInformation(subParams);
			
			HashMap<String, String> tabsResponse = xmlUtility.processTABSResponse(responseElement.getResult());
			
			logger.info("Call to check Subscriber Status on TABS API returned result : "+tabsResponse);
			logger.info("Value of receiverParams for this subscriber is = "+receiverParams);
			
			if (tabsResponse.get("errorResponse") != null) actionResponse =  StatusCodes.OTHER_ERRORS;
			else if(tabsResponse.get("STATUS").equalsIgnoreCase("30")) 
			{
				subscriberDetail.setStatus("Active");
				actionResponse =  StatusCodes.SUCCESS;
			}
			else
			{
				subscriberDetail.setStatus("Deactivated");
				actionResponse = StatusCodes.BARRED_SUBSCRIBER;
			}
			
		} catch (RemoteException e) {
			e.printStackTrace();
			actionResponse = StatusCodes.OTHER_ERRORS;
		}
		
		if (actionResponse == StatusCodes.OTHER_ERRORS)
		{
			errorMessage = "There was an error conencting to the TABS Interface to pull details for Postpaid Subscriber renewal. " +
			"Please check and confirm its not continious";
			logger.error("Error: Error connecting to TABS");
			emailExecutor.sendQueuedEmails(errorMessage);
			return actionResponse;
		}
		else if (receiverParams.equalsIgnoreCase(""))
		{
			errorMessage = "The configuration file for the renewal module is either corrupt or not properly set. Please consult the documentation " +
					"and then set the details for Postpaid Blackberry renewal.";
			logger.error("Error: Error renewal setting for application not set");
			emailExecutor.sendQueuedEmails(errorMessage);
			return StatusCodes.OTHER_ERRORS;
		}
		else if ((currentStatus.trim().equalsIgnoreCase("Deactivated") && actionResponse ==  StatusCodes.SUCCESS))
		{
			logger.info("The value of the receiverParams set = "+receiverParams);
			
			//Prosumer Prepaid:Internet Browsing,Enterprise Plus:Internet Browsing,Prepaid BIS Social,Prepaid BIS Lite,10:10240,5:10000,20
			receiverParamsValue = receiverParams.split(",");
			String [] servicePlans = null;	
			String costOfService = "";
			
			//Loop through the configured param value, pick out the RIM part and build the RIM service names
			for (int count = 0;count < 4 ; count++){
				if (receiverParamsValue[count].contains(servicePlanName.trim()))
				{
					String []serviceNames = receiverParamsValue[count].split(":");
					
					servicePlans = new String[serviceNames.length - 1];
					
					for (int i=0; i < serviceNames.length -1;i++){
						servicePlans[i] = serviceNames[i];
					}
					
					costOfService = serviceNames[serviceNames.length - 1];
				}
			}
			
			if (servicePlans != null)
			{
				rimStatus = rimUtility.activateSubscriptionByUSSDChannel(subscriberDetail, servicePlans);
			}
			else
			{
				errorMessage = "The configuration file for the renewal module is either corrupt or not properly set for the Blackberry service. " +
						"Please consult the documentation " +
				"and then set the details for Postpaid Blackberry renewal.";
				logger.error("Error: Error renewal setting for application not set properly for the Blackberry services");
				emailExecutor.sendQueuedEmails(errorMessage);
				return StatusCodes.OTHER_ERRORS;
			}
						
			if ((rimStatus == 0 || (rimStatus >= 21000 && rimStatus <= 25000))) 
			{
				subscriberDetail.setNext_subscription_date(getLastDateOfMonth());	
				subscriberDetail.setStatus("Active");
				rimStatus = hibernateUtility.updateSubscriberDetail(subscriberDetail);
				
				if (rimStatus == StatusCodes.SUCCESS)
				{
					TransactionLog transactionLog = new TransactionLog();
					transactionLog.setDate_created(new GregorianCalendar());		
					transactionLog.setMsisdn(subscriberDetail.getMsisdn());
					transactionLog.setService(subscriberDetail.getServiceplan());
					transactionLog.setShortcode("RENEWAL");			
					transactionLog.setDescription("APPLICATION RENEW MODULE");
					transactionLog.setStatus("SUCCESSFUL");
					hibernateUtility.saveTransactionlog(transactionLog);
				}
				else
				{
					errorMessage = "################CRITICAL###############\n" +
							"The subscriber " +subscriberDetail.getMsisdn()+" has been renewed but the record was NOT " +
							"updated on the database. Please rectify manually."+
					"and then set the details for Postpaid Blackberry renewal.";
					logger.error("Error: Error subscriber detail was not updated on the database. Please rectify manually.");
					emailExecutor.sendQueuedEmails(errorMessage);
				}
					
				actionResponse = 0;
			}
			else
			{
				//log this error
				emailExecutor.sendQueuedEmails("");
				actionResponse = 0;
			}
		}
		else if (currentStatus.trim().equalsIgnoreCase("Active") && actionResponse ==  StatusCodes.BARRED_SUBSCRIBER)
		{
			rimStatus = rimUtility.cancelSubscription(subscriberDetail);
			logger.info("The value of rimSttaus = "+rimStatus);
			if (rimStatus == 0 || (rimStatus >= 21000 && rimStatus <= 25000))
			{
				subscriberDetail.setStatus("Deactivated");		
				actionResponse = hibernateUtility.updateSubscriberDetail(subscriberDetail);
				
				if (actionResponse != 0)
				{
					errorMessage = "################CRITICAL###############\n" +
					"The subscriber " +subscriberDetail.getMsisdn()+" is due for activation, because BAR status on TABS, but attempts to " +
							"update the subscriber information on the database failed. Please rectify manually";
					logger.error("Error: Error subscriber detail was not updated on the database. Please rectify manually.");
					emailExecutor.sendQueuedEmails(errorMessage);
				}				
					
				actionResponse = StatusCodes.SUCCESS;
			}
			else
			{
				errorMessage = "################CRITICAL###############\n" +
				"The subscriber " +subscriberDetail.getMsisdn()+" is due for activation, because BAR status on TABS, but attempts to " +
						"deactivate the subscriber on RIM failed. Please rectify manually and review logs for reasons for failure.";
				logger.error("Error: Error subscriber detail was not updated on the database. Please rectify manually.");
				emailExecutor.sendQueuedEmails(errorMessage);
				actionResponse = 0;
			}
		}
		
		logger.info("The call to renew all subscribers at the begining of the month ran for subscriber "+subscriberDetail.getMsisdn()+" and" +
				" returned a response of "+actionResponse);
		return actionResponse;		
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}

	public int logTransaction()
	{
		return 0;
	}

	public void setSubscriber(SubscriberDetail subscriber)
	{
		this.subscriberDetail = subscriber;
	}
	
	private String stripLeadingMsisdnPrefix(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("0")){
			return Msisdn.substring(1, Msisdn.length());
		}
		else if (Msisdn.startsWith("234")){
			return Msisdn.substring(3, Msisdn.length());
		}
		else if(Msisdn.startsWith("+234")){
			return Msisdn.substring(4, Msisdn.length());
		}
		else return Msisdn;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		
	}

}
