package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.TelnetServiceManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class GetSubscriberIMSIEMAPostUgandaCommandImpl implements Command {
	private TelnetServiceManager telnetConnector = new TelnetServiceManager();
	private SubscriberDetail subscriberDetail;
	private BillingPlanObjectUtility billingPlanObject;
	private TransactionLog transactionLog = new TransactionLog();
	private HibernateUtility hibernateUtility;
	private String receiverParams;
	private Logger logger = Logger.getLogger(GetSubscriberIMSIEMAPostUgandaCommandImpl.class);
	
	@SuppressWarnings("unchecked")
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		
		String [] receiverParamValues = receiverParams.split(":SEP:");
		
		logger.info("Execute called on GetSubscriberIMSIEMAPostCommandImpl for subscriber with msisdn "+subscriberDetail.getMsisdn());
				
		try {
			Map<String, String> responseMap = telnetConnector.getSubscriberInformation(stripLeadingMsisdnPlus(subscriberDetail.getMsisdn()));
			
			transactionLog.setId(UUID.randomUUID().toString());
			transactionLog.setMsisdn(subscriberDetail.getMsisdn());
			transactionLog.setDate_created(new GregorianCalendar());
			transactionLog.setService("GET IMSI");
			transactionLog.setShortcode(subscriberDetail.getShortCode());
			
			if (responseMap == null || !responseMap.containsKey("imsi"))
			{
				
				transactionLog.setStatus("FAILED");
				transactionLog.setDescription("Error retrieving IMSI from EMA");
				
				logger.info("Error retrieveing IMSI, returning status "+StatusCodes.OTHER_ERRORS_IMSI);
				return StatusCodes.OTHER_ERRORS_IMSI;
			}
			else if(!responseMap.get("imsi").equalsIgnoreCase("")) 
			{
				String subscriberImsi = responseMap.get("imsi");
				if (!subscriberImsi.startsWith("64101028") && !subscriberImsi.startsWith("64101029") && !subscriberImsi.startsWith("64101038"))
					return new Integer(receiverParamValues[1]);
				
				transactionLog.setStatus("SUCCESSFUL");
				transactionLog.setDescription(subscriberImsi);
								
				subscriberDetail.setImsi(subscriberImsi);
				logger.info("IMSI value is "+subscriberImsi+" for subscriber "+subscriberDetail.getMsisdn());
				return StatusCodes.SUCCESS;
			}
			else 
			{
				transactionLog.setStatus("FAILED");
				transactionLog.setDescription("No IMSI returned");
				
				logger.info("Error retrieveing IMSI, returning status "+StatusCodes.IMSI_NOT_FOUND);
				return StatusCodes.IMSI_NOT_FOUND;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			transactionLog.setStatus("FAILED");
			transactionLog.setDescription("An error occurred retrieving the IMSI");
			return StatusCodes.OTHER_ERRORS;
		}
		finally
		{
			hibernateUtility.saveTransactionlog(transactionLog);
		}
	}
	
	private String stripLeadingMsisdnPlus(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("0")){
			return "256"+Msisdn.substring(1, Msisdn.length());
		}
		else if(Msisdn.startsWith("+256")){
			return Msisdn.substring(1, Msisdn.length());
		}
		else return Msisdn;
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}

	public int rollBack()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;
	}
	
}
