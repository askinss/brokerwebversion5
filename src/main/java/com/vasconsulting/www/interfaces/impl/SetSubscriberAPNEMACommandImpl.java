package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.TelnetServiceManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.StatusCodes;

public class SetSubscriberAPNEMACommandImpl implements Command
{
	private TelnetServiceManager telnetConnector;
	private SubscriberDetail subscriberDetail;
	private TransactionLog transactionLog;
	private HibernateUtility hibernateUtility;
	private String receiverParams;
	private Logger logger = Logger
			.getLogger(SetSubscriberAPNEMACommandImpl.class);
	private BillingPlanObjectUtility billingPlanObject;

	@SuppressWarnings("unchecked")
	public int execute()
	{
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = (hibernateUtility == null) ? (HibernateUtility) ContextLoaderImpl
				.getBeans("hibernateUtility") : hibernateUtility;
		telnetConnector = (telnetConnector == null) ? (TelnetServiceManager)ContextLoaderImpl
						.getBeans("telnetConnector") : telnetConnector;
		transactionLog = new TransactionLog();
		transactionLog.setDate_created(new GregorianCalendar());
		transactionLog.setMsisdn(subscriberDetail.getMsisdn());
		transactionLog.setShortcode(billingPlanObject.getShortCode());
		transactionLog.setService(subscriberDetail.getServiceplan());
		transactionLog.setDescription("SET APN");

		logger.info("Execute called on SetSubscriberAPNEMACommandImpl for subscriber with msisdn "
				+ subscriberDetail.getMsisdn());

		try {
			convertToEMACommand(receiverParams, subscriberDetail.getMsisdn());
			
			String responseMap = telnetConnector.addBlackberryAPNToSubscriber 
					(convertToEMACommand(receiverParams, subscriberDetail.getMsisdn()));//Ema Command would be passed as a parameter to the SetSubscriberAPNEMACommandImpl billingCommand
			logger.info("Response from EMA for setting APN on msisdn "+subscriberDetail.getMsisdn() + " is: " +responseMap);
			if (!isAPNSet(responseMap)) {
				transactionLog.setStatus("FAILED");
				transactionLog.setService("");
				hibernateUtility.saveTransactionlog(transactionLog);
				transactionLog.setId(UUID.randomUUID().toString());
				transactionLog.setDescription("SHORTCODE");
				hibernateUtility.saveTransactionlog(transactionLog);
				logger.info("Error Setting APN, returning status "
						+ StatusCodes.OTHER_ERRORS_IMSI);
				return StatusCodes.OTHER_ERRORS_IMSI;
			} else if (isAPNSet(responseMap)) {
				transactionLog.setStatus("SUCCESSFUL");
				hibernateUtility.saveTransactionlog(transactionLog);
				logger.info("Call to set APN returned value " + responseMap
						+ " for subscriber " + subscriberDetail.getMsisdn());
				return StatusCodes.SUCCESS;
			} else {
				transactionLog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionLog);
				transactionLog.setId(UUID.randomUUID().toString());
				transactionLog.setDescription("SHORTCODE");
				hibernateUtility.saveTransactionlog(transactionLog);
				logger.info("Error Setting APN, returning status "
						+ StatusCodes.IMSI_NOT_FOUND);
				return StatusCodes.IMSI_NOT_FOUND;
			}

		} catch (Exception e) {
			e.printStackTrace();
			hibernateUtility.saveTransactionlog(transactionLog);
			transactionLog.setId(UUID.randomUUID().toString());
			transactionLog.setDescription("SHORTCODE");
			hibernateUtility.saveTransactionlog(transactionLog);
			return StatusCodes.OTHER_ERRORS;
		}
	}
	
	private boolean isAPNSet(String responseMap){
		return (responseMap.equalsIgnoreCase("0") || responseMap.equalsIgnoreCase("10216"));
	}
	
	public String convertToEMACommand(String param, String msisdn, String apnId, String eqsId){
		String command = String.format(param, msisdn,apnId,eqsId);
		return command;
	}
	
	public String convertToEMACommand(String param, String msisdn){
		String command = String.format(param, msisdn);
		return command;
	}

	public TelnetServiceManager getTelnetConnector() {
		return telnetConnector;
	}

	public void setTelnetConnector(TelnetServiceManager telnetConnector) {
		this.telnetConnector = telnetConnector;
	}

	public SubscriberDetail getSubscriberDetail() {
		return subscriberDetail;
	}

	public void setSubscriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public BillingPlanObjectUtility getBillingPlanObjectLive() {
		return billingPlanObject;
	}

	public void setBillingPlanObject(
			BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;
	}

	public TransactionLog getTransactionLog() {
		return transactionLog;
	}

	public void setTransactionLog(TransactionLog transactionLog) {
		this.transactionLog = transactionLog;
	}

	public HibernateUtility getHibernateUtility() {
		return hibernateUtility;
	}

	public void setHibernateUtility(HibernateUtility hibernateUtility) {
		this.hibernateUtility = hibernateUtility;
	}

	public String getReceiverParams() {
		return receiverParams;
	}

	public void setReceiverParams(String receiverParams) {
		this.receiverParams = receiverParams;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}

	public int rollBack()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}



}
