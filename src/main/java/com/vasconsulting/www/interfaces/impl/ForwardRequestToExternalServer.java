package com.vasconsulting.www.interfaces.impl;

import java.io.IOException;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;

public class ForwardRequestToExternalServer implements Command {
	private Logger logger = Logger.getLogger(ForwardRequestToExternalServer.class);
	private SubscriberDetail subscriberDetail;
	private String receiverParams;
	private BillingPlanObjectUtility billingPlanObject;

	
	public int execute() {
		String externalServerURL = receiverParams;
		String params = "?msg=" + subscriberDetail.getShortCode().trim() + "&msisdn=" + subscriberDetail.getMsisdn();
		HttpClient client = new HttpClient();
		String url = externalServerURL+params;
		logger.info("URL is: "+url);
		GetMethod method = new GetMethod(url);
		method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, 
	    		new DefaultHttpMethodRetryHandler(3, false));
		try {
		      // Execute the method.
		      int statusCode = client.executeMethod(method);

		      if (statusCode != HttpStatus.SC_OK) {
		        System.err.println("Method failed: " + method.getStatusLine());
		      }

		      // Read the response body.
		      byte[] responseBody = method.getResponseBody();

		      // Deal with the response.
		      // Use caution: ensure correct character encoding and is not binary data
		      logger.info(new String(responseBody));

		    } catch (HttpException e) {
		      System.err.println("Fatal protocol violation: " + e.getMessage());
		      e.printStackTrace();
		    } catch (IOException e) {
		      System.err.println("Fatal transport error: " + e.getMessage());
		      e.printStackTrace();
		    } finally {
		      // Release the connection.
		      method.releaseConnection();
		    } 		
		return 0;
	}

	
	public void setReceiverParameters(String receiverParams) {
		this.receiverParams = receiverParams;
	}

	
	public int logTransaction() {
		// TODO Auto-generated method stub
		return 0;
	}


	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}

}
