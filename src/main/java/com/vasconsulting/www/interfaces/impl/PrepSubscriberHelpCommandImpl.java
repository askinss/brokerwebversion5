package com.vasconsulting.www.interfaces.impl;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.UCIPServiceRequestManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.SendSmsToKannelService;
import com.vasconsulting.www.utility.StatusCodes;

public class PrepSubscriberHelpCommandImpl implements Command
{
	private SubscriberDetail subscriberDetail;
	private String receiverParams;
	Logger logger = Logger.getLogger(PrepSubscriberHelpCommandImpl.class);
	private SendSmsToKannelService smsService;
	private LoadAllProperties properties;
	private String messageToSend = "";
	
	
	public int execute()
	{
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		smsService = 
			(SendSmsToKannelService)ContextLoaderImpl.getBeans("smsService");
		properties = 
			(LoadAllProperties)ContextLoaderImpl.getBeans("loadProperties");
		
		
		logger.info("Execute called on PrepSubscriberHelpCommandImpl for subscriber with MSISDN = "+subscriberDetail.getMsisdn());
		
		if (receiverParams.equalsIgnoreCase(""))
			return StatusCodes.WRONG_AUTORESPONSE_STATUS_CONFIG_FORMAT;
		
		messageToSend = properties.getProperty(receiverParams);
		
		logger.info("Sending auto response text "+messageToSend+" to "+subscriberDetail.getMsisdn());
		
		smsService.sendMessageToKannel(messageToSend, subscriberDetail.getMsisdn());		
		
		return 0;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}

	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		// TODO Auto-generated method stub
		
	}

}
