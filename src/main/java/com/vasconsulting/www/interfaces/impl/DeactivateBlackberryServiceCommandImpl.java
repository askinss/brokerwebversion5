package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.RIMXMLResponseDetail;
import com.vasconsulting.www.utility.RIMXMLUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class DeactivateBlackberryServiceCommandImpl implements Command {
	private RIMXMLUtility rimUtility = new RIMXMLUtility();
	private int rimStatus;
	private SubscriberDetail subscriberDetail;
	private TransactionLog transactionLog;
	private HibernateUtility hibernateUtility;
	private BillingPlanObjectUtility billingPlanObject;
	@SuppressWarnings("unused")
	private String receiverParams;
	private RIMXMLResponseDetail responseData = new RIMXMLResponseDetail();
	private Logger logger = Logger.getLogger(DeactivateBlackberryServiceCommandImpl.class);
	private EmailTaskExecutor myEmailTaskExecutor;
	
	public int execute() {
		/**
		 * Load the spring managed beans from the context.
		 */
				
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");	
		myEmailTaskExecutor = 
			(EmailTaskExecutor)ContextLoaderImpl.getBeans("myEmailTaskExecutor");
		logger.info("Execute called on DeactivateBlackberryServiceCommandImpl for subscriber with msisdn "+subscriberDetail.getMsisdn());
		
		SubscriberDetail sub = hibernateUtility.getSubscriberInformation("msisdn", subscriberDetail.getMsisdn());
		
		transactionLog = new TransactionLog();
		transactionLog.setDate_created(new GregorianCalendar());
		transactionLog.setMsisdn(subscriberDetail.getMsisdn());
		transactionLog.setService(subscriberDetail.getServiceplan());
		transactionLog.setShortcode(billingPlanObject.getShortCode());
		
		try
		{
			if (null == sub.getImsi())
				return StatusCodes.SUCCESS;
		}
		catch(NullPointerException ex)
		{
			return StatusCodes.OTHER_ERRORS;
		}
		
		try{
			responseData = rimUtility.cancelSubscriptionByIMSI(sub);
			rimStatus = new Integer(responseData.getErrorCode()).intValue();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			transactionLog.setStatus("FAILED");
			hibernateUtility.saveTransactionlog(transactionLog);
			return StatusCodes.OTHER_ERRORS;
		}

		
		if (rimStatus == 0 || (rimStatus >= 21000 && rimStatus <= 21050)) 
		{
			try
			{
				sub.setStatus("Deactivated");
				hibernateUtility.updateSubscriberDetail(sub);
			}
			catch(Exception ex)
			{
				myEmailTaskExecutor.sendQueuedEmails(sub.getMsisdn()+ " requested for a deactivate on RIM. This was successful, but the " +
						"subscriebr status has not been updated to deactivated status. Please rectify manually.");
			}
			transactionLog.setId(UUID.randomUUID().toString());
			transactionLog.setDescription("DEACTIVATE BB");
			transactionLog.setStatus("SUCCESSFUL");
			
			logger.info("Deactivate call on "+subscriberDetail.getMsisdn()+" returned "+rimStatus+". This is okay! proceeding.");			
			rimStatus = 0;
		}
		else 
		{
			transactionLog.setDescription("DEACTIVATE BB");
			transactionLog.setStatus("FAILED");
			logger.info("Deactivate call on "+subscriberDetail.getMsisdn()+" returned "+rimStatus);
		}
		
		hibernateUtility.saveTransactionlog(transactionLog);
		
		return rimStatus;
	}

	public int logTransaction()
	{
		return 0;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams; 		
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}

}
