package com.vasconsulting.www.interfaces.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.EulogyService;
import com.vasconsulting.www.utility.StatusCodes;

public class CheckEulogyPostCommandImpl implements Command {
	private HibernateUtility hibernateUtility;
	private EulogyService eulogyService;
	private String receiverParams;
	private SubscriberDetail subscriberDetail;
	private TransactionLog transactionLog;
	private BillingPlanObjectUtility billingPlanObjectLive;
	private Logger logger = Logger.getLogger(CheckEulogyPostCommandImpl.class);

	public int execute() {
		subscriberDetail = 
				(SubscriberDetail)ContextLoaderImpl.getBeans("subscriberDetail");
		billingPlanObjectLive = 
				(BillingPlanObjectUtility)ContextLoaderImpl.getBeans("billingPlanObject");
		hibernateUtility = 
				(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		
		eulogyService = (eulogyService == null) ? (new EulogyService()) : eulogyService;
		
		logger.info("Execute called on CheckEulogyPostCommandImpl for subscriber with msisdn "+
		stripLeadingMsisdnPlus(subscriberDetail.getMsisdn()));
		int amountToDeduct = new Integer(receiverParams).intValue();
		Double accountBalance;
		try{
			List<String> subStatus = eulogyService.checkPostSubscriberEulogy(stripLeadingMsisdnPlus(subscriberDetail.getMsisdn()));
			String status = subStatus.get(0);
			String accountBalString = subStatus.get(1);
			accountBalance = Double.valueOf(accountBalString);
			logger.info("The configured amount parameter for this plan is = "+this.receiverParams);
			logger.info("The response from eulogy is: status = "+status+ " accountBalance = "+accountBalString);
			if(amountToDeduct > accountBalance.intValue()) return StatusCodes.INSUFFICENT_BALANCE;
			else if(!status.equalsIgnoreCase("A")) return StatusCodes.INACTIVE_SUBSCRIBER;
			else return StatusCodes.SUCCESS;
		}catch(NullPointerException e){
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;
		}catch(Exception e){
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;
		}
	}

	private String stripLeadingMsisdnPlus(String msisdn){
		if (msisdn.startsWith("250")){
			return msisdn.substring(3, msisdn.length());
		}
		else if(msisdn.startsWith("+250")){
			return msisdn.substring(4, msisdn.length());
		}
		else return msisdn;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public int logTransaction() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		// TODO Auto-generated method stub
		
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		// TODO Auto-generated method stub
		
	}

	
}


/*package com.vasconsulting.www.interfaces.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.EulogyService;
import com.vasconsulting.www.utility.StatusCodes;

public class CheckEulogyPostCommandImpl implements Command {
	private EulogyService eulogyService;
	private String receiverParams;
	private SubscriberDetail subscriberDetail;
	private TransactionLog transactionLog;
	private BillingPlanObjectUtility billingPlanObject;
	private HibernateUtility hibernateUtility;
	private Logger logger = Logger.getLogger(CheckEulogyPostCommandImpl.class);

	public int execute() {
		eulogyService = (eulogyService == null) ? (new EulogyService()) : eulogyService;
		hibernateUtility = (hibernateUtility == null) ?	(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility") : hibernateUtility;
		logger.info("Execute called on CheckEulogyPostCommandImpl for subscriber with msisdn "+subscriberDetail.getMsisdn());
		transactionLog = (transactionLog == null) ? (new TransactionLog()) : transactionLog;
		transactionLog.setDescription("Check Postpaid Subscriber Eulogy Limit");
		transactionLog.setMsisdn(subscriberDetail.getMsisdn());
		transactionLog.setService(subscriberDetail.getServiceplan());
		transactionLog.setShortcode(subscriberDetail.getShortCode());
		int amountToDeduct = new Integer(receiverParams).intValue();
		Double accountBalance;
		try{
			List<String> subStatus = eulogyService.checkPostSubscriberEulogy(stripLeadingMsisdnPlus(subscriberDetail.getMsisdn()));
			String status = subStatus.get(0);
			String accountBalString = subStatus.get(1);
			accountBalance = Double.valueOf(accountBalString);
			logger.info("The configured amount parameter for this plan is = "+this.receiverParams);
			logger.info("The response from eulogy is: status = "+status+ " accountBalance = "+accountBalString);
			if(amountToDeduct > accountBalance.intValue()) {
				transactionLog.setStatus("SUCCESSFUL");
				hibernateUtility.saveTransactionlog(transactionLog);
				transactionLog.setDescription("SHORTCODE");
				transactionLog.setId(UUID.randomUUID().toString());
				transactionLog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionLog);
				return StatusCodes.INSUFFICENT_BALANCE;
			}
			else if(!status.equalsIgnoreCase("A")){
				transactionLog.setStatus("SUCCESSFUL");
				hibernateUtility.saveTransactionlog(transactionLog);
				transactionLog.setDescription("SHORTCODE");
				transactionLog.setId(UUID.randomUUID().toString());
				transactionLog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionLog);
				return StatusCodes.INACTIVE_SUBSCRIBER;
			}
			else {
				transactionLog.setStatus("SUCCESSFUL");
				hibernateUtility.saveTransactionlog(transactionLog);
				return StatusCodes.SUCCESS;
			}
		}catch(Exception e){
			transactionLog.setStatus("FAILED");
			hibernateUtility.saveTransactionlog(transactionLog);
			transactionLog.setDescription("SHORTCODE");
			transactionLog.setId(UUID.randomUUID().toString());
			transactionLog.setStatus("FAILED");
			hibernateUtility.saveTransactionlog(transactionLog);
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;
		}
	}

	public SubscriberDetail getSubscriberDetail() {
		return subscriberDetail;
	}

	public void setSubscriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	private String stripLeadingMsisdnPlus(String msisdn){
		if (msisdn.startsWith("250")){
			return msisdn;
		}
		else if(msisdn.startsWith("+250")){
			return msisdn.substring(1, msisdn.length());
		}
		else if(msisdn.startsWith("0")){
			return ("25" + msisdn);
		}
		else return msisdn;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}

	public int logTransaction() {
		// TODO Auto-generated method stub
		return 0;
	}

	public EulogyService getEulogyService() {
		return eulogyService;
	}

	public void setEulogyService(EulogyService eulogyService) {
		this.eulogyService = eulogyService;
	}

	public TransactionLog getTransactionLog() {
		return transactionLog;
	}

	public void setTransactionLog(TransactionLog transactionLog) {
		this.transactionLog = transactionLog;
	}

	public BillingPlanObjectUtility getBillingPlanObject() {
		return billingPlanObject;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;
	}

	public HibernateUtility getHibernateUtility() {
		return hibernateUtility;
	}

	public void setHibernateUtility(HibernateUtility hibernateUtility) {
		this.hibernateUtility = hibernateUtility;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}

}
*/