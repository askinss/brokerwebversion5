package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.FutureRenewal;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class SynchronizeFutureRenewalCommandImpl implements Command {
	private HibernateUtility hibernateUtility;
	private Logger logger = Logger
			.getLogger(SynchronizeFutureRenewalCommandImpl.class);
	private SubscriberDetail subscriberDetail;
	private FutureRenewal futureRenewal;
	private FutureRenewal futureRenewalDB;

	public int execute() {
		hibernateUtility = (HibernateUtility) ContextLoaderImpl
				.getBeans("hibernateUtility");
		futureRenewal = (futureRenewal == null) ? (FutureRenewal) ContextLoaderImpl
				.getBeans("futureRenewal") : futureRenewal;
		logger.info("Execute called on SynchronizeFutureRenewalCommandImpl for"
				+ " subscriber with MSISDN = " + subscriberDetail.getMsisdn());
		futureRenewalDB.setId(futureRenewal.getId());
		futureRenewalDB.setMsisdn(futureRenewal.getMsisdn());
		futureRenewalDB.setPurchasedAt(futureRenewal.getPurchasedAt());
		futureRenewalDB.setUsedAt(new GregorianCalendar());
		futureRenewalDB.setStatus("Used");
		try {
			if (hibernateUtility.updateFutureRenewal(futureRenewalDB) == 0) {
				return StatusCodes.SUCCESS;
			} else
				return StatusCodes.OTHER_ERRORS;
		} catch (Exception e) {
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;
		}
	}

	public void setReceiverParameters(String receiverParams) {
		// TODO Auto-generated method stub

	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub

	}

	public int logTransaction() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		// TODO Auto-generated method stub
		
	}
}
