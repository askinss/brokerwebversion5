package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.HomiscoService;
import com.vasconsulting.www.utility.StatusCodes;

public class SetupHomiscoPostCommandImpl implements Command {
	private SubscriberDetail subscriberDetail;
	private BillingPlanObjectUtility billingPlanObject;
	private TransactionLog transactionLog;
	private HibernateUtility hibernateUtility;
	private HomiscoService homiscoService;
	private String receiverParams;
	private Logger logger = Logger.getLogger(SetupHomiscoPostCommandImpl.class);
	
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = 
				(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		String [] receiverParamValues = receiverParams.split(":SEP:");
		
		
		logger.info("Execute called on SetupHomiscoPostCommandImpl for subscriber with msisdn "+subscriberDetail.getMsisdn());
				
		try {
			homiscoService = new HomiscoService();
			int responseValue = homiscoService.createPostSubscriberHomisco(
					stripLeadingMsisdnPlus(subscriberDetail.getMsisdn()), receiverParamValues[0], receiverParamValues[1], 
					receiverParamValues[2]);
			
			transactionLog = new TransactionLog();
			transactionLog.setDate_created(new GregorianCalendar());		
			transactionLog.setMsisdn(subscriberDetail.getMsisdn());
			transactionLog.setShortcode(billingPlanObject.getShortCode());			
			transactionLog.setDescription("Setup Homisco");
			
			if (responseValue <= 0)
			{
				int responseCode;
				if (responseValue == 0)
					responseCode = StatusCodes.OTHER_ERRORS_SETTING_HOMISCO;
				else if (responseValue == -1)
				{
					responseCode = StatusCodes.INVALID_MSISDN_SETTING_HOMISCO;
				}
				else if (responseValue == -2)
				{
					responseCode = StatusCodes.MINUTE_BUNDLE_SETTING_HOMISCO;
				}
				else
				{
					responseCode = StatusCodes.PREPAID_SUB_SETTING_HOMISCO;
				}
				transactionLog.setStatus("FAILED");
				transactionLog.setService(subscriberDetail.getServiceplan());
				hibernateUtility.saveTransactionlog(transactionLog);
				logger.info("Error setting up Homisco, code = "+responseValue+" returning status "+
						responseCode+", for subscriber["+subscriberDetail.getMsisdn()+"]");
				return responseCode;
			}
			else
			{
				transactionLog.setStatus("SUCCESSFUL");
				transactionLog.setService(subscriberDetail.getServiceplan());
				hibernateUtility.saveTransactionlog(transactionLog);
				subscriberDetail.setImsi(subscriberDetail.getImsi());
				logger.info("Homisco Billing Platform returned "+responseValue+" for subscriber "+subscriberDetail.getMsisdn());
				return StatusCodes.SUCCESS;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS_SETTING_HOMISCO;
		}		
	}
	
	public HomiscoService getHomiscoService() {
		return homiscoService;
	}

	public void setHomiscoService(HomiscoService homiscoService) {
		this.homiscoService = homiscoService;
	}

	private String stripLeadingMsisdnPlus(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("0")){
			return Msisdn;
		}
		else if(Msisdn.startsWith("+260")){
			return Msisdn.substring(3, Msisdn.length());
		}
		else if(Msisdn.startsWith("260")){
			return Msisdn.substring(2, Msisdn.length());
		}
		else return Msisdn;
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}

	public int rollBack()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}
	
}