package com.vasconsulting.www.interfaces.impl;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class CheckSubscriberDBStateInSyncWithSpringCommandImpl implements Command {
	private SubscriberDetail subscriberDetail;
	private HibernateUtility hibernateUtility;
	private TransactionLog transactionlog;
	Logger logger = Logger.getLogger(CheckSubscriberDBStateInSyncWithSpringCommandImpl.class);
	private BillingPlanObjectUtility billingPlanObject;
	
	/**
	 * This method will execute and perform service change on TABS based on the supplied
	 * receiver. The method will log at the end of the method execution the state of the 
	 * transaction run.
	 * @author Nnamdi Jibunoh
	 * @param
	 */
	public int execute() {
		/**
		 * Retrieve the Spring managed beans from the container
		 */
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		
		logger.info("Execute called on CheckSubscriberDBStateInSyncWithSpringCommandImpl for subscriber wit MSISDN = "+subscriberDetail.getMsisdn());
		transactionlog = new TransactionLog();
		transactionlog.setDate_created(new GregorianCalendar());
		transactionlog.setDescription("CHECK DB STATUS");
		transactionlog.setMsisdn(subscriberDetail.getMsisdn());
		transactionlog.setService(subscriberDetail.getServiceplan());
		transactionlog.setShortcode(subscriberDetail.getShortCode());
		transactionlog.setStatus("FAILED");
		int status = StatusCodes.OTHER_ERRORS;
		try {
			
			SubscriberDetail subscriber = hibernateUtility.getSubscriberInformation("msisdn", subscriberDetail.getMsisdn());
			
			logger.info("Call to check Subscriber Status database returned result : "+subscriber);
			
			{
				if (subscriber.getStatus().equalsIgnoreCase(subscriberDetail.getStatus()) && !hasNextSubscriptionDateChanged(subscriber.getNext_subscription_date()))
				{
						transactionlog.setStatus("SUCCESSFUL");
						hibernateUtility.saveTransactionlog(transactionlog);
						return StatusCodes.SUCCESS;								
				}
				else return StatusCodes.SUBSCRIBER_STATE_OUT_OF_SYNC;
			}			
			
		} catch (Exception e) {
			e.printStackTrace();
		}	
		return status;
	}
	
	private boolean hasNextSubscriptionDateChanged(
			Calendar next_subscription_date) {
		return (next_subscription_date.getTimeInMillis() > System.currentTimeMillis());
	}

	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	public void setReceiverParameters(String receiverParams)
	{
		// TODO Auto-generated method stub
		
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}
	

}
