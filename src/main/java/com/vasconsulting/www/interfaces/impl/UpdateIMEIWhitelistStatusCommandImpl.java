package com.vasconsulting.www.interfaces.impl;

import com.vasconsulting.www.domain.IMEIWhitelist;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

public class UpdateIMEIWhitelistStatusCommandImpl implements Command {
	private HibernateUtility hibernateUtility;
	private IMEIWhitelist imeiWhitelist;
	private Logger logger = Logger.getLogger(UpdateIMEIWhitelistStatusCommandImpl.class);
	private SubscriberDetail subscriberDetail;
	private TransactionLog transactionlog;


	
	public int execute() {
		try{
			logger.info("Inside the execute method of UpdateIMEIWhitelistStatusCommandImpl");
			hibernateUtility = 
					(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
			logger.info("Execute called on UpdateIMEIWhitelistStatusCommandImpl for" +
					" subscriber with MSISDN = "+subscriberDetail.getMsisdn());
			transactionlog = new TransactionLog();
			transactionlog.setDate_created(new GregorianCalendar());
			transactionlog.setMsisdn(subscriberDetail.getMsisdn());
			transactionlog.setShortcode(subscriberDetail.getShortCode());
			transactionlog.setDescription("Synchronize IMEI State");
			imeiWhitelist = hibernateUtility.getWhiteListedIMEI(subscriberDetail.getImei());
			imeiWhitelist.setImei(subscriberDetail.getImei());
			imeiWhitelist.setStatus("used");
			imeiWhitelist.setUsed_by_msisdn(subscriberDetail.getMsisdn());
			imeiWhitelist.setDate_updated(new GregorianCalendar());
			hibernateUtility.updateIMEIWhitelist(imeiWhitelist);
			transactionlog.setStatus("SUCCESSFUL");			
			hibernateUtility.saveTransactionlog(transactionlog);
			return StatusCodes.SUCCESS;
		}catch(Exception e){
			transactionlog.setStatus("FAILED");
			hibernateUtility.saveTransactionlog(transactionlog);
			return StatusCodes.OTHER_ERRORS;
		}

	}

	
	public void setReceiverParameters(String receiverParams) {
		// TODO Auto-generated method stub

	}

	
	public int logTransaction() {
		// TODO Auto-generated method stub
		return 0;
	}


	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}


	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		// TODO Auto-generated method stub
		
	}

}
