package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class CheckPrepSubscriberStatusCommandImpl implements Command {
	private SubscriberDetail subscriberDetail;
	private HibernateUtility hibernateUtility;
	private TransactionLog transactionlog;
	Logger logger = Logger.getLogger(CheckPrepSubscriberStatusCommandImpl.class);
	private BillingPlanObjectUtility billingPlanObject;
	
	/**
	 * This method will execute and perform service change on TABS based on the supplied
	 * receiver. The method will log at the end of the method execution the state of the 
	 * transaction run.
	 * @author Nnamdi Jibunoh
	 * @param
	 */
	public int execute() {
		/**
		 * Retrieve the Spring managed beans from the container
		 */
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		
		logger.info("Execute called on CheckPrepSubscriberStatusCommandImpl for subscriber wit MSISDN = "+subscriberDetail.getMsisdn());
		transactionlog = new TransactionLog();
		transactionlog.setDate_created(new GregorianCalendar());
		transactionlog.setDescription("CHECK DB STATUS");
		transactionlog.setMsisdn(subscriberDetail.getMsisdn());
		transactionlog.setService(subscriberDetail.getServiceplan());
		transactionlog.setShortcode(subscriberDetail.getShortCode());
			
		try {
			
			SubscriberDetail subscriber = hibernateUtility.getSubscriberInformation("msisdn", subscriberDetail.getMsisdn());
			
			logger.info("Call to check Subscriber Status database returned result : "+subscriber);
			
			if (subscriber == null || subscriber.getId().equalsIgnoreCase("")) return StatusCodes.SUCCESS;
			else //if(!subscriber.getId().equalsIgnoreCase("")) 
			{
				if (subscriber.getStatus().equalsIgnoreCase("Active"))
				{
						transactionlog.setStatus("SUCCESSFUL");
						hibernateUtility.saveTransactionlog(transactionlog);
						transactionlog.setStatus("FAILED");	
						transactionlog.setId(UUID.randomUUID().toString());
						transactionlog.setDescription("SHORTCODE");
						hibernateUtility.saveTransactionlog(transactionlog);
						logger.error("Subscriber cannot migrate while still active, aborting.");
						return StatusCodes.SUBSCRIBER_CANNOT_MIGRATE_WHILE_ACTIVE;								
				}
				else return StatusCodes.SUCCESS;
			}			
			
		} catch (Exception e) {
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;
		}		
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	public void setReceiverParameters(String receiverParams)
	{
		// TODO Auto-generated method stub
		
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}
	

}
