package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.celtel.databundle.service.impl.TelnetServiceManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.RIMXMLUtility;

public class DeactivateOldDBIMSICommandImpl implements Command {
	@Autowired
	private TelnetServiceManager telnetConnector;
	@Autowired
	private SubscriberDetail subscriberDetail;
	@Autowired
	private BillingPlanObjectUtility billingPlanObject;
	private TransactionLog transactionLog;
	@Autowired
	private HibernateUtility hibernateUtility;
	private String receiverParams;
	private Logger logger = Logger.getLogger(DeactivateOldDBIMSICommandImpl.class);
	private LoadAllProperties properties;
	private RIMXMLUtility rimXMLUtility;


	public int execute() {
		
		hibernateUtility = (hibernateUtility == null) ?	(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility") : hibernateUtility;
		transactionLog = (transactionLog == null) ? (new TransactionLog()) : transactionLog;	
		transactionLog.setDate_created(new GregorianCalendar());		
		transactionLog.setMsisdn(subscriberDetail.getMsisdn());
		transactionLog.setShortcode(billingPlanObject.getShortCode());	
		transactionLog.setService(subscriberDetail.getServiceplan());
		transactionLog.setDescription("Deactivate Old DB IMSI");
		properties = (properties == null) ? (new LoadAllProperties()) : properties;
		rimXMLUtility = (rimXMLUtility == null)? (new RIMXMLUtility()) : rimXMLUtility;
		logger.info("Execute called on DeactivateOldDBIMSICommandImpl for subscriber with MSISDN = "+subscriberDetail.getMsisdn());
		if (!isContainerIMSISameAsDBIMSI()){
			try{
				deactivateOldIMSI();
				transactionLog.setStatus("SUCCESSFUL");
			}catch(Exception e){
				e.printStackTrace();
				transactionLog.setStatus("FAILED");
			}
			hibernateUtility.saveTransactionlog(transactionLog);
		}
		return 0;

	}

	public SubscriberDetail getSubscriberDBIMSI(){
		if (hibernateUtility.getSubscriberInformation(subscriberDetail).size() > 0)
			return hibernateUtility.getSubscriberInformation(subscriberDetail).iterator().next();
		else
			return null;
	}

	public boolean isContainerIMSISameAsDBIMSI(){
		if (getSubscriberDBIMSI() == null)
			return true;
		else
		{
			logger.info("Container/EMA IMSI is: "+subscriberDetail.getImsi()+ " and DB IMSI is "+getSubscriberDBIMSI().getImsi()+" for msisdn "+subscriberDetail.getMsisdn());
			return (subscriberDetail.getImsi().equalsIgnoreCase(getSubscriberDBIMSI().getImsi()));
		}
	}

	public void deactivateOldIMSI(){
		logger.info("Attempting to Deactivate Old IMSI "+getSubscriberDBIMSI().getImsi() + "for msisdn "+getSubscriberDBIMSI().getMsisdn());
		rimXMLUtility.cancelSubscriptionByIMSI(getSubscriberDBIMSI());
	}

	public BillingPlanObjectUtility getBillingPlanObject() {
		return this.billingPlanObject;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;
	}

	public HibernateUtility getHibernateUtility() {
		return hibernateUtility;
	}

	public void setHibernateUtility(HibernateUtility hibernateUtility) {
		this.hibernateUtility = hibernateUtility;
	}


	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams.toLowerCase();
	}

	public String getReceiverParameters(){
		return receiverParams;
	}

	public int rollBack()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public LoadAllProperties getProperties() {
		return properties;
	}

	public void setProperties(LoadAllProperties properties) {
		this.properties = properties;
	}

	public String configuredIMSIs() {
		String imsiType = subscriberType() + "IMSIRange";
		logger.info("Trying to get configured range for: "+imsiType);
		return properties.getProperty(imsiType);
	}

	public String subscriberType(){
		return billingPlanObject.getDescription().toLowerCase().split(" ")[0];
	}

	public boolean isIMSIValid(String imsi) {
		boolean resp = false;
		logger.info("Configured imsi range is: "+configuredIMSIs());
		String[] configuredIMSIArray = configuredIMSIs().split(",");
		for (int i = 0; i < configuredIMSIArray.length; i++ ){
			if (imsi.startsWith(configuredIMSIArray[i].trim())){
				resp = true;
				return resp;
			}
		}
		return resp;
	}

	public boolean isIMSIValidPrepaidIMSI(String imsi){
		return false;
	}

	public SubscriberDetail getSubscriberDetail() {
		return this.subscriberDetail;
	}

	public void setSubscriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public TelnetServiceManager getTelnetConnector() {
		return telnetConnector;
	}

	public void setTelnetConnector(TelnetServiceManager telnetConnector) {
		this.telnetConnector = telnetConnector;
	}

	public TransactionLog getTransactionLog() {
		return transactionLog;
	}

	public void setTransactionLog(TransactionLog transactionLog) {
		this.transactionLog = transactionLog;
	}

	public RIMXMLUtility getRimXMLUtility() {
		return rimXMLUtility;
	}

	public void setRimXMLUtility(RIMXMLUtility rimXMLUtility) {
		this.rimXMLUtility = rimXMLUtility;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
}
