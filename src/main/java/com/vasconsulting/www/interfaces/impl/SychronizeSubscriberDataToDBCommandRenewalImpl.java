/**
 * This class is used to check the type of subscriber. If the subscriber is a POSTPAID or PREPAID subscriber
 * It returns 0 if subscriber is postpaid and 500 if prepaid.
 * @author nnamdi Jibunoh
 */
package com.vasconsulting.www.interfaces.impl;

import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.StatusCodes;

public class SychronizeSubscriberDataToDBCommandRenewalImpl implements Command {
	
	private SubscriberDetail subscriberDetail;
	private BillingPlanObjectUtility billingPlanObject;
	private int status;
	Collection<SubscriberDetail> subscriberDetailDB;
	private HibernateUtility hibernateUtility;
	private EmailTaskExecutor emailTaskExecutor;
	Logger logger = Logger.getLogger(SychronizeSubscriberDataToDBCommandRenewalImpl.class);
		
	/**
	 * This method would make an attempt to update subscriber details in DB, and for a failure would send an email to the admin	 * @author Nnamdi Jibunoh
	 * @param
	 */
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");		
		emailTaskExecutor = 
			(EmailTaskExecutor)ContextLoaderImpl.getBeans("myEmailTaskExecutor");
		SubscriberDetail subscriberDetailHib = new SubscriberDetail();
		
		logger.info("Execute called on SychronizeSubscriberDataToDBCommandRenewalImpl for subscriber with MSISDN = "+subscriberDetail.getMsisdn());
		subscriberDetail.setLast_subscription_date(new GregorianCalendar());
		subscriberDetail.setNext_subscription_date(getNextSubscriptionDate(subscriberDetail.getServicetype()));
		subscriberDetailHib.setAutoRenew(subscriberDetail.getAutoRenew());
		subscriberDetailHib.setDate_created(subscriberDetail.getDate_created());
		subscriberDetailHib.setEmail(subscriberDetail.getEmail());
		subscriberDetailHib.setFirstname(subscriberDetail.getFirstname());
		subscriberDetailHib.setId(subscriberDetail.getId());
		subscriberDetailHib.setImei(subscriberDetail.getImei());
		subscriberDetailHib.setImsi(subscriberDetail.getImsi());
		subscriberDetailHib.setLast_subscription_date(new GregorianCalendar());
		subscriberDetailHib.setLastname(subscriberDetail.getLastname());
		subscriberDetailHib.setMiddlename(subscriberDetail.getMiddlename());
		subscriberDetailHib.setMsisdn(subscriberDetail.getMsisdn());
		subscriberDetailHib.setNext_subscription_date(subscriberDetail.getNext_subscription_date());
		subscriberDetailHib.setPin(subscriberDetail.getPin());
		subscriberDetailHib.setPostpaidSubscriber(subscriberDetail.getPostpaidSubscriber());
		subscriberDetailHib.setPrepaidSubscriber(subscriberDetail.getPrepaidSubscriber());
		subscriberDetailHib.setServiceplan(subscriberDetail.getServiceplan());
		
		if (subscriberDetail.getServicetype() < 1)
		{
			subscriberDetailHib.setServicetype(new Integer(billingPlanObject.getValidity()));
		}
		else 
		{
			subscriberDetailHib.setServicetype(subscriberDetail.getServicetype());
		}
		
		subscriberDetailHib.setShortCode(subscriberDetail.getShortCode());
		subscriberDetailHib.setStatus("Active");
				
		logger.info("Next subscription date for subscriber with MSISDN: "+subscriberDetail.getMsisdn()+"" +
				" is "+subscriberDetail.getNext_subscription_date().getTime());
		
		status = hibernateUtility.updateSubscriberDetail(subscriberDetailHib);
		if (status != StatusCodes.SUCCESS)
		{
			emailTaskExecutor.sendQueuedEmails("********* CRITICAL ["+subscriberDetail.getMsisdn()+"] ************\n" +
					"Subscriber with details "+
					"\nMSISDN: "+subscriberDetail.getMsisdn()+
					"\nNEXT_SUBSCRIPTION_DATE: " +subscriberDetail.getNext_subscription_date()+
					"\nSERVICETYPE: " +subscriberDetail.getServicetype()+
					"\nSERVICEPLANID: " +subscriberDetail.getServiceplan()+
					"\nSHORTCODE: " +subscriberDetail.getShortCode()+
					"could not be synchronized to database in the renewal. This mostly likely means the subscriber " +
							"detail is out-of-sync with the network. Please retify manually or notify the administrator.");
		}
		return 0;	

	}
	
	
	private GregorianCalendar getNextSubscriptionDate(int noOfDays) {
		GregorianCalendar calendar1 = new GregorianCalendar();
		calendar1.add(GregorianCalendar.DAY_OF_MONTH,
				new Integer(noOfDays).intValue());
		return calendar1;
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
		
	public void setReceiverParameters(String receiverParams)
	{
		// TODO Auto-generated method stub
		
	}


	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}	
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}


	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		// TODO Auto-generated method stub
		this.billingPlanObject = billingPlanObject;
	}

}
