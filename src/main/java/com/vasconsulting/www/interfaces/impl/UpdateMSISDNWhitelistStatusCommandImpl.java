package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.IMEIWhitelist;
import com.vasconsulting.www.domain.MSISDNWhitelist;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class UpdateMSISDNWhitelistStatusCommandImpl implements Command {
	private HibernateUtility hibernateUtility;
	private MSISDNWhitelist msisdnsWhitelist;
	private Logger logger = Logger.getLogger(UpdateMSISDNWhitelistStatusCommandImpl.class);
	private SubscriberDetail subscriberDetail;
	private TransactionLog transactionlog;

	
	public int execute() {
		try{
			System.out.println("Inside the execute method of UpdateMSISDNWhitelistStatusCommandImpl");
			logger.info("Execute called on UpdateMSISDNWhitelistStatusCommandImpl for" +
					" subscriber with MSISDN = "+subscriberDetail.getMsisdn());		
			hibernateUtility = 
					(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
			transactionlog = new TransactionLog();
			transactionlog.setDate_created(new GregorianCalendar());
			transactionlog.setMsisdn(subscriberDetail.getMsisdn());
			transactionlog.setShortcode(subscriberDetail.getShortCode());
			transactionlog.setDescription("Synchronize MSISDN State");
			msisdnsWhitelist = hibernateUtility.getWhiteListedMSISDN(subscriberDetail.getMsisdn());
			msisdnsWhitelist.setMsisdn(subscriberDetail.getMsisdn());
			msisdnsWhitelist.setStatus("used");
			msisdnsWhitelist.setDate_updated(new GregorianCalendar());
			msisdnsWhitelist.setUsed_by_imei(subscriberDetail.getImei());
			hibernateUtility.updateMSISDNWhitelist(msisdnsWhitelist);
			transactionlog.setStatus("SUCCESSFUL");			
			hibernateUtility.saveTransactionlog(transactionlog);
			return StatusCodes.SUCCESS;
		}catch(Exception e){
			e.printStackTrace();
			transactionlog.setStatus("FAILED");
			hibernateUtility.saveTransactionlog(transactionlog);
			return StatusCodes.OTHER_ERRORS;
		}
	}

	
	public void setReceiverParameters(String receiverParams) {
		// TODO Auto-generated method stub

	}

	
	public int logTransaction() {
		// TODO Auto-generated method stub
		return 0;
	}


	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}


	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		// TODO Auto-generated method stub
		
	}

}
