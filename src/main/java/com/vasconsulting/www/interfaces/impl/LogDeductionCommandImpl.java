package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.vasconsulting.www.domain.DeductionLog;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class LogDeductionCommandImpl implements Command {
	@Autowired
	protected SubscriberDetail subscriberDetail;
	@Autowired
	private BillingPlanObjectUtility billingPlanObject;
	private DeductionLog deductionLog = new DeductionLog();
	private String receiverParams;
	@Autowired
	private HibernateUtility hibernateUtility;
	private Logger logger = Logger.getLogger(LogDeductionCommandImpl.class);
	
	public int execute() {
		/**
		 * Retrieve the Spring managed beans from the container
		 */
		
		
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		
		deductionLog.setDate_created(new GregorianCalendar());		
		deductionLog.setMsisdn(subscriberDetail.getMsisdn());
		deductionLog.setImsi(subscriberDetail.getImsi());
		deductionLog.setAmount(Double.valueOf(billingPlanObject.getCost()));
		deductionLog.setIsprepaidsubscriber(new Integer(receiverParams));
		deductionLog.setService(subscriberDetail.getServiceplan());
		deductionLog.setShortcode(subscriberDetail.getShortCode());
		
		try {
			int response = hibernateUtility.saveDeductionLogs(deductionLog);
			
			logger.info("Request to log the save deductionLog entry returned with code"+response);
			return StatusCodes.SUCCESS;
			
		} catch (Exception e) {		
			
			e.printStackTrace();
			return StatusCodes.SUCCESS;
		}
		
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
		
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		// TODO Auto-generated method stub
		this.subscriberDetail = subscriberDetail;
	}

	public BillingPlanObjectUtility getBillingPlanObject() {
		return (billingPlanObject == null) ? (BillingPlanObjectUtility)ContextLoaderImpl.getBeans("billingPlanObject") : billingPlanObject;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;
	}
	
	public HibernateUtility getHibernateUtility() {
		return hibernateUtility;
	}

	public void setHibernateUtility(HibernateUtility hibernateUtility) {
		this.hibernateUtility = hibernateUtility;
	}

}
