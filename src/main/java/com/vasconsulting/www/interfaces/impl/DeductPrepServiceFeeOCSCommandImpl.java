package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.UCIPServiceRequestManager;
import com.huawei.www.bme.cbsinterface.cbs.accountmgrmsg.AdjustAccountResultMsg;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.OCSSubscriberUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class DeductPrepServiceFeeOCSCommandImpl implements Command {
	private SubscriberDetail subscriberDetail;
	private BillingPlanObjectUtility billingPlanObject;
	private TransactionLog transactionLog;
	private HibernateUtility hibernateUtility;
	private String receiverParams;
	private Logger logger = Logger.getLogger(DeductPrepServiceFeeOCSCommandImpl.class);
	private OCSSubscriberUtility ocsConnector =  new OCSSubscriberUtility();
	
	public int execute() {
		/**
		 * Retrieve the Spring managed beans from the container
		 */
		
		hibernateUtility = (hibernateUtility == null) ?
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility") : hibernateUtility;
		transactionLog = (transactionLog == null) ? new TransactionLog() : transactionLog;
		transactionLog.setDate_created(new GregorianCalendar());		
		transactionLog.setMsisdn(subscriberDetail.getMsisdn());
		transactionLog.setService(subscriberDetail.getServiceplan());
		transactionLog.setShortcode(billingPlanObject.getShortCode());
		transactionLog.setDescription("DEDUCT AIRTIME");
		
		try {
			if (receiverParams == null)
				return StatusCodes.ERROR_NO_AMT_TO_DEDUCT;
			
			Long amountToDeduct = Long.valueOf(billingPlanObject.getCost()) * Long.valueOf(receiverParams);
			amountToDeduct = amountToDeduct * -1;
			logger.info("The actual amount to deduct from subscriber ["+subscriberDetail.getMsisdn()+"] is = "+amountToDeduct);			
						
			AdjustAccountResultMsg ocsResponse = 
					ocsConnector.deductServiceAirtime(subscriberDetail.getMsisdn(), amountToDeduct, billingPlanObject.getExternalData());
			
			if (ocsResponse != null)
			{
				logger.info("Call to bill subscriber["+subscriberDetail.getMsisdn()+"] code = "+ocsResponse.getResultHeader().getResultCode());
				logger.info("Call to bill subscriber["+subscriberDetail.getMsisdn()+"] messg = "+ocsResponse.getResultHeader().getResultDesc());
			}
			
			if (ocsResponse != null && ocsResponse.getResultHeader().getResultCode().equalsIgnoreCase("405000000")) {
				transactionLog.setStatus("SUCCESSFUL");
				hibernateUtility.saveTransactionlog(transactionLog);
				transactionLog.setId(UUID.randomUUID().toString());
				transactionLog.setService(String.valueOf(amountToDeduct));
				transactionLog.setDescription("AMOUNT DEDUCTED");
				hibernateUtility.saveTransactionlog(transactionLog);
				return StatusCodes.SUCCESS;
			}
			else {				
				transactionLog.setStatus("FAILED");				
				hibernateUtility.saveTransactionlog(transactionLog);
				transactionLog.setId(UUID.randomUUID().toString());
				transactionLog.setDescription("SHORTCODE");
				hibernateUtility.saveTransactionlog(transactionLog);				
				return StatusCodes.INSUFFICENT_BALANCE;
			}
			
		} catch (Exception e) {		
			transactionLog.setStatus("FAILED");				
			hibernateUtility.saveTransactionlog(transactionLog);
			transactionLog.setDescription("SHORTCODE");
			transactionLog.setId(UUID.randomUUID().toString());
			hibernateUtility.saveTransactionlog(transactionLog);
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS_IN_DEDUCTION;
		}
		
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}
	

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}
}
