package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.TelnetServiceManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.HibernateUtilityImpl;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.RIMQueryUtil;
import com.vasconsulting.www.utility.RIMXMLResponseDetail;
import com.vasconsulting.www.utility.RIMXMLUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class PostPaidSubscriberNotAllowedCommandImpl implements Command {
	private SubscriberDetail subscriberDetail;
	private TransactionLog transactionLog;
	private HibernateUtility hibernateUtility;
	private BillingPlanObjectUtility billingPlanObject;	
	private Logger logger = Logger.getLogger(PostPaidSubscriberNotAllowedCommandImpl.class);
	private EmailTaskExecutor myEmailTaskExecutor;
	private LoadAllProperties properties;
	private String receiverParams;
	
	public int execute() {
		/**
		 * Load the spring managed beans from the context.
		 */
				
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");	
		myEmailTaskExecutor = 
			(EmailTaskExecutor)ContextLoaderImpl.getBeans("myEmailTaskExecutor");
		properties = 
			(LoadAllProperties)ContextLoaderImpl.getBeans("loadProperties");
		
		logger.info("Execute called on PostPaidSubscriberNotAllowed for subscriber with msisdn "+subscriberDetail.getMsisdn());
		
		SubscriberDetail sub = hibernateUtility.getSubscriberInformation("msisdn", subscriberDetail.getMsisdn());
		
		transactionLog = new TransactionLog();
		transactionLog.setDate_created(new GregorianCalendar());
		transactionLog.setMsisdn(subscriberDetail.getMsisdn());
		transactionLog.setService(subscriberDetail.getServiceplan());
		transactionLog.setShortcode(billingPlanObject.getShortCode());
		transactionLog.setService("Check Prepaid/Postpaid");
		hibernateUtility.saveTransactionlog(transactionLog);
		
		
		if (!(sub.getPrepaidSubscriber() == 1)){
			return StatusCodes.POSTPAID_SUBSCRIBER_NOT_ALLOWED;
		}else return StatusCodes.SUCCESS;			
		
	}

	public int logTransaction()
	{
		return 0;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams; 		
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}
	
	

}
