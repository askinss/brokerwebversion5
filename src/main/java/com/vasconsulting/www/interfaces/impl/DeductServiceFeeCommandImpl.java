package com.vasconsulting.www.interfaces.impl;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Map;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.UCIPServiceRequestManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class DeductServiceFeeCommandImpl implements Command {
	private UCIPServiceRequestManager ucipConnector = new UCIPServiceRequestManager();
	private SubscriberDetail subscriberDetail;
	private BillingPlanObjectUtility billingPlanObject;
	private TransactionLog transactionLog;
	private HibernateUtility hibernateUtility;
	private String receiverParams;
	private Logger logger = Logger.getLogger(DeductServiceFeeCommandImpl.class);
	
	public int execute() {
		/**
		 * Retrieve the Spring managed beans from the container
		 */
		
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		
		try {
			if (receiverParams == null)
				return StatusCodes.ERROR_NO_AMT_TO_DEDUCT;
			
			double amountToDeduct = new Double(receiverParams).doubleValue();
			DecimalFormat decimalFormatter = new DecimalFormat("#####.##");
			
			/**
			 * Get the amount to deduct from the subscriber based on the time of the month it is.
			 */				
			
			double daysToEndOfMonth = daysBetween(new GregorianCalendar(), getLastDateOfMonth());
			int numberOfDayInMonth = new GregorianCalendar().getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
			
			logger.info("The configured amount parameter for this plan is = "+this.receiverParams);
			logger.info("The number of days to end of month is ="+daysToEndOfMonth);
			logger.info("This month has a total number of "+numberOfDayInMonth+" days");
			
			daysToEndOfMonth = daysToEndOfMonth / numberOfDayInMonth;			
			amountToDeduct = amountToDeduct * daysToEndOfMonth;
			
			amountToDeduct = amountToDeduct;
			
			logger.info("The actual amount to deduct from subscriber ["+subscriberDetail.getMsisdn()+"] is = "+amountToDeduct);			
						
			@SuppressWarnings("unchecked")
			Map<String, String> ucipResponse = ucipConnector.updateSubscriberBalance(subscriberDetail.getMsisdn(), 
					new Long(Math.round(new Double(decimalFormatter.format(amountToDeduct)))).toString(), 
					billingPlanObject.getExternalData());
			
			transactionLog = new TransactionLog();
			transactionLog.setDate_created(new GregorianCalendar());		
			transactionLog.setMsisdn(subscriberDetail.getMsisdn());
			transactionLog.setService(subscriberDetail.getServiceplan());
			transactionLog.setShortcode(billingPlanObject.getShortCode());			
			transactionLog.setDescription("DEDUCT AIRTIME");
			
			if (ucipResponse.get("responseCode").equalsIgnoreCase("0")) {
				transactionLog.setStatus("SUCCESSFUL");
				hibernateUtility.saveTransactionlog(transactionLog);
				
				return StatusCodes.SUCCESS;
			}
			else {
				transactionLog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionLog);
				
				return StatusCodes.INSUFFICENT_BALANCE;
			}
			
		} catch (Exception e) {			
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS_IN_DEDUCTION;
		}
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}
	
	private int daysBetween(GregorianCalendar startDate, GregorianCalendar endDate) {  
		GregorianCalendar date = (GregorianCalendar) startDate.clone();  
		int daysBetween = 0;  
		while (date.before(endDate)) {  
			date.add(GregorianCalendar.DAY_OF_MONTH, 1);  
			daysBetween++;  
		}  
		return daysBetween;  
	}
	
	private GregorianCalendar getLastDateOfMonth(){
		GregorianCalendar calendar  = new GregorianCalendar();
		int dayOfMonth  = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
		
		int daysToAdd = dayOfMonth - calendar.get(GregorianCalendar.DAY_OF_MONTH);
		
		calendar.add(GregorianCalendar.DAY_OF_MONTH, daysToAdd);	
		return calendar;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}

}
