package com.vasconsulting.www.interfaces.impl;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import sun.net.TelnetProtocolException;

import com.celtel.databundle.service.impl.TelnetServiceManager;
import com.celtel.databundle.service.impl.UCIPServiceRequestManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.RIMQueryUtil;
import com.vasconsulting.www.utility.RIMXMLUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class DeductPrepServiceFeeCommandRenewalImpl implements Command {
	private UCIPServiceRequestManager ucipConnector = new UCIPServiceRequestManager();
	private SubscriberDetail subscriberDetail;
	private BillingPlanObjectUtility billingPlanObject;
	private TransactionLog transactionLog;
	private HibernateUtility hibernateUtility;
	private String receiverParams;
	private Logger logger = Logger.getLogger(DeductPrepServiceFeeCommandRenewalImpl.class);
	
	public int execute() {
		/**
		 * Retrieve the Spring managed beans from the container
		 */		
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");	
		transactionLog = new TransactionLog();
		transactionLog.setDate_created(new GregorianCalendar());		
		transactionLog.setMsisdn(subscriberDetail.getMsisdn());
		transactionLog.setService(subscriberDetail.getServiceplan());
		transactionLog.setShortcode(subscriberDetail.getShortCode()+ "_Renewal");
		transactionLog.setDescription("DEDUCT AIRTIME");
		logger.info("Execute called on DeductPrepServiceFeeCommandRenewalImpl for subscriber with msisdn "
				+ subscriberDetail.getMsisdn());
		
		try {
			if (receiverParams == null)
				return StatusCodes.ERROR_NO_AMT_TO_DEDUCT;
			Double amountToDeduct = Double.valueOf(receiverParams);			
			amountToDeduct = amountToDeduct * -1;
			
			logger.info("The actual amount to deduct from subscriber ["+subscriberDetail.getMsisdn()+"] is = "+amountToDeduct);			
						
			@SuppressWarnings("unchecked")
			Map<String, String> ucipResponse = ucipConnector.updateSubscriberBalance(subscriberDetail.getMsisdn(), 
					new Integer(amountToDeduct.intValue()).toString(), billingPlanObject.getExternalData());			
			
			if (ucipResponse.get("responseCode").equalsIgnoreCase("0")) {
				transactionLog.setStatus("SUCCESSFUL");
				hibernateUtility.saveTransactionlog(transactionLog);
				transactionLog.setId(UUID.randomUUID().toString());
				transactionLog.setService(String.valueOf(amountToDeduct));
				transactionLog.setDescription("AMOUNT DEDUCTED");
				hibernateUtility.saveTransactionlog(transactionLog);
				return StatusCodes.SUCCESS;
			}
			else {
				transactionLog.setStatus("FAILED");				
				hibernateUtility.saveTransactionlog(transactionLog);
				transactionLog.setId(UUID.randomUUID().toString());
				transactionLog.setDescription("SHORTCODE");
				hibernateUtility.saveTransactionlog(transactionLog);				
				return StatusCodes.INSUFFICENT_RENEWAL_BALANCE;
			}
			
		} catch (Exception e) {		
			transactionLog.setStatus("FAILED");				
			hibernateUtility.saveTransactionlog(transactionLog);
			transactionLog.setDescription("SHORTCODE");
			transactionLog.setId(UUID.randomUUID().toString());
			hibernateUtility.saveTransactionlog(transactionLog);
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS_IN_DEDUCTION;
		}
		
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}
	
	private int daysBetween(GregorianCalendar startDate, GregorianCalendar endDate) {  
		GregorianCalendar date = (GregorianCalendar) startDate.clone();  
		int daysBetween = 0;  
		while (date.before(endDate)) {  
			date.add(GregorianCalendar.DAY_OF_MONTH, 1);  
			daysBetween++;  
		}  
		return daysBetween;  
	}
	
	private GregorianCalendar getLastDateOfMonth(){
		GregorianCalendar calendar  = new GregorianCalendar();
		int dayOfMonth  = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
		
		int daysToAdd = dayOfMonth - calendar.get(GregorianCalendar.DAY_OF_MONTH);
		
		calendar.add(GregorianCalendar.DAY_OF_MONTH, daysToAdd);	
		return calendar;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}

}
