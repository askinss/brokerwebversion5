package com.vasconsulting.www.interfaces.impl;

import com.vasconsulting.www.utility.StatusCodes;

public class RemoveSubscriberEulogyServiceCommandImpl extends
		CreateSubscriberEulogyServiceCommandImpl {
	

	public RemoveSubscriberEulogyServiceCommandImpl(){
		super.flag = "D";
	}
	
	public int execute(){
		System.out.println("This is receiverParams: "+receiverParams);
		int responseCode = super.execute();
		if (responseCode == StatusCodes.EULOGY_MSISDN_SUCCESSFULLY_DETACHED || responseCode == StatusCodes.EULOGY_WRONG_COMPONENT_ID || responseCode == StatusCodes.EULOGY_MSISDN_DOES_NOT_EXIST){
			return StatusCodes.SUCCESS;
		}else return responseCode;
	}

}
