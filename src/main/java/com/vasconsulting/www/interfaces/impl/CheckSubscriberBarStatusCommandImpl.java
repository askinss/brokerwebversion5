package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.interfaces.TabsInterface;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class CheckSubscriberBarStatusCommandImpl implements Command {
	private SubscriberDetail subscriberDetail;
	private TransactionLog transactionlog;
	Logger logger = Logger.getLogger(CheckSubscriberBarStatusCommandImpl.class);
	private String receiverParams;
	private HibernateUtility hibernateUtility;
	private TabsInterface tabs;
	private BillingPlanObjectUtility billingPlanObject;

	public int execute() {

		logger.info("Execute called on CheckSubscriberPostpidStatusCommandImpl for subscriber wit MSISDN = "
				+ subscriberDetail.getMsisdn());
		hibernateUtility = 
				(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		tabs = (tabs == null) ? (TabsInterface)ContextLoaderImpl.getBeans("tabs") : tabs;
		transactionlog = new TransactionLog();
		transactionlog.setDate_created(new GregorianCalendar());
		transactionlog.setDescription("CHECK SUBSCRIBER BARRED STATUS");
		transactionlog.setMsisdn(subscriberDetail.getMsisdn());
		transactionlog.setService(subscriberDetail.getServiceplan());
		transactionlog.setShortcode(subscriberDetail.getShortCode());
		
		try {
			HashMap<String,String> tabsResponse = tabs.getSubscriberInformation(subscriberDetail.getMsisdn());

			logger.info("Call to check Subscriber Status on TABS API returned result : "
					+ tabsResponse);

			if (tabsResponse.get("errorResponse") != null){
				transactionlog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionlog);
				transactionlog.setId(UUID.randomUUID().toString());
				transactionlog.setDescription("SHORTCODE");
				hibernateUtility.saveTransactionlog(transactionlog);
				return StatusCodes.OTHER_ERRORS;
				}
			else if (tabsResponse.get("STATUS").equalsIgnoreCase("30")) {
				subscriberDetail.setStatus("Active");
				transactionlog.setStatus("SUCCESSFUL");
				hibernateUtility.saveTransactionlog(transactionlog);
				return StatusCodes.SUCCESS;
			} else {
				subscriberDetail.setStatus("Deactivated");
				transactionlog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionlog);
				transactionlog.setId(UUID.randomUUID().toString());
				transactionlog.setDescription("SHORTCODE");
				hibernateUtility.saveTransactionlog(transactionlog);
				return StatusCodes.BARRED_SUBSCRIBER;
			}


		} catch (Exception e) {
			subscriberDetail.setStatus("Deactivated");
			transactionlog.setStatus("FAILED");
			hibernateUtility.saveTransactionlog(transactionlog);
			transactionlog.setId(UUID.randomUUID().toString());
			transactionlog.setDescription("SHORTCODE");
			hibernateUtility.saveTransactionlog(transactionlog);
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;
		}
	}

	
	public void setReceiverParameters(String receiverParams) {
		this.receiverParams = receiverParams;
	}

	
	public int logTransaction() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}


	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}

}
