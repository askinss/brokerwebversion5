/**
 * This class is used to check the type of subscriber. If the subscriber is a POSTPAID or PREPAID subscriber
 * It returns 0 if subscriber is postpaid and 500 if prepaid.
 * @author nnamdi Jibunoh
 */
package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.interfaces.TabsInterface;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class CheckSubscriberPostpaidStatusCommandImpl implements Command {
	private SubscriberDetail subscriberDetail;
	Logger logger = Logger.getLogger(CheckSubscriberPostpaidStatusCommandImpl.class);
	private String receiverParams;
	private HibernateUtility hibernateUtility;
	private TransactionLog transactionlog;
	private TabsInterface tabs;
	private BillingPlanObjectUtility billingPlanObject;
		
	/**
	 * This method will execute and perform service change on TABS based on the supplied
	 * receiver. The method will log at the end of the method execution the state of the 
	 * transaction run.
	 * @author Nnamdi Jibunoh
	 * @param
	 */
	public int execute() {
		/**
		 * Retrieve the Spring managed beans from the container
		 */
		logger.info("Execute called on CheckSubscriberPostpidStatusCommandImpl for subscriber wit MSISDN = "+subscriberDetail.getMsisdn());
		hibernateUtility = 
				(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");	
		tabs = (TabsInterface)ContextLoaderImpl.getBeans("tabs");
		
		transactionlog = new TransactionLog();
		transactionlog.setDate_created(new GregorianCalendar());
		transactionlog.setDescription("CHECK SUBSCRIBER POSTPAID STATUS");
		transactionlog.setMsisdn(subscriberDetail.getMsisdn());
		transactionlog.setService(subscriberDetail.getServiceplan());
		transactionlog.setShortcode(subscriberDetail.getShortCode());
		
		try {
			HashMap<String, String> tabsResponse = tabs.getPreOrPost(subscriberDetail.getMsisdn());
			
			logger.info("Call to check Subscriber Type on TABS API returned result : "+tabsResponse);
			
			if (tabsResponse.get("errorResponse") != null){
				transactionlog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionlog);
				transactionlog.setId(UUID.randomUUID().toString());
				transactionlog.setDescription("SHORTCODE");
				hibernateUtility.saveTransactionlog(transactionlog);
				return StatusCodes.OTHER_ERRORS;
			}else if(tabsResponse.get("PREPOST_PAID").equalsIgnoreCase("POST")){
				subscriberDetail.setPostpaidSubscriber(1);
				subscriberDetail.setPrepaidSubscriber(0);
				return StatusCodes.SUCCESS;
			}
			else {
				transactionlog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionlog);
				transactionlog.setId(UUID.randomUUID().toString());
				transactionlog.setDescription("SHORTCODE");
				hibernateUtility.saveTransactionlog(transactionlog);
				return StatusCodes.PREPAID_SUBSCRIBER;
			}
			
		} catch (Exception e) {
			transactionlog.setStatus("FAILED");
			hibernateUtility.saveTransactionlog(transactionlog);
			transactionlog.setId(UUID.randomUUID().toString());
			transactionlog.setDescription("SHORTCODE");
			hibernateUtility.saveTransactionlog(transactionlog);
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;
		}		
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
		

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}

}
