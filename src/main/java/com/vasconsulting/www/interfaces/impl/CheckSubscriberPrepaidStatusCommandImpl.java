/**
 * This class is used to check the type of subscriber. If the subscriber is a POSTPAID or PREPAID subscriber
 * It returns 0 if subscriber is postpaid and 500 if prepaid.
 * @author nnamdi Jibunoh
 */
package com.vasconsulting.www.interfaces.impl;

import java.rmi.RemoteException;
import java.util.HashMap;

import org.apache.log4j.Logger;

import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKGProxy;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPreOrPostPaidElement;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetPreOrPostPaidResponseElement;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;
import com.vasconsulting.www.utility.XMLUtility;

public class CheckSubscriberPrepaidStatusCommandImpl implements Command {
	private TBI_KPI_PKGProxy tbiProxy = new TBI_KPI_PKGProxy();
	private SubscriberDetail subscriberDetail;
	private XMLUtility xmlUtility = new  XMLUtility();
	Logger logger = Logger.getLogger(CheckSubscriberPrepaidStatusCommandImpl.class);
	private BillingPlanObjectUtility billingPlanObject;
		
	/**
	 * This method will execute and perform service change on TABS based on the supplied
	 * receiver. The method will log at the end of the method execution the state of the 
	 * transaction run.
	 * @author Nnamdi Jibunoh
	 * @param
	 */
	public int execute() {
		/**
		 * Retrieve the Spring managed beans from the container
		 */
		logger.info("Execute called on CheckSubscriberPrepaidStatusCommandImpl for subscriber wit MSISDN = "+subscriberDetail.getMsisdn());
		
		
		GetPreOrPostPaidElement subParams = new GetPreOrPostPaidElement();
		subParams.setSubno(stripLeadingMsisdnPrefix(subscriberDetail.getMsisdn()));
		subParams.setArea("0");
		subParams.setSubscrType("G");
		
		try {
			GetPreOrPostPaidResponseElement responseElement = tbiProxy.getPreOrPostPaid(subParams);
			
			HashMap<String, String> tabsResponse = xmlUtility.processTABSResponse(responseElement.getResult());
			
			logger.info("Call to check Subscriber Type on TABS API returned result : "+tabsResponse);
			
			if (tabsResponse.get("errorResponse") != null) return StatusCodes.OTHER_ERRORS;
			else if(tabsResponse.get("PREPOST_PAID").equalsIgnoreCase("PREP")) 
			{
				subscriberDetail.setImsi(tabsResponse.get("IMSI_NUMBER").trim());
				subscriberDetail.setPostpaidSubscriber(0);
				subscriberDetail.setPrepaidSubscriber(1);
				return StatusCodes.SUCCESS;
			}
			else {
				return StatusCodes.POSTPAID_SUBSCRIBER;
			}
			
		} catch (RemoteException e) {
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;
		}		
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
		
	private String stripLeadingMsisdnPrefix(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("0")){
			return Msisdn.substring(1, Msisdn.length());
		}
		else if (Msisdn.startsWith("234")){
			return Msisdn.substring(3, Msisdn.length());
		}
		else if(Msisdn.startsWith("+234")){
			return Msisdn.substring(4, Msisdn.length());
		}
		else return Msisdn;
	}

	public void setReceiverParameters(String receiverParams)
	{
		// TODO Auto-generated method stub
		
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}	
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}

}
