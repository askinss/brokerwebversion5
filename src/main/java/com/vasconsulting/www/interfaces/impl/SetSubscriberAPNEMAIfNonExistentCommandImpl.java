package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.TelnetServiceManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.StatusCodes;

public class SetSubscriberAPNEMAIfNonExistentCommandImpl extends
		SetSubscriberAPNEMACommandImpl {
	private TelnetServiceManager telnetConnector;
	private SubscriberDetail subscriberDetail;
	private TransactionLog transactionLog;
	private HibernateUtility hibernateUtility;
	private String receiverParams;
	private Logger logger = Logger
			.getLogger(SetSubscriberAPNEMAIfNonExistentCommandImpl.class);
	private BillingPlanObjectUtility billingPlanObject;
	private String receiver;

	@SuppressWarnings("unchecked")
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		subscriberDetail = (subscriberDetail == null) ? (SubscriberDetail) ContextLoaderImpl
				.getBeans("subscriberDetail") : subscriberDetail;
		hibernateUtility = (hibernateUtility == null) ? (HibernateUtility) ContextLoaderImpl
				.getBeans("hibernateUtility") : hibernateUtility;
		subscriberDetail = (subscriberDetail == null) ? (SubscriberDetail) ContextLoaderImpl
				.getBeans("subscriberDetail") : subscriberDetail;
		billingPlanObject = (billingPlanObject == null) ? (BillingPlanObjectUtility) ContextLoaderImpl
				.getBeans("billingPlanObject") : billingPlanObject;

		logger.info("Execute called on SetSubscriberAPNEMAIfNonExistentCommandImpl for subscriber with msisdn "
				+ subscriberDetail.getMsisdn());

		if (isSubscriberHavingBBAPN(subscriberDetail.getMsisdn())) {
			transactionLog = new TransactionLog();
			transactionLog.setDate_created(new GregorianCalendar());
			transactionLog.setMsisdn(subscriberDetail.getMsisdn());
			transactionLog.setShortcode(billingPlanObject.getShortCode());
			transactionLog.setService(subscriberDetail.getServiceplan());
			transactionLog.setDescription("Check Subscriber Has BB.net APN");
			transactionLog.setStatus("SUCCESSFUL");
			hibernateUtility.saveTransactionlog(transactionLog);
			logger.info("Subscriber " + subscriberDetail.getMsisdn()
					+ " has BB.net APN set");
			return StatusCodes.SUCCESS;
		} else {
			super.setBillingPlanObject(billingPlanObject);
			super.setReceiver(receiver);
			super.setReceiverParameters(receiverParams);
			super.setSubscriberDetail(subscriberDetail);
			super.setTelnetConnector(telnetConnector);
			return super.execute();
		}
	}


	public TelnetServiceManager getTelnetConnector() {
		return telnetConnector;
	}

	public void setTelnetConnector(TelnetServiceManager telnetConnector) {
		this.telnetConnector = telnetConnector;
	}

	public SubscriberDetail getSubscriberDetail() {
		return subscriberDetail;
	}

	public void setSubscriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public BillingPlanObjectUtility getBillingPlanObjectLive() {
		return billingPlanObject;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;
	}

	public TransactionLog getTransactionLog() {
		return transactionLog;
	}

	public void setTransactionLog(TransactionLog transactionLog) {
		this.transactionLog = transactionLog;
	}

	public HibernateUtility getHibernateUtility() {
		return hibernateUtility;
	}

	public void setHibernateUtility(HibernateUtility hibernateUtility) {
		this.hibernateUtility = hibernateUtility;
	}

	public String getReceiverParams() {
		return receiverParams;
	}

	public void setReceiverParams(String receiverParams) {
		this.receiverParams = receiverParams;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public int logTransaction() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams) {
		this.receiverParams = receiverParams;
	}

	public int rollBack() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public boolean isSubscriberHavingBBAPN(String msisdn) {
		try {
			telnetConnector = (telnetConnector == null) ? (TelnetServiceManager) ContextLoaderImpl
					.getBeans("telnetConnector") : telnetConnector;
			if (receiver == null) receiver = new LoadAllProperties().getProperty("apnID");
			return telnetConnector.getRawSubscriberInformation(msisdn)
					.contains("APNID,6,EQOSID,1");
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
