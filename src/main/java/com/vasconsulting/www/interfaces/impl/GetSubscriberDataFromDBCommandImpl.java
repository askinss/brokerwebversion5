/**
 * This class is used to check the type of subscriber. If the subscriber is a POSTPAID or PREPAID subscriber
 * It returns 0 if subscriber is postpaid and 500 if prepaid.
 * @author nnamdi Jibunoh
 */
package com.vasconsulting.www.interfaces.impl;

import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class GetSubscriberDataFromDBCommandImpl implements Command {
	
	private SubscriberDetail subscriberDetail;
	Collection<SubscriberDetail> subscriberDetailDB;
	private HibernateUtility hibernateUtility;
	Logger logger = Logger.getLogger(GetSubscriberDataFromDBCommandImpl.class);
	private BillingPlanObjectUtility billingPlanObject;
		
	/**
	 * This method will execute and perform service change on TABS based on the supplied
	 * receiver. The method will log at the end of the method execution the state of the 
	 * transaction run.
	 * @author Nnamdi Jibunoh
	 * @param
	 */
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		
		logger.info("Execute called on GetSubscriberDataFromDBCommandImpl for subscriber with MSISDN = "+subscriberDetail.getMsisdn());
				
		subscriberDetailDB = hibernateUtility.getSubscriberInformation(subscriberDetail);
		
		if (subscriberDetailDB != null && subscriberDetailDB.size() > 0)
		{
			for (Iterator<SubscriberDetail> iterator = subscriberDetailDB.iterator(); iterator.hasNext();) {
				SubscriberDetail subscriber = iterator.next();
				
				/**
				 * TODO:5000 Compare the returned value against the in-session value to make sure they are the same because in future
				 * all subscribers would be harmonized
				 */
				
			}
		}
		else 
		{
			logger.info("Subscriber "+subscriberDetail.getMsisdn()+" was not found in the database, so no in-session subscriberDetail is intact");
			return 0;
		}
		return 0;
			
					
			
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
		
	public void setReceiverParameters(String receiverParams)
	{
		// TODO Auto-generated method stub
		
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}	
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}
}
