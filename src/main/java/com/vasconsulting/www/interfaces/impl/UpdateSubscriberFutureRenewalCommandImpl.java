package com.vasconsulting.www.interfaces.impl;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.FutureRenewal;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class UpdateSubscriberFutureRenewalCommandImpl implements Command {
	private FutureRenewal futureRenewalDB;
	private FutureRenewal futureRenewal;
	private HibernateUtility hibernateUtility;
	private SubscriberDetail subscriberDetail;
	private Logger logger = Logger.getLogger(UpdateSubscriberFutureRenewalCommandImpl.class);

	public int execute() {
		hibernateUtility = (HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		ArrayList<FutureRenewal> futureRenewals = hibernateUtility.subscriberFutureRenewals(subscriberDetail.getMsisdn());
		if (futureRenewals.size() == 0) return StatusCodes.SUCCESS;
		futureRenewalDB = futureRenewals.get(0); 
		futureRenewalDB.setStatus("Used");
		futureRenewalDB.setUsedAt(new GregorianCalendar());
		logger.info("Execute called on UpdateSubscriberFutureRenewalCommandImpl for subscriber with msisdn "+futureRenewalDB.getMsisdn());
		futureRenewal = new FutureRenewal();
		futureRenewal.setId(futureRenewalDB.getId());
		futureRenewal.setMsisdn(futureRenewalDB.getMsisdn());
		futureRenewal.setPurchasedAt(futureRenewalDB.getPurchasedAt());
		futureRenewal.setShortCode(futureRenewalDB.getShortCode());
		futureRenewal.setStatus(futureRenewalDB.getStatus());
		futureRenewal.setUsedAt(futureRenewalDB.getUsedAt());
		hibernateUtility.updateFutureRenewal(futureRenewal);
		return StatusCodes.SUCCESS;
	}

	public void setReceiverParameters(String receiverParams) {
		// TODO Auto-generated method stub

	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub

	}

	public int logTransaction() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		// TODO Auto-generated method stub
		
	}

}
