package com.vasconsulting.www.interfaces.impl;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;

import org.apache.log4j.Logger;


public class GetSubscriberDetailsDBSimSwapCommandImpl implements Command {

	public SubscriberDetail getsubscriberDetail() {
		return subscriberDetail;
	}

	public void setsubscriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public SubscriberDetail getSubscriberDetail() {
		return subscriberDetail;
	}

	public void setSubscriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	private SubscriberDetail subscriberDetail;
	private HibernateUtility hibernateUtility;
	private Logger logger = Logger.getLogger(GetSubscriberBBStatusFromDBCommandImpl.class);

	public int execute() {
		hibernateUtility = (hibernateUtility == null) ? (HibernateUtility) ContextLoaderImpl
				.getBeans("hibernateUtility") : hibernateUtility;
		logger.info("Execute called on GetSubscriberBBStatusFromDBCommandImpl for subscriber with MSISDN = "+subscriberDetail.getMsisdn());

		subscriberDetail.setId(getSubscriberDBIMSI().getId());
		subscriberDetail.setMsisdn(getSubscriberDBIMSI().getMsisdn());
		subscriberDetail.setServiceplan(getSubscriberDBIMSI().getServiceplan());
		subscriberDetail.setDate_created(getSubscriberDBIMSI()
				.getDate_created());
		subscriberDetail.setLast_subscription_date(getSubscriberDBIMSI()
				.getLast_subscription_date());
		subscriberDetail.setServicetype(getSubscriberDBIMSI().getServicetype());
		subscriberDetail.setAutoRenew(getSubscriberDBIMSI().getAutoRenew());
		subscriberDetail.setNext_subscription_date(getSubscriberDBIMSI()
				.getNext_subscription_date());
		subscriberDetail.setShortCode(getSubscriberDBIMSI().getShortCode());
		subscriberDetail.setStatus(getSubscriberDBIMSI().getStatus());
		subscriberDetail.setPostpaidSubscriber(getSubscriberDBIMSI()
				.getPostpaidSubscriber());
		subscriberDetail.setPrepaidSubscriber(getSubscriberDBIMSI()
				.getPrepaidSubscriber());
		return 0;

	}

	public SubscriberDetail getSubscriberDBIMSI() {
		return hibernateUtility.getSubscriberInformation("msisdn", subscriberDetail.getMsisdn());
	}

	public void setSubscriberDetails(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;

	}

	public SubscriberDetail getSubscriberDetails() {
		return subscriberDetail;
	}

	public void setReceiverParameters(String receiverParams) {
		// TODO Auto-generated method stub
		
	}

	public int logTransaction() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		// TODO Auto-generated method stub
		
	}
}
