package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.TelnetServiceManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class CheckIMEIValidForPromoCommandImpl implements Command {
	private HibernateUtility hibernateUtility;
	private SubscriberDetail subscriberDetail;
	private TransactionLog transactionlog;
	private Logger logger = Logger
			.getLogger(CheckIMEIValidForPromoCommandImpl.class);
	private TelnetServiceManager telnet = new TelnetServiceManager();
	private BillingPlanObjectUtility billingPlanObject;

	public int execute() {
		hibernateUtility = (HibernateUtility) ContextLoaderImpl
				.getBeans("hibernateUtility");
		logger.info("Execute called on CheckIMEIValidForPromoCommandImpl for"
				+ " subscriber with MSISDN = " + subscriberDetail.getMsisdn());
		transactionlog = new TransactionLog();
		transactionlog.setDate_created(new GregorianCalendar());
		transactionlog.setMsisdn(subscriberDetail.getMsisdn());
		transactionlog.setShortcode(subscriberDetail.getShortCode());

		String imei;
		try {
			imei = telnet.getSubscriberIMEI(subscriberDetail.getMsisdn())
					.get("imeisv").toString();
			transactionlog.setDescription("GET IMEI");
			hibernateUtility.saveTransactionlog(transactionlog);
			logger.info("Got IMEI: " + imei + "for subscriber with MSISDN: "
					+ subscriberDetail.getMsisdn());
			boolean isIMEIWhitelist;
			transactionlog.setDescription("Check if IMEI is valid for promo");
			transactionlog.setId(UUID.randomUUID().toString());

			try {
				isIMEIWhitelist = hibernateUtility.isIMEIWhitelisted(imei);
				if (!(hibernateUtility.getWhiteListedIMEI(imei) == null)) {
					transactionlog.setStatus("SUCCESSFUL");
					hibernateUtility.saveTransactionlog(transactionlog);
					subscriberDetail.setImei(hibernateUtility
							.getWhiteListedIMEI(imei).getImei());
					System.out.println("Set IMEI for Subscriber: "
							+ subscriberDetail.getImei());
					return StatusCodes.SUCCESS;
				} else {
					transactionlog.setStatus("FAILED");
					hibernateUtility.saveTransactionlog(transactionlog);
					return StatusCodes.SUBSCRIBER_NOT_ALLOWED;
				}
			} catch (Exception e) {
				e.printStackTrace();
				transactionlog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionlog);
				return StatusCodes.OTHER_ERRORS;
			}
		} catch (Exception e) {
			e.printStackTrace();
			hibernateUtility.saveTransactionlog(transactionlog);
			transactionlog.setDescription("GET IMEI");
			hibernateUtility.saveTransactionlog(transactionlog);
			return StatusCodes.OTHER_ERRORS;
		}
	}

	public void setReceiverParameters(String receiverParams) {

	}

	public int logTransaction() {
		return 0;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub

	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;
	}

}
