package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class CheckMSISDNValidForPromoCommandImpl implements Command {
	private HibernateUtility hibernateUtility;
	private SubscriberDetail subscriberDetail;
	private TransactionLog transactionlog;
	private Logger logger = Logger.getLogger(CheckMSISDNValidForPromoCommandImpl.class);
	private BillingPlanObjectUtility billingPlanObject;

	public int execute() {
		hibernateUtility = 
				(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		logger.info("Execute called on CheckMSISDNValidForPromoCommandImpl for" +
				" subscriber with MSISDN = "+subscriberDetail.getMsisdn());
		transactionlog = new TransactionLog();
		transactionlog.setDate_created(new GregorianCalendar());
		transactionlog.setMsisdn(subscriberDetail.getMsisdn());
		transactionlog.setDescription("Check if MSISDN is valid for promo");
		transactionlog.setShortcode(subscriberDetail.getShortCode());
		try{
			if (hibernateUtility.isMSISDNWhitelisted(subscriberDetail.getMsisdn())){
				transactionlog.setStatus("SUCCESSFUL");
				transactionlog.setId(UUID.randomUUID().toString());
				hibernateUtility.saveTransactionlog(transactionlog);
				return StatusCodes.SUCCESS;
			}
			else{ 
				transactionlog.setId(UUID.randomUUID().toString());
				transactionlog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionlog);
				return StatusCodes.SUBSCRIBER_NOT_ALLOWED;
			}
		}catch(Exception e){
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;
		}
	}
	
	public void setReceiverParameters(String receiverParams) {
		// TODO Auto-generated method stub

	}
	
	public int logTransaction() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;
	}

}
