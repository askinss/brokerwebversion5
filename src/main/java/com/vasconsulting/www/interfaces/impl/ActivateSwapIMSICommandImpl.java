package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.celtel.databundle.service.impl.TelnetServiceManager;
import com.vasconsulting.www.domain.ReconStatus;
import com.vasconsulting.www.domain.ReconcilDetail;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.RIMXMLUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class ActivateSwapIMSICommandImpl implements Command {
	@Autowired
	private TelnetServiceManager telnetConnector;
	@Autowired
	private SubscriberDetail subscriberDetail;
	@Autowired
	private BillingPlanObjectUtility billingPlanObject;
	private TransactionLog transactionLog;
	@Autowired
	private HibernateUtility hibernateUtility;
	private String receiverParams;
	private Logger logger = Logger.getLogger(ActivateSwapIMSICommandImpl.class);
	private LoadAllProperties properties;
	private RIMXMLUtility rimXMLUtility;
	private ReconcilDetail reconcilDetail;
	private ReconStatus reconStatusDb;
	


	public int execute() {
		
		hibernateUtility = (hibernateUtility == null) ?	(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility") : hibernateUtility;
		transactionLog = (transactionLog == null) ? (new TransactionLog()) : transactionLog;	
		reconStatusDb = (reconStatusDb == null) ? (new ReconStatus()) : reconStatusDb;
		transactionLog.setDate_created(new GregorianCalendar());		
		transactionLog.setMsisdn(subscriberDetail.getMsisdn());
		transactionLog.setShortcode(billingPlanObject.getShortCode());	
		transactionLog.setService(subscriberDetail.getServiceplan());
		transactionLog.setDescription("AcitveSimSwap");
		properties = (properties == null) ? (new LoadAllProperties()) : properties;
		rimXMLUtility = (rimXMLUtility == null)? (new RIMXMLUtility()) : rimXMLUtility;
		logger.info("Execute called on ActivateSwapIMSICommandImpl for subscriber with MSISDN = "+subscriberDetail.getMsisdn());
		if (!isContainerIMSISameAsDBIMSI()){
			try{
				activateNewIMSI();
				reconStatusDb.setMsisdn(subscriberDetail.getMsisdn());
				reconStatusDb.setNewimsi(reconcilDetail.getImsi());
				reconStatusDb.setOldimsi(subscriberDetail.getImsi());
				reconStatusDb.setStatus("Active");
				reconStatusDb.setTransdate(new GregorianCalendar());
				hibernateUtility.saveReconStatus(reconStatusDb);
				updateNewImsi();
				
				transactionLog.setStatus("SUCCESSFUL");
			}catch(Exception e){
				e.printStackTrace();
				transactionLog.setStatus("FAILED");
			}
			hibernateUtility.saveTransactionlog(transactionLog);
		}
		return 0;

	}

	
	
/**
	private int updateReconStatus() {
		// TODO Auto-generated method stub
	//if (getReconStatus().getNewimsi().equalsIgnoreCase(getReconSubscriberDBIMSI().getImsi())) 
	
	//{
	//return StatusCodes.SWAPACTIVE;
	//}
	
	//else
		
	//{
       logger.info("updating ReconStatus with new imsi: "+getReconSubscriberDBIMSI().getImsi()+ " msisdn: "+getSubscriberDBIMSI().getMsisdn() + "oldimsi: " +getSubscriberDBIMSI().getImsi());
		
		this.hibernateUtility.updateReconStatus(getReconSubscriberDBIMSI(), getSubscriberDBIMSI());
		
	//}
	return 0;		
		
	}
**/
    
	
	public ReconStatus getReconStatus() {
		 
		reconStatusDb = this.hibernateUtility.getReconStatusInformation(subscriberDetail.getMsisdn());
			
		return reconStatusDb;
			
		}


	public SubscriberDetail getSubscriberDBIMSI(){
		if (hibernateUtility.getSubscriberInformation(subscriberDetail).size() > 0)
			return hibernateUtility.getSubscriberInformation(subscriberDetail).iterator().next();
		else
			return null;
	}
	
	public ReconcilDetail getReconSubscriberDBIMSI(){		
		
		reconcilDetail = this.hibernateUtility.getIMSIFromDBByMsisdn(subscriberDetail.getMsisdn());		
		
		return reconcilDetail;	
		
		}
	

	public boolean isContainerIMSISameAsDBIMSI(){
		if (getReconSubscriberDBIMSI() == null)
			return true;
		else
		{
			logger.info("Reconcil table IMSI is: "+getReconSubscriberDBIMSI().getImsi()+ " and Subscriber DB IMSI is "+getSubscriberDBIMSI().getImsi()+" for msisdn "+subscriberDetail.getMsisdn());
			return (getReconSubscriberDBIMSI().getImsi().equalsIgnoreCase(getSubscriberDBIMSI().getImsi()));
		}
	}

	
	
	public void activateNewIMSI(){
		logger.info("Attempting to Activate New IMSI "+getReconSubscriberDBIMSI().getImsi() + "for msisdn "+getSubscriberDBIMSI().getMsisdn() + "on RIM server");
		
		rimXMLUtility.modifyBillingIdentifier(getSubscriberDBIMSI(), getReconSubscriberDBIMSI());
	}
	
	public void updateNewImsi(){
		logger.info("updating new Imsi"+getReconSubscriberDBIMSI()+ "for msisdn "+getSubscriberDBIMSI().getMsisdn());
		
		this.hibernateUtility.createSwap(getReconSubscriberDBIMSI(), getSubscriberDBIMSI());
	}
	
	
	
	
	public BillingPlanObjectUtility getBillingPlanObject() {
		return this.billingPlanObject;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;
	}

	public HibernateUtility getHibernateUtility() {
		return hibernateUtility;
	}

	public void setHibernateUtility(HibernateUtility hibernateUtility) {
		this.hibernateUtility = hibernateUtility;
	}


	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams.toLowerCase();
	}

	public String getReceiverParameters(){
		return receiverParams;
	}

	public int rollBack()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public LoadAllProperties getProperties() {
		return properties;
	}

	public void setProperties(LoadAllProperties properties) {
		this.properties = properties;
	}

	public String configuredIMSIs() {
		String imsiType = subscriberType() + "IMSIRange";
		logger.info("Trying to get configured range for: "+imsiType);
		return properties.getProperty(imsiType);
	}

	public String subscriberType(){
		return billingPlanObject.getDescription().toLowerCase().split(" ")[0];
	}

	public boolean isIMSIValid(String imsi) {
		boolean resp = false;
		logger.info("Configured imsi range is: "+configuredIMSIs());
		String[] configuredIMSIArray = configuredIMSIs().split(",");
		for (int i = 0; i < configuredIMSIArray.length; i++ ){
			if (imsi.startsWith(configuredIMSIArray[i].trim())){
				resp = true;
				return resp;
			}
		}
		return resp;
	}

	public boolean isIMSIValidPrepaidIMSI(String imsi){
		return false;
	}

	public SubscriberDetail getSubscriberDetail() {
		return this.subscriberDetail;
	}

	public void setSubscriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public TelnetServiceManager getTelnetConnector() {
		return telnetConnector;
	}

	public void setTelnetConnector(TelnetServiceManager telnetConnector) {
		this.telnetConnector = telnetConnector;
	}

	public TransactionLog getTransactionLog() {
		return transactionLog;
	}

	public void setTransactionLog(TransactionLog transactionLog) {
		this.transactionLog = transactionLog;
	}

	public RIMXMLUtility getRimXMLUtility() {
		return rimXMLUtility;
	}

	public void setRimXMLUtility(RIMXMLUtility rimXMLUtility) {
		this.rimXMLUtility = rimXMLUtility;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
}

