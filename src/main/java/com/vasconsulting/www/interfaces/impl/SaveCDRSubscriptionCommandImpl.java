package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.CDRSubscriptionDetail;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.RIMXMLUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class SaveCDRSubscriptionCommandImpl implements Command {
	private SubscriberDetail subscriberDetail;
	private HibernateUtility hibernateUtility;
	private TransactionLog transactionLog = new TransactionLog();
	private String receiverParams;
	private RIMXMLUtility rimUtility;
	Logger logger = Logger.getLogger(SaveCDRSubscriptionCommandImpl.class);
	
	/**
	 * This method will execute and perform service change on TABS based on the supplied
	 * receiver. The method will log at the end of the method execution the state of the 
	 * transaction run.
	 * @author Nnamdi Jibunoh
	 * @param
	 */
	public int execute() {
		/**
		 * Retrieve the Spring managed beans from the container
		 */
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		rimUtility = new RIMXMLUtility();
		
		String [] receiverParamValues = receiverParams.split(":SEP:");
		
		logger.info("Execute called on SaveCDRSubscriptionCommandImpl for subscriber wit MSISDN = "+subscriberDetail.getMsisdn());
			
		try {
			transactionLog.setId(UUID.randomUUID().toString());
			transactionLog.setMsisdn(subscriberDetail.getMsisdn());
			transactionLog.setDate_created(new GregorianCalendar());
			transactionLog.setService("SAVE CDR");
			transactionLog.setShortcode(subscriberDetail.getShortCode());
			
			CDRSubscriptionDetail cdrDetails = new CDRSubscriptionDetail();
			
			cdrDetails.setStatus("NEW");
			cdrDetails.setDate_created(new GregorianCalendar());
			cdrDetails.setServiceName(receiverParamValues[0]);
			cdrDetails.setSubscriberType(new Integer(receiverParamValues[1]));
			cdrDetails.setAmount(new Integer(receiverParamValues[2]));
			cdrDetails.setSource(receiverParamValues[3]);
			cdrDetails.setApnid(receiverParamValues[4]);
			cdrDetails.setMsisdn(subscriberDetail.getMsisdn());
			cdrDetails.setImsi(subscriberDetail.getImsi());
			cdrDetails.setShortCode(subscriberDetail.getShortCode());
			
			int status = hibernateUtility.savePostpaidCDR(cdrDetails);
			
			logger.info("Call to save cdr details for msisdn["+subscriberDetail.getImsi()+"] returned : "+status);
			
			if (status == 0) 
			{
				transactionLog.setStatus("SUCCESSFUL");
				transactionLog.setDescription("CDR details saved correctly");
				return StatusCodes.SUCCESS;
			}
			else //if(!subscriber.getId().equalsIgnoreCase("")) 
			{
				transactionLog.setStatus("FAILURE");
				transactionLog.setDescription("Didnt save CDR details, please correct");
				return StatusCodes.OTHER_ERRORS;
			}			
			
		} catch (Exception e) {
			rimUtility.cancelSubscriptionByIMSI(subscriberDetail);
			e.printStackTrace();
			transactionLog.setStatus("FAILURE");
			transactionLog.setDescription("An error occured saving CDR details, please correct");
			return StatusCodes.OTHER_ERRORS;
		}
		finally
		{
			hibernateUtility.saveTransactionlog(transactionLog);
		}
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		// TODO Auto-generated method stub
		
	}
	

}
