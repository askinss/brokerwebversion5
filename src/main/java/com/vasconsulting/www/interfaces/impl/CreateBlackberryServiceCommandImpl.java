package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.RIMQueryUtil;
import com.vasconsulting.www.utility.RIMXMLResponseDetail;
import com.vasconsulting.www.utility.RIMXMLUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class CreateBlackberryServiceCommandImpl implements Command {
	private RIMXMLUtility rimUtility = new RIMXMLUtility();
	private HibernateUtility hibernateUtility;
	protected SubscriberDetail subscriberDetail;
	private RIMXMLResponseDetail rimStatus;
	protected String receiverParams;
	private BillingPlanObjectUtility billingPlanObject;
	Logger logger = Logger.getLogger(CreateBlackberryServiceCommandImpl.class);

	protected TransactionLog transactionLog;

	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = 
				(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		logger.info("Execute called on CreateBlackberryServiceCommandImpl for subscriber with msisdn "+subscriberDetail.getMsisdn());
		logger.info("Activating RIM services "+receiverParams+", for subscriber = "+subscriberDetail.getMsisdn());

		setUpTransaction();
		if (receiverParams == null || receiverParams == "")
			return StatusCodes.WRONG_SERVICENAME_FORMAT;

		subscriberDetail.setServiceplan(receiverParams);
		try{
			rimStatus = rimUtility.activateRIMService(subscriberDetail);
			if (rimStatus.getErrorCode().equalsIgnoreCase("0") || (new Integer(rimStatus.getErrorCode()) >= 21000 && 
					new Integer(rimStatus.getErrorCode()) <= 21020)) 
			{
				logger.info("RIM Call returned a value of "+rimStatus+". Result ok, proceeding.");
				transactionLog.setStatus("SUCCESSFUL");
			}
			else 
			{
				transactionLog.setStatus("FAILED");
				transactionLog.setDescription("SHORTCODE");
				hibernateUtility.saveTransactionlog(transactionLog);
				transactionLog.setId(UUID.randomUUID().toString());
				transactionLog.setDescription("RIM ACTIVATION");

			}

			hibernateUtility.saveTransactionlog(transactionLog);

			return new Integer(rimStatus.getErrorCode()).intValue();

		}
		catch(Exception e){
			transactionLog.setStatus("FAILED");
			hibernateUtility.saveTransactionlog(transactionLog);
			transactionLog.setId(UUID.randomUUID().toString());
			transactionLog.setDescription("SHORTCODE");
			hibernateUtility.saveTransactionlog(transactionLog);			
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;

		}
	}
	
	protected void setUpTransaction(){
		transactionLog = (transactionLog == null) ? new TransactionLog() : transactionLog;
		transactionLog.setDate_created(new GregorianCalendar());
		transactionLog.setDescription("RIM ACTIVATION");
		transactionLog.setMsisdn(subscriberDetail.getMsisdn());
		transactionLog.setService(receiverParams);
		transactionLog.setShortcode(billingPlanObject.getShortCode());
	}

	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;

	}
	

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public TransactionLog getTransactionLog() {
		return transactionLog;
	}

	public void setTransactionLog(TransactionLog transactionLog) {
		this.transactionLog = transactionLog;
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}
}
