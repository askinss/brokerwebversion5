/**
 * This class is used to check the type of subscriber. If the subscriber is a POSTPAID or PREPAID subscriber
 * It returns 0 if subscriber is postpaid and 500 if prepaid.
 * @author nnamdi Jibunoh
 */
package com.vasconsulting.www.interfaces.impl;

import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.LocalDBIMSILookUp;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.StatusCodes;

public class LocalDBIMSILookUpCommandImpl implements Command {
	
	private SubscriberDetail subscriberDetail;
	private BillingPlanObjectUtility billingPlanObject =  new BillingPlanObjectUtility();
	private SubscriberDetail subscriberDetailLocal;
	private LocalDBIMSILookUp subscriberIMSI;
	private HibernateUtility hibernateUtility;
	private EmailTaskExecutor emailTaskExecutor;
	Logger logger = Logger.getLogger(LocalDBIMSILookUpCommandImpl.class);
		
	/**
	 * This method will execute and perform service change on TABS based on the supplied
	 * receiver. The method will log at the end of the method execution the state of the 
	 * transaction run.
	 * @author Nnamdi Jibunoh
	 * @param
	 */
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		
		logger.info("Execute called on LocalDBIMSILookUpCommandImpl for subscriber with MSISDN = "+subscriberDetail.getMsisdn());
				
		subscriberIMSI = hibernateUtility.getIMSIFromDBByMsisdn(subscriberDetail.getMsisdn());
		
		
		if (subscriberIMSI != null && !subscriberIMSI.getImsi().isEmpty())
		{
			int status = -1;
			subscriberDetail.setImsi(subscriberIMSI.getImsi());
			
			return StatusCodes.SUCCESS;								
			
		}
		else
		{
			logger.info("The IMSI retrieved is not correct, aborting...");
			logger.info("subscriberIMSI object is = "+subscriberIMSI);
			
			return StatusCodes.OTHER_ERRORS;
		}
			
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
		
	public void setReceiverParameters(String receiverParams)
	{
		// TODO Auto-generated method stub
		
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}	
		
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		// TODO Auto-generated method stub
		this.billingPlanObject = billingPlanObject;
	}
}
