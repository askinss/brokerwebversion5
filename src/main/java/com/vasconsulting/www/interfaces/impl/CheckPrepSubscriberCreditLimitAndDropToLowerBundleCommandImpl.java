package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.utility.StatusCodes;

public class CheckPrepSubscriberCreditLimitAndDropToLowerBundleCommandImpl
		extends CheckPrepSubscriberCreditLimitCommandImpl {
	private Logger logger = Logger
			.getLogger(CheckPrepSubscriberCreditLimitAndDropToLowerBundleCommandImpl.class);

	public int execute() {
		int respCode = super.execute();
		System.out.println("Account balance is: " + super.accountBalance);
		if (respCode == StatusCodes.INSUFFICENT_BALANCE_CHECK) {
			for (String rec : receiver.split(",")) {
				String shortCode = rec.split(":")[0];
				String cost = rec.split(":")[1];
				int noOfDays = Integer.valueOf(rec.split(":")[2]);
				float calculatedCost = Float.valueOf(cost)
						* Float.valueOf(receiverParams);
				logger.info("This is the calculatedCost: " + calculatedCost);
				if (super.accountBalance > calculatedCost) {
					billingPlanObject.setShortCode(shortCode);
					billingPlanObject.setCost(cost);
					respCode = StatusCodes.SUCCESS;
					super.subscriberDetail.setShortCode(shortCode);
					super.subscriberDetail
							.setNext_subscription_date(getNextSubscriptionDate(noOfDays));
					        super.subscriberDetail.setServicetype(noOfDays);
					try {
						transactionlog.setId(UUID.randomUUID().toString());
						transactionlog
								.setDescription("DROPPED TO CHEAPER PLAN "
										+ shortCode);
						transactionlog.setStatus("SUCCESSFUL");
						transactionlog.setShortcode(shortCode);
						hibernateUtility.saveTransactionlog(transactionlog);
					} catch (Exception e) {
						e.printStackTrace();
					}
					logger.info("New next_subscription_date is: "
							+ super.subscriberDetail
									.getNext_subscription_date().getTime());
					break;
				}
			}
		}
		return respCode;
	}

	protected GregorianCalendar getNextSubscriptionDate(int noOfDays) {
		GregorianCalendar calendar1 = new GregorianCalendar();
		calendar1.add(GregorianCalendar.DAY_OF_MONTH, noOfDays);
		return calendar1;
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}

}
