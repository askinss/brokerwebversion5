package com.vasconsulting.www.interfaces.impl;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class SetPrepSubscriberDayCommandImpl implements Command
{
	private SubscriberDetail subscriberDetail;
	private HibernateUtility hibernateUtility;
	private String receiverParams;
	Logger logger = Logger.getLogger(SetPrepSubscriberDayCommandImpl.class);
	private BillingPlanObjectUtility billingPlanObject;
	
	public int execute()
	{
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
				
		logger.info("Execute called on SetPrepSubscriberRenewalModeCommandImpl for subscriber with MSISDN = "+subscriberDetail.getMsisdn());
				
		logger.info("Subscriber is subscribing for Airtel Blackberry day service setting next subscription to 11:59 of the next subscription date");
		
		SubscriberDetail subscriberDetailDB = hibernateUtility.getSubscriberInformation("msisdn",subscriberDetail.getMsisdn());
		
		logger.info("Value of subscriber in SetPrepSubscriberDayCommandImpl class is "+subscriberDetailDB);
		
		if (subscriberDetailDB != null)
		{
			if(subscriberDetailDB.getMsisdn().equalsIgnoreCase(subscriberDetail.getMsisdn()) &			
				subscriberDetailDB.getStatus().equalsIgnoreCase("Active"))		
				subscriberDetail.setNext_subscription_date(getNextSubscriptionDate(subscriberDetailDB.getNext_subscription_date()));
			else 
				subscriberDetail.setNext_subscription_date(getTodayDateAndTime());
		}
		else
		{
			logger.info("The subscriber is new, so setting the next subscription date to today midnight");
			subscriberDetail.setNext_subscription_date(null);
			subscriberDetail.setNext_subscription_date(getTodayDateAndTime());
		}			
		
		return StatusCodes.SUCCESS;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}

	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	private GregorianCalendar getTodayDateAndTime(){		
		GregorianCalendar calendar  = new GregorianCalendar();
		calendar.set(GregorianCalendar.HOUR_OF_DAY, 23);
		calendar.set(GregorianCalendar.MINUTE, 58);
		logger.info("The value of next subscription date = "+calendar);
		return calendar;
	}
	
	private Calendar getNextSubscriptionDate(Calendar calendar)
	{
		calendar.add(GregorianCalendar.DAY_OF_MONTH, 1);
		return calendar;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}

}
