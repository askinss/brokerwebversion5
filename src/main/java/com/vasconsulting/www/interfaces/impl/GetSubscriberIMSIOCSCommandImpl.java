package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.provisioning.ibm.Subscriber;
import org.springframework.beans.factory.annotation.Autowired;

import com.celtel.databundle.service.impl.TelnetServiceManager;
import com.huawei.mds.access.webservice.server.bean.ProvisionMethods;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.OCSSubscriberUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class GetSubscriberIMSIOCSCommandImpl implements Command {
	
	@Autowired
	private TelnetServiceManager telnetConnector;
	@Autowired
	protected SubscriberDetail subscriberDetail;
	@Autowired
	private BillingPlanObjectUtility billingPlanObject;
	private TransactionLog transactionLog;
	@Autowired
	private HibernateUtility hibernateUtility;
	protected String receiverParams;
	protected Logger logger = Logger.getLogger(GetSubscriberIMSIOCSCommandImpl.class);
	@Autowired
	private LoadAllProperties properties = new LoadAllProperties();
	private OCSSubscriberUtility ocsConnector =  new OCSSubscriberUtility();
	

	@SuppressWarnings("unchecked")
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = (hibernateUtility == null) ?
				(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility") : hibernateUtility;
		transactionLog = (transactionLog == null) ? (new TransactionLog()) : transactionLog;
		
		transactionLog.setDate_created(new GregorianCalendar());		
		transactionLog.setMsisdn(subscriberDetail.getMsisdn());
		transactionLog.setShortcode(billingPlanObject.getShortCode());	
		transactionLog.setDescription("GET IMSI");
		transactionLog.setStatus("FAILED");
		telnetConnector = (telnetConnector == null) ? (new TelnetServiceManager()) : telnetConnector;
		int response = StatusCodes.OTHER_ERRORS;

		logger.info("Execute called on GetSubscriberIMSIOCSCommandImpl for subscriber with msisdn "+subscriberDetail.getMsisdn());

		try 
		{
			//logger.info("The other deduct = "+new OCSSubscriberUtility().deductServiceAirtime("752604874", -1, "Test"));
			//logger.info("here");
			//logger.info("The other deduct = "+new OCSSubscriberUtility().getSubscriberHLRDetails(subscriberDetail.getMsisdn()));
			
			String[] responseAirtel = ocsConnector.getAirtelSubscriberIMSIAndType(subscriberDetail.getMsisdn());
			
			if (responseAirtel == null)
			{
				//this number is a postpaid, bail out
				transactionLog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionLog);
				transactionLog.setId(UUID.randomUUID().toString());
				transactionLog.setDescription("OCS HLR FAILURE");
				hibernateUtility.saveTransactionlog(transactionLog);
				return StatusCodes.OTHER_ERRORS;
			}
			else if (responseAirtel[1].equalsIgnoreCase("POST"))
			{
				//this number is a postpaid, bail out
				transactionLog = new TransactionLog();
				transactionLog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionLog);
				transactionLog.setId(UUID.randomUUID().toString());
				transactionLog.setDescription("SUBSCRIBER NOT PREPAID");
				hibernateUtility.saveTransactionlog(transactionLog);
				return StatusCodes.POSTPAID_SUBSCRIBER;
			}
			else
			{
				//this number is a prepaid number proceed
				subscriberDetail.setImsi(responseAirtel[0]);
				return StatusCodes.SUCCESS;
			}				 
				
		}
		catch (Exception e) {
			transactionLog.setStatus("FAILED");
			e.printStackTrace();
			
		}finally {
			transactionLog.setId(UUID.randomUUID().toString());
			hibernateUtility.saveTransactionlog(transactionLog);
		}
		return response;
	}

	public BillingPlanObjectUtility getBillingPlanObject() {
		return (billingPlanObject == null) ? (BillingPlanObjectUtility)ContextLoaderImpl.getBeans("billingPlanObject") : billingPlanObject;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;
	}

	public HibernateUtility getHibernateUtility() {
		return hibernateUtility;
	}

	public void setHibernateUtility(HibernateUtility hibernateUtility) {
		this.hibernateUtility = hibernateUtility;
	}

	private String stripLeadingMsisdnPlus(String msisdn){
		String Msisdn = msisdn;
		System.out.println("Configured OPCO Country Code is: "+properties.getProperty("opcoCountryCode"));
		if (msisdn.startsWith("0")){
			return properties.getProperty("opcoCountryCode")+Msisdn.substring(1, Msisdn.length());
		}
		else if(Msisdn.startsWith("+"+properties.getProperty("opcoCountryCode"))){
			return Msisdn.substring(1, Msisdn.length());
		}
		else return Msisdn;
	}

	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams.toLowerCase();
	}

	public String getReceiverParameters(){
		return receiverParams;
	}

	public int rollBack()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public LoadAllProperties getProperties() {
		return properties;
	}

	public void setProperties(LoadAllProperties properties) {
		this.properties = properties;
	}

	public String configuredIMSIs() {
		String imsiType = subscriberType() + "IMSIRange";
		logger.info("Trying to get configured range for: "+imsiType);
		return properties.getProperty(imsiType);
	}
	
	protected String subscriberType(){
		if (!((receiverParams == null) || receiverParams.isEmpty())){
			return receiverParams.toLowerCase();
		} else return billingPlanObject.getDescription().toLowerCase().split(" ")[0];
	}

	private String receiverParameters() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isIMSIValid(String imsi) {
		boolean resp = false;
		logger.info("Configured imsi range is: "+configuredIMSIs());
		String[] configuredIMSIArray = configuredIMSIs().split(",");
		for (int i = 0; i < configuredIMSIArray.length; i++ ){
			if (imsi.startsWith(configuredIMSIArray[i].trim())){
				resp = true;
				return resp;
			}
		}
		return resp;
	}

	public boolean isIMSIValidPrepaidIMSI(String imsi){
		return false;
	}

	public SubscriberDetail getSubscriberDetail() {
		return (subscriberDetail == null) ? (SubscriberDetail)ContextLoaderImpl.getBeans("subscriberDetail") : this.subscriberDetail;
	}

	public void setSubscriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public TelnetServiceManager getTelnetConnector() {
		return telnetConnector;
	}

	public void setTelnetConnector(TelnetServiceManager telnetConnector) {
		this.telnetConnector = telnetConnector;
	}

	public TransactionLog getTransactionLog() {
		return transactionLog;
	}

	public void setTransactionLog(TransactionLog transactionLog) {
		this.transactionLog = transactionLog;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
}
