package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.Ttspromo;

public class UpdateSubscriberTTSPromoProfileCommandImpl implements Command  {
	private HibernateUtility hibernateUtility;
	private Ttspromo ttsPromo;
	private Logger logger = Logger.getLogger(UpdateSubscriberTTSPromoProfileCommandImpl.class);
	private SubscriberDetail subscriberDetail;

	
	public int execute() {
		hibernateUtility = 
				(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		logger.info("Execute called on CreateSubscriberTTSPromoProfileCommandImpl for" +
				" subscriber with MSISDN = "+subscriberDetail.getMsisdn());
		try{
			ttsPromo = hibernateUtility.getTTSPromoDetails(subscriberDetail.getMsisdn());
			ttsPromo.setImei(subscriberDetail.getImei());
			ttsPromo.setMsisdn(subscriberDetail.getMsisdn());
			ttsPromo.setUsage_count(ttsPromo.getUsage_count() + 1);
			ttsPromo.setLast_subscription_date(new GregorianCalendar());
			ttsPromo.setNext_subscription_date(subscriberDetail.getNext_subscription_date());
			hibernateUtility.saveTTSPromoDetails(ttsPromo);
			return StatusCodes.SUCCESS;
		}catch(Exception e){
			return StatusCodes.OTHER_ERRORS;

		}

	}

	
	public void setReceiverParameters(String receiverParams) {
		// TODO Auto-generated method stub

	}

	
	public int logTransaction() {
		// TODO Auto-generated method stub
		return 0;
	}


	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}


	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		// TODO Auto-generated method stub
		
	}

}
