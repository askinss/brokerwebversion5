package com.vasconsulting.www.interfaces.impl;

import java.rmi.RemoteException;
import java.util.GregorianCalendar;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class CheckPostSubscriberStatusCommandImpl implements Command {
	private SubscriberDetail subscriberDetail;
	private HibernateUtility hibernateUtility;
	private TransactionLog transactionLog = new TransactionLog();
	Logger logger = Logger.getLogger(CheckPostSubscriberStatusCommandImpl.class);
	private BillingPlanObjectUtility billingPlanObject;
	
	/**
	 * This method will execute and perform service change on TABS based on the supplied
	 * receiver. The method will log at the end of the method execution the state of the 
	 * transaction run.
	 * @author Nnamdi Jibunoh
	 * @param
	 */
	public int execute() {
		/**
		 * Retrieve the Spring managed beans from the container
		 */
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		
		logger.info("Execute called on CheckPostSubscriberStatusCommandImpl for subscriber wit MSISDN = "+subscriberDetail.getMsisdn());
			
		try {
			transactionLog.setId(UUID.randomUUID().toString());
			transactionLog.setMsisdn(subscriberDetail.getMsisdn());
			transactionLog.setDate_created(new GregorianCalendar());
			transactionLog.setService("CHECK DB STATUS");
			transactionLog.setShortcode(subscriberDetail.getShortCode());
			
			SubscriberDetail subscriber = hibernateUtility.getSubscriberInformation("msisdn", subscriberDetail.getMsisdn());
			
			logger.info("Call to check Subscriber Status database returned result : "+subscriber);
			
			if (subscriber == null || subscriber.getId().equalsIgnoreCase(""))
			{
				logger.info("About to return the code "+StatusCodes.SUCCESS+", for subscriber "+subscriberDetail.getMsisdn());
				transactionLog.setStatus("SUCCESSFUL");
				transactionLog.setDescription("Subcriber not found in Broker, proceeding");
				return StatusCodes.SUCCESS;
			}
			else //if(!subscriber.getId().equalsIgnoreCase("")) 
			{
				if (subscriber.getStatus().equalsIgnoreCase("Active"))
				{					
					logger.info("About to return the code "+StatusCodes.SUBSCRIBER_CANNOT_MIGRATE_WHILE_ACTIVE+", for subscriber "+
							subscriberDetail.getMsisdn());
					transactionLog.setStatus("FAILED");
					transactionLog.setDescription("Subcriber found active on another service, aborting");
					return StatusCodes.SUBSCRIBER_CANNOT_MIGRATE_WHILE_ACTIVE;														
				}
				else 
				{
					transactionLog.setStatus("SUCCESSFUL");
					transactionLog.setDescription("Subcriber found in Broker deactivated, proceeding");
					return StatusCodes.SUCCESS;
				}
			}			
			
		} catch (Exception e) {
			e.printStackTrace();
			transactionLog.setStatus("FAILED");
			transactionLog.setDescription("An error occured while checking Broker");
			return StatusCodes.OTHER_ERRORS;
		}
		finally
		{
			hibernateUtility.saveTransactionlog(transactionLog);
		}
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	public void setReceiverParameters(String receiverParams)
	{
		// TODO Auto-generated method stub
		
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;
	}
	

}
