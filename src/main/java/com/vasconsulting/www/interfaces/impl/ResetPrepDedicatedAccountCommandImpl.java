package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.Map;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.UCIPServiceRequestManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.HibernateUtilityImpl;
import com.vasconsulting.www.utility.StatusCodes;

public class ResetPrepDedicatedAccountCommandImpl implements Command {
	private UCIPServiceRequestManager ucipConnector = new UCIPServiceRequestManager();
	private String receiverParams;
	private SubscriberDetail subscriberDetail;
	private HibernateUtility hibernateUtility;
	Map<String, String> ucipReturns;
	private BillingPlanObjectUtility billingPlanObject;
	private TransactionLog transactionLog;
	Logger logger = Logger.getLogger(ResetPrepDedicatedAccountCommandImpl.class);
	
	
	@SuppressWarnings("unchecked")
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		logger.info("Execute has been called on ResetPrepDedicatedAccountCommandImpl for subscriber subscriber with msisdn "+subscriberDetail.getMsisdn());
		
				
		try {
			ucipReturns = ucipConnector.updateSubscriberDedicatedAccount(subscriberDetail.getMsisdn(),"DA_RESET");
			//getSubscriberBalance(subscriberDetail.getMsisdn());
									
			logger.info("Response from AIR is "+ucipReturns);
			
			if (ucipReturns.get("responseCode").equalsIgnoreCase("0")){
				transactionLog = new TransactionLog();
			
				transactionLog.setDate_created(new GregorianCalendar());
				transactionLog.setDescription("Dedicated Account SETUP");
				transactionLog.setMsisdn(subscriberDetail.getMsisdn());
				transactionLog.setService("Set value is 0");
				transactionLog.setShortcode(billingPlanObject.getShortCode());
				transactionLog.setStatus("Completed");
				
				hibernateUtility.saveTransactionlog(transactionLog);
			}
			else {
				return new Integer(ucipReturns.get("responseCode")).intValue();
			}
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return StatusCodes.WRONG_DAVALUE_FORMAT;
		} catch (Exception e) {
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS_IN;
		}
		return StatusCodes.SUCCESS;
	}
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}
	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}
}
