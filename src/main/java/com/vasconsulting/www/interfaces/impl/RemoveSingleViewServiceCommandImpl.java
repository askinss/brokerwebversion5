package com.vasconsulting.www.interfaces.impl;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.interfaces.SingleInterface;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.interfaces.SingleInterface;
import com.vasconsulting.www.utility.StatusCodes;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.GregorianCalendar;
import java.util.UUID;

/**
 * Created by osleonard on 4/10/15.
 */
public class RemoveSingleViewServiceCommandImpl implements Command {

    private SubscriberDetail subscriberDetail;
    private BillingPlanObjectUtility billingPlanObject;
    private SingleInterface single;
    private TransactionLog transactionlog;
    private HibernateUtility hibernateUtility;
    private Logger logger = Logger.getLogger(RemoveSingleViewServiceCommandImpl.class);

    @Autowired
    private String receiverParams;

    public int execute() {

        billingPlanObject = (billingPlanObject == null) ?
                (BillingPlanObjectUtility) ContextLoaderImpl.getBeans("billingPlanObject") : billingPlanObject;
        hibernateUtility = (HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
        transactionlog = (transactionlog == null) ? (new TransactionLog()) : transactionlog;
        single = (SingleInterface)ContextLoaderImpl.getBeans("single");

        logger.info("Execute called on RemoveSingleViewServiceCommandImpl  for subscriber with MSISDN = " + subscriberDetail.getMsisdn());

        String equipID = receiverParams;
        transactionlog = new TransactionLog();
        transactionlog.setDate_created(new GregorianCalendar());
        transactionlog.setDescription("Remove SingleView Service");
        transactionlog.setShortcode(subscriberDetail.getShortCode());

        String responseCode = "";

        if(equipID == null || equipID == ""){
            return StatusCodes.SINGLEVIEW_NO_EQUIP_ID_CONFIGURED;
        }
        else{

            logger.info("Configure EquipID is " + equipID + "MSISDN is " + subscriberDetail.getMsisdn());

            try{
                responseCode = single.deleteService(stripLeadingMsisdnPrefix(subscriberDetail.getMsisdn()), equipID);
                logger.info("Response from SingleView is: "+ responseCode);
                if((responseCode != null && responseCode.equalsIgnoreCase("SUCCESS"))){
                    transactionlog.setDescription("SUCCESSFUL");
                    hibernateUtility.saveTransactionlog(transactionlog);
                    logger.info("RemoveSingleViewServiceImpl for subscriber "+subscriberDetail.getMsisdn()+" returned with code "+responseCode);
                    return StatusCodes.SUCCESS;
                }
                else {
                    transactionlog.setStatus("FAILED");
                    hibernateUtility.saveTransactionlog(transactionlog);
                    transactionlog.setId(UUID.randomUUID().toString());
                    transactionlog.setDescription("EQUIPID");
                    hibernateUtility.saveTransactionlog(transactionlog);
                    return StatusCodes.SINGLEVIEW_OTHER_ERRORS_CHANGE_SERVICE;

                }

            }
            catch (Exception e){
                transactionlog.setStatus("FAILED");
                hibernateUtility.saveTransactionlog(transactionlog);
                transactionlog.setId(UUID.randomUUID().toString());
                transactionlog.setDescription("EQUIPID");
                hibernateUtility.saveTransactionlog(transactionlog);
                e.printStackTrace();
                return StatusCodes.SINGLEVIEW_OTHER_ERRORS_CHANGE_SERVICE;

            }

        }

    }

    public void setReceiverParameters(String receiverParams) {

        this.receiverParams = receiverParams;

    }

    public void setReceiver(String receiver) {

    }

    public int logTransaction() {
        return 0;
    }

    public SubscriberDetail getSubscriberDetail(){
        return subscriberDetail;
    }

    public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
        this.subscriberDetail = subscriberDetail ;

    }

    public BillingPlanObjectUtility getBillingPlanObject(){
        return billingPlanObject;
    }


    public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {

        this.billingPlanObject = billingPlanObject;

    }

    private String stripLeadingMsisdnPrefix(String msisdn){
        String Msisdn = msisdn;
        if (msisdn.startsWith("0")){
            return Msisdn.substring(1, Msisdn.length());
        }
        else if (Msisdn.startsWith("260")){
            return Msisdn.substring(3, Msisdn.length());
        }
        else if(Msisdn.startsWith("+260")){
            return Msisdn.substring(4, Msisdn.length());
        }
        else return Msisdn;
    }
}
