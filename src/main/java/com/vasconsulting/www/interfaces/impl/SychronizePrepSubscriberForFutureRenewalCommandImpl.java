package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.FutureRenewal;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class SychronizePrepSubscriberForFutureRenewalCommandImpl implements
		Command {
	private SubscriberDetail subscriberDetail;
	private BillingPlanObjectUtility billingPlanObject;
	private TransactionLog transactionLog;
	private HibernateUtility hibernateUtility;
	private Logger logger = Logger
			.getLogger(SychronizePrepSubscriberForFutureRenewalCommandImpl.class);
	private FutureRenewal futureRenewal;

	public int execute() {
		subscriberDetail = (subscriberDetail == null) ? (SubscriberDetail) ContextLoaderImpl
				.getBeans("subscriberDetail") : subscriberDetail;
		billingPlanObject = (billingPlanObject == null) ? (BillingPlanObjectUtility) ContextLoaderImpl
				.getBeans("billingPlanObject") : billingPlanObject;
		hibernateUtility = (hibernateUtility == null) ? (HibernateUtility) ContextLoaderImpl
				.getBeans("hibernateUtility") : hibernateUtility;
		transactionLog = (transactionLog == null) ? new TransactionLog()
				: transactionLog;
		futureRenewal = (futureRenewal == null) ? (FutureRenewal) ContextLoaderImpl
				.getBeans("futureRenewal") : futureRenewal;
		transactionLog.setDate_created(new GregorianCalendar());
		transactionLog.setMsisdn(subscriberDetail.getMsisdn());
		transactionLog.setService(subscriberDetail.getServiceplan());
		transactionLog.setShortcode(billingPlanObject.getShortCode());
		futureRenewal.setMsisdn(subscriberDetail.getMsisdn());
		futureRenewal.setShortCode(subscriberDetail.getShortCode());
		futureRenewal.setPurchasedAt(new GregorianCalendar());
		futureRenewal.setStatus("Available");
		logger.info("SychronizePrepSubscriberForFutureRenewalCommandImpl called for subscriber with MSISDN: "
				+ subscriberDetail.getMsisdn());
		if (hibernateUtility.saveFutureRenewal(futureRenewal) == 0) {
			logger.info("Successfully saved subscriber with msisdn "+ futureRenewal.getMsisdn() + " for advanced subscription of "+futureRenewal.getShortCode());
			return StatusCodes.SUCCESS;
		} else
			return StatusCodes.OTHER_ERRORS;
	}

	public void setReceiverParameters(String receiverParams) {
		// TODO Auto-generated method stub

	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub

	}

	public int logTransaction() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setSusbcriberDetails(SubscriberDetail subscriberDetail) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}

}
