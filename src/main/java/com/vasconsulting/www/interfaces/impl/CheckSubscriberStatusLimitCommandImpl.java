/**
 * This class is used to check the type of subscriber. If the subscriber is a POSTPAID or PREPAID subscriber
 * It returns 0 if subscriber is postpaid and 500 if prepaid.
 * @author nnamdi Jibunoh
 */
package com.vasconsulting.www.interfaces.impl;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.UCIPServiceRequestManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class CheckSubscriberStatusLimitCommandImpl implements Command {
	
	private SubscriberDetail subscriberDetail;
	private HibernateUtility hibernateUtility;
	private UCIPServiceRequestManager ucipConnector = new UCIPServiceRequestManager();
	private Logger logger = Logger.getLogger(CheckSubscriberStatusLimitCommandImpl.class);
	private String subscriberCreditLimit;
	private float accountBalance;
	private String receiverParams;
	private BillingPlanObjectUtility billingPlanObject;
		
	/**
	 * This method will execute and perform service change on TABS based on the supplied
	 * receiver. The method will log at the end of the method execution the state of the 
	 * transaction run.
	 * @author Nnamdi Jibunoh
	 * @param
	 */
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
				
		logger.info("Execute called on CheckSubscriberCreditLimitCommandImpl for subscriber with MSISDN = "+subscriberDetail.getMsisdn());
				
		subscriberCreditLimit = hibernateUtility.getSubscriberTABSCreditLimit(stripLeadingMsisdnPrefix(subscriberDetail.getMsisdn()));
		
		logger.info("Subscriber with MSISDN "+subscriberDetail.getMsisdn()+", has a credit limit of "+subscriberCreditLimit);
		
		if (subscriberCreditLimit == null)
			return StatusCodes.NO_CREDIT_LIMIT;
		else{
			try {
				
				@SuppressWarnings("unchecked")
				Map<String, String> responseMap = ucipConnector.getSubscriberBalance(subscriberDetail.getMsisdn());
				logger.info("Result from the AIR Servers is"+responseMap);
				
				if (responseMap == null)
					return StatusCodes.ERROR_CHECKING_SUB_BALANCE;
				else
				{
					accountBalance = new Float(responseMap.get("accountValue1")).floatValue();
					
					accountBalance = accountBalance / 100;//This is to convert the account balance to Naria instead of Kobo
					
					float subscriberCreditLimitValue = new Float(subscriberCreditLimit).floatValue();
					
					/**
					 * Get the amount to deduct from the subscriber based on the time of the month it is.
					 */
					double amountToDeduct = new Double(receiverParams).doubleValue();
					
					double daysToEndOfMonth = daysBetween(new GregorianCalendar(), getLastDateOfMonth());
					int numberOfDayInMonth = new GregorianCalendar().getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
					
					logger.info("The configured amount parameter for this plan is = "+this.receiverParams);
					logger.info("The number of days to end of month is ="+daysToEndOfMonth);
					logger.info("This month has a total number of "+numberOfDayInMonth+" days");
					
					daysToEndOfMonth = daysToEndOfMonth / numberOfDayInMonth;			
					amountToDeduct = Math.round(amountToDeduct * daysToEndOfMonth);
					/**
					 * End of get amount to deduct.
					 */
					
					logger.info("The cost of this service for subscriber "+subscriberDetail.getMsisdn()+" would be "+amountToDeduct);
					
					if ((subscriberCreditLimitValue + accountBalance) >= amountToDeduct)
					{
						
						logger.info("Subscriber "+subscriberDetail.getMsisdn()+" balance now is "+accountBalance
								+", new balance if deduction occurs would be less by"+
								amountToDeduct);
						return StatusCodes.SUCCESS;
					}
					else
					{
						return StatusCodes.INSUFFICENT_BALANCE;
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return StatusCodes.ERROR_CHECKING_SUB_BALANCE;
			}
		}
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
		
	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
		
	}
	
	private int daysBetween(GregorianCalendar startDate, GregorianCalendar endDate) {  
		GregorianCalendar date = (GregorianCalendar) startDate.clone();  
		int daysBetween = 0;  
		while (date.before(endDate)) {  
			date.add(GregorianCalendar.DAY_OF_MONTH, 1);  
			daysBetween++;  
		}  
		return daysBetween;  
	}
	
	private GregorianCalendar getLastDateOfMonth(){
		GregorianCalendar calendar  = new GregorianCalendar();
		int dayOfMonth  = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
		
		int daysToAdd = dayOfMonth - calendar.get(GregorianCalendar.DAY_OF_MONTH);
		
		calendar.add(GregorianCalendar.DAY_OF_MONTH, daysToAdd);	
		return calendar;
	}
	
	private String stripLeadingMsisdnPrefix(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("0")){
			return Msisdn.substring(1, Msisdn.length());
		}
		else if (Msisdn.startsWith("234")){
			return Msisdn.substring(3, Msisdn.length());
		}
		else if(Msisdn.startsWith("+234")){
			return Msisdn.substring(4, Msisdn.length());
		}
		else return Msisdn;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}

}
