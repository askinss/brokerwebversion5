package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.interfaces.RenewalCommand;
import com.vasconsulting.www.interfaces.TabsInterface;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class RenewSubscriberTABSServiceCommandImpl implements RenewalCommand
{
	private SubscriberDetail subscriberDetail;
	private String receiverParams;
	Logger logger = Logger.getLogger(RenewSubscriberTABSServiceCommandImpl.class);
	private HibernateUtility hibernateUtility;
	private TransactionLog transactionlog = new TransactionLog();
	private TabsInterface tabs;
	
	public int execute()
	{
		logger.info("Execute called on RenewSubscriberTABSServiceCommandImpl for subscriber with MSISDN = "+subscriberDetail.getMsisdn());
		
		hibernateUtility = 
				(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		tabs = (TabsInterface)ContextLoaderImpl.getBeans("tabs");
		
		try {
			String responseCode = tabs.changeService(subscriberDetail.getMsisdn(),receiverParams);
			
			if ((responseCode != null && responseCode.equalsIgnoreCase("0")) || 
					(responseCode != null && responseCode.equalsIgnoreCase("3172")))
			{
				transactionlog.setDate_created(new GregorianCalendar());
				transactionlog.setMsisdn(subscriberDetail.getMsisdn());
				transactionlog.setDescription("Renew TABS Service: By Renewal Module");
				transactionlog.setService(receiverParams);
				transactionlog.setStatus("Successful");
				
				hibernateUtility.saveTransactionlog(transactionlog);
				
				logger.info("RenewSubscriberTABSServiceCommandImpl for subscriber "+subscriberDetail.getMsisdn()+" returned with code"+responseCode);
				
				return StatusCodes.SUCCESS;
			}
			else return StatusCodes.OTHER_ERRORS_CHANGE_SERVICE;
			
		} catch (Exception e) {
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS_CHANGE_SERVICE;
		}
		
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}

	public int logTransaction()
	{
		return 0;
	}

	public void setSubscriber(SubscriberDetail subscriber)
	{
		this.subscriberDetail = subscriber;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		// TODO Auto-generated method stub
		
	}

}
