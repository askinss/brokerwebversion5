package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.TelnetServiceManager;
import com.celtel.databundle.service.impl.UCIPServiceRequestManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.interfaces.RenewalCommand;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.RIMXMLResponseDetail;
import com.vasconsulting.www.utility.RIMXMLUtility;
import com.vasconsulting.www.utility.SendSmsToKannelService;
import com.vasconsulting.www.utility.StatusCodes;

public class HourlyPrepaidBBRenewalCommandImpl implements RenewalCommand {
	private String receiverParams, errorMessage;
	private Logger logger = Logger
			.getLogger(HourlyPrepaidBBRenewalCommandImpl.class);
	private SubscriberDetail subscriberDetail;
	private UCIPServiceRequestManager ucipConnector = new UCIPServiceRequestManager();
	// private RIMQueryUtil rimUtility = new RIMQueryUtil();
	private RIMXMLUtility rimUtility = new RIMXMLUtility();
	private RIMXMLResponseDetail rimStatus;
	private HibernateUtility hibernateUtility = (HibernateUtility) ContextLoaderImpl
			.getBeans("hibernateUtility");
	private EmailTaskExecutor emailExecutor = (EmailTaskExecutor) ContextLoaderImpl
			.getBeans("myEmailTaskExecutor");
	private SendSmsToKannelService smsService = (SendSmsToKannelService) ContextLoaderImpl
			.getBeans("smsService");
	private LoadAllProperties properties = (LoadAllProperties) ContextLoaderImpl
			.getBeans("loadProperties");
	private TelnetServiceManager telnetManager = new TelnetServiceManager();
	private TransactionLog transaction;

	@SuppressWarnings("unchecked")
	public int execute() {
		int autoRenew = subscriberDetail.getAutoRenew();
		int status = -1;
		float accountBalance;
		transaction = new TransactionLog();
		try{
		if (autoRenew == 0) {
			// deactivate sub
			subscriberDetail.setStatus("Deactivated");
			// status = rimUtility.cancelSubscription(subscriberDetail);
			try {
				rimStatus = rimUtility
						.cancelSubscriptionByIMSI(subscriberDetail);
			} catch (Exception e) {
				e.printStackTrace();
				return StatusCodes.OTHER_ERRORS;
			}

			if (rimStatus.getErrorCode().equalsIgnoreCase("0")
					|| ((new Integer(rimStatus.getErrorCode()) >= 21000 && (new Integer(
							rimStatus.getErrorCode()) <= 21500)))
					|| (new Integer(rimStatus.getErrorCode()) <= 61040)) {
				try {
					telnetManager.removeBlackberryAPNToSubscriber(
							subscriberDetail.getMsisdn(),
							properties.getProperty("APNID.Blackberry"));
				} catch (Exception e) {
					errorMessage = subscriberDetail.getMsisdn()
							+ " was deactivated on RIM because of stop auto renew, but "
							+ "the APN was not removed successfully for "
							+ subscriberDetail.getMsisdn()
							+ ". Please rectify manually.";
					emailExecutor.sendQueuedEmails(errorMessage);

					e.printStackTrace();
				}
				status = hibernateUtility
						.updateSubscriberDetail(subscriberDetail);
				if (status == 0) {
					transaction.setDate_created(new GregorianCalendar());
					transaction.setDescription("DEACTIVATION");
					transaction.setService(subscriberDetail.getServiceplan());
					transaction.setMsisdn(subscriberDetail.getMsisdn());
					transaction.setShortcode("SCHEDULER:NO_AUTO_RENEW");
					hibernateUtility.saveTransactionlog(transaction);
				} else {
					errorMessage = "Subscriber data did not update for subscriber with MSISDN "
							+ subscriberDetail.getMsisdn()
							+ " in the hourly renewal job. Please rectify the subscriber data manually.";
					logger.error("Error: Subscriber data did not update in database."
							+ subscriberDetail.getMsisdn());
					emailExecutor.sendQueuedEmails(errorMessage);
					return StatusCodes.SUCCESS;
				}
			} else {
				errorMessage = "Subscriber deactivation was not successfully on RIM for subscriber with MSISDN "
						+ subscriberDetail.getMsisdn()
						+ " in the hourly renewal job. The subscriber has auto renewal disabled. Please rectify the subscriber data manually.";
				logger.error("Error: Subscriber data did not update in database."
						+ subscriberDetail.getMsisdn());
				emailExecutor.sendQueuedEmails(errorMessage);
				return StatusCodes.SUCCESS;
			}
		} else {
			if (receiverParams.equalsIgnoreCase("") || receiverParams == null) {
				errorMessage = "The configuration file for the renewal module is either corrupt or not properly set. Please consult the documentation "
						+ "and then set the details for Prepaid Blackberry renewal.["
						+ subscriberDetail.getMsisdn() + "]";
				logger.error("Error: Error renewal setting for application not set");
				emailExecutor.sendQueuedEmails(errorMessage);
				return StatusCodes.OTHER_ERRORS;
			}

			// amt:sep:rim details - Seperate rim details with : in the details
			// if neccessary
			String provParams[] = receiverParams.split(":sep:");

			Map<String, String> responseMap = new HashMap<String, String>();
			try {
				responseMap = ucipConnector
						.getSubscriberBalance(subscriberDetail.getMsisdn());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			logger.info("Result from the AIR Servers is" + responseMap);

			if (responseMap == null
					|| !(responseMap.get("responseCode").equalsIgnoreCase("0")) || 
					responseMap.get("responseCode").equalsIgnoreCase("102") || 
					responseMap.get("responseCode").equalsIgnoreCase("126")) {
				try {
					rimStatus = rimUtility
							.cancelSubscriptionByIMSI(subscriberDetail);
				} catch (Exception e) {
					e.printStackTrace();
					return StatusCodes.OTHER_ERRORS;
				}

				if (rimStatus.getErrorCode().equalsIgnoreCase("0")
						|| ((new Integer(rimStatus.getErrorCode()) >= 21000 && (new Integer(
								rimStatus.getErrorCode()) <= 21500)))
						|| (new Integer(rimStatus.getErrorCode()) <= 61040)) {
					try {
						logger.info("About to pull APN");
						logger.info("EMA response is = "
								+ telnetManager.removeBlackberryAPNToSubscriber(
										subscriberDetail.getMsisdn(),
										properties
												.getProperty("APNID.Blackberry")));
					} catch (Exception e) {
						errorMessage = subscriberDetail.getMsisdn()
								+ " was deactivated on RIM because of insufficent balance, but "
								+ "the APN was not removed successfully for "
								+ subscriberDetail.getMsisdn()
								+ ". Please rectify manually.";
						emailExecutor.sendQueuedEmails(errorMessage);

						e.printStackTrace();
						return StatusCodes.OTHER_ERRORS;
					}

					subscriberDetail.setStatus("Deactivated");
					hibernateUtility.updateSubscriberDetail(subscriberDetail);

					logger.info("Sending Insufficent renewal balance message to "
							+ subscriberDetail.getMsisdn());
					smsService
							.sendMessageToKannel(
									properties
											.getProperty("insufficentbalanceforrenewalmessage"),
									subscriberDetail.getMsisdn());
				}
				return StatusCodes.ERROR_CHECKING_SUB_BALANCE;
			} else {
				accountBalance = Float
						.valueOf(responseMap.get("accountValue1"));

				//Double amountToDeduct = Double.valueOf(provParams[0].trim());
				Double amountToDeduct = (new Double(provParams[0].trim()).doubleValue()) * 10000;
				logger.info("Subscriber [" + subscriberDetail.getMsisdn()
						+ "] current account balance is =" + accountBalance);

				logger.info("The cost of this service for subscriber "
						+ subscriberDetail.getMsisdn() + " would be "
						+ amountToDeduct);

				if (accountBalance >= amountToDeduct) {
					logger.info("Subscriber "
							+ subscriberDetail.getMsisdn()
							+ " balance now is "
							+ accountBalance
							+ ", new balance if deduction occurs would be less by "
							+ amountToDeduct);
					try {
						rimStatus = rimUtility
								.activateRIMService(subscriberDetail);
					} catch (Exception e) {
						e.printStackTrace();
						return StatusCodes.OTHER_ERRORS;
					}

					if (rimStatus.getErrorCode().equalsIgnoreCase("0")
							|| ((new Integer(rimStatus.getErrorCode()) >= 21000 && (new Integer(
									rimStatus.getErrorCode()) <= 21500)))) {
						try {
							amountToDeduct = amountToDeduct * -1;
							responseMap = ucipConnector
									.updateSubscriberBalance(
											subscriberDetail.getMsisdn(),
											new Integer((amountToDeduct)
													.intValue()).toString(),
											subscriberDetail.getServiceplan()
													.replaceAll(" ", "")
													+ "_Renew"
													+ subscriberDetail
															.getServicetype());
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							return StatusCodes.OTHER_ERRORS;
						}

						if (responseMap.get("responseCode").equalsIgnoreCase(
								"0")) {
							GregorianCalendar calendar = new GregorianCalendar();

							subscriberDetail
									.setLast_subscription_date(calendar);
							subscriberDetail
									.setNext_subscription_date(getNextSubscriptionDate(subscriberDetail
											.getServicetype()));
							subscriberDetail.setStatus("Active");

							status = hibernateUtility
									.updateSubscriberDetail(subscriberDetail);

							if (status == 0) {
								transaction = new TransactionLog();
								transaction.setDate_created(calendar);
								transaction.setDescription("RENEWAL");
								transaction.setMsisdn(subscriberDetail
										.getMsisdn());
								transaction.setShortcode("SCHEDULER");
								hibernateUtility
										.saveTransactionlog(transaction);

								return StatusCodes.SUCCESS;
							} else {
								errorMessage = "Subscriber data in DB for subscriber with MSISDN "
										+ subscriberDetail.getMsisdn()
										+ " was not successful in the hourly renewal job. All other requirements are satisfied. "
										+ "Please check and rectify the subscriber data manually.";

								logger.error("Error: Subscriber could not update on DB."
										+ subscriberDetail.getMsisdn());
								emailExecutor.sendQueuedEmails(errorMessage);
								return StatusCodes.SUCCESS;
							}
						} else {
							try {
								rimStatus = rimUtility
										.cancelSubscriptionByIMSI(subscriberDetail);
							} catch (Exception e) {
								e.printStackTrace();
								return StatusCodes.OTHER_ERRORS;
							}
							if (rimStatus.getErrorCode().equalsIgnoreCase("0")
									|| ((new Integer(rimStatus.getErrorCode()) >= 21000 && (new Integer(
											rimStatus.getErrorCode()) <= 21500)))
									|| (new Integer(rimStatus.getErrorCode()) <= 61040)) {
								subscriberDetail.setStatus("Deactivated");
								hibernateUtility
										.updateSubscriberDetail(subscriberDetail);

								/*
								 * smsService.sendMessageToKannel(properties.
								 * getProperty("unsuccessfulrenewalmessage"),
								 * subscriberDetail.getMsisdn());
								 */
								logger.info("Updated subscriber ["
										+ subscriberDetail.getMsisdn()
										+ "], returning insufficent balance message");

								return StatusCodes.INSUFFICENT_RENEWAL_BALANCE;
							} else {
								errorMessage = "Subscriber RIM deactivation for subscriber with MSISDN "
										+ subscriberDetail.getMsisdn()
										+ " was not successful in the hourly renewal job. The subscriber may still be active for free, please"
										+ "rectify manually.";

								logger.error("Error: Subscriber could not be deactivated on DB."
										+ subscriberDetail.getMsisdn());
								emailExecutor.sendQueuedEmails(errorMessage);

								return StatusCodes.INSUFFICENT_RENEWAL_BALANCE;
							}

						}
					} else {
						try {
							rimStatus = rimUtility
									.cancelSubscriptionByIMSI(subscriberDetail);
						} catch (Exception e) {
							e.printStackTrace();
							return StatusCodes.OTHER_ERRORS;
						}

						if (rimStatus.getErrorCode().equalsIgnoreCase("0")
								|| ((new Integer(rimStatus.getErrorCode()) >= 21000 && (new Integer(
										rimStatus.getErrorCode()) <= 21500)))
								|| (new Integer(rimStatus.getErrorCode()) <= 61040)) {
							subscriberDetail.setStatus("Deactivated");
							hibernateUtility
									.updateSubscriberDetail(subscriberDetail);
						}
						errorMessage = "Subscriber activation on RIM for subscriber with MSISDN "
								+ subscriberDetail.getMsisdn()
								+ " was not successful in the hourly renewal job. Please check and rectify the subscriber data manually.";

						logger.error("Error: Subscriber could not activate on RIM."
								+ subscriberDetail.getMsisdn());
						emailExecutor.sendQueuedEmails(errorMessage);

						return StatusCodes.SUCCESS;
					}
				} else {
					try {
						rimStatus = rimUtility
								.cancelSubscriptionByIMSI(subscriberDetail);
					} catch (Exception e) {
						logger.error("Connection to RIM failed");
						e.printStackTrace();
						return StatusCodes.OTHER_ERRORS;
					}

					if (rimStatus.getErrorCode().equalsIgnoreCase("0")
							|| ((new Integer(rimStatus.getErrorCode()) >= 21000 && (new Integer(
									rimStatus.getErrorCode()) <= 21500)))
							|| (new Integer(rimStatus.getErrorCode()) <= 61040)) {
						try {
							logger.info("About to pull APN");
							logger.info("EMA response is = "
									+ telnetManager.removeBlackberryAPNToSubscriber(
											subscriberDetail.getMsisdn(),
											properties
													.getProperty("APNID.Blackberry")));
						} catch (Exception e) {
							errorMessage = subscriberDetail.getMsisdn()
									+ " was deactivated on RIM because of insufficent balance, but "
									+ "the APN was not removed successfully for "
									+ subscriberDetail.getMsisdn()
									+ ". Please rectify manually.";
							emailExecutor.sendQueuedEmails(errorMessage);

							e.printStackTrace();
						}

						subscriberDetail.setStatus("Deactivated");
						hibernateUtility
								.updateSubscriberDetail(subscriberDetail);

						logger.info("Sending Insufficent renewal balance message to "
								+ subscriberDetail.getMsisdn());
						smsService
								.sendMessageToKannel(
										properties
												.getProperty("insufficentbalanceforrenewalmessage"),
										subscriberDetail.getMsisdn());
					} else {
						errorMessage = "Subscriber activation on RIM for subscriber with MSISDN "
								+ subscriberDetail.getMsisdn()
								+ " was not successful in the hourly renewal job. Please check and rectify the subscriber data manually.";

						logger.error("Error: Subscriber could not activate on RIM."
								+ subscriberDetail.getMsisdn());
						emailExecutor.sendQueuedEmails(errorMessage);

						logger.info("Sending Insufficent renewal balance message to "
								+ subscriberDetail.getMsisdn());
						smsService
								.sendMessageToKannel(
										properties
												.getProperty("insufficentbalanceforrenewalmessage"),
										subscriberDetail.getMsisdn());
					}
					return StatusCodes.INSUFFICENT_RENEWAL_BALANCE;
				}
			}
		}
		return 0;}
		catch(NullPointerException e){
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;
		}catch(Exception e){
			e.printStackTrace();
			return 0;
		}
	}

	public void setReceiverParameters(String receiverParams) {
		this.receiverParams = receiverParams;
	}

	public int logTransaction() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setSubscriber(SubscriberDetail subscriber) {
		this.subscriberDetail = subscriber;
	}

	private GregorianCalendar getNextSubscriptionDate(int noOfDays) {
		GregorianCalendar calendar1 = new GregorianCalendar();
		calendar1.add(GregorianCalendar.DAY_OF_MONTH,
				new Integer(noOfDays).intValue());
		return calendar1;
	}

	public int rollBack() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		// TODO Auto-generated method stub
		
	}

}
