/**
 * This class is used to check the type of subscriber. If the subscriber is a POSTPAID or PREPAID subscriber
 * It returns 0 if subscriber is postpaid and 500 if prepaid.
 * @author nnamdi Jibunoh
 */
package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.domain.ReconStatus;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.RIMXMLResponseDetail;
import com.vasconsulting.www.utility.RIMXMLUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class ImsiSwapCommandImpl implements Command {

	private SubscriberDetail subscriberDetail;
	private SubscriberDetail subscriberDetailDB;
	public SubscriberDetail getSubscriberDetailDB() {
		return subscriberDetailDB;
	}

	public void setSubscriberDetailDB(SubscriberDetail subscriberDetailDB) {
		this.subscriberDetailDB = subscriberDetailDB;
	}

	private Logger logger = Logger
			.getLogger(ImsiSwapCommandImpl.class);
	@SuppressWarnings("unused")
	private String receiverParams;
	private BillingPlanObjectUtility billingPlanObject;
	private TransactionLog transactionLog;
	private HibernateUtility hibernateUtility;
	private RIMXMLUtility rimUtility;
	
	private ReconStatus reconstatus;

	public int execute() {
		hibernateUtility = (hibernateUtility == null) ?
				(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility") : hibernateUtility;
		transactionLog = (transactionLog == null) ? (new TransactionLog()) : transactionLog;
		rimUtility = (rimUtility == null) ? (new RIMXMLUtility()) : rimUtility;
		transactionLog.setDate_created(new GregorianCalendar());		
		transactionLog.setMsisdn(subscriberDetail.getMsisdn());
		transactionLog.setService(subscriberDetail.getServiceplan());
		transactionLog.setDescription("SIM SWAP");
		transactionLog.setStatus("FAILED");
		logger.info("Execute called on SimSwapCommandImpl for subscriber with MSISDN = "+subscriberDetail.getMsisdn());

		subscriberDetailDB = hibernateUtility.getSubscriberInformation("msisdn", subscriberDetail.getMsisdn());
		logger.info("Subscriber status in SIMSwapCommandImpl is: "+subscriberDetailDB.getStatus());
		Integer rimStatus = StatusCodes.OTHER_ERRORS;
		if (!subscriberDetailDB.getStatus().equalsIgnoreCase("Active") || (subscriberDetailDB.getStatus() == null)){
			hibernateUtility.saveTransactionlog(transactionLog);
			logger.info("Subscriber is not in an active state so cannot have a SIM swap");
			return StatusCodes.SUBSCRIBER_NOT_ALLOWED;
		}
		try{
			RIMXMLResponseDetail responseData = rimUtility.modifyBillingIdentifier(subscriberDetailDB, subscriberDetail.getImsi());
			logger.info("response from RIM is: "+ responseData);
			rimStatus = new Integer(responseData.getErrorCode()).intValue();
			if (rimStatus == 0){
				transactionLog.setStatus("SUCCESFUL");
			}
			
			/**
			 * Author Akinsola
			 * VAscon
			 * This is to update the reconstatus to keep track of reconciliation
			 * 
			 * 
			 * 
			 */
			reconstatus.setMsisdn(subscriberDetail.getMsisdn());
			reconstatus.setNewimsi(subscriberDetail.getImsi());
			reconstatus.setOldimsi(subscriberDetailDB.getImsi());
			reconstatus.setStatus("Activated");
			reconstatus.setTransdate(new GregorianCalendar());
			hibernateUtility.saveTransactionlog(transactionLog);
			
			
		}catch(Exception e){
			hibernateUtility.saveTransactionlog(transactionLog);
			e.printStackTrace();
		}
		return rimStatus;

	}

	public RIMXMLUtility getRimUtility() {
		return rimUtility;
	}

	public void setRimUtility(RIMXMLUtility rimUtility) {
		this.rimUtility = rimUtility;
	}

	public TransactionLog getTransactionlog() {
		return transactionLog;
	}

	public void setTransactionlog(TransactionLog transactionlog) {
		transactionlog = new TransactionLog();
		this.transactionLog = transactionlog;
	}

	public HibernateUtility getHibernateUtility() {
		return hibernateUtility;
	}

	public void setHibernateUtility(HibernateUtility hibernateUtility) {
		this.hibernateUtility = hibernateUtility;
	}

	public BillingPlanObjectUtility getBillingPlanObject() {
		return billingPlanObject;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;
	}

	public int logTransaction() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams) {
		this.receiverParams = receiverParams;

	}

	public void setSubscriberDetails(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;

	}

	public SubscriberDetail getSubscriberDetails(){
		return subscriberDetail; 
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}
}
