package com.vasconsulting.www.interfaces.impl;

import java.rmi.RemoteException;
import java.util.GregorianCalendar;
import java.util.HashMap;

import org.apache.log4j.Logger;

import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKGProxy;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscriberInformationElement;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscriberInformationResponseElement;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.interfaces.RenewalCommand;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.HibernateUtilityImpl;
import com.vasconsulting.www.utility.RIMQueryUtil;
import com.vasconsulting.www.utility.StatusCodes;
import com.vasconsulting.www.utility.XMLUtility;

public class SynchronizeBlackberryServiceCommandImpl implements RenewalCommand {
	
	private RIMQueryUtil rimUtility = new RIMQueryUtil();
	private int rimStatus;
	private SubscriberDetail subscriberDetail;
	private TransactionLog transactionLog;
	private HibernateUtility hibernateUtility = new HibernateUtilityImpl();
	private XMLUtility xmlUtility = new XMLUtility();
	@SuppressWarnings("unused")
	private String receiverParams;
	private TBI_KPI_PKGProxy tbiProxy = new TBI_KPI_PKGProxy();
	private Logger logger = Logger.getLogger(SynchronizeBlackberryServiceCommandImpl.class);
	private int actionResponse;
	
	public int execute() {	
		
		String currentStatus = subscriberDetail.getStatus();
		
		GetSubscriberInformationElement subParams = new GetSubscriberInformationElement();
		subParams.setSubno(stripLeadingMsisdnPrefix(subscriberDetail.getMsisdn()));
		
		try {
			GetSubscriberInformationResponseElement responseElement = tbiProxy.getSubscriberInformation(subParams);
			
			HashMap<String, String> tabsResponse = xmlUtility.processTABSResponse(responseElement.getResult());
			
			logger.info("Call to check Subscriber Status on TABS API returned result : "+tabsResponse);
			
			if (tabsResponse.get("errorResponse") != null) actionResponse =  StatusCodes.OTHER_ERRORS;
			else if(tabsResponse.get("STATUS").equalsIgnoreCase("30")) 
			{
				subscriberDetail.setStatus("Active");
				actionResponse =  StatusCodes.SUCCESS;
			}
			else
			{
				subscriberDetail.setStatus("Deactivated");
				actionResponse = StatusCodes.BARRED_SUBSCRIBER;
			}
			
		} catch (RemoteException e) {
			e.printStackTrace();
			actionResponse = StatusCodes.OTHER_ERRORS;
		}	
		
		if(actionResponse == StatusCodes.OTHER_ERRORS)
		{
			//Send email to administrator about this failure and continue to other subscribers.
			//set actionResponse to 0
		}
		//subscriber has been barred on TABS but still active on db
		else if (actionResponse == StatusCodes.BARRED_SUBSCRIBER && currentStatus.trim().equalsIgnoreCase("Active"))
		{
			logger.info("Subscriber active on local system, but Barred on TABS, deactivating subscriber!");
			//deactivate on rim, update subscriberdetail and add transactionlog
			rimStatus = rimUtility.cancelSubscription(subscriberDetail);		
			
			if (rimStatus == 0 || (rimStatus >= 21000 && rimStatus <= 25000)) 
			{
				logger.info("Deactivate call on "+subscriberDetail.getMsisdn()+" returned "+rimStatus+". This is okay! proceeding.");
				
				subscriberDetail.setStatus("Deactivated");				
				rimStatus = hibernateUtility.updateSubscriberDetail(subscriberDetail);
				
				if (rimStatus == StatusCodes.SUCCESS)
				{
					transactionLog = new TransactionLog();
					transactionLog.setDate_created(new GregorianCalendar());
					transactionLog.setDescription("DEACTIVATE");
					transactionLog.setMsisdn(subscriberDetail.getMsisdn());
					transactionLog.setService(subscriberDetail.getServiceplan());
					transactionLog.setShortcode("RENEWAL");
					
					transactionLog.setStatus("SUCCESSFUL");
					rimStatus = hibernateUtility.saveTransactionlog(transactionLog);
					
					if (rimStatus != StatusCodes.SUCCESS)					
						//TODO send email to admin about failure to add transactionlog for this sub.
					
					actionResponse = StatusCodes.SUCCESS;					
				}
				else
				{
					//TODO send email to admin about failure to update this sub
				}
			}
			else 
			{
				logger.info("RIM Deactivate call on "+subscriberDetail.getMsisdn()+" returned "+rimStatus);
				//TODO send email to admin that the subscriber is still active bcos of failure on RIM
			}						
		}
		else if (actionResponse == StatusCodes.SUCCESS && currentStatus.equalsIgnoreCase("Deactivated"))
		{
			//activate on rim, update subscriberdetail and add transactionlog
			logger.info("Subscriber inactive on local system, but active on TABS, activating subscriber!");
			
			rimStatus = rimUtility.activateSubscriptionByUSSDChannel(subscriberDetail);
			
			if (rimStatus == 0 || (rimStatus >= 21000 && rimStatus <= 25000)) 
			{
				logger.info("Activate call on "+subscriberDetail.getMsisdn()+" returned "+rimStatus+". This is okay! proceeding.");
				
				subscriberDetail.setStatus("Active");				
				rimStatus = hibernateUtility.updateSubscriberDetail(subscriberDetail);
				
				if (rimStatus == StatusCodes.SUCCESS)
				{
					transactionLog = new TransactionLog();
					transactionLog.setDate_created(new GregorianCalendar());
					transactionLog.setDescription("ACTIVATE");
					transactionLog.setMsisdn(subscriberDetail.getMsisdn());
					transactionLog.setService(subscriberDetail.getServiceplan());
					transactionLog.setShortcode("RENEWAL");
					
					transactionLog.setStatus("SUCCESSFUL");
					rimStatus = hibernateUtility.saveTransactionlog(transactionLog);
					
					if (rimStatus != StatusCodes.SUCCESS)					
						//TODO send email to admin about failure to add transactionlog for this sub.
					
					actionResponse = StatusCodes.SUCCESS;					
				}
				else
				{
					//TODO send email to admin about failure to update this sub
					//set actionResponse to 0
				}				
			}
			else
			{
				logger.info("RIM Deactivate call on "+subscriberDetail.getMsisdn()+" returned "+rimStatus);
				//TODO send email to admin that the subscriber is still active bcos of failure on RIM
				//set actionResponse to 0
			}
		}
		
		return actionResponse;
	}

	public int logTransaction()
	{
		return 0;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams; 		
	}

	public void setSubscriber(SubscriberDetail subscriber)
	{
		this.subscriberDetail = subscriber;
	}
	
	private String stripLeadingMsisdnPrefix(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("0")){
			return Msisdn.substring(1, Msisdn.length());
		}
		else if (Msisdn.startsWith("234")){
			return Msisdn.substring(3, Msisdn.length());
		}
		else if(Msisdn.startsWith("+234")){
			return Msisdn.substring(4, Msisdn.length());
		}
		else return Msisdn;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		// TODO Auto-generated method stub
		
	}

}
