package com.vasconsulting.www.interfaces.impl;

import com.vasconsulting.www.utility.StatusCodes;
import org.apache.log4j.Logger;


public class GetSubscriberIMSIEMACommandForRequestForwardingImpl extends GetSubscriberIMSIEMACommandImpl {
	private Logger logger = Logger.getLogger(GetSubscriberIMSIEMACommandForRequestForwardingImpl.class);
	
	public int execute(){
		super.setReceiverParameters("prepaid");//Setting default susbcriber to prepaid
		int respCode = super.execute();
		logger.info("Subscriber IMSI as retrieved from EMA is: "+subscriberDetail.getImsi());
		logger.info("Response Code from Super is: "+respCode);
		if (respCode == StatusCodes.SUCCESS || respCode == StatusCodes.PREPAID_SUBSCRIBER_NOT_ALLOWED){
			subscriberDetail.setShortCode("prep"+subscriberDetail.getShortCode());
			respCode = StatusCodes.SUCCESS;
		} else if (respCode == StatusCodes.SUCCESS || respCode == StatusCodes.POSTPAID_SUBSCRIBER_NOT_ALLOWED){
			subscriberDetail.setShortCode("post"+subscriberDetail.getShortCode());
			respCode = StatusCodes.SUCCESS;
		}
		return respCode;	
	}

}
