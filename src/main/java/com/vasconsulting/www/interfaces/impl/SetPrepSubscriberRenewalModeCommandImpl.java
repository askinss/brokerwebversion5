package com.vasconsulting.www.interfaces.impl;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.SendSmsToKannelService;
import com.vasconsulting.www.utility.StatusCodes;

public class SetPrepSubscriberRenewalModeCommandImpl implements Command
{
	private SubscriberDetail subscriberDetail;
	private HibernateUtility hibernateUtility;
	private SubscriberDetail subscriberDetailLocal;
	private String receiverParams;
	Logger logger = Logger.getLogger(SetPrepSubscriberRenewalModeCommandImpl.class);
	
	public int execute()
	{
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		
		logger.info("Execute called on SetPrepSubscriberRenewalModeCommandImpl for subscriber with MSISDN = "+subscriberDetail.getMsisdn());
		
		if (receiverParams.equalsIgnoreCase(""))
			return StatusCodes.WRONG_AUTORESPONSE_MODE_CONFIG_FORMAT;
		
		logger.info("Subscriber is setting the auto renewal flag to "+receiverParams);
		
		subscriberDetailLocal = hibernateUtility.getSubscriberInformation(
				"msisdn", subscriberDetail.getMsisdn());

		if (subscriberDetailLocal != null) {
			subscriberDetailLocal.setAutoRenew(new Integer(receiverParams));

			int status = hibernateUtility
					.updateSubscriberDetail(subscriberDetailLocal);

			if (status == 0)
				return StatusCodes.SUCCESS;
			else {
				return StatusCodes.OTHER_ERRORS;
			}
		} else {
			return StatusCodes.SUBSCRIBER_NOT_IN_DB;
		}
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}

	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		// TODO Auto-generated method stub
		
	}

}
