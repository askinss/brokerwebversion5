package com.vasconsulting.www.interfaces.impl;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.RIMXMLResponseDetail;
import com.vasconsulting.www.utility.RIMXMLUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class CheckBlackberryServiceCommandImpl implements Command {
	private RIMXMLUtility rimUtility = new RIMXMLUtility();
	private SubscriberDetail subscriberDetail;
	private HibernateUtility hibernateUtility;
	private TransactionLog transactionlog;
	private RIMXMLResponseDetail rimStatus;
	private String receiverParams;
	Logger logger = Logger.getLogger(CheckBlackberryServiceCommandImpl.class);
	private BillingPlanObjectUtility billingPlanObject;

	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = 
				(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");

		logger.info("Execute called on CheckBlackberryServiceCommandImpl for subscriber with msisdn "+subscriberDetail.getMsisdn());
		logger.info("Checking for active RIM services for subscriber = "+subscriberDetail.getMsisdn());


		rimStatus = rimUtility.checkRIMStatusByIMSI(subscriberDetail );

		subscriberDetail.setServiceplan(receiverParams);
		transactionlog = new TransactionLog();
		transactionlog.setDate_created(new GregorianCalendar());
		transactionlog.setDescription("CHECK RIM SERVICE");
		transactionlog.setMsisdn(subscriberDetail.getMsisdn());
		transactionlog.setService(subscriberDetail.getServiceplan());
		transactionlog.setShortcode(subscriberDetail.getShortCode());
		
		
		System.out.println("This is RIM status: "+rimStatus.getErrorDescription());
		try{
			if(rimStatus.getErrorDescription().startsWith("Activated"))
			{
				transactionlog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionlog);
				transactionlog.setId(UUID.randomUUID().toString());
				transactionlog.setDescription("SHORTCODE");
				hibernateUtility.saveTransactionlog(transactionlog);
				logger.error("Subscriber already active on RIM aborting.");
				return StatusCodes.SUBSCRIBER_CANNOT_MIGRATE_WHILE_ACTIVE;
			}
			else return StatusCodes.SUCCESS;

		}catch(Exception e){
			transactionlog.setStatus("FAILED");
			hibernateUtility.saveTransactionlog(transactionlog);
			transactionlog.setId(UUID.randomUUID().toString());
			transactionlog.setDescription("SHORTCODE");
			hibernateUtility.saveTransactionlog(transactionlog);
			return StatusCodes.OTHER_ERRORS;
		}
	}

	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;

	}

	public int rollBack()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;
	}

}
