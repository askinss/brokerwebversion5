/**
 * This class is used to check the type of subscriber. If the subscriber is a POSTPAID or PREPAID subscriber
 * It returns 0 if subscriber is postpaid and 500 if prepaid.
 * @author nnamdi Jibunoh
 */
package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.UCIPServiceRequestManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class CheckPrepSubscriberCreditLimitCommandImpl implements Command {

	protected SubscriberDetail subscriberDetail;
	private UCIPServiceRequestManager ucipConnector;
	protected Logger logger = Logger
			.getLogger(CheckPrepSubscriberCreditLimitCommandImpl.class);
	protected float accountBalance;
	protected String receiverParams;
	protected String receiver;
	protected TransactionLog transactionlog;
	protected BillingPlanObjectUtility billingPlanObject;
	protected HibernateUtility hibernateUtility;

	/**
	 * This method will execute and perform service change on TABS based on the
	 * supplied receiver. The method will log at the end of the method execution
	 * the state of the transaction run.
	 * 
	 * @author Nnamdi Jibunoh
	 * @param
	 */
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		ucipConnector = (ucipConnector == null) ? (UCIPServiceRequestManager)ContextLoaderImpl
				.getBeans("ucipConnector") : ucipConnector;
		hibernateUtility = (hibernateUtility == null) ? (HibernateUtility) ContextLoaderImpl
				.getBeans("hibernateUtility") : hibernateUtility;
		transactionlog = (transactionlog == null) ? new TransactionLog() : transactionlog;
		transactionlog.setDate_created(new GregorianCalendar());
		transactionlog.setDescription("CHECK ACCOUNT BALANCE");
		transactionlog.setMsisdn(subscriberDetail.getMsisdn());
		transactionlog.setShortcode(subscriberDetail.getShortCode());
		transactionlog.setService(subscriberDetail.getServiceplan());

		logger.info("Execute called on CheckPrepSubscriberCreditLimitCommandImpl for subscriber with MSISDN = "
				+ subscriberDetail.getMsisdn());
		logger.info(billingPlanObject.getShortCode());

		if (receiverParams == null || receiverParams.equalsIgnoreCase("")) {
			transactionlog.setStatus("FAILED");
			hibernateUtility.saveTransactionlog(transactionlog);
			transactionlog.setId(UUID.randomUUID().toString());
			transactionlog.setDescription("SHORTCODE");
			hibernateUtility.saveTransactionlog(transactionlog);
			return StatusCodes.NO_CREDIT_LIMIT;
		} else {
			try {

				@SuppressWarnings("unchecked")
				Map<String, String> responseMap = ucipConnector.getSubscriberBalance(subscriberDetail.getMsisdn());
				logger.info("Result from the AIR Servers is" + responseMap);

				if (responseMap == null) {
					transactionlog.setStatus("FAILED");
					hibernateUtility.saveTransactionlog(transactionlog);
					transactionlog.setId(UUID.randomUUID().toString());
					transactionlog.setDescription("SHORTCODE");
					hibernateUtility.saveTransactionlog(transactionlog);
					return StatusCodes.ERROR_CHECKING_SUB_BALANCE;
				} else {
					accountBalance = Float.valueOf(responseMap.get("accountValue1"));
					logger.info("The configured amount parameter for this plan is = "
							+ billingPlanObject.getCost());
					logger.info("Configured multiplier is: "+receiverParams);
					double amountToDeduct = Double.valueOf(billingPlanObject.getCost()) * Double.valueOf(receiverParams);
					logger.info("Subscriber [" + subscriberDetail.getMsisdn()
							+ "] current account balance is =" + accountBalance);

					logger.info("The cost of this service for subscriber "
							+ subscriberDetail.getMsisdn() + " would be "
							+ amountToDeduct);

					if (accountBalance >= amountToDeduct) {
						transactionlog.setStatus("SUCCESSFUL");
						hibernateUtility.saveTransactionlog(transactionlog);
						logger.info("Subscriber "
								+ subscriberDetail.getMsisdn()
								+ " balance now is "
								+ accountBalance
								+ ", new balance if deduction occurs would be less by "
								+ amountToDeduct);
						return StatusCodes.SUCCESS;
					} else {
						transactionlog.setStatus("SUCCESSFUL");
						hibernateUtility.saveTransactionlog(transactionlog);
						transactionlog.setId(UUID.randomUUID().toString());
						transactionlog.setStatus("FAILED");
						transactionlog.setDescription("SHORTCODE");
						hibernateUtility.saveTransactionlog(transactionlog);
						return StatusCodes.INSUFFICENT_BALANCE_CHECK;
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				transactionlog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionlog);
				transactionlog.setId(UUID.randomUUID().toString());
				transactionlog.setDescription("SHORTCODE");
				hibernateUtility.saveTransactionlog(transactionlog);
				e.printStackTrace();
				return StatusCodes.ERROR_CHECKING_SUB_BALANCE;
			}
		}
	}

	public int logTransaction() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams) {
		this.receiverParams = receiverParams;

	}

	private int daysBetween(GregorianCalendar startDate,
			GregorianCalendar endDate) {
		GregorianCalendar date = (GregorianCalendar) startDate.clone();
		int daysBetween = 0;
		while (date.before(endDate)) {
			date.add(GregorianCalendar.DAY_OF_MONTH, 1);
			daysBetween++;
		}
		return daysBetween;
	}
	
	private GregorianCalendar getLastDateOfMonth() {
		GregorianCalendar calendar = new GregorianCalendar();
		int dayOfMonth = calendar
				.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);

		int daysToAdd = dayOfMonth
				- calendar.get(GregorianCalendar.DAY_OF_MONTH);

		calendar.add(GregorianCalendar.DAY_OF_MONTH, daysToAdd);
		return calendar;
	}

	private String stripLeadingMsisdnPrefix(String msisdn) {
		String Msisdn = msisdn;
		if (msisdn.startsWith("0")) {
			return Msisdn.substring(1, Msisdn.length());
		} else if (Msisdn.startsWith("243")) {
			return Msisdn.substring(3, Msisdn.length());
		} else if (Msisdn.startsWith("+243")) {
			return Msisdn.substring(4, Msisdn.length());
		} else
			return Msisdn;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
		
	}

	public UCIPServiceRequestManager getUcipConnector() {
		return ucipConnector;
	}

	public void setUcipConnector(UCIPServiceRequestManager ucipConnector) {
		this.ucipConnector = ucipConnector;
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;
	}

}
