package com.vasconsulting.www.interfaces.impl;

import java.rmi.RemoteException;
import java.util.GregorianCalendar;
import java.util.UUID;

import org.apache.log4j.Logger;

import ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.OrderManagementProxy;
import ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeserviceElement;
import ws.its.tabs.webservices.ordermangement.OrderManagement_wsdl.types.ChangeserviceResponseElement;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.interfaces.TabsInterface;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.RIMQueryUtil;
import com.vasconsulting.www.utility.StatusCodes;
import com.vasconsulting.www.utility.XMLUtility;

public class CreateSubscriberTABSServiceCommandSchImpl implements Command {
	private HibernateUtility hibernateUtility;
	private SubscriberDetail subscriberDetail;
	XMLUtility xmlUtility = new XMLUtility();
	private LoadAllProperties properties;
	private Logger logger = Logger.getLogger(CreateSubscriberTABSServiceCommandSchImpl.class);
	private TransactionLog transactionlog;
	private TabsInterface tabs;
	private BillingPlanObjectUtility billingPlanObject;
	
		
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		properties = 
			(LoadAllProperties)ContextLoaderImpl.getBeans("loadProperties");
		tabs = (TabsInterface)ContextLoaderImpl.getBeans("tabs");
		subscriberDetail = (SubscriberDetail)ContextLoaderImpl.getBeans("subscriberDetail");
		
		
		logger.info("Execute called on CreateSubscriberTABSServiceCommandSchImpl for subscriber with MSISDN = "+subscriberDetail.getMsisdn());
		
		String responseCode = "";
		transactionlog = new TransactionLog();
		transactionlog.setDate_created(new GregorianCalendar());
		transactionlog.setMsisdn(subscriberDetail.getMsisdn());
		transactionlog.setDescription("Change TABS Service");
		transactionlog.setShortcode(subscriberDetail.getShortCode());
		
		try {
			responseCode = tabs.changeService(subscriberDetail.getMsisdn(),properties.getProperty(subscriberDetail.getShortCode()));
			logger.info("Response from TABS = "+responseCode);
			
			if ((responseCode != null && responseCode.equalsIgnoreCase("0")) || 
					(responseCode != null && responseCode.equalsIgnoreCase("3172")) || 
					(responseCode != null && responseCode.equalsIgnoreCase("300512"))){				
				transactionlog.setService(properties.getProperty(subscriberDetail.getShortCode()));
				transactionlog.setStatus("SUCCESSFUL");				
				hibernateUtility.saveTransactionlog(transactionlog);				
				logger.info("CreateSubscriberTABSServiceCommandSchImpl for subscriber "+subscriberDetail.getMsisdn()+" returned with code "+responseCode);
				
				return StatusCodes.SUCCESS;
			}
			else{
				transactionlog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionlog);
				transactionlog.setId(UUID.randomUUID().toString());
				transactionlog.setDescription("SHORTCODE");
				hibernateUtility.saveTransactionlog(transactionlog);
				return StatusCodes.OTHER_ERRORS_CHANGE_SERVICE;
			}			
		} catch (Exception e) {
			transactionlog.setStatus("FAILED");
			hibernateUtility.saveTransactionlog(transactionlog);
			transactionlog.setId(UUID.randomUUID().toString());
			transactionlog.setDescription("SHORTCODE");
			hibernateUtility.saveTransactionlog(transactionlog);			
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS_CHANGE_SERVICE;
		}
	}

	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams){
	}
	
	private String stripLeadingMsisdnPrefix(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("0")){
			return Msisdn.substring(1, Msisdn.length());
		}
		else if (Msisdn.startsWith("255")){
			return Msisdn.substring(3, Msisdn.length());
		}
		else if(Msisdn.startsWith("+255")){
			return Msisdn.substring(4, Msisdn.length());
		}
		else return Msisdn;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}

}
