package com.vasconsulting.www.interfaces.impl;

import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.StatusCodes;
import com.vasconsulting.www.utility.TimeUtilities;

public class SychronizePostSubscriberForFirstOfNextMonthToDBCommandImpl implements Command {
	
	private SubscriberDetail subscriberDetail;
	private SubscriberDetail subscriberDetailLocal;
	Collection<SubscriberDetail> subscriberDetailDB;
	private HibernateUtility hibernateUtility;
	private EmailTaskExecutor emailTaskExecutor;
	Logger logger = Logger.getLogger(SychronizePostSubscriberForFirstOfNextMonthToDBCommandImpl.class);
		
	/**
	 * This method will execute and perform service change on TABS based on the supplied
	 * receiver. The method will log at the end of the method execution the state of the 
	 * transaction run.
	 * @author Nnamdi Jibunoh
	 * @param
	 */
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");		
		emailTaskExecutor = 
			(EmailTaskExecutor)ContextLoaderImpl.getBeans("myEmailTaskExecutor");
		
		logger.info("Execute called on SychronizePostSubscriberForFirstOfNextMonthToDBCommandImpl for subscriber with MSISDN = "+subscriberDetail.getMsisdn());
				
		subscriberDetailDB = hibernateUtility.getSubscriberInformation(subscriberDetail);
		subscriberDetail.setNext_subscription_date(getNextSubscriptionDate(Integer.valueOf(subscriberDetail.getServicetype())));
		subscriberDetail.setLast_subscription_date(new GregorianCalendar());
		
		if (subscriberDetailDB != null && subscriberDetailDB.size() > 0)
		{
			for (Iterator<SubscriberDetail> iterator = subscriberDetailDB.iterator(); iterator.hasNext();) {
				SubscriberDetail subscriber = iterator.next();
				
				/**
				 * FIX:5000 Compare the returned value against the in-session value to make sure they are the same because in future
				 * all subscribers would be harmonized
				 */
				if ((subscriber != null) && (subscriber.getMsisdn().equalsIgnoreCase(subscriberDetail.getMsisdn()) ))
				{					
					
					int status = -1;
					try
					{							
						subscriberDetailLocal = new SubscriberDetail();
						
						subscriberDetailLocal.setMsisdn(subscriberDetail.getMsisdn());
						subscriberDetailLocal.setImsi(subscriberDetail.getImsi());
						subscriberDetailLocal.setServiceplan(subscriberDetail.getServiceplan());
						
						subscriberDetailLocal.setFirstname(subscriberDetail.getFirstname());
						subscriberDetailLocal.setLastname(subscriberDetail.getLastname());
						subscriberDetailLocal.setMiddlename(subscriberDetail.getMiddlename());
						
						subscriberDetailLocal.setEmail(subscriberDetail.getEmail());
						subscriberDetailLocal.setPin(subscriberDetail.getPin());
						subscriberDetailLocal.setImei(subscriberDetail.getImei());
						
						subscriberDetailLocal.setShortCode(subscriberDetail.getShortCode());
						subscriberDetailLocal.setPrepaidSubscriber(0);
						subscriberDetailLocal.setPostpaidSubscriber(1);
						subscriberDetailLocal.setAutoRenew(subscriberDetail.getAutoRenew());
						subscriberDetailLocal.setServicetype(subscriberDetail.getServicetype());
						
						subscriberDetailLocal.setDate_created(subscriber.getDate_created());
						subscriberDetailLocal.setLast_subscription_date(subscriberDetail.getLast_subscription_date());
						
						logger.info("Setting Next Subscription Date to be"+TimeUtilities.firstDayOfNextMonth().getTime());
						
						subscriberDetailLocal.setNext_subscription_date(TimeUtilities.firstDayOfNextMonth());
						
						
						subscriberDetailLocal.setStatus("Active");
						subscriberDetailLocal.setId(subscriber.getId());
													
						status = hibernateUtility.updateSubscriberDetail(subscriberDetailLocal);
						logger.info("The updateSubscriberDetail method call returned a value of = "+status);
						
						if (status != StatusCodes.SUCCESS)
						{
							emailTaskExecutor.sendQueuedEmails("********* CRITICAL ["+subscriberDetail.getMsisdn()+"] ************\n" +
									"Subscriber with details "+
									subscriberDetail.getMsisdn()+" could not be synchronized to database. This mostly likely means the subscriber " +
											"detail is out-of-sync with the network. Please retify manually or notify the administrator.");
						}
					}
					catch(Exception ex)
					{
						emailTaskExecutor.sendQueuedEmails("********* CRITICAL ["+subscriberDetail.getMsisdn()+"] ************\n" +
								"Subscriber with details "+
								subscriberDetail.getMsisdn()+" could not be synchronized to database. This mostly likely means the subscriber " +
										"detail is out-of-sync with the network. Please retify manually or notify the administrator.");
					}						
					return status;
				
				}				
			}
		}
		else 
		{
			/**
			 * FIX:1000. Save the in-session subscriberDetail to DB
			 */
			logger.info("Subscriber "+subscriberDetail.getMsisdn()+" was not found in the database, so in-session " +
					"subscriberDetail is being synchronized to database");
			
			subscriberDetailLocal = new SubscriberDetail();
			
			subscriberDetailLocal.setMsisdn(subscriberDetail.getMsisdn());
			subscriberDetailLocal.setImsi(subscriberDetail.getImsi());
			subscriberDetailLocal.setServiceplan(subscriberDetail.getServiceplan());
			
			subscriberDetailLocal.setFirstname(subscriberDetail.getFirstname());
			subscriberDetailLocal.setLastname(subscriberDetail.getLastname());
			subscriberDetailLocal.setMiddlename(subscriberDetail.getMiddlename());
			
			subscriberDetailLocal.setEmail(subscriberDetail.getEmail());
			subscriberDetailLocal.setPin(subscriberDetail.getPin());
			subscriberDetailLocal.setImei(subscriberDetail.getImei());
			
			subscriberDetailLocal.setShortCode(subscriberDetail.getShortCode());
			subscriberDetailLocal.setPrepaidSubscriber(subscriberDetail.getPrepaidSubscriber());
			subscriberDetailLocal.setPostpaidSubscriber(subscriberDetail.getPostpaidSubscriber());
			subscriberDetailLocal.setAutoRenew(subscriberDetail.getAutoRenew());
			subscriberDetailLocal.setServicetype(subscriberDetail.getServicetype());
			
			subscriberDetailLocal.setDate_created(subscriberDetail.getDate_created());
			subscriberDetailLocal.setLast_subscription_date(subscriberDetail.getLast_subscription_date());
			logger.info("Setting Next Subscription Date to be"+TimeUtilities.firstDayOfNextMonth().getTime());
			
			subscriberDetailLocal.setNext_subscription_date(TimeUtilities.firstDayOfNextMonth());
			subscriberDetailLocal.setStatus("Active");
			
						
			int status = -1;
			
			try
			{
				status = hibernateUtility.saveSubscriber(subscriberDetailLocal);				
			}
			catch(Exception ex)
			{
				emailTaskExecutor.sendQueuedEmails("********* CRITICAL ["+subscriberDetail.getMsisdn()+"] ************\n" +
						"Subscriber with details "+
						subscriberDetail.getMsisdn()+" could not be synchronized to database. This mostly likely means the subscriber " +
								"detail is out-of-sync with the network. Please retify manually or notify the administrator.");
			}		
			
			logger.info("The saveSubscriber method call returned a value of = "+status);
			
			if (status != StatusCodes.SUCCESS)
			{
				emailTaskExecutor.sendQueuedEmails("********* CRITICAL ["+subscriberDetail.getMsisdn()+"] ************\n" +
						"Subscriber with details "+
						subscriberDetail.getMsisdn()+" could not be synchronized to database. This mostly likely means the subscriber " +
								"detail is out-of-sync with the network. Please retify manually or notify the administrator.");
				return status;
			}
			return StatusCodes.SUCCESS;
		}
		return StatusCodes.OTHER_ERRORS;	
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
		
	public void setReceiverParameters(String receiverParams)
	{
		// TODO Auto-generated method stub
		
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}	
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		// TODO Auto-generated method stub
		
	}
	
	private GregorianCalendar getNextSubscriptionDate(int noOfDays){
		GregorianCalendar calendar1 = new GregorianCalendar();
		calendar1.add(GregorianCalendar.DAY_OF_MONTH, noOfDays);
		return calendar1;
	}

}
