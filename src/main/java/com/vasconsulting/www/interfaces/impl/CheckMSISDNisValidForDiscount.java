/**
 * This class is used to check the type of subscriber. If the subscriber is a POSTPAID or PREPAID subscriber
 * It returns 0 if subscriber is postpaid and 500 if prepaid.
 * @author nnamdi Jibunoh
 */
package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class CheckMSISDNisValidForDiscount implements Command {

	private SubscriberDetail subscriberDetail;
	private Logger logger = Logger
			.getLogger(CheckMSISDNisValidForDiscount.class);
	private String receiverParams;
	private BillingPlanObjectUtility billingPlanObject;
	private TransactionLog transactionlog;
	private HibernateUtility hibernateUtility;

	@Autowired
	public void setTransactionlog(TransactionLog transactionlog) {
		transactionlog = new TransactionLog();
		this.transactionlog = transactionlog;
	}

	@Autowired
	public void setHibernateUtility(HibernateUtility hibernateUtility) {
		this.hibernateUtility = hibernateUtility;
	}

	/**
	 * This method will execute and perform service change on TABS based on the
	 * supplied receiver. The method will log at the end of the method execution
	 * the state of the transaction run.
	 * 
	 * @author Nnamdi Jibunoh
	 * @param
	 */
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		transactionlog.setDate_created(new GregorianCalendar());		
		transactionlog.setMsisdn(subscriberDetail.getMsisdn());
		transactionlog.setShortcode(subscriberDetail.getShortCode());
		transactionlog.setService(subscriberDetail.getServiceplan());

		logger.info("Execute called on CheckMSISDNisValidForDiscount for subscriber with MSISDN = "
				+ subscriberDetail.getMsisdn());
		try{
			if (hibernateUtility.getBBPromoInitialYesStatusByMSISDN(subscriberDetail.getMsisdn()) != null){
				logger.info("New cost of service is: "+receiverParams);
				billingPlanObject.setCost(receiverParams);
				transactionlog.setDescription("SET MSISDN COST TO: "+receiverParams);
				transactionlog.setStatus("SUCCESSFUL");
				transactionlog.setId(UUID.randomUUID().toString());
				hibernateUtility.saveTransactionlog(transactionlog);
				return StatusCodes.SUCCESS;
			}else {
				transactionlog.setDescription("MSISDN NOT VALID FOR DISCOUNT");
				transactionlog.setId(UUID.randomUUID().toString());
				transactionlog.setStatus("SUCCESSFUL");
				hibernateUtility.saveTransactionlog(transactionlog);
				return StatusCodes.SUCCESS;
			}
		}
		catch(Exception e){
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;
		}


	}

	public BillingPlanObjectUtility getBillingPlanObject() {
		return billingPlanObject;
	}


	public int logTransaction() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams) {
		this.receiverParams = receiverParams;

	}


	public SubscriberDetail getSubscriberDetail(){
		return subscriberDetail; 
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;
	}
}
