package com.vasconsulting.www.interfaces.impl;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class CheckSubscriberAutoRenewStatusSchImpl implements Command  {
	private SubscriberDetail subscriberDetail;
	private BillingPlanObjectUtility billingPlanObject;

	
	public int execute() {
		// TODO Auto-generated method stub
		int autoRenew = subscriberDetail.getAutoRenew();
		if (autoRenew == 0){
			return StatusCodes.NO_AUTORENEWAL;
		}
		
		return 0;
	}

	
	public void setReceiverParameters(String receiverParams) {
		// TODO Auto-generated method stub
		
	}

	
	public int logTransaction() {
		// TODO Auto-generated method stub
		return 0;
	}


	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}


	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}
}
