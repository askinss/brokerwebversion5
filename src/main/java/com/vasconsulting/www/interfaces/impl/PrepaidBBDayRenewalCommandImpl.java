package com.vasconsulting.www.interfaces.impl;

import java.text.DecimalFormat;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.UCIPServiceRequestManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.interfaces.RenewalCommand;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.RIMQueryUtil;
import com.vasconsulting.www.utility.StatusCodes;

/**
 * This is the concrete class for the prepaid daily service renewal.
 * This class is generated and called by AutoRenewAllPrepDayBBSubscriber class  
 * @author nnamdi
 *
 */
public class PrepaidBBDayRenewalCommandImpl implements RenewalCommand
{
	private String receiverParams,errorMessage;
	private Logger logger = Logger.getLogger(PrepaidBBDayRenewalCommandImpl.class);
	private SubscriberDetail subscriberDetail;
	private UCIPServiceRequestManager ucipConnector = new UCIPServiceRequestManager();
	private RIMQueryUtil rimUtility = new RIMQueryUtil();
	private HibernateUtility hibernateUtility = (HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
	private EmailTaskExecutor emailExecutor = (EmailTaskExecutor)ContextLoaderImpl.getBeans("myEmailTaskExecutor");
	private TransactionLog transaction;
	private BillingPlanObjectUtility billingPlanObject;
	
	@SuppressWarnings("unchecked")
	public int execute()
	{		
		int autoRenew = subscriberDetail.getAutoRenew();
		int status = -1;
		double accountBalance;
		logger.info("Value of autoRenew"+autoRenew);
		if (autoRenew == 0)
		{
			//deactivate sub
			subscriberDetail.setStatus("Deactivated");
			status = rimUtility.cancelSubscription(subscriberDetail);
			
			if (status == 0 || (status >=21000 && status <= 21500))
			{
				status = hibernateUtility.updateSubscriberDetail(subscriberDetail);
				if (status == 0)
				{
					transaction = new TransactionLog();
					transaction.setDate_created(new GregorianCalendar());
					transaction.setDescription("DEACTIVATION");
					transaction.setMsisdn(subscriberDetail.getMsisdn());
					transaction.setShortcode("SCHEDULER:NO_AUTO_RENEW");
					hibernateUtility.saveTransactionlog(transaction);
				}
				else
				{
					errorMessage = "Subscriber data did not update for subscriber with MSISDN "+subscriberDetail.getMsisdn() +
							" in the hourly renewal job. Please rectify the subscriber data manually.";
					logger.error("Error: Subscriber data did not update in database."+subscriberDetail.getMsisdn());
					emailExecutor.sendQueuedEmails(errorMessage);
					return StatusCodes.SUCCESS;
				}
			}	
			else {
				errorMessage = "Subscriber deactivation was not successfully on RIM for subscriber with MSISDN "+subscriberDetail.getMsisdn() +
				" in the hourly renewal job. The subscriber has auto renewal disabled. Please rectify the subscriber data manually.";
				logger.error("Error: Subscriber data did not update in database."+subscriberDetail.getMsisdn());
				emailExecutor.sendQueuedEmails(errorMessage);
				return StatusCodes.SUCCESS;
			}
		}
		else
		{
			if (receiverParams.equalsIgnoreCase("") || receiverParams == null)
			{
				errorMessage = "The configuration file for the renewal module is either corrupt or not properly set. Please consult the documentation " +
						"and then set the details for Prepaid Blackberry renewal. ["+subscriberDetail.getMsisdn()+"]";
				logger.error("Error: Error renewal setting for application not set");
				emailExecutor.sendQueuedEmails(errorMessage);
				return StatusCodes.OTHER_ERRORS;
			}
			
			//amt:sep:rim details - Seperate rim details with : in the details if neccessary
			String provParams[] = receiverParams.split(":sep:");
			DecimalFormat decimalFormatter = new DecimalFormat("#####.##");
						
			Map<String, String> responseMap = new HashMap<String, String>();
			try {
				responseMap = ucipConnector.getSubscriberBalance(subscriberDetail.getMsisdn());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			logger.info("Result from the AIR Servers is"+responseMap);
			
			if (responseMap == null)
				return StatusCodes.ERROR_CHECKING_SUB_BALANCE;
			else
			{					
				accountBalance = new Double(responseMap.get("accountValue1")).doubleValue();
				
				double amountToDeduct = new Double(provParams[0]).doubleValue();
				accountBalance = accountBalance / 100;//This is to convert the account balance to kobo instead of Naira
				
				logger.info("Subscriber ["+subscriberDetail.getMsisdn()+"] current account balance is ="+accountBalance);					
				
				logger.info("The cost of this service for subscriber "+subscriberDetail.getMsisdn()+" would be "+amountToDeduct);
				
				if (accountBalance >= amountToDeduct)
				{					
					logger.info("Subscriber "+subscriberDetail.getMsisdn()+" balance now is "+accountBalance
							+", new balance if deduction occurs would be less by "+
							amountToDeduct);
					
					status = rimUtility.activateSubscriptionByUSSDChannel(subscriberDetail, provParams[1].split(":"));
					
					if (status == 0 || (status >=21000 && status <= 21500))
					{
						try {
							
							responseMap = ucipConnector.
							updateSubscriberDedicatedAccount(subscriberDetail.getMsisdn(), provParams[2], 
									"10" ,"", new Integer(provParams[3]).intValue(),
									subscriberDetail.getServiceplan()+"_Renew");
							
							responseMap = ucipConnector.updateSubscriberBalance(subscriberDetail.getMsisdn(), 
									new Long(Math.round(new Double(decimalFormatter.format(amountToDeduct * -100)))).toString()
									, subscriberDetail.getServiceplan()+"_Renewal"+subscriberDetail.getServicetype());
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						if (responseMap.get("responseCode").equalsIgnoreCase("0"))
						{
							GregorianCalendar calendar = new GregorianCalendar();
							
							subscriberDetail.setLast_subscription_date(calendar);
							subscriberDetail.setNext_subscription_date(getNextSubscriptionDate());
							subscriberDetail.setStatus("Active");
							
							status = hibernateUtility.updateSubscriberDetail(subscriberDetail);
							
							if (status == 0)
							{
								transaction = new TransactionLog();
								transaction.setDate_created(calendar);
								transaction.setDescription("RENEWAL");
								transaction.setMsisdn(subscriberDetail.getMsisdn());
								transaction.setShortcode("SCHEDULER");
								hibernateUtility.saveTransactionlog(transaction);
								
								return StatusCodes.SUCCESS;
							}
							else
							{
								errorMessage = "Subscriber data in DB for subscriber with MSISDN "+subscriberDetail.getMsisdn() +
								" was not successful in the hourly renewal job. All other requirements are satisfied. " +
								"Please check and rectify the subscriber data manually.["+subscriberDetail.getMsisdn()+"]";
								
								logger.error("Error: Subscriber could not update on DB."+subscriberDetail.getMsisdn());
								emailExecutor.sendQueuedEmails(errorMessage);
								return StatusCodes.SUCCESS;
							}
						}
						else
						{
							status = rimUtility.cancelSubscription(subscriberDetail);
							if (status == 0 || (status >=21000 && status <= 21500))
							{
								subscriberDetail.setStatus("Deactivated");
								hibernateUtility.updateSubscriberDetail(subscriberDetail);
								
								return StatusCodes.OTHER_ERROR_RENEWAL;
								
							}
							else
							{
								errorMessage = "Subscriber RIM deactivation for subscriber with MSISDN "+subscriberDetail.getMsisdn() +
								" was not successful in the hourly renewal job. The subscriber may still be active for free, please" +
								"rectify manually.["+subscriberDetail.getMsisdn()+"]";
								
								logger.error("Error: Subscriber could not be deactivated on DB."+subscriberDetail.getMsisdn());
								emailExecutor.sendQueuedEmails(errorMessage);
							}
							return StatusCodes.SUCCESS;
						}
					}
					else
					{
						subscriberDetail.setStatus("Deactivated");
						status = hibernateUtility.updateSubscriberDetail(subscriberDetail);
						
						if (status == 0 )
						{
							errorMessage = "Subscriber activation on RIM for subscriber with MSISDN "+subscriberDetail.getMsisdn() +
							" was not successful in the hourly renewal job. Please check and rectify the subscriber data manually. " +
							"["+subscriberDetail.getMsisdn()+"]";
							
							logger.error("Error: Subscriber could not activate on RIM."+subscriberDetail.getMsisdn());
							emailExecutor.sendQueuedEmails(errorMessage);							
						}
						
						
						return StatusCodes.SUCCESS;
					}
				}
				else{
					status = rimUtility.cancelSubscription(subscriberDetail);
					
					if (status == 0 || (status >=21000 && status <= 21500))
					{
						subscriberDetail.setStatus("Deactivated");
						status = hibernateUtility.updateSubscriberDetail(subscriberDetail);
						
						if (status == 0)
							return StatusCodes.SUCCESS;
						else
						{
							errorMessage = "Service on RIM for subscriber with MSISDN "+subscriberDetail.getMsisdn() +
							" was successful deactivated in the hourly renewal job, but the status in the database is still active. " +
							"Please check and rectify the subscriber data manually.["+subscriberDetail.getMsisdn()+"]";
							
							logger.error("Error: Subscriber could not update subscriber in Database ["+subscriberDetail.getMsisdn()+"]");
							emailExecutor.sendQueuedEmails(errorMessage);
						}
					}
					else
					{
						errorMessage = "Subscriber de-activation on RIM for subscriber with MSISDN "+subscriberDetail.getMsisdn() +
						" was not successful in the hourly renewal job. Please check and rectify the subscriber data manually." +
						"["+subscriberDetail.getMsisdn()+"]";
						
						logger.error("Error: Subscriber could not de-activate on RIM ["+subscriberDetail.getMsisdn()+"]");
						emailExecutor.sendQueuedEmails(errorMessage);
					}
					return StatusCodes.INSUFFICENT_RENEWAL_BALANCE;
				}
			}		
		}
		return 0;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}

	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setSubscriber(SubscriberDetail subscriber)
	{
		this.subscriberDetail = subscriber;
	}
	
	private GregorianCalendar getNextSubscriptionDate(){
		GregorianCalendar calendar1 = new GregorianCalendar();
		calendar1.set(GregorianCalendar.HOUR_OF_DAY, 11);
		calendar1.set(GregorianCalendar.MINUTE, 58);
		
		calendar1.add(GregorianCalendar.DAY_OF_MONTH, 1);
		return calendar1;
	}

	public int rollBack()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}

}
