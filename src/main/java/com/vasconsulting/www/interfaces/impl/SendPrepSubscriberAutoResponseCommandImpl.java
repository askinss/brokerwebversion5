package com.vasconsulting.www.interfaces.impl;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.SendSmsToKannelService;
import com.vasconsulting.www.utility.StatusCodes;

public class SendPrepSubscriberAutoResponseCommandImpl implements Command
{
	private SubscriberDetail subscriberDetail;
	private String receiverParams;
	Logger logger = Logger.getLogger(SendPrepSubscriberAutoResponseCommandImpl.class);
	private SendSmsToKannelService smsService;
	
	/**
	 * MESSAGE FOR ALL: Dear subscriber, your Airtel Blackberry service is Live. Please remove and replace your battery to get the service running.
	 * 
	 * BISDAY: Thank you for subscribing to Airtel Blackberry same day service, this service expires at midnight. To disable auto renewal, send 
	 * stopautorenew to 440
	 * BISDAY: Dear subscriber, your Blackberry service expires in 4 hours. To disable auto renewal, send stopautorenew to 440
	 * 
	 * Other BB Service: Thank you for subscribing to Airtel Blackberry service. To disable auto renewal, send stopautorenewal to 440
	 * Other BB Service: Dear subscriber, your current Airtel Blackberry service expires on %1$ta %1$te %1$tb,%1$tY. To disable auto renewal, please send stopautorenew
	 * to 440
	 * 
	 * MY Other BB Service: Dear subscriber, your current Airtel Blackberry service is until %1te. To disable auto renewal, send stopautorenew to 440
	 */
	
	public int execute()
	{
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		smsService = 
			(SendSmsToKannelService)ContextLoaderImpl.getBeans("smsService");
		
		logger.info("Execute called on SendPrepSubscriberAutoResponse for subscriber with MSISDN = "+subscriberDetail.getMsisdn());
		
		if (receiverParams.equalsIgnoreCase(""))
			return StatusCodes.WRONG_AUTORESPONSE_STATUS_CONFIG_FORMAT;
		
		logger.info("Sending auto response text "+receiverParams+" to "+subscriberDetail.getMsisdn());
		
		smsService.sendMessageToKannel(receiverParams, subscriberDetail.getMsisdn());		
		
		return 0;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}

	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		// TODO Auto-generated method stub
		
	}

}
