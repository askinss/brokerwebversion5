package com.vasconsulting.www.interfaces.impl;

import com.vasconsulting.www.domain.*;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

public class UpdateBBTestDriveFirstYes implements Command {
	private HibernateUtility hibernateUtility;
	private SubscriberDetail subscriberDetail;
	private TransactionLog transactionlog;
	private Logger logger;
	private BbTestPromoFirstYes bbTestPromoFirstYes;
	@SuppressWarnings("unused")
	private BillingPlanObjectUtility billingPlanObject;

	public UpdateBBTestDriveFirstYes() {
		logger = Logger.getLogger(UpdateBBTestDriveFirstYes.class);
	}

	public int execute() {
		hibernateUtility = (HibernateUtility) ContextLoaderImpl
				.getBeans("hibernateUtility");
		logger.info((new StringBuilder(
				"Execute called on UpdateBBTestDriveFirstYes for subscriber with MSISDN = "))
				.append(subscriberDetail.getMsisdn()).toString());
		transactionlog = new TransactionLog();
		transactionlog.setDate_created(new GregorianCalendar());
		transactionlog.setMsisdn(subscriberDetail.getMsisdn());
		transactionlog.setShortcode(subscriberDetail.getShortCode());
		transactionlog.setDescription("Setting Subsequent Use Status to 1");
		bbTestPromoFirstYes = hibernateUtility
				.getBBPromoInitialYesStatusByMSISDN(subscriberDetail
						.getMsisdn());
		if (bbTestPromoFirstYes == null) {
			return StatusCodes.SUCCESS;
		}
		try {
			bbTestPromoFirstYes.setDate_created(new GregorianCalendar());
			bbTestPromoFirstYes.setDate_updated(new GregorianCalendar());
			bbTestPromoFirstYes.setMsisdn(subscriberDetail.getMsisdn());
			bbTestPromoFirstYes.setStatus(1);
			hibernateUtility.updateBBPromoInitialYes(bbTestPromoFirstYes);
			transactionlog.setStatus("SUCCESSFUL");
			hibernateUtility.saveTransactionlog(transactionlog);
			return StatusCodes.SUCCESS;
		} catch (Exception e) {
			return StatusCodes.OTHER_ERRORS;
		}
	}

	public void setReceiverParameters(String s) {
	}

	public int logTransaction() {
		return 0;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub

	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;

	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;

	}
}
