package com.vasconsulting.www.interfaces.impl;

import java.rmi.RemoteException;
import java.util.HashMap;

import org.apache.log4j.Logger;

import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKGProxy;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscriberInformationElement;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscriberInformationResponseElement;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.RenewalCommand;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;
import com.vasconsulting.www.utility.XMLUtility;

public class SynchronizeSubscriberBarStatusCommandImpl implements RenewalCommand {
	private TBI_KPI_PKGProxy tbiProxy = new TBI_KPI_PKGProxy();
	private SubscriberDetail subscriberDetail;
	private XMLUtility xmlUtility = new XMLUtility();
	Logger logger = Logger.getLogger(SynchronizeSubscriberBarStatusCommandImpl.class);
	
	/**
	 * This method will execute and perform service change on TABS based on the supplied
	 * receiver. The method will log at the end of the method execution the state of the 
	 * transaction run.
	 * @author Nnamdi Jibunoh
	 * @param
	 */
	public int execute() {
		
		logger.info("Execute called on SynchronizeSubscriberBarStatusCommandImpl for subscriber wit MSISDN = "+subscriberDetail.getMsisdn());
		
		GetSubscriberInformationElement subParams = new GetSubscriberInformationElement();
		subParams.setSubno(stripLeadingMsisdnPrefix(subscriberDetail.getMsisdn()));
		
		try {
			GetSubscriberInformationResponseElement responseElement = tbiProxy.getSubscriberInformation(subParams);
			
			HashMap<String, String> tabsResponse = xmlUtility.processTABSResponse(responseElement.getResult());
			
			logger.info("Call to check Subscriber Status on TABS API returned result : "+tabsResponse);
			
			if (tabsResponse.get("errorResponse") != null) return StatusCodes.OTHER_ERRORS;
			else if(tabsResponse.get("STATUS").equalsIgnoreCase("30")) 
			{
				subscriberDetail.setStatus("Active");
				return StatusCodes.SUCCESS;
			}
			else
			{
				subscriberDetail.setStatus("Deactivated");
				return StatusCodes.BARRED_SUBSCRIBER;
			}
			
		} catch (RemoteException e) {
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;
		}		
	}
	
	public int logTransaction()
	{
		return 0;
	}
	
	private String stripLeadingMsisdnPrefix(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("0")){
			return Msisdn.substring(1, Msisdn.length());
		}
		else if (Msisdn.startsWith("255")){
			return Msisdn.substring(3, Msisdn.length());
		}
		else if(Msisdn.startsWith("+255")){
			return Msisdn.substring(4, Msisdn.length());
		}
		else return Msisdn;
	}

	public void setReceiverParameters(String receiverParams)
	{
		
	}

	public void setSubscriber(SubscriberDetail subscriber)
	{
		this.subscriberDetail = subscriber;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		// TODO Auto-generated method stub
		
	}
	

}
