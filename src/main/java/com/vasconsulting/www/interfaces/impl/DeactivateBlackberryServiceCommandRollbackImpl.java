package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.TelnetServiceManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.HibernateUtilityImpl;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.RIMQueryUtil;
import com.vasconsulting.www.utility.RIMXMLResponseDetail;
import com.vasconsulting.www.utility.RIMXMLUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class DeactivateBlackberryServiceCommandRollbackImpl implements Command {
	private RIMXMLUtility rimUtility = new RIMXMLUtility();
	private int rimStatus;
	private SubscriberDetail subscriberDetail;
	private TransactionLog transactionLog;
	private HibernateUtility hibernateUtility;
	private BillingPlanObjectUtility billingPlanObject;
	private String receiverParams;
	private RIMXMLResponseDetail responseData = new RIMXMLResponseDetail();
	private String [] receiverParamsValues;
	private Logger logger = Logger.getLogger(DeactivateBlackberryServiceCommandRollbackImpl.class);
	private EmailTaskExecutor myEmailTaskExecutor;
	private TelnetServiceManager telnetManager = new TelnetServiceManager();
	private LoadAllProperties properties;
	
	public int execute() {
		/**
		 * Load the spring managed beans from the context.
		 */
				
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");	
		myEmailTaskExecutor = 
			(EmailTaskExecutor)ContextLoaderImpl.getBeans("myEmailTaskExecutor");
		properties = 
			(LoadAllProperties)ContextLoaderImpl.getBeans("loadProperties");
		
		logger.info("Execute called on DeactivateBlackberryServiceCommandRollbackImpl for subscriber with msisdn "+subscriberDetail.getMsisdn());
		
		
		transactionLog = new TransactionLog();
		transactionLog.setDate_created(new GregorianCalendar());
		transactionLog.setMsisdn(subscriberDetail.getMsisdn());
		transactionLog.setService(subscriberDetail.getServiceplan());
		transactionLog.setShortcode(subscriberDetail.getShortCode()+"_Rollback");
		transactionLog.setDescription("Deactivate BB");
		
		try{
			responseData = rimUtility.cancelSubscriptionByIMSI(subscriberDetail);
			rimStatus = new Integer(responseData.getErrorCode()).intValue();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			transactionLog.setStatus("FAILED");
			hibernateUtility.saveTransactionlog(transactionLog);
			return StatusCodes.OTHER_ERRORS;
		}
		
		if (rimStatus == 0 || (rimStatus >= 21000 && rimStatus <= 21050)) 
		{
			transactionLog.setId(UUID.randomUUID().toString());
			transactionLog.setDescription("DEACTIVATE BB");
			transactionLog.setStatus("SUCCESSFUL");
			
			logger.info("Deactivate call on "+subscriberDetail.getMsisdn()+" returned "+rimStatus+". This is okay! proceeding.");			
			rimStatus = 0;
		}
		else 
		{
			transactionLog.setDescription("DEACTIVATE BB");
			transactionLog.setStatus("FAILED");
			logger.info("Deactivate call on "+subscriberDetail.getMsisdn()+" returned "+rimStatus);
		}
		
		hibernateUtility.saveTransactionlog(transactionLog);		
		return rimStatus;
	}

	public int logTransaction()
	{
		return 0;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams; 		
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}

}
