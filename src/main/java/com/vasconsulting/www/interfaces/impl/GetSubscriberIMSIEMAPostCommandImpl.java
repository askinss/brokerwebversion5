package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.TelnetServiceManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class GetSubscriberIMSIEMAPostCommandImpl implements Command {
	private TelnetServiceManager telnetConnector = new TelnetServiceManager();
	private SubscriberDetail subscriberDetail;
	private BillingPlanObjectUtility billingPlanObject;
	private TransactionLog transactionLog;
	private HibernateUtility hibernateUtility;
	private String receiverParams;
	private Logger logger = Logger.getLogger(GetSubscriberIMSIEMAPostCommandImpl.class);
	
	@SuppressWarnings("unchecked")
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		transactionLog = new TransactionLog();
		transactionLog.setDate_created(new GregorianCalendar());		
		transactionLog.setMsisdn(subscriberDetail.getMsisdn());
		transactionLog.setShortcode(billingPlanObject.getShortCode());	
		transactionLog.setService(subscriberDetail.getServiceplan());
		transactionLog.setDescription("GET IMSI");
		
		logger.info("Execute called on GetSubscriberIMSIEMACommandImpl for subscriber with msisdn "+subscriberDetail.getMsisdn());
				
		try {
			Map<String, String> responseMap = telnetConnector.getSubscriberInformation(stripLeadingMsisdnPlus(subscriberDetail.getMsisdn()));			
			logger.info("This is imsi response "+responseMap);
			if (responseMap == null || !responseMap.containsKey("imsi"))
			{				
				transactionLog.setStatus("FAILED");
				transactionLog.setService("");
				hibernateUtility.saveTransactionlog(transactionLog);
				transactionLog.setId(UUID.randomUUID().toString());
				transactionLog.setDescription("SHORTCODE");
				hibernateUtility.saveTransactionlog(transactionLog);
				logger.info("Error retrieveing IMSI, returning status "+StatusCodes.OTHER_ERRORS_IMSI);
				return StatusCodes.OTHER_ERRORS_IMSI;
			}
			else if(!responseMap.get("imsi").equalsIgnoreCase("")) 
			{
				String subscriberImsi = responseMap.get("imsi");
				if (subscriberImsi.startsWith("64005010") && subscriberImsi.startsWith("64005012") &&
						subscriberImsi.startsWith("64005022") && subscriberImsi.startsWith("64005042") && 
						subscriberImsi.startsWith("64005052") && subscriberImsi.startsWith("64005062") && 
						subscriberImsi.startsWith("64005072") && subscriberImsi.startsWith("64005092") && 
						subscriberImsi.startsWith("64005102") && subscriberImsi.startsWith("64005093"))
					
				{
					transactionLog.setStatus("FAILED");
					transactionLog.setService("");
					hibernateUtility.saveTransactionlog(transactionLog);
					transactionLog.setId(UUID.randomUUID().toString());
					transactionLog.setDescription("SHORTCODE");
					hibernateUtility.saveTransactionlog(transactionLog);
					return StatusCodes.PREPAID_SUBSCRIBER;
				}
				
				transactionLog.setStatus("SUCCESSFUL");
				transactionLog.setService(subscriberImsi);
				hibernateUtility.saveTransactionlog(transactionLog);				
				subscriberDetail.setImsi(subscriberImsi);
				logger.info("IMSI value is "+subscriberImsi+" for subscriber "+subscriberDetail.getMsisdn());
				return StatusCodes.SUCCESS;
			}
			else 
			{
				transactionLog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionLog);
				transactionLog.setId(UUID.randomUUID().toString());
				transactionLog.setDescription("SHORTCODE");
				hibernateUtility.saveTransactionlog(transactionLog);
				logger.info("Error retrieveing IMSI, returning status "+StatusCodes.IMSI_NOT_FOUND);
				return StatusCodes.IMSI_NOT_FOUND;
			}
			
		} catch (Exception e) {
			transactionLog.setStatus("FAILED");
			hibernateUtility.saveTransactionlog(transactionLog);
			transactionLog.setId(UUID.randomUUID().toString());
			transactionLog.setDescription("SHORTCODE");
			hibernateUtility.saveTransactionlog(transactionLog);
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;
		}		
	}
	
	private String stripLeadingMsisdnPlus(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("0")){
			return "255"+Msisdn.substring(1, Msisdn.length());
		}
		else if(Msisdn.startsWith("+255")){
			return Msisdn.substring(1, Msisdn.length());
		}
		else return Msisdn;
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}

	public int rollBack()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}
	
}
