package com.vasconsulting.www.interfaces.impl;

import org.apache.log4j.Logger;

import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.Ttspromo;

public class CheckSubscriberTTSPromoUsageCountCommandImpl implements Command  {
	private HibernateUtility hibernateUtility;
	private Ttspromo ttsPromo;
	private Logger logger = Logger.getLogger(CheckSubscriberTTSPromoUsageCountCommandImpl.class);
	private SubscriberDetail subscriberDetail;
	private BillingPlanObjectUtility billingPlanObject;

	
	public int execute() {
		hibernateUtility = 
				(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		logger.info("Execute called on CheckSubscriberTTSPromoUsageCountCommandImpl for" +
				" subscriber with MSISDN = "+subscriberDetail.getMsisdn());
		try{
			ttsPromo = hibernateUtility.getTTSPromoDetails(subscriberDetail.getMsisdn());
			if (ttsPromo.getUsage_count() <= 6){
				return StatusCodes.SUCCESS;
			}else{
				return StatusCodes.SUBSCRIBER_NOT_ALLOWED;
			}
		}catch(Exception e){
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;
		}

	}

	
	public void setReceiverParameters(String receiverParams) {
		// TODO Auto-generated method stub

	}

	
	public int logTransaction() {
		// TODO Auto-generated method stub
		return 0;
	}


	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}


	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}

}
