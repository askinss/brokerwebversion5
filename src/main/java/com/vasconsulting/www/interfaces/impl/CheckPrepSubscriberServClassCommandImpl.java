/**
 * This class is used to check the status of the subscriber on the platform
 * @author nnamdi Jibunoh
 */
package com.vasconsulting.www.interfaces.impl;

import java.util.Map;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.UCIPServiceRequestManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.StatusCodes;

public class CheckPrepSubscriberServClassCommandImpl implements Command {
	
	private SubscriberDetail subscriberDetail;
	private String receiverParams;
	private UCIPServiceRequestManager ucipConnector = new UCIPServiceRequestManager();
	Logger logger = Logger.getLogger(CheckPrepSubscriberServClassCommandImpl.class);
	private boolean isServiceClassAllowed = false;
	private LoadAllProperties properties;
	private BillingPlanObjectUtility billingPlanObject;
		
	/**
	 * This method will execute and perform service class check on all the services that the subscriber has subscribed for on the platform.
	 * @author Nnamdi Jibunoh
	 * @param
	 */
	@SuppressWarnings("unchecked")
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		properties = 
			(LoadAllProperties)ContextLoaderImpl.getBeans("loadProperties");
		
		logger.info("Execute called on CheckPrepSubscriberServClassCommandImpl for subscriber with MSISDN = "+subscriberDetail.getMsisdn());
			
		
		Map<String, String> ucipResponse = null;
		try {
			ucipResponse = ucipConnector.getSubscriberDetails(subscriberDetail.getMsisdn());
		} catch (Exception e) {
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;
		}
		
		if (ucipResponse == null) return StatusCodes.OTHER_ERRORS;
		String currentServiceClass = ucipResponse.get("serviceClassCurrent");
		
		String[] receiverParamsValues = properties.getProperty("allowedserviceclasses").split(",");//serviceplanid,validity
		
		for (String temp : receiverParamsValues)
		{
			if (temp.equalsIgnoreCase(currentServiceClass.trim()))
			{
				isServiceClassAllowed = true;
			}
		}
					
		if (isServiceClassAllowed)
		{
			return StatusCodes.SUCCESS;
		}
		else
		{
			logger.info("Subscriber current service class is "+currentServiceClass+", this is not in the allowed " +
					"list, aborting.");
			return StatusCodes.SUBSCRIBER_SERVICECLASS_NOT_ALLOWED;
		}		
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
		
	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}
		
}
