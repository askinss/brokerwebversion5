package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.TelnetServiceManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.interfaces.RenewalCommand;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.EmailTaskExecutor;
import com.vasconsulting.www.utility.HibernateUtilityImpl;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.RIMXMLResponseDetail;
import com.vasconsulting.www.utility.RIMXMLUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class RenewSubscriberOnBBPostCommandImpl implements RenewalCommand
{
	private String receiverParams;
	private SubscriberDetail subscriberDetail;
	private HibernateUtility hibernateUtility = (HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
	EmailTaskExecutor emailExecutor = (EmailTaskExecutor)ContextLoaderImpl.getBeans("myEmailTaskExecutor");
	Logger logger = Logger.getLogger(RenewSubscriberOnBBPostCommandImpl.class);
	//private int actionResponse;
	//private XMLUtility xmlUtility = new XMLUtility();
	//private ChangeserviceResponseElement responseElement;
	//private RIMQueryUtil rimUtility = new RIMQueryUtil();
	private RIMXMLUtility rimUtility = new RIMXMLUtility();
	private RIMXMLResponseDetail rimStatus;
	private String[] receiverParamValues;
	private String errorMessage = "";
	private LoadAllProperties properties = (LoadAllProperties)ContextLoaderImpl.getBeans("loadProperties");
	private TelnetServiceManager telnetManager = new TelnetServiceManager();
	private TransactionLog transaction;

	/**
	 * This method is used to get the last day of the month.
	 * @param noOfDays
	 * @return
	 *//*
	private GregorianCalendar getLastDateOfMonth(){
		GregorianCalendar calendar  = new GregorianCalendar();
		int dayOfMonth  = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);

		int daysToAdd = dayOfMonth - calendar.get(GregorianCalendar.DAY_OF_MONTH);

		calendar.add(GregorianCalendar.DAY_OF_MONTH, daysToAdd);	
		return calendar;
	}*/

	/**
	 * This method is used to calculate the next_subscription_date for a subscriber based on the supplied service tye value
	 * @param serviceType
	 * @return
	 */
	private GregorianCalendar getNextSubscriptionDate(int serviceType)
	{
		GregorianCalendar calendar  = new GregorianCalendar();
		calendar.add(GregorianCalendar.DAY_OF_MONTH, serviceType);

		return calendar;
	}

	public int execute()
	{
		logger.info("Execute called on RenewSubscriberOnBBPostCommandImpl for subscriber with MSISDN = "+subscriberDetail.getMsisdn());

		//productType:SEP:cost:SEP:userID
		receiverParamValues = receiverParams.split(":SEP:");

		int isAutorenewalOn = subscriberDetail.getAutoRenew();		

		if(isAutorenewalOn == 0)
		{
			logger.info("["+subscriberDetail.getMsisdn()+"] has disabled auto renewal, deactivating active service on RIM and removing APN");

			//deactivate sub
			subscriberDetail.setStatus("Deactivated");
			subscriberDetail.setPostpaidSubscriber(1);
			subscriberDetail.setPrepaidSubscriber(0);
			rimStatus = rimUtility.cancelSubscriptionByIMSI(subscriberDetail);

			if (rimStatus.getErrorCode().equalsIgnoreCase("0") || ((new Integer(rimStatus.getErrorCode()) >= 21000 && 
					(new Integer(rimStatus.getErrorCode()) <= 25000)))) 
			{
				try {
					telnetManager.removeBlackberryAPNToSubscriber(subscriberDetail.getMsisdn(), properties.getProperty("APNID.Blackberry"));
				} catch (Exception e) {
					errorMessage = subscriberDetail.getMsisdn()+" was deactivated on RIM because of stop auto renew, but " +
							"the APN was not removed successfully for "+subscriberDetail.getMsisdn()+". Please rectify manually.";
					emailExecutor.sendQueuedEmails(errorMessage);

					e.printStackTrace();
				}

				int status = -1;

				try
				{
					status = hibernateUtility.updateSubscriberDetail(subscriberDetail);
				}
				catch(Exception ex)
				{
					errorMessage = "Subscriber data did not update for subscriber with MSISDN "+subscriberDetail.getMsisdn() +
							" in the daily Postpaid renewal job. Please rectify the subscriber data manually.";
					logger.error("Error: Subscriber data did not update in database."+subscriberDetail.getMsisdn());
					emailExecutor.sendQueuedEmails(errorMessage);
					ex.printStackTrace();
				}
				if (status == 0)
				{
					transaction = new TransactionLog();
					transaction.setDate_created(new GregorianCalendar());
					transaction.setDescription("DEACTIVATION");
					transaction.setMsisdn(subscriberDetail.getMsisdn());
					transaction.setShortcode("SCHEDULER:NO_AUTO_RENEW");
					hibernateUtility.saveTransactionlog(transaction);

					logger.info("["+subscriberDetail.getMsisdn()+"] has auto renewal deactivated and has been successfully deactivated.");
					return StatusCodes.SUBSCRIBER_DISABLED_AUTORENEWAL;
				}
				else
				{
					errorMessage = "Subscriber data did not update for subscriber with MSISDN "+subscriberDetail.getMsisdn() +
							" in the daily Postpaid renewal job. Please rectify the subscriber data manually.";
					logger.error("Error: Subscriber data did not update in database."+subscriberDetail.getMsisdn());
					emailExecutor.sendQueuedEmails(errorMessage);
					return StatusCodes.OTHER_ERROR_RENEWAL;
				}
			}	
			else {
				errorMessage = "Subscriber deactivation was not successfully on RIM for subscriber with MSISDN "+subscriberDetail.getMsisdn() +
						" in the hourly renewal job. The subscriber has auto renewal disabled. Please rectify the subscriber data manually.";
				logger.error("Error: Subscriber data did not update in database."+subscriberDetail.getMsisdn());
				emailExecutor.sendQueuedEmails(errorMessage);
				return StatusCodes.OTHER_ERROR_RENEWAL;
			}		
		}
		else
		{
			logger.info("["+subscriberDetail.getMsisdn()+"] has enabled auto renewal, adding Homisco bill item and activating service on " +
					"RIM");
			rimStatus = rimUtility.activateRIMService(subscriberDetail);

			if (rimStatus.getErrorCode().equalsIgnoreCase("0") || ((new Integer(rimStatus.getErrorCode()) >= 21000 && 
					(new Integer(rimStatus.getErrorCode()) <= 25000)))) 
			{				
				logger.info("stripLeadingMsisdnPlus(subscriberDetail.getMsisdn())="+stripLeadingMsisdnPlus(subscriberDetail.getMsisdn()));
				logger.info("receiverParamValues[0] = "+receiverParamValues[0]);
				logger.info("receiverParamValues[1] = "+receiverParamValues[1]);
				logger.info("receiverParamValues[2] = "+receiverParamValues[2]);
				int status = 0;
				try{
					status = hibernateUtility.createPostSubscriberHomisco(stripLeadingMsisdnPlus(subscriberDetail.getMsisdn()), 
							receiverParamValues[0], receiverParamValues[1], receiverParamValues[2]);
				}catch(Exception e){
					status = -100;
					e.printStackTrace();
				}


				if (status > 0)
				{
					logger.info("["+subscriberDetail.getMsisdn()+"] response from adding Bill item on Homisco is = "+status);
					GregorianCalendar calendar = new GregorianCalendar();

					subscriberDetail.setStatus("Active");
					subscriberDetail.setLast_subscription_date(calendar);
					subscriberDetail.setNext_subscription_date(getNextSubscriptionDate(subscriberDetail.getServicetype()));

					try
					{
						status = hibernateUtility.updateSubscriberDetail(subscriberDetail);
						logger.info("["+subscriberDetail.getMsisdn()+"] response from DB update is = "+status);
					}
					catch(Exception ex)
					{
						ex.printStackTrace();
					}

					if (status == 0)
					{
						transaction = new TransactionLog();
						transaction.setDate_created(calendar);
						transaction.setDescription("POSTPAID_RENEWAL");
						transaction.setMsisdn(subscriberDetail.getMsisdn());
						transaction.setShortcode("SCHEDULER");
						hibernateUtility.saveTransactionlog(transaction);

						return StatusCodes.SUCCESS;
					}
					else
					{
						errorMessage = "*************CRITICAL****************\n" +
								"Renewal data did not update for subscriber with MSISDN "+subscriberDetail.getMsisdn() +
								" in the daily Postpaid renewal job. Please rectify the subscriber data manually. Everything was successful for subscriber" +
								"except saving to database so please treat as soon as possible.";
						logger.error("Error: Subscriber data did not update in database."+subscriberDetail.getMsisdn());
						emailExecutor.sendQueuedEmails(errorMessage);
						return StatusCodes.OTHER_ERROR_RENEWAL;
					}
				}
				else
				{
					rimStatus = rimUtility.cancelSubscriptionByIMSI(subscriberDetail);					

					errorMessage = "Renewal data did not update for subscriber with MSISDN "+subscriberDetail.getMsisdn() +
							" in the daily Postpaid renewal job. This error was specifically from the attempt to add the bill item to Homisco, it" +
							"failed with error code "+status;
					logger.error("Error: Subscriber data did add bill item in Homisco."+subscriberDetail.getMsisdn());
					emailExecutor.sendQueuedEmails(errorMessage);
					return StatusCodes.OTHER_ERROR_RENEWAL;
				}				
			}		
			else
			{

				errorMessage = "Renewal data did not update for subscriber with MSISDN "+subscriberDetail.getMsisdn() +
						" in the daily Postpaid renewal job. This error was specifically from the attempt to add the bill item to Homisco, it" +
						"failed with error code "+rimStatus;

				rimStatus = rimUtility.cancelSubscriptionByIMSI(subscriberDetail);

				logger.error("Error: Subscriber data did add bill item in Homisco."+subscriberDetail.getMsisdn());
				emailExecutor.sendQueuedEmails(errorMessage);
				return StatusCodes.OTHER_ERROR_RENEWAL;
			}
		}			
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}

	public int logTransaction()
	{
		return 0;
	}

	public void setSubscriber(SubscriberDetail subscriber)
	{
		this.subscriberDetail = subscriber;
	}

	private String stripLeadingMsisdnPlus(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("0")){
			return Msisdn;
		}
		else if(Msisdn.startsWith("+260")){
			return Msisdn.substring(3, Msisdn.length());
		}
		else if(Msisdn.startsWith("260")){
			return Msisdn.substring(2, Msisdn.length());
		}
		else return Msisdn;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		// TODO Auto-generated method stub
		
	}

}