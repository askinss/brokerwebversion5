package com.vasconsulting.www.interfaces.impl;



import java.util.GregorianCalendar;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.EulogyService;
import com.vasconsulting.www.utility.StatusCodes;

public class CreateSubscriberEulogyServiceCommandImpl implements Command {
	private BillingPlanObjectUtility billingPlanObject;
	private HibernateUtility hibernateUtility;
	private SubscriberDetail subscriberDetail;
	private Logger logger = Logger.getLogger(CreateSubscriberEulogyServiceCommandImpl.class);
	private TransactionLog transactionlog;
	@Autowired
	private EulogyService eulogyService;
	protected String receiverParams;
	protected String flag;
	
		
	public int execute() {
		billingPlanObject = (billingPlanObject == null) ? 
				(BillingPlanObjectUtility)ContextLoaderImpl.getBeans("billingPlanObject") : billingPlanObject;
		hibernateUtility = (hibernateUtility == null) ?
				(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility") : hibernateUtility;
		transactionlog = (transactionlog == null) ? (new TransactionLog()) : transactionlog;
		eulogyService = (eulogyService == null) ? (new EulogyService()) : eulogyService;
		flag = (flag == null) ? ("A") : flag;
		
		transactionlog.setDate_created(new GregorianCalendar());		
		transactionlog.setMsisdn(subscriberDetail.getMsisdn());
		transactionlog.setShortcode(billingPlanObject.getShortCode());	
		transactionlog.setService(subscriberDetail.getServiceplan());
		transactionlog.setDescription("CREATE EULOGY SERVICE");
		transactionlog.setStatus("FAILED");
		String memberId = receiverParams.split(",")[0];
		String componentId = receiverParams.split(",")[1];
		logger.info("MemberID is: "+memberId+" ComponentID is: "+componentId);
		int status = StatusCodes.OTHER_ERRORS;
		logger.info("Execute called on CreateSubscriberEulogyServiceCommandImpl for subscriber with msisdn "+subscriberDetail.getMsisdn());
		try {
			String eulogyResponse = eulogyService.updateEulogySubDetails(subscriberDetail.getMsisdn(), flag, memberId, componentId).get(0);
			logger.info("Response from Eulogy is '"+ eulogyResponse + "'");
			if (eulogyResponse.equals("Y") || eulogyResponse.equals("K")){
				status = StatusCodes.SUCCESS;
				transactionlog.setStatus("SUCCESS");
			} else if (eulogyResponse.equals("R")){
				logger.info("Return Code from Eulogy is 'R' meaning: Duplicate MSISDN present");
				status = StatusCodes.EULOGY_DUPLICATE_MSISDN;
			} else if (eulogyResponse.equals("Z")){
				logger.info("Return Code from Eulogy is 'Z' meaning: Component ID is incorrect");
				status = StatusCodes.EULOGY_WRONG_COMPONENT_ID;
			} else if (eulogyResponse.equals("L")){
				logger.info("Return Code from Eulogy is 'L' meaning: Component ID is or member is Wrong");
				status = StatusCodes.EULOGY_WRONG_COMPONENT_ID_OR_MEMBER_ID;
			} else if (eulogyResponse.equals("P")){
				logger.info("Return Code from Eulogy is 'P' meaning: Flag should be A/D");
				status = StatusCodes.EULOGY_WRONG_FLAG;
			} else if (eulogyResponse.equals("T")){
				logger.info("Return Code from Eulogy is 'T' meaning: Component ID sent is null");
				status = StatusCodes.EULOGY_NULL_COMPONENT_ID;
			} else if (eulogyResponse.equals("X")){
				logger.info("Return Code from Eulogy is 'X' meaning: MSISDN does not exist");
				status = StatusCodes.EULOGY_MSISDN_DOES_NOT_EXIST;
			} else if (eulogyResponse.equals("N")){
				logger.info("Return Code from Eulogy is 'N' meaning: MSISDN Successfully detached");
				status = StatusCodes.EULOGY_MSISDN_SUCCESSFULLY_DETACHED;
			}
		} catch(Exception e){
			e.printStackTrace();
		} finally{
			hibernateUtility.saveTransactionlog(transactionlog);
		}
		return status;
	}

	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams){
		this.receiverParams = receiverParams;
	}

	public HibernateUtility getHibernateUtility() {
		return hibernateUtility;
	}

	public void setHibernateUtility(HibernateUtility hibernateUtility) {
		this.hibernateUtility = hibernateUtility;
	}

	public SubscriberDetail getSubscriberDetail() {
		return subscriberDetail;
	}

	public void setSubscriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public TransactionLog getTransactionlog() {
		return transactionlog;
	}

	public void setTransactionlog(TransactionLog transactionlog) {
		this.transactionlog = transactionlog;
	}

	public EulogyService getEulogyService() {
		return eulogyService;
	}

	public void setEulogyService(EulogyService eulogyService) {
		this.eulogyService = eulogyService;
	}

	public BillingPlanObjectUtility getBillingPlanObject() {
		return billingPlanObject;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}
	
}
