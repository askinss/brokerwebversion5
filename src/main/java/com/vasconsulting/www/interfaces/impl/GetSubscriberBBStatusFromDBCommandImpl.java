/**
 * This class is used to check the status of the subscriber on the platform
 * @author nnamdi Jibunoh
 */
package com.vasconsulting.www.interfaces.impl;

import java.util.Collection;
import java.util.Formatter;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.SendSmsToKannelService;
import com.vasconsulting.www.utility.StatusCodes;

public class GetSubscriberBBStatusFromDBCommandImpl implements Command {
	
	private SubscriberDetail subscriberDetail;
	Collection<SubscriberDetail> subscriberDetailDB;
	private HibernateUtility hibernateUtility;
	private SendSmsToKannelService smsService;
	private String receiverParams;
	private Formatter format = new Formatter();
	Logger logger = Logger.getLogger(GetSubscriberBBStatusFromDBCommandImpl.class);
	
	private LoadAllProperties properties;
	private BillingPlanObjectUtility billingPlanObject;
		
	/**
	 * This method will execute and perform status check on all the services that the subscriber has subscribed for on the platform.
	 * @author Nnamdi Jibunoh
	 * @param
	 */
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		smsService = 
			(SendSmsToKannelService)ContextLoaderImpl.getBeans("smsService");		
		properties = 
			(LoadAllProperties)ContextLoaderImpl.getBeans("loadProperties");
		
		logger.info("Execute called on GetSubscriberBBStatusFromDBCommandImpl for subscriber with MSISDN = "+subscriberDetail.getMsisdn());
				
		//subscriberDetail.setServicetype(0);
		//subscriberDetailDB = hibernateUtility.getSubscriberInformation(subscriberDetail);
		SubscriberDetail subDetail = hibernateUtility.getSubscriberInformation("msisdn", subscriberDetail.getMsisdn());
		
		String[] receiverParamsValues = receiverParams.split(",");//serviceplanid,validity
		
		if (receiverParamsValues.length != 2)
			return StatusCodes.WRONG_STATUS_FORMAT;
		
		/*if (subscriberDetailDB != null && subscriberDetailDB.size() > 0)
		{
			for (Iterator<SubscriberDetail> iterator = subscriberDetailDB.iterator(); iterator.hasNext();) {
				SubscriberDetail subscriber = iterator.next();
				
				if (receiverParamsValues[0].trim().equalsIgnoreCase(subscriber.getServiceplan()))
				{
					logger.info("Value of database returned subscriber = "+subscriberDetailDB);
					
					smsService.sendMessageToKannel(
							format.format(properties.getProperty(receiverParamsValues[0].replaceAll(" ", "")+"statusmessage"),
									subscriber.getStatus(),
							receiverParamsValues[1],
							convertValidityToDay(subscriberDetail.getServicetype()),
							subscriber.getNext_subscription_date())+""
							,subscriber.getMsisdn());
				}
			}
		}*/
		if(subDetail != null)
		{
			if (receiverParamsValues[0].trim().equalsIgnoreCase(subDetail.getServiceplan()))
			{
				logger.info("Value of database returned subscriber = "+subDetail);
				String messageToSend = properties.getProperty(receiverParamsValues[0].replaceAll(" ", "")+"statusmessage");
				
				messageToSend = String.format(messageToSend, subDetail.getStatus(), 
						convertServiceToDisplay(subDetail.getServiceplan())+" "+receiverParamsValues[1],
						convertValidityToDay(subDetail.getServicetype()), subDetail.getNext_subscription_date());
				
				logger.info("Sending a status of "+messageToSend+" to MSISDN = "+subDetail.getMsisdn());
				smsService.sendMessageToKannel(messageToSend, subDetail.getMsisdn());
			}
		}
		else 
		{
			logger.info("Subscriber "+subscriberDetail.getMsisdn()+" was not found in the database, so subscriber " +
					"cannot be using any of the services");
			
			
			return StatusCodes.SUBSCRIBER_NOT_IN_DB;
		}
			
		return 0;			
			
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
		
	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}	
	
	private String convertValidityToDay(int serviceType){
		return (serviceType == 1) ? "daily" : (serviceType == 7) ? "weekly" : "monthly";
	}
	
	private String convertServiceToDisplay(String servicePlan){
		if (servicePlan.equalsIgnoreCase("Prosumer Prepaid")) return "Internet";
		else if (servicePlan.equalsIgnoreCase("Prosumer Prepaid B")) return "Internet";
		else if (servicePlan.equalsIgnoreCase("Prepaid Prosumer B")) return "Internet";
		else if (servicePlan.equalsIgnoreCase("Prepaid Prosumer")) return "Internet";
		else if (servicePlan.equalsIgnoreCase("Enterprise Plus B")) return "Enterprise";
		else if (servicePlan.equalsIgnoreCase("Enterprise Plus")) return "Enterprise";
		else if (servicePlan.equalsIgnoreCase("Prepaid BIS Social")) return "Social";
		else if (servicePlan.equalsIgnoreCase("Prepaid Social Plan")) return "Social";
		else if (servicePlan.equalsIgnoreCase("Prepaid Complete")) return "Complete";

		else  return "Complete";
	}

	public int rollBack()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}

}
