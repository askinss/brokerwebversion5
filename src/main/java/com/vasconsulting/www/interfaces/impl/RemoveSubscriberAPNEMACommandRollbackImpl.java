package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.TelnetServiceManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.StatusCodes;

public class RemoveSubscriberAPNEMACommandRollbackImpl implements Command
{
	private TelnetServiceManager telnetConnector = new TelnetServiceManager();
	private SubscriberDetail subscriberDetail;
	private BillingPlanObjectUtility billingPlanObject;
	private TransactionLog transactionLog;
	private HibernateUtility hibernateUtility;
	private LoadAllProperties properties = (LoadAllProperties) ContextLoaderImpl
			.getBeans("loadProperties");
	private Logger logger = Logger
			.getLogger(RemoveSubscriberAPNEMACommandRollbackImpl.class);

	@SuppressWarnings("unchecked")
	public int execute()
	{
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = (HibernateUtility) ContextLoaderImpl
				.getBeans("hibernateUtility");
		transactionLog = new TransactionLog();
		transactionLog.setDate_created(new GregorianCalendar());
		transactionLog.setMsisdn(subscriberDetail.getMsisdn());
		transactionLog.setShortcode(subscriberDetail.getShortCode()+"_Rollback");
		transactionLog.setService(subscriberDetail.getServiceplan());
		transactionLog.setDescription("REMOVE APN");

		logger.info("Execute called on RemoveSubscriberAPNEMACommandRollbackImpl for subscriber with msisdn "
				+ subscriberDetail.getMsisdn());

		try {
			String responseMap = telnetConnector.removeBlackberryAPNToSubscriber(
					subscriberDetail.getMsisdn(),
					properties.getProperty("APNID.Blackberry"));			
			if (!responseMap.equalsIgnoreCase("0")) {
				transactionLog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionLog);
				transactionLog.setId(UUID.randomUUID().toString());
				transactionLog.setDescription("SHORTCODE");
				hibernateUtility.saveTransactionlog(transactionLog);
				logger.info("Error Removing APN, returning status "
						+ StatusCodes.OTHER_ERRORS_IMSI);
				return StatusCodes.OTHER_ERRORS_IMSI;
			} else if (responseMap.equalsIgnoreCase("0")) {

				transactionLog.setStatus("SUCCESSFUL");
				hibernateUtility.saveTransactionlog(transactionLog);

				logger.info("Call to Remove APN returned value " + responseMap
						+ " for subscriber " + subscriberDetail.getMsisdn());
				return StatusCodes.SUCCESS;
			} else {
				transactionLog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionLog);
				transactionLog.setId(UUID.randomUUID().toString());
				transactionLog.setDescription("SHORTCODE");
				hibernateUtility.saveTransactionlog(transactionLog);
				logger.info("Error retrieveing IMSI, returning status "
						+ StatusCodes.IMSI_NOT_FOUND);
				return StatusCodes.IMSI_NOT_FOUND;
			}

		} catch (Exception e) {
			e.printStackTrace();
			transactionLog.setStatus("FAILED");
			hibernateUtility.saveTransactionlog(transactionLog);
			transactionLog.setId(UUID.randomUUID().toString());
			transactionLog.setDescription("SHORTCODE");
			hibernateUtility.saveTransactionlog(transactionLog);
			return StatusCodes.OTHER_ERRORS;
		}
	}

	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams){
	}

	public int rollBack()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}

}
