/**
 * This class is used to check the type of subscriber. If the subscriber is a POSTPAID or PREPAID subscriber
 * It returns 0 if subscriber is postpaid and 500 if prepaid.
 * @author nnamdi Jibunoh
 */
package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.ReconcilDetail;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class SychronizeSubscriberDataToDBSimSwapCommandImpl implements Command {

	private ReconcilDetail reconcilDetail;
	private SubscriberDetail subscriberDetail;
	private HibernateUtility hibernateUtility;
	private TransactionLog transactionLog = new TransactionLog();
	Logger logger = Logger
			.getLogger(SychronizeSubscriberDataToDBSimSwapCommandImpl.class);


	public int execute() {

		hibernateUtility = 
				(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");		

		logger.info("Execute called on SychronizeSubscriberDataToDBCommandImpl for subscriber with MSISDN = "+subscriberDetail.getMsisdn());

		SubscriberDetail subscriberDetailLocal = new SubscriberDetail();
		subscriberDetailLocal.setMsisdn(subscriberDetail.getMsisdn());
		subscriberDetailLocal.setImsi(reconcilDetail.getImsi());
		subscriberDetailLocal.setServiceplan(subscriberDetail.getServiceplan());
		subscriberDetailLocal.setFirstname(subscriberDetail.getFirstname());
		subscriberDetailLocal.setLastname(subscriberDetail.getLastname());
		subscriberDetailLocal.setMiddlename(subscriberDetail.getMiddlename());
		subscriberDetailLocal.setEmail(subscriberDetail.getEmail());
		subscriberDetailLocal.setPin(subscriberDetail.getPin());
		subscriberDetailLocal.setImei(subscriberDetail.getImei());
		subscriberDetailLocal.setShortCode(subscriberDetail.getShortCode());
		subscriberDetailLocal.setPrepaidSubscriber(subscriberDetail.getPrepaidSubscriber());
		subscriberDetailLocal.setPostpaidSubscriber(subscriberDetail.getPrepaidSubscriber());
		subscriberDetailLocal.setAutoRenew(subscriberDetail.getAutoRenew());
		subscriberDetailLocal.setDate_created(subscriberDetail.getDate_created());
		subscriberDetailLocal.setLast_subscription_date(subscriberDetail.getLast_subscription_date());
		subscriberDetailLocal.setNext_subscription_date(subscriberDetail.getNext_subscription_date());
		subscriberDetailLocal.setStatus("Active");
		subscriberDetailLocal.setId(subscriberDetail.getId());
		transactionLog.setId(UUID.randomUUID().toString());
		transactionLog.setMsisdn(subscriberDetail.getMsisdn());
		transactionLog.setDate_created(new GregorianCalendar());
		transactionLog.setService("SYNC SIM SWAP SUBSCRIBER");
		transactionLog.setShortcode(subscriberDetail.getShortCode());
		transactionLog.setStatus("FAILED");
		try{
			if (hibernateUtility.updateSubscriberDetail(subscriberDetailLocal) == 0){
				transactionLog.setStatus("SUCCESSFUL");
				hibernateUtility.saveTransactionlog(transactionLog);
				return StatusCodes.SUCCESS;
			}
			else{
				hibernateUtility.saveTransactionlog(transactionLog);
				return StatusCodes.OTHER_ERRORS;
			}
		}catch(Exception e){
			e.printStackTrace();
			hibernateUtility.saveTransactionlog(transactionLog);
			return StatusCodes.OTHER_ERRORS;
		}

	}

	public int logTransaction() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams) {
		// TODO Auto-generated method stub

	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		// TODO Auto-generated method stub
		
	}

}
