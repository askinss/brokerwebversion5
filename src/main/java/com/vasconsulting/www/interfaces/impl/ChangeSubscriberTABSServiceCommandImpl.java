package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.interfaces.TabsInterface;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class ChangeSubscriberTABSServiceCommandImpl implements Command {
	private HibernateUtility hibernateUtility;
	private SubscriberDetail subscriberDetail;
	private Logger logger = Logger.getLogger(ChangeSubscriberTABSServiceCommandImpl.class);
	private String []receiverParams;
	private String receiverParam;
	private TransactionLog transactionlog;
	private TabsInterface tabs;
	private BillingPlanObjectUtility billingPlanObject;
	
		
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		tabs = (TabsInterface)ContextLoaderImpl.getBeans("tabs");

		
		logger.info("Execute called on ChangeSubscriberTABSServiceCommandImpl for subscriber with MSISDN = "+subscriberDetail.getMsisdn());
		
		String responseCode = "";
		transactionlog = new TransactionLog();
		transactionlog.setDate_created(new GregorianCalendar());
		transactionlog.setMsisdn(subscriberDetail.getMsisdn());
		transactionlog.setDescription("Change TABS Service");
		transactionlog.setShortcode(subscriberDetail.getShortCode());
		
		try {
			responseCode = tabs.changeService(subscriberDetail.getMsisdn(),receiverParam);
			logger.info("Response from TABS = "+responseCode);
			
			if ((responseCode != null && responseCode.equalsIgnoreCase("0")) || 
					(responseCode != null && responseCode.equalsIgnoreCase("3172"))){				
				transactionlog.setService(receiverParam);
				transactionlog.setStatus("SUCCESSFUL");				
				hibernateUtility.saveTransactionlog(transactionlog);				
				logger.info("ChangeSubscriberTABSServiceCommandImpl for subscriber "+subscriberDetail.getMsisdn()+" returned with code "+responseCode);
				
				return StatusCodes.SUCCESS;
			}
			else{
				transactionlog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionlog);
				transactionlog.setId(UUID.randomUUID().toString());
				transactionlog.setDescription("SHORTCODE");
				hibernateUtility.saveTransactionlog(transactionlog);
				return StatusCodes.OTHER_ERRORS_CHANGE_SERVICE;
			}			
		} catch (Exception e) {
			transactionlog.setStatus("FAILED");
			hibernateUtility.saveTransactionlog(transactionlog);
			transactionlog.setId(UUID.randomUUID().toString());
			transactionlog.setDescription("SHORTCODE");
			hibernateUtility.saveTransactionlog(transactionlog);			
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS_CHANGE_SERVICE;
		}
	}

	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParam = receiverParams;
	}
	
	private String stripLeadingMsisdnPrefix(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("0")){
			return Msisdn.substring(1, Msisdn.length());
		}
		else if (Msisdn.startsWith("255")){
			return Msisdn.substring(3, Msisdn.length());
		}
		else if(Msisdn.startsWith("+255")){
			return Msisdn.substring(4, Msisdn.length());
		}
		else return Msisdn;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;
	}

}
