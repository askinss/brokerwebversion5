package com.vasconsulting.www.interfaces.impl;

import java.rmi.RemoteException;
import java.util.HashMap;

import org.apache.log4j.Logger;

import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.TBI_KPI_PKGProxy;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscriberInformationElement;
import ws.its.tabs.webservices.TBI_KPI_PKG.TBI_KPI_PKG_wsdl.types.GetSubscriberInformationResponseElement;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;


public class CheckSubscriberStatusCommandImpl implements Command {
	private SubscriberDetail subscriberDetail;
	private HibernateUtility hibernateUtility;
	Logger logger = Logger.getLogger(CheckSubscriberStatusCommandImpl.class);
	private BillingPlanObjectUtility billingPlanObject;
	
	/**
	 * This method will execute and perform service change on TABS based on the supplied
	 * receiver. The method will log at the end of the method execution the state of the 
	 * transaction run.
	 * @author Nnamdi Jibunoh
	 * @param
	 */
	public int execute() {
		/**
		 * Retrieve the Spring managed beans from the container
		 */
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		
		logger.info("Execute called on CheckSubscriberStatusCommandImpl for subscriber wit MSISDN = "+subscriberDetail.getMsisdn());
			
		try {
			
			SubscriberDetail subscriber = hibernateUtility.getSubscriberInformation("msisdn", subscriberDetail.getMsisdn());
			
			logger.info("Call to check Subscriber Status database returned result : "+subscriber);
			
			if (subscriber == null) return StatusCodes.SUCCESS;
			else //if(!subscriber.getId().equalsIgnoreCase("")) 
			{
				if (subscriber.getStatus().equalsIgnoreCase("Active"))
				{
					logger.info("Subscriber has an Active subscription, requested operation is not allowed in this state. Aborting");
					return StatusCodes.SUBCRIBER_IS_ALREADY_IN_ACTIVE_STATE;
				}
				return StatusCodes.SUCCESS;
			}			
			
		} catch (Exception e) {
			e.printStackTrace();
			return StatusCodes.OTHER_ERRORS;
		}		
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	public void setReceiverParameters(String receiverParams)
	{
		// TODO Auto-generated method stub
		
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}
	

}
