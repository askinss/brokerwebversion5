package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.celtel.databundle.service.impl.TelnetServiceManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.StatusCodes;

public class GetSubscriberIMSIEMACommandImpl implements Command {
	@Autowired
	private TelnetServiceManager telnetConnector;
	@Autowired
	protected SubscriberDetail subscriberDetail;
	@Autowired
	private BillingPlanObjectUtility billingPlanObject;
	private TransactionLog transactionLog;
	@Autowired
	private HibernateUtility hibernateUtility;
	protected String receiverParams;
	protected Logger logger = Logger.getLogger(GetSubscriberIMSIEMACommandImpl.class);
	@Autowired
	private LoadAllProperties properties = new LoadAllProperties();


	@SuppressWarnings("unchecked")
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		hibernateUtility = (hibernateUtility == null) ?
				(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility") : hibernateUtility;
		transactionLog = (transactionLog == null) ? (new TransactionLog()) : transactionLog;
		
		transactionLog.setDate_created(new GregorianCalendar());		
		transactionLog.setMsisdn(subscriberDetail.getMsisdn());
		transactionLog.setShortcode(billingPlanObject.getShortCode());	
		transactionLog.setDescription("GET IMSI");
		transactionLog.setStatus("FAILED");
		telnetConnector = (telnetConnector == null) ? (new TelnetServiceManager()) : telnetConnector;
		int response = StatusCodes.OTHER_ERRORS;

		logger.info("Execute called on GetSubscriberIMSIEMACommandImpl for subscriber with msisdn "+subscriberDetail.getMsisdn());

		try {
			Map<String, String> responseMap = telnetConnector.getSubscriberInformation(stripLeadingMsisdnPlus(subscriberDetail.getMsisdn()));			
			logger.info("This is imsi response "+responseMap);
			if (responseMap == null || !responseMap.containsKey("imsi"))
			{				
				logger.info("Error retrieveing IMSI, returning status "+StatusCodes.OTHER_ERRORS_IMSI);
				response = StatusCodes.OTHER_ERRORS_IMSI;
			}
			else if(!responseMap.get("imsi").equalsIgnoreCase("") && isIMSIValid(responseMap.get("imsi"))
					&& !responseMap.get("msisdn").equalsIgnoreCase("") &&
					subscriberDetail.getMsisdn().equalsIgnoreCase(responseMap.get("msisdn"))) 
			{
				String subscriberImsi = responseMap.get("imsi");				
				transactionLog.setStatus("SUCCESSFUL");
				transactionLog.setService(subscriberImsi);				
				subscriberDetail.setImsi(subscriberImsi);
				logger.info("IMSI value is "+subscriberImsi+" for subscriber "+subscriberDetail.getMsisdn());
				response = StatusCodes.SUCCESS;
			}
			else if(!responseMap.get("imsi").equalsIgnoreCase("") && !isIMSIValid(responseMap.get("imsi"))) 
			{
				subscriberDetail.setImsi(responseMap.get("imsi"));
				logger.info(" Subscriber not allowed, check msisdn"+subscriberDetail.getMsisdn() + 
						"imsi "+ responseMap.get("imsi") +""
						+ " to confirm, imsi range is "
						+ "configurable in project_path/webapps/blackberry/WEB-INF/classes/com/vasconsulting/www/utility/applicationconfig.properties");
				if (subscriberType().equalsIgnoreCase("prepaid")){
					response = StatusCodes.POSTPAID_SUBSCRIBER_NOT_ALLOWED;
				}else{
					response = StatusCodes.PREPAID_SUBSCRIBER_NOT_ALLOWED;
				}

			}
			else 
			{
				response = StatusCodes.IMSI_NOT_FOUND;
			}

		} catch (Exception e) {
			transactionLog.setStatus("FAILED");
			e.printStackTrace();
			
		}finally {
			transactionLog.setId(UUID.randomUUID().toString());
			hibernateUtility.saveTransactionlog(transactionLog);
		}
		return response;
	}

	public BillingPlanObjectUtility getBillingPlanObject() {
		return (billingPlanObject == null) ? (BillingPlanObjectUtility)ContextLoaderImpl.getBeans("billingPlanObject") : billingPlanObject;
	}

	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;
	}

	public HibernateUtility getHibernateUtility() {
		return hibernateUtility;
	}

	public void setHibernateUtility(HibernateUtility hibernateUtility) {
		this.hibernateUtility = hibernateUtility;
	}

	private String stripLeadingMsisdnPlus(String msisdn){
		String Msisdn = msisdn;
		System.out.println("Configured OPCO Country Code is: "+properties.getProperty("opcoCountryCode"));
		if (msisdn.startsWith("0")){
			return properties.getProperty("opcoCountryCode")+Msisdn.substring(1, Msisdn.length());
		}
		else if(Msisdn.startsWith("+"+properties.getProperty("opcoCountryCode"))){
			return Msisdn.substring(1, Msisdn.length());
		}
		else return Msisdn;
	}

	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams.toLowerCase();
	}

	public String getReceiverParameters(){
		return receiverParams;
	}

	public int rollBack()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public LoadAllProperties getProperties() {
		return properties;
	}

	public void setProperties(LoadAllProperties properties) {
		this.properties = properties;
	}

	public String configuredIMSIs() {
		String imsiType = subscriberType() + "IMSIRange";
		logger.info("Trying to get configured range for: "+imsiType);
		return properties.getProperty(imsiType);
	}
	
	protected String subscriberType(){
		if (!((receiverParams == null) || receiverParams.isEmpty())){
			return receiverParams.toLowerCase();
		} else return billingPlanObject.getDescription().toLowerCase().split(" ")[0];
	}

	private String receiverParameters() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isIMSIValid(String imsi) {
		boolean resp = false;
		logger.info("Configured imsi range is: "+configuredIMSIs());
		String[] configuredIMSIArray = configuredIMSIs().split(",");
		for (int i = 0; i < configuredIMSIArray.length; i++ ){
			if (imsi.startsWith(configuredIMSIArray[i].trim())){
				resp = true;
				return resp;
			}
		}
		return resp;
	}

	public boolean isIMSIValidPrepaidIMSI(String imsi){
		return false;
	}

	public SubscriberDetail getSubscriberDetail() {
		return (subscriberDetail == null) ? (SubscriberDetail)ContextLoaderImpl.getBeans("subscriberDetail") : this.subscriberDetail;
	}

	public void setSubscriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}

	public TelnetServiceManager getTelnetConnector() {
		return telnetConnector;
	}

	public void setTelnetConnector(TelnetServiceManager telnetConnector) {
		this.telnetConnector = telnetConnector;
	}

	public TransactionLog getTransactionLog() {
		return transactionLog;
	}

	public void setTransactionLog(TransactionLog transactionLog) {
		this.transactionLog = transactionLog;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
}
