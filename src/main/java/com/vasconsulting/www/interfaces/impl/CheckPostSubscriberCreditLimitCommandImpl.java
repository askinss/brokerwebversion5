/**
 * This class is used to check the type of subscriber. If the subscriber is a POSTPAID or PREPAID subscriber
 * It returns 0 if subscriber is postpaid and 500 if prepaid.
 * @author nnamdi Jibunoh
 */
package com.vasconsulting.www.interfaces.impl;

import java.math.BigDecimal;
import java.util.GregorianCalendar;
import java.util.UUID;

import org.apache.log4j.Logger;

import ug.co.waridtel.ObjectInfo;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.HomiscoResults;
import com.vasconsulting.www.utility.OCSSubscriberUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class CheckPostSubscriberCreditLimitCommandImpl implements Command {
	
	private SubscriberDetail subscriberDetail;
	private Logger logger = Logger.getLogger(CheckPostSubscriberCreditLimitCommandImpl.class);
	private TransactionLog transactionLog = new TransactionLog();
	private HibernateUtility hibernateUtility;
	private BillingPlanObjectUtility billingPlanObject;
	private String receiverParams;
	private OCSSubscriberUtility ocsUtility = new OCSSubscriberUtility();
		
	/**
	 * This method will execute and perform service change on Homisco/OCS based on the supplied
	 * receiver. The method will log at the end of the method execution the state of the 
	 * transaction run.
	 * @author Nnamdi Jibunoh
	 * @param
	 */
	public int execute() 
	{
			hibernateUtility = 
				(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
			
			String [] receiverParamValues = receiverParams.split(":SEP:");
			
			logger.info("Execute called on CheckPostSubscriberCreditLimitCommandImpl for subscriber with msisdn "+subscriberDetail.getMsisdn());
			
			transactionLog.setId(UUID.randomUUID().toString());
			transactionLog.setMsisdn(subscriberDetail.getMsisdn());
			transactionLog.setShortcode(billingPlanObject.getShortCode());
			transactionLog.setDate_created(new GregorianCalendar());
			transactionLog.setService("CHECK ACCOUNT BALANCE");
			
			if (ocsUtility.isSubscriberFromAirtel(subscriberDetail.getMsisdn()))
			{			
				try {
					int responseValue = 0;
					
					HomiscoResults subscriberCredit = 
						hibernateUtility.getPostpaidSubscriberCreditLimit(stripLeadingMsisdnPlus(subscriberDetail.getMsisdn()));
					
									
					//Iterator<String> iterator = subscriberCredit.keySet().iterator();
					String status = null;
					
					/*if (iterator.hasNext())
					{
						status = iterator.next();
					}*/
					
					status = subscriberCredit.getStatus();
					
					if (status.equalsIgnoreCase("ACTIVE") || status.equalsIgnoreCase("NO LIMIT") || status.equalsIgnoreCase("NO BARRING")){
						if (status.equalsIgnoreCase("ACTIVE"))
						{
							BigDecimal remainingBalance = subscriberCredit.getBalance();
							
							BigDecimal amountToBill = new BigDecimal(receiverParamValues[1]);
							
							if (remainingBalance.compareTo(amountToBill) < 0)
							{
								transactionLog.setStatus("FAILURE");
								transactionLog.setDescription("Subscriber remaining balance is not up to service charge");
								
								logger.info("Subscriber doesnt have enough balance left to activate this service returning "+
										StatusCodes.INSUFFICENT_POSTPAID_BALANCE);
								return StatusCodes.INSUFFICENT_POSTPAID_BALANCE;
							}
								
						}
						transactionLog.setStatus("SUCCESSFUL");
						transactionLog.setDescription("Subscriber has enough balance");
						
						subscriberDetail.setImsi(subscriberDetail.getImsi());
						logger.info("Homisco Billing Platform returned "+status+" for subscriber "+subscriberDetail.getMsisdn());
						return StatusCodes.SUCCESS;
					}
					else if(status.equalsIgnoreCase("DISCONNECTED") || 
							status.equalsIgnoreCase("SUSPENDED") || status.equalsIgnoreCase("NOT ACTIVE") || status.equalsIgnoreCase("NOT POSTPAID"))
					{
						transactionLog.setStatus("FAILED");
						transactionLog.setDescription("Subscriber is either disconnected/suspended/not active or is prepaid");
						
						if(status.equalsIgnoreCase("DISCONNECTED"))
							logger.info("Error setting up Homisco, code = "+responseValue+" returning status "+
								StatusCodes.POSTPAID_DISCONNECTED_ERROR+", for subscriber["+subscriberDetail.getMsisdn()+"]");
						
						/*if(status.equalsIgnoreCase("NO BARRING"))
							logger.info("Error setting up Homisco, code = "+responseValue+" returning status "+
								StatusCodes.POSTPAID_NO_BARRING_ERROR+", for subscriber["+subscriberDetail.getMsisdn()+"]");*/
						
						if(status.equalsIgnoreCase("NOT ACTIVE"))
							logger.info("Error setting up Homisco, code = "+responseValue+" returning status "+
								StatusCodes.POSTPAID_NOT_ACTIVE_ERROR+", for subscriber["+subscriberDetail.getMsisdn()+"]");
						
						if (status.equalsIgnoreCase("NOT POSTPAID"))
							logger.info("Error setting up Homisco, code = "+responseValue+" returning status "+
								StatusCodes.POSTPAID_NOT_POSTPAID_ERROR+", for subscriber["+subscriberDetail.getMsisdn()+"]");
						
						logger.info("Error checking balance, returning code = "+StatusCodes.INSUFFICENT_POSTPAID_BALANCE);
						
						
					}
									
				} 
				catch(ClassCastException ex)
				{
					ex.printStackTrace();
					transactionLog.setStatus("FAILED");
					transactionLog.setDescription("An error occured while checking account balance in VANX, is subscriber postpaid?");
					return StatusCodes.POSTPAID_NOT_POSTPAID_ERROR;
				}
				catch (Exception e) {
					e.printStackTrace();
					transactionLog.setStatus("FAILED");
					transactionLog.setDescription("An error occured while checking account balance in VANX");
					return StatusCodes.OTHER_ERRORS;
				}	
				finally
				{
					hibernateUtility.saveTransactionlog(transactionLog);
				}
				return StatusCodes.INSUFFICENT_POSTPAID_BALANCE;
			}
			else
			{
				//that means that the subscriber is from Warid
				try
				{
					ObjectInfo responseOCS = ocsUtility.getSubscriberInfoOCS(subscriberDetail.getMsisdn());
					logger.info("Result from the OCS Servers is" + responseOCS);
					
					if (responseOCS != null)
					{
						logger.info("responseOCS.getCurrentCredit() = "+responseOCS.getCurrentCredit());
						logger.info("responseOCS.getIMSI() = "+responseOCS.getIMSI());
						logger.info("responseOCS.getIsprecharged() = "+responseOCS.getIsprecharged());
						
						BigDecimal amountToBill = new BigDecimal(receiverParamValues[1]);
						BigDecimal subscriberBalance = new BigDecimal(responseOCS.getCurrentCredit());
						
						if (responseOCS.getIsprecharged() == 0 || responseOCS.getIsprecharged() == 100 && responseOCS.getCurrentCredit() > 0)
						{
							if (subscriberBalance.compareTo(amountToBill) < 0)
							{
								transactionLog.setStatus("FAILURE");
								transactionLog.setDescription("Subscriber remaining balance is not up to service charge");
								
								logger.info("Subscriber doesnt have enough balance left to activate this service returning "+
										StatusCodes.INSUFFICENT_POSTPAID_BALANCE);
								return StatusCodes.INSUFFICENT_POSTPAID_BALANCE;
							}
							transactionLog.setStatus("SUCCESSFUL");
							transactionLog.setDescription("Subscriber has enough balance");
							
							subscriberDetail.setImsi(responseOCS.getIMSI());
							
							logger.info("OCS Platform returned UGX"+responseOCS.getCurrentCredit()+" for subscriber "+subscriberDetail.getMsisdn());
							return StatusCodes.SUCCESS;
						}
						else
						{
							transactionLog.setStatus("FAILURE");
							transactionLog.setDescription("Subscriber is not Postpaid");
							
							logger.info("Subscriber is not Postpaid so cannot activate this service returning "+
									StatusCodes.PREPAID_SUBSCRIBER);
							return StatusCodes.PREPAID_SUBSCRIBER;
						}
											
					}
					else
					{
						transactionLog.setStatus("FAILED");
						transactionLog.setDescription("An error occured while checking account balance in VANX");
						logger.info("OCS returned "+responseOCS+" for the number "+subscriberDetail.getMsisdn());
						return StatusCodes.OTHER_ERRORS;
					}
				}
				catch (Exception e) {
					e.printStackTrace();
					transactionLog.setStatus("FAILED");
					transactionLog.setDescription("An error occured while checking account balance in VANX");
					return StatusCodes.OTHER_ERRORS;
				}	
				finally
				{
					hibernateUtility.saveTransactionlog(transactionLog);
				}
			}
	}
	
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
		
	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
		
	}
	
	
	private String stripLeadingMsisdnPlus(String msisdn){
		String Msisdn = msisdn;
		if (msisdn.startsWith("0")){
			return Msisdn;
		}
		else if(Msisdn.startsWith("+256")){
			return "0"+Msisdn.substring(4, Msisdn.length());
		}
		else if(Msisdn.startsWith("256")){
			return "0"+Msisdn.substring(3, Msisdn.length());
		}
		else return Msisdn;
	}

	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}

	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
		this.billingPlanObject = billingPlanObject;
	}

}
