package com.vasconsulting.www.interfaces.impl;
import org.apache.log4j.Logger;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.StatusCodes;

public class CheckSubscriberAutoRenewStatusRenewalImpl implements Command  {
	private SubscriberDetail subscriberDetail;
	Logger logger = Logger.getLogger(CheckSubscriberAutoRenewStatusRenewalImpl.class);
	private BillingPlanObjectUtility billingPlanObject;

	
	public int execute() {
		// TODO Auto-generated method stub
		logger.info("Execute called on CheckSubscriberAutoRenewStatusRenewalImpl" +
				" for subscriber with MSISDN = "+subscriberDetail.getMsisdn());
		logger.info("Autorenew for subscriber with MSISDN "+subscriberDetail.getMsisdn() + " is: "+subscriberDetail.getAutoRenew());
		int autoRenew = subscriberDetail.getAutoRenew();
		if (autoRenew == 0){
			logger.info("Subscriber with MSISDN "+subscriberDetail.getMsisdn() + " would not be renewed");
			return StatusCodes.NO_AUTORENEWAL;
		}
		
		return 0;
	}

	
	public void setReceiverParameters(String receiverParams) {
		// TODO Auto-generated method stub
		
	}

	
	public int logTransaction() {
		// TODO Auto-generated method stub
		return 0;
	}


	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}


	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
		
	}
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}
}
