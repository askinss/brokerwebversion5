package com.vasconsulting.www.interfaces.impl;

import java.util.GregorianCalendar;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.celtel.databundle.service.impl.UCIPServiceRequestManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.HibernateUtilityImpl;
import com.vasconsulting.www.utility.StatusCodes;

public class EmptyPrepDedicatedAccountCommandRollbackImpl implements Command {
	private UCIPServiceRequestManager ucipConnector = new UCIPServiceRequestManager();
	private String receiverParams;
	private SubscriberDetail subscriberDetail;
	private HibernateUtility hibernateUtility;
	Map<String, String> ucipReturns;
	private BillingPlanObjectUtility billingPlanObject;
	private TransactionLog transactionLog;
	Logger logger = Logger.getLogger(EmptyPrepDedicatedAccountCommandRollbackImpl.class);
	
	
	@SuppressWarnings("unchecked")
	public int execute() {
		/**
		 * Retrieve the spring managed beans from the container.
		 */
		
		hibernateUtility = 
			(HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		
		logger.info("Execute has been called on EmptyPrepDedicatedAccountCommandRollbackImpl for subscriber subscriber with msisdn "+subscriberDetail.getMsisdn());
		
		
		String [] daValueToSet = receiverParams.split(":");//DA:AMOUNT:VALIDITY
		transactionLog = new TransactionLog();				
		transactionLog.setDate_created(new GregorianCalendar());
		transactionLog.setDescription("Empty DA");
		transactionLog.setMsisdn(subscriberDetail.getMsisdn());
		transactionLog.setShortcode(subscriberDetail.getShortCode()+"_Rollback");	
		
		if (daValueToSet != null && daValueToSet.length > 1)
		{
			try {
				ucipReturns = ucipConnector.getSubscriberBalance(subscriberDetail.getMsisdn());
				
				String[] expiryDates = ((String)ucipReturns.get("expiryDate")).split(",");
				String expiryDate = expiryDates[5];
				transactionLog.setService("Removed value is "+daValueToSet[1]);
				
				ucipReturns = ucipConnector.
					updateSubscriberDedicatedAccount(subscriberDetail.getMsisdn(), "-"+daValueToSet[1], daValueToSet[0], expiryDate, 
							0,subscriberDetail.getServiceplan().replace(" ", "")+"_DA_ROLLBACK");		
				
				logger.info("Response from AIR is "+ucipReturns);
				
				if (ucipReturns.get("responseCode").equalsIgnoreCase("0")){
					
					transactionLog.setStatus("SUCCESSFUL");
					hibernateUtility.saveTransactionlog(transactionLog);
				}
				else {
					transactionLog.setStatus("FAILED");
					hibernateUtility.saveTransactionlog(transactionLog);
					transactionLog.setId(UUID.randomUUID().toString());
					transactionLog.setDescription("SHORTCODE");
					hibernateUtility.saveTransactionlog(transactionLog);
					return new Integer(ucipReturns.get("responseCode")).intValue();
				}
				
			} catch (NumberFormatException e) {
				transactionLog.setStatus("FAILED");				
				hibernateUtility.saveTransactionlog(transactionLog);
				transactionLog.setId(UUID.randomUUID().toString());
				transactionLog.setDescription("SHORTCODE");
				hibernateUtility.saveTransactionlog(transactionLog);
				e.printStackTrace();
				return StatusCodes.WRONG_DAVALUE_FORMAT;
			} catch (Exception e) {
				transactionLog.setStatus("FAILED");
				hibernateUtility.saveTransactionlog(transactionLog);
				transactionLog.setId(UUID.randomUUID().toString());
				transactionLog.setDescription("SHORTCODE");
				hibernateUtility.saveTransactionlog(transactionLog);
				e.printStackTrace();
				return StatusCodes.OTHER_ERRORS_IN;
			}
			return StatusCodes.SUCCESS;
		}
		else
		{
			return StatusCodes.WRONG_DAVALUE_FORMAT;
		}
	}
	public int logTransaction()
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	public void setReceiverParameters(String receiverParams)
	{
		this.receiverParams = receiverParams;
	}
	public void setReceiver(String receiver) {
		// TODO Auto-generated method stub
		
	}
	public void setSusbcriberDetail(SubscriberDetail subscriberDetail) {
		this.subscriberDetail = subscriberDetail;
	}
	
	public void setBillingPlanObject(BillingPlanObjectUtility billingPlanObject) {
	    this.billingPlanObject = billingPlanObject;
	}

}
