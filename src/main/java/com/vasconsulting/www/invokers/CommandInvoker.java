/**
 * This is invoker class. It defines an addCommand method that will
 * be used to receive all the commands that need to be executed in an arraylist. This list
 * is looped and executed at runtime.
 * @author Nnamdi Jibunoh
 * Date created 18th july 2011
 * Company VAS Consulting
 */
package com.vasconsulting.www.invokers;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.utility.StatusCodes;

public class CommandInvoker 
{	
	private ArrayList<Command> commands = new ArrayList<Command>();
	Logger logger = Logger.getLogger(CommandInvoker.class);
	
	public void addCommand(Command command)
	{
		commands.add(command);
	}
	
	public int provision()
	{		
		int statusCode = -1;
		logger.info("Size of the loaded Commands is "+commands.size());
		for (Command iterable_element : commands) 
		{			
			statusCode = iterable_element.execute();
			
			if (statusCode != 0) return statusCode;
		}
		return StatusCodes.SUCCESS;
	}
}
