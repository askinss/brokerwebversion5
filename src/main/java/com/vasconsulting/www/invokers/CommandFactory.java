/**
 * This is a factory class that is used to generate instances of the supplied class by name.
 * This factory class ensures that there is a loose coupling between the command names in the 
 * xml configuration file and the actual class that performs the required action.
 * @author Nnamdi Jibunoh
 */
package com.vasconsulting.www.invokers;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.Command;
import com.vasconsulting.www.interfaces.RenewalCommand;

public class CommandFactory 
{
	/**
	 * This is the standard factory for all command object in the SMS and USSD modules of the application.
	 * @param commandClassName
	 * @param fullPathToClass
	 * @param receiverParams
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Command GetCommandInstance(String commandClassName,String fullPathToClass, String receiverParams)	
	{
		Class c;
		Object toReturn = null;
		
		try {
			c = Class.forName(fullPathToClass+"."+commandClassName);
		
			
			Constructor<Command> constructor = c.getConstructor();		
			
			toReturn = constructor.newInstance();
			
		} 
		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Command command = (Command)toReturn;
		command.setReceiverParameters(receiverParams);
		
		return command;
		
	}	
	
	/**
	 * This commandFactory is used to generate the instance of commands in the renewal module of the application.
	 * @param commandClassName
	 * @param fullPathToClass
	 * @param receiverParams
	 * @param subscriberDetail
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Command GetCommandInstance(String commandClassName,String fullPathToClass, String receiverParams, SubscriberDetail subscriberDetail)	
	{
		Class c;
		Object toReturn = null;
		
		try {
			c = Class.forName(fullPathToClass+"."+commandClassName);
		
			
			Constructor<Command> constructor = c.getConstructor();		
			
			toReturn = constructor.newInstance();
			
		} 
		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		RenewalCommand command = (RenewalCommand)toReturn;
		command.setSubscriber(subscriberDetail);	
		command.setReceiverParameters(receiverParams);
		
		return command;		
	}	
	
	public Command GetCommandInstance(String commandClassName,String fullPathToClass, String receiverParams, String receiver){
		Command command = GetCommandInstance(commandClassName, fullPathToClass, receiverParams);
		command.setReceiver(receiver);
		return command;
		
	}
	
}
