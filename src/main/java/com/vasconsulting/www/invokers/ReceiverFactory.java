/**
 * This is a factory class that is used to generate instances of the supplied class by name.
 * This factory class ensures that there is a loose coupling between the command names in the 
 * xml configuration file and the actual class that performs the required action.
 * @author Nnamdi Jibunoh
 */
package com.vasconsulting.www.invokers;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.vasconsulting.www.interfaces.Command;

public class ReceiverFactory 
{
		
	public Object GetReceiverInstance(String receiverClassName,String fullPathToClass)
	{
		Object toReturn = null;
		Class c;
		try{
			c = Class.forName(fullPathToClass+"."+receiverClassName);
			
			Constructor<Command> constructor = c.getConstructor();		
			
			toReturn = constructor.newInstance();
		}
		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return toReturn;
		
	}	
	
}
