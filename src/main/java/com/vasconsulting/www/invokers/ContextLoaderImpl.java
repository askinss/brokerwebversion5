package com.vasconsulting.www.invokers;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;


public class ContextLoaderImpl implements ApplicationContextAware
{
	private static ApplicationContext ctx;
	
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException{
		this.ctx = applicationContext;
	}
	
	public static Object getBeans(String className){
		
		return ctx.getBean(className);
	}
	
}
