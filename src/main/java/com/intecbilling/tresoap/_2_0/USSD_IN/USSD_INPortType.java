/**
 * USSD_INPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.intecbilling.tresoap._2_0.USSD_IN;

public interface USSD_INPortType extends java.rmi.Remote {

    /**
     * Guided to UDG:WSAfetchAvailableUsageLimit.Fetch& [1]
     */
    public com.intecbilling.tresoap._2_0.USSD.FetchAvailableUsageLimitResp fetchAvailableUsageLimit_Fetch(com.intecbilling.tresoap._2_0.USSD.FetchAvailableUsageLimit parameters) throws java.rmi.RemoteException;

    /**
     * Guided to UDG:WSACHANGESERVICE.Update& [1]
     */
    public com.intecbilling.tresoap._2_0.USSD.CHANGESERVICEResponseType CHANGESERVICE_Update(com.intecbilling.tresoap._2_0.USSD.CHANGESERVICE parameters) throws java.rmi.RemoteException;
}
