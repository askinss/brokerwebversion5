/**
 * USSD_INLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.intecbilling.tresoap._2_0.USSD_IN;

import com.vasconsulting.www.utility.LoadAllProperties;

public class USSD_INLocator extends org.apache.axis.client.Service implements com.intecbilling.tresoap._2_0.USSD_IN.USSD_IN {
	
	private LoadAllProperties properties = new LoadAllProperties();

    public USSD_INLocator() {
    }


    public USSD_INLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public USSD_INLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for USSD_INHttpsSoap11Endpoint
     
   // private java.lang.String USSD_INHttpsSoap11Endpoint_address = "https://172.27.147.212:8237/services/USSD_IN.USSD_INHttpSoap11Endpoint";
    private java.lang.String USSD_INHttpsSoap11Endpoint_address = properties.getProperty("getUSSD_INHttpsSoap11EndpointAddress");
     
    
    

    public java.lang.String getUSSD_INHttpsSoap11EndpointAddress() {
        return USSD_INHttpsSoap11Endpoint_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String USSD_INHttpsSoap11EndpointWSDDServiceName = "USSD_INHttpsSoap11Endpoint";

    public java.lang.String getUSSD_INHttpsSoap11EndpointWSDDServiceName() {
        return USSD_INHttpsSoap11EndpointWSDDServiceName;
    }

    public void setUSSD_INHttpsSoap11EndpointWSDDServiceName(java.lang.String name) {
        USSD_INHttpsSoap11EndpointWSDDServiceName = name;
    }

    public com.intecbilling.tresoap._2_0.USSD_IN.USSD_INPortType getUSSD_INHttpsSoap11Endpoint() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
       System.out.println("Sending Singleview request to "+USSD_INHttpsSoap11Endpoint_address);
        try {
            endpoint = new java.net.URL(USSD_INHttpsSoap11Endpoint_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getUSSD_INHttpsSoap11Endpoint(endpoint);
    }

    public com.intecbilling.tresoap._2_0.USSD_IN.USSD_INPortType getUSSD_INHttpsSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.intecbilling.tresoap._2_0.USSD_IN.USSD_INSoap11BindingStub _stub = new com.intecbilling.tresoap._2_0.USSD_IN.USSD_INSoap11BindingStub(portAddress, this);
            _stub.setPortName(getUSSD_INHttpsSoap11EndpointWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setUSSD_INHttpsSoap11EndpointEndpointAddress(java.lang.String address) {
        USSD_INHttpsSoap11Endpoint_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.intecbilling.tresoap._2_0.USSD_IN.USSD_INPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.intecbilling.tresoap._2_0.USSD_IN.USSD_INSoap11BindingStub _stub = new com.intecbilling.tresoap._2_0.USSD_IN.USSD_INSoap11BindingStub(new java.net.URL(USSD_INHttpsSoap11Endpoint_address), this);
                _stub.setPortName(getUSSD_INHttpsSoap11EndpointWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("USSD_INHttpsSoap11Endpoint".equals(inputPortName)) {
            return getUSSD_INHttpsSoap11Endpoint();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tresoap.intecbilling.com/2.0/USSD_IN", "USSD_IN");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tresoap.intecbilling.com/2.0/USSD_IN", "USSD_INHttpsSoap11Endpoint"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("USSD_INHttpsSoap11Endpoint".equals(portName)) {
            setUSSD_INHttpsSoap11EndpointEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
