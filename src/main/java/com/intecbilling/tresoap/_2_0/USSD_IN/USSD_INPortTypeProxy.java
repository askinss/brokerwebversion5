package com.intecbilling.tresoap._2_0.USSD_IN;

public class USSD_INPortTypeProxy implements com.intecbilling.tresoap._2_0.USSD_IN.USSD_INPortType {
  private String _endpoint = null;
  private com.intecbilling.tresoap._2_0.USSD_IN.USSD_INPortType uSSD_INPortType = null;
  
  public USSD_INPortTypeProxy() {
    _initUSSD_INPortTypeProxy();
  }
  
  public USSD_INPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initUSSD_INPortTypeProxy();
  }
  
  private void _initUSSD_INPortTypeProxy() {
    try {
      uSSD_INPortType = (new com.intecbilling.tresoap._2_0.USSD_IN.USSD_INLocator()).getUSSD_INHttpsSoap11Endpoint();
      if (uSSD_INPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)uSSD_INPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)uSSD_INPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (uSSD_INPortType != null)
      ((javax.xml.rpc.Stub)uSSD_INPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.intecbilling.tresoap._2_0.USSD_IN.USSD_INPortType getUSSD_INPortType() {
    if (uSSD_INPortType == null)
      _initUSSD_INPortTypeProxy();
    return uSSD_INPortType;
  }
  
  public com.intecbilling.tresoap._2_0.USSD.FetchAvailableUsageLimitResp fetchAvailableUsageLimit_Fetch(com.intecbilling.tresoap._2_0.USSD.FetchAvailableUsageLimit parameters) throws java.rmi.RemoteException{
    if (uSSD_INPortType == null)
      _initUSSD_INPortTypeProxy();
    return uSSD_INPortType.fetchAvailableUsageLimit_Fetch(parameters);
  }
  
  public com.intecbilling.tresoap._2_0.USSD.CHANGESERVICEResponseType CHANGESERVICE_Update(com.intecbilling.tresoap._2_0.USSD.CHANGESERVICE parameters) throws java.rmi.RemoteException{
    if (uSSD_INPortType == null)
      _initUSSD_INPortTypeProxy();
    return uSSD_INPortType.CHANGESERVICE_Update(parameters);
  }
  
  
}