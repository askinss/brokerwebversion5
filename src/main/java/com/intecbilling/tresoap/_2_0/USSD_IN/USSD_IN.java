/**
 * USSD_IN.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.intecbilling.tresoap._2_0.USSD_IN;

public interface USSD_IN extends javax.xml.rpc.Service {
    public java.lang.String getUSSD_INHttpsSoap11EndpointAddress();

    public com.intecbilling.tresoap._2_0.USSD_IN.USSD_INPortType getUSSD_INHttpsSoap11Endpoint() throws javax.xml.rpc.ServiceException;

    public com.intecbilling.tresoap._2_0.USSD_IN.USSD_INPortType getUSSD_INHttpsSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
