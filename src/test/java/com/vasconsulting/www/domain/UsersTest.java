package com.vasconsulting.www.domain;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;

import org.hibernate.validator.HibernateValidator;
import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.vasconsulting.www.invokers.ContextLoaderImpl;

 
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/config/bbspring-test-HSQL-servlet.xml")
@Transactional
public class UsersTest {
    private LocalValidatorFactoryBean localValidatorFactory;
      
    
    private SessionFactory sessionFactory;
    
    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	private Session currentSession;
 
    @Before
    public void setup() {
        localValidatorFactory = new LocalValidatorFactoryBean();
        localValidatorFactory.setProviderClass(HibernateValidator.class);
        localValidatorFactory.afterPropertiesSet();
        this.sessionFactory = (SessionFactory)ContextLoaderImpl
				.getBeans("sessionFactory");
        currentSession = this.sessionFactory.getCurrentSession();
    }
    
    @Test
    public void shouldInvalidateWrongPassword(){
        final Users user = new Users();
        user.setUsername("fooBar");
        user.setPassword("Password1");
        Set<ConstraintViolation<Users>> constraintViolations = localValidatorFactory.validate(user);
        System.out.println(constraintViolations);
        assertTrue(constraintViolations.size() >= 1);
    }
    
    @Test
    public void testUsernameLengthLessThanThreeCharsCausesValidationError() {
        final Users user = new Users();
        user.setUsername("fo");
        user.setPassword("P@ssw0rd1");
        Set<ConstraintViolation<Users>> constraintViolations = localValidatorFactory.validate(user);
        assertTrue(constraintViolations.size() == 1);//Expect 1 violation on username
    }
    
    @Test
    public void testUsernameWithNonAlphaNumericCharsCausesValidationError() {
        final Users user = new Users();
        user.setUsername("fooBa)");
        user.setPassword("P@ssw0rd1");
        Set<ConstraintViolation<Users>> constraintViolations = localValidatorFactory.validate(user);
        assertTrue(constraintViolations.size() == 1);//Expect 1 violation on username
    }
    
    @Test
    public void shouldPersistEncryptedPassword() {
        assertEquals(0, currentSession.createQuery("from Users").list().size());
        Users user = new Users();
        user.setUsername("fooBar");
        user.setPassword("P@ssw0rd1");
        currentSession.persist(user);
        currentSession.flush();
        //assertEquals(1, currentSession.createQuery("from Users").list().size());
        @SuppressWarnings("unchecked")
		ArrayList<Users> response = (ArrayList<Users>)currentSession.createQuery("from Users where username = :msisdn and encryptedPassword = :password")
		.setString("password", user.getEncryptedPassword())
		.setString("msisdn", user.getUsername())
		.list();
        assertEquals("mXTch5jSLvLmKhOFrVGiGg==",response.get(0).getEncryptedPassword());
    }

}