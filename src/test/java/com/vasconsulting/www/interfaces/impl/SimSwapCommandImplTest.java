package com.vasconsulting.www.interfaces.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.context.ApplicationContext;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.HibernateUtilityImpl;
import com.vasconsulting.www.utility.RIMXMLResponseDetail;
import com.vasconsulting.www.utility.RIMXMLUtility;

public class SimSwapCommandImplTest extends TestCase {
	private SimSwapCommandImpl simSwap;
	private SubscriberDetail subscriberDetail;
	private SubscriberDetail subscriberDetailDB;
	@Mock
	private HibernateUtility hibernateUtility;
	@Mock
	private ApplicationContext ctx;
	private RIMXMLResponseDetail rimResponseDetail;
	private RIMXMLUtility rimUtility;

	@Before
	public void setUp() throws Exception {
		simSwap = new SimSwapCommandImpl();
		subscriberDetail = new SubscriberDetail();
		subscriberDetailDB = new SubscriberDetail();
		subscriberDetail.setMsisdn("255766666666");
		subscriberDetail.setImsi("645017700000");
		subscriberDetailDB.setMsisdn("255766666666");
		subscriberDetailDB.setImsi("645017700002");
		subscriberDetailDB.setStatus("Active");
		simSwap.setSubscriberDetailDB(subscriberDetailDB);
		hibernateUtility = mock(HibernateUtilityImpl.class);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		simSwap.setSubscriberDetails(subscriberDetail);
		ctx = mock(ApplicationContext.class);
		when(ctx.getBean("subscriberDetail")).thenReturn(subscriberDetail);
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		ContextLoaderImpl context = new ContextLoaderImpl();
		context.setApplicationContext(ctx);
		Collection<SubscriberDetail> subs = new ArrayList<SubscriberDetail>();
		subs.add(subscriberDetailDB);
		when(hibernateUtility.getSubscriberInformation(any(String.class),any(String.class))).thenReturn(subscriberDetailDB);
		rimUtility = mock(RIMXMLUtility.class);
		simSwap.setHibernateUtility(hibernateUtility);
	}
	
	
	@Test
	public void testExecute(){
		rimResponseDetail = new RIMXMLResponseDetail();
		rimResponseDetail.setErrorCode("0");
		when(rimUtility.modifyBillingIdentifier(any(SubscriberDetail.class),any(String.class))).thenReturn(rimResponseDetail);
		simSwap.setRimUtility(rimUtility);
		assertEquals(simSwap.execute(),0);	
	}
	
	@Test
	public void testExecuteWithFailure(){
		rimResponseDetail = new RIMXMLResponseDetail();
		rimResponseDetail.setErrorCode("64000");
		when(rimUtility.modifyBillingIdentifier(any(SubscriberDetail.class),any(String.class))).thenReturn(rimResponseDetail);
		simSwap.setRimUtility(rimUtility);
		assertEquals(simSwap.execute(),64000);	
	}

}
