package com.vasconsulting.www.interfaces.impl;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.context.ApplicationContext;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.EulogyService;
import com.vasconsulting.www.utility.HibernateUtilityImpl;


public class RemoveSubscriberEulogyServiceCommandImplTest {
	
	private RemoveSubscriberEulogyServiceCommandImpl removeSubscriberEulogyServiceCommandImpl;
	private SubscriberDetail subscriberDetail;
	private BillingPlanObjectUtility billingPlanObject;
	@Mock
	private HibernateUtility hibernateUtility;
	@Mock
	private EulogyService eulogyService;
	@Mock
	private ApplicationContext ctx;
	List<String> eulogyResponse;
	
	@Before
	public void setUp(){
		removeSubscriberEulogyServiceCommandImpl = new RemoveSubscriberEulogyServiceCommandImpl();
		subscriberDetail = new SubscriberDetail();
		billingPlanObject = new BillingPlanObjectUtility();
		billingPlanObject.setShortCode("BISMonth");
		removeSubscriberEulogyServiceCommandImpl.setBillingPlanObject(billingPlanObject);
		ctx = mock(ApplicationContext.class);
		eulogyService = mock(EulogyService.class);
		hibernateUtility = mock(HibernateUtilityImpl.class);
	}
	
	@Test
	public void testFlag(){
		assertEquals("D", removeSubscriberEulogyServiceCommandImpl.flag);
	}
	
	@Test
	public void testExecuteMSISDNDetachSuccess(){
		subscriberDetail.setMsisdn("250");
		removeSubscriberEulogyServiceCommandImpl.setSubscriberDetail(subscriberDetail);
		removeSubscriberEulogyServiceCommandImpl.setReceiverParameters("101,C001");
		eulogyResponse = new ArrayList<String>();
		eulogyResponse.add(0, "N");
		when(eulogyService.updateEulogySubDetails("250", "D", "101", "C001")).thenReturn(eulogyResponse);
		removeSubscriberEulogyServiceCommandImpl.setEulogyService(eulogyService);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		removeSubscriberEulogyServiceCommandImpl.setHibernateUtility(hibernateUtility);
		assertEquals(0,removeSubscriberEulogyServiceCommandImpl.execute());	
	}
	
	@Test
	public void testExecuteComponentIDIncorrect(){
		subscriberDetail.setMsisdn("250");
		removeSubscriberEulogyServiceCommandImpl.setSubscriberDetail(subscriberDetail);
		removeSubscriberEulogyServiceCommandImpl.setReceiverParameters("101,C001");
		eulogyResponse = new ArrayList<String>();
		eulogyResponse.add(0, "Z");
		when(eulogyService.updateEulogySubDetails("250", "D", "101", "C001")).thenReturn(eulogyResponse);
		removeSubscriberEulogyServiceCommandImpl.setEulogyService(eulogyService);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		removeSubscriberEulogyServiceCommandImpl.setHibernateUtility(hibernateUtility);
		assertEquals(0,removeSubscriberEulogyServiceCommandImpl.execute());	
	}

}
