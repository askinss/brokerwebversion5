package com.vasconsulting.www.interfaces.impl;

import static org.mockito.Mockito.*;
import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.context.ApplicationContext;

import com.celtel.databundle.service.impl.TelnetServiceManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.interfaces.impl.SetSubscriberAPNEMACommandImpl;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.HibernateUtilityImpl;
import com.vasconsulting.www.utility.LoadAllProperties;

public class SetSubscriberAPNEMAIfNonExistentCommandImplTest extends TestCase {	
	private SetSubscriberAPNEMAIfNonExistentCommandImpl setSubscriberAPNEMACommandImpl;
	@Mock
	private TelnetServiceManager telnetConnector;
	private SubscriberDetail subscriberDetail;
	private BillingPlanObjectUtility billingPlanObject;
	@Mock
	private HibernateUtility hibernateUtility;
	@Mock
	private LoadAllProperties properties;
	@Mock
	private ApplicationContext ctx;
	
	@Before
	public void setUp(){
		setSubscriberAPNEMACommandImpl = new SetSubscriberAPNEMAIfNonExistentCommandImpl();
		subscriberDetail = new SubscriberDetail();
		billingPlanObject = new BillingPlanObjectUtility();
	}
	
	@Test
	public void testIsSubscriberHavingBBAPN() throws Exception {
		billingPlanObject.setDescription("Prepaid BIS Monthly Plan");
		subscriberDetail.setMsisdn("255766666666");
		subscriberDetail.setServiceplan("");
		subscriberDetail.setServicetype(1);
		billingPlanObject.setShortCode("bisday");
		telnetConnector = spy(new TelnetServiceManager());
		hibernateUtility = mock(HibernateUtilityImpl.class);
		ctx = mock(ApplicationContext.class);
		when(ctx.getBean("subscriberDetail")).thenReturn(subscriberDetail);
		when(ctx.getBean("billingPlanObject")).thenReturn(billingPlanObject);
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		ContextLoaderImpl context = new ContextLoaderImpl();
		context.setApplicationContext(ctx);
		doReturn("RESP:0:MSISDN,242055009775:IMSI,629015656127836:AUTHD,AVAILABLE:CSP,3:GPRS,PDPCONTEXT,APNID,2,EQOSID,1,VPAA,0,PDPTY,IPV4,PDPID,1,PDPCONTEXT,"
				+ "APNID,6,EQOSID,1,VPAA,0,PDPTY,IPV4,PDPID,2,PDPCONTEXT,APNID,7,EQOSID,1,VPAA,0,PDPTY,IPV4,PDPID,3,PDPCONTEXT,APNID,8,EQOSID,1,VPAA,0,PDPTY,IPV4,"
				+ "PDPID,4,PDPCONTEXT,APNID,11,EQOSID,1,VPAA,0,PDPTY,IPV4,PDPID,5,PDPCONTEXT,APNID,18,EQOSID,1,VPAA,0,PDPTY,IPV4,PDPID,6,PDPCONTEXT,APNID,19,"
				+ "EQOSID,1,VPAA,0,PDPTY,IPV4,PDPID,7,PDPCONTEXT,APNID,20,EQOSID,1,VPAA,0,PDPTY,IPV4,PDPID,8,PDPCONTEXT,APNID,12,EQOSID,1,VPAA,0,PDPTY,IPV4,PDPID,"
				+ "9:NAM,0:CFU,1,0,TS10,0,BS20,0,BS30:CFNRC,1,0,TS10,0,BS20,0,BS30:CAW,1,1,TS10,0,BS20,0,BS30:BS26,1:BS3G,1:CAT,10:CLIP,1:DBSG,1:HOLD,1:MPTY,1:OICK,"
				+ "22:OFA,0:PWD,0000:SOCFRC,0:SOCFU,1:SOCLIP,0:TS11,1:TS21,1:TS22,1:LOC,4-242055005012,,242055005012,,4-242055294420:VLRID,4-242055005012:TSMO,0:RSA,2:SCHAR,0-0:REDMCH,1:IMEISV,3551670588202201;")
				.when(telnetConnector).getRawSubscriberInformation(subscriberDetail.getMsisdn());
		setSubscriberAPNEMACommandImpl.setBillingPlanObject(billingPlanObject);
		setSubscriberAPNEMACommandImpl.setReceiverParameters("SET:HLRSUB:MSISDN,%s:NAM,0:GPRS,DEF,PDPCONTEXT,APNID,7,EQOSID,8,VPAA,0,PDPTY,IPV4;");
		setSubscriberAPNEMACommandImpl.setReceiver("APNID,6,EQOSID,1");
		setSubscriberAPNEMACommandImpl.setTelnetConnector(telnetConnector);
		setSubscriberAPNEMACommandImpl.setSubscriberDetail(subscriberDetail);
		assertTrue(setSubscriberAPNEMACommandImpl.isSubscriberHavingBBAPN(subscriberDetail.getMsisdn()));
	}
	
	@Test
	public void testSuperWasCalledWhenSubscriberDoesNotHaveBBAPN() throws Exception {
		billingPlanObject.setDescription("Prepaid BIS Monthly Plan");
		subscriberDetail.setMsisdn("255766666666");
		subscriberDetail.setServiceplan("");
		subscriberDetail.setServicetype(1);
		billingPlanObject.setShortCode("bisday");
		telnetConnector = spy(new TelnetServiceManager());
		hibernateUtility = mock(HibernateUtilityImpl.class);
		ctx = mock(ApplicationContext.class);
		when(ctx.getBean("subscriberDetail")).thenReturn(subscriberDetail);
		when(ctx.getBean("billingPlanObject")).thenReturn(billingPlanObject);
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		ContextLoaderImpl context = new ContextLoaderImpl();
		context.setApplicationContext(ctx);
		doReturn("").when(telnetConnector).addBlackberryAPNToSubscriber(anyString());
		doReturn("RESP:0:MSISDN,242055009775:IMSI,629015656127836:AUTHD,AVAILABLE:CSP,3:GPRS,PDPCONTEXT,APNID,2,EQOSID,1,VPAA,0,PDPTY,IPV4,PDPID,1,PDPCONTEXT,"
				+ "VPAA,0,PDPTY,IPV4,PDPID,2,PDPCONTEXT,APNID,7,EQOSID,1,VPAA,0,PDPTY,IPV4,PDPID,3,PDPCONTEXT,APNID,8,EQOSID,1,VPAA,0,PDPTY,IPV4,"
				+ "PDPID,4,PDPCONTEXT,APNID,11,EQOSID,1,VPAA,0,PDPTY,IPV4,PDPID,5,PDPCONTEXT,APNID,18,EQOSID,1,VPAA,0,PDPTY,IPV4,PDPID,6,PDPCONTEXT,APNID,19,"
				+ "EQOSID,1,VPAA,0,PDPTY,IPV4,PDPID,7,PDPCONTEXT,APNID,20,EQOSID,1,VPAA,0,PDPTY,IPV4,PDPID,8,PDPCONTEXT,APNID,12,EQOSID,1,VPAA,0,PDPTY,IPV4,PDPID,"
				+ "9:NAM,0:CFU,1,0,TS10,0,BS20,0,BS30:CFNRC,1,0,TS10,0,BS20,0,BS30:CAW,1,1,TS10,0,BS20,0,BS30:BS26,1:BS3G,1:CAT,10:CLIP,1:DBSG,1:HOLD,1:MPTY,1:OICK,"
				+ "22:OFA,0:PWD,0000:SOCFRC,0:SOCFU,1:SOCLIP,0:TS11,1:TS21,1:TS22,1:LOC,4-242055005012,,242055005012,,4-242055294420:VLRID,4-242055005012:TSMO,0:RSA,2:SCHAR,0-0:REDMCH,1:IMEISV,3551670588202201;")
				.when(telnetConnector).getRawSubscriberInformation(subscriberDetail.getMsisdn());
		setSubscriberAPNEMACommandImpl.setBillingPlanObject(billingPlanObject);
		setSubscriberAPNEMACommandImpl.setReceiverParameters("SET:HLRSUB:MSISDN,%s:NAM,0:GPRS,DEF,PDPCONTEXT,APNID,7,EQOSID,8,VPAA,0,PDPTY,IPV4;");
		setSubscriberAPNEMACommandImpl.setReceiver("APNID,6,EQOSID,1");
		setSubscriberAPNEMACommandImpl.setTelnetConnector(telnetConnector);
		setSubscriberAPNEMACommandImpl.setSubscriberDetail(subscriberDetail);
		setSubscriberAPNEMACommandImpl.execute();
		verify(telnetConnector).addBlackberryAPNToSubscriber(anyString());
	}
	
	
}