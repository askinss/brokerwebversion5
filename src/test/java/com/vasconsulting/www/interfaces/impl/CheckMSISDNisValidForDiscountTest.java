package com.vasconsulting.www.interfaces.impl;

import junit.framework.TestCase;

import org.junit.Test;
import com.vasconsulting.www.domain.BbTestPromoFirstYes;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.interfaces.impl.CheckMSISDNisValidForDiscount;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.HibernateUtilityImpl;
import org.mockito.Mock;
import static org.mockito.Mockito.*;



public class CheckMSISDNisValidForDiscountTest extends TestCase {
	private CheckMSISDNisValidForDiscount checkMSISDNisValidForDiscount;
	private SubscriberDetail subscriberDetail;
	@Mock
	private HibernateUtility hibernateUtility;
	private BillingPlanObjectUtility billingPlanObjectLive;
	private TransactionLog transactionlog;

	@Test
	public void testPromoSubscriberIsExistingExecute() throws Exception {
		subscriberDetail = new SubscriberDetail();
		transactionlog = new TransactionLog();
		hibernateUtility = mock(HibernateUtilityImpl.class);
		subscriberDetail.setMsisdn("255");
		BbTestPromoFirstYes value = new BbTestPromoFirstYes();
		when(hibernateUtility.getBBPromoInitialYesStatusByMSISDN(subscriberDetail.getMsisdn())).thenReturn(value);
		billingPlanObjectLive = new BillingPlanObjectUtility();
		billingPlanObjectLive.setCost("1000");
		checkMSISDNisValidForDiscount = new CheckMSISDNisValidForDiscount();
		checkMSISDNisValidForDiscount.setSusbcriberDetail(subscriberDetail);
		checkMSISDNisValidForDiscount.setHibernateUtility(hibernateUtility);		
		checkMSISDNisValidForDiscount.setTransactionlog(transactionlog);
		checkMSISDNisValidForDiscount.setBillingPlanObject(billingPlanObjectLive);
		checkMSISDNisValidForDiscount.setReceiverParameters("500");		
		assertEquals(0, checkMSISDNisValidForDiscount.execute());
		assertEquals("500", billingPlanObjectLive.getCost());
	}
	
	@Test
	public void testPromoSubscriberNotFoundExecute() throws Exception {
		subscriberDetail = new SubscriberDetail();
		transactionlog = new TransactionLog();
		hibernateUtility = mock(HibernateUtilityImpl.class);
		subscriberDetail.setMsisdn("255");
		BbTestPromoFirstYes value = null;
		when(hibernateUtility.getBBPromoInitialYesStatusByMSISDN(subscriberDetail.getMsisdn())).thenReturn(value);
		billingPlanObjectLive = new BillingPlanObjectUtility();
		billingPlanObjectLive.setCost("1000");
		checkMSISDNisValidForDiscount = new CheckMSISDNisValidForDiscount();
		checkMSISDNisValidForDiscount.setSusbcriberDetail(subscriberDetail);
		checkMSISDNisValidForDiscount.setHibernateUtility(hibernateUtility);		
		checkMSISDNisValidForDiscount.setTransactionlog(transactionlog);
		checkMSISDNisValidForDiscount.setBillingPlanObject(billingPlanObjectLive);
		checkMSISDNisValidForDiscount.setReceiverParameters("500");		
		assertEquals(0, checkMSISDNisValidForDiscount.execute());
		assertEquals("1000", billingPlanObjectLive.getCost());
	}

}
