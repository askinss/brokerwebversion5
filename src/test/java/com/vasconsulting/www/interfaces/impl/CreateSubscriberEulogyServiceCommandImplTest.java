package com.vasconsulting.www.interfaces.impl;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.context.ApplicationContext;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.EulogyService;
import com.vasconsulting.www.utility.HibernateUtilityImpl;


public class CreateSubscriberEulogyServiceCommandImplTest {
	
	private CreateSubscriberEulogyServiceCommandImpl createSubscriberEulogyServiceCommandImpl;
	private SubscriberDetail subscriberDetail;
	private BillingPlanObjectUtility billingPlanObject;
	@Mock
	private HibernateUtility hibernateUtility;
	@Mock
	private EulogyService eulogyService;
	@Mock
	private ApplicationContext ctx;
	List<String> eulogyResponse;
	
	@Before
	public void setUp(){
		createSubscriberEulogyServiceCommandImpl = new CreateSubscriberEulogyServiceCommandImpl();
		subscriberDetail = new SubscriberDetail();
		billingPlanObject = new BillingPlanObjectUtility();
		billingPlanObject.setShortCode("BISMonth");
		createSubscriberEulogyServiceCommandImpl.setBillingPlanObject(billingPlanObject);
		ctx = mock(ApplicationContext.class);
		eulogyService = mock(EulogyService.class);
		hibernateUtility = mock(HibernateUtilityImpl.class);

	}
	
	@Test
	public void testExecuteSuccess(){
		subscriberDetail.setMsisdn("250");
		createSubscriberEulogyServiceCommandImpl.setSubscriberDetail(subscriberDetail);
		createSubscriberEulogyServiceCommandImpl.setReceiverParameters("101,C001");
		eulogyResponse = new ArrayList<String>();
		eulogyResponse.add(0, "Y");
		when(eulogyService.updateEulogySubDetails("250", "A", "101", "C001")).thenReturn(eulogyResponse);
		createSubscriberEulogyServiceCommandImpl.setEulogyService(eulogyService);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		createSubscriberEulogyServiceCommandImpl.setHibernateUtility(hibernateUtility);
		assertEquals(0,createSubscriberEulogyServiceCommandImpl.execute());	
	}
	
	@Test
	public void testExecuteWrongComponentId(){
		subscriberDetail.setMsisdn("250");
		createSubscriberEulogyServiceCommandImpl.setSubscriberDetail(subscriberDetail);
		createSubscriberEulogyServiceCommandImpl.setReceiverParameters("102,C001");
		eulogyResponse = new ArrayList<String>();
		eulogyResponse.add(0, "Z");
		when(eulogyService.updateEulogySubDetails("250", "A", "102", "C001")).thenReturn(eulogyResponse);
		createSubscriberEulogyServiceCommandImpl.setEulogyService(eulogyService);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		createSubscriberEulogyServiceCommandImpl.setHibernateUtility(hibernateUtility);
		assertEquals(9001,createSubscriberEulogyServiceCommandImpl.execute());
	}
	
	@Test
	public void testExecuteMSISDNDoesNotExist(){
		subscriberDetail.setMsisdn("250");
		createSubscriberEulogyServiceCommandImpl.setSubscriberDetail(subscriberDetail);
		createSubscriberEulogyServiceCommandImpl.setReceiverParameters("C001,101");
		eulogyResponse = new ArrayList<String>();
		eulogyResponse.add(0, "X");
		when(eulogyService.updateEulogySubDetails("250", "A", "C001", "101")).thenReturn(eulogyResponse);
		createSubscriberEulogyServiceCommandImpl.setEulogyService(eulogyService);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		createSubscriberEulogyServiceCommandImpl.setHibernateUtility(hibernateUtility);
		assertEquals(9005,createSubscriberEulogyServiceCommandImpl.execute());
	}
	
	

}
