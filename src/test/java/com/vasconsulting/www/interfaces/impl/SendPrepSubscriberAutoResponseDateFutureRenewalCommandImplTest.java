package com.vasconsulting.www.interfaces.impl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.context.ApplicationContext;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.SendSmsToKannelService;


public class SendPrepSubscriberAutoResponseDateFutureRenewalCommandImplTest {
	
	@Mock
	private SendSmsToKannelService smsService;
	@Mock
	private ApplicationContext ctx;
	private SubscriberDetail subscriberDetail;
	private SendPrepSubscriberAutoResponseDateFutureRenewalCommandImpl sendPrepSubscriberAutoResponseDateFutureRenewalCommandImpl;
	
	
	@Before
	public void setUp() throws Exception {
		smsService = mock(SendSmsToKannelService.class);
		subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("255766666666");
		subscriberDetail.setShortCode("bb30ActiveSubscriber");
		ctx = mock(ApplicationContext.class);
		BillingPlanObjectUtility billingPlanObject = new BillingPlanObjectUtility();
		billingPlanObject.setDescription("Prepaid COMPLETE Daily Plan");
		when(ctx.getBean("subscriberDetail")).thenReturn(subscriberDetail);
		when(ctx.getBean("smsService")).thenReturn(smsService);
		ContextLoaderImpl context = new ContextLoaderImpl();
		context.setApplicationContext(ctx);
		sendPrepSubscriberAutoResponseDateFutureRenewalCommandImpl = new SendPrepSubscriberAutoResponseDateFutureRenewalCommandImpl();
		sendPrepSubscriberAutoResponseDateFutureRenewalCommandImpl.setReceiverParameters("You have purchased %s for future use");
		sendPrepSubscriberAutoResponseDateFutureRenewalCommandImpl.setBillingPlanObject(billingPlanObject);
	}
	
	@Test
	public void testExecuteSMSIncludesShortcode(){
		sendPrepSubscriberAutoResponseDateFutureRenewalCommandImpl.setSusbcriberDetail(subscriberDetail);
		sendPrepSubscriberAutoResponseDateFutureRenewalCommandImpl.execute();
		String message = "You have purchased Prepaid COMPLETE Daily Plan for future use";
		verify(smsService).sendMessageToKannel(eq(message), eq(subscriberDetail.getMsisdn()));
	}

}
