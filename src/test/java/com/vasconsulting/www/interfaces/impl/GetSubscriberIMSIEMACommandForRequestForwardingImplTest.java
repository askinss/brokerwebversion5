package com.vasconsulting.www.interfaces.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.context.ApplicationContext;

import com.celtel.databundle.service.impl.TelnetServiceManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.HibernateUtilityImpl;


public class GetSubscriberIMSIEMACommandForRequestForwardingImplTest {
	private GetSubscriberIMSIEMACommandForRequestForwardingImpl getSubscriberIMSIEMACommandForRequestForwarding;
	@Mock
	private TelnetServiceManager telnetConnector;
	private SubscriberDetail subscriberDetail;
	@Mock
	private ApplicationContext ctx;
	private BillingPlanObjectUtility billingPlanObject;
	@Mock
	private HibernateUtility hibernateUtility;
	private Map<String, String> imsiMap;
	private Map<String, String> imsi;

	@Before
	public void setUp() throws Exception {
		getSubscriberIMSIEMACommandForRequestForwarding = new GetSubscriberIMSIEMACommandForRequestForwardingImpl();
		subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("255766666666");
		subscriberDetail.setShortCode("bismonth");
		billingPlanObject = new BillingPlanObjectUtility();
		billingPlanObject.setShortCode("bismonth");
		hibernateUtility = mock(HibernateUtilityImpl.class);
		imsiMap = new HashMap<String, String>();
		imsiMap.put("imsi", "645017700000");
		imsiMap.put("msisdn", "255766666666");
		telnetConnector = spy(new TelnetServiceManager());
		doReturn(imsiMap).when(telnetConnector).getSubscriberInformation("255766666666");
		getSubscriberIMSIEMACommandForRequestForwarding.setTelnetConnector(telnetConnector);
		getSubscriberIMSIEMACommandForRequestForwarding.setBillingPlanObject(billingPlanObject);
		getSubscriberIMSIEMACommandForRequestForwarding.setSubscriberDetail(subscriberDetail);
		ctx = mock(ApplicationContext.class);
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		when(ctx.getBean("subscriberDetail")).thenReturn(subscriberDetail);
		when(ctx.getBean("billingPlanObject")).thenReturn(billingPlanObject);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		ContextLoaderImpl context = new ContextLoaderImpl();
		context.setApplicationContext(ctx);
	}
	
	@Test
	public void testShortcodePrepaid(){
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		when(ctx.getBean("subscriberDetail")).thenReturn(subscriberDetail);
		when(ctx.getBean("billingPlanObject")).thenReturn(billingPlanObject);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		ContextLoaderImpl context = new ContextLoaderImpl();
		context.setApplicationContext(ctx);
		getSubscriberIMSIEMACommandForRequestForwarding.execute();
		assertEquals("prepbismonth", subscriberDetail.getShortCode());
	}
	
	@Test
	public void testShortcodePostpaid() throws Exception{
		imsi = new HashMap<String, String>();
		imsi.put("imsi", "645017800000"); //matching defined postPaidIMSIRange
		imsi.put("msisdn", "255766666666");
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		when(ctx.getBean("subscriberDetail")).thenReturn(subscriberDetail);
		when(ctx.getBean("billingPlanObject")).thenReturn(billingPlanObject);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		ContextLoaderImpl context = new ContextLoaderImpl();
		telnetConnector = spy(new TelnetServiceManager());
		doReturn(imsi).when(telnetConnector).getSubscriberInformation("255766666666");
		SubscriberDetail sub = subscriberDetail;
		when(ctx.getBean("subscriberDetail")).thenReturn(sub);
		context.setApplicationContext(ctx);
		getSubscriberIMSIEMACommandForRequestForwarding.setTelnetConnector(telnetConnector);
		getSubscriberIMSIEMACommandForRequestForwarding.execute();
		assertEquals("postbismonth", sub.getShortCode());
	}
	

}
