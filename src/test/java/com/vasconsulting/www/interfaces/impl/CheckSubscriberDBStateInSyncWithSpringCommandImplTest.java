package com.vasconsulting.www.interfaces.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.GregorianCalendar;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.context.ApplicationContext;

import static org.mockito.Matchers.*;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.HibernateUtilityImpl;
import com.vasconsulting.www.utility.StatusCodes;
import com.vasconsulting.www.utility.TimeUtilities;

import junit.framework.TestCase;


public class CheckSubscriberDBStateInSyncWithSpringCommandImplTest extends TestCase {
	private CheckSubscriberDBStateInSyncWithSpringCommandImpl checkSubscriberDBStateInSyncWithSpringCommandImpl;
	private SubscriberDetail subscriberDetail;
	private SubscriberDetail subscriber;
	@Mock
	private HibernateUtility hibernateUtility;
	@Mock
	private ApplicationContext ctx;
	
	@Before
	public void setUp(){
		ctx = mock(ApplicationContext.class);
		hibernateUtility = mock(HibernateUtilityImpl.class);
		subscriberDetail = new SubscriberDetail();
		subscriber = new SubscriberDetail();
		subscriberDetail.setMsisdn("25576662222");
		subscriberDetail.setStatus("Active");
		subscriberDetail.setNext_subscription_date(new GregorianCalendar());
		subscriberDetail.setImsi("645017700000");
		subscriber.setMsisdn("25576662222");
		subscriber.setStatus("Active");
		subscriber.setNext_subscription_date(new GregorianCalendar());
		subscriber.setImsi("645017700000");
		when(hibernateUtility.getSubscriberInformation("msisdn", "25576662222")).thenReturn(subscriber);
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		when(ctx.getBean("subscriberDetail")).thenReturn(subscriberDetail);
		ContextLoaderImpl context = new ContextLoaderImpl();
		context.setApplicationContext(ctx);
		checkSubscriberDBStateInSyncWithSpringCommandImpl = new CheckSubscriberDBStateInSyncWithSpringCommandImpl();
		checkSubscriberDBStateInSyncWithSpringCommandImpl.setSusbcriberDetail(subscriberDetail);
	}
	
	@Test
	public void testSubscriberDBStateInSyncWithSpringState(){
		assertEquals(0,checkSubscriberDBStateInSyncWithSpringCommandImpl.execute());
	}
}
