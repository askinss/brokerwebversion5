package com.vasconsulting.www.interfaces.impl;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;
import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.context.ApplicationContext;

import com.celtel.databundle.service.impl.TelnetServiceManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.interfaces.impl.SetSubscriberAPNEMACommandImpl;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.HibernateUtilityImpl;
import com.vasconsulting.www.utility.LoadAllProperties;

public class SetSubscriberAPNEMACommandImplTest extends TestCase {	
	private SetSubscriberAPNEMACommandImpl setSubscriberAPNEMACommandImpl;
	@Mock
	private TelnetServiceManager telnetConnector;
	private SubscriberDetail subscriberDetail;
	private BillingPlanObjectUtility billingPlanObject;
	@Mock
	private HibernateUtility hibernateUtility;
	@Mock
	private LoadAllProperties properties;
	@Mock
	private ApplicationContext ctx;
	
	@Before
	public void setUp(){
		setSubscriberAPNEMACommandImpl = new SetSubscriberAPNEMACommandImpl();
		subscriberDetail = new SubscriberDetail();
		billingPlanObject = new BillingPlanObjectUtility();
	}
	
	@Test
	public void testConvertToEMACommand() throws Exception {
		setSubscriberAPNEMACommandImpl = new SetSubscriberAPNEMACommandImpl();
		String command = setSubscriberAPNEMACommandImpl.convertToEMACommand("SET:HLRSUB:MSISDN,%s:NAM,0:GPRS,DEF,PDPCONTEXT,APNID,7,EQOSID,8,VPAA,0,PDPTY,IPV4;","23470812");
		assertEquals(command,"SET:HLRSUB:MSISDN,23470812:NAM,0:GPRS,DEF,PDPCONTEXT,APNID,7,EQOSID,8,VPAA,0,PDPTY,IPV4;");		
	}
	

	@Test
	public void testConvertToEMAMsisdnAloneCommand() throws Exception {
		setSubscriberAPNEMACommandImpl = new SetSubscriberAPNEMACommandImpl();
		String command = setSubscriberAPNEMACommandImpl.convertToEMACommand("SET:HLRSUB:MSISDN,%s:PDPCP,3;","23470812");
		assertEquals(command,"SET:HLRSUB:MSISDN,23470812:PDPCP,3;");		
	}
	
	@Test
	public void testEMAReturns10216() throws Exception {
		billingPlanObject.setDescription("Prepaid BIS Monthly Plan");
		subscriberDetail.setMsisdn("255766666666");
		subscriberDetail.setServiceplan("");
		subscriberDetail.setServicetype(1);
		billingPlanObject.setShortCode("bisday");
		telnetConnector = spy(new TelnetServiceManager());
		hibernateUtility = mock(HibernateUtilityImpl.class);
		ctx = mock(ApplicationContext.class);
		when(ctx.getBean("subscriberDetail")).thenReturn(subscriberDetail);
		when(ctx.getBean("billingPlanObject")).thenReturn(billingPlanObject);
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		ContextLoaderImpl context = new ContextLoaderImpl();
		context.setApplicationContext(ctx);
		doReturn("10216").when(telnetConnector).addBlackberryAPNToSubscriber("SET:HLRSUB:MSISDN,255766666666:NAM,0:GPRS,DEF,PDPCONTEXT,APNID,7,EQOSID,8,VPAA,0,PDPTY,IPV4;");
		setSubscriberAPNEMACommandImpl.setBillingPlanObject(billingPlanObject);
		setSubscriberAPNEMACommandImpl.setReceiverParameters("SET:HLRSUB:MSISDN,%s:NAM,0:GPRS,DEF,PDPCONTEXT,APNID,7,EQOSID,8,VPAA,0,PDPTY,IPV4;");
		setSubscriberAPNEMACommandImpl.setTelnetConnector(telnetConnector);
		setSubscriberAPNEMACommandImpl.setSubscriberDetail(subscriberDetail);
		assertEquals(0, setSubscriberAPNEMACommandImpl.execute());
	}
}