package com.vasconsulting.www.interfaces.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;

import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.celtel.databundle.service.impl.TelnetServiceManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.HibernateUtilityImpl;

import junit.framework.TestCase;


public class DeactivateOldDBIMSICommandImplTest extends TestCase {
	private DeactivateOldDBIMSICommandImpl deactivateOldDBIMSICommandImpl;
	@Autowired
	private TelnetServiceManager telnetConnector;
	@Autowired
	private SubscriberDetail subscriberDetail;
	@Autowired
	private BillingPlanObjectUtility billingPlanObject;
	@Autowired
	private HibernateUtility hibernateUtility;
	@Mock
	private ApplicationContext ctx;
	private TransactionLog transactionLog;
	private SubscriberDetail subscriberDetailDB;
	
	public void setUp(){
		ctx = mock(ApplicationContext.class);
		subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("25576662222");
		subscriberDetail.setServiceplan("");
		subscriberDetail.setServicetype(1);
		subscriberDetail.setImsi("645017700000");
		subscriberDetailDB = new SubscriberDetail();
		subscriberDetailDB.setMsisdn("25576662222");
		subscriberDetailDB.setImsi("64501770000");
		billingPlanObject = new BillingPlanObjectUtility();
		billingPlanObject.setShortCode("BES");
		when(ctx.getBean("subscriberDetail")).thenReturn(subscriberDetail);
		when(ctx.getBean("billingPlanObject")).thenReturn(billingPlanObject);
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		hibernateUtility = mock(HibernateUtilityImpl.class);
		ContextLoaderImpl context = new ContextLoaderImpl();
		context.setApplicationContext(ctx);
		transactionLog = new TransactionLog();
		transactionLog.setId("");
		deactivateOldDBIMSICommandImpl = new DeactivateOldDBIMSICommandImpl();
		deactivateOldDBIMSICommandImpl.setHibernateUtility(hibernateUtility);
		deactivateOldDBIMSICommandImpl.setTransactionLog(transactionLog);
	}

	public void testExecute(){
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		Collection<SubscriberDetail> sub = new ArrayList<SubscriberDetail>();
		sub.add(subscriberDetailDB);		
		when(hibernateUtility.getSubscriberInformation(any(SubscriberDetail.class))).thenReturn(sub);
		deactivateOldDBIMSICommandImpl.setSubscriberDetail(subscriberDetail);
		deactivateOldDBIMSICommandImpl.setBillingPlanObject(billingPlanObject);
		deactivateOldDBIMSICommandImpl.execute();
		assertEquals("SUCCESSFUL", transactionLog.getStatus());
	}
	
	public void testExecuteDBandSpringContainerIMSISame(){
		subscriberDetailDB.setImsi("645017700000");
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		Collection<SubscriberDetail> sub = new ArrayList<SubscriberDetail>();
		sub.add(subscriberDetailDB);		
		when(hibernateUtility.getSubscriberInformation(any(SubscriberDetail.class))).thenReturn(sub);
		deactivateOldDBIMSICommandImpl.setSubscriberDetail(subscriberDetail);
		deactivateOldDBIMSICommandImpl.setBillingPlanObject(billingPlanObject);
		deactivateOldDBIMSICommandImpl.execute();
		assertNull(transactionLog.getStatus());
	}
	
	public void testExecuteSubscriberNotInDb(){
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		Collection<SubscriberDetail> sub = new ArrayList<SubscriberDetail>();
		when(hibernateUtility.getSubscriberInformation(any(SubscriberDetail.class))).thenReturn(sub);
		deactivateOldDBIMSICommandImpl.setSubscriberDetail(subscriberDetail);
		deactivateOldDBIMSICommandImpl.setBillingPlanObject(billingPlanObject);
		deactivateOldDBIMSICommandImpl.execute();
		assertNull(transactionLog.getStatus());
	}
}
