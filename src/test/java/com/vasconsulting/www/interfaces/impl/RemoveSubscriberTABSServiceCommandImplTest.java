package com.vasconsulting.www.interfaces.impl;

import static org.mockito.Mockito.*;
import static org.mockito.Matchers.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.context.ApplicationContext;

import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.interfaces.TabsInterface;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.EulogyService;
import com.vasconsulting.www.utility.HibernateUtilityImpl;
import com.vasconsulting.www.utility.LoadAllProperties;
import com.vasconsulting.www.utility.Tabs6;

import junit.framework.TestCase;


public class RemoveSubscriberTABSServiceCommandImplTest extends TestCase {
	
	private RemoveSubscriberTABSServiceCommandImpl removeSubscriberTABSServiceCommandImpl;
	private SubscriberDetail subscriberDetail;
	private BillingPlanObjectUtility billingPlanObject;
	@Mock
	private HibernateUtility hibernateUtility;
	@Mock
	private TabsInterface tabs;
	@Mock
	private ApplicationContext ctx;
	private LoadAllProperties properties;

	@Before
	public void setUp(){
		removeSubscriberTABSServiceCommandImpl = new RemoveSubscriberTABSServiceCommandImpl();
		subscriberDetail = new SubscriberDetail();
		properties = new LoadAllProperties();
		properties.setPropertiesFile("config/applicationconfig.properties");
		subscriberDetail.setMsisdn("260777898777");
		subscriberDetail.setShortCode("bis30");
		subscriberDetail.setServiceplan("Prosumer B");
		billingPlanObject = new BillingPlanObjectUtility();
		billingPlanObject.setShortCode("BISMonth");
		removeSubscriberTABSServiceCommandImpl.setBillingPlanObject(billingPlanObject);
		ctx = mock(ApplicationContext.class);
		tabs = mock(Tabs6.class);
		hibernateUtility = mock(HibernateUtilityImpl.class);
		removeSubscriberTABSServiceCommandImpl.setTabs(tabs);
		removeSubscriberTABSServiceCommandImpl.setHibernateUtility(hibernateUtility);
		removeSubscriberTABSServiceCommandImpl.setSusbcriberDetail(subscriberDetail);
		removeSubscriberTABSServiceCommandImpl.setProperties(properties);
	}
	
	@Test
	public void testTabsReceivedBBLITE_BISToDelete(){
		removeSubscriberTABSServiceCommandImpl.execute();
		verify(tabs).deleteService(subscriberDetail.getMsisdn(), "BBLITE_BIS");
	}
	
	@Test
	public void testTabsReceivedBBLITE_BESToDelete(){
		subscriberDetail.setShortCode("bes30");
		subscriberDetail.setServiceplan("Enterprise B");
		removeSubscriberTABSServiceCommandImpl.execute();
		verify(tabs).deleteService(subscriberDetail.getMsisdn(), "BBLITE_BES");
	}
	
	@Test
	public void testTabsReceivedBBLITE_SOCToDelete(){
		subscriberDetail.setShortCode("soc30");
		subscriberDetail.setServiceplan("Social Plan");
		removeSubscriberTABSServiceCommandImpl.execute();
		verify(tabs).deleteService(subscriberDetail.getMsisdn(), "BBLITE_SOC");
	}
	
	@Test
	public void testTabsReceivedBBLITE_COMToDelete(){
		subscriberDetail.setShortCode("com30");
		subscriberDetail.setServiceplan("Complete Plan");
		removeSubscriberTABSServiceCommandImpl.execute();
		verify(tabs).deleteService(subscriberDetail.getMsisdn(), "BBLITE_COM");
	}
}
