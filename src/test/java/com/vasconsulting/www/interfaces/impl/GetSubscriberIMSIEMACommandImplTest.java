package com.vasconsulting.www.interfaces.impl;
import static org.mockito.Mockito.*;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.hamcrest.collection.IsMapContaining;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.context.ApplicationContext;

import com.celtel.databundle.service.impl.TelnetServiceManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.interfaces.impl.GetSubscriberIMSIEMACommandImpl;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.HibernateUtilityImpl;
import com.vasconsulting.www.utility.LoadAllProperties;


public class GetSubscriberIMSIEMACommandImplTest extends TestCase {
	private GetSubscriberIMSIEMACommandImpl getSubscriberIMSIEMACommand;
	@Mock
	private TelnetServiceManager telnetConnector;
	private SubscriberDetail subscriberDetail;
	private BillingPlanObjectUtility billingPlanObject;
	@Mock
	private HibernateUtility hibernateUtility;
	@Mock
	private LoadAllProperties properties;
	@Mock
	private ApplicationContext ctx;
	@Mock
	private Map<String, String> imsiMap;
	
	@Before
	public void setUp(){
		getSubscriberIMSIEMACommand = new GetSubscriberIMSIEMACommandImpl();
		subscriberDetail = new SubscriberDetail();
		billingPlanObject = new BillingPlanObjectUtility();
	}
	
	@Test
	public void testSubscriberType(){
		billingPlanObject.setDescription("Prepaid BIS Weekly Plan");
		getSubscriberIMSIEMACommand.setBillingPlanObject(billingPlanObject);
		assertEquals("prepaid", getSubscriberIMSIEMACommand.subscriberType());
	}
	
	@Test
	public void testConfiguredIMSIsPrepaid(){
		billingPlanObject.setDescription("Prepaid BIS Weekly Plan");
		getSubscriberIMSIEMACommand.setBillingPlanObject(billingPlanObject);
		assertEquals("6450177",
				getSubscriberIMSIEMACommand.configuredIMSIs());
	}
	
	@Test
	public void testConfiguredIMSIsPostpaid(){
		billingPlanObject.setDescription("Postpaid BIS Monthly Plan");
		getSubscriberIMSIEMACommand.setBillingPlanObject(billingPlanObject);
		assertEquals("6450178",
				getSubscriberIMSIEMACommand.configuredIMSIs());
	}
	
	@Test
	public void testIsIMSIvalidPostpaid(){
		billingPlanObject.setDescription("Postpaid BIS Monthly Plan");
		getSubscriberIMSIEMACommand.setBillingPlanObject(billingPlanObject);
		assertTrue(getSubscriberIMSIEMACommand.isIMSIValid("645017800000"));
	}	
	
	@Test
	public void testIsIMSIvalidPrepaid(){
		billingPlanObject.setDescription("Prepaid BIS Monthly Plan");
		getSubscriberIMSIEMACommand.setBillingPlanObject(billingPlanObject);
		assertTrue(getSubscriberIMSIEMACommand.isIMSIValid("645017700000"));
	}
	
	@Test
	public void testExecutePrepaidWithValidIMSI() throws Exception{
		billingPlanObject.setDescription("Prepaid BIS Monthly Plan");
		getSubscriberIMSIEMACommand.setBillingPlanObject(billingPlanObject);
		hibernateUtility = mock(HibernateUtilityImpl.class);
		subscriberDetail.setMsisdn("255766666666");
		subscriberDetail.setServiceplan("");
		subscriberDetail.setServicetype(1);
		billingPlanObject.setShortCode("bisday");
		Map<String, String> imsiMap = new HashMap<String, String>();
		imsiMap.put("imsi", "645017700000");
		imsiMap.put("msisdn", "255766666666");
		telnetConnector = spy(new TelnetServiceManager());
		doReturn(imsiMap).when(telnetConnector).getSubscriberInformation("255766666666");
		ctx = mock(ApplicationContext.class);
		when(ctx.getBean("subscriberDetail")).thenReturn(subscriberDetail);
		when(ctx.getBean("billingPlanObject")).thenReturn(billingPlanObject);
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		ContextLoaderImpl context = new ContextLoaderImpl();
		context.setApplicationContext(ctx);
		getSubscriberIMSIEMACommand.setTelnetConnector(telnetConnector);
		getSubscriberIMSIEMACommand.setSubscriberDetail(subscriberDetail);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		assertEquals(0,getSubscriberIMSIEMACommand.execute());
	}
	
	@Test
	public void testExecutePostpaidWithValidIMSI() throws Exception{
		billingPlanObject.setDescription("Postpaid BIS Monthly Plan");
		getSubscriberIMSIEMACommand.setBillingPlanObject(billingPlanObject);
		hibernateUtility = mock(HibernateUtilityImpl.class);
		subscriberDetail.setMsisdn("25576662222");
		subscriberDetail.setServiceplan("");
		subscriberDetail.setServicetype(1);
		billingPlanObject.setShortCode("BES");
		getSubscriberIMSIEMACommand.setBillingPlanObject(billingPlanObject);
		getSubscriberIMSIEMACommand.setSubscriberDetail(subscriberDetail);	
		telnetConnector = mock(TelnetServiceManager.class);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		ctx = mock(ApplicationContext.class);
		when(ctx.getBean("subscriberDetail")).thenReturn(subscriberDetail);
		when(ctx.getBean("billingPlanObject")).thenReturn(billingPlanObject);
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		ContextLoaderImpl context = new ContextLoaderImpl();
		context.setApplicationContext(ctx);
		Map<String, String> imsiMap = new HashMap<String, String>();
		imsiMap.put("imsi", "645017800000");
		imsiMap.put("msisdn", "25576662222");
		when(telnetConnector.getSubscriberInformation(anyString())).thenReturn(imsiMap);
		getSubscriberIMSIEMACommand.setTelnetConnector(telnetConnector);
		assertEquals(0,getSubscriberIMSIEMACommand.execute());
	}
	
	@Test
	public void testExecutePostpaidTryingPrepaid() throws Exception{
		getSubscriberIMSIEMACommand.setReceiverParameters("prepaid");
		getSubscriberIMSIEMACommand.setProperties(new LoadAllProperties());
		hibernateUtility = mock(HibernateUtilityImpl.class);
		subscriberDetail.setMsisdn("255762222666");
		subscriberDetail.setServiceplan("");
		subscriberDetail.setServicetype(1);
		billingPlanObject.setShortCode("BESprep");
		getSubscriberIMSIEMACommand.setBillingPlanObject(billingPlanObject);
		getSubscriberIMSIEMACommand.setSubscriberDetail(subscriberDetail);	
		telnetConnector = mock(TelnetServiceManager.class);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		Map<String, String> imsiMap = new HashMap<String, String>();
		imsiMap.put("imsi", "6400504100000");
		when(telnetConnector.getSubscriberInformation(anyString())).thenReturn(imsiMap);
		getSubscriberIMSIEMACommand.setTelnetConnector(telnetConnector);
		getSubscriberIMSIEMACommand.setHibernateUtility(hibernateUtility);
		assertEquals(1023,getSubscriberIMSIEMACommand.execute());
	}
	
	
	@Test
	public void testExecuteStoresEMAImsiInSpringContainer() throws Exception{
		billingPlanObject.setDescription("Postpaid BIS Monthly Plan");
		getSubscriberIMSIEMACommand.setBillingPlanObject(billingPlanObject);
		hibernateUtility = mock(HibernateUtilityImpl.class);
		subscriberDetail.setMsisdn("25576662222");
		subscriberDetail.setServiceplan("");
		subscriberDetail.setServicetype(1);
		billingPlanObject.setShortCode("BES");
		getSubscriberIMSIEMACommand.setBillingPlanObject(billingPlanObject);
		getSubscriberIMSIEMACommand.setSubscriberDetail(subscriberDetail);	
		telnetConnector = mock(TelnetServiceManager.class);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		ctx = mock(ApplicationContext.class);
		when(ctx.getBean("subscriberDetail")).thenReturn(subscriberDetail);
		when(ctx.getBean("billingPlanObject")).thenReturn(billingPlanObject);
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		ContextLoaderImpl context = new ContextLoaderImpl();
		context.setApplicationContext(ctx);
		Map<String, String> imsiMap = new HashMap<String, String>();
		imsiMap.put("imsi", "645017800000");
		imsiMap.put("msisdn", "25576662222");
		when(telnetConnector.getSubscriberInformation(anyString())).thenReturn(imsiMap);
		getSubscriberIMSIEMACommand.setTelnetConnector(telnetConnector);
		getSubscriberIMSIEMACommand.execute();
		assertEquals("645017800000", subscriberDetail.getImsi());
	}
	
	@Test
	public void testExecutePrepaidTryingPostpaid() throws Exception{
		getSubscriberIMSIEMACommand.setReceiverParameters("postpaid");
		getSubscriberIMSIEMACommand.setProperties(new LoadAllProperties());
		hibernateUtility = mock(HibernateUtilityImpl.class);
		subscriberDetail.setMsisdn("255762222666");
		subscriberDetail.setServiceplan("");
		subscriberDetail.setServicetype(1);
		billingPlanObject.setShortCode("BESprep");
		getSubscriberIMSIEMACommand.setBillingPlanObject(billingPlanObject);
		getSubscriberIMSIEMACommand.setSubscriberDetail(subscriberDetail);	
		telnetConnector = mock(TelnetServiceManager.class);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		Map<String, String> imsiMap = new HashMap<String, String>();
		imsiMap.put("imsi", "6400504200000");
		when(telnetConnector.getSubscriberInformation(anyString())).thenReturn(imsiMap);
		getSubscriberIMSIEMACommand.setTelnetConnector(telnetConnector);
		getSubscriberIMSIEMACommand.setHibernateUtility(hibernateUtility);
		assertEquals(1027,getSubscriberIMSIEMACommand.execute());
	}
	

}
