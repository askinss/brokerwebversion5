package com.vasconsulting.www.interfaces.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.*;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.context.ApplicationContext;

import com.celtel.databundle.service.impl.UCIPServiceRequestManager;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.HibernateUtilityImpl;

public class CheckPrepSubscriberCreditLimitAndDropToLowerBundleCommandImplTest extends TestCase {

	private SubscriberDetail subscriberDetail;
	@Mock
	private HibernateUtility hibernateUtility;
	private BillingPlanObjectUtility billingPlanObjectLive;
	private TransactionLog transactionlog;
	private Map<String, String> responseMap;
	private UCIPServiceRequestManager ucipConnector;
	@Mock
	private ApplicationContext ctx;

	private CheckPrepSubscriberCreditLimitAndDropToLowerBundleCommandImpl checkPrepSubscriberCreditLimitAndDropToLowerBundle;
	
	@Before
	public void setUp() throws Exception{
		billingPlanObjectLive = new BillingPlanObjectUtility();
		billingPlanObjectLive.setCost("1000");
		billingPlanObjectLive.setShortCode("bis30");
		subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("255766666666");
		subscriberDetail.setShortCode("bis30");
		hibernateUtility = mock(HibernateUtilityImpl.class);
		ctx = mock(ApplicationContext.class);
		responseMap = new HashMap<String, String>();
		responseMap.put("accountValue1", "70000");
		ucipConnector = mock(UCIPServiceRequestManager.class);
		when(ucipConnector.getSubscriberBalance(anyString())).thenReturn(responseMap);
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		when(ctx.getBean("subscriberDetail")).thenReturn(subscriberDetail);
		when(ctx.getBean("billingPlanObject")).thenReturn(billingPlanObjectLive);
		checkPrepSubscriberCreditLimitAndDropToLowerBundle = new CheckPrepSubscriberCreditLimitAndDropToLowerBundleCommandImpl();
		checkPrepSubscriberCreditLimitAndDropToLowerBundle.setBillingPlanObject(billingPlanObjectLive);
		checkPrepSubscriberCreditLimitAndDropToLowerBundle.setSusbcriberDetail(subscriberDetail);
		checkPrepSubscriberCreditLimitAndDropToLowerBundle.setUcipConnector(ucipConnector);
		ContextLoaderImpl context = new ContextLoaderImpl();
		context.setApplicationContext(ctx);
	}
	
	@Test
	public void testExecutePickNextCheaperPlan(){
		checkPrepSubscriberCreditLimitAndDropToLowerBundle.setReceiver("bis7:300:7,bis1:100:1");
		checkPrepSubscriberCreditLimitAndDropToLowerBundle.setReceiverParameters("100");
		int resp = checkPrepSubscriberCreditLimitAndDropToLowerBundle.execute();
		assertEquals(0,resp);
		assertEquals("bis7",billingPlanObjectLive.getShortCode());
		assertEquals("300",billingPlanObjectLive.getCost());
		
	}
	
	@Test
	public void testExecutePickNextCheaperPlanIrrespectiveOfOrder(){
		checkPrepSubscriberCreditLimitAndDropToLowerBundle.setReceiver("bis10:500:10,bis7:300:7,bis20:700:20,bis1:100:1");
		checkPrepSubscriberCreditLimitAndDropToLowerBundle.setReceiverParameters("100");
		int resp = checkPrepSubscriberCreditLimitAndDropToLowerBundle.execute();
		assertEquals(0,resp);
		assertEquals("bis10",billingPlanObjectLive.getShortCode());
		assertEquals("500",billingPlanObjectLive.getCost());
		
	}
	
	@Test
	public void testExecuteSetCorrectSubscriberShortcode(){
		checkPrepSubscriberCreditLimitAndDropToLowerBundle.setReceiver("bis10:500:10,bis7:300:7,bis20:700:20,bis1:100:1");
		checkPrepSubscriberCreditLimitAndDropToLowerBundle.setReceiverParameters("100");
		int resp = checkPrepSubscriberCreditLimitAndDropToLowerBundle.execute();
		assertEquals(0,resp);
		assertEquals("bis10",subscriberDetail.getShortCode());
		
	}
	
	@Test
	public void testExecuteAirtimeNotEnoughtForLowerBundle(){
		responseMap.put("accountValue1", "10");
		checkPrepSubscriberCreditLimitAndDropToLowerBundle.setReceiver("bis10:500:10,bis7:300:7,bis20:700:20,bis1:100:1");
		checkPrepSubscriberCreditLimitAndDropToLowerBundle.setReceiverParameters("100");
		int resp = checkPrepSubscriberCreditLimitAndDropToLowerBundle.execute();
		assertEquals(1090,resp);
	}
	
	@Test
	public void testExecuteAirtimeEnoughtForOriginalBundle(){
		responseMap.put("accountValue1", "120000");
		checkPrepSubscriberCreditLimitAndDropToLowerBundle.setReceiver("bis10:500:10,bis7:300:7,bis20:700:20,bis1:100:1");
		checkPrepSubscriberCreditLimitAndDropToLowerBundle.setReceiverParameters("100");
		int resp = checkPrepSubscriberCreditLimitAndDropToLowerBundle.execute();
		assertEquals(0,resp);
	}
	
	@Test
	public void testExecuteNextSubscriptionDateSetCorrectly(){
		responseMap.put("accountValue1", "20000");
		checkPrepSubscriberCreditLimitAndDropToLowerBundle.setReceiver("bis10:500:10,bis7:300:7,bis20:700:20,bis1:100:1");
		checkPrepSubscriberCreditLimitAndDropToLowerBundle.setReceiverParameters("100");
		checkPrepSubscriberCreditLimitAndDropToLowerBundle.execute();
		int noOfDays = (int)(subscriberDetail.getNext_subscription_date().getTimeInMillis() - new GregorianCalendar().getTimeInMillis())/1000;
		assertTrue(noOfDays >= 86300);
	}
	

}
