package com.vasconsulting.www.utility;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

import net.rubyeye.xmemcached.MemcachedClient;
import net.rubyeye.xmemcached.exception.MemcachedException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.vasconsulting.www.domain.FutureRenewal;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;
import com.vasconsulting.www.interfaces.HibernateUtility;
import com.vasconsulting.www.invokers.ContextLoaderImpl;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:config/bbspring-test-servlet.xml")
@DirtiesContext
@ActiveProfiles(profiles = "brokerservice")
public class BrokerServiceTest {

	private BrokerService brokerService;
	@Mock
	private HibernateUtility hibernateUtility;
	@Mock
	private ApplicationContext ctx;
	private SubscriberDetail subscriberDetail;
	private SubscriberDetail subscriberDetailDB;
	private FutureRenewal futureRenewal;
	@SuppressWarnings("unused")
	private BillingPlanObjectUtility billingPlanObjectLive;
	private LoadAllBillingPlanObjects loadAllBillingPlanObjects;
	private SendSmsToKannelService smsService;
	LoadAllProperties properties = new LoadAllProperties();
	private MemcachedClient memcachedClient = mock(MemcachedClient.class);
	
	@Test
	public void testSimSwap() throws Exception {
		hibernateUtility = mock(HibernateUtilityImpl.class);
		ctx = mock(ApplicationContext.class);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		brokerService = new BrokerService();
		loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
				.getBeans("loadSimSwapBillingPlanObjects");
		brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		brokerService.setMemcachedClient(memcachedClient);
		when(memcachedClient.add(anyString(),anyInt(),anyString())).thenReturn(true);
		brokerService.setProperties(properties);
		subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("2609967959365");
		subscriberDetail.setStatus("Active");
		subscriberDetail.setPrepaidSubscriber(1);
		subscriberDetail.setPostpaidSubscriber(0);
		subscriberDetail.setImsi("645017700000");
		ArrayList<SubscriberDetail> subscriberDetails = new ArrayList<SubscriberDetail>();
		subscriberDetails.add(subscriberDetail);
		when(hibernateUtility.getSubscriberInformation(any(String.class),any(String.class))).thenReturn(subscriberDetail);
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		brokerService.setSmsService(smsService);
		assertEquals(0,brokerService.simswap(subscriberDetail));
	}
	
	@Test
	public void testSMSNotWasSent() throws Exception{
		brokerService = new BrokerService();
		loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
				.getBeans("loadAllBillingPlanObjects");
		brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		LoadAllProperties properties = new LoadAllProperties();
		brokerService.setProperties(properties);
		brokerService.setMemcachedClient(memcachedClient);
		when(memcachedClient.add(anyString(),anyInt(),anyString())).thenReturn(true);
		subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("265996795951");
		subscriberDetail.setShortCode("corporatebes");
		smsService = mock(SendSmsToKannelService.class);
		brokerService.setSmsService(smsService);
		brokerService.activate("corporatebes",subscriberDetail);
		verify(smsService, never()).sendMessageToKannel("265996795951", "");
	}
	
	@Test
	public void testSMSWasSent() throws Exception{
		brokerService = new BrokerService();
		loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
				.getBeans("loadAllBillingPlanObjects");
		brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		LoadAllProperties properties = new LoadAllProperties();
		brokerService.setProperties(properties);
		brokerService.setMemcachedClient(memcachedClient);
		when(memcachedClient.add(anyString(),anyInt(),anyString())).thenReturn(true);
		subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("265996795951");
		smsService = mock(SendSmsToKannelService.class);
		brokerService.setSmsService(smsService);
		brokerService.setProperties(properties);
		brokerService.activate("sendSMSShortCode",subscriberDetail);
		verify(smsService, never()).sendMessageToKannel("265996795951", "");
	}
	
	@Test
	public void testNoRollBack() throws Exception{
		brokerService = spy(new BrokerService());
		loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
				.getBeans("loadAllBillingPlanObjects");
		brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		LoadAllProperties properties = new LoadAllProperties();
		brokerService.setProperties(properties);
		brokerService.setMemcachedClient(memcachedClient);
		when(memcachedClient.add(anyString(),anyInt(),anyString())).thenReturn(true);
		subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("265996795951");
		smsService = mock(SendSmsToKannelService.class);
		brokerService.setSmsService(smsService);
		brokerService.setProperties(properties);
		brokerService.activate("fakeHomiscoFailure",subscriberDetail);
		verify(brokerService, never()).rollback((SubscriberDetail)anyObject());
	}
	
	@Test
	public void testRollBack() throws Exception{
		brokerService = spy(new BrokerService());
		loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
				.getBeans("loadAllBillingPlanObjects");
		brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		LoadAllProperties properties = new LoadAllProperties();
		brokerService.setProperties(properties);
		brokerService.setMemcachedClient(memcachedClient);
		when(memcachedClient.add(anyString(),anyInt(),anyString())).thenReturn(true);
		subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("265996795951");
		smsService = mock(SendSmsToKannelService.class);
		brokerService.setSmsService(smsService);
		brokerService.setProperties(properties);
		brokerService.activate("fakeHomiscoFailureWantsRollBack",subscriberDetail);
		verify(brokerService).rollback((SubscriberDetail)anyObject());
	}
	
	
	@Test
	public void testRollBackDoesNotChangeShortcode() throws Exception{
		brokerService = new BrokerService();
		hibernateUtility = (HibernateUtility)ContextLoaderImpl.getBeans("hibernateUtility");
		loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
				.getBeans("loadAllBillingPlanObjects");
		brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		LoadAllProperties properties = new LoadAllProperties();
		when(memcachedClient.add(anyString(),anyInt(),anyString())).thenReturn(true);
		brokerService.setProperties(properties);
		brokerService.setMemcachedClient(memcachedClient);
		subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("265996795951");
		smsService = mock(SendSmsToKannelService.class);
		brokerService.setSmsService(smsService);
		brokerService.setHibernateUtility(hibernateUtility);
		brokerService.activate("fakeHomiscoFailureWantsRollBack",subscriberDetail);
		assertEquals(subscriberDetail.getShortCode(), "fakeHomiscoFailureWantsRollBack");
	}
	
	@Test
	public void testSubscriberImsiIsUpdated() throws Exception {
		brokerService = new BrokerService();
		hibernateUtility = mock(HibernateUtilityImpl.class);
		ctx = mock(ApplicationContext.class);
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		brokerService = new BrokerService();
		loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
				.getBeans("loadSimSwapBillingPlanObjects");
		brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		brokerService.setMemcachedClient(memcachedClient);
		when(memcachedClient.add(anyString(),anyInt(),anyString())).thenReturn(true);
		brokerService.setProperties(properties);
		subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("265996795951");
		subscriberDetail.setStatus("Active");
		subscriberDetail.setPrepaidSubscriber(1);
		subscriberDetail.setPostpaidSubscriber(0);
		subscriberDetail.setImsi("645017700001");
		String oldImsi = subscriberDetail.getImsi();
		ArrayList<SubscriberDetail> subscriberDetails = new ArrayList<SubscriberDetail>();
		subscriberDetails.add(subscriberDetail);
		when(hibernateUtility.getSubscriberInformation(any(String.class),any(String.class))).thenReturn(subscriberDetail);
		brokerService.setSmsService(smsService);
		brokerService.simswap(subscriberDetail);
		assertNotEquals(oldImsi,subscriberDetail.getImsi());
		assertEquals(subscriberDetail.getImsi(),"645017700000");
	}
	
	@Test
	public void testDeactivate() throws Exception {
		hibernateUtility = mock(HibernateUtilityImpl.class);
		ctx = mock(ApplicationContext.class);
		smsService = mock(SendSmsToKannelService.class);
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		brokerService = new BrokerService();
		loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
				.getBeans("loadDeactivateBillingPlanObjects");
		brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		LoadAllProperties properties = new LoadAllProperties();
		brokerService.setProperties(properties);
		doNothing().when(smsService).sendMessageToKannel(anyString(), anyString());
		brokerService.setSmsService(smsService);
		subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("265996795951");
		subscriberDetail.setShortCode("bbs1");
		when(memcachedClient.add(anyString(),anyInt(),anyString())).thenReturn(true);
		brokerService.setMemcachedClient(memcachedClient);
		assertEquals(0,brokerService.deactivate(subscriberDetail));
	}
	
	@Test
	public void testDroppriceLowerShortCodeAndNewPrice() throws Exception {
		brokerService = new BrokerService();
		loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
				.getBeans("loadAllBillingPlanObjects");
		brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		hibernateUtility = mock(HibernateUtilityImpl.class);
		ctx = mock(ApplicationContext.class);
		smsService = mock(SendSmsToKannelService.class);
		doNothing().when(smsService).sendMessageToKannel(anyString(), anyString());
		brokerService.setSmsService(smsService);
		LoadAllProperties properties = new LoadAllProperties();
		brokerService.setProperties(properties);
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("265996795951");
		brokerService.setMemcachedClient(memcachedClient);
		when(memcachedClient.add(anyString(),anyInt(),anyString())).thenReturn(true);
		brokerService.activate("bb60",subscriberDetail);
		billingPlanObjectLive = ((BillingPlanObjectUtility)ContextLoaderImpl.getBeans("billingPlanObject"));
		assertEquals("bb7",subscriberDetail.getShortCode());
	}	
	
	@Test
	public void testSubscriberForFutureRenewal() throws Exception{
		hibernateUtility = mock(HibernateUtilityImpl.class);
		ctx = mock(ApplicationContext.class);
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		brokerService = new BrokerService();
		loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
				.getBeans("loadAllBillingPlanObjects");
		brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		LoadAllProperties properties = new LoadAllProperties();
		brokerService.setProperties(properties);
		subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("265996795951");
		futureRenewal = (FutureRenewal)ContextLoaderImpl.getBeans("futureRenewal");
		brokerService.setFutureRenewal(futureRenewal);
		brokerService.setMemcachedClient(memcachedClient);
		when(memcachedClient.add(anyString(),anyInt(),anyString())).thenReturn(true);
		brokerService.setSmsService(smsService);
		brokerService.activate("bb30ActiveSubscriber",subscriberDetail);
		assertEquals(subscriberDetail.getMsisdn(), futureRenewal.getMsisdn());
	}
	
	@Test
	public void testPriceNotZeroAfterFutureRenewal() throws Exception{
		hibernateUtility = mock(HibernateUtilityImpl.class);
		ctx = mock(ApplicationContext.class);
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		brokerService = new BrokerService();
		loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
				.getBeans("loadAllBillingPlanObjects");
		brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		LoadAllProperties properties = new LoadAllProperties();
		brokerService.setProperties(properties);
		subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("265996795951");
		futureRenewal = (FutureRenewal)ContextLoaderImpl.getBeans("futureRenewal");
		brokerService.setFutureRenewal(futureRenewal);
		brokerService.setMemcachedClient(memcachedClient);
		when(memcachedClient.add(anyString(),anyInt(),anyString())).thenReturn(true);
		brokerService.setHibernateUtility(hibernateUtility);
		brokerService.setSmsService(smsService);
		brokerService.activate("bb30ActiveSubscriber",subscriberDetail);
		assertNotEquals(brokerService.getBillingPlanObject().getCost(), "0");
	}
	
	@Test
	public void testSuccessMessageIsEmptyForFutureRenewal() throws Exception{
		hibernateUtility = mock(HibernateUtilityImpl.class);
		ctx = mock(ApplicationContext.class);
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		brokerService = new BrokerService();
		loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
				.getBeans("loadAllBillingPlanObjects");
		brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		LoadAllProperties properties = new LoadAllProperties();
		brokerService.setProperties(properties);
		billingPlanObjectLive = (BillingPlanObjectUtility)ContextLoaderImpl.getBeans("billingPlanObject");
		subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("265996795951");
		brokerService.setMemcachedClient(memcachedClient);
		when(memcachedClient.add(anyString(),anyInt(),anyString())).thenReturn(true);
		brokerService.activate("bb30ActiveSubscriber",subscriberDetail);
		assertEquals("", brokerService.getBillingPlanObject().getSuccessMessage());
	}
	
	@Test
	public void testCostShortCodeIsSameAfterFutureRenewal() throws Exception{
		hibernateUtility = mock(HibernateUtilityImpl.class);
		ctx = mock(ApplicationContext.class);
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		brokerService = new BrokerService();
		loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
				.getBeans("loadAllBillingPlanObjects");
		brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		brokerService.setMemcachedClient(memcachedClient);
		when(memcachedClient.add(anyString(),anyInt(),anyString())).thenReturn(true);
		LoadAllProperties properties = new LoadAllProperties();
		brokerService.setProperties(properties);
		subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("265996795951");
		brokerService.activate("bb30ActiveSubscriber",subscriberDetail);
		assertEquals(subscriberDetail.getShortCode(), "bb30ActiveSubscriber");
		
	}
	
	@Test
	public void testSomeAmbigiousShortCodeWasSent(){
		hibernateUtility = mock(HibernateUtilityImpl.class);
		ctx = mock(ApplicationContext.class);
		when(ctx.getBean("hibernateUtility")).thenReturn(hibernateUtility);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		brokerService = new BrokerService();
		loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
				.getBeans("loadAllBillingPlanObjects");
		brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		LoadAllProperties properties = new LoadAllProperties();
		smsService = mock(SendSmsToKannelService.class);
		doNothing().when(smsService).sendMessageToKannel(anyString(), anyString());
		brokerService.setSmsService(smsService);
		brokerService.setProperties(properties);
		subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("265996795951");
		assertEquals(StatusCodes.SHORTCODE_NOT_FOUND, brokerService.activate("AmbigiouShortCode",subscriberDetail));
	}
	
	@Test
	public void testScheduledJob(){
		hibernateUtility = mock(HibernateUtilityImpl.class);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		brokerService = spy(new BrokerService());
		loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
				.getBeans("loadSchedulerBillingPlanObjects");
		brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		LoadAllProperties properties = new LoadAllProperties();
		smsService = mock(SendSmsToKannelService.class);
		doNothing().when(smsService).sendMessageToKannel(anyString(), anyString());
		brokerService.setSmsService(smsService);
		brokerService.setHibernateUtility(hibernateUtility);
		brokerService.setProperties(properties);
		brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		FutureRenewal futureRenewal = new FutureRenewal();
		brokerService.setFutureRenewal(futureRenewal);
		subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("260996795951");
		int status = brokerService.scheduleJob("fakeSchedulerWantsRollBack",subscriberDetail,false);
		System.out.println("Status from Scheduled Job is: "+status);
		verify(brokerService).rollback(subscriberDetail);
	}	
	
	@Test
	public void testScheduledJobSetCorrectSubscriberShortCode(){
		hibernateUtility = mock(HibernateUtilityImpl.class);
		when(hibernateUtility.saveTransactionlog(any(TransactionLog.class))).thenReturn(0);
		brokerService = spy(new BrokerService());
		loadAllBillingPlanObjects = (LoadAllBillingPlanObjects) ContextLoaderImpl
				.getBeans("loadSchedulerBillingPlanObjects");
		brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		LoadAllProperties properties = new LoadAllProperties();
		smsService = mock(SendSmsToKannelService.class);
		doNothing().when(smsService).sendMessageToKannel(anyString(), anyString());
		brokerService.setSmsService(smsService);
		brokerService.setHibernateUtility(hibernateUtility);
		brokerService.setProperties(properties);
		brokerService.setLoadAllBillingPlanObjects(loadAllBillingPlanObjects);
		FutureRenewal futureRenewal = new FutureRenewal();
		brokerService.setFutureRenewal(futureRenewal);
		subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("260996795951");
		brokerService.scheduleJob("shortCodeNotChangesch",subscriberDetail,false);
		assertEquals("shortcodenotchange",subscriberDetail.getShortCode());
	}
	
	@Test
	public void testisNoRollBackErrorCode(){
		LoadAllProperties properties = new LoadAllProperties();
		brokerService = new BrokerService();
		brokerService.setProperties(properties);
		assertTrue(brokerService.isNoRollBackErrorCode(7002));
		assertFalse(brokerService.isNoRollBackErrorCode(1090));
	}
	
	@Test
	public void testisNoRollBackScheduler(){
		LoadAllProperties properties = new LoadAllProperties();
		brokerService = new BrokerService();
		FutureRenewal futureRenewal = new FutureRenewal();
		brokerService.setProperties(properties);
		brokerService.setFutureRenewal(futureRenewal);
		subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("265996795951");
		assertFalse(brokerService.isNoRollBackScheduler(1090,subscriberDetail));
		assertTrue(brokerService.isNoRollBackScheduler(7002,subscriberDetail));
	}
}
