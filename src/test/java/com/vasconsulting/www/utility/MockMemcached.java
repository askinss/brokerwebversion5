package com.vasconsulting.www.utility;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.TimeoutException;

import net.rubyeye.xmemcached.CASOperation;
import net.rubyeye.xmemcached.Counter;
import net.rubyeye.xmemcached.GetsResponse;
import net.rubyeye.xmemcached.KeyIterator;
import net.rubyeye.xmemcached.KeyProvider;
import net.rubyeye.xmemcached.MemcachedClient;
import net.rubyeye.xmemcached.MemcachedClientCallable;
import net.rubyeye.xmemcached.MemcachedClientStateListener;
import net.rubyeye.xmemcached.auth.AuthInfo;
import net.rubyeye.xmemcached.buffer.BufferAllocator;
import net.rubyeye.xmemcached.exception.MemcachedException;
import net.rubyeye.xmemcached.impl.ReconnectRequest;
import net.rubyeye.xmemcached.networking.Connector;
import net.rubyeye.xmemcached.transcoders.Transcoder;
import net.rubyeye.xmemcached.utils.Protocol;

public class MockMemcached implements MemcachedClient {

	public boolean add(String arg0, int arg1, Object arg2)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean add(String arg0, int arg1, Object arg2, long arg3)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public <T> boolean add(String arg0, int arg1, T arg2, Transcoder<T> arg3)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public <T> boolean add(String arg0, int arg1, T arg2, Transcoder<T> arg3,
			long arg4) throws TimeoutException, InterruptedException,
			MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public void addServer(InetSocketAddress arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}

	public void addServer(String arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}

	public void addServer(String arg0, int arg1) throws IOException {
		// TODO Auto-generated method stub
		
	}

	public void addServer(InetSocketAddress arg0, int arg1) throws IOException {
		// TODO Auto-generated method stub
		
	}

	public void addServer(String arg0, int arg1, int arg2) throws IOException {
		// TODO Auto-generated method stub
		
	}

	public void addStateListener(MemcachedClientStateListener arg0) {
		// TODO Auto-generated method stub
		
	}

	public void addWithNoReply(String arg0, int arg1, Object arg2)
			throws InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public <T> void addWithNoReply(String arg0, int arg1, T arg2,
			Transcoder<T> arg3) throws InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public boolean append(String arg0, Object arg1) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean append(String arg0, Object arg1, long arg2)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public void appendWithNoReply(String arg0, Object arg1)
			throws InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public void beginWithNamespace(String arg0) {
		// TODO Auto-generated method stub
		
	}

	public <T> boolean cas(String arg0, CASOperation<T> arg1)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public <T> boolean cas(String arg0, GetsResponse<T> arg1,
			CASOperation<T> arg2) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public <T> boolean cas(String arg0, int arg1, CASOperation<T> arg2)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean cas(String arg0, int arg1, Object arg2, long arg3)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public <T> boolean cas(String arg0, int arg1, CASOperation<T> arg2,
			Transcoder<T> arg3) throws TimeoutException, InterruptedException,
			MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public <T> boolean cas(String arg0, int arg1, GetsResponse<T> arg2,
			CASOperation<T> arg3) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean cas(String arg0, int arg1, Object arg2, long arg3, long arg4)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public <T> boolean cas(String arg0, int arg1, T arg2, Transcoder<T> arg3,
			long arg4) throws TimeoutException, InterruptedException,
			MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public <T> boolean cas(String arg0, int arg1, GetsResponse<T> arg2,
			CASOperation<T> arg3, Transcoder<T> arg4) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public <T> boolean cas(String arg0, int arg1, T arg2, Transcoder<T> arg3,
			long arg4, long arg5) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public <T> void casWithNoReply(String arg0, CASOperation<T> arg1)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public <T> void casWithNoReply(String arg0, GetsResponse<T> arg1,
			CASOperation<T> arg2) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public <T> void casWithNoReply(String arg0, int arg1, CASOperation<T> arg2)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public <T> void casWithNoReply(String arg0, int arg1, GetsResponse<T> arg2,
			CASOperation<T> arg3) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public long decr(String arg0, long arg1) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return 0;
	}

	public long decr(String arg0, long arg1, long arg2)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return 0;
	}

	public long decr(String arg0, long arg1, long arg2, long arg3)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return 0;
	}

	public long decr(String arg0, long arg1, long arg2, long arg3, int arg4)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return 0;
	}

	public void decrWithNoReply(String arg0, long arg1)
			throws InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public boolean delete(String arg0) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean delete(String arg0, int arg1) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean delete(String arg0, long arg1) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean delete(String arg0, long arg1, long arg2)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public void deleteWithNoReply(String arg0) throws InterruptedException,
			MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public void deleteWithNoReply(String arg0, int arg1)
			throws InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public void endWithNamespace() {
		// TODO Auto-generated method stub
		
	}

	public void flushAll() throws TimeoutException, InterruptedException,
			MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public void flushAll(long arg0) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public void flushAll(InetSocketAddress arg0) throws MemcachedException,
			InterruptedException, TimeoutException {
		// TODO Auto-generated method stub
		
	}

	public void flushAll(String arg0) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public void flushAll(InetSocketAddress arg0, long arg1)
			throws MemcachedException, InterruptedException, TimeoutException {
		// TODO Auto-generated method stub
		
	}

	public void flushAll(int arg0, long arg1) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public void flushAll(InetSocketAddress arg0, long arg1, int arg2)
			throws MemcachedException, InterruptedException, TimeoutException {
		// TODO Auto-generated method stub
		
	}

	public void flushAllWithNoReply() throws InterruptedException,
			MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public void flushAllWithNoReply(InetSocketAddress arg0)
			throws MemcachedException, InterruptedException {
		// TODO Auto-generated method stub
		
	}

	public void flushAllWithNoReply(int arg0) throws InterruptedException,
			MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public void flushAllWithNoReply(InetSocketAddress arg0, int arg1)
			throws MemcachedException, InterruptedException {
		// TODO Auto-generated method stub
		
	}

	public <T> T get(String arg0) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> Map<String, T> get(Collection<String> arg0)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> T get(String arg0, long arg1) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> T get(String arg0, Transcoder<T> arg1) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> Map<String, T> get(Collection<String> arg0, Transcoder<T> arg1)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> Map<String, T> get(Collection<String> arg0, long arg1)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> T get(String arg0, long arg1, Transcoder<T> arg2)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> Map<String, T> get(Collection<String> arg0, long arg1,
			Transcoder<T> arg2) throws TimeoutException, InterruptedException,
			MemcachedException {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> T getAndTouch(String arg0, int arg1) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> T getAndTouch(String arg0, int arg1, long arg2)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return null;
	}

	public Map<InetSocketAddress, AuthInfo> getAuthInfoMap() {
		// TODO Auto-generated method stub
		return null;
	}

	public Collection<InetSocketAddress> getAvailableServers() {
		// TODO Auto-generated method stub
		return null;
	}

	public Collection<InetSocketAddress> getAvaliableServers() {
		// TODO Auto-generated method stub
		return null;
	}

	public long getConnectTimeout() {
		// TODO Auto-generated method stub
		return 0;
	}

	public Connector getConnector() {
		// TODO Auto-generated method stub
		return null;
	}

	public Counter getCounter(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public Counter getCounter(String arg0, long arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public long getHealSessionInterval() {
		// TODO Auto-generated method stub
		return 0;
	}

	public KeyIterator getKeyIterator(InetSocketAddress arg0)
			throws MemcachedException, InterruptedException, TimeoutException {
		// TODO Auto-generated method stub
		return null;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	public long getOpTimeout() {
		// TODO Auto-generated method stub
		return 0;
	}

	public Protocol getProtocol() {
		// TODO Auto-generated method stub
		return null;
	}

	public Queue<ReconnectRequest> getReconnectRequestQueue() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<String> getServersDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	public Collection<MemcachedClientStateListener> getStateListeners() {
		// TODO Auto-generated method stub
		return null;
	}

	public Map<InetSocketAddress, Map<String, String>> getStats()
			throws MemcachedException, InterruptedException, TimeoutException {
		// TODO Auto-generated method stub
		return null;
	}

	public Map<InetSocketAddress, Map<String, String>> getStats(long arg0)
			throws MemcachedException, InterruptedException, TimeoutException {
		// TODO Auto-generated method stub
		return null;
	}

	public Map<InetSocketAddress, Map<String, String>> getStatsByItem(
			String arg0) throws MemcachedException, InterruptedException,
			TimeoutException {
		// TODO Auto-generated method stub
		return null;
	}

	public Map<InetSocketAddress, Map<String, String>> getStatsByItem(
			String arg0, long arg1) throws MemcachedException,
			InterruptedException, TimeoutException {
		// TODO Auto-generated method stub
		return null;
	}

	public int getTimeoutExceptionThreshold() {
		// TODO Auto-generated method stub
		return 0;
	}

	public Transcoder getTranscoder() {
		// TODO Auto-generated method stub
		return null;
	}

	public Map<InetSocketAddress, String> getVersions()
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return null;
	}

	public Map<InetSocketAddress, String> getVersions(long arg0)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> GetsResponse<T> gets(String arg0) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> Map<String, GetsResponse<T>> gets(Collection<String> arg0)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> GetsResponse<T> gets(String arg0, long arg1)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> GetsResponse<T> gets(String arg0, Transcoder arg1)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> Map<String, GetsResponse<T>> gets(Collection<String> arg0,
			long arg1) throws TimeoutException, InterruptedException,
			MemcachedException {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> Map<String, GetsResponse<T>> gets(Collection<String> arg0,
			Transcoder<T> arg1) throws TimeoutException, InterruptedException,
			MemcachedException {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> GetsResponse<T> gets(String arg0, long arg1, Transcoder<T> arg2)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> Map<String, GetsResponse<T>> gets(Collection<String> arg0,
			long arg1, Transcoder<T> arg2) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return null;
	}

	public long incr(String arg0, long arg1) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return 0;
	}

	public long incr(String arg0, long arg1, long arg2)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return 0;
	}

	public long incr(String arg0, long arg1, long arg2, long arg3)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return 0;
	}

	public long incr(String arg0, long arg1, long arg2, long arg3, int arg4)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return 0;
	}

	public void incrWithNoReply(String arg0, long arg1)
			throws InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public void invalidateNamespace(String arg0) throws MemcachedException,
			InterruptedException, TimeoutException {
		// TODO Auto-generated method stub
		
	}

	public void invalidateNamespace(String arg0, long arg1)
			throws MemcachedException, InterruptedException, TimeoutException {
		// TODO Auto-generated method stub
		
	}

	public boolean isFailureMode() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean isSanitizeKeys() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean isShutdown() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean prepend(String arg0, Object arg1) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean prepend(String arg0, Object arg1, long arg2)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public void prependWithNoReply(String arg0, Object arg1)
			throws InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public void removeServer(String arg0) {
		// TODO Auto-generated method stub
		
	}

	public void removeStateListener(MemcachedClientStateListener arg0) {
		// TODO Auto-generated method stub
		
	}

	public boolean replace(String arg0, int arg1, Object arg2)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean replace(String arg0, int arg1, Object arg2, long arg3)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public <T> boolean replace(String arg0, int arg1, T arg2, Transcoder<T> arg3)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public <T> boolean replace(String arg0, int arg1, T arg2,
			Transcoder<T> arg3, long arg4) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public void replaceWithNoReply(String arg0, int arg1, Object arg2)
			throws InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public <T> void replaceWithNoReply(String arg0, int arg1, T arg2,
			Transcoder<T> arg3) throws InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public boolean set(String arg0, int arg1, Object arg2)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean set(String arg0, int arg1, Object arg2, long arg3)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public <T> boolean set(String arg0, int arg1, T arg2, Transcoder<T> arg3)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public <T> boolean set(String arg0, int arg1, T arg2, Transcoder<T> arg3,
			long arg4) throws TimeoutException, InterruptedException,
			MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public void setAuthInfoMap(Map<InetSocketAddress, AuthInfo> arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setBufferAllocator(BufferAllocator arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setConnectTimeout(long arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setConnectionPoolSize(int arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setEnableHealSession(boolean arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setEnableHeartBeat(boolean arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setFailureMode(boolean arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setHealSessionInterval(long arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setKeyProvider(KeyProvider arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setLoggingLevelVerbosity(InetSocketAddress arg0, int arg1)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public void setLoggingLevelVerbosityWithNoReply(InetSocketAddress arg0,
			int arg1) throws InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public void setMergeFactor(int arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setName(String arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setOpTimeout(long arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setOptimizeGet(boolean arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setOptimizeMergeBuffer(boolean arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setPrimitiveAsString(boolean arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setSanitizeKeys(boolean arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setTimeoutExceptionThreshold(int arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setTranscoder(Transcoder arg0) {
		// TODO Auto-generated method stub
		
	}

	public void setWithNoReply(String arg0, int arg1, Object arg2)
			throws InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public <T> void setWithNoReply(String arg0, int arg1, T arg2,
			Transcoder<T> arg3) throws InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		
	}

	public void shutdown() throws IOException {
		// TODO Auto-generated method stub
		
	}

	public Map<String, String> stats(InetSocketAddress arg0)
			throws MemcachedException, InterruptedException, TimeoutException {
		// TODO Auto-generated method stub
		return null;
	}

	public Map<String, String> stats(InetSocketAddress arg0, long arg1)
			throws MemcachedException, InterruptedException, TimeoutException {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean touch(String arg0, int arg1) throws TimeoutException,
			InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean touch(String arg0, int arg1, long arg2)
			throws TimeoutException, InterruptedException, MemcachedException {
		// TODO Auto-generated method stub
		return true;
	}

	public <T> T withNamespace(String arg0, MemcachedClientCallable<T> arg1)
			throws MemcachedException, InterruptedException, TimeoutException {
		// TODO Auto-generated method stub
		return null;
	}
	
}
