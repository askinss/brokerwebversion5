package com.vasconsulting.www.utility;

import java.util.HashMap;
import java.util.Map;

import com.celtel.databundle.service.impl.UCIPServiceRequestManager;

public class MockUcipConnector extends UCIPServiceRequestManager {
	
	private String balance;

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public Map<String, String> getSubscriberBalance(String msisdn){
		Map<String, String> responseMap = new HashMap<String, String>();
		responseMap.put("accountValue1", balance);
		responseMap.put("msisdn", msisdn);
		return responseMap;
	}
	
	public Map<String, String>updateSubscriberBalance(String msisdn, 
			String cost, String externalData){
		Map<String, String> responseMap = new HashMap<String, String>();
		responseMap.put("responseCode", "0");
		return responseMap;
	}
}
