package com.vasconsulting.www.utility;

import java.util.HashMap;
import java.util.Map;

import com.celtel.databundle.service.impl.TelnetServiceManager;

public class MockTelnetConnector extends TelnetServiceManager {
	
	private String emaimsi;

	public String getEmaimsi() {
		return emaimsi;
	}

	public void setEmaimsi(String emaimsi) {
		this.emaimsi = emaimsi;
	}

	public Map<String, String> getSubscriberInformation(String msisdn){
		Map<String, String> imsiMap = new HashMap<String, String>();
		imsiMap.put("imsi", emaimsi);
		imsiMap.put("msisdn", msisdn);
		return imsiMap;
	}
	
	public String addBlackberryAPNToSubscriber(String command){
		return "10216";
	}
	
	public String addBlackberryAPNToSubscriber(){
		return "10216";
	}
}
