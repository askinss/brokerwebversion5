package com.vasconsulting.www.utility;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.vasconsulting.www.interfaces.TabsInterface;
import com.vasconsulting.www.invokers.ContextLoaderImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:config/bbspring-servletWS.xml")
@DirtiesContext
public class Tabs6Test {
	private TabsInterface tabs6;
	private String msisdn = "msisdn";
	private String equipID = "equipID";

	@Test
	public void testChangeService() throws Exception {
		tabs6 = (TabsInterface)ContextLoaderImpl.getBeans("tabs");
		assertNotNull(tabs6.changeService(msisdn, equipID));
	}
	
	@Test
	public void testDeleteService() throws Exception{
		tabs6 = (TabsInterface)ContextLoaderImpl.getBeans("tabs");
		assertNotNull(tabs6.deleteService(msisdn, equipID));
	}
	
	@Test
	public void testGetPreOrPost() throws Exception{
		tabs6 = (TabsInterface)ContextLoaderImpl.getBeans("tabs");
		assertNotNull(tabs6.getPreOrPost(msisdn));
	}
	
	@Test
	public void testGetService() throws Exception{
		tabs6 = (TabsInterface)ContextLoaderImpl.getBeans("tabs");
		assertNotNull(tabs6.getService(msisdn));
	}
	
	@Test
	public void testGetSubscriberInfo() throws Exception{
		tabs6 = (TabsInterface)ContextLoaderImpl.getBeans("tabs");
		assertNotNull(tabs6.getSubscriberInformation(msisdn));
	}
}
