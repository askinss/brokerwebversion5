package com.vasconsulting.www.utility;


import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.UUID;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.huawei.www.bme.cbsinterface.cbs.businessmgr.Subscriber;
import com.vasconsulting.www.domain.ErrorDetail;
import com.vasconsulting.www.domain.FutureRenewal;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.domain.TransactionLog;


public class MockHibernateUtility extends HibernateUtilityImpl {

	private SessionFactory sessionFactory;
	private SessionFactory sessionFactoryTabs;
	@Autowired
	private SubscriberDetail subscriberDetailDB;
	private Random rand = new Random();
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = null;
	}
	public SessionFactory getSessionFactoryTabs() {
		return sessionFactoryTabs;
	}
	public void setSessionFactoryTabs(SessionFactory sessionFactoryTabs) {
		this.sessionFactoryTabs = null;
	}
	
	public int saveTransactionlog(TransactionLog transactionLog){
		return 0;
	}
	
	public int saveErrorLogs(ErrorDetail error){
		return 0;
	}
	
	private SubscriberDetail sub(){
		SubscriberDetail subscriberDetail = new SubscriberDetail();
		subscriberDetail.setMsisdn("265996795951");
		subscriberDetail.setStatus("Active");
		subscriberDetail.setPrepaidSubscriber(1);
		subscriberDetail.setPostpaidSubscriber(0);
		subscriberDetail.setImsi("645017700001");
		return subscriberDetail;
	}
	
	public Collection<SubscriberDetail> getSubscriberInformation(SubscriberDetail subscriberDetail) {
		Collection<SubscriberDetail>subs = new ArrayList<SubscriberDetail>();
		subs.add(sub());
		return subs;
		
	}
	
	public SubscriberDetail getSubscriberInformation(String param, String msisdn) {
		return sub();
	}
	
	public ArrayList<SubscriberDetail> getSubscribersExpiringToday(){
		SubscriberDetail sub = new SubscriberDetail();
		sub.setId(UUID.randomUUID().toString());
		sub.setAutoRenew(1);
		sub.setImsi(generateRandomIMSI());
		sub.setMsisdn(generateRandomMSISDN());
		String[] serviceplan = { "Prepaid Prosumer B", "Enterprise Plus B", "Prepaid Complete", "Prepaid Social Plan" };
		sub.setServiceplan(serviceplan[rand.nextInt(3)]);
		int[] servicetype = { 1, 7, 30, 60 };
		sub.setServicetype(servicetype[rand.nextInt(3)]);
		sub.setStatus("Active");
		sub.setShortCode("bb"+sub.getServicetype());
		ArrayList<SubscriberDetail> subs = new ArrayList<SubscriberDetail>();
		subs.add(sub);
		return subs;
	}
	
	private String generateRandomIMSI(){
		int concat = rand.nextInt(100000000);
		return ("6450177" + String.valueOf(concat));
	}
	
	private String generateRandomMSISDN(){
		int concat = rand.nextInt(1000000000);
		return ("260" + String.valueOf(concat));
	}
	
	public int updateSubscriberDetail(SubscriberDetail subscriberDetail){
		return 0;
	}
	
	public int updateFutureRenewal(FutureRenewal futureRenewal){
		return 0;
	}
	
	public int saveFutureRenewal(FutureRenewal futureRenewal) {
		return 0;
	}
	
	public ArrayList<FutureRenewal> subscriberFutureRenewals(String msisdn){
		FutureRenewal futureRenewal = new FutureRenewal();
		futureRenewal.setId(UUID.randomUUID().toString());
		futureRenewal.setMsisdn(msisdn);
		futureRenewal.setPurchasedAt(daysAgo(-1));
		futureRenewal.setStatus("Available");
		futureRenewal.setShortCode("bb30");
		ArrayList<FutureRenewal> futureRenewals = new ArrayList<FutureRenewal>();
		futureRenewals.add(futureRenewal);
		for (int i = 0; i < 4; i++){
			futureRenewal.setId(UUID.randomUUID().toString());
			futureRenewal.setPurchasedAt(daysAgo(-(i + new Random().nextInt(10))));
			futureRenewals.add(futureRenewal);
		}
		return futureRenewals;
	}

	
	private GregorianCalendar daysAgo(int noOfDays){
		GregorianCalendar day = new GregorianCalendar();
		day.set(GregorianCalendar.HOUR, 0);
		day.set(GregorianCalendar.MINUTE, 0);
		day.set(GregorianCalendar.SECOND, 0);
		day.set(GregorianCalendar.MILLISECOND, 0);
		day.set(GregorianCalendar.AM_PM, GregorianCalendar.AM);
		day.add(GregorianCalendar.DAY_OF_MONTH, noOfDays);
		System.out.println("Date calculated is: "+day.getTime());
		return day;
	}
	
}
