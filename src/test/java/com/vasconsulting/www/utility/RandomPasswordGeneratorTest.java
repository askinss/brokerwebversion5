package com.vasconsulting.www.utility;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.test.annotation.Repeat;

public class RandomPasswordGeneratorTest {

	@Test
	@Repeat(10)
	public void passwordMatchUsersRegex() {
		for (int i = 0; i < 10000; i++)
			assertTrue(new String(
					RandomPasswordGenerator.generateRandomPassword())
					.matches("^.*(?=.{8,})(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#%&+]).*$"));
	}

}
