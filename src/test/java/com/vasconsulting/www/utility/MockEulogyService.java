package com.vasconsulting.www.utility;


import java.util.ArrayList;
import java.util.List;



public class MockEulogyService extends EulogyService {
	
	public List<String> updateEulogySubDetails(final String msisdn, final String flag, 
			final String memberId, final String componentId){
		final List<String> responseCode = new ArrayList<String>();
		responseCode.add("N");
		return responseCode;
	}
	
}
