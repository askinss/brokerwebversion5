package com.vasconsulting.www.utility;

import com.vasconsulting.www.interfaces.impl.CreateBlackberryServiceCommandRenewalImpl;

public class MockCreateBlackberrryService extends
		CreateBlackberryServiceCommandRenewalImpl {
	
	public int execute(){
		int response = super.execute();
		if (response == 61080){
			return 0;
		}else return response;
	}

}
