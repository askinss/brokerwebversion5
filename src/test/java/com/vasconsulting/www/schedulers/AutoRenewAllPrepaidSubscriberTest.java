package com.vasconsulting.www.schedulers;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.vasconsulting.www.domain.FutureRenewal;
import com.vasconsulting.www.domain.SubscriberDetail;
import com.vasconsulting.www.invokers.ContextLoaderImpl;
import com.vasconsulting.www.utility.BillingPlanObjectUtility;
import com.vasconsulting.www.utility.BrokerService;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:config/bbspring-test-servletSch.xml")
@DirtiesContext
public class AutoRenewAllPrepaidSubscriberTest {

	private AutoRenewAllPrepaidSubscriber autoRenewAllPrepaidSubscriber;
	private JobExecutionContext arg0;


	@Test
	public void testexecuteInternalSetFutureRenewalPlanStatusToUsed() throws JobExecutionException{
		//TODO rewrite test
	}
	
	@Test
	public void testexecuteInternalSetFutureRenewalPlanUsedDateToToday() throws JobExecutionException{
		//TODO rewrite test
	}

	private String convertTimeToString(Calendar time) {
		Calendar newTime = new GregorianCalendar();
		newTime.set(time.YEAR, time.MONTH, time.DAY_OF_MONTH);
		return String.valueOf(newTime.getTime());
	}

}
